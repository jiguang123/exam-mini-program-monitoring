// index.js
// const app = getApp()
const app = getApp()
Page({
  data: {
    headerHeight: app.globalData.headerHeight,
    statusBarHeight: app.globalData.statusBarHeight,
    entryInfos: [
      { icon: '../../static/images/multiroom.png', title: '多人会议', navigateTo: '../meeting/meeting' },
    ],
  },

  onLoad() {

  },
  selectTemplate(event) {
    console.log('index selectTemplate', event)
    this.setData({
      template: event.detail.value,
    }) 
  },
  handleEntry(e) {
    const url = this.data.entryInfos[e.currentTarget.id].navigateTo
    wx.navigateTo({
      url,
    })
  },

  //点击扫一扫
  scanningback(){

    // console.log("我要上传图片到七牛云")
    // //获取七牛云token 
    // wx.request({
    //   url: 'https://zjapi.eburkt.com/api/services/app/QiNiu/GetQiniuToken', //仅为示例，并非真实的接口地址
    //   header: {
    //   'content-type': 'application/json' // 默认值
    //   },
    //   success (res) {
    //   console.log(res.data.result)
    //   let token = res.data.result
    //   wx.setStorage({
    //     key: 'Token', //本地缓存中的指定的 key
    //     data: token, 
    //   })
    //   }
    //   })


    //   wx.chooseImage({
    //     count: 1,
    //     sizeType: ['original', 'compressed'],
    //     sourceType: ['album', 'camera'],
    //     success (res) {
    //     // tempFilePath可以作为img标签的src属性显示图片
    //     const tempFilePaths = res.tempFilePaths
    //     debugger
    //     wx.uploadFile({
    //       url: 'https://up-z0.qiniup.com/',
    //       filePath: tempFilePaths[0],
    //       name: 'file',
    //       formData: {
    //         token: wx.getStorageSync('Token')
    //       },
    //       success: uploadFileRes => {
    //         var key = JSON.parse(uploadFileRes.data).key;
    //         var partImgUrl = 'http://cdn.eburkt.com/' + key;
    //         console.log("图片地址",partImgUrl)
    //       },
    //       fail: err => {
    //         console.log('fail失败', err);
    //       }
    //     });
    //     }
    //     })

     


    function getUrlParametersAll(url) {
      if (!url.includes("?")) return url;
      let result = {};
      if (url.indexOf("?") !== -1) {
        let str = url.substr(url.indexOf("?") + 1);
        let strs = str.split("&");
        for (let i = 0; i < strs.length; i++) {
          result[strs[i].split("=")[0]] = decodeURI(strs[i].split("=")[1]);
        }
      }
      return result;
    }
    //识别二维码
    wx.scanCode({
        success: res => {
            console.log(res.result);
            if(res.result.indexOf('sigkey') != -1){
     let rr =  getUrlParametersAll(res.result) 
              console.log(rr)
             let localVideo = true
             let localAudio = true
             let roomID =  `${rr.studentId}`
             let userID= rr.studentId
             let sdkAppID = rr.txappid
             let userSig = rr.sigkey
              wx.navigateTo({
                // ?roomID=${roomID}&localVideo=${localVideo}&localAudio=${localAudio}&userID=${userID}&sdkAppID=${Signature.sdkAppID}&userSig=${Signature.userSig}
                url:  `../meeting/room/room?roomID=${roomID}&localVideo=${localVideo}&localAudio=${localAudio}&userID=${userID}&sdkAppID=${sdkAppID}&userSig=${userSig}`,
              })
            }else{
              wx.showToast({
                title: '二维码错误！',
                icon: 'none',
                duration: 2000
              })
            }
        }
    })



  }
})
