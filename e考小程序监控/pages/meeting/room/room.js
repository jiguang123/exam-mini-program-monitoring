import TRTC from '../../../static/trtc-wx';
// let _signalR = new signalR.signalR();
var Hub = require("../../../utils/miniProgramSignalr")
var QQMapWX = require('../../../static/qqmap-wx-jssdk');
const address  = "https://admin.cexam.com.cn"
const sdnaddress = "http://cdn.cexam.com.cn/"
let qqmapsdk = new QQMapWX({
  key: 'NIZBZ-KM36J-E36FQ-KZAHM-565I6-EIBQI'
});
let roomTime = null
Page({
  data: {
    _rtcConfig: {
      sdkAppID: '', // 必要参数 开通实时音视频服务创建应用后分配的 sdkAppID
      userID: '', // 必要参数 用户 ID 可以由您的帐号系统指定
      userSig: '', // 必要参数 身份签名，相当于登录密码的作用
      mode:"SD",
      minBitrate:600,
     
    },
    isshow:false,
    pusher: null,
    playerList: [],
    show_memberList: false,
    localAudio: false,
    localVideo: false,
    direction:"back",
    livePlayerCtx:"",
    ctxs:"",
    MiniTimer : 0,
    device:"back",
    Cheanum:0,
    NetworkPompt:false,
    Mininum:180,
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log(options,"Load")
    let option;
    if(options.q){
      function getUrlParametersAll(url) {
        if (!url.includes("?")) return url;
        let result = {};
        if (url.indexOf("?") !== -1) {
          let str = url.substr(url.indexOf("?") + 1);
          let strs = str.split("&");
          for (let i = 0; i < strs.length; i++) {
            result[strs[i].split("=")[0]] = decodeURI(strs[i].split("=")[1]);
          }
        }
        return result;
      }

    option = getUrlParametersAll(decodeURIComponent(options.q))
    option.roomID = option.studentId
    option.sdkAppID = option.txappid
    option.userID = option.studentId
    option.userSig = option.sigkey
    option.localAudio = "true"
    option.localVideo = "true"
      console.log(option)
    }else{
      option = options
      console.log(option)
    }
  
    let _this = this
    let MiniInterval =  null
    this.hubConnect = new Hub.HubConnection();
    // this.hubConnect.start(`ws://localhost:12155/signalr-message?MiniStudentId=${option.roomID}`);         
    this.hubConnect.start(`wss://admin.cexam.com.cn/signalr-message?MiniStudentId=${option.roomID}`);
    this.hubConnect.onOpen = res => {
      console.log("成功开启连接")
    }
        wx.getLocation({
             type:"gcj02",
             success (res) {
              console.log(res,"授权")
              _this.Loca(option)
              },fail(err) {
                console.log(err,"未授权")
                wx.getSetting({  //先查看授权情况
                  success:function(res){
                    var statu = res.authSetting;
                    if(!statu['scope.userLocation']){   //判断是否授权，没有授权就提示下面的信息
                      wx.showModal({
                        title:'需要获取您的地理位置，请确认授权，否则小程序功能将无法使用',
                        cancelColor: '需要获取您的地理位置，请确认授权，否则地图功能将无法使用',
                        success:function(tip){
                          if(tip.confirm){ //查看是否点击确定
                            wx.openSetting({  //打开设置
                              success:function(data){
                                if(data.authSetting["scope.userLocation"] == true){ //到这一步表示打开了位置授权
                                  wx.showToast({
                                    title: '授权成功',
                                    icon: 'success',
                                    duration: 1000
                                  })
                                  
                                  /*
                                  可以在这里重新请求数据等操作
                                  */
                                 _this.Loca(option)
                                }else{
                                  wx.showToast({
                                    title: '授权失败',
                                    icon: 'none',
                                    duration: 1000
                                  })
                                }
                              },
                              fail:function(){
                              }
                            })
                          }else{
                            wx.showToast({
                              title: '授权失败',
                              icon: 'none',
                              duration: 1000
                            })
                            setTimeout(()=>{
                              wx.redirectTo({
                                url: '/pages/index/index',
                            });
                            },2000)
                          }
                        }
                      })
                    }
                  }
                })
              }
            })
this.Loca(option)
    this.hubConnect.onMessage = res =>{
      // console.log("服务器返回的信息",res)
      if(res=="发送失败"){
          _this.setData({
            NetworkPompt:true
            })
          return false
      }else if(res == "发送成功"){
        _this.setData({
          NetworkPompt:false
          })
      }
      if(res.indexOf("mini") != -1){
          console.log("接收到推流命令")
              _this.setData({
              isshow:true,
              Mininum:180,
              })
              if(_this.data.MiniTimer !== 0){
                _this.setData({
                  MiniTimer:1
                  })
                  this.hubConnect.information("sendStreamState",[{studentId:option.roomID*1,streamType:"mini",isConnected:true,message:null}])
              }
              if (!MiniInterval) {
                console.log("执行计时器")
                MiniInterval = setInterval(() => {
                  if (_this.data.MiniTimer == 0){
                    _this.init(option)
                       _this.enterRoom(option)
                      
                  }
                    _this.data.MiniTimer++
                    
                  if (_this.data.MiniTimer > _this.data.Mininum) {
                    _this.exitRoom() 
                    clearInterval(MiniInterval)
                    MiniInterval = null
                      _this.setData({
                        MiniTimer:0,
                        isshow:false
                        })
                  }
              }, 1000)
              }
      }else if(Number.isFinite(res*1)){
        console.log(res,"screenshot")
        // wx.setStorage({
        //   key: 'ID', //本地缓存中的指定的 key
        //   data: res, 
        // })
        this.screenshot()
      }else if(res == "结束考试"){
        wx.redirectTo({
          url: '/pages/index/index',
      });
      }
    }
 



   
    // _signalR.on("connection", function () {
    //   //消息格式
    //   _signalR.sendMessage([{studentId:options.roomID, streamType: 'mini', isConnected: true, message: null}]);
    // });
    // _signalR.connection(`ws://localhost:44302/signalr-message?StudentId=130756`);//服务端链接
    // console.log('room onload', options)
    // console.log(_signalR,"_signalR")
    //获取相机实例
    this.ctxs = wx.createCameraContext()
    this.TRTC = new TRTC(this)
    // 将String 类型的 true false 转换成 boolean
    Object.getOwnPropertyNames(option).forEach((key) => {
      if (option[key] === 'true') {
        option[key] = true
      }
      if (option[key] === 'false') {
        option[key] = false
      }
    })
    this.init(option)
    this.bindTRTCRoomEvent()


    let _thi = this
  

    //MiniHeart 心跳包 每三秒发一次 考生端如果在10秒内没有接受到 为副摄异常
    roomTime = setInterval(() => {
            _thi.hubConnect.information("MiniHeart",[{studentId:option.roomID*1}])
    }, 3000);

  // this.ctxs.startRecord({
  //   success: (res) => {
  //     console.log('startRecord',res)
  //   }
  // })
  // setTimeout(()=>{
  //   this.ctxs.stopRecord({
  //     success: (res) => {
  //       console.log("开启成功")
  //     }
  //   })
  // },2000)


    //获取七牛云token 
    wx.request({
      url: `${address}/api/services/app/QiNiu/GetQiniuToken`, 
      header: {
      'content-type': 'application/json' // 默认值
      },
      success (res) {
      console.log(res.data.result)
      let token = res.data.result
      wx.setStorage({
        key: 'Token', //本地缓存中的指定的 key
        data: token, 
      })
      }
      })
  },

 Loca(option){
  setTimeout(()=>{
    qqmapsdk.reverseGeocoder({
      success: function(res) {
        console.log(res);
        wx.request({
          url: `${address}/api/services/app/Examinee/UpdateStudentAddress`,
          method: 'put',
          data:{
            "Coordinate":`${res.result.location.lat},${res.result.location.lng}`,
            "Address": res.result.address,
            "Area":res.result.address_component.district,
            "City":res.result.address_component.city,
            "Province":res.result.address_component.province,
            "id":option.roomID
          },
          success: function (res) {
            console.log(res)
          }
        })

      }
     })
  },1000)
 },

  //websocket 接收到截屏信息 进行截屏 判断 是否在推流 this.data.isShow   false ： 没有推流 调相机的 截屏 即可 true : 在推流 调live-pusher api 截屏 
  screenshot(){
    let imgsrc;
    let _this = this
        if(this.data.isshow){
            _this.ctx.snapshot({
                        success(res) {
                          console.log(res,"截屏的数据")
                          wx.uploadFile({
                                          url: 'https://up.qiniup.com/',
                                          filePath: res.tempImagePath,
                                          name: 'file',
                                          formData: {
                                            token: wx.getStorageSync('Token')
                                          },
                                          success: uploadFileRes => {
                                            var key = JSON.parse(uploadFileRes.data).key;
                                            imgsrc = `${sdnaddress}` + key;
                                            console.log("图片地址",imgsrc)
                                            _this.SnapEvnent(imgsrc)
                                          },
                                          fail: err => {
                                            console.log('fail失败', err);
                                          }
                                        });
                        },fail(error) {
                          console.log('fail', error)
                        }})
                    }else{
                        _this.ctxs.takePhoto({
                          quality: 'high',
                          success: (res) => {
                              console.log(res.tempImagePath)
                              wx.uploadFile({
                                      url: 'https://up.qiniup.com/',
                                      filePath: res.tempImagePath,
                                      name: 'file',
                                      formData: {
                                        token: wx.getStorageSync('Token')
                                      },
                                      success: uploadFileRes => {
                                        var key = JSON.parse(uploadFileRes.data).key;
                                        imgsrc = `${sdnaddress}` + key;
                                        console.log("图片地址",imgsrc)
                                        _this.SnapEvnent(imgsrc)
                                      },
                                      fail: err => {
                                        console.log('fail失败', err);
                                      }
                                    });
                            },
                          })
        }

  },

  SnapEvnent(imgsrc){
    console.log(imgsrc,wx.getStorageSync('ID')*1)
    wx.request({
      url:`${address}/api/services/app/Examinee/SaveMiniSnapEvent`,
      method: 'post',
      data:{
        "eventId": wx.getStorageSync('ID')*1,
        "imagePath": imgsrc
      },
      success: function (res) {
        console.log(res)
      }
    })
  },

  onReady(res) {
    this.ctx = wx.createLivePusherContext('pusher')
    this.livePlayerCtx = wx.createLivePlayerContext('player')
    wx.setKeepScreenOn({
      keepScreenOn: true
    })
        //不推流开启 摄像头预览 
        this.ctx.startPreview({
          success(tiem) {
            console.log(tiem,"摄像头预览")
          },
          fail(err){
            console.log(err,"摄像头预览失败")
          }
        })


        //有推流情况下的 截屏 
        this.ctx.snapshot({
          success(res) {
            console.log(res,"截屏的数据")
          },fail(error) {
            console.log('fail', error)
          }})
    },


  
  onUnload() {
    console.log('room unload')
    this.hubConnect.close({ reason: "退出" })
    clearInterval(roomTime)
    roomTime = null
  },

  init(option) {
    // pusher 初始化参数
    const pusherConfig = {
      beautyLevel: 9,
    }
    const pusher = this.TRTC.createPusher(pusherConfig)
    this.setData({
      _rtcConfig: {
        userID: option.userID,
        sdkAppID: option.sdkAppID,
        userSig: option.userSig,
      },
      pusher: pusher.pusherAttributes,
      localAudio: option.localAudio,
      localVideo: option.localVideo
    })
  },
  //停止推流 退出房间
  exitRoom() {
    const result = this.TRTC.exitRoom()
    // 状态机重置，会返回更新后的pusher和playerList
    this.setData({
        pusher: result.pusher,
        playerList: result.playerList,
    })
},

  enterRoom(option) {
    let that = this
    const strRoomID = `${option.roomID}`
const config = Object.assign(this.data._rtcConfig,{strRoomID},{userDefineRecordID: `${option.roomID}`+"_miniRecord"})
this.setData({
  pusher: this.TRTC.setPusherAttributes({mode:'SD',}),
})
    this.setData({
      pusher: this.TRTC.enterRoom(config),
    }, () => {
       this.TRTC.getPusherInstance().start() // 开始推流
       this.hubConnect.information("sendStreamState",[{studentId:option.roomID*1,streamType:"mini",isConnected:true,message:null}])
        //消息格式
      })},
  // 设置 pusher 属性
  setPusherAttributesHandler(option) {
    this.setData({
      pusher: this.TRTC.setPusherAttributes(option),
    })
  },

  // 设置某个 player 属性
  setPlayerAttributesHandler(player, option) {
    this.setData({
      playerList: this.TRTC.setPlayerAttributes(player.streamID, option),
    })
  },
  // 事件监听
  bindTRTCRoomEvent() {
    const TRTC_EVENT = this.TRTC.EVENT
    // 初始化事件订阅
    this.TRTC.on(TRTC_EVENT.LOCAL_JOIN, (event) => {
      console.log('* room LOCAL_JOIN', event)
      getApp().aegisReportEvent('inMeetingRoom', 'inMeetingRoom-success')
      if (this.data.localVideo) {
        this.setPusherAttributesHandler({ enableCamera: true })
      }
      if (this.data.localAudio) {
        this.setPusherAttributesHandler({ enableMic: true })
      }
    })
    this.TRTC.on(TRTC_EVENT.LOCAL_LEAVE, (event) => {
      console.log('* room LOCAL_LEAVE', event)
    })
    //监听推流出错
    this.TRTC.on(TRTC_EVENT.ERROR, (event) => {
 
      console.log('* room ERROR', event)
      console.log("副摄推流出错")
    })
    this.TRTC.on(TRTC_EVENT.REMOTE_USER_JOIN, (event) => {
      console.log('* room REMOTE_USER_JOIN', event)
      const { userID } = event.data
      wx.showToast({
        title: `${userID} 进入了房间`,
        icon: 'none',
        duration: 2000,
      })
    })
    // 远端用户退出
    this.TRTC.on(TRTC_EVENT.REMOTE_USER_LEAVE, (event) => {
      console.log('* room REMOTE_USER_LEAVE', event)
      const { userID, playerList } = event.data
      this.setData({
        playerList: playerList
      })
      wx.showToast({
        title: `${userID} 离开了房间`,
        icon: 'none',
        duration: 2000,
      })
    })
    // 远端用户推送视频
    this.TRTC.on(TRTC_EVENT.REMOTE_VIDEO_ADD, (event) => {
      console.log('* room REMOTE_VIDEO_ADD',  event)
      const { player } = event.data
      // 开始播放远端的视频流，默认是不播放的
      this.setPlayerAttributesHandler(player, { muteVideo: false })
    })
    // 远端用户取消推送视频
    this.TRTC.on(TRTC_EVENT.REMOTE_VIDEO_REMOVE, (event) => {
      console.log('* room REMOTE_VIDEO_REMOVE', event)
      const { player } = event.data
      this.setPlayerAttributesHandler(player, { muteVideo: true })
    })
    // 远端用户推送音频
    this.TRTC.on(TRTC_EVENT.REMOTE_AUDIO_ADD, (event) => {
      console.log('* room REMOTE_AUDIO_ADD', event)
      const { player } = event.data
      this.setPlayerAttributesHandler(player, { muteAudio: false })
    })
    // 远端用户取消推送音频
    this.TRTC.on(TRTC_EVENT.REMOTE_AUDIO_REMOVE, (event) => {
      console.log('* room REMOTE_AUDIO_REMOVE', event)
      const { player } = event.data
      this.setPlayerAttributesHandler(player, { muteAudio: true })
    })
    this.TRTC.on(TRTC_EVENT.REMOTE_AUDIO_VOLUME_UPDATE, (event) => {
      console.log('* room REMOTE_AUDIO_VOLUME_UPDATE', event)
      const { playerList } = event.data
      this.setData({
        playerList: playerList
      })
    })
    this.TRTC.on(TRTC_EVENT.LOCAL_AUDIO_VOLUME_UPDATE, (event) => {
      // console.log('* room LOCAL_AUDIO_VOLUME_UPDATE', event)
      const { pusher } = event.data
      this.setData({
        pusher: pusher
      })
    })
  },

  // 是否订阅某一个player Audio
  _mutePlayerAudio(event) {
    const player = event.currentTarget.dataset.value
    if (player.hasAudio && player.muteAudio) {
      this.setPlayerAttributesHandler(player, { muteAudio: false })
      return
    }
    if (player.hasAudio && !player.muteAudio) {
      this.setPlayerAttributesHandler(player, { muteAudio: true })
      return
    }
  },

  // 订阅 / 取消订阅某一个player Audio
  _mutePlayerVideo(event) {
    const player = event.currentTarget.dataset.value
    if (player.hasVideo && player.muteVideo) {
      this.setPlayerAttributesHandler(player, { muteVideo: false })
      return
    }
    if (player.hasVideo && !player.muteVideo) {
      this.setPlayerAttributesHandler(player, { muteVideo: true })
      return
    }
  },

  // 挂断退出房间
  _hangUp() {
    this.exitRoom()
    wx.redirectTo({
      url: '/pages/index/index',
  });
  },

  // 设置美颜
  _setPusherBeautyHandle() {
    const  beautyLevel = this.data.pusher.beautyLevel === 0 ? 9 : 0
    this.setPusherAttributesHandler({ beautyLevel })
  },

  // 订阅 / 取消订阅 Audio
  _pusherAudioHandler() {
    if (this.data.pusher.enableMic) {
      this.setPusherAttributesHandler({ enableMic: false })
    } else {
      this.setPusherAttributesHandler({ enableMic: true })
    }
  },

  // 前后Video
  _pusherVideoHandler() {
    // if (this.data.pusher.enableCamera) {
    //   this.setPusherAttributesHandler({ enableCamera: false })
    // } else {
    //   this.setPusherAttributesHandler({ enableCamera: true })
    // }

    if(this.data.isshow){
      //推流的 前后转化
      if(this.data.device == "back"){
        // debugger
        this.setData({
          device:"front",
          pusher: this.TRTC.setPusherAttributes({'frontCamera': 'front'})
        })
        this.ctx.switchCamera()
        // this.TRTC.createPusher({'frontCamera': 'front'})
      }else{
        // debugger
        this.setData({
          device:"back",
          pusher: this.TRTC.setPusherAttributes({'frontCamera': 'back'})
        })
        this.ctx.switchCamera()
          // this.TRTC.createPusher({'frontCamera': 'back'})
      }
    }else{
          //相机的前后转换 
        if(this.data.device == "back"){
          this.setData({
            device:"front",
          })
        }else{
          this.setData({
            device:"back",
          })
        }
    }

   
  },


  

  _switchMemberListPanel() {
   
  },
  _handleClose() {
    this.setData({
      show_memberList: false
    })
  },
  // 请保持跟 wxml 中绑定的事件名称一致
  _pusherStateChangeHandler(event) {
    this.TRTC.pusherEventHandler(event)
  },
  _pusherNetStatusHandler(event) {
    this.TRTC.pusherNetStatusHandler(event)
  },
  _pusherErrorHandler(event) {
    this.TRTC.pusherErrorHandler(event)
  },
  _pusherBGMStartHandler(event) {
    this.TRTC.pusherBGMStartHandler(event)
  },
  _pusherBGMProgressHandler(event) {
    this.TRTC.pusherBGMProgressHandler(event)
  },
  _pusherBGMCompleteHandler(event) {
    this.TRTC.pusherBGMCompleteHandler(event)
  },
  _pusherAudioVolumeNotify(event) {
    this.TRTC.pusherAudioVolumeNotify(event)
  },
  _playerStateChange(event) {
    this.TRTC.playerEventHandler(event)
  },
  _playerFullscreenChange(event) {
    this.TRTC.playerFullscreenChange(event)
  },
  _playerNetStatus(event) {
    this.TRTC.playerNetStatus(event)
  },
  _playerAudioVolumeNotify(event) {
    this.TRTC.playerAudioVolumeNotify(event)
  },
})

