(global["webpackJsonp"] = global["webpackJsonp"] || []).push([["common/vendor"],[
/* 0 */,
/* 1 */
/*!*********************************************************!*\
  !*** ./node_modules/@dcloudio/uni-mp-weixin/dist/wx.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var objectKeys = ['qy', 'env', 'error', 'version', 'lanDebug', 'cloud', 'serviceMarket', 'router', 'worklet'];
var singlePageDisableKey = ['lanDebug', 'router', 'worklet'];
var target = typeof globalThis !== 'undefined' ? globalThis : function () {
  return this;
}();
var key = ['w', 'x'].join('');
var oldWx = target[key];
var launchOption = oldWx.getLaunchOptionsSync ? oldWx.getLaunchOptionsSync() : null;
function isWxKey(key) {
  if (launchOption && launchOption.scene === 1154 && singlePageDisableKey.includes(key)) {
    return false;
  }
  return objectKeys.indexOf(key) > -1 || typeof oldWx[key] === 'function';
}
function initWx() {
  var newWx = {};
  for (var _key in oldWx) {
    if (isWxKey(_key)) {
      // TODO wrapper function
      newWx[_key] = oldWx[_key];
    }
  }
  return newWx;
}
target[key] = initWx();
var _default = target[key];
exports.default = _default;

/***/ }),
/* 2 */
/*!************************************************************!*\
  !*** ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(wx, global) {

var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ 4);
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createApp = createApp;
exports.createComponent = createComponent;
exports.createPage = createPage;
exports.createPlugin = createPlugin;
exports.createSubpackageApp = createSubpackageApp;
exports.default = void 0;
var _slicedToArray2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/slicedToArray */ 5));
var _defineProperty2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/defineProperty */ 11));
var _construct2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/construct */ 15));
var _toConsumableArray2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/toConsumableArray */ 18));
var _typeof2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/typeof */ 13));
var _uniI18n = __webpack_require__(/*! @dcloudio/uni-i18n */ 22);
var _vue = _interopRequireDefault(__webpack_require__(/*! vue */ 25));
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
var realAtob;
var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
var b64re = /^(?:[A-Za-z\d+/]{4})*?(?:[A-Za-z\d+/]{2}(?:==)?|[A-Za-z\d+/]{3}=?)?$/;
if (typeof atob !== 'function') {
  realAtob = function realAtob(str) {
    str = String(str).replace(/[\t\n\f\r ]+/g, '');
    if (!b64re.test(str)) {
      throw new Error("Failed to execute 'atob' on 'Window': The string to be decoded is not correctly encoded.");
    }

    // Adding the padding if missing, for semplicity
    str += '=='.slice(2 - (str.length & 3));
    var bitmap;
    var result = '';
    var r1;
    var r2;
    var i = 0;
    for (; i < str.length;) {
      bitmap = b64.indexOf(str.charAt(i++)) << 18 | b64.indexOf(str.charAt(i++)) << 12 | (r1 = b64.indexOf(str.charAt(i++))) << 6 | (r2 = b64.indexOf(str.charAt(i++)));
      result += r1 === 64 ? String.fromCharCode(bitmap >> 16 & 255) : r2 === 64 ? String.fromCharCode(bitmap >> 16 & 255, bitmap >> 8 & 255) : String.fromCharCode(bitmap >> 16 & 255, bitmap >> 8 & 255, bitmap & 255);
    }
    return result;
  };
} else {
  // 注意atob只能在全局对象上调用，例如：`const Base64 = {atob};Base64.atob('xxxx')`是错误的用法
  realAtob = atob;
}
function b64DecodeUnicode(str) {
  return decodeURIComponent(realAtob(str).split('').map(function (c) {
    return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
  }).join(''));
}
function getCurrentUserInfo() {
  var token = wx.getStorageSync('uni_id_token') || '';
  var tokenArr = token.split('.');
  if (!token || tokenArr.length !== 3) {
    return {
      uid: null,
      role: [],
      permission: [],
      tokenExpired: 0
    };
  }
  var userInfo;
  try {
    userInfo = JSON.parse(b64DecodeUnicode(tokenArr[1]));
  } catch (error) {
    throw new Error('获取当前用户信息出错，详细错误信息为：' + error.message);
  }
  userInfo.tokenExpired = userInfo.exp * 1000;
  delete userInfo.exp;
  delete userInfo.iat;
  return userInfo;
}
function uniIdMixin(Vue) {
  Vue.prototype.uniIDHasRole = function (roleId) {
    var _getCurrentUserInfo = getCurrentUserInfo(),
      role = _getCurrentUserInfo.role;
    return role.indexOf(roleId) > -1;
  };
  Vue.prototype.uniIDHasPermission = function (permissionId) {
    var _getCurrentUserInfo2 = getCurrentUserInfo(),
      permission = _getCurrentUserInfo2.permission;
    return this.uniIDHasRole('admin') || permission.indexOf(permissionId) > -1;
  };
  Vue.prototype.uniIDTokenValid = function () {
    var _getCurrentUserInfo3 = getCurrentUserInfo(),
      tokenExpired = _getCurrentUserInfo3.tokenExpired;
    return tokenExpired > Date.now();
  };
}
var _toString = Object.prototype.toString;
var hasOwnProperty = Object.prototype.hasOwnProperty;
function isFn(fn) {
  return typeof fn === 'function';
}
function isStr(str) {
  return typeof str === 'string';
}
function isObject(obj) {
  return obj !== null && (0, _typeof2.default)(obj) === 'object';
}
function isPlainObject(obj) {
  return _toString.call(obj) === '[object Object]';
}
function hasOwn(obj, key) {
  return hasOwnProperty.call(obj, key);
}
function noop() {}

/**
 * Create a cached version of a pure function.
 */
function cached(fn) {
  var cache = Object.create(null);
  return function cachedFn(str) {
    var hit = cache[str];
    return hit || (cache[str] = fn(str));
  };
}

/**
 * Camelize a hyphen-delimited string.
 */
var camelizeRE = /-(\w)/g;
var camelize = cached(function (str) {
  return str.replace(camelizeRE, function (_, c) {
    return c ? c.toUpperCase() : '';
  });
});
function sortObject(obj) {
  var sortObj = {};
  if (isPlainObject(obj)) {
    Object.keys(obj).sort().forEach(function (key) {
      sortObj[key] = obj[key];
    });
  }
  return !Object.keys(sortObj) ? obj : sortObj;
}
var HOOKS = ['invoke', 'success', 'fail', 'complete', 'returnValue'];
var globalInterceptors = {};
var scopedInterceptors = {};
function mergeHook(parentVal, childVal) {
  var res = childVal ? parentVal ? parentVal.concat(childVal) : Array.isArray(childVal) ? childVal : [childVal] : parentVal;
  return res ? dedupeHooks(res) : res;
}
function dedupeHooks(hooks) {
  var res = [];
  for (var i = 0; i < hooks.length; i++) {
    if (res.indexOf(hooks[i]) === -1) {
      res.push(hooks[i]);
    }
  }
  return res;
}
function removeHook(hooks, hook) {
  var index = hooks.indexOf(hook);
  if (index !== -1) {
    hooks.splice(index, 1);
  }
}
function mergeInterceptorHook(interceptor, option) {
  Object.keys(option).forEach(function (hook) {
    if (HOOKS.indexOf(hook) !== -1 && isFn(option[hook])) {
      interceptor[hook] = mergeHook(interceptor[hook], option[hook]);
    }
  });
}
function removeInterceptorHook(interceptor, option) {
  if (!interceptor || !option) {
    return;
  }
  Object.keys(option).forEach(function (hook) {
    if (HOOKS.indexOf(hook) !== -1 && isFn(option[hook])) {
      removeHook(interceptor[hook], option[hook]);
    }
  });
}
function addInterceptor(method, option) {
  if (typeof method === 'string' && isPlainObject(option)) {
    mergeInterceptorHook(scopedInterceptors[method] || (scopedInterceptors[method] = {}), option);
  } else if (isPlainObject(method)) {
    mergeInterceptorHook(globalInterceptors, method);
  }
}
function removeInterceptor(method, option) {
  if (typeof method === 'string') {
    if (isPlainObject(option)) {
      removeInterceptorHook(scopedInterceptors[method], option);
    } else {
      delete scopedInterceptors[method];
    }
  } else if (isPlainObject(method)) {
    removeInterceptorHook(globalInterceptors, method);
  }
}
function wrapperHook(hook, params) {
  return function (data) {
    return hook(data, params) || data;
  };
}
function isPromise(obj) {
  return !!obj && ((0, _typeof2.default)(obj) === 'object' || typeof obj === 'function') && typeof obj.then === 'function';
}
function queue(hooks, data, params) {
  var promise = false;
  for (var i = 0; i < hooks.length; i++) {
    var hook = hooks[i];
    if (promise) {
      promise = Promise.resolve(wrapperHook(hook, params));
    } else {
      var res = hook(data, params);
      if (isPromise(res)) {
        promise = Promise.resolve(res);
      }
      if (res === false) {
        return {
          then: function then() {}
        };
      }
    }
  }
  return promise || {
    then: function then(callback) {
      return callback(data);
    }
  };
}
function wrapperOptions(interceptor) {
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  ['success', 'fail', 'complete'].forEach(function (name) {
    if (Array.isArray(interceptor[name])) {
      var oldCallback = options[name];
      options[name] = function callbackInterceptor(res) {
        queue(interceptor[name], res, options).then(function (res) {
          /* eslint-disable no-mixed-operators */
          return isFn(oldCallback) && oldCallback(res) || res;
        });
      };
    }
  });
  return options;
}
function wrapperReturnValue(method, returnValue) {
  var returnValueHooks = [];
  if (Array.isArray(globalInterceptors.returnValue)) {
    returnValueHooks.push.apply(returnValueHooks, (0, _toConsumableArray2.default)(globalInterceptors.returnValue));
  }
  var interceptor = scopedInterceptors[method];
  if (interceptor && Array.isArray(interceptor.returnValue)) {
    returnValueHooks.push.apply(returnValueHooks, (0, _toConsumableArray2.default)(interceptor.returnValue));
  }
  returnValueHooks.forEach(function (hook) {
    returnValue = hook(returnValue) || returnValue;
  });
  return returnValue;
}
function getApiInterceptorHooks(method) {
  var interceptor = Object.create(null);
  Object.keys(globalInterceptors).forEach(function (hook) {
    if (hook !== 'returnValue') {
      interceptor[hook] = globalInterceptors[hook].slice();
    }
  });
  var scopedInterceptor = scopedInterceptors[method];
  if (scopedInterceptor) {
    Object.keys(scopedInterceptor).forEach(function (hook) {
      if (hook !== 'returnValue') {
        interceptor[hook] = (interceptor[hook] || []).concat(scopedInterceptor[hook]);
      }
    });
  }
  return interceptor;
}
function invokeApi(method, api, options) {
  for (var _len = arguments.length, params = new Array(_len > 3 ? _len - 3 : 0), _key = 3; _key < _len; _key++) {
    params[_key - 3] = arguments[_key];
  }
  var interceptor = getApiInterceptorHooks(method);
  if (interceptor && Object.keys(interceptor).length) {
    if (Array.isArray(interceptor.invoke)) {
      var res = queue(interceptor.invoke, options);
      return res.then(function (options) {
        // 重新访问 getApiInterceptorHooks, 允许 invoke 中再次调用 addInterceptor,removeInterceptor
        return api.apply(void 0, [wrapperOptions(getApiInterceptorHooks(method), options)].concat(params));
      });
    } else {
      return api.apply(void 0, [wrapperOptions(interceptor, options)].concat(params));
    }
  }
  return api.apply(void 0, [options].concat(params));
}
var promiseInterceptor = {
  returnValue: function returnValue(res) {
    if (!isPromise(res)) {
      return res;
    }
    return new Promise(function (resolve, reject) {
      res.then(function (res) {
        if (res[0]) {
          reject(res[0]);
        } else {
          resolve(res[1]);
        }
      });
    });
  }
};
var SYNC_API_RE = /^\$|Window$|WindowStyle$|sendHostEvent|sendNativeEvent|restoreGlobal|requireGlobal|getCurrentSubNVue|getMenuButtonBoundingClientRect|^report|interceptors|Interceptor$|getSubNVueById|requireNativePlugin|upx2px|hideKeyboard|canIUse|^create|Sync$|Manager$|base64ToArrayBuffer|arrayBufferToBase64|getLocale|setLocale|invokePushCallback|getWindowInfo|getDeviceInfo|getAppBaseInfo|getSystemSetting|getAppAuthorizeSetting|initUTS|requireUTS|registerUTS/;
var CONTEXT_API_RE = /^create|Manager$/;

// Context例外情况
var CONTEXT_API_RE_EXC = ['createBLEConnection'];

// 同步例外情况
var ASYNC_API = ['createBLEConnection', 'createPushMessage'];
var CALLBACK_API_RE = /^on|^off/;
function isContextApi(name) {
  return CONTEXT_API_RE.test(name) && CONTEXT_API_RE_EXC.indexOf(name) === -1;
}
function isSyncApi(name) {
  return SYNC_API_RE.test(name) && ASYNC_API.indexOf(name) === -1;
}
function isCallbackApi(name) {
  return CALLBACK_API_RE.test(name) && name !== 'onPush';
}
function handlePromise(promise) {
  return promise.then(function (data) {
    return [null, data];
  }).catch(function (err) {
    return [err];
  });
}
function shouldPromise(name) {
  if (isContextApi(name) || isSyncApi(name) || isCallbackApi(name)) {
    return false;
  }
  return true;
}

/* eslint-disable no-extend-native */
if (!Promise.prototype.finally) {
  Promise.prototype.finally = function (callback) {
    var promise = this.constructor;
    return this.then(function (value) {
      return promise.resolve(callback()).then(function () {
        return value;
      });
    }, function (reason) {
      return promise.resolve(callback()).then(function () {
        throw reason;
      });
    });
  };
}
function promisify(name, api) {
  if (!shouldPromise(name) || !isFn(api)) {
    return api;
  }
  return function promiseApi() {
    var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    for (var _len2 = arguments.length, params = new Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
      params[_key2 - 1] = arguments[_key2];
    }
    if (isFn(options.success) || isFn(options.fail) || isFn(options.complete)) {
      return wrapperReturnValue(name, invokeApi.apply(void 0, [name, api, options].concat(params)));
    }
    return wrapperReturnValue(name, handlePromise(new Promise(function (resolve, reject) {
      invokeApi.apply(void 0, [name, api, Object.assign({}, options, {
        success: resolve,
        fail: reject
      })].concat(params));
    })));
  };
}
var EPS = 1e-4;
var BASE_DEVICE_WIDTH = 750;
var isIOS = false;
var deviceWidth = 0;
var deviceDPR = 0;
function checkDeviceWidth() {
  var _wx$getSystemInfoSync = wx.getSystemInfoSync(),
    platform = _wx$getSystemInfoSync.platform,
    pixelRatio = _wx$getSystemInfoSync.pixelRatio,
    windowWidth = _wx$getSystemInfoSync.windowWidth; // uni=>wx runtime 编译目标是 uni 对象，内部不允许直接使用 uni

  deviceWidth = windowWidth;
  deviceDPR = pixelRatio;
  isIOS = platform === 'ios';
}
function upx2px(number, newDeviceWidth) {
  if (deviceWidth === 0) {
    checkDeviceWidth();
  }
  number = Number(number);
  if (number === 0) {
    return 0;
  }
  var result = number / BASE_DEVICE_WIDTH * (newDeviceWidth || deviceWidth);
  if (result < 0) {
    result = -result;
  }
  result = Math.floor(result + EPS);
  if (result === 0) {
    if (deviceDPR === 1 || !isIOS) {
      result = 1;
    } else {
      result = 0.5;
    }
  }
  return number < 0 ? -result : result;
}
var LOCALE_ZH_HANS = 'zh-Hans';
var LOCALE_ZH_HANT = 'zh-Hant';
var LOCALE_EN = 'en';
var LOCALE_FR = 'fr';
var LOCALE_ES = 'es';
var messages = {};
var locale;
{
  locale = normalizeLocale(wx.getSystemInfoSync().language) || LOCALE_EN;
}
function initI18nMessages() {
  if (!isEnableLocale()) {
    return;
  }
  var localeKeys = Object.keys(__uniConfig.locales);
  if (localeKeys.length) {
    localeKeys.forEach(function (locale) {
      var curMessages = messages[locale];
      var userMessages = __uniConfig.locales[locale];
      if (curMessages) {
        Object.assign(curMessages, userMessages);
      } else {
        messages[locale] = userMessages;
      }
    });
  }
}
initI18nMessages();
var i18n = (0, _uniI18n.initVueI18n)(locale, {});
var t = i18n.t;
var i18nMixin = i18n.mixin = {
  beforeCreate: function beforeCreate() {
    var _this = this;
    var unwatch = i18n.i18n.watchLocale(function () {
      _this.$forceUpdate();
    });
    this.$once('hook:beforeDestroy', function () {
      unwatch();
    });
  },
  methods: {
    $$t: function $$t(key, values) {
      return t(key, values);
    }
  }
};
var setLocale = i18n.setLocale;
var getLocale = i18n.getLocale;
function initAppLocale(Vue, appVm, locale) {
  var state = Vue.observable({
    locale: locale || i18n.getLocale()
  });
  var localeWatchers = [];
  appVm.$watchLocale = function (fn) {
    localeWatchers.push(fn);
  };
  Object.defineProperty(appVm, '$locale', {
    get: function get() {
      return state.locale;
    },
    set: function set(v) {
      state.locale = v;
      localeWatchers.forEach(function (watch) {
        return watch(v);
      });
    }
  });
}
function isEnableLocale() {
  return typeof __uniConfig !== 'undefined' && __uniConfig.locales && !!Object.keys(__uniConfig.locales).length;
}
function include(str, parts) {
  return !!parts.find(function (part) {
    return str.indexOf(part) !== -1;
  });
}
function startsWith(str, parts) {
  return parts.find(function (part) {
    return str.indexOf(part) === 0;
  });
}
function normalizeLocale(locale, messages) {
  if (!locale) {
    return;
  }
  locale = locale.trim().replace(/_/g, '-');
  if (messages && messages[locale]) {
    return locale;
  }
  locale = locale.toLowerCase();
  if (locale === 'chinese') {
    // 支付宝
    return LOCALE_ZH_HANS;
  }
  if (locale.indexOf('zh') === 0) {
    if (locale.indexOf('-hans') > -1) {
      return LOCALE_ZH_HANS;
    }
    if (locale.indexOf('-hant') > -1) {
      return LOCALE_ZH_HANT;
    }
    if (include(locale, ['-tw', '-hk', '-mo', '-cht'])) {
      return LOCALE_ZH_HANT;
    }
    return LOCALE_ZH_HANS;
  }
  var lang = startsWith(locale, [LOCALE_EN, LOCALE_FR, LOCALE_ES]);
  if (lang) {
    return lang;
  }
}
// export function initI18n() {
//   const localeKeys = Object.keys(__uniConfig.locales || {})
//   if (localeKeys.length) {
//     localeKeys.forEach((locale) =>
//       i18n.add(locale, __uniConfig.locales[locale])
//     )
//   }
// }

function getLocale$1() {
  // 优先使用 $locale
  if (isFn(getApp)) {
    var app = getApp({
      allowDefault: true
    });
    if (app && app.$vm) {
      return app.$vm.$locale;
    }
  }
  return normalizeLocale(wx.getSystemInfoSync().language) || LOCALE_EN;
}
function setLocale$1(locale) {
  var app = isFn(getApp) ? getApp() : false;
  if (!app) {
    return false;
  }
  var oldLocale = app.$vm.$locale;
  if (oldLocale !== locale) {
    app.$vm.$locale = locale;
    onLocaleChangeCallbacks.forEach(function (fn) {
      return fn({
        locale: locale
      });
    });
    return true;
  }
  return false;
}
var onLocaleChangeCallbacks = [];
function onLocaleChange(fn) {
  if (onLocaleChangeCallbacks.indexOf(fn) === -1) {
    onLocaleChangeCallbacks.push(fn);
  }
}
if (typeof global !== 'undefined') {
  global.getLocale = getLocale$1;
}
var interceptors = {
  promiseInterceptor: promiseInterceptor
};
var baseApi = /*#__PURE__*/Object.freeze({
  __proto__: null,
  upx2px: upx2px,
  getLocale: getLocale$1,
  setLocale: setLocale$1,
  onLocaleChange: onLocaleChange,
  addInterceptor: addInterceptor,
  removeInterceptor: removeInterceptor,
  interceptors: interceptors
});
function findExistsPageIndex(url) {
  var pages = getCurrentPages();
  var len = pages.length;
  while (len--) {
    var page = pages[len];
    if (page.$page && page.$page.fullPath === url) {
      return len;
    }
  }
  return -1;
}
var redirectTo = {
  name: function name(fromArgs) {
    if (fromArgs.exists === 'back' && fromArgs.delta) {
      return 'navigateBack';
    }
    return 'redirectTo';
  },
  args: function args(fromArgs) {
    if (fromArgs.exists === 'back' && fromArgs.url) {
      var existsPageIndex = findExistsPageIndex(fromArgs.url);
      if (existsPageIndex !== -1) {
        var delta = getCurrentPages().length - 1 - existsPageIndex;
        if (delta > 0) {
          fromArgs.delta = delta;
        }
      }
    }
  }
};
var previewImage = {
  args: function args(fromArgs) {
    var currentIndex = parseInt(fromArgs.current);
    if (isNaN(currentIndex)) {
      return;
    }
    var urls = fromArgs.urls;
    if (!Array.isArray(urls)) {
      return;
    }
    var len = urls.length;
    if (!len) {
      return;
    }
    if (currentIndex < 0) {
      currentIndex = 0;
    } else if (currentIndex >= len) {
      currentIndex = len - 1;
    }
    if (currentIndex > 0) {
      fromArgs.current = urls[currentIndex];
      fromArgs.urls = urls.filter(function (item, index) {
        return index < currentIndex ? item !== urls[currentIndex] : true;
      });
    } else {
      fromArgs.current = urls[0];
    }
    return {
      indicator: false,
      loop: false
    };
  }
};
var UUID_KEY = '__DC_STAT_UUID';
var deviceId;
function useDeviceId(result) {
  deviceId = deviceId || wx.getStorageSync(UUID_KEY);
  if (!deviceId) {
    deviceId = Date.now() + '' + Math.floor(Math.random() * 1e7);
    wx.setStorage({
      key: UUID_KEY,
      data: deviceId
    });
  }
  result.deviceId = deviceId;
}
function addSafeAreaInsets(result) {
  if (result.safeArea) {
    var safeArea = result.safeArea;
    result.safeAreaInsets = {
      top: safeArea.top,
      left: safeArea.left,
      right: result.windowWidth - safeArea.right,
      bottom: result.screenHeight - safeArea.bottom
    };
  }
}
function populateParameters(result) {
  var _result$brand = result.brand,
    brand = _result$brand === void 0 ? '' : _result$brand,
    _result$model = result.model,
    model = _result$model === void 0 ? '' : _result$model,
    _result$system = result.system,
    system = _result$system === void 0 ? '' : _result$system,
    _result$language = result.language,
    language = _result$language === void 0 ? '' : _result$language,
    theme = result.theme,
    version = result.version,
    platform = result.platform,
    fontSizeSetting = result.fontSizeSetting,
    SDKVersion = result.SDKVersion,
    pixelRatio = result.pixelRatio,
    deviceOrientation = result.deviceOrientation;
  // const isQuickApp = "mp-weixin".indexOf('quickapp-webview') !== -1

  var extraParam = {};

  // osName osVersion
  var osName = '';
  var osVersion = '';
  {
    osName = system.split(' ')[0] || '';
    osVersion = system.split(' ')[1] || '';
  }
  var hostVersion = version;

  // deviceType
  var deviceType = getGetDeviceType(result, model);

  // deviceModel
  var deviceBrand = getDeviceBrand(brand);

  // hostName
  var _hostName = getHostName(result);

  // deviceOrientation
  var _deviceOrientation = deviceOrientation; // 仅 微信 百度 支持

  // devicePixelRatio
  var _devicePixelRatio = pixelRatio;

  // SDKVersion
  var _SDKVersion = SDKVersion;

  // hostLanguage
  var hostLanguage = language.replace(/_/g, '-');

  // wx.getAccountInfoSync

  var parameters = {
    appId: "__UNI__9B401D5",
    appName: "e考监控",
    appVersion: "1.0.5",
    appVersionCode: "105",
    appLanguage: getAppLanguage(hostLanguage),
    uniCompileVersion: "3.8.4",
    uniRuntimeVersion: "3.8.4",
    uniPlatform: undefined || "mp-weixin",
    deviceBrand: deviceBrand,
    deviceModel: model,
    deviceType: deviceType,
    devicePixelRatio: _devicePixelRatio,
    deviceOrientation: _deviceOrientation,
    osName: osName.toLocaleLowerCase(),
    osVersion: osVersion,
    hostTheme: theme,
    hostVersion: hostVersion,
    hostLanguage: hostLanguage,
    hostName: _hostName,
    hostSDKVersion: _SDKVersion,
    hostFontSizeSetting: fontSizeSetting,
    windowTop: 0,
    windowBottom: 0,
    // TODO
    osLanguage: undefined,
    osTheme: undefined,
    ua: undefined,
    hostPackageName: undefined,
    browserName: undefined,
    browserVersion: undefined
  };
  Object.assign(result, parameters, extraParam);
}
function getGetDeviceType(result, model) {
  var deviceType = result.deviceType || 'phone';
  {
    var deviceTypeMaps = {
      ipad: 'pad',
      windows: 'pc',
      mac: 'pc'
    };
    var deviceTypeMapsKeys = Object.keys(deviceTypeMaps);
    var _model = model.toLocaleLowerCase();
    for (var index = 0; index < deviceTypeMapsKeys.length; index++) {
      var _m = deviceTypeMapsKeys[index];
      if (_model.indexOf(_m) !== -1) {
        deviceType = deviceTypeMaps[_m];
        break;
      }
    }
  }
  return deviceType;
}
function getDeviceBrand(brand) {
  var deviceBrand = brand;
  if (deviceBrand) {
    deviceBrand = brand.toLocaleLowerCase();
  }
  return deviceBrand;
}
function getAppLanguage(defaultLanguage) {
  return getLocale$1 ? getLocale$1() : defaultLanguage;
}
function getHostName(result) {
  var _platform = 'WeChat';
  var _hostName = result.hostName || _platform; // mp-jd
  {
    if (result.environment) {
      _hostName = result.environment;
    } else if (result.host && result.host.env) {
      _hostName = result.host.env;
    }
  }
  return _hostName;
}
var getSystemInfo = {
  returnValue: function returnValue(result) {
    useDeviceId(result);
    addSafeAreaInsets(result);
    populateParameters(result);
  }
};
var showActionSheet = {
  args: function args(fromArgs) {
    if ((0, _typeof2.default)(fromArgs) === 'object') {
      fromArgs.alertText = fromArgs.title;
    }
  }
};
var getAppBaseInfo = {
  returnValue: function returnValue(result) {
    var _result = result,
      version = _result.version,
      language = _result.language,
      SDKVersion = _result.SDKVersion,
      theme = _result.theme;
    var _hostName = getHostName(result);
    var hostLanguage = language.replace('_', '-');
    result = sortObject(Object.assign(result, {
      appId: "__UNI__9B401D5",
      appName: "e考监控",
      appVersion: "1.0.5",
      appVersionCode: "105",
      appLanguage: getAppLanguage(hostLanguage),
      hostVersion: version,
      hostLanguage: hostLanguage,
      hostName: _hostName,
      hostSDKVersion: SDKVersion,
      hostTheme: theme
    }));
  }
};
var getDeviceInfo = {
  returnValue: function returnValue(result) {
    var _result2 = result,
      brand = _result2.brand,
      model = _result2.model;
    var deviceType = getGetDeviceType(result, model);
    var deviceBrand = getDeviceBrand(brand);
    useDeviceId(result);
    result = sortObject(Object.assign(result, {
      deviceType: deviceType,
      deviceBrand: deviceBrand,
      deviceModel: model
    }));
  }
};
var getWindowInfo = {
  returnValue: function returnValue(result) {
    addSafeAreaInsets(result);
    result = sortObject(Object.assign(result, {
      windowTop: 0,
      windowBottom: 0
    }));
  }
};
var getAppAuthorizeSetting = {
  returnValue: function returnValue(result) {
    var locationReducedAccuracy = result.locationReducedAccuracy;
    result.locationAccuracy = 'unsupported';
    if (locationReducedAccuracy === true) {
      result.locationAccuracy = 'reduced';
    } else if (locationReducedAccuracy === false) {
      result.locationAccuracy = 'full';
    }
  }
};

// import navigateTo from 'uni-helpers/navigate-to'

var compressImage = {
  args: function args(fromArgs) {
    // https://developers.weixin.qq.com/community/develop/doc/000c08940c865011298e0a43256800?highLine=compressHeight
    if (fromArgs.compressedHeight && !fromArgs.compressHeight) {
      fromArgs.compressHeight = fromArgs.compressedHeight;
    }
    if (fromArgs.compressedWidth && !fromArgs.compressWidth) {
      fromArgs.compressWidth = fromArgs.compressedWidth;
    }
  }
};
var protocols = {
  redirectTo: redirectTo,
  // navigateTo,  // 由于在微信开发者工具的页面参数，会显示__id__参数，因此暂时关闭mp-weixin对于navigateTo的AOP
  previewImage: previewImage,
  getSystemInfo: getSystemInfo,
  getSystemInfoSync: getSystemInfo,
  showActionSheet: showActionSheet,
  getAppBaseInfo: getAppBaseInfo,
  getDeviceInfo: getDeviceInfo,
  getWindowInfo: getWindowInfo,
  getAppAuthorizeSetting: getAppAuthorizeSetting,
  compressImage: compressImage
};
var todos = ['vibrate', 'preloadPage', 'unPreloadPage', 'loadSubPackage'];
var canIUses = [];
var CALLBACKS = ['success', 'fail', 'cancel', 'complete'];
function processCallback(methodName, method, returnValue) {
  return function (res) {
    return method(processReturnValue(methodName, res, returnValue));
  };
}
function processArgs(methodName, fromArgs) {
  var argsOption = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
  var returnValue = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  var keepFromArgs = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
  if (isPlainObject(fromArgs)) {
    // 一般 api 的参数解析
    var toArgs = keepFromArgs === true ? fromArgs : {}; // returnValue 为 false 时，说明是格式化返回值，直接在返回值对象上修改赋值
    if (isFn(argsOption)) {
      argsOption = argsOption(fromArgs, toArgs) || {};
    }
    for (var key in fromArgs) {
      if (hasOwn(argsOption, key)) {
        var keyOption = argsOption[key];
        if (isFn(keyOption)) {
          keyOption = keyOption(fromArgs[key], fromArgs, toArgs);
        }
        if (!keyOption) {
          // 不支持的参数
          console.warn("The '".concat(methodName, "' method of platform '\u5FAE\u4FE1\u5C0F\u7A0B\u5E8F' does not support option '").concat(key, "'"));
        } else if (isStr(keyOption)) {
          // 重写参数 key
          toArgs[keyOption] = fromArgs[key];
        } else if (isPlainObject(keyOption)) {
          // {name:newName,value:value}可重新指定参数 key:value
          toArgs[keyOption.name ? keyOption.name : key] = keyOption.value;
        }
      } else if (CALLBACKS.indexOf(key) !== -1) {
        if (isFn(fromArgs[key])) {
          toArgs[key] = processCallback(methodName, fromArgs[key], returnValue);
        }
      } else {
        if (!keepFromArgs) {
          toArgs[key] = fromArgs[key];
        }
      }
    }
    return toArgs;
  } else if (isFn(fromArgs)) {
    fromArgs = processCallback(methodName, fromArgs, returnValue);
  }
  return fromArgs;
}
function processReturnValue(methodName, res, returnValue) {
  var keepReturnValue = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
  if (isFn(protocols.returnValue)) {
    // 处理通用 returnValue
    res = protocols.returnValue(methodName, res);
  }
  return processArgs(methodName, res, returnValue, {}, keepReturnValue);
}
function wrapper(methodName, method) {
  if (hasOwn(protocols, methodName)) {
    var protocol = protocols[methodName];
    if (!protocol) {
      // 暂不支持的 api
      return function () {
        console.error("Platform '\u5FAE\u4FE1\u5C0F\u7A0B\u5E8F' does not support '".concat(methodName, "'."));
      };
    }
    return function (arg1, arg2) {
      // 目前 api 最多两个参数
      var options = protocol;
      if (isFn(protocol)) {
        options = protocol(arg1);
      }
      arg1 = processArgs(methodName, arg1, options.args, options.returnValue);
      var args = [arg1];
      if (typeof arg2 !== 'undefined') {
        args.push(arg2);
      }
      if (isFn(options.name)) {
        methodName = options.name(arg1);
      } else if (isStr(options.name)) {
        methodName = options.name;
      }
      var returnValue = wx[methodName].apply(wx, args);
      if (isSyncApi(methodName)) {
        // 同步 api
        return processReturnValue(methodName, returnValue, options.returnValue, isContextApi(methodName));
      }
      return returnValue;
    };
  }
  return method;
}
var todoApis = Object.create(null);
var TODOS = ['onTabBarMidButtonTap', 'subscribePush', 'unsubscribePush', 'onPush', 'offPush', 'share'];
function createTodoApi(name) {
  return function todoApi(_ref) {
    var fail = _ref.fail,
      complete = _ref.complete;
    var res = {
      errMsg: "".concat(name, ":fail method '").concat(name, "' not supported")
    };
    isFn(fail) && fail(res);
    isFn(complete) && complete(res);
  };
}
TODOS.forEach(function (name) {
  todoApis[name] = createTodoApi(name);
});
var providers = {
  oauth: ['weixin'],
  share: ['weixin'],
  payment: ['wxpay'],
  push: ['weixin']
};
function getProvider(_ref2) {
  var service = _ref2.service,
    success = _ref2.success,
    fail = _ref2.fail,
    complete = _ref2.complete;
  var res = false;
  if (providers[service]) {
    res = {
      errMsg: 'getProvider:ok',
      service: service,
      provider: providers[service]
    };
    isFn(success) && success(res);
  } else {
    res = {
      errMsg: 'getProvider:fail service not found'
    };
    isFn(fail) && fail(res);
  }
  isFn(complete) && complete(res);
}
var extraApi = /*#__PURE__*/Object.freeze({
  __proto__: null,
  getProvider: getProvider
});
var getEmitter = function () {
  var Emitter;
  return function getUniEmitter() {
    if (!Emitter) {
      Emitter = new _vue.default();
    }
    return Emitter;
  };
}();
function apply(ctx, method, args) {
  return ctx[method].apply(ctx, args);
}
function $on() {
  return apply(getEmitter(), '$on', Array.prototype.slice.call(arguments));
}
function $off() {
  return apply(getEmitter(), '$off', Array.prototype.slice.call(arguments));
}
function $once() {
  return apply(getEmitter(), '$once', Array.prototype.slice.call(arguments));
}
function $emit() {
  return apply(getEmitter(), '$emit', Array.prototype.slice.call(arguments));
}
var eventApi = /*#__PURE__*/Object.freeze({
  __proto__: null,
  $on: $on,
  $off: $off,
  $once: $once,
  $emit: $emit
});

/**
 * 框架内 try-catch
 */
/**
 * 开发者 try-catch
 */
function tryCatch(fn) {
  return function () {
    try {
      return fn.apply(fn, arguments);
    } catch (e) {
      // TODO
      console.error(e);
    }
  };
}
function getApiCallbacks(params) {
  var apiCallbacks = {};
  for (var name in params) {
    var param = params[name];
    if (isFn(param)) {
      apiCallbacks[name] = tryCatch(param);
      delete params[name];
    }
  }
  return apiCallbacks;
}
var cid;
var cidErrMsg;
var enabled;
function normalizePushMessage(message) {
  try {
    return JSON.parse(message);
  } catch (e) {}
  return message;
}
function invokePushCallback(args) {
  if (args.type === 'enabled') {
    enabled = true;
  } else if (args.type === 'clientId') {
    cid = args.cid;
    cidErrMsg = args.errMsg;
    invokeGetPushCidCallbacks(cid, args.errMsg);
  } else if (args.type === 'pushMsg') {
    var message = {
      type: 'receive',
      data: normalizePushMessage(args.message)
    };
    for (var i = 0; i < onPushMessageCallbacks.length; i++) {
      var callback = onPushMessageCallbacks[i];
      callback(message);
      // 该消息已被阻止
      if (message.stopped) {
        break;
      }
    }
  } else if (args.type === 'click') {
    onPushMessageCallbacks.forEach(function (callback) {
      callback({
        type: 'click',
        data: normalizePushMessage(args.message)
      });
    });
  }
}
var getPushCidCallbacks = [];
function invokeGetPushCidCallbacks(cid, errMsg) {
  getPushCidCallbacks.forEach(function (callback) {
    callback(cid, errMsg);
  });
  getPushCidCallbacks.length = 0;
}
function getPushClientId(args) {
  if (!isPlainObject(args)) {
    args = {};
  }
  var _getApiCallbacks = getApiCallbacks(args),
    success = _getApiCallbacks.success,
    fail = _getApiCallbacks.fail,
    complete = _getApiCallbacks.complete;
  var hasSuccess = isFn(success);
  var hasFail = isFn(fail);
  var hasComplete = isFn(complete);
  Promise.resolve().then(function () {
    if (typeof enabled === 'undefined') {
      enabled = false;
      cid = '';
      cidErrMsg = 'uniPush is not enabled';
    }
    getPushCidCallbacks.push(function (cid, errMsg) {
      var res;
      if (cid) {
        res = {
          errMsg: 'getPushClientId:ok',
          cid: cid
        };
        hasSuccess && success(res);
      } else {
        res = {
          errMsg: 'getPushClientId:fail' + (errMsg ? ' ' + errMsg : '')
        };
        hasFail && fail(res);
      }
      hasComplete && complete(res);
    });
    if (typeof cid !== 'undefined') {
      invokeGetPushCidCallbacks(cid, cidErrMsg);
    }
  });
}
var onPushMessageCallbacks = [];
// 不使用 defineOnApi 实现，是因为 defineOnApi 依赖 UniServiceJSBridge ，该对象目前在小程序上未提供，故简单实现
var onPushMessage = function onPushMessage(fn) {
  if (onPushMessageCallbacks.indexOf(fn) === -1) {
    onPushMessageCallbacks.push(fn);
  }
};
var offPushMessage = function offPushMessage(fn) {
  if (!fn) {
    onPushMessageCallbacks.length = 0;
  } else {
    var index = onPushMessageCallbacks.indexOf(fn);
    if (index > -1) {
      onPushMessageCallbacks.splice(index, 1);
    }
  }
};
var baseInfo = wx.getAppBaseInfo && wx.getAppBaseInfo();
if (!baseInfo) {
  baseInfo = wx.getSystemInfoSync();
}
var host = baseInfo ? baseInfo.host : null;
var shareVideoMessage = host && host.env === 'SAAASDK' ? wx.miniapp.shareVideoMessage : wx.shareVideoMessage;
var api = /*#__PURE__*/Object.freeze({
  __proto__: null,
  shareVideoMessage: shareVideoMessage,
  getPushClientId: getPushClientId,
  onPushMessage: onPushMessage,
  offPushMessage: offPushMessage,
  invokePushCallback: invokePushCallback
});
var mocks = ['__route__', '__wxExparserNodeId__', '__wxWebviewId__'];
function findVmByVueId(vm, vuePid) {
  var $children = vm.$children;
  // 优先查找直属(反向查找:https://github.com/dcloudio/uni-app/issues/1200)
  for (var i = $children.length - 1; i >= 0; i--) {
    var childVm = $children[i];
    if (childVm.$scope._$vueId === vuePid) {
      return childVm;
    }
  }
  // 反向递归查找
  var parentVm;
  for (var _i = $children.length - 1; _i >= 0; _i--) {
    parentVm = findVmByVueId($children[_i], vuePid);
    if (parentVm) {
      return parentVm;
    }
  }
}
function initBehavior(options) {
  return Behavior(options);
}
function isPage() {
  return !!this.route;
}
function initRelation(detail) {
  this.triggerEvent('__l', detail);
}
function selectAllComponents(mpInstance, selector, $refs) {
  var components = mpInstance.selectAllComponents(selector) || [];
  components.forEach(function (component) {
    var ref = component.dataset.ref;
    $refs[ref] = component.$vm || toSkip(component);
    {
      if (component.dataset.vueGeneric === 'scoped') {
        component.selectAllComponents('.scoped-ref').forEach(function (scopedComponent) {
          selectAllComponents(scopedComponent, selector, $refs);
        });
      }
    }
  });
}
function syncRefs(refs, newRefs) {
  var oldKeys = (0, _construct2.default)(Set, (0, _toConsumableArray2.default)(Object.keys(refs)));
  var newKeys = Object.keys(newRefs);
  newKeys.forEach(function (key) {
    var oldValue = refs[key];
    var newValue = newRefs[key];
    if (Array.isArray(oldValue) && Array.isArray(newValue) && oldValue.length === newValue.length && newValue.every(function (value) {
      return oldValue.includes(value);
    })) {
      return;
    }
    refs[key] = newValue;
    oldKeys.delete(key);
  });
  oldKeys.forEach(function (key) {
    delete refs[key];
  });
  return refs;
}
function initRefs(vm) {
  var mpInstance = vm.$scope;
  var refs = {};
  Object.defineProperty(vm, '$refs', {
    get: function get() {
      var $refs = {};
      selectAllComponents(mpInstance, '.vue-ref', $refs);
      // TODO 暂不考虑 for 中的 scoped
      var forComponents = mpInstance.selectAllComponents('.vue-ref-in-for') || [];
      forComponents.forEach(function (component) {
        var ref = component.dataset.ref;
        if (!$refs[ref]) {
          $refs[ref] = [];
        }
        $refs[ref].push(component.$vm || toSkip(component));
      });
      return syncRefs(refs, $refs);
    }
  });
}
function handleLink(event) {
  var _ref3 = event.detail || event.value,
    vuePid = _ref3.vuePid,
    vueOptions = _ref3.vueOptions; // detail 是微信,value 是百度(dipatch)

  var parentVm;
  if (vuePid) {
    parentVm = findVmByVueId(this.$vm, vuePid);
  }
  if (!parentVm) {
    parentVm = this.$vm;
  }
  vueOptions.parent = parentVm;
}
function markMPComponent(component) {
  // 在 Vue 中标记为小程序组件
  var IS_MP = '__v_isMPComponent';
  Object.defineProperty(component, IS_MP, {
    configurable: true,
    enumerable: false,
    value: true
  });
  return component;
}
function toSkip(obj) {
  var OB = '__ob__';
  var SKIP = '__v_skip';
  if (isObject(obj) && Object.isExtensible(obj)) {
    // 避免被 @vue/composition-api 观测
    Object.defineProperty(obj, OB, {
      configurable: true,
      enumerable: false,
      value: (0, _defineProperty2.default)({}, SKIP, true)
    });
  }
  return obj;
}
var WORKLET_RE = /_(.*)_worklet_factory_/;
function initWorkletMethods(mpMethods, vueMethods) {
  if (vueMethods) {
    Object.keys(vueMethods).forEach(function (name) {
      var matches = name.match(WORKLET_RE);
      if (matches) {
        var workletName = matches[1];
        mpMethods[name] = vueMethods[name];
        mpMethods[workletName] = vueMethods[workletName];
      }
    });
  }
}
var MPPage = Page;
var MPComponent = Component;
var customizeRE = /:/g;
var customize = cached(function (str) {
  return camelize(str.replace(customizeRE, '-'));
});
function initTriggerEvent(mpInstance) {
  var oldTriggerEvent = mpInstance.triggerEvent;
  var newTriggerEvent = function newTriggerEvent(event) {
    for (var _len3 = arguments.length, args = new Array(_len3 > 1 ? _len3 - 1 : 0), _key3 = 1; _key3 < _len3; _key3++) {
      args[_key3 - 1] = arguments[_key3];
    }
    // 事件名统一转驼峰格式，仅处理：当前组件为 vue 组件、当前组件为 vue 组件子组件
    if (this.$vm || this.dataset && this.dataset.comType) {
      event = customize(event);
    } else {
      // 针对微信/QQ小程序单独补充驼峰格式事件，以兼容历史项目
      var newEvent = customize(event);
      if (newEvent !== event) {
        oldTriggerEvent.apply(this, [newEvent].concat(args));
      }
    }
    return oldTriggerEvent.apply(this, [event].concat(args));
  };
  try {
    // 京东小程序 triggerEvent 为只读
    mpInstance.triggerEvent = newTriggerEvent;
  } catch (error) {
    mpInstance._triggerEvent = newTriggerEvent;
  }
}
function initHook(name, options, isComponent) {
  var oldHook = options[name];
  options[name] = function () {
    markMPComponent(this);
    initTriggerEvent(this);
    if (oldHook) {
      for (var _len4 = arguments.length, args = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
        args[_key4] = arguments[_key4];
      }
      return oldHook.apply(this, args);
    }
  };
}
if (!MPPage.__$wrappered) {
  MPPage.__$wrappered = true;
  Page = function Page() {
    var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    initHook('onLoad', options);
    return MPPage(options);
  };
  Page.after = MPPage.after;
  Component = function Component() {
    var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    initHook('created', options);
    return MPComponent(options);
  };
}
var PAGE_EVENT_HOOKS = ['onPullDownRefresh', 'onReachBottom', 'onAddToFavorites', 'onShareTimeline', 'onShareAppMessage', 'onPageScroll', 'onResize', 'onTabItemTap'];
function initMocks(vm, mocks) {
  var mpInstance = vm.$mp[vm.mpType];
  mocks.forEach(function (mock) {
    if (hasOwn(mpInstance, mock)) {
      vm[mock] = mpInstance[mock];
    }
  });
}
function hasHook(hook, vueOptions) {
  if (!vueOptions) {
    return true;
  }
  if (_vue.default.options && Array.isArray(_vue.default.options[hook])) {
    return true;
  }
  vueOptions = vueOptions.default || vueOptions;
  if (isFn(vueOptions)) {
    if (isFn(vueOptions.extendOptions[hook])) {
      return true;
    }
    if (vueOptions.super && vueOptions.super.options && Array.isArray(vueOptions.super.options[hook])) {
      return true;
    }
    return false;
  }
  if (isFn(vueOptions[hook]) || Array.isArray(vueOptions[hook])) {
    return true;
  }
  var mixins = vueOptions.mixins;
  if (Array.isArray(mixins)) {
    return !!mixins.find(function (mixin) {
      return hasHook(hook, mixin);
    });
  }
}
function initHooks(mpOptions, hooks, vueOptions) {
  hooks.forEach(function (hook) {
    if (hasHook(hook, vueOptions)) {
      mpOptions[hook] = function (args) {
        return this.$vm && this.$vm.__call_hook(hook, args);
      };
    }
  });
}
function initUnknownHooks(mpOptions, vueOptions) {
  var excludes = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];
  findHooks(vueOptions).forEach(function (hook) {
    return initHook$1(mpOptions, hook, excludes);
  });
}
function findHooks(vueOptions) {
  var hooks = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
  if (vueOptions) {
    Object.keys(vueOptions).forEach(function (name) {
      if (name.indexOf('on') === 0 && isFn(vueOptions[name])) {
        hooks.push(name);
      }
    });
  }
  return hooks;
}
function initHook$1(mpOptions, hook, excludes) {
  if (excludes.indexOf(hook) === -1 && !hasOwn(mpOptions, hook)) {
    mpOptions[hook] = function (args) {
      return this.$vm && this.$vm.__call_hook(hook, args);
    };
  }
}
function initVueComponent(Vue, vueOptions) {
  vueOptions = vueOptions.default || vueOptions;
  var VueComponent;
  if (isFn(vueOptions)) {
    VueComponent = vueOptions;
  } else {
    VueComponent = Vue.extend(vueOptions);
  }
  vueOptions = VueComponent.options;
  return [VueComponent, vueOptions];
}
function initSlots(vm, vueSlots) {
  if (Array.isArray(vueSlots) && vueSlots.length) {
    var $slots = Object.create(null);
    vueSlots.forEach(function (slotName) {
      $slots[slotName] = true;
    });
    vm.$scopedSlots = vm.$slots = $slots;
  }
}
function initVueIds(vueIds, mpInstance) {
  vueIds = (vueIds || '').split(',');
  var len = vueIds.length;
  if (len === 1) {
    mpInstance._$vueId = vueIds[0];
  } else if (len === 2) {
    mpInstance._$vueId = vueIds[0];
    mpInstance._$vuePid = vueIds[1];
  }
}
function initData(vueOptions, context) {
  var data = vueOptions.data || {};
  var methods = vueOptions.methods || {};
  if (typeof data === 'function') {
    try {
      data = data.call(context); // 支持 Vue.prototype 上挂的数据
    } catch (e) {
      if (Object({"NODE_ENV":"development","VUE_APP_DARK_MODE":"false","VUE_APP_NAME":"e考监控","VUE_APP_PLATFORM":"mp-weixin","BASE_URL":"/"}).VUE_APP_DEBUG) {
        console.warn('根据 Vue 的 data 函数初始化小程序 data 失败，请尽量确保 data 函数中不访问 vm 对象，否则可能影响首次数据渲染速度。', data);
      }
    }
  } else {
    try {
      // 对 data 格式化
      data = JSON.parse(JSON.stringify(data));
    } catch (e) {}
  }
  if (!isPlainObject(data)) {
    data = {};
  }
  Object.keys(methods).forEach(function (methodName) {
    if (context.__lifecycle_hooks__.indexOf(methodName) === -1 && !hasOwn(data, methodName)) {
      data[methodName] = methods[methodName];
    }
  });
  return data;
}
var PROP_TYPES = [String, Number, Boolean, Object, Array, null];
function createObserver(name) {
  return function observer(newVal, oldVal) {
    if (this.$vm) {
      this.$vm[name] = newVal; // 为了触发其他非 render watcher
    }
  };
}

function initBehaviors(vueOptions, initBehavior) {
  var vueBehaviors = vueOptions.behaviors;
  var vueExtends = vueOptions.extends;
  var vueMixins = vueOptions.mixins;
  var vueProps = vueOptions.props;
  if (!vueProps) {
    vueOptions.props = vueProps = [];
  }
  var behaviors = [];
  if (Array.isArray(vueBehaviors)) {
    vueBehaviors.forEach(function (behavior) {
      behaviors.push(behavior.replace('uni://', "wx".concat("://")));
      if (behavior === 'uni://form-field') {
        if (Array.isArray(vueProps)) {
          vueProps.push('name');
          vueProps.push('value');
        } else {
          vueProps.name = {
            type: String,
            default: ''
          };
          vueProps.value = {
            type: [String, Number, Boolean, Array, Object, Date],
            default: ''
          };
        }
      }
    });
  }
  if (isPlainObject(vueExtends) && vueExtends.props) {
    behaviors.push(initBehavior({
      properties: initProperties(vueExtends.props, true)
    }));
  }
  if (Array.isArray(vueMixins)) {
    vueMixins.forEach(function (vueMixin) {
      if (isPlainObject(vueMixin) && vueMixin.props) {
        behaviors.push(initBehavior({
          properties: initProperties(vueMixin.props, true)
        }));
      }
    });
  }
  return behaviors;
}
function parsePropType(key, type, defaultValue, file) {
  // [String]=>String
  if (Array.isArray(type) && type.length === 1) {
    return type[0];
  }
  return type;
}
function initProperties(props) {
  var isBehavior = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
  var file = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';
  var options = arguments.length > 3 ? arguments[3] : undefined;
  var properties = {};
  if (!isBehavior) {
    properties.vueId = {
      type: String,
      value: ''
    };
    {
      if (options.virtualHost) {
        properties.virtualHostStyle = {
          type: null,
          value: ''
        };
        properties.virtualHostClass = {
          type: null,
          value: ''
        };
      }
    }
    // scopedSlotsCompiler auto
    properties.scopedSlotsCompiler = {
      type: String,
      value: ''
    };
    properties.vueSlots = {
      // 小程序不能直接定义 $slots 的 props，所以通过 vueSlots 转换到 $slots
      type: null,
      value: [],
      observer: function observer(newVal, oldVal) {
        var $slots = Object.create(null);
        newVal.forEach(function (slotName) {
          $slots[slotName] = true;
        });
        this.setData({
          $slots: $slots
        });
      }
    };
  }
  if (Array.isArray(props)) {
    // ['title']
    props.forEach(function (key) {
      properties[key] = {
        type: null,
        observer: createObserver(key)
      };
    });
  } else if (isPlainObject(props)) {
    // {title:{type:String,default:''},content:String}
    Object.keys(props).forEach(function (key) {
      var opts = props[key];
      if (isPlainObject(opts)) {
        // title:{type:String,default:''}
        var value = opts.default;
        if (isFn(value)) {
          value = value();
        }
        opts.type = parsePropType(key, opts.type);
        properties[key] = {
          type: PROP_TYPES.indexOf(opts.type) !== -1 ? opts.type : null,
          value: value,
          observer: createObserver(key)
        };
      } else {
        // content:String
        var type = parsePropType(key, opts);
        properties[key] = {
          type: PROP_TYPES.indexOf(type) !== -1 ? type : null,
          observer: createObserver(key)
        };
      }
    });
  }
  return properties;
}
function wrapper$1(event) {
  // TODO 又得兼容 mpvue 的 mp 对象
  try {
    event.mp = JSON.parse(JSON.stringify(event));
  } catch (e) {}
  event.stopPropagation = noop;
  event.preventDefault = noop;
  event.target = event.target || {};
  if (!hasOwn(event, 'detail')) {
    event.detail = {};
  }
  if (hasOwn(event, 'markerId')) {
    event.detail = (0, _typeof2.default)(event.detail) === 'object' ? event.detail : {};
    event.detail.markerId = event.markerId;
  }
  if (isPlainObject(event.detail)) {
    event.target = Object.assign({}, event.target, event.detail);
  }
  return event;
}
function getExtraValue(vm, dataPathsArray) {
  var context = vm;
  dataPathsArray.forEach(function (dataPathArray) {
    var dataPath = dataPathArray[0];
    var value = dataPathArray[2];
    if (dataPath || typeof value !== 'undefined') {
      // ['','',index,'disable']
      var propPath = dataPathArray[1];
      var valuePath = dataPathArray[3];
      var vFor;
      if (Number.isInteger(dataPath)) {
        vFor = dataPath;
      } else if (!dataPath) {
        vFor = context;
      } else if (typeof dataPath === 'string' && dataPath) {
        if (dataPath.indexOf('#s#') === 0) {
          vFor = dataPath.substr(3);
        } else {
          vFor = vm.__get_value(dataPath, context);
        }
      }
      if (Number.isInteger(vFor)) {
        context = value;
      } else if (!propPath) {
        context = vFor[value];
      } else {
        if (Array.isArray(vFor)) {
          context = vFor.find(function (vForItem) {
            return vm.__get_value(propPath, vForItem) === value;
          });
        } else if (isPlainObject(vFor)) {
          context = Object.keys(vFor).find(function (vForKey) {
            return vm.__get_value(propPath, vFor[vForKey]) === value;
          });
        } else {
          console.error('v-for 暂不支持循环数据：', vFor);
        }
      }
      if (valuePath) {
        context = vm.__get_value(valuePath, context);
      }
    }
  });
  return context;
}
function processEventExtra(vm, extra, event, __args__) {
  var extraObj = {};
  if (Array.isArray(extra) && extra.length) {
    /**
     *[
     *    ['data.items', 'data.id', item.data.id],
     *    ['metas', 'id', meta.id]
     *],
     *[
     *    ['data.items', 'data.id', item.data.id],
     *    ['metas', 'id', meta.id]
     *],
     *'test'
     */
    extra.forEach(function (dataPath, index) {
      if (typeof dataPath === 'string') {
        if (!dataPath) {
          // model,prop.sync
          extraObj['$' + index] = vm;
        } else {
          if (dataPath === '$event') {
            // $event
            extraObj['$' + index] = event;
          } else if (dataPath === 'arguments') {
            extraObj['$' + index] = event.detail ? event.detail.__args__ || __args__ : __args__;
          } else if (dataPath.indexOf('$event.') === 0) {
            // $event.target.value
            extraObj['$' + index] = vm.__get_value(dataPath.replace('$event.', ''), event);
          } else {
            extraObj['$' + index] = vm.__get_value(dataPath);
          }
        }
      } else {
        extraObj['$' + index] = getExtraValue(vm, dataPath);
      }
    });
  }
  return extraObj;
}
function getObjByArray(arr) {
  var obj = {};
  for (var i = 1; i < arr.length; i++) {
    var element = arr[i];
    obj[element[0]] = element[1];
  }
  return obj;
}
function processEventArgs(vm, event) {
  var args = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];
  var extra = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : [];
  var isCustom = arguments.length > 4 ? arguments[4] : undefined;
  var methodName = arguments.length > 5 ? arguments[5] : undefined;
  var isCustomMPEvent = false; // wxcomponent 组件，传递原始 event 对象

  // fixed 用户直接触发 mpInstance.triggerEvent
  var __args__ = isPlainObject(event.detail) ? event.detail.__args__ || [event.detail] : [event.detail];
  if (isCustom) {
    // 自定义事件
    isCustomMPEvent = event.currentTarget && event.currentTarget.dataset && event.currentTarget.dataset.comType === 'wx';
    if (!args.length) {
      // 无参数，直接传入 event 或 detail 数组
      if (isCustomMPEvent) {
        return [event];
      }
      return __args__;
    }
  }
  var extraObj = processEventExtra(vm, extra, event, __args__);
  var ret = [];
  args.forEach(function (arg) {
    if (arg === '$event') {
      if (methodName === '__set_model' && !isCustom) {
        // input v-model value
        ret.push(event.target.value);
      } else {
        if (isCustom && !isCustomMPEvent) {
          ret.push(__args__[0]);
        } else {
          // wxcomponent 组件或内置组件
          ret.push(event);
        }
      }
    } else {
      if (Array.isArray(arg) && arg[0] === 'o') {
        ret.push(getObjByArray(arg));
      } else if (typeof arg === 'string' && hasOwn(extraObj, arg)) {
        ret.push(extraObj[arg]);
      } else {
        ret.push(arg);
      }
    }
  });
  return ret;
}
var ONCE = '~';
var CUSTOM = '^';
function isMatchEventType(eventType, optType) {
  return eventType === optType || optType === 'regionchange' && (eventType === 'begin' || eventType === 'end');
}
function getContextVm(vm) {
  var $parent = vm.$parent;
  // 父组件是 scoped slots 或者其他自定义组件时继续查找
  while ($parent && $parent.$parent && ($parent.$options.generic || $parent.$parent.$options.generic || $parent.$scope._$vuePid)) {
    $parent = $parent.$parent;
  }
  return $parent && $parent.$parent;
}
function handleEvent(event) {
  var _this2 = this;
  event = wrapper$1(event);

  // [['tap',[['handle',[1,2,a]],['handle1',[1,2,a]]]]]
  var dataset = (event.currentTarget || event.target).dataset;
  if (!dataset) {
    return console.warn('事件信息不存在');
  }
  var eventOpts = dataset.eventOpts || dataset['event-opts']; // 支付宝 web-view 组件 dataset 非驼峰
  if (!eventOpts) {
    return console.warn('事件信息不存在');
  }

  // [['handle',[1,2,a]],['handle1',[1,2,a]]]
  var eventType = event.type;
  var ret = [];
  eventOpts.forEach(function (eventOpt) {
    var type = eventOpt[0];
    var eventsArray = eventOpt[1];
    var isCustom = type.charAt(0) === CUSTOM;
    type = isCustom ? type.slice(1) : type;
    var isOnce = type.charAt(0) === ONCE;
    type = isOnce ? type.slice(1) : type;
    if (eventsArray && isMatchEventType(eventType, type)) {
      eventsArray.forEach(function (eventArray) {
        var methodName = eventArray[0];
        if (methodName) {
          var handlerCtx = _this2.$vm;
          if (handlerCtx.$options.generic) {
            // mp-weixin,mp-toutiao 抽象节点模拟 scoped slots
            handlerCtx = getContextVm(handlerCtx) || handlerCtx;
          }
          if (methodName === '$emit') {
            handlerCtx.$emit.apply(handlerCtx, processEventArgs(_this2.$vm, event, eventArray[1], eventArray[2], isCustom, methodName));
            return;
          }
          var handler = handlerCtx[methodName];
          if (!isFn(handler)) {
            var _type = _this2.$vm.mpType === 'page' ? 'Page' : 'Component';
            var path = _this2.route || _this2.is;
            throw new Error("".concat(_type, " \"").concat(path, "\" does not have a method \"").concat(methodName, "\""));
          }
          if (isOnce) {
            if (handler.once) {
              return;
            }
            handler.once = true;
          }
          var params = processEventArgs(_this2.$vm, event, eventArray[1], eventArray[2], isCustom, methodName);
          params = Array.isArray(params) ? params : [];
          // 参数尾部增加原始事件对象用于复杂表达式内获取额外数据
          if (/=\s*\S+\.eventParams\s*\|\|\s*\S+\[['"]event-params['"]\]/.test(handler.toString())) {
            // eslint-disable-next-line no-sparse-arrays
            params = params.concat([,,,,,,,,,, event]);
          }
          ret.push(handler.apply(handlerCtx, params));
        }
      });
    }
  });
  if (eventType === 'input' && ret.length === 1 && typeof ret[0] !== 'undefined') {
    return ret[0];
  }
}
var eventChannels = {};
var eventChannelStack = [];
function getEventChannel(id) {
  if (id) {
    var eventChannel = eventChannels[id];
    delete eventChannels[id];
    return eventChannel;
  }
  return eventChannelStack.shift();
}
var hooks = ['onShow', 'onHide', 'onError', 'onPageNotFound', 'onThemeChange', 'onUnhandledRejection'];
function initEventChannel() {
  _vue.default.prototype.getOpenerEventChannel = function () {
    // 微信小程序使用自身getOpenerEventChannel
    {
      return this.$scope.getOpenerEventChannel();
    }
  };
  var callHook = _vue.default.prototype.__call_hook;
  _vue.default.prototype.__call_hook = function (hook, args) {
    if (hook === 'onLoad' && args && args.__id__) {
      this.__eventChannel__ = getEventChannel(args.__id__);
      delete args.__id__;
    }
    return callHook.call(this, hook, args);
  };
}
function initScopedSlotsParams() {
  var center = {};
  var parents = {};
  function currentId(fn) {
    var vueIds = this.$options.propsData.vueId;
    if (vueIds) {
      var vueId = vueIds.split(',')[0];
      fn(vueId);
    }
  }
  _vue.default.prototype.$hasSSP = function (vueId) {
    var slot = center[vueId];
    if (!slot) {
      parents[vueId] = this;
      this.$on('hook:destroyed', function () {
        delete parents[vueId];
      });
    }
    return slot;
  };
  _vue.default.prototype.$getSSP = function (vueId, name, needAll) {
    var slot = center[vueId];
    if (slot) {
      var params = slot[name] || [];
      if (needAll) {
        return params;
      }
      return params[0];
    }
  };
  _vue.default.prototype.$setSSP = function (name, value) {
    var index = 0;
    currentId.call(this, function (vueId) {
      var slot = center[vueId];
      var params = slot[name] = slot[name] || [];
      params.push(value);
      index = params.length - 1;
    });
    return index;
  };
  _vue.default.prototype.$initSSP = function () {
    currentId.call(this, function (vueId) {
      center[vueId] = {};
    });
  };
  _vue.default.prototype.$callSSP = function () {
    currentId.call(this, function (vueId) {
      if (parents[vueId]) {
        parents[vueId].$forceUpdate();
      }
    });
  };
  _vue.default.mixin({
    destroyed: function destroyed() {
      var propsData = this.$options.propsData;
      var vueId = propsData && propsData.vueId;
      if (vueId) {
        delete center[vueId];
        delete parents[vueId];
      }
    }
  });
}
function parseBaseApp(vm, _ref4) {
  var mocks = _ref4.mocks,
    initRefs = _ref4.initRefs;
  initEventChannel();
  {
    initScopedSlotsParams();
  }
  if (vm.$options.store) {
    _vue.default.prototype.$store = vm.$options.store;
  }
  uniIdMixin(_vue.default);
  _vue.default.prototype.mpHost = "mp-weixin";
  _vue.default.mixin({
    beforeCreate: function beforeCreate() {
      if (!this.$options.mpType) {
        return;
      }
      this.mpType = this.$options.mpType;
      this.$mp = (0, _defineProperty2.default)({
        data: {}
      }, this.mpType, this.$options.mpInstance);
      this.$scope = this.$options.mpInstance;
      delete this.$options.mpType;
      delete this.$options.mpInstance;
      if (this.mpType === 'page' && typeof getApp === 'function') {
        // hack vue-i18n
        var app = getApp();
        if (app.$vm && app.$vm.$i18n) {
          this._i18n = app.$vm.$i18n;
        }
      }
      if (this.mpType !== 'app') {
        initRefs(this);
        initMocks(this, mocks);
      }
    }
  });
  var appOptions = {
    onLaunch: function onLaunch(args) {
      if (this.$vm) {
        // 已经初始化过了，主要是为了百度，百度 onShow 在 onLaunch 之前
        return;
      }
      {
        if (wx.canIUse && !wx.canIUse('nextTick')) {
          // 事实 上2.2.3 即可，简单使用 2.3.0 的 nextTick 判断
          console.error('当前微信基础库版本过低，请将 微信开发者工具-详情-项目设置-调试基础库版本 更换为`2.3.0`以上');
        }
      }
      this.$vm = vm;
      this.$vm.$mp = {
        app: this
      };
      this.$vm.$scope = this;
      // vm 上也挂载 globalData
      this.$vm.globalData = this.globalData;
      this.$vm._isMounted = true;
      this.$vm.__call_hook('mounted', args);
      this.$vm.__call_hook('onLaunch', args);
    }
  };

  // 兼容旧版本 globalData
  appOptions.globalData = vm.$options.globalData || {};
  // 将 methods 中的方法挂在 getApp() 中
  var methods = vm.$options.methods;
  if (methods) {
    Object.keys(methods).forEach(function (name) {
      appOptions[name] = methods[name];
    });
  }
  initAppLocale(_vue.default, vm, normalizeLocale(wx.getSystemInfoSync().language) || LOCALE_EN);
  initHooks(appOptions, hooks);
  initUnknownHooks(appOptions, vm.$options);
  return appOptions;
}
function parseApp(vm) {
  return parseBaseApp(vm, {
    mocks: mocks,
    initRefs: initRefs
  });
}
function createApp(vm) {
  App(parseApp(vm));
  return vm;
}
var encodeReserveRE = /[!'()*]/g;
var encodeReserveReplacer = function encodeReserveReplacer(c) {
  return '%' + c.charCodeAt(0).toString(16);
};
var commaRE = /%2C/g;

// fixed encodeURIComponent which is more conformant to RFC3986:
// - escapes [!'()*]
// - preserve commas
var encode = function encode(str) {
  return encodeURIComponent(str).replace(encodeReserveRE, encodeReserveReplacer).replace(commaRE, ',');
};
function stringifyQuery(obj) {
  var encodeStr = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : encode;
  var res = obj ? Object.keys(obj).map(function (key) {
    var val = obj[key];
    if (val === undefined) {
      return '';
    }
    if (val === null) {
      return encodeStr(key);
    }
    if (Array.isArray(val)) {
      var result = [];
      val.forEach(function (val2) {
        if (val2 === undefined) {
          return;
        }
        if (val2 === null) {
          result.push(encodeStr(key));
        } else {
          result.push(encodeStr(key) + '=' + encodeStr(val2));
        }
      });
      return result.join('&');
    }
    return encodeStr(key) + '=' + encodeStr(val);
  }).filter(function (x) {
    return x.length > 0;
  }).join('&') : null;
  return res ? "?".concat(res) : '';
}
function parseBaseComponent(vueComponentOptions) {
  var _ref5 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
    isPage = _ref5.isPage,
    initRelation = _ref5.initRelation;
  var needVueOptions = arguments.length > 2 ? arguments[2] : undefined;
  var _initVueComponent = initVueComponent(_vue.default, vueComponentOptions),
    _initVueComponent2 = (0, _slicedToArray2.default)(_initVueComponent, 2),
    VueComponent = _initVueComponent2[0],
    vueOptions = _initVueComponent2[1];
  var options = _objectSpread({
    multipleSlots: true,
    // styleIsolation: 'apply-shared',
    addGlobalClass: true
  }, vueOptions.options || {});
  {
    // 微信 multipleSlots 部分情况有 bug，导致内容顺序错乱 如 u-list，提供覆盖选项
    if (vueOptions['mp-weixin'] && vueOptions['mp-weixin'].options) {
      Object.assign(options, vueOptions['mp-weixin'].options);
    }
  }
  var componentOptions = {
    options: options,
    data: initData(vueOptions, _vue.default.prototype),
    behaviors: initBehaviors(vueOptions, initBehavior),
    properties: initProperties(vueOptions.props, false, vueOptions.__file, options),
    lifetimes: {
      attached: function attached() {
        var properties = this.properties;
        var options = {
          mpType: isPage.call(this) ? 'page' : 'component',
          mpInstance: this,
          propsData: properties
        };
        initVueIds(properties.vueId, this);

        // 处理父子关系
        initRelation.call(this, {
          vuePid: this._$vuePid,
          vueOptions: options
        });

        // 初始化 vue 实例
        this.$vm = new VueComponent(options);

        // 处理$slots,$scopedSlots（暂不支持动态变化$slots）
        initSlots(this.$vm, properties.vueSlots);

        // 触发首次 setData
        this.$vm.$mount();
      },
      ready: function ready() {
        // 当组件 props 默认值为 true，初始化时传入 false 会导致 created,ready 触发, 但 attached 不触发
        // https://developers.weixin.qq.com/community/develop/doc/00066ae2844cc0f8eb883e2a557800
        if (this.$vm) {
          this.$vm._isMounted = true;
          this.$vm.__call_hook('mounted');
          this.$vm.__call_hook('onReady');
        }
      },
      detached: function detached() {
        this.$vm && this.$vm.$destroy();
      }
    },
    pageLifetimes: {
      show: function show(args) {
        this.$vm && this.$vm.__call_hook('onPageShow', args);
      },
      hide: function hide() {
        this.$vm && this.$vm.__call_hook('onPageHide');
      },
      resize: function resize(size) {
        this.$vm && this.$vm.__call_hook('onPageResize', size);
      }
    },
    methods: {
      __l: handleLink,
      __e: handleEvent
    }
  };
  // externalClasses
  if (vueOptions.externalClasses) {
    componentOptions.externalClasses = vueOptions.externalClasses;
  }
  if (Array.isArray(vueOptions.wxsCallMethods)) {
    vueOptions.wxsCallMethods.forEach(function (callMethod) {
      componentOptions.methods[callMethod] = function (args) {
        return this.$vm[callMethod](args);
      };
    });
  }
  if (needVueOptions) {
    return [componentOptions, vueOptions, VueComponent];
  }
  if (isPage) {
    return componentOptions;
  }
  return [componentOptions, VueComponent];
}
function parseComponent(vueComponentOptions, needVueOptions) {
  return parseBaseComponent(vueComponentOptions, {
    isPage: isPage,
    initRelation: initRelation
  }, needVueOptions);
}
var hooks$1 = ['onShow', 'onHide', 'onUnload'];
hooks$1.push.apply(hooks$1, PAGE_EVENT_HOOKS);
function parseBasePage(vuePageOptions) {
  var _parseComponent = parseComponent(vuePageOptions, true),
    _parseComponent2 = (0, _slicedToArray2.default)(_parseComponent, 2),
    pageOptions = _parseComponent2[0],
    vueOptions = _parseComponent2[1];
  initHooks(pageOptions.methods, hooks$1, vueOptions);
  pageOptions.methods.onLoad = function (query) {
    this.options = query;
    var copyQuery = Object.assign({}, query);
    delete copyQuery.__id__;
    this.$page = {
      fullPath: '/' + (this.route || this.is) + stringifyQuery(copyQuery)
    };
    this.$vm.$mp.query = query; // 兼容 mpvue
    this.$vm.__call_hook('onLoad', query);
  };
  {
    initUnknownHooks(pageOptions.methods, vuePageOptions, ['onReady']);
  }
  {
    initWorkletMethods(pageOptions.methods, vueOptions.methods);
  }
  return pageOptions;
}
function parsePage(vuePageOptions) {
  return parseBasePage(vuePageOptions);
}
function createPage(vuePageOptions) {
  {
    return Component(parsePage(vuePageOptions));
  }
}
function createComponent(vueOptions) {
  {
    return Component(parseComponent(vueOptions));
  }
}
function createSubpackageApp(vm) {
  var appOptions = parseApp(vm);
  var app = getApp({
    allowDefault: true
  });
  vm.$scope = app;
  var globalData = app.globalData;
  if (globalData) {
    Object.keys(appOptions.globalData).forEach(function (name) {
      if (!hasOwn(globalData, name)) {
        globalData[name] = appOptions.globalData[name];
      }
    });
  }
  Object.keys(appOptions).forEach(function (name) {
    if (!hasOwn(app, name)) {
      app[name] = appOptions[name];
    }
  });
  if (isFn(appOptions.onShow) && wx.onAppShow) {
    wx.onAppShow(function () {
      for (var _len5 = arguments.length, args = new Array(_len5), _key5 = 0; _key5 < _len5; _key5++) {
        args[_key5] = arguments[_key5];
      }
      vm.__call_hook('onShow', args);
    });
  }
  if (isFn(appOptions.onHide) && wx.onAppHide) {
    wx.onAppHide(function () {
      for (var _len6 = arguments.length, args = new Array(_len6), _key6 = 0; _key6 < _len6; _key6++) {
        args[_key6] = arguments[_key6];
      }
      vm.__call_hook('onHide', args);
    });
  }
  if (isFn(appOptions.onLaunch)) {
    var args = wx.getLaunchOptionsSync && wx.getLaunchOptionsSync();
    vm.__call_hook('onLaunch', args);
  }
  return vm;
}
function createPlugin(vm) {
  var appOptions = parseApp(vm);
  if (isFn(appOptions.onShow) && wx.onAppShow) {
    wx.onAppShow(function () {
      for (var _len7 = arguments.length, args = new Array(_len7), _key7 = 0; _key7 < _len7; _key7++) {
        args[_key7] = arguments[_key7];
      }
      vm.__call_hook('onShow', args);
    });
  }
  if (isFn(appOptions.onHide) && wx.onAppHide) {
    wx.onAppHide(function () {
      for (var _len8 = arguments.length, args = new Array(_len8), _key8 = 0; _key8 < _len8; _key8++) {
        args[_key8] = arguments[_key8];
      }
      vm.__call_hook('onHide', args);
    });
  }
  if (isFn(appOptions.onLaunch)) {
    var args = wx.getLaunchOptionsSync && wx.getLaunchOptionsSync();
    vm.__call_hook('onLaunch', args);
  }
  return vm;
}
todos.forEach(function (todoApi) {
  protocols[todoApi] = false;
});
canIUses.forEach(function (canIUseApi) {
  var apiName = protocols[canIUseApi] && protocols[canIUseApi].name ? protocols[canIUseApi].name : canIUseApi;
  if (!wx.canIUse(apiName)) {
    protocols[canIUseApi] = false;
  }
});
var uni = {};
if (typeof Proxy !== 'undefined' && "mp-weixin" !== 'app-plus') {
  uni = new Proxy({}, {
    get: function get(target, name) {
      if (hasOwn(target, name)) {
        return target[name];
      }
      if (baseApi[name]) {
        return baseApi[name];
      }
      if (api[name]) {
        return promisify(name, api[name]);
      }
      {
        if (extraApi[name]) {
          return promisify(name, extraApi[name]);
        }
        if (todoApis[name]) {
          return promisify(name, todoApis[name]);
        }
      }
      if (eventApi[name]) {
        return eventApi[name];
      }
      return promisify(name, wrapper(name, wx[name]));
    },
    set: function set(target, name, value) {
      target[name] = value;
      return true;
    }
  });
} else {
  Object.keys(baseApi).forEach(function (name) {
    uni[name] = baseApi[name];
  });
  {
    Object.keys(todoApis).forEach(function (name) {
      uni[name] = promisify(name, todoApis[name]);
    });
    Object.keys(extraApi).forEach(function (name) {
      uni[name] = promisify(name, todoApis[name]);
    });
  }
  Object.keys(eventApi).forEach(function (name) {
    uni[name] = eventApi[name];
  });
  Object.keys(api).forEach(function (name) {
    uni[name] = promisify(name, api[name]);
  });
  Object.keys(wx).forEach(function (name) {
    if (hasOwn(wx, name) || hasOwn(protocols, name)) {
      uni[name] = promisify(name, wrapper(name, wx[name]));
    }
  });
}
wx.createApp = createApp;
wx.createPage = createPage;
wx.createComponent = createComponent;
wx.createSubpackageApp = createSubpackageApp;
wx.createPlugin = createPlugin;
var uni$1 = uni;
var _default = uni$1;
exports.default = _default;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/wx.js */ 1)["default"], __webpack_require__(/*! ./../../../webpack/buildin/global.js */ 3)))

/***/ }),
/* 3 */
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 4 */
/*!**********************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/interopRequireDefault.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}
module.exports = _interopRequireDefault, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 5 */
/*!**************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/slicedToArray.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var arrayWithHoles = __webpack_require__(/*! ./arrayWithHoles.js */ 6);
var iterableToArrayLimit = __webpack_require__(/*! ./iterableToArrayLimit.js */ 7);
var unsupportedIterableToArray = __webpack_require__(/*! ./unsupportedIterableToArray.js */ 8);
var nonIterableRest = __webpack_require__(/*! ./nonIterableRest.js */ 10);
function _slicedToArray(arr, i) {
  return arrayWithHoles(arr) || iterableToArrayLimit(arr, i) || unsupportedIterableToArray(arr, i) || nonIterableRest();
}
module.exports = _slicedToArray, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 6 */
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/arrayWithHoles.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
}
module.exports = _arrayWithHoles, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 7 */
/*!*********************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/iterableToArrayLimit.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _iterableToArrayLimit(arr, i) {
  var _i = null == arr ? null : "undefined" != typeof Symbol && arr[Symbol.iterator] || arr["@@iterator"];
  if (null != _i) {
    var _s,
      _e,
      _x,
      _r,
      _arr = [],
      _n = !0,
      _d = !1;
    try {
      if (_x = (_i = _i.call(arr)).next, 0 === i) {
        if (Object(_i) !== _i) return;
        _n = !1;
      } else for (; !(_n = (_s = _x.call(_i)).done) && (_arr.push(_s.value), _arr.length !== i); _n = !0) {
        ;
      }
    } catch (err) {
      _d = !0, _e = err;
    } finally {
      try {
        if (!_n && null != _i["return"] && (_r = _i["return"](), Object(_r) !== _r)) return;
      } finally {
        if (_d) throw _e;
      }
    }
    return _arr;
  }
}
module.exports = _iterableToArrayLimit, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 8 */
/*!***************************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/unsupportedIterableToArray.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var arrayLikeToArray = __webpack_require__(/*! ./arrayLikeToArray.js */ 9);
function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return arrayLikeToArray(o, minLen);
}
module.exports = _unsupportedIterableToArray, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 9 */
/*!*****************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/arrayLikeToArray.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;
  for (var i = 0, arr2 = new Array(len); i < len; i++) {
    arr2[i] = arr[i];
  }
  return arr2;
}
module.exports = _arrayLikeToArray, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 10 */
/*!****************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/nonIterableRest.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}
module.exports = _nonIterableRest, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 11 */
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/defineProperty.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var toPropertyKey = __webpack_require__(/*! ./toPropertyKey.js */ 12);
function _defineProperty(obj, key, value) {
  key = toPropertyKey(key);
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }
  return obj;
}
module.exports = _defineProperty, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 12 */
/*!**************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/toPropertyKey.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__(/*! ./typeof.js */ 13)["default"];
var toPrimitive = __webpack_require__(/*! ./toPrimitive.js */ 14);
function _toPropertyKey(arg) {
  var key = toPrimitive(arg, "string");
  return _typeof(key) === "symbol" ? key : String(key);
}
module.exports = _toPropertyKey, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 13 */
/*!*******************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/typeof.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _typeof(obj) {
  "@babel/helpers - typeof";

  return (module.exports = _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) {
    return typeof obj;
  } : function (obj) {
    return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
  }, module.exports.__esModule = true, module.exports["default"] = module.exports), _typeof(obj);
}
module.exports = _typeof, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 14 */
/*!************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/toPrimitive.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__(/*! ./typeof.js */ 13)["default"];
function _toPrimitive(input, hint) {
  if (_typeof(input) !== "object" || input === null) return input;
  var prim = input[Symbol.toPrimitive];
  if (prim !== undefined) {
    var res = prim.call(input, hint || "default");
    if (_typeof(res) !== "object") return res;
    throw new TypeError("@@toPrimitive must return a primitive value.");
  }
  return (hint === "string" ? String : Number)(input);
}
module.exports = _toPrimitive, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 15 */
/*!**********************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/construct.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var setPrototypeOf = __webpack_require__(/*! ./setPrototypeOf.js */ 16);
var isNativeReflectConstruct = __webpack_require__(/*! ./isNativeReflectConstruct.js */ 17);
function _construct(Parent, args, Class) {
  if (isNativeReflectConstruct()) {
    module.exports = _construct = Reflect.construct.bind(), module.exports.__esModule = true, module.exports["default"] = module.exports;
  } else {
    module.exports = _construct = function _construct(Parent, args, Class) {
      var a = [null];
      a.push.apply(a, args);
      var Constructor = Function.bind.apply(Parent, a);
      var instance = new Constructor();
      if (Class) setPrototypeOf(instance, Class.prototype);
      return instance;
    }, module.exports.__esModule = true, module.exports["default"] = module.exports;
  }
  return _construct.apply(null, arguments);
}
module.exports = _construct, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 16 */
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/setPrototypeOf.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _setPrototypeOf(o, p) {
  module.exports = _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  }, module.exports.__esModule = true, module.exports["default"] = module.exports;
  return _setPrototypeOf(o, p);
}
module.exports = _setPrototypeOf, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 17 */
/*!*************************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/isNativeReflectConstruct.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _isNativeReflectConstruct() {
  if (typeof Reflect === "undefined" || !Reflect.construct) return false;
  if (Reflect.construct.sham) return false;
  if (typeof Proxy === "function") return true;
  try {
    Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
    return true;
  } catch (e) {
    return false;
  }
}
module.exports = _isNativeReflectConstruct, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 18 */
/*!******************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/toConsumableArray.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var arrayWithoutHoles = __webpack_require__(/*! ./arrayWithoutHoles.js */ 19);
var iterableToArray = __webpack_require__(/*! ./iterableToArray.js */ 20);
var unsupportedIterableToArray = __webpack_require__(/*! ./unsupportedIterableToArray.js */ 8);
var nonIterableSpread = __webpack_require__(/*! ./nonIterableSpread.js */ 21);
function _toConsumableArray(arr) {
  return arrayWithoutHoles(arr) || iterableToArray(arr) || unsupportedIterableToArray(arr) || nonIterableSpread();
}
module.exports = _toConsumableArray, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 19 */
/*!******************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/arrayWithoutHoles.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var arrayLikeToArray = __webpack_require__(/*! ./arrayLikeToArray.js */ 9);
function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) return arrayLikeToArray(arr);
}
module.exports = _arrayWithoutHoles, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 20 */
/*!****************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/iterableToArray.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _iterableToArray(iter) {
  if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter);
}
module.exports = _iterableToArray, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 21 */
/*!******************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/nonIterableSpread.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}
module.exports = _nonIterableSpread, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 22 */
/*!*************************************************************!*\
  !*** ./node_modules/@dcloudio/uni-i18n/dist/uni-i18n.es.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(uni, global) {

var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ 4);
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LOCALE_ZH_HANT = exports.LOCALE_ZH_HANS = exports.LOCALE_FR = exports.LOCALE_ES = exports.LOCALE_EN = exports.I18n = exports.Formatter = void 0;
exports.compileI18nJsonStr = compileI18nJsonStr;
exports.hasI18nJson = hasI18nJson;
exports.initVueI18n = initVueI18n;
exports.isI18nStr = isI18nStr;
exports.isString = void 0;
exports.normalizeLocale = normalizeLocale;
exports.parseI18nJson = parseI18nJson;
exports.resolveLocale = resolveLocale;
var _slicedToArray2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/slicedToArray */ 5));
var _classCallCheck2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ 23));
var _createClass2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/createClass */ 24));
var _typeof2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/typeof */ 13));
var isObject = function isObject(val) {
  return val !== null && (0, _typeof2.default)(val) === 'object';
};
var defaultDelimiters = ['{', '}'];
var BaseFormatter = /*#__PURE__*/function () {
  function BaseFormatter() {
    (0, _classCallCheck2.default)(this, BaseFormatter);
    this._caches = Object.create(null);
  }
  (0, _createClass2.default)(BaseFormatter, [{
    key: "interpolate",
    value: function interpolate(message, values) {
      var delimiters = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : defaultDelimiters;
      if (!values) {
        return [message];
      }
      var tokens = this._caches[message];
      if (!tokens) {
        tokens = parse(message, delimiters);
        this._caches[message] = tokens;
      }
      return compile(tokens, values);
    }
  }]);
  return BaseFormatter;
}();
exports.Formatter = BaseFormatter;
var RE_TOKEN_LIST_VALUE = /^(?:\d)+/;
var RE_TOKEN_NAMED_VALUE = /^(?:\w)+/;
function parse(format, _ref) {
  var _ref2 = (0, _slicedToArray2.default)(_ref, 2),
    startDelimiter = _ref2[0],
    endDelimiter = _ref2[1];
  var tokens = [];
  var position = 0;
  var text = '';
  while (position < format.length) {
    var char = format[position++];
    if (char === startDelimiter) {
      if (text) {
        tokens.push({
          type: 'text',
          value: text
        });
      }
      text = '';
      var sub = '';
      char = format[position++];
      while (char !== undefined && char !== endDelimiter) {
        sub += char;
        char = format[position++];
      }
      var isClosed = char === endDelimiter;
      var type = RE_TOKEN_LIST_VALUE.test(sub) ? 'list' : isClosed && RE_TOKEN_NAMED_VALUE.test(sub) ? 'named' : 'unknown';
      tokens.push({
        value: sub,
        type: type
      });
    }
    //  else if (char === '%') {
    //   // when found rails i18n syntax, skip text capture
    //   if (format[position] !== '{') {
    //     text += char
    //   }
    // }
    else {
      text += char;
    }
  }
  text && tokens.push({
    type: 'text',
    value: text
  });
  return tokens;
}
function compile(tokens, values) {
  var compiled = [];
  var index = 0;
  var mode = Array.isArray(values) ? 'list' : isObject(values) ? 'named' : 'unknown';
  if (mode === 'unknown') {
    return compiled;
  }
  while (index < tokens.length) {
    var token = tokens[index];
    switch (token.type) {
      case 'text':
        compiled.push(token.value);
        break;
      case 'list':
        compiled.push(values[parseInt(token.value, 10)]);
        break;
      case 'named':
        if (mode === 'named') {
          compiled.push(values[token.value]);
        } else {
          if (true) {
            console.warn("Type of token '".concat(token.type, "' and format of value '").concat(mode, "' don't match!"));
          }
        }
        break;
      case 'unknown':
        if (true) {
          console.warn("Detect 'unknown' type of token!");
        }
        break;
    }
    index++;
  }
  return compiled;
}
var LOCALE_ZH_HANS = 'zh-Hans';
exports.LOCALE_ZH_HANS = LOCALE_ZH_HANS;
var LOCALE_ZH_HANT = 'zh-Hant';
exports.LOCALE_ZH_HANT = LOCALE_ZH_HANT;
var LOCALE_EN = 'en';
exports.LOCALE_EN = LOCALE_EN;
var LOCALE_FR = 'fr';
exports.LOCALE_FR = LOCALE_FR;
var LOCALE_ES = 'es';
exports.LOCALE_ES = LOCALE_ES;
var hasOwnProperty = Object.prototype.hasOwnProperty;
var hasOwn = function hasOwn(val, key) {
  return hasOwnProperty.call(val, key);
};
var defaultFormatter = new BaseFormatter();
function include(str, parts) {
  return !!parts.find(function (part) {
    return str.indexOf(part) !== -1;
  });
}
function startsWith(str, parts) {
  return parts.find(function (part) {
    return str.indexOf(part) === 0;
  });
}
function normalizeLocale(locale, messages) {
  if (!locale) {
    return;
  }
  locale = locale.trim().replace(/_/g, '-');
  if (messages && messages[locale]) {
    return locale;
  }
  locale = locale.toLowerCase();
  if (locale === 'chinese') {
    // 支付宝
    return LOCALE_ZH_HANS;
  }
  if (locale.indexOf('zh') === 0) {
    if (locale.indexOf('-hans') > -1) {
      return LOCALE_ZH_HANS;
    }
    if (locale.indexOf('-hant') > -1) {
      return LOCALE_ZH_HANT;
    }
    if (include(locale, ['-tw', '-hk', '-mo', '-cht'])) {
      return LOCALE_ZH_HANT;
    }
    return LOCALE_ZH_HANS;
  }
  var locales = [LOCALE_EN, LOCALE_FR, LOCALE_ES];
  if (messages && Object.keys(messages).length > 0) {
    locales = Object.keys(messages);
  }
  var lang = startsWith(locale, locales);
  if (lang) {
    return lang;
  }
}
var I18n = /*#__PURE__*/function () {
  function I18n(_ref3) {
    var locale = _ref3.locale,
      fallbackLocale = _ref3.fallbackLocale,
      messages = _ref3.messages,
      watcher = _ref3.watcher,
      formater = _ref3.formater;
    (0, _classCallCheck2.default)(this, I18n);
    this.locale = LOCALE_EN;
    this.fallbackLocale = LOCALE_EN;
    this.message = {};
    this.messages = {};
    this.watchers = [];
    if (fallbackLocale) {
      this.fallbackLocale = fallbackLocale;
    }
    this.formater = formater || defaultFormatter;
    this.messages = messages || {};
    this.setLocale(locale || LOCALE_EN);
    if (watcher) {
      this.watchLocale(watcher);
    }
  }
  (0, _createClass2.default)(I18n, [{
    key: "setLocale",
    value: function setLocale(locale) {
      var _this = this;
      var oldLocale = this.locale;
      this.locale = normalizeLocale(locale, this.messages) || this.fallbackLocale;
      if (!this.messages[this.locale]) {
        // 可能初始化时不存在
        this.messages[this.locale] = {};
      }
      this.message = this.messages[this.locale];
      // 仅发生变化时，通知
      if (oldLocale !== this.locale) {
        this.watchers.forEach(function (watcher) {
          watcher(_this.locale, oldLocale);
        });
      }
    }
  }, {
    key: "getLocale",
    value: function getLocale() {
      return this.locale;
    }
  }, {
    key: "watchLocale",
    value: function watchLocale(fn) {
      var _this2 = this;
      var index = this.watchers.push(fn) - 1;
      return function () {
        _this2.watchers.splice(index, 1);
      };
    }
  }, {
    key: "add",
    value: function add(locale, message) {
      var override = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
      var curMessages = this.messages[locale];
      if (curMessages) {
        if (override) {
          Object.assign(curMessages, message);
        } else {
          Object.keys(message).forEach(function (key) {
            if (!hasOwn(curMessages, key)) {
              curMessages[key] = message[key];
            }
          });
        }
      } else {
        this.messages[locale] = message;
      }
    }
  }, {
    key: "f",
    value: function f(message, values, delimiters) {
      return this.formater.interpolate(message, values, delimiters).join('');
    }
  }, {
    key: "t",
    value: function t(key, locale, values) {
      var message = this.message;
      if (typeof locale === 'string') {
        locale = normalizeLocale(locale, this.messages);
        locale && (message = this.messages[locale]);
      } else {
        values = locale;
      }
      if (!hasOwn(message, key)) {
        console.warn("Cannot translate the value of keypath ".concat(key, ". Use the value of keypath as default."));
        return key;
      }
      return this.formater.interpolate(message[key], values).join('');
    }
  }]);
  return I18n;
}();
exports.I18n = I18n;
function watchAppLocale(appVm, i18n) {
  // 需要保证 watch 的触发在组件渲染之前
  if (appVm.$watchLocale) {
    // vue2
    appVm.$watchLocale(function (newLocale) {
      i18n.setLocale(newLocale);
    });
  } else {
    appVm.$watch(function () {
      return appVm.$locale;
    }, function (newLocale) {
      i18n.setLocale(newLocale);
    });
  }
}
function getDefaultLocale() {
  if (typeof uni !== 'undefined' && uni.getLocale) {
    return uni.getLocale();
  }
  // 小程序平台，uni 和 uni-i18n 互相引用，导致访问不到 uni，故在 global 上挂了 getLocale
  if (typeof global !== 'undefined' && global.getLocale) {
    return global.getLocale();
  }
  return LOCALE_EN;
}
function initVueI18n(locale) {
  var messages = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var fallbackLocale = arguments.length > 2 ? arguments[2] : undefined;
  var watcher = arguments.length > 3 ? arguments[3] : undefined;
  // 兼容旧版本入参
  if (typeof locale !== 'string') {
    var _ref4 = [messages, locale];
    locale = _ref4[0];
    messages = _ref4[1];
  }
  if (typeof locale !== 'string') {
    // 因为小程序平台，uni-i18n 和 uni 互相引用，导致此时访问 uni 时，为 undefined
    locale = getDefaultLocale();
  }
  if (typeof fallbackLocale !== 'string') {
    fallbackLocale = typeof __uniConfig !== 'undefined' && __uniConfig.fallbackLocale || LOCALE_EN;
  }
  var i18n = new I18n({
    locale: locale,
    fallbackLocale: fallbackLocale,
    messages: messages,
    watcher: watcher
  });
  var _t = function t(key, values) {
    if (typeof getApp !== 'function') {
      // app view
      /* eslint-disable no-func-assign */
      _t = function t(key, values) {
        return i18n.t(key, values);
      };
    } else {
      var isWatchedAppLocale = false;
      _t = function t(key, values) {
        var appVm = getApp().$vm;
        // 可能$vm还不存在，比如在支付宝小程序中，组件定义较早，在props的default里使用了t()函数（如uni-goods-nav），此时app还未初始化
        // options: {
        // 	type: Array,
        // 	default () {
        // 		return [{
        // 			icon: 'shop',
        // 			text: t("uni-goods-nav.options.shop"),
        // 		}, {
        // 			icon: 'cart',
        // 			text: t("uni-goods-nav.options.cart")
        // 		}]
        // 	}
        // },
        if (appVm) {
          // 触发响应式
          appVm.$locale;
          if (!isWatchedAppLocale) {
            isWatchedAppLocale = true;
            watchAppLocale(appVm, i18n);
          }
        }
        return i18n.t(key, values);
      };
    }
    return _t(key, values);
  };
  return {
    i18n: i18n,
    f: function f(message, values, delimiters) {
      return i18n.f(message, values, delimiters);
    },
    t: function t(key, values) {
      return _t(key, values);
    },
    add: function add(locale, message) {
      var override = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
      return i18n.add(locale, message, override);
    },
    watch: function watch(fn) {
      return i18n.watchLocale(fn);
    },
    getLocale: function getLocale() {
      return i18n.getLocale();
    },
    setLocale: function setLocale(newLocale) {
      return i18n.setLocale(newLocale);
    }
  };
}
var isString = function isString(val) {
  return typeof val === 'string';
};
exports.isString = isString;
var formater;
function hasI18nJson(jsonObj, delimiters) {
  if (!formater) {
    formater = new BaseFormatter();
  }
  return walkJsonObj(jsonObj, function (jsonObj, key) {
    var value = jsonObj[key];
    if (isString(value)) {
      if (isI18nStr(value, delimiters)) {
        return true;
      }
    } else {
      return hasI18nJson(value, delimiters);
    }
  });
}
function parseI18nJson(jsonObj, values, delimiters) {
  if (!formater) {
    formater = new BaseFormatter();
  }
  walkJsonObj(jsonObj, function (jsonObj, key) {
    var value = jsonObj[key];
    if (isString(value)) {
      if (isI18nStr(value, delimiters)) {
        jsonObj[key] = compileStr(value, values, delimiters);
      }
    } else {
      parseI18nJson(value, values, delimiters);
    }
  });
  return jsonObj;
}
function compileI18nJsonStr(jsonStr, _ref5) {
  var locale = _ref5.locale,
    locales = _ref5.locales,
    delimiters = _ref5.delimiters;
  if (!isI18nStr(jsonStr, delimiters)) {
    return jsonStr;
  }
  if (!formater) {
    formater = new BaseFormatter();
  }
  var localeValues = [];
  Object.keys(locales).forEach(function (name) {
    if (name !== locale) {
      localeValues.push({
        locale: name,
        values: locales[name]
      });
    }
  });
  localeValues.unshift({
    locale: locale,
    values: locales[locale]
  });
  try {
    return JSON.stringify(compileJsonObj(JSON.parse(jsonStr), localeValues, delimiters), null, 2);
  } catch (e) {}
  return jsonStr;
}
function isI18nStr(value, delimiters) {
  return value.indexOf(delimiters[0]) > -1;
}
function compileStr(value, values, delimiters) {
  return formater.interpolate(value, values, delimiters).join('');
}
function compileValue(jsonObj, key, localeValues, delimiters) {
  var value = jsonObj[key];
  if (isString(value)) {
    // 存在国际化
    if (isI18nStr(value, delimiters)) {
      jsonObj[key] = compileStr(value, localeValues[0].values, delimiters);
      if (localeValues.length > 1) {
        // 格式化国际化语言
        var valueLocales = jsonObj[key + 'Locales'] = {};
        localeValues.forEach(function (localValue) {
          valueLocales[localValue.locale] = compileStr(value, localValue.values, delimiters);
        });
      }
    }
  } else {
    compileJsonObj(value, localeValues, delimiters);
  }
}
function compileJsonObj(jsonObj, localeValues, delimiters) {
  walkJsonObj(jsonObj, function (jsonObj, key) {
    compileValue(jsonObj, key, localeValues, delimiters);
  });
  return jsonObj;
}
function walkJsonObj(jsonObj, walk) {
  if (Array.isArray(jsonObj)) {
    for (var i = 0; i < jsonObj.length; i++) {
      if (walk(jsonObj, i)) {
        return true;
      }
    }
  } else if (isObject(jsonObj)) {
    for (var key in jsonObj) {
      if (walk(jsonObj, key)) {
        return true;
      }
    }
  }
  return false;
}
function resolveLocale(locales) {
  return function (locale) {
    if (!locale) {
      return locale;
    }
    locale = normalizeLocale(locale) || locale;
    return resolveLocaleChain(locale).find(function (locale) {
      return locales.indexOf(locale) > -1;
    });
  };
}
function resolveLocaleChain(locale) {
  var chain = [];
  var tokens = locale.split('-');
  while (tokens.length) {
    chain.push(tokens.join('-'));
    tokens.pop();
  }
  return chain;
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js */ 2)["default"], __webpack_require__(/*! ./../../../webpack/buildin/global.js */ 3)))

/***/ }),
/* 23 */
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/classCallCheck.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}
module.exports = _classCallCheck, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 24 */
/*!************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/createClass.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var toPropertyKey = __webpack_require__(/*! ./toPropertyKey.js */ 12);
function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, toPropertyKey(descriptor.key), descriptor);
  }
}
function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  Object.defineProperty(Constructor, "prototype", {
    writable: false
  });
  return Constructor;
}
module.exports = _createClass, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 25 */
/*!******************************************************************************************!*\
  !*** ./node_modules/@dcloudio/vue-cli-plugin-uni/packages/mp-vue/dist/mp.runtime.esm.js ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/*!
 * Vue.js v2.6.11
 * (c) 2014-2023 Evan You
 * Released under the MIT License.
 */
/*  */

var emptyObject = Object.freeze({});

// These helpers produce better VM code in JS engines due to their
// explicitness and function inlining.
function isUndef (v) {
  return v === undefined || v === null
}

function isDef (v) {
  return v !== undefined && v !== null
}

function isTrue (v) {
  return v === true
}

function isFalse (v) {
  return v === false
}

/**
 * Check if value is primitive.
 */
function isPrimitive (value) {
  return (
    typeof value === 'string' ||
    typeof value === 'number' ||
    // $flow-disable-line
    typeof value === 'symbol' ||
    typeof value === 'boolean'
  )
}

/**
 * Quick object check - this is primarily used to tell
 * Objects from primitive values when we know the value
 * is a JSON-compliant type.
 */
function isObject (obj) {
  return obj !== null && typeof obj === 'object'
}

/**
 * Get the raw type string of a value, e.g., [object Object].
 */
var _toString = Object.prototype.toString;

function toRawType (value) {
  return _toString.call(value).slice(8, -1)
}

/**
 * Strict object type check. Only returns true
 * for plain JavaScript objects.
 */
function isPlainObject (obj) {
  return _toString.call(obj) === '[object Object]'
}

function isRegExp (v) {
  return _toString.call(v) === '[object RegExp]'
}

/**
 * Check if val is a valid array index.
 */
function isValidArrayIndex (val) {
  var n = parseFloat(String(val));
  return n >= 0 && Math.floor(n) === n && isFinite(val)
}

function isPromise (val) {
  return (
    isDef(val) &&
    typeof val.then === 'function' &&
    typeof val.catch === 'function'
  )
}

/**
 * Convert a value to a string that is actually rendered.
 */
function toString (val) {
  return val == null
    ? ''
    : Array.isArray(val) || (isPlainObject(val) && val.toString === _toString)
      ? JSON.stringify(val, null, 2)
      : String(val)
}

/**
 * Convert an input value to a number for persistence.
 * If the conversion fails, return original string.
 */
function toNumber (val) {
  var n = parseFloat(val);
  return isNaN(n) ? val : n
}

/**
 * Make a map and return a function for checking if a key
 * is in that map.
 */
function makeMap (
  str,
  expectsLowerCase
) {
  var map = Object.create(null);
  var list = str.split(',');
  for (var i = 0; i < list.length; i++) {
    map[list[i]] = true;
  }
  return expectsLowerCase
    ? function (val) { return map[val.toLowerCase()]; }
    : function (val) { return map[val]; }
}

/**
 * Check if a tag is a built-in tag.
 */
var isBuiltInTag = makeMap('slot,component', true);

/**
 * Check if an attribute is a reserved attribute.
 */
var isReservedAttribute = makeMap('key,ref,slot,slot-scope,is');

/**
 * Remove an item from an array.
 */
function remove (arr, item) {
  if (arr.length) {
    var index = arr.indexOf(item);
    if (index > -1) {
      return arr.splice(index, 1)
    }
  }
}

/**
 * Check whether an object has the property.
 */
var hasOwnProperty = Object.prototype.hasOwnProperty;
function hasOwn (obj, key) {
  return hasOwnProperty.call(obj, key)
}

/**
 * Create a cached version of a pure function.
 */
function cached (fn) {
  var cache = Object.create(null);
  return (function cachedFn (str) {
    var hit = cache[str];
    return hit || (cache[str] = fn(str))
  })
}

/**
 * Camelize a hyphen-delimited string.
 */
var camelizeRE = /-(\w)/g;
var camelize = cached(function (str) {
  return str.replace(camelizeRE, function (_, c) { return c ? c.toUpperCase() : ''; })
});

/**
 * Capitalize a string.
 */
var capitalize = cached(function (str) {
  return str.charAt(0).toUpperCase() + str.slice(1)
});

/**
 * Hyphenate a camelCase string.
 */
var hyphenateRE = /\B([A-Z])/g;
var hyphenate = cached(function (str) {
  return str.replace(hyphenateRE, '-$1').toLowerCase()
});

/**
 * Simple bind polyfill for environments that do not support it,
 * e.g., PhantomJS 1.x. Technically, we don't need this anymore
 * since native bind is now performant enough in most browsers.
 * But removing it would mean breaking code that was able to run in
 * PhantomJS 1.x, so this must be kept for backward compatibility.
 */

/* istanbul ignore next */
function polyfillBind (fn, ctx) {
  function boundFn (a) {
    var l = arguments.length;
    return l
      ? l > 1
        ? fn.apply(ctx, arguments)
        : fn.call(ctx, a)
      : fn.call(ctx)
  }

  boundFn._length = fn.length;
  return boundFn
}

function nativeBind (fn, ctx) {
  return fn.bind(ctx)
}

var bind = Function.prototype.bind
  ? nativeBind
  : polyfillBind;

/**
 * Convert an Array-like object to a real Array.
 */
function toArray (list, start) {
  start = start || 0;
  var i = list.length - start;
  var ret = new Array(i);
  while (i--) {
    ret[i] = list[i + start];
  }
  return ret
}

/**
 * Mix properties into target object.
 */
function extend (to, _from) {
  for (var key in _from) {
    to[key] = _from[key];
  }
  return to
}

/**
 * Merge an Array of Objects into a single Object.
 */
function toObject (arr) {
  var res = {};
  for (var i = 0; i < arr.length; i++) {
    if (arr[i]) {
      extend(res, arr[i]);
    }
  }
  return res
}

/* eslint-disable no-unused-vars */

/**
 * Perform no operation.
 * Stubbing args to make Flow happy without leaving useless transpiled code
 * with ...rest (https://flow.org/blog/2017/05/07/Strict-Function-Call-Arity/).
 */
function noop (a, b, c) {}

/**
 * Always return false.
 */
var no = function (a, b, c) { return false; };

/* eslint-enable no-unused-vars */

/**
 * Return the same value.
 */
var identity = function (_) { return _; };

/**
 * Check if two values are loosely equal - that is,
 * if they are plain objects, do they have the same shape?
 */
function looseEqual (a, b) {
  if (a === b) { return true }
  var isObjectA = isObject(a);
  var isObjectB = isObject(b);
  if (isObjectA && isObjectB) {
    try {
      var isArrayA = Array.isArray(a);
      var isArrayB = Array.isArray(b);
      if (isArrayA && isArrayB) {
        return a.length === b.length && a.every(function (e, i) {
          return looseEqual(e, b[i])
        })
      } else if (a instanceof Date && b instanceof Date) {
        return a.getTime() === b.getTime()
      } else if (!isArrayA && !isArrayB) {
        var keysA = Object.keys(a);
        var keysB = Object.keys(b);
        return keysA.length === keysB.length && keysA.every(function (key) {
          return looseEqual(a[key], b[key])
        })
      } else {
        /* istanbul ignore next */
        return false
      }
    } catch (e) {
      /* istanbul ignore next */
      return false
    }
  } else if (!isObjectA && !isObjectB) {
    return String(a) === String(b)
  } else {
    return false
  }
}

/**
 * Return the first index at which a loosely equal value can be
 * found in the array (if value is a plain object, the array must
 * contain an object of the same shape), or -1 if it is not present.
 */
function looseIndexOf (arr, val) {
  for (var i = 0; i < arr.length; i++) {
    if (looseEqual(arr[i], val)) { return i }
  }
  return -1
}

/**
 * Ensure a function is called only once.
 */
function once (fn) {
  var called = false;
  return function () {
    if (!called) {
      called = true;
      fn.apply(this, arguments);
    }
  }
}

var ASSET_TYPES = [
  'component',
  'directive',
  'filter'
];

var LIFECYCLE_HOOKS = [
  'beforeCreate',
  'created',
  'beforeMount',
  'mounted',
  'beforeUpdate',
  'updated',
  'beforeDestroy',
  'destroyed',
  'activated',
  'deactivated',
  'errorCaptured',
  'serverPrefetch'
];

/*  */



var config = ({
  /**
   * Option merge strategies (used in core/util/options)
   */
  // $flow-disable-line
  optionMergeStrategies: Object.create(null),

  /**
   * Whether to suppress warnings.
   */
  silent: false,

  /**
   * Show production mode tip message on boot?
   */
  productionTip: "development" !== 'production',

  /**
   * Whether to enable devtools
   */
  devtools: "development" !== 'production',

  /**
   * Whether to record perf
   */
  performance: false,

  /**
   * Error handler for watcher errors
   */
  errorHandler: null,

  /**
   * Warn handler for watcher warns
   */
  warnHandler: null,

  /**
   * Ignore certain custom elements
   */
  ignoredElements: [],

  /**
   * Custom user key aliases for v-on
   */
  // $flow-disable-line
  keyCodes: Object.create(null),

  /**
   * Check if a tag is reserved so that it cannot be registered as a
   * component. This is platform-dependent and may be overwritten.
   */
  isReservedTag: no,

  /**
   * Check if an attribute is reserved so that it cannot be used as a component
   * prop. This is platform-dependent and may be overwritten.
   */
  isReservedAttr: no,

  /**
   * Check if a tag is an unknown element.
   * Platform-dependent.
   */
  isUnknownElement: no,

  /**
   * Get the namespace of an element
   */
  getTagNamespace: noop,

  /**
   * Parse the real tag name for the specific platform.
   */
  parsePlatformTagName: identity,

  /**
   * Check if an attribute must be bound using property, e.g. value
   * Platform-dependent.
   */
  mustUseProp: no,

  /**
   * Perform updates asynchronously. Intended to be used by Vue Test Utils
   * This will significantly reduce performance if set to false.
   */
  async: true,

  /**
   * Exposed for legacy reasons
   */
  _lifecycleHooks: LIFECYCLE_HOOKS
});

/*  */

/**
 * unicode letters used for parsing html tags, component names and property paths.
 * using https://www.w3.org/TR/html53/semantics-scripting.html#potentialcustomelementname
 * skipping \u10000-\uEFFFF due to it freezing up PhantomJS
 */
var unicodeRegExp = /a-zA-Z\u00B7\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u037D\u037F-\u1FFF\u200C-\u200D\u203F-\u2040\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD/;

/**
 * Check if a string starts with $ or _
 */
function isReserved (str) {
  var c = (str + '').charCodeAt(0);
  return c === 0x24 || c === 0x5F
}

/**
 * Define a property.
 */
function def (obj, key, val, enumerable) {
  Object.defineProperty(obj, key, {
    value: val,
    enumerable: !!enumerable,
    writable: true,
    configurable: true
  });
}

/**
 * Parse simple path.
 */
var bailRE = new RegExp(("[^" + (unicodeRegExp.source) + ".$_\\d]"));
function parsePath (path) {
  if (bailRE.test(path)) {
    return
  }
  var segments = path.split('.');
  return function (obj) {
    for (var i = 0; i < segments.length; i++) {
      if (!obj) { return }
      obj = obj[segments[i]];
    }
    return obj
  }
}

/*  */

// can we use __proto__?
var hasProto = '__proto__' in {};

// Browser environment sniffing
var inBrowser = typeof window !== 'undefined';
var inWeex = typeof WXEnvironment !== 'undefined' && !!WXEnvironment.platform;
var weexPlatform = inWeex && WXEnvironment.platform.toLowerCase();
var UA = inBrowser && window.navigator.userAgent.toLowerCase();
var isIE = UA && /msie|trident/.test(UA);
var isIE9 = UA && UA.indexOf('msie 9.0') > 0;
var isEdge = UA && UA.indexOf('edge/') > 0;
var isAndroid = (UA && UA.indexOf('android') > 0) || (weexPlatform === 'android');
var isIOS = (UA && /iphone|ipad|ipod|ios/.test(UA)) || (weexPlatform === 'ios');
var isChrome = UA && /chrome\/\d+/.test(UA) && !isEdge;
var isPhantomJS = UA && /phantomjs/.test(UA);
var isFF = UA && UA.match(/firefox\/(\d+)/);

// Firefox has a "watch" function on Object.prototype...
var nativeWatch = ({}).watch;
if (inBrowser) {
  try {
    var opts = {};
    Object.defineProperty(opts, 'passive', ({
      get: function get () {
      }
    })); // https://github.com/facebook/flow/issues/285
    window.addEventListener('test-passive', null, opts);
  } catch (e) {}
}

// this needs to be lazy-evaled because vue may be required before
// vue-server-renderer can set VUE_ENV
var _isServer;
var isServerRendering = function () {
  if (_isServer === undefined) {
    /* istanbul ignore if */
    if (!inBrowser && !inWeex && typeof global !== 'undefined') {
      // detect presence of vue-server-renderer and avoid
      // Webpack shimming the process
      _isServer = global['process'] && global['process'].env.VUE_ENV === 'server';
    } else {
      _isServer = false;
    }
  }
  return _isServer
};

// detect devtools
var devtools = inBrowser && window.__VUE_DEVTOOLS_GLOBAL_HOOK__;

/* istanbul ignore next */
function isNative (Ctor) {
  return typeof Ctor === 'function' && /native code/.test(Ctor.toString())
}

var hasSymbol =
  typeof Symbol !== 'undefined' && isNative(Symbol) &&
  typeof Reflect !== 'undefined' && isNative(Reflect.ownKeys);

var _Set;
/* istanbul ignore if */ // $flow-disable-line
if (typeof Set !== 'undefined' && isNative(Set)) {
  // use native Set when available.
  _Set = Set;
} else {
  // a non-standard Set polyfill that only works with primitive keys.
  _Set = /*@__PURE__*/(function () {
    function Set () {
      this.set = Object.create(null);
    }
    Set.prototype.has = function has (key) {
      return this.set[key] === true
    };
    Set.prototype.add = function add (key) {
      this.set[key] = true;
    };
    Set.prototype.clear = function clear () {
      this.set = Object.create(null);
    };

    return Set;
  }());
}

/*  */

var warn = noop;
var tip = noop;
var generateComponentTrace = (noop); // work around flow check
var formatComponentName = (noop);

if (true) {
  var hasConsole = typeof console !== 'undefined';
  var classifyRE = /(?:^|[-_])(\w)/g;
  var classify = function (str) { return str
    .replace(classifyRE, function (c) { return c.toUpperCase(); })
    .replace(/[-_]/g, ''); };

  warn = function (msg, vm) {
    var trace = vm ? generateComponentTrace(vm) : '';

    if (config.warnHandler) {
      config.warnHandler.call(null, msg, vm, trace);
    } else if (hasConsole && (!config.silent)) {
      console.error(("[Vue warn]: " + msg + trace));
    }
  };

  tip = function (msg, vm) {
    if (hasConsole && (!config.silent)) {
      console.warn("[Vue tip]: " + msg + (
        vm ? generateComponentTrace(vm) : ''
      ));
    }
  };

  formatComponentName = function (vm, includeFile) {
    if (vm.$root === vm) {
      if (vm.$options && vm.$options.__file) { // fixed by xxxxxx
        return ('') + vm.$options.__file
      }
      return '<Root>'
    }
    var options = typeof vm === 'function' && vm.cid != null
      ? vm.options
      : vm._isVue
        ? vm.$options || vm.constructor.options
        : vm;
    var name = options.name || options._componentTag;
    var file = options.__file;
    if (!name && file) {
      var match = file.match(/([^/\\]+)\.vue$/);
      name = match && match[1];
    }

    return (
      (name ? ("<" + (classify(name)) + ">") : "<Anonymous>") +
      (file && includeFile !== false ? (" at " + file) : '')
    )
  };

  var repeat = function (str, n) {
    var res = '';
    while (n) {
      if (n % 2 === 1) { res += str; }
      if (n > 1) { str += str; }
      n >>= 1;
    }
    return res
  };

  generateComponentTrace = function (vm) {
    if (vm._isVue && vm.$parent) {
      var tree = [];
      var currentRecursiveSequence = 0;
      while (vm && vm.$options.name !== 'PageBody') {
        if (tree.length > 0) {
          var last = tree[tree.length - 1];
          if (last.constructor === vm.constructor) {
            currentRecursiveSequence++;
            vm = vm.$parent;
            continue
          } else if (currentRecursiveSequence > 0) {
            tree[tree.length - 1] = [last, currentRecursiveSequence];
            currentRecursiveSequence = 0;
          }
        }
        !vm.$options.isReserved && tree.push(vm);
        vm = vm.$parent;
      }
      return '\n\nfound in\n\n' + tree
        .map(function (vm, i) { return ("" + (i === 0 ? '---> ' : repeat(' ', 5 + i * 2)) + (Array.isArray(vm)
            ? ((formatComponentName(vm[0])) + "... (" + (vm[1]) + " recursive calls)")
            : formatComponentName(vm))); })
        .join('\n')
    } else {
      return ("\n\n(found in " + (formatComponentName(vm)) + ")")
    }
  };
}

/*  */

var uid = 0;

/**
 * A dep is an observable that can have multiple
 * directives subscribing to it.
 */
var Dep = function Dep () {
  this.id = uid++;
  this.subs = [];
};

Dep.prototype.addSub = function addSub (sub) {
  this.subs.push(sub);
};

Dep.prototype.removeSub = function removeSub (sub) {
  remove(this.subs, sub);
};

Dep.prototype.depend = function depend () {
  if (Dep.SharedObject.target) {
    Dep.SharedObject.target.addDep(this);
  }
};

Dep.prototype.notify = function notify () {
  // stabilize the subscriber list first
  var subs = this.subs.slice();
  if ( true && !config.async) {
    // subs aren't sorted in scheduler if not running async
    // we need to sort them now to make sure they fire in correct
    // order
    subs.sort(function (a, b) { return a.id - b.id; });
  }
  for (var i = 0, l = subs.length; i < l; i++) {
    subs[i].update();
  }
};

// The current target watcher being evaluated.
// This is globally unique because only one watcher
// can be evaluated at a time.
// fixed by xxxxxx (nvue shared vuex)
/* eslint-disable no-undef */
Dep.SharedObject = {};
Dep.SharedObject.target = null;
Dep.SharedObject.targetStack = [];

function pushTarget (target) {
  Dep.SharedObject.targetStack.push(target);
  Dep.SharedObject.target = target;
  Dep.target = target;
}

function popTarget () {
  Dep.SharedObject.targetStack.pop();
  Dep.SharedObject.target = Dep.SharedObject.targetStack[Dep.SharedObject.targetStack.length - 1];
  Dep.target = Dep.SharedObject.target;
}

/*  */

var VNode = function VNode (
  tag,
  data,
  children,
  text,
  elm,
  context,
  componentOptions,
  asyncFactory
) {
  this.tag = tag;
  this.data = data;
  this.children = children;
  this.text = text;
  this.elm = elm;
  this.ns = undefined;
  this.context = context;
  this.fnContext = undefined;
  this.fnOptions = undefined;
  this.fnScopeId = undefined;
  this.key = data && data.key;
  this.componentOptions = componentOptions;
  this.componentInstance = undefined;
  this.parent = undefined;
  this.raw = false;
  this.isStatic = false;
  this.isRootInsert = true;
  this.isComment = false;
  this.isCloned = false;
  this.isOnce = false;
  this.asyncFactory = asyncFactory;
  this.asyncMeta = undefined;
  this.isAsyncPlaceholder = false;
};

var prototypeAccessors = { child: { configurable: true } };

// DEPRECATED: alias for componentInstance for backwards compat.
/* istanbul ignore next */
prototypeAccessors.child.get = function () {
  return this.componentInstance
};

Object.defineProperties( VNode.prototype, prototypeAccessors );

var createEmptyVNode = function (text) {
  if ( text === void 0 ) text = '';

  var node = new VNode();
  node.text = text;
  node.isComment = true;
  return node
};

function createTextVNode (val) {
  return new VNode(undefined, undefined, undefined, String(val))
}

// optimized shallow clone
// used for static nodes and slot nodes because they may be reused across
// multiple renders, cloning them avoids errors when DOM manipulations rely
// on their elm reference.
function cloneVNode (vnode) {
  var cloned = new VNode(
    vnode.tag,
    vnode.data,
    // #7975
    // clone children array to avoid mutating original in case of cloning
    // a child.
    vnode.children && vnode.children.slice(),
    vnode.text,
    vnode.elm,
    vnode.context,
    vnode.componentOptions,
    vnode.asyncFactory
  );
  cloned.ns = vnode.ns;
  cloned.isStatic = vnode.isStatic;
  cloned.key = vnode.key;
  cloned.isComment = vnode.isComment;
  cloned.fnContext = vnode.fnContext;
  cloned.fnOptions = vnode.fnOptions;
  cloned.fnScopeId = vnode.fnScopeId;
  cloned.asyncMeta = vnode.asyncMeta;
  cloned.isCloned = true;
  return cloned
}

/*
 * not type checking this file because flow doesn't play well with
 * dynamically accessing methods on Array prototype
 */

var arrayProto = Array.prototype;
var arrayMethods = Object.create(arrayProto);

var methodsToPatch = [
  'push',
  'pop',
  'shift',
  'unshift',
  'splice',
  'sort',
  'reverse'
];

/**
 * Intercept mutating methods and emit events
 */
methodsToPatch.forEach(function (method) {
  // cache original method
  var original = arrayProto[method];
  def(arrayMethods, method, function mutator () {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    var result = original.apply(this, args);
    var ob = this.__ob__;
    var inserted;
    switch (method) {
      case 'push':
      case 'unshift':
        inserted = args;
        break
      case 'splice':
        inserted = args.slice(2);
        break
    }
    if (inserted) { ob.observeArray(inserted); }
    // notify change
    ob.dep.notify();
    return result
  });
});

/*  */

var arrayKeys = Object.getOwnPropertyNames(arrayMethods);

/**
 * In some cases we may want to disable observation inside a component's
 * update computation.
 */
var shouldObserve = true;

function toggleObserving (value) {
  shouldObserve = value;
}

/**
 * Observer class that is attached to each observed
 * object. Once attached, the observer converts the target
 * object's property keys into getter/setters that
 * collect dependencies and dispatch updates.
 */
var Observer = function Observer (value) {
  this.value = value;
  this.dep = new Dep();
  this.vmCount = 0;
  def(value, '__ob__', this);
  if (Array.isArray(value)) {
    if (hasProto) {
      {// fixed by xxxxxx 微信小程序使用 plugins 之后，数组方法被直接挂载到了数组对象上，需要执行 copyAugment 逻辑
        if(value.push !== value.__proto__.push){
          copyAugment(value, arrayMethods, arrayKeys);
        } else {
          protoAugment(value, arrayMethods);
        }
      }
    } else {
      copyAugment(value, arrayMethods, arrayKeys);
    }
    this.observeArray(value);
  } else {
    this.walk(value);
  }
};

/**
 * Walk through all properties and convert them into
 * getter/setters. This method should only be called when
 * value type is Object.
 */
Observer.prototype.walk = function walk (obj) {
  var keys = Object.keys(obj);
  for (var i = 0; i < keys.length; i++) {
    defineReactive$$1(obj, keys[i]);
  }
};

/**
 * Observe a list of Array items.
 */
Observer.prototype.observeArray = function observeArray (items) {
  for (var i = 0, l = items.length; i < l; i++) {
    observe(items[i]);
  }
};

// helpers

/**
 * Augment a target Object or Array by intercepting
 * the prototype chain using __proto__
 */
function protoAugment (target, src) {
  /* eslint-disable no-proto */
  target.__proto__ = src;
  /* eslint-enable no-proto */
}

/**
 * Augment a target Object or Array by defining
 * hidden properties.
 */
/* istanbul ignore next */
function copyAugment (target, src, keys) {
  for (var i = 0, l = keys.length; i < l; i++) {
    var key = keys[i];
    def(target, key, src[key]);
  }
}

/**
 * Attempt to create an observer instance for a value,
 * returns the new observer if successfully observed,
 * or the existing observer if the value already has one.
 */
function observe (value, asRootData) {
  if (!isObject(value) || value instanceof VNode) {
    return
  }
  var ob;
  if (hasOwn(value, '__ob__') && value.__ob__ instanceof Observer) {
    ob = value.__ob__;
  } else if (
    shouldObserve &&
    !isServerRendering() &&
    (Array.isArray(value) || isPlainObject(value)) &&
    Object.isExtensible(value) &&
    !value._isVue &&
    !value.__v_isMPComponent
  ) {
    ob = new Observer(value);
  }
  if (asRootData && ob) {
    ob.vmCount++;
  }
  return ob
}

/**
 * Define a reactive property on an Object.
 */
function defineReactive$$1 (
  obj,
  key,
  val,
  customSetter,
  shallow
) {
  var dep = new Dep();

  var property = Object.getOwnPropertyDescriptor(obj, key);
  if (property && property.configurable === false) {
    return
  }

  // cater for pre-defined getter/setters
  var getter = property && property.get;
  var setter = property && property.set;
  if ((!getter || setter) && arguments.length === 2) {
    val = obj[key];
  }

  var childOb = !shallow && observe(val);
  Object.defineProperty(obj, key, {
    enumerable: true,
    configurable: true,
    get: function reactiveGetter () {
      var value = getter ? getter.call(obj) : val;
      if (Dep.SharedObject.target) { // fixed by xxxxxx
        dep.depend();
        if (childOb) {
          childOb.dep.depend();
          if (Array.isArray(value)) {
            dependArray(value);
          }
        }
      }
      return value
    },
    set: function reactiveSetter (newVal) {
      var value = getter ? getter.call(obj) : val;
      /* eslint-disable no-self-compare */
      if (newVal === value || (newVal !== newVal && value !== value)) {
        return
      }
      /* eslint-enable no-self-compare */
      if ( true && customSetter) {
        customSetter();
      }
      // #7981: for accessor properties without setter
      if (getter && !setter) { return }
      if (setter) {
        setter.call(obj, newVal);
      } else {
        val = newVal;
      }
      childOb = !shallow && observe(newVal);
      dep.notify();
    }
  });
}

/**
 * Set a property on an object. Adds the new property and
 * triggers change notification if the property doesn't
 * already exist.
 */
function set (target, key, val) {
  if ( true &&
    (isUndef(target) || isPrimitive(target))
  ) {
    warn(("Cannot set reactive property on undefined, null, or primitive value: " + ((target))));
  }
  if (Array.isArray(target) && isValidArrayIndex(key)) {
    target.length = Math.max(target.length, key);
    target.splice(key, 1, val);
    return val
  }
  if (key in target && !(key in Object.prototype)) {
    target[key] = val;
    return val
  }
  var ob = (target).__ob__;
  if (target._isVue || (ob && ob.vmCount)) {
     true && warn(
      'Avoid adding reactive properties to a Vue instance or its root $data ' +
      'at runtime - declare it upfront in the data option.'
    );
    return val
  }
  if (!ob) {
    target[key] = val;
    return val
  }
  defineReactive$$1(ob.value, key, val);
  ob.dep.notify();
  return val
}

/**
 * Delete a property and trigger change if necessary.
 */
function del (target, key) {
  if ( true &&
    (isUndef(target) || isPrimitive(target))
  ) {
    warn(("Cannot delete reactive property on undefined, null, or primitive value: " + ((target))));
  }
  if (Array.isArray(target) && isValidArrayIndex(key)) {
    target.splice(key, 1);
    return
  }
  var ob = (target).__ob__;
  if (target._isVue || (ob && ob.vmCount)) {
     true && warn(
      'Avoid deleting properties on a Vue instance or its root $data ' +
      '- just set it to null.'
    );
    return
  }
  if (!hasOwn(target, key)) {
    return
  }
  delete target[key];
  if (!ob) {
    return
  }
  ob.dep.notify();
}

/**
 * Collect dependencies on array elements when the array is touched, since
 * we cannot intercept array element access like property getters.
 */
function dependArray (value) {
  for (var e = (void 0), i = 0, l = value.length; i < l; i++) {
    e = value[i];
    e && e.__ob__ && e.__ob__.dep.depend();
    if (Array.isArray(e)) {
      dependArray(e);
    }
  }
}

/*  */

/**
 * Option overwriting strategies are functions that handle
 * how to merge a parent option value and a child option
 * value into the final value.
 */
var strats = config.optionMergeStrategies;

/**
 * Options with restrictions
 */
if (true) {
  strats.el = strats.propsData = function (parent, child, vm, key) {
    if (!vm) {
      warn(
        "option \"" + key + "\" can only be used during instance " +
        'creation with the `new` keyword.'
      );
    }
    return defaultStrat(parent, child)
  };
}

/**
 * Helper that recursively merges two data objects together.
 */
function mergeData (to, from) {
  if (!from) { return to }
  var key, toVal, fromVal;

  var keys = hasSymbol
    ? Reflect.ownKeys(from)
    : Object.keys(from);

  for (var i = 0; i < keys.length; i++) {
    key = keys[i];
    // in case the object is already observed...
    if (key === '__ob__') { continue }
    toVal = to[key];
    fromVal = from[key];
    if (!hasOwn(to, key)) {
      set(to, key, fromVal);
    } else if (
      toVal !== fromVal &&
      isPlainObject(toVal) &&
      isPlainObject(fromVal)
    ) {
      mergeData(toVal, fromVal);
    }
  }
  return to
}

/**
 * Data
 */
function mergeDataOrFn (
  parentVal,
  childVal,
  vm
) {
  if (!vm) {
    // in a Vue.extend merge, both should be functions
    if (!childVal) {
      return parentVal
    }
    if (!parentVal) {
      return childVal
    }
    // when parentVal & childVal are both present,
    // we need to return a function that returns the
    // merged result of both functions... no need to
    // check if parentVal is a function here because
    // it has to be a function to pass previous merges.
    return function mergedDataFn () {
      return mergeData(
        typeof childVal === 'function' ? childVal.call(this, this) : childVal,
        typeof parentVal === 'function' ? parentVal.call(this, this) : parentVal
      )
    }
  } else {
    return function mergedInstanceDataFn () {
      // instance merge
      var instanceData = typeof childVal === 'function'
        ? childVal.call(vm, vm)
        : childVal;
      var defaultData = typeof parentVal === 'function'
        ? parentVal.call(vm, vm)
        : parentVal;
      if (instanceData) {
        return mergeData(instanceData, defaultData)
      } else {
        return defaultData
      }
    }
  }
}

strats.data = function (
  parentVal,
  childVal,
  vm
) {
  if (!vm) {
    if (childVal && typeof childVal !== 'function') {
       true && warn(
        'The "data" option should be a function ' +
        'that returns a per-instance value in component ' +
        'definitions.',
        vm
      );

      return parentVal
    }
    return mergeDataOrFn(parentVal, childVal)
  }

  return mergeDataOrFn(parentVal, childVal, vm)
};

/**
 * Hooks and props are merged as arrays.
 */
function mergeHook (
  parentVal,
  childVal
) {
  var res = childVal
    ? parentVal
      ? parentVal.concat(childVal)
      : Array.isArray(childVal)
        ? childVal
        : [childVal]
    : parentVal;
  return res
    ? dedupeHooks(res)
    : res
}

function dedupeHooks (hooks) {
  var res = [];
  for (var i = 0; i < hooks.length; i++) {
    if (res.indexOf(hooks[i]) === -1) {
      res.push(hooks[i]);
    }
  }
  return res
}

LIFECYCLE_HOOKS.forEach(function (hook) {
  strats[hook] = mergeHook;
});

/**
 * Assets
 *
 * When a vm is present (instance creation), we need to do
 * a three-way merge between constructor options, instance
 * options and parent options.
 */
function mergeAssets (
  parentVal,
  childVal,
  vm,
  key
) {
  var res = Object.create(parentVal || null);
  if (childVal) {
     true && assertObjectType(key, childVal, vm);
    return extend(res, childVal)
  } else {
    return res
  }
}

ASSET_TYPES.forEach(function (type) {
  strats[type + 's'] = mergeAssets;
});

/**
 * Watchers.
 *
 * Watchers hashes should not overwrite one
 * another, so we merge them as arrays.
 */
strats.watch = function (
  parentVal,
  childVal,
  vm,
  key
) {
  // work around Firefox's Object.prototype.watch...
  if (parentVal === nativeWatch) { parentVal = undefined; }
  if (childVal === nativeWatch) { childVal = undefined; }
  /* istanbul ignore if */
  if (!childVal) { return Object.create(parentVal || null) }
  if (true) {
    assertObjectType(key, childVal, vm);
  }
  if (!parentVal) { return childVal }
  var ret = {};
  extend(ret, parentVal);
  for (var key$1 in childVal) {
    var parent = ret[key$1];
    var child = childVal[key$1];
    if (parent && !Array.isArray(parent)) {
      parent = [parent];
    }
    ret[key$1] = parent
      ? parent.concat(child)
      : Array.isArray(child) ? child : [child];
  }
  return ret
};

/**
 * Other object hashes.
 */
strats.props =
strats.methods =
strats.inject =
strats.computed = function (
  parentVal,
  childVal,
  vm,
  key
) {
  if (childVal && "development" !== 'production') {
    assertObjectType(key, childVal, vm);
  }
  if (!parentVal) { return childVal }
  var ret = Object.create(null);
  extend(ret, parentVal);
  if (childVal) { extend(ret, childVal); }
  return ret
};
strats.provide = mergeDataOrFn;

/**
 * Default strategy.
 */
var defaultStrat = function (parentVal, childVal) {
  return childVal === undefined
    ? parentVal
    : childVal
};

/**
 * Validate component names
 */
function checkComponents (options) {
  for (var key in options.components) {
    validateComponentName(key);
  }
}

function validateComponentName (name) {
  if (!new RegExp(("^[a-zA-Z][\\-\\.0-9_" + (unicodeRegExp.source) + "]*$")).test(name)) {
    warn(
      'Invalid component name: "' + name + '". Component names ' +
      'should conform to valid custom element name in html5 specification.'
    );
  }
  if (isBuiltInTag(name) || config.isReservedTag(name)) {
    warn(
      'Do not use built-in or reserved HTML elements as component ' +
      'id: ' + name
    );
  }
}

/**
 * Ensure all props option syntax are normalized into the
 * Object-based format.
 */
function normalizeProps (options, vm) {
  var props = options.props;
  if (!props) { return }
  var res = {};
  var i, val, name;
  if (Array.isArray(props)) {
    i = props.length;
    while (i--) {
      val = props[i];
      if (typeof val === 'string') {
        name = camelize(val);
        res[name] = { type: null };
      } else if (true) {
        warn('props must be strings when using array syntax.');
      }
    }
  } else if (isPlainObject(props)) {
    for (var key in props) {
      val = props[key];
      name = camelize(key);
      res[name] = isPlainObject(val)
        ? val
        : { type: val };
    }
  } else if (true) {
    warn(
      "Invalid value for option \"props\": expected an Array or an Object, " +
      "but got " + (toRawType(props)) + ".",
      vm
    );
  }
  options.props = res;
}

/**
 * Normalize all injections into Object-based format
 */
function normalizeInject (options, vm) {
  var inject = options.inject;
  if (!inject) { return }
  var normalized = options.inject = {};
  if (Array.isArray(inject)) {
    for (var i = 0; i < inject.length; i++) {
      normalized[inject[i]] = { from: inject[i] };
    }
  } else if (isPlainObject(inject)) {
    for (var key in inject) {
      var val = inject[key];
      normalized[key] = isPlainObject(val)
        ? extend({ from: key }, val)
        : { from: val };
    }
  } else if (true) {
    warn(
      "Invalid value for option \"inject\": expected an Array or an Object, " +
      "but got " + (toRawType(inject)) + ".",
      vm
    );
  }
}

/**
 * Normalize raw function directives into object format.
 */
function normalizeDirectives (options) {
  var dirs = options.directives;
  if (dirs) {
    for (var key in dirs) {
      var def$$1 = dirs[key];
      if (typeof def$$1 === 'function') {
        dirs[key] = { bind: def$$1, update: def$$1 };
      }
    }
  }
}

function assertObjectType (name, value, vm) {
  if (!isPlainObject(value)) {
    warn(
      "Invalid value for option \"" + name + "\": expected an Object, " +
      "but got " + (toRawType(value)) + ".",
      vm
    );
  }
}

/**
 * Merge two option objects into a new one.
 * Core utility used in both instantiation and inheritance.
 */
function mergeOptions (
  parent,
  child,
  vm
) {
  if (true) {
    checkComponents(child);
  }

  if (typeof child === 'function') {
    child = child.options;
  }

  normalizeProps(child, vm);
  normalizeInject(child, vm);
  normalizeDirectives(child);

  // Apply extends and mixins on the child options,
  // but only if it is a raw options object that isn't
  // the result of another mergeOptions call.
  // Only merged options has the _base property.
  if (!child._base) {
    if (child.extends) {
      parent = mergeOptions(parent, child.extends, vm);
    }
    if (child.mixins) {
      for (var i = 0, l = child.mixins.length; i < l; i++) {
        parent = mergeOptions(parent, child.mixins[i], vm);
      }
    }
  }

  var options = {};
  var key;
  for (key in parent) {
    mergeField(key);
  }
  for (key in child) {
    if (!hasOwn(parent, key)) {
      mergeField(key);
    }
  }
  function mergeField (key) {
    var strat = strats[key] || defaultStrat;
    options[key] = strat(parent[key], child[key], vm, key);
  }
  return options
}

/**
 * Resolve an asset.
 * This function is used because child instances need access
 * to assets defined in its ancestor chain.
 */
function resolveAsset (
  options,
  type,
  id,
  warnMissing
) {
  /* istanbul ignore if */
  if (typeof id !== 'string') {
    return
  }
  var assets = options[type];
  // check local registration variations first
  if (hasOwn(assets, id)) { return assets[id] }
  var camelizedId = camelize(id);
  if (hasOwn(assets, camelizedId)) { return assets[camelizedId] }
  var PascalCaseId = capitalize(camelizedId);
  if (hasOwn(assets, PascalCaseId)) { return assets[PascalCaseId] }
  // fallback to prototype chain
  var res = assets[id] || assets[camelizedId] || assets[PascalCaseId];
  if ( true && warnMissing && !res) {
    warn(
      'Failed to resolve ' + type.slice(0, -1) + ': ' + id,
      options
    );
  }
  return res
}

/*  */



function validateProp (
  key,
  propOptions,
  propsData,
  vm
) {
  var prop = propOptions[key];
  var absent = !hasOwn(propsData, key);
  var value = propsData[key];
  // boolean casting
  var booleanIndex = getTypeIndex(Boolean, prop.type);
  if (booleanIndex > -1) {
    if (absent && !hasOwn(prop, 'default')) {
      value = false;
    } else if (value === '' || value === hyphenate(key)) {
      // only cast empty string / same name to boolean if
      // boolean has higher priority
      var stringIndex = getTypeIndex(String, prop.type);
      if (stringIndex < 0 || booleanIndex < stringIndex) {
        value = true;
      }
    }
  }
  // check default value
  if (value === undefined) {
    value = getPropDefaultValue(vm, prop, key);
    // since the default value is a fresh copy,
    // make sure to observe it.
    var prevShouldObserve = shouldObserve;
    toggleObserving(true);
    observe(value);
    toggleObserving(prevShouldObserve);
  }
  if (
    true
  ) {
    assertProp(prop, key, value, vm, absent);
  }
  return value
}

/**
 * Get the default value of a prop.
 */
function getPropDefaultValue (vm, prop, key) {
  // no default, return undefined
  if (!hasOwn(prop, 'default')) {
    return undefined
  }
  var def = prop.default;
  // warn against non-factory defaults for Object & Array
  if ( true && isObject(def)) {
    warn(
      'Invalid default value for prop "' + key + '": ' +
      'Props with type Object/Array must use a factory function ' +
      'to return the default value.',
      vm
    );
  }
  // the raw prop value was also undefined from previous render,
  // return previous default value to avoid unnecessary watcher trigger
  if (vm && vm.$options.propsData &&
    vm.$options.propsData[key] === undefined &&
    vm._props[key] !== undefined
  ) {
    return vm._props[key]
  }
  // call factory function for non-Function types
  // a value is Function if its prototype is function even across different execution context
  return typeof def === 'function' && getType(prop.type) !== 'Function'
    ? def.call(vm)
    : def
}

/**
 * Assert whether a prop is valid.
 */
function assertProp (
  prop,
  name,
  value,
  vm,
  absent
) {
  if (prop.required && absent) {
    warn(
      'Missing required prop: "' + name + '"',
      vm
    );
    return
  }
  if (value == null && !prop.required) {
    return
  }
  var type = prop.type;
  var valid = !type || type === true;
  var expectedTypes = [];
  if (type) {
    if (!Array.isArray(type)) {
      type = [type];
    }
    for (var i = 0; i < type.length && !valid; i++) {
      var assertedType = assertType(value, type[i]);
      expectedTypes.push(assertedType.expectedType || '');
      valid = assertedType.valid;
    }
  }

  if (!valid) {
    warn(
      getInvalidTypeMessage(name, value, expectedTypes),
      vm
    );
    return
  }
  var validator = prop.validator;
  if (validator) {
    if (!validator(value)) {
      warn(
        'Invalid prop: custom validator check failed for prop "' + name + '".',
        vm
      );
    }
  }
}

var simpleCheckRE = /^(String|Number|Boolean|Function|Symbol)$/;

function assertType (value, type) {
  var valid;
  var expectedType = getType(type);
  if (simpleCheckRE.test(expectedType)) {
    var t = typeof value;
    valid = t === expectedType.toLowerCase();
    // for primitive wrapper objects
    if (!valid && t === 'object') {
      valid = value instanceof type;
    }
  } else if (expectedType === 'Object') {
    valid = isPlainObject(value);
  } else if (expectedType === 'Array') {
    valid = Array.isArray(value);
  } else {
    valid = value instanceof type;
  }
  return {
    valid: valid,
    expectedType: expectedType
  }
}

/**
 * Use function string name to check built-in types,
 * because a simple equality check will fail when running
 * across different vms / iframes.
 */
function getType (fn) {
  var match = fn && fn.toString().match(/^\s*function (\w+)/);
  return match ? match[1] : ''
}

function isSameType (a, b) {
  return getType(a) === getType(b)
}

function getTypeIndex (type, expectedTypes) {
  if (!Array.isArray(expectedTypes)) {
    return isSameType(expectedTypes, type) ? 0 : -1
  }
  for (var i = 0, len = expectedTypes.length; i < len; i++) {
    if (isSameType(expectedTypes[i], type)) {
      return i
    }
  }
  return -1
}

function getInvalidTypeMessage (name, value, expectedTypes) {
  var message = "Invalid prop: type check failed for prop \"" + name + "\"." +
    " Expected " + (expectedTypes.map(capitalize).join(', '));
  var expectedType = expectedTypes[0];
  var receivedType = toRawType(value);
  var expectedValue = styleValue(value, expectedType);
  var receivedValue = styleValue(value, receivedType);
  // check if we need to specify expected value
  if (expectedTypes.length === 1 &&
      isExplicable(expectedType) &&
      !isBoolean(expectedType, receivedType)) {
    message += " with value " + expectedValue;
  }
  message += ", got " + receivedType + " ";
  // check if we need to specify received value
  if (isExplicable(receivedType)) {
    message += "with value " + receivedValue + ".";
  }
  return message
}

function styleValue (value, type) {
  if (type === 'String') {
    return ("\"" + value + "\"")
  } else if (type === 'Number') {
    return ("" + (Number(value)))
  } else {
    return ("" + value)
  }
}

function isExplicable (value) {
  var explicitTypes = ['string', 'number', 'boolean'];
  return explicitTypes.some(function (elem) { return value.toLowerCase() === elem; })
}

function isBoolean () {
  var args = [], len = arguments.length;
  while ( len-- ) args[ len ] = arguments[ len ];

  return args.some(function (elem) { return elem.toLowerCase() === 'boolean'; })
}

/*  */

function handleError (err, vm, info) {
  // Deactivate deps tracking while processing error handler to avoid possible infinite rendering.
  // See: https://github.com/vuejs/vuex/issues/1505
  pushTarget();
  try {
    if (vm) {
      var cur = vm;
      while ((cur = cur.$parent)) {
        var hooks = cur.$options.errorCaptured;
        if (hooks) {
          for (var i = 0; i < hooks.length; i++) {
            try {
              var capture = hooks[i].call(cur, err, vm, info) === false;
              if (capture) { return }
            } catch (e) {
              globalHandleError(e, cur, 'errorCaptured hook');
            }
          }
        }
      }
    }
    globalHandleError(err, vm, info);
  } finally {
    popTarget();
  }
}

function invokeWithErrorHandling (
  handler,
  context,
  args,
  vm,
  info
) {
  var res;
  try {
    res = args ? handler.apply(context, args) : handler.call(context);
    if (res && !res._isVue && isPromise(res) && !res._handled) {
      res.catch(function (e) { return handleError(e, vm, info + " (Promise/async)"); });
      // issue #9511
      // avoid catch triggering multiple times when nested calls
      res._handled = true;
    }
  } catch (e) {
    handleError(e, vm, info);
  }
  return res
}

function globalHandleError (err, vm, info) {
  if (config.errorHandler) {
    try {
      return config.errorHandler.call(null, err, vm, info)
    } catch (e) {
      // if the user intentionally throws the original error in the handler,
      // do not log it twice
      if (e !== err) {
        logError(e, null, 'config.errorHandler');
      }
    }
  }
  logError(err, vm, info);
}

function logError (err, vm, info) {
  if (true) {
    warn(("Error in " + info + ": \"" + (err.toString()) + "\""), vm);
  }
  /* istanbul ignore else */
  if ((inBrowser || inWeex) && typeof console !== 'undefined') {
    console.error(err);
  } else {
    throw err
  }
}

/*  */

var callbacks = [];
var pending = false;

function flushCallbacks () {
  pending = false;
  var copies = callbacks.slice(0);
  callbacks.length = 0;
  for (var i = 0; i < copies.length; i++) {
    copies[i]();
  }
}

// Here we have async deferring wrappers using microtasks.
// In 2.5 we used (macro) tasks (in combination with microtasks).
// However, it has subtle problems when state is changed right before repaint
// (e.g. #6813, out-in transitions).
// Also, using (macro) tasks in event handler would cause some weird behaviors
// that cannot be circumvented (e.g. #7109, #7153, #7546, #7834, #8109).
// So we now use microtasks everywhere, again.
// A major drawback of this tradeoff is that there are some scenarios
// where microtasks have too high a priority and fire in between supposedly
// sequential events (e.g. #4521, #6690, which have workarounds)
// or even between bubbling of the same event (#6566).
var timerFunc;

// The nextTick behavior leverages the microtask queue, which can be accessed
// via either native Promise.then or MutationObserver.
// MutationObserver has wider support, however it is seriously bugged in
// UIWebView in iOS >= 9.3.3 when triggered in touch event handlers. It
// completely stops working after triggering a few times... so, if native
// Promise is available, we will use it:
/* istanbul ignore next, $flow-disable-line */
if (typeof Promise !== 'undefined' && isNative(Promise)) {
  var p = Promise.resolve();
  timerFunc = function () {
    p.then(flushCallbacks);
    // In problematic UIWebViews, Promise.then doesn't completely break, but
    // it can get stuck in a weird state where callbacks are pushed into the
    // microtask queue but the queue isn't being flushed, until the browser
    // needs to do some other work, e.g. handle a timer. Therefore we can
    // "force" the microtask queue to be flushed by adding an empty timer.
    if (isIOS) { setTimeout(noop); }
  };
} else if (!isIE && typeof MutationObserver !== 'undefined' && (
  isNative(MutationObserver) ||
  // PhantomJS and iOS 7.x
  MutationObserver.toString() === '[object MutationObserverConstructor]'
)) {
  // Use MutationObserver where native Promise is not available,
  // e.g. PhantomJS, iOS7, Android 4.4
  // (#6466 MutationObserver is unreliable in IE11)
  var counter = 1;
  var observer = new MutationObserver(flushCallbacks);
  var textNode = document.createTextNode(String(counter));
  observer.observe(textNode, {
    characterData: true
  });
  timerFunc = function () {
    counter = (counter + 1) % 2;
    textNode.data = String(counter);
  };
} else if (typeof setImmediate !== 'undefined' && isNative(setImmediate)) {
  // Fallback to setImmediate.
  // Technically it leverages the (macro) task queue,
  // but it is still a better choice than setTimeout.
  timerFunc = function () {
    setImmediate(flushCallbacks);
  };
} else {
  // Fallback to setTimeout.
  timerFunc = function () {
    setTimeout(flushCallbacks, 0);
  };
}

function nextTick (cb, ctx) {
  var _resolve;
  callbacks.push(function () {
    if (cb) {
      try {
        cb.call(ctx);
      } catch (e) {
        handleError(e, ctx, 'nextTick');
      }
    } else if (_resolve) {
      _resolve(ctx);
    }
  });
  if (!pending) {
    pending = true;
    timerFunc();
  }
  // $flow-disable-line
  if (!cb && typeof Promise !== 'undefined') {
    return new Promise(function (resolve) {
      _resolve = resolve;
    })
  }
}

/*  */

/* not type checking this file because flow doesn't play well with Proxy */

var initProxy;

if (true) {
  var allowedGlobals = makeMap(
    'Infinity,undefined,NaN,isFinite,isNaN,' +
    'parseFloat,parseInt,decodeURI,decodeURIComponent,encodeURI,encodeURIComponent,' +
    'Math,Number,Date,Array,Object,Boolean,String,RegExp,Map,Set,JSON,Intl,' +
    'require' // for Webpack/Browserify
  );

  var warnNonPresent = function (target, key) {
    warn(
      "Property or method \"" + key + "\" is not defined on the instance but " +
      'referenced during render. Make sure that this property is reactive, ' +
      'either in the data option, or for class-based components, by ' +
      'initializing the property. ' +
      'See: https://vuejs.org/v2/guide/reactivity.html#Declaring-Reactive-Properties.',
      target
    );
  };

  var warnReservedPrefix = function (target, key) {
    warn(
      "Property \"" + key + "\" must be accessed with \"$data." + key + "\" because " +
      'properties starting with "$" or "_" are not proxied in the Vue instance to ' +
      'prevent conflicts with Vue internals. ' +
      'See: https://vuejs.org/v2/api/#data',
      target
    );
  };

  var hasProxy =
    typeof Proxy !== 'undefined' && isNative(Proxy);

  if (hasProxy) {
    var isBuiltInModifier = makeMap('stop,prevent,self,ctrl,shift,alt,meta,exact');
    config.keyCodes = new Proxy(config.keyCodes, {
      set: function set (target, key, value) {
        if (isBuiltInModifier(key)) {
          warn(("Avoid overwriting built-in modifier in config.keyCodes: ." + key));
          return false
        } else {
          target[key] = value;
          return true
        }
      }
    });
  }

  var hasHandler = {
    has: function has (target, key) {
      var has = key in target;
      var isAllowed = allowedGlobals(key) ||
        (typeof key === 'string' && key.charAt(0) === '_' && !(key in target.$data));
      if (!has && !isAllowed) {
        if (key in target.$data) { warnReservedPrefix(target, key); }
        else { warnNonPresent(target, key); }
      }
      return has || !isAllowed
    }
  };

  var getHandler = {
    get: function get (target, key) {
      if (typeof key === 'string' && !(key in target)) {
        if (key in target.$data) { warnReservedPrefix(target, key); }
        else { warnNonPresent(target, key); }
      }
      return target[key]
    }
  };

  initProxy = function initProxy (vm) {
    if (hasProxy) {
      // determine which proxy handler to use
      var options = vm.$options;
      var handlers = options.render && options.render._withStripped
        ? getHandler
        : hasHandler;
      vm._renderProxy = new Proxy(vm, handlers);
    } else {
      vm._renderProxy = vm;
    }
  };
}

/*  */

var seenObjects = new _Set();

/**
 * Recursively traverse an object to evoke all converted
 * getters, so that every nested property inside the object
 * is collected as a "deep" dependency.
 */
function traverse (val) {
  _traverse(val, seenObjects);
  seenObjects.clear();
}

function _traverse (val, seen) {
  var i, keys;
  var isA = Array.isArray(val);
  if ((!isA && !isObject(val)) || Object.isFrozen(val) || val instanceof VNode) {
    return
  }
  if (val.__ob__) {
    var depId = val.__ob__.dep.id;
    if (seen.has(depId)) {
      return
    }
    seen.add(depId);
  }
  if (isA) {
    i = val.length;
    while (i--) { _traverse(val[i], seen); }
  } else {
    keys = Object.keys(val);
    i = keys.length;
    while (i--) { _traverse(val[keys[i]], seen); }
  }
}

var mark;
var measure;

if (true) {
  var perf = inBrowser && window.performance;
  /* istanbul ignore if */
  if (
    perf &&
    perf.mark &&
    perf.measure &&
    perf.clearMarks &&
    perf.clearMeasures
  ) {
    mark = function (tag) { return perf.mark(tag); };
    measure = function (name, startTag, endTag) {
      perf.measure(name, startTag, endTag);
      perf.clearMarks(startTag);
      perf.clearMarks(endTag);
      // perf.clearMeasures(name)
    };
  }
}

/*  */

var normalizeEvent = cached(function (name) {
  var passive = name.charAt(0) === '&';
  name = passive ? name.slice(1) : name;
  var once$$1 = name.charAt(0) === '~'; // Prefixed last, checked first
  name = once$$1 ? name.slice(1) : name;
  var capture = name.charAt(0) === '!';
  name = capture ? name.slice(1) : name;
  return {
    name: name,
    once: once$$1,
    capture: capture,
    passive: passive
  }
});

function createFnInvoker (fns, vm) {
  function invoker () {
    var arguments$1 = arguments;

    var fns = invoker.fns;
    if (Array.isArray(fns)) {
      var cloned = fns.slice();
      for (var i = 0; i < cloned.length; i++) {
        invokeWithErrorHandling(cloned[i], null, arguments$1, vm, "v-on handler");
      }
    } else {
      // return handler return value for single handlers
      return invokeWithErrorHandling(fns, null, arguments, vm, "v-on handler")
    }
  }
  invoker.fns = fns;
  return invoker
}

function updateListeners (
  on,
  oldOn,
  add,
  remove$$1,
  createOnceHandler,
  vm
) {
  var name, def$$1, cur, old, event;
  for (name in on) {
    def$$1 = cur = on[name];
    old = oldOn[name];
    event = normalizeEvent(name);
    if (isUndef(cur)) {
       true && warn(
        "Invalid handler for event \"" + (event.name) + "\": got " + String(cur),
        vm
      );
    } else if (isUndef(old)) {
      if (isUndef(cur.fns)) {
        cur = on[name] = createFnInvoker(cur, vm);
      }
      if (isTrue(event.once)) {
        cur = on[name] = createOnceHandler(event.name, cur, event.capture);
      }
      add(event.name, cur, event.capture, event.passive, event.params);
    } else if (cur !== old) {
      old.fns = cur;
      on[name] = old;
    }
  }
  for (name in oldOn) {
    if (isUndef(on[name])) {
      event = normalizeEvent(name);
      remove$$1(event.name, oldOn[name], event.capture);
    }
  }
}

/*  */

/*  */

// fixed by xxxxxx (mp properties)
function extractPropertiesFromVNodeData(data, Ctor, res, context) {
  var propOptions = Ctor.options.mpOptions && Ctor.options.mpOptions.properties;
  if (isUndef(propOptions)) {
    return res
  }
  var externalClasses = Ctor.options.mpOptions.externalClasses || [];
  var attrs = data.attrs;
  var props = data.props;
  if (isDef(attrs) || isDef(props)) {
    for (var key in propOptions) {
      var altKey = hyphenate(key);
      var result = checkProp(res, props, key, altKey, true) ||
          checkProp(res, attrs, key, altKey, false);
      // externalClass
      if (
        result &&
        res[key] &&
        externalClasses.indexOf(altKey) !== -1 &&
        context[camelize(res[key])]
      ) {
        // 赋值 externalClass 真正的值(模板里 externalClass 的值可能是字符串)
        res[key] = context[camelize(res[key])];
      }
    }
  }
  return res
}

function extractPropsFromVNodeData (
  data,
  Ctor,
  tag,
  context// fixed by xxxxxx
) {
  // we are only extracting raw values here.
  // validation and default values are handled in the child
  // component itself.
  var propOptions = Ctor.options.props;
  if (isUndef(propOptions)) {
    // fixed by xxxxxx
    return extractPropertiesFromVNodeData(data, Ctor, {}, context)
  }
  var res = {};
  var attrs = data.attrs;
  var props = data.props;
  if (isDef(attrs) || isDef(props)) {
    for (var key in propOptions) {
      var altKey = hyphenate(key);
      if (true) {
        var keyInLowerCase = key.toLowerCase();
        if (
          key !== keyInLowerCase &&
          attrs && hasOwn(attrs, keyInLowerCase)
        ) {
          tip(
            "Prop \"" + keyInLowerCase + "\" is passed to component " +
            (formatComponentName(tag || Ctor)) + ", but the declared prop name is" +
            " \"" + key + "\". " +
            "Note that HTML attributes are case-insensitive and camelCased " +
            "props need to use their kebab-case equivalents when using in-DOM " +
            "templates. You should probably use \"" + altKey + "\" instead of \"" + key + "\"."
          );
        }
      }
      checkProp(res, props, key, altKey, true) ||
      checkProp(res, attrs, key, altKey, false);
    }
  }
  // fixed by xxxxxx
  return extractPropertiesFromVNodeData(data, Ctor, res, context)
}

function checkProp (
  res,
  hash,
  key,
  altKey,
  preserve
) {
  if (isDef(hash)) {
    if (hasOwn(hash, key)) {
      res[key] = hash[key];
      if (!preserve) {
        delete hash[key];
      }
      return true
    } else if (hasOwn(hash, altKey)) {
      res[key] = hash[altKey];
      if (!preserve) {
        delete hash[altKey];
      }
      return true
    }
  }
  return false
}

/*  */

// The template compiler attempts to minimize the need for normalization by
// statically analyzing the template at compile time.
//
// For plain HTML markup, normalization can be completely skipped because the
// generated render function is guaranteed to return Array<VNode>. There are
// two cases where extra normalization is needed:

// 1. When the children contains components - because a functional component
// may return an Array instead of a single root. In this case, just a simple
// normalization is needed - if any child is an Array, we flatten the whole
// thing with Array.prototype.concat. It is guaranteed to be only 1-level deep
// because functional components already normalize their own children.
function simpleNormalizeChildren (children) {
  for (var i = 0; i < children.length; i++) {
    if (Array.isArray(children[i])) {
      return Array.prototype.concat.apply([], children)
    }
  }
  return children
}

// 2. When the children contains constructs that always generated nested Arrays,
// e.g. <template>, <slot>, v-for, or when the children is provided by user
// with hand-written render functions / JSX. In such cases a full normalization
// is needed to cater to all possible types of children values.
function normalizeChildren (children) {
  return isPrimitive(children)
    ? [createTextVNode(children)]
    : Array.isArray(children)
      ? normalizeArrayChildren(children)
      : undefined
}

function isTextNode (node) {
  return isDef(node) && isDef(node.text) && isFalse(node.isComment)
}

function normalizeArrayChildren (children, nestedIndex) {
  var res = [];
  var i, c, lastIndex, last;
  for (i = 0; i < children.length; i++) {
    c = children[i];
    if (isUndef(c) || typeof c === 'boolean') { continue }
    lastIndex = res.length - 1;
    last = res[lastIndex];
    //  nested
    if (Array.isArray(c)) {
      if (c.length > 0) {
        c = normalizeArrayChildren(c, ((nestedIndex || '') + "_" + i));
        // merge adjacent text nodes
        if (isTextNode(c[0]) && isTextNode(last)) {
          res[lastIndex] = createTextVNode(last.text + (c[0]).text);
          c.shift();
        }
        res.push.apply(res, c);
      }
    } else if (isPrimitive(c)) {
      if (isTextNode(last)) {
        // merge adjacent text nodes
        // this is necessary for SSR hydration because text nodes are
        // essentially merged when rendered to HTML strings
        res[lastIndex] = createTextVNode(last.text + c);
      } else if (c !== '') {
        // convert primitive to vnode
        res.push(createTextVNode(c));
      }
    } else {
      if (isTextNode(c) && isTextNode(last)) {
        // merge adjacent text nodes
        res[lastIndex] = createTextVNode(last.text + c.text);
      } else {
        // default key for nested array children (likely generated by v-for)
        if (isTrue(children._isVList) &&
          isDef(c.tag) &&
          isUndef(c.key) &&
          isDef(nestedIndex)) {
          c.key = "__vlist" + nestedIndex + "_" + i + "__";
        }
        res.push(c);
      }
    }
  }
  return res
}

/*  */

function initProvide (vm) {
  var provide = vm.$options.provide;
  if (provide) {
    vm._provided = typeof provide === 'function'
      ? provide.call(vm)
      : provide;
  }
}

function initInjections (vm) {
  var result = resolveInject(vm.$options.inject, vm);
  if (result) {
    toggleObserving(false);
    Object.keys(result).forEach(function (key) {
      /* istanbul ignore else */
      if (true) {
        defineReactive$$1(vm, key, result[key], function () {
          warn(
            "Avoid mutating an injected value directly since the changes will be " +
            "overwritten whenever the provided component re-renders. " +
            "injection being mutated: \"" + key + "\"",
            vm
          );
        });
      } else {}
    });
    toggleObserving(true);
  }
}

function resolveInject (inject, vm) {
  if (inject) {
    // inject is :any because flow is not smart enough to figure out cached
    var result = Object.create(null);
    var keys = hasSymbol
      ? Reflect.ownKeys(inject)
      : Object.keys(inject);

    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];
      // #6574 in case the inject object is observed...
      if (key === '__ob__') { continue }
      var provideKey = inject[key].from;
      var source = vm;
      while (source) {
        if (source._provided && hasOwn(source._provided, provideKey)) {
          result[key] = source._provided[provideKey];
          break
        }
        source = source.$parent;
      }
      if (!source) {
        if ('default' in inject[key]) {
          var provideDefault = inject[key].default;
          result[key] = typeof provideDefault === 'function'
            ? provideDefault.call(vm)
            : provideDefault;
        } else if (true) {
          warn(("Injection \"" + key + "\" not found"), vm);
        }
      }
    }
    return result
  }
}

/*  */



/**
 * Runtime helper for resolving raw children VNodes into a slot object.
 */
function resolveSlots (
  children,
  context
) {
  if (!children || !children.length) {
    return {}
  }
  var slots = {};
  for (var i = 0, l = children.length; i < l; i++) {
    var child = children[i];
    var data = child.data;
    // remove slot attribute if the node is resolved as a Vue slot node
    if (data && data.attrs && data.attrs.slot) {
      delete data.attrs.slot;
    }
    // named slots should only be respected if the vnode was rendered in the
    // same context.
    if ((child.context === context || child.fnContext === context) &&
      data && data.slot != null
    ) {
      var name = data.slot;
      var slot = (slots[name] || (slots[name] = []));
      if (child.tag === 'template') {
        slot.push.apply(slot, child.children || []);
      } else {
        slot.push(child);
      }
    } else {
      // fixed by xxxxxx 临时 hack 掉 uni-app 中的异步 name slot page
      if(child.asyncMeta && child.asyncMeta.data && child.asyncMeta.data.slot === 'page'){
        (slots['page'] || (slots['page'] = [])).push(child);
      }else{
        (slots.default || (slots.default = [])).push(child);
      }
    }
  }
  // ignore slots that contains only whitespace
  for (var name$1 in slots) {
    if (slots[name$1].every(isWhitespace)) {
      delete slots[name$1];
    }
  }
  return slots
}

function isWhitespace (node) {
  return (node.isComment && !node.asyncFactory) || node.text === ' '
}

/*  */

function normalizeScopedSlots (
  slots,
  normalSlots,
  prevSlots
) {
  var res;
  var hasNormalSlots = Object.keys(normalSlots).length > 0;
  var isStable = slots ? !!slots.$stable : !hasNormalSlots;
  var key = slots && slots.$key;
  if (!slots) {
    res = {};
  } else if (slots._normalized) {
    // fast path 1: child component re-render only, parent did not change
    return slots._normalized
  } else if (
    isStable &&
    prevSlots &&
    prevSlots !== emptyObject &&
    key === prevSlots.$key &&
    !hasNormalSlots &&
    !prevSlots.$hasNormal
  ) {
    // fast path 2: stable scoped slots w/ no normal slots to proxy,
    // only need to normalize once
    return prevSlots
  } else {
    res = {};
    for (var key$1 in slots) {
      if (slots[key$1] && key$1[0] !== '$') {
        res[key$1] = normalizeScopedSlot(normalSlots, key$1, slots[key$1]);
      }
    }
  }
  // expose normal slots on scopedSlots
  for (var key$2 in normalSlots) {
    if (!(key$2 in res)) {
      res[key$2] = proxyNormalSlot(normalSlots, key$2);
    }
  }
  // avoriaz seems to mock a non-extensible $scopedSlots object
  // and when that is passed down this would cause an error
  if (slots && Object.isExtensible(slots)) {
    (slots)._normalized = res;
  }
  def(res, '$stable', isStable);
  def(res, '$key', key);
  def(res, '$hasNormal', hasNormalSlots);
  return res
}

function normalizeScopedSlot(normalSlots, key, fn) {
  var normalized = function () {
    var res = arguments.length ? fn.apply(null, arguments) : fn({});
    res = res && typeof res === 'object' && !Array.isArray(res)
      ? [res] // single vnode
      : normalizeChildren(res);
    return res && (
      res.length === 0 ||
      (res.length === 1 && res[0].isComment) // #9658
    ) ? undefined
      : res
  };
  // this is a slot using the new v-slot syntax without scope. although it is
  // compiled as a scoped slot, render fn users would expect it to be present
  // on this.$slots because the usage is semantically a normal slot.
  if (fn.proxy) {
    Object.defineProperty(normalSlots, key, {
      get: normalized,
      enumerable: true,
      configurable: true
    });
  }
  return normalized
}

function proxyNormalSlot(slots, key) {
  return function () { return slots[key]; }
}

/*  */

/**
 * Runtime helper for rendering v-for lists.
 */
function renderList (
  val,
  render
) {
  var ret, i, l, keys, key;
  if (Array.isArray(val) || typeof val === 'string') {
    ret = new Array(val.length);
    for (i = 0, l = val.length; i < l; i++) {
      ret[i] = render(val[i], i, i, i); // fixed by xxxxxx
    }
  } else if (typeof val === 'number') {
    ret = new Array(val);
    for (i = 0; i < val; i++) {
      ret[i] = render(i + 1, i, i, i); // fixed by xxxxxx
    }
  } else if (isObject(val)) {
    if (hasSymbol && val[Symbol.iterator]) {
      ret = [];
      var iterator = val[Symbol.iterator]();
      var result = iterator.next();
      while (!result.done) {
        ret.push(render(result.value, ret.length, i, i++)); // fixed by xxxxxx
        result = iterator.next();
      }
    } else {
      keys = Object.keys(val);
      ret = new Array(keys.length);
      for (i = 0, l = keys.length; i < l; i++) {
        key = keys[i];
        ret[i] = render(val[key], key, i, i); // fixed by xxxxxx
      }
    }
  }
  if (!isDef(ret)) {
    ret = [];
  }
  (ret)._isVList = true;
  return ret
}

/*  */

/**
 * Runtime helper for rendering <slot>
 */
function renderSlot (
  name,
  fallback,
  props,
  bindObject
) {
  var scopedSlotFn = this.$scopedSlots[name];
  var nodes;
  if (scopedSlotFn) { // scoped slot
    props = props || {};
    if (bindObject) {
      if ( true && !isObject(bindObject)) {
        warn(
          'slot v-bind without argument expects an Object',
          this
        );
      }
      props = extend(extend({}, bindObject), props);
    }
    // fixed by xxxxxx app-plus scopedSlot
    nodes = scopedSlotFn(props, this, props._i) || fallback;
  } else {
    nodes = this.$slots[name] || fallback;
  }

  var target = props && props.slot;
  if (target) {
    return this.$createElement('template', { slot: target }, nodes)
  } else {
    return nodes
  }
}

/*  */

/**
 * Runtime helper for resolving filters
 */
function resolveFilter (id) {
  return resolveAsset(this.$options, 'filters', id, true) || identity
}

/*  */

function isKeyNotMatch (expect, actual) {
  if (Array.isArray(expect)) {
    return expect.indexOf(actual) === -1
  } else {
    return expect !== actual
  }
}

/**
 * Runtime helper for checking keyCodes from config.
 * exposed as Vue.prototype._k
 * passing in eventKeyName as last argument separately for backwards compat
 */
function checkKeyCodes (
  eventKeyCode,
  key,
  builtInKeyCode,
  eventKeyName,
  builtInKeyName
) {
  var mappedKeyCode = config.keyCodes[key] || builtInKeyCode;
  if (builtInKeyName && eventKeyName && !config.keyCodes[key]) {
    return isKeyNotMatch(builtInKeyName, eventKeyName)
  } else if (mappedKeyCode) {
    return isKeyNotMatch(mappedKeyCode, eventKeyCode)
  } else if (eventKeyName) {
    return hyphenate(eventKeyName) !== key
  }
}

/*  */

/**
 * Runtime helper for merging v-bind="object" into a VNode's data.
 */
function bindObjectProps (
  data,
  tag,
  value,
  asProp,
  isSync
) {
  if (value) {
    if (!isObject(value)) {
       true && warn(
        'v-bind without argument expects an Object or Array value',
        this
      );
    } else {
      if (Array.isArray(value)) {
        value = toObject(value);
      }
      var hash;
      var loop = function ( key ) {
        if (
          key === 'class' ||
          key === 'style' ||
          isReservedAttribute(key)
        ) {
          hash = data;
        } else {
          var type = data.attrs && data.attrs.type;
          hash = asProp || config.mustUseProp(tag, type, key)
            ? data.domProps || (data.domProps = {})
            : data.attrs || (data.attrs = {});
        }
        var camelizedKey = camelize(key);
        var hyphenatedKey = hyphenate(key);
        if (!(camelizedKey in hash) && !(hyphenatedKey in hash)) {
          hash[key] = value[key];

          if (isSync) {
            var on = data.on || (data.on = {});
            on[("update:" + key)] = function ($event) {
              value[key] = $event;
            };
          }
        }
      };

      for (var key in value) loop( key );
    }
  }
  return data
}

/*  */

/**
 * Runtime helper for rendering static trees.
 */
function renderStatic (
  index,
  isInFor
) {
  var cached = this._staticTrees || (this._staticTrees = []);
  var tree = cached[index];
  // if has already-rendered static tree and not inside v-for,
  // we can reuse the same tree.
  if (tree && !isInFor) {
    return tree
  }
  // otherwise, render a fresh tree.
  tree = cached[index] = this.$options.staticRenderFns[index].call(
    this._renderProxy,
    null,
    this // for render fns generated for functional component templates
  );
  markStatic(tree, ("__static__" + index), false);
  return tree
}

/**
 * Runtime helper for v-once.
 * Effectively it means marking the node as static with a unique key.
 */
function markOnce (
  tree,
  index,
  key
) {
  markStatic(tree, ("__once__" + index + (key ? ("_" + key) : "")), true);
  return tree
}

function markStatic (
  tree,
  key,
  isOnce
) {
  if (Array.isArray(tree)) {
    for (var i = 0; i < tree.length; i++) {
      if (tree[i] && typeof tree[i] !== 'string') {
        markStaticNode(tree[i], (key + "_" + i), isOnce);
      }
    }
  } else {
    markStaticNode(tree, key, isOnce);
  }
}

function markStaticNode (node, key, isOnce) {
  node.isStatic = true;
  node.key = key;
  node.isOnce = isOnce;
}

/*  */

function bindObjectListeners (data, value) {
  if (value) {
    if (!isPlainObject(value)) {
       true && warn(
        'v-on without argument expects an Object value',
        this
      );
    } else {
      var on = data.on = data.on ? extend({}, data.on) : {};
      for (var key in value) {
        var existing = on[key];
        var ours = value[key];
        on[key] = existing ? [].concat(existing, ours) : ours;
      }
    }
  }
  return data
}

/*  */

function resolveScopedSlots (
  fns, // see flow/vnode
  res,
  // the following are added in 2.6
  hasDynamicKeys,
  contentHashKey
) {
  res = res || { $stable: !hasDynamicKeys };
  for (var i = 0; i < fns.length; i++) {
    var slot = fns[i];
    if (Array.isArray(slot)) {
      resolveScopedSlots(slot, res, hasDynamicKeys);
    } else if (slot) {
      // marker for reverse proxying v-slot without scope on this.$slots
      if (slot.proxy) {
        slot.fn.proxy = true;
      }
      res[slot.key] = slot.fn;
    }
  }
  if (contentHashKey) {
    (res).$key = contentHashKey;
  }
  return res
}

/*  */

function bindDynamicKeys (baseObj, values) {
  for (var i = 0; i < values.length; i += 2) {
    var key = values[i];
    if (typeof key === 'string' && key) {
      baseObj[values[i]] = values[i + 1];
    } else if ( true && key !== '' && key !== null) {
      // null is a special value for explicitly removing a binding
      warn(
        ("Invalid value for dynamic directive argument (expected string or null): " + key),
        this
      );
    }
  }
  return baseObj
}

// helper to dynamically append modifier runtime markers to event names.
// ensure only append when value is already string, otherwise it will be cast
// to string and cause the type check to miss.
function prependModifier (value, symbol) {
  return typeof value === 'string' ? symbol + value : value
}

/*  */

function installRenderHelpers (target) {
  target._o = markOnce;
  target._n = toNumber;
  target._s = toString;
  target._l = renderList;
  target._t = renderSlot;
  target._q = looseEqual;
  target._i = looseIndexOf;
  target._m = renderStatic;
  target._f = resolveFilter;
  target._k = checkKeyCodes;
  target._b = bindObjectProps;
  target._v = createTextVNode;
  target._e = createEmptyVNode;
  target._u = resolveScopedSlots;
  target._g = bindObjectListeners;
  target._d = bindDynamicKeys;
  target._p = prependModifier;
}

/*  */

function FunctionalRenderContext (
  data,
  props,
  children,
  parent,
  Ctor
) {
  var this$1 = this;

  var options = Ctor.options;
  // ensure the createElement function in functional components
  // gets a unique context - this is necessary for correct named slot check
  var contextVm;
  if (hasOwn(parent, '_uid')) {
    contextVm = Object.create(parent);
    // $flow-disable-line
    contextVm._original = parent;
  } else {
    // the context vm passed in is a functional context as well.
    // in this case we want to make sure we are able to get a hold to the
    // real context instance.
    contextVm = parent;
    // $flow-disable-line
    parent = parent._original;
  }
  var isCompiled = isTrue(options._compiled);
  var needNormalization = !isCompiled;

  this.data = data;
  this.props = props;
  this.children = children;
  this.parent = parent;
  this.listeners = data.on || emptyObject;
  this.injections = resolveInject(options.inject, parent);
  this.slots = function () {
    if (!this$1.$slots) {
      normalizeScopedSlots(
        data.scopedSlots,
        this$1.$slots = resolveSlots(children, parent)
      );
    }
    return this$1.$slots
  };

  Object.defineProperty(this, 'scopedSlots', ({
    enumerable: true,
    get: function get () {
      return normalizeScopedSlots(data.scopedSlots, this.slots())
    }
  }));

  // support for compiled functional template
  if (isCompiled) {
    // exposing $options for renderStatic()
    this.$options = options;
    // pre-resolve slots for renderSlot()
    this.$slots = this.slots();
    this.$scopedSlots = normalizeScopedSlots(data.scopedSlots, this.$slots);
  }

  if (options._scopeId) {
    this._c = function (a, b, c, d) {
      var vnode = createElement(contextVm, a, b, c, d, needNormalization);
      if (vnode && !Array.isArray(vnode)) {
        vnode.fnScopeId = options._scopeId;
        vnode.fnContext = parent;
      }
      return vnode
    };
  } else {
    this._c = function (a, b, c, d) { return createElement(contextVm, a, b, c, d, needNormalization); };
  }
}

installRenderHelpers(FunctionalRenderContext.prototype);

function createFunctionalComponent (
  Ctor,
  propsData,
  data,
  contextVm,
  children
) {
  var options = Ctor.options;
  var props = {};
  var propOptions = options.props;
  if (isDef(propOptions)) {
    for (var key in propOptions) {
      props[key] = validateProp(key, propOptions, propsData || emptyObject);
    }
  } else {
    if (isDef(data.attrs)) { mergeProps(props, data.attrs); }
    if (isDef(data.props)) { mergeProps(props, data.props); }
  }

  var renderContext = new FunctionalRenderContext(
    data,
    props,
    children,
    contextVm,
    Ctor
  );

  var vnode = options.render.call(null, renderContext._c, renderContext);

  if (vnode instanceof VNode) {
    return cloneAndMarkFunctionalResult(vnode, data, renderContext.parent, options, renderContext)
  } else if (Array.isArray(vnode)) {
    var vnodes = normalizeChildren(vnode) || [];
    var res = new Array(vnodes.length);
    for (var i = 0; i < vnodes.length; i++) {
      res[i] = cloneAndMarkFunctionalResult(vnodes[i], data, renderContext.parent, options, renderContext);
    }
    return res
  }
}

function cloneAndMarkFunctionalResult (vnode, data, contextVm, options, renderContext) {
  // #7817 clone node before setting fnContext, otherwise if the node is reused
  // (e.g. it was from a cached normal slot) the fnContext causes named slots
  // that should not be matched to match.
  var clone = cloneVNode(vnode);
  clone.fnContext = contextVm;
  clone.fnOptions = options;
  if (true) {
    (clone.devtoolsMeta = clone.devtoolsMeta || {}).renderContext = renderContext;
  }
  if (data.slot) {
    (clone.data || (clone.data = {})).slot = data.slot;
  }
  return clone
}

function mergeProps (to, from) {
  for (var key in from) {
    to[camelize(key)] = from[key];
  }
}

/*  */

/*  */

/*  */

/*  */

// inline hooks to be invoked on component VNodes during patch
var componentVNodeHooks = {
  init: function init (vnode, hydrating) {
    if (
      vnode.componentInstance &&
      !vnode.componentInstance._isDestroyed &&
      vnode.data.keepAlive
    ) {
      // kept-alive components, treat as a patch
      var mountedNode = vnode; // work around flow
      componentVNodeHooks.prepatch(mountedNode, mountedNode);
    } else {
      var child = vnode.componentInstance = createComponentInstanceForVnode(
        vnode,
        activeInstance
      );
      child.$mount(hydrating ? vnode.elm : undefined, hydrating);
    }
  },

  prepatch: function prepatch (oldVnode, vnode) {
    var options = vnode.componentOptions;
    var child = vnode.componentInstance = oldVnode.componentInstance;
    updateChildComponent(
      child,
      options.propsData, // updated props
      options.listeners, // updated listeners
      vnode, // new parent vnode
      options.children // new children
    );
  },

  insert: function insert (vnode) {
    var context = vnode.context;
    var componentInstance = vnode.componentInstance;
    if (!componentInstance._isMounted) {
      callHook(componentInstance, 'onServiceCreated');
      callHook(componentInstance, 'onServiceAttached');
      componentInstance._isMounted = true;
      callHook(componentInstance, 'mounted');
    }
    if (vnode.data.keepAlive) {
      if (context._isMounted) {
        // vue-router#1212
        // During updates, a kept-alive component's child components may
        // change, so directly walking the tree here may call activated hooks
        // on incorrect children. Instead we push them into a queue which will
        // be processed after the whole patch process ended.
        queueActivatedComponent(componentInstance);
      } else {
        activateChildComponent(componentInstance, true /* direct */);
      }
    }
  },

  destroy: function destroy (vnode) {
    var componentInstance = vnode.componentInstance;
    if (!componentInstance._isDestroyed) {
      if (!vnode.data.keepAlive) {
        componentInstance.$destroy();
      } else {
        deactivateChildComponent(componentInstance, true /* direct */);
      }
    }
  }
};

var hooksToMerge = Object.keys(componentVNodeHooks);

function createComponent (
  Ctor,
  data,
  context,
  children,
  tag
) {
  if (isUndef(Ctor)) {
    return
  }

  var baseCtor = context.$options._base;

  // plain options object: turn it into a constructor
  if (isObject(Ctor)) {
    Ctor = baseCtor.extend(Ctor);
  }

  // if at this stage it's not a constructor or an async component factory,
  // reject.
  if (typeof Ctor !== 'function') {
    if (true) {
      warn(("Invalid Component definition: " + (String(Ctor))), context);
    }
    return
  }

  // async component
  var asyncFactory;
  if (isUndef(Ctor.cid)) {
    asyncFactory = Ctor;
    Ctor = resolveAsyncComponent(asyncFactory, baseCtor);
    if (Ctor === undefined) {
      // return a placeholder node for async component, which is rendered
      // as a comment node but preserves all the raw information for the node.
      // the information will be used for async server-rendering and hydration.
      return createAsyncPlaceholder(
        asyncFactory,
        data,
        context,
        children,
        tag
      )
    }
  }

  data = data || {};

  // resolve constructor options in case global mixins are applied after
  // component constructor creation
  resolveConstructorOptions(Ctor);

  // transform component v-model data into props & events
  if (isDef(data.model)) {
    transformModel(Ctor.options, data);
  }

  // extract props
  var propsData = extractPropsFromVNodeData(data, Ctor, tag, context); // fixed by xxxxxx

  // functional component
  if (isTrue(Ctor.options.functional)) {
    return createFunctionalComponent(Ctor, propsData, data, context, children)
  }

  // extract listeners, since these needs to be treated as
  // child component listeners instead of DOM listeners
  var listeners = data.on;
  // replace with listeners with .native modifier
  // so it gets processed during parent component patch.
  data.on = data.nativeOn;

  if (isTrue(Ctor.options.abstract)) {
    // abstract components do not keep anything
    // other than props & listeners & slot

    // work around flow
    var slot = data.slot;
    data = {};
    if (slot) {
      data.slot = slot;
    }
  }

  // install component management hooks onto the placeholder node
  installComponentHooks(data);

  // return a placeholder vnode
  var name = Ctor.options.name || tag;
  var vnode = new VNode(
    ("vue-component-" + (Ctor.cid) + (name ? ("-" + name) : '')),
    data, undefined, undefined, undefined, context,
    { Ctor: Ctor, propsData: propsData, listeners: listeners, tag: tag, children: children },
    asyncFactory
  );

  return vnode
}

function createComponentInstanceForVnode (
  vnode, // we know it's MountedComponentVNode but flow doesn't
  parent // activeInstance in lifecycle state
) {
  var options = {
    _isComponent: true,
    _parentVnode: vnode,
    parent: parent
  };
  // check inline-template render functions
  var inlineTemplate = vnode.data.inlineTemplate;
  if (isDef(inlineTemplate)) {
    options.render = inlineTemplate.render;
    options.staticRenderFns = inlineTemplate.staticRenderFns;
  }
  return new vnode.componentOptions.Ctor(options)
}

function installComponentHooks (data) {
  var hooks = data.hook || (data.hook = {});
  for (var i = 0; i < hooksToMerge.length; i++) {
    var key = hooksToMerge[i];
    var existing = hooks[key];
    var toMerge = componentVNodeHooks[key];
    if (existing !== toMerge && !(existing && existing._merged)) {
      hooks[key] = existing ? mergeHook$1(toMerge, existing) : toMerge;
    }
  }
}

function mergeHook$1 (f1, f2) {
  var merged = function (a, b) {
    // flow complains about extra args which is why we use any
    f1(a, b);
    f2(a, b);
  };
  merged._merged = true;
  return merged
}

// transform component v-model info (value and callback) into
// prop and event handler respectively.
function transformModel (options, data) {
  var prop = (options.model && options.model.prop) || 'value';
  var event = (options.model && options.model.event) || 'input'
  ;(data.attrs || (data.attrs = {}))[prop] = data.model.value;
  var on = data.on || (data.on = {});
  var existing = on[event];
  var callback = data.model.callback;
  if (isDef(existing)) {
    if (
      Array.isArray(existing)
        ? existing.indexOf(callback) === -1
        : existing !== callback
    ) {
      on[event] = [callback].concat(existing);
    }
  } else {
    on[event] = callback;
  }
}

/*  */

var SIMPLE_NORMALIZE = 1;
var ALWAYS_NORMALIZE = 2;

// wrapper function for providing a more flexible interface
// without getting yelled at by flow
function createElement (
  context,
  tag,
  data,
  children,
  normalizationType,
  alwaysNormalize
) {
  if (Array.isArray(data) || isPrimitive(data)) {
    normalizationType = children;
    children = data;
    data = undefined;
  }
  if (isTrue(alwaysNormalize)) {
    normalizationType = ALWAYS_NORMALIZE;
  }
  return _createElement(context, tag, data, children, normalizationType)
}

function _createElement (
  context,
  tag,
  data,
  children,
  normalizationType
) {
  if (isDef(data) && isDef((data).__ob__)) {
     true && warn(
      "Avoid using observed data object as vnode data: " + (JSON.stringify(data)) + "\n" +
      'Always create fresh vnode data objects in each render!',
      context
    );
    return createEmptyVNode()
  }
  // object syntax in v-bind
  if (isDef(data) && isDef(data.is)) {
    tag = data.is;
  }
  if (!tag) {
    // in case of component :is set to falsy value
    return createEmptyVNode()
  }
  // warn against non-primitive key
  if ( true &&
    isDef(data) && isDef(data.key) && !isPrimitive(data.key)
  ) {
    {
      warn(
        'Avoid using non-primitive value as key, ' +
        'use string/number value instead.',
        context
      );
    }
  }
  // support single function children as default scoped slot
  if (Array.isArray(children) &&
    typeof children[0] === 'function'
  ) {
    data = data || {};
    data.scopedSlots = { default: children[0] };
    children.length = 0;
  }
  if (normalizationType === ALWAYS_NORMALIZE) {
    children = normalizeChildren(children);
  } else if (normalizationType === SIMPLE_NORMALIZE) {
    children = simpleNormalizeChildren(children);
  }
  var vnode, ns;
  if (typeof tag === 'string') {
    var Ctor;
    ns = (context.$vnode && context.$vnode.ns) || config.getTagNamespace(tag);
    if (config.isReservedTag(tag)) {
      // platform built-in elements
      if ( true && isDef(data) && isDef(data.nativeOn)) {
        warn(
          ("The .native modifier for v-on is only valid on components but it was used on <" + tag + ">."),
          context
        );
      }
      vnode = new VNode(
        config.parsePlatformTagName(tag), data, children,
        undefined, undefined, context
      );
    } else if ((!data || !data.pre) && isDef(Ctor = resolveAsset(context.$options, 'components', tag))) {
      // component
      vnode = createComponent(Ctor, data, context, children, tag);
    } else {
      // unknown or unlisted namespaced elements
      // check at runtime because it may get assigned a namespace when its
      // parent normalizes children
      vnode = new VNode(
        tag, data, children,
        undefined, undefined, context
      );
    }
  } else {
    // direct component options / constructor
    vnode = createComponent(tag, data, context, children);
  }
  if (Array.isArray(vnode)) {
    return vnode
  } else if (isDef(vnode)) {
    if (isDef(ns)) { applyNS(vnode, ns); }
    if (isDef(data)) { registerDeepBindings(data); }
    return vnode
  } else {
    return createEmptyVNode()
  }
}

function applyNS (vnode, ns, force) {
  vnode.ns = ns;
  if (vnode.tag === 'foreignObject') {
    // use default namespace inside foreignObject
    ns = undefined;
    force = true;
  }
  if (isDef(vnode.children)) {
    for (var i = 0, l = vnode.children.length; i < l; i++) {
      var child = vnode.children[i];
      if (isDef(child.tag) && (
        isUndef(child.ns) || (isTrue(force) && child.tag !== 'svg'))) {
        applyNS(child, ns, force);
      }
    }
  }
}

// ref #5318
// necessary to ensure parent re-render when deep bindings like :style and
// :class are used on slot nodes
function registerDeepBindings (data) {
  if (isObject(data.style)) {
    traverse(data.style);
  }
  if (isObject(data.class)) {
    traverse(data.class);
  }
}

/*  */

function initRender (vm) {
  vm._vnode = null; // the root of the child tree
  vm._staticTrees = null; // v-once cached trees
  var options = vm.$options;
  var parentVnode = vm.$vnode = options._parentVnode; // the placeholder node in parent tree
  var renderContext = parentVnode && parentVnode.context;
  vm.$slots = resolveSlots(options._renderChildren, renderContext);
  vm.$scopedSlots = emptyObject;
  // bind the createElement fn to this instance
  // so that we get proper render context inside it.
  // args order: tag, data, children, normalizationType, alwaysNormalize
  // internal version is used by render functions compiled from templates
  vm._c = function (a, b, c, d) { return createElement(vm, a, b, c, d, false); };
  // normalization is always applied for the public version, used in
  // user-written render functions.
  vm.$createElement = function (a, b, c, d) { return createElement(vm, a, b, c, d, true); };

  // $attrs & $listeners are exposed for easier HOC creation.
  // they need to be reactive so that HOCs using them are always updated
  var parentData = parentVnode && parentVnode.data;

  /* istanbul ignore else */
  if (true) {
    defineReactive$$1(vm, '$attrs', parentData && parentData.attrs || emptyObject, function () {
      !isUpdatingChildComponent && warn("$attrs is readonly.", vm);
    }, true);
    defineReactive$$1(vm, '$listeners', options._parentListeners || emptyObject, function () {
      !isUpdatingChildComponent && warn("$listeners is readonly.", vm);
    }, true);
  } else {}
}

var currentRenderingInstance = null;

function renderMixin (Vue) {
  // install runtime convenience helpers
  installRenderHelpers(Vue.prototype);

  Vue.prototype.$nextTick = function (fn) {
    return nextTick(fn, this)
  };

  Vue.prototype._render = function () {
    var vm = this;
    var ref = vm.$options;
    var render = ref.render;
    var _parentVnode = ref._parentVnode;

    if (_parentVnode) {
      vm.$scopedSlots = normalizeScopedSlots(
        _parentVnode.data.scopedSlots,
        vm.$slots,
        vm.$scopedSlots
      );
    }

    // set parent vnode. this allows render functions to have access
    // to the data on the placeholder node.
    vm.$vnode = _parentVnode;
    // render self
    var vnode;
    try {
      // There's no need to maintain a stack because all render fns are called
      // separately from one another. Nested component's render fns are called
      // when parent component is patched.
      currentRenderingInstance = vm;
      vnode = render.call(vm._renderProxy, vm.$createElement);
    } catch (e) {
      handleError(e, vm, "render");
      // return error render result,
      // or previous vnode to prevent render error causing blank component
      /* istanbul ignore else */
      if ( true && vm.$options.renderError) {
        try {
          vnode = vm.$options.renderError.call(vm._renderProxy, vm.$createElement, e);
        } catch (e) {
          handleError(e, vm, "renderError");
          vnode = vm._vnode;
        }
      } else {
        vnode = vm._vnode;
      }
    } finally {
      currentRenderingInstance = null;
    }
    // if the returned array contains only a single node, allow it
    if (Array.isArray(vnode) && vnode.length === 1) {
      vnode = vnode[0];
    }
    // return empty vnode in case the render function errored out
    if (!(vnode instanceof VNode)) {
      if ( true && Array.isArray(vnode)) {
        warn(
          'Multiple root nodes returned from render function. Render function ' +
          'should return a single root node.',
          vm
        );
      }
      vnode = createEmptyVNode();
    }
    // set parent
    vnode.parent = _parentVnode;
    return vnode
  };
}

/*  */

function ensureCtor (comp, base) {
  if (
    comp.__esModule ||
    (hasSymbol && comp[Symbol.toStringTag] === 'Module')
  ) {
    comp = comp.default;
  }
  return isObject(comp)
    ? base.extend(comp)
    : comp
}

function createAsyncPlaceholder (
  factory,
  data,
  context,
  children,
  tag
) {
  var node = createEmptyVNode();
  node.asyncFactory = factory;
  node.asyncMeta = { data: data, context: context, children: children, tag: tag };
  return node
}

function resolveAsyncComponent (
  factory,
  baseCtor
) {
  if (isTrue(factory.error) && isDef(factory.errorComp)) {
    return factory.errorComp
  }

  if (isDef(factory.resolved)) {
    return factory.resolved
  }

  var owner = currentRenderingInstance;
  if (owner && isDef(factory.owners) && factory.owners.indexOf(owner) === -1) {
    // already pending
    factory.owners.push(owner);
  }

  if (isTrue(factory.loading) && isDef(factory.loadingComp)) {
    return factory.loadingComp
  }

  if (owner && !isDef(factory.owners)) {
    var owners = factory.owners = [owner];
    var sync = true;
    var timerLoading = null;
    var timerTimeout = null

    ;(owner).$on('hook:destroyed', function () { return remove(owners, owner); });

    var forceRender = function (renderCompleted) {
      for (var i = 0, l = owners.length; i < l; i++) {
        (owners[i]).$forceUpdate();
      }

      if (renderCompleted) {
        owners.length = 0;
        if (timerLoading !== null) {
          clearTimeout(timerLoading);
          timerLoading = null;
        }
        if (timerTimeout !== null) {
          clearTimeout(timerTimeout);
          timerTimeout = null;
        }
      }
    };

    var resolve = once(function (res) {
      // cache resolved
      factory.resolved = ensureCtor(res, baseCtor);
      // invoke callbacks only if this is not a synchronous resolve
      // (async resolves are shimmed as synchronous during SSR)
      if (!sync) {
        forceRender(true);
      } else {
        owners.length = 0;
      }
    });

    var reject = once(function (reason) {
       true && warn(
        "Failed to resolve async component: " + (String(factory)) +
        (reason ? ("\nReason: " + reason) : '')
      );
      if (isDef(factory.errorComp)) {
        factory.error = true;
        forceRender(true);
      }
    });

    var res = factory(resolve, reject);

    if (isObject(res)) {
      if (isPromise(res)) {
        // () => Promise
        if (isUndef(factory.resolved)) {
          res.then(resolve, reject);
        }
      } else if (isPromise(res.component)) {
        res.component.then(resolve, reject);

        if (isDef(res.error)) {
          factory.errorComp = ensureCtor(res.error, baseCtor);
        }

        if (isDef(res.loading)) {
          factory.loadingComp = ensureCtor(res.loading, baseCtor);
          if (res.delay === 0) {
            factory.loading = true;
          } else {
            timerLoading = setTimeout(function () {
              timerLoading = null;
              if (isUndef(factory.resolved) && isUndef(factory.error)) {
                factory.loading = true;
                forceRender(false);
              }
            }, res.delay || 200);
          }
        }

        if (isDef(res.timeout)) {
          timerTimeout = setTimeout(function () {
            timerTimeout = null;
            if (isUndef(factory.resolved)) {
              reject(
                 true
                  ? ("timeout (" + (res.timeout) + "ms)")
                  : undefined
              );
            }
          }, res.timeout);
        }
      }
    }

    sync = false;
    // return in case resolved synchronously
    return factory.loading
      ? factory.loadingComp
      : factory.resolved
  }
}

/*  */

function isAsyncPlaceholder (node) {
  return node.isComment && node.asyncFactory
}

/*  */

function getFirstComponentChild (children) {
  if (Array.isArray(children)) {
    for (var i = 0; i < children.length; i++) {
      var c = children[i];
      if (isDef(c) && (isDef(c.componentOptions) || isAsyncPlaceholder(c))) {
        return c
      }
    }
  }
}

/*  */

/*  */

function initEvents (vm) {
  vm._events = Object.create(null);
  vm._hasHookEvent = false;
  // init parent attached events
  var listeners = vm.$options._parentListeners;
  if (listeners) {
    updateComponentListeners(vm, listeners);
  }
}

var target;

function add (event, fn) {
  target.$on(event, fn);
}

function remove$1 (event, fn) {
  target.$off(event, fn);
}

function createOnceHandler (event, fn) {
  var _target = target;
  return function onceHandler () {
    var res = fn.apply(null, arguments);
    if (res !== null) {
      _target.$off(event, onceHandler);
    }
  }
}

function updateComponentListeners (
  vm,
  listeners,
  oldListeners
) {
  target = vm;
  updateListeners(listeners, oldListeners || {}, add, remove$1, createOnceHandler, vm);
  target = undefined;
}

function eventsMixin (Vue) {
  var hookRE = /^hook:/;
  Vue.prototype.$on = function (event, fn) {
    var vm = this;
    if (Array.isArray(event)) {
      for (var i = 0, l = event.length; i < l; i++) {
        vm.$on(event[i], fn);
      }
    } else {
      (vm._events[event] || (vm._events[event] = [])).push(fn);
      // optimize hook:event cost by using a boolean flag marked at registration
      // instead of a hash lookup
      if (hookRE.test(event)) {
        vm._hasHookEvent = true;
      }
    }
    return vm
  };

  Vue.prototype.$once = function (event, fn) {
    var vm = this;
    function on () {
      vm.$off(event, on);
      fn.apply(vm, arguments);
    }
    on.fn = fn;
    vm.$on(event, on);
    return vm
  };

  Vue.prototype.$off = function (event, fn) {
    var vm = this;
    // all
    if (!arguments.length) {
      vm._events = Object.create(null);
      return vm
    }
    // array of events
    if (Array.isArray(event)) {
      for (var i$1 = 0, l = event.length; i$1 < l; i$1++) {
        vm.$off(event[i$1], fn);
      }
      return vm
    }
    // specific event
    var cbs = vm._events[event];
    if (!cbs) {
      return vm
    }
    if (!fn) {
      vm._events[event] = null;
      return vm
    }
    // specific handler
    var cb;
    var i = cbs.length;
    while (i--) {
      cb = cbs[i];
      if (cb === fn || cb.fn === fn) {
        cbs.splice(i, 1);
        break
      }
    }
    return vm
  };

  Vue.prototype.$emit = function (event) {
    var vm = this;
    if (true) {
      var lowerCaseEvent = event.toLowerCase();
      if (lowerCaseEvent !== event && vm._events[lowerCaseEvent]) {
        tip(
          "Event \"" + lowerCaseEvent + "\" is emitted in component " +
          (formatComponentName(vm)) + " but the handler is registered for \"" + event + "\". " +
          "Note that HTML attributes are case-insensitive and you cannot use " +
          "v-on to listen to camelCase events when using in-DOM templates. " +
          "You should probably use \"" + (hyphenate(event)) + "\" instead of \"" + event + "\"."
        );
      }
    }
    var cbs = vm._events[event];
    if (cbs) {
      cbs = cbs.length > 1 ? toArray(cbs) : cbs;
      var args = toArray(arguments, 1);
      var info = "event handler for \"" + event + "\"";
      for (var i = 0, l = cbs.length; i < l; i++) {
        invokeWithErrorHandling(cbs[i], vm, args, vm, info);
      }
    }
    return vm
  };
}

/*  */

var activeInstance = null;
var isUpdatingChildComponent = false;

function setActiveInstance(vm) {
  var prevActiveInstance = activeInstance;
  activeInstance = vm;
  return function () {
    activeInstance = prevActiveInstance;
  }
}

function initLifecycle (vm) {
  var options = vm.$options;

  // locate first non-abstract parent
  var parent = options.parent;
  if (parent && !options.abstract) {
    while (parent.$options.abstract && parent.$parent) {
      parent = parent.$parent;
    }
    parent.$children.push(vm);
  }

  vm.$parent = parent;
  vm.$root = parent ? parent.$root : vm;

  vm.$children = [];
  vm.$refs = {};

  vm._watcher = null;
  vm._inactive = null;
  vm._directInactive = false;
  vm._isMounted = false;
  vm._isDestroyed = false;
  vm._isBeingDestroyed = false;
}

function lifecycleMixin (Vue) {
  Vue.prototype._update = function (vnode, hydrating) {
    var vm = this;
    var prevEl = vm.$el;
    var prevVnode = vm._vnode;
    var restoreActiveInstance = setActiveInstance(vm);
    vm._vnode = vnode;
    // Vue.prototype.__patch__ is injected in entry points
    // based on the rendering backend used.
    if (!prevVnode) {
      // initial render
      vm.$el = vm.__patch__(vm.$el, vnode, hydrating, false /* removeOnly */);
    } else {
      // updates
      vm.$el = vm.__patch__(prevVnode, vnode);
    }
    restoreActiveInstance();
    // update __vue__ reference
    if (prevEl) {
      prevEl.__vue__ = null;
    }
    if (vm.$el) {
      vm.$el.__vue__ = vm;
    }
    // if parent is an HOC, update its $el as well
    if (vm.$vnode && vm.$parent && vm.$vnode === vm.$parent._vnode) {
      vm.$parent.$el = vm.$el;
    }
    // updated hook is called by the scheduler to ensure that children are
    // updated in a parent's updated hook.
  };

  Vue.prototype.$forceUpdate = function () {
    var vm = this;
    if (vm._watcher) {
      vm._watcher.update();
    }
  };

  Vue.prototype.$destroy = function () {
    var vm = this;
    if (vm._isBeingDestroyed) {
      return
    }
    callHook(vm, 'beforeDestroy');
    vm._isBeingDestroyed = true;
    // remove self from parent
    var parent = vm.$parent;
    if (parent && !parent._isBeingDestroyed && !vm.$options.abstract) {
      remove(parent.$children, vm);
    }
    // teardown watchers
    if (vm._watcher) {
      vm._watcher.teardown();
    }
    var i = vm._watchers.length;
    while (i--) {
      vm._watchers[i].teardown();
    }
    // remove reference from data ob
    // frozen object may not have observer.
    if (vm._data.__ob__) {
      vm._data.__ob__.vmCount--;
    }
    // call the last hook...
    vm._isDestroyed = true;
    // invoke destroy hooks on current rendered tree
    vm.__patch__(vm._vnode, null);
    // fire destroyed hook
    callHook(vm, 'destroyed');
    // turn off all instance listeners.
    vm.$off();
    // remove __vue__ reference
    if (vm.$el) {
      vm.$el.__vue__ = null;
    }
    // release circular reference (#6759)
    if (vm.$vnode) {
      vm.$vnode.parent = null;
    }
  };
}

function updateChildComponent (
  vm,
  propsData,
  listeners,
  parentVnode,
  renderChildren
) {
  if (true) {
    isUpdatingChildComponent = true;
  }

  // determine whether component has slot children
  // we need to do this before overwriting $options._renderChildren.

  // check if there are dynamic scopedSlots (hand-written or compiled but with
  // dynamic slot names). Static scoped slots compiled from template has the
  // "$stable" marker.
  var newScopedSlots = parentVnode.data.scopedSlots;
  var oldScopedSlots = vm.$scopedSlots;
  var hasDynamicScopedSlot = !!(
    (newScopedSlots && !newScopedSlots.$stable) ||
    (oldScopedSlots !== emptyObject && !oldScopedSlots.$stable) ||
    (newScopedSlots && vm.$scopedSlots.$key !== newScopedSlots.$key)
  );

  // Any static slot children from the parent may have changed during parent's
  // update. Dynamic scoped slots may also have changed. In such cases, a forced
  // update is necessary to ensure correctness.
  var needsForceUpdate = !!(
    renderChildren ||               // has new static slots
    vm.$options._renderChildren ||  // has old static slots
    hasDynamicScopedSlot
  );

  vm.$options._parentVnode = parentVnode;
  vm.$vnode = parentVnode; // update vm's placeholder node without re-render

  if (vm._vnode) { // update child tree's parent
    vm._vnode.parent = parentVnode;
  }
  vm.$options._renderChildren = renderChildren;

  // update $attrs and $listeners hash
  // these are also reactive so they may trigger child update if the child
  // used them during render
  vm.$attrs = parentVnode.data.attrs || emptyObject;
  vm.$listeners = listeners || emptyObject;

  // update props
  if (propsData && vm.$options.props) {
    toggleObserving(false);
    var props = vm._props;
    var propKeys = vm.$options._propKeys || [];
    for (var i = 0; i < propKeys.length; i++) {
      var key = propKeys[i];
      var propOptions = vm.$options.props; // wtf flow?
      props[key] = validateProp(key, propOptions, propsData, vm);
    }
    toggleObserving(true);
    // keep a copy of raw propsData
    vm.$options.propsData = propsData;
  }
  
  // fixed by xxxxxx update properties(mp runtime)
  vm._$updateProperties && vm._$updateProperties(vm);
  
  // update listeners
  listeners = listeners || emptyObject;
  var oldListeners = vm.$options._parentListeners;
  vm.$options._parentListeners = listeners;
  updateComponentListeners(vm, listeners, oldListeners);

  // resolve slots + force update if has children
  if (needsForceUpdate) {
    vm.$slots = resolveSlots(renderChildren, parentVnode.context);
    vm.$forceUpdate();
  }

  if (true) {
    isUpdatingChildComponent = false;
  }
}

function isInInactiveTree (vm) {
  while (vm && (vm = vm.$parent)) {
    if (vm._inactive) { return true }
  }
  return false
}

function activateChildComponent (vm, direct) {
  if (direct) {
    vm._directInactive = false;
    if (isInInactiveTree(vm)) {
      return
    }
  } else if (vm._directInactive) {
    return
  }
  if (vm._inactive || vm._inactive === null) {
    vm._inactive = false;
    for (var i = 0; i < vm.$children.length; i++) {
      activateChildComponent(vm.$children[i]);
    }
    callHook(vm, 'activated');
  }
}

function deactivateChildComponent (vm, direct) {
  if (direct) {
    vm._directInactive = true;
    if (isInInactiveTree(vm)) {
      return
    }
  }
  if (!vm._inactive) {
    vm._inactive = true;
    for (var i = 0; i < vm.$children.length; i++) {
      deactivateChildComponent(vm.$children[i]);
    }
    callHook(vm, 'deactivated');
  }
}

function callHook (vm, hook) {
  // #7573 disable dep collection when invoking lifecycle hooks
  pushTarget();
  var handlers = vm.$options[hook];
  var info = hook + " hook";
  if (handlers) {
    for (var i = 0, j = handlers.length; i < j; i++) {
      invokeWithErrorHandling(handlers[i], vm, null, vm, info);
    }
  }
  if (vm._hasHookEvent) {
    vm.$emit('hook:' + hook);
  }
  popTarget();
}

/*  */

var MAX_UPDATE_COUNT = 100;

var queue = [];
var activatedChildren = [];
var has = {};
var circular = {};
var waiting = false;
var flushing = false;
var index = 0;

/**
 * Reset the scheduler's state.
 */
function resetSchedulerState () {
  index = queue.length = activatedChildren.length = 0;
  has = {};
  if (true) {
    circular = {};
  }
  waiting = flushing = false;
}

// Async edge case #6566 requires saving the timestamp when event listeners are
// attached. However, calling performance.now() has a perf overhead especially
// if the page has thousands of event listeners. Instead, we take a timestamp
// every time the scheduler flushes and use that for all event listeners
// attached during that flush.
var currentFlushTimestamp = 0;

// Async edge case fix requires storing an event listener's attach timestamp.
var getNow = Date.now;

// Determine what event timestamp the browser is using. Annoyingly, the
// timestamp can either be hi-res (relative to page load) or low-res
// (relative to UNIX epoch), so in order to compare time we have to use the
// same timestamp type when saving the flush timestamp.
// All IE versions use low-res event timestamps, and have problematic clock
// implementations (#9632)
if (inBrowser && !isIE) {
  var performance = window.performance;
  if (
    performance &&
    typeof performance.now === 'function' &&
    getNow() > document.createEvent('Event').timeStamp
  ) {
    // if the event timestamp, although evaluated AFTER the Date.now(), is
    // smaller than it, it means the event is using a hi-res timestamp,
    // and we need to use the hi-res version for event listener timestamps as
    // well.
    getNow = function () { return performance.now(); };
  }
}

/**
 * Flush both queues and run the watchers.
 */
function flushSchedulerQueue () {
  currentFlushTimestamp = getNow();
  flushing = true;
  var watcher, id;

  // Sort queue before flush.
  // This ensures that:
  // 1. Components are updated from parent to child. (because parent is always
  //    created before the child)
  // 2. A component's user watchers are run before its render watcher (because
  //    user watchers are created before the render watcher)
  // 3. If a component is destroyed during a parent component's watcher run,
  //    its watchers can be skipped.
  queue.sort(function (a, b) { return a.id - b.id; });

  // do not cache length because more watchers might be pushed
  // as we run existing watchers
  for (index = 0; index < queue.length; index++) {
    watcher = queue[index];
    if (watcher.before) {
      watcher.before();
    }
    id = watcher.id;
    has[id] = null;
    watcher.run();
    // in dev build, check and stop circular updates.
    if ( true && has[id] != null) {
      circular[id] = (circular[id] || 0) + 1;
      if (circular[id] > MAX_UPDATE_COUNT) {
        warn(
          'You may have an infinite update loop ' + (
            watcher.user
              ? ("in watcher with expression \"" + (watcher.expression) + "\"")
              : "in a component render function."
          ),
          watcher.vm
        );
        break
      }
    }
  }

  // keep copies of post queues before resetting state
  var activatedQueue = activatedChildren.slice();
  var updatedQueue = queue.slice();

  resetSchedulerState();

  // call component updated and activated hooks
  callActivatedHooks(activatedQueue);
  callUpdatedHooks(updatedQueue);

  // devtool hook
  /* istanbul ignore if */
  if (devtools && config.devtools) {
    devtools.emit('flush');
  }
}

function callUpdatedHooks (queue) {
  var i = queue.length;
  while (i--) {
    var watcher = queue[i];
    var vm = watcher.vm;
    if (vm._watcher === watcher && vm._isMounted && !vm._isDestroyed) {
      callHook(vm, 'updated');
    }
  }
}

/**
 * Queue a kept-alive component that was activated during patch.
 * The queue will be processed after the entire tree has been patched.
 */
function queueActivatedComponent (vm) {
  // setting _inactive to false here so that a render function can
  // rely on checking whether it's in an inactive tree (e.g. router-view)
  vm._inactive = false;
  activatedChildren.push(vm);
}

function callActivatedHooks (queue) {
  for (var i = 0; i < queue.length; i++) {
    queue[i]._inactive = true;
    activateChildComponent(queue[i], true /* true */);
  }
}

/**
 * Push a watcher into the watcher queue.
 * Jobs with duplicate IDs will be skipped unless it's
 * pushed when the queue is being flushed.
 */
function queueWatcher (watcher) {
  var id = watcher.id;
  if (has[id] == null) {
    has[id] = true;
    if (!flushing) {
      queue.push(watcher);
    } else {
      // if already flushing, splice the watcher based on its id
      // if already past its id, it will be run next immediately.
      var i = queue.length - 1;
      while (i > index && queue[i].id > watcher.id) {
        i--;
      }
      queue.splice(i + 1, 0, watcher);
    }
    // queue the flush
    if (!waiting) {
      waiting = true;

      if ( true && !config.async) {
        flushSchedulerQueue();
        return
      }
      nextTick(flushSchedulerQueue);
    }
  }
}

/*  */



var uid$2 = 0;

/**
 * A watcher parses an expression, collects dependencies,
 * and fires callback when the expression value changes.
 * This is used for both the $watch() api and directives.
 */
var Watcher = function Watcher (
  vm,
  expOrFn,
  cb,
  options,
  isRenderWatcher
) {
  this.vm = vm;
  if (isRenderWatcher) {
    vm._watcher = this;
  }
  vm._watchers.push(this);
  // options
  if (options) {
    this.deep = !!options.deep;
    this.user = !!options.user;
    this.lazy = !!options.lazy;
    this.sync = !!options.sync;
    this.before = options.before;
  } else {
    this.deep = this.user = this.lazy = this.sync = false;
  }
  this.cb = cb;
  this.id = ++uid$2; // uid for batching
  this.active = true;
  this.dirty = this.lazy; // for lazy watchers
  this.deps = [];
  this.newDeps = [];
  this.depIds = new _Set();
  this.newDepIds = new _Set();
  this.expression =  true
    ? expOrFn.toString()
    : undefined;
  // parse expression for getter
  if (typeof expOrFn === 'function') {
    this.getter = expOrFn;
  } else {
    this.getter = parsePath(expOrFn);
    if (!this.getter) {
      this.getter = noop;
       true && warn(
        "Failed watching path: \"" + expOrFn + "\" " +
        'Watcher only accepts simple dot-delimited paths. ' +
        'For full control, use a function instead.',
        vm
      );
    }
  }
  this.value = this.lazy
    ? undefined
    : this.get();
};

/**
 * Evaluate the getter, and re-collect dependencies.
 */
Watcher.prototype.get = function get () {
  pushTarget(this);
  var value;
  var vm = this.vm;
  try {
    value = this.getter.call(vm, vm);
  } catch (e) {
    if (this.user) {
      handleError(e, vm, ("getter for watcher \"" + (this.expression) + "\""));
    } else {
      throw e
    }
  } finally {
    // "touch" every property so they are all tracked as
    // dependencies for deep watching
    if (this.deep) {
      traverse(value);
    }
    popTarget();
    this.cleanupDeps();
  }
  return value
};

/**
 * Add a dependency to this directive.
 */
Watcher.prototype.addDep = function addDep (dep) {
  var id = dep.id;
  if (!this.newDepIds.has(id)) {
    this.newDepIds.add(id);
    this.newDeps.push(dep);
    if (!this.depIds.has(id)) {
      dep.addSub(this);
    }
  }
};

/**
 * Clean up for dependency collection.
 */
Watcher.prototype.cleanupDeps = function cleanupDeps () {
  var i = this.deps.length;
  while (i--) {
    var dep = this.deps[i];
    if (!this.newDepIds.has(dep.id)) {
      dep.removeSub(this);
    }
  }
  var tmp = this.depIds;
  this.depIds = this.newDepIds;
  this.newDepIds = tmp;
  this.newDepIds.clear();
  tmp = this.deps;
  this.deps = this.newDeps;
  this.newDeps = tmp;
  this.newDeps.length = 0;
};

/**
 * Subscriber interface.
 * Will be called when a dependency changes.
 */
Watcher.prototype.update = function update () {
  /* istanbul ignore else */
  if (this.lazy) {
    this.dirty = true;
  } else if (this.sync) {
    this.run();
  } else {
    queueWatcher(this);
  }
};

/**
 * Scheduler job interface.
 * Will be called by the scheduler.
 */
Watcher.prototype.run = function run () {
  if (this.active) {
    var value = this.get();
    if (
      value !== this.value ||
      // Deep watchers and watchers on Object/Arrays should fire even
      // when the value is the same, because the value may
      // have mutated.
      isObject(value) ||
      this.deep
    ) {
      // set new value
      var oldValue = this.value;
      this.value = value;
      if (this.user) {
        try {
          this.cb.call(this.vm, value, oldValue);
        } catch (e) {
          handleError(e, this.vm, ("callback for watcher \"" + (this.expression) + "\""));
        }
      } else {
        this.cb.call(this.vm, value, oldValue);
      }
    }
  }
};

/**
 * Evaluate the value of the watcher.
 * This only gets called for lazy watchers.
 */
Watcher.prototype.evaluate = function evaluate () {
  this.value = this.get();
  this.dirty = false;
};

/**
 * Depend on all deps collected by this watcher.
 */
Watcher.prototype.depend = function depend () {
  var i = this.deps.length;
  while (i--) {
    this.deps[i].depend();
  }
};

/**
 * Remove self from all dependencies' subscriber list.
 */
Watcher.prototype.teardown = function teardown () {
  if (this.active) {
    // remove self from vm's watcher list
    // this is a somewhat expensive operation so we skip it
    // if the vm is being destroyed.
    if (!this.vm._isBeingDestroyed) {
      remove(this.vm._watchers, this);
    }
    var i = this.deps.length;
    while (i--) {
      this.deps[i].removeSub(this);
    }
    this.active = false;
  }
};

/*  */

var sharedPropertyDefinition = {
  enumerable: true,
  configurable: true,
  get: noop,
  set: noop
};

function proxy (target, sourceKey, key) {
  sharedPropertyDefinition.get = function proxyGetter () {
    return this[sourceKey][key]
  };
  sharedPropertyDefinition.set = function proxySetter (val) {
    this[sourceKey][key] = val;
  };
  Object.defineProperty(target, key, sharedPropertyDefinition);
}

function initState (vm) {
  vm._watchers = [];
  var opts = vm.$options;
  if (opts.props) { initProps(vm, opts.props); }
  if (opts.methods) { initMethods(vm, opts.methods); }
  if (opts.data) {
    initData(vm);
  } else {
    observe(vm._data = {}, true /* asRootData */);
  }
  if (opts.computed) { initComputed(vm, opts.computed); }
  if (opts.watch && opts.watch !== nativeWatch) {
    initWatch(vm, opts.watch);
  }
}

function initProps (vm, propsOptions) {
  var propsData = vm.$options.propsData || {};
  var props = vm._props = {};
  // cache prop keys so that future props updates can iterate using Array
  // instead of dynamic object key enumeration.
  var keys = vm.$options._propKeys = [];
  var isRoot = !vm.$parent;
  // root instance props should be converted
  if (!isRoot) {
    toggleObserving(false);
  }
  var loop = function ( key ) {
    keys.push(key);
    var value = validateProp(key, propsOptions, propsData, vm);
    /* istanbul ignore else */
    if (true) {
      var hyphenatedKey = hyphenate(key);
      if (isReservedAttribute(hyphenatedKey) ||
          config.isReservedAttr(hyphenatedKey)) {
        warn(
          ("\"" + hyphenatedKey + "\" is a reserved attribute and cannot be used as component prop."),
          vm
        );
      }
      defineReactive$$1(props, key, value, function () {
        if (!isRoot && !isUpdatingChildComponent) {
          {
            if(vm.mpHost === 'mp-baidu' || vm.mpHost === 'mp-kuaishou' || vm.mpHost === 'mp-xhs'){//百度、快手、小红书 observer 在 setData callback 之后触发，直接忽略该 warn
                return
            }
            //fixed by xxxxxx __next_tick_pending,uni://form-field 时不告警
            if(
                key === 'value' && 
                Array.isArray(vm.$options.behaviors) &&
                vm.$options.behaviors.indexOf('uni://form-field') !== -1
              ){
              return
            }
            if(vm._getFormData){
              return
            }
            var $parent = vm.$parent;
            while($parent){
              if($parent.__next_tick_pending){
                return  
              }
              $parent = $parent.$parent;
            }
          }
          warn(
            "Avoid mutating a prop directly since the value will be " +
            "overwritten whenever the parent component re-renders. " +
            "Instead, use a data or computed property based on the prop's " +
            "value. Prop being mutated: \"" + key + "\"",
            vm
          );
        }
      });
    } else {}
    // static props are already proxied on the component's prototype
    // during Vue.extend(). We only need to proxy props defined at
    // instantiation here.
    if (!(key in vm)) {
      proxy(vm, "_props", key);
    }
  };

  for (var key in propsOptions) loop( key );
  toggleObserving(true);
}

function initData (vm) {
  var data = vm.$options.data;
  data = vm._data = typeof data === 'function'
    ? getData(data, vm)
    : data || {};
  if (!isPlainObject(data)) {
    data = {};
     true && warn(
      'data functions should return an object:\n' +
      'https://vuejs.org/v2/guide/components.html#data-Must-Be-a-Function',
      vm
    );
  }
  // proxy data on instance
  var keys = Object.keys(data);
  var props = vm.$options.props;
  var methods = vm.$options.methods;
  var i = keys.length;
  while (i--) {
    var key = keys[i];
    if (true) {
      if (methods && hasOwn(methods, key)) {
        warn(
          ("Method \"" + key + "\" has already been defined as a data property."),
          vm
        );
      }
    }
    if (props && hasOwn(props, key)) {
       true && warn(
        "The data property \"" + key + "\" is already declared as a prop. " +
        "Use prop default value instead.",
        vm
      );
    } else if (!isReserved(key)) {
      proxy(vm, "_data", key);
    }
  }
  // observe data
  observe(data, true /* asRootData */);
}

function getData (data, vm) {
  // #7573 disable dep collection when invoking data getters
  pushTarget();
  try {
    return data.call(vm, vm)
  } catch (e) {
    handleError(e, vm, "data()");
    return {}
  } finally {
    popTarget();
  }
}

var computedWatcherOptions = { lazy: true };

function initComputed (vm, computed) {
  // $flow-disable-line
  var watchers = vm._computedWatchers = Object.create(null);
  // computed properties are just getters during SSR
  var isSSR = isServerRendering();

  for (var key in computed) {
    var userDef = computed[key];
    var getter = typeof userDef === 'function' ? userDef : userDef.get;
    if ( true && getter == null) {
      warn(
        ("Getter is missing for computed property \"" + key + "\"."),
        vm
      );
    }

    if (!isSSR) {
      // create internal watcher for the computed property.
      watchers[key] = new Watcher(
        vm,
        getter || noop,
        noop,
        computedWatcherOptions
      );
    }

    // component-defined computed properties are already defined on the
    // component prototype. We only need to define computed properties defined
    // at instantiation here.
    if (!(key in vm)) {
      defineComputed(vm, key, userDef);
    } else if (true) {
      if (key in vm.$data) {
        warn(("The computed property \"" + key + "\" is already defined in data."), vm);
      } else if (vm.$options.props && key in vm.$options.props) {
        warn(("The computed property \"" + key + "\" is already defined as a prop."), vm);
      }
    }
  }
}

function defineComputed (
  target,
  key,
  userDef
) {
  var shouldCache = !isServerRendering();
  if (typeof userDef === 'function') {
    sharedPropertyDefinition.get = shouldCache
      ? createComputedGetter(key)
      : createGetterInvoker(userDef);
    sharedPropertyDefinition.set = noop;
  } else {
    sharedPropertyDefinition.get = userDef.get
      ? shouldCache && userDef.cache !== false
        ? createComputedGetter(key)
        : createGetterInvoker(userDef.get)
      : noop;
    sharedPropertyDefinition.set = userDef.set || noop;
  }
  if ( true &&
      sharedPropertyDefinition.set === noop) {
    sharedPropertyDefinition.set = function () {
      warn(
        ("Computed property \"" + key + "\" was assigned to but it has no setter."),
        this
      );
    };
  }
  Object.defineProperty(target, key, sharedPropertyDefinition);
}

function createComputedGetter (key) {
  return function computedGetter () {
    var watcher = this._computedWatchers && this._computedWatchers[key];
    if (watcher) {
      if (watcher.dirty) {
        watcher.evaluate();
      }
      if (Dep.SharedObject.target) {// fixed by xxxxxx
        watcher.depend();
      }
      return watcher.value
    }
  }
}

function createGetterInvoker(fn) {
  return function computedGetter () {
    return fn.call(this, this)
  }
}

function initMethods (vm, methods) {
  var props = vm.$options.props;
  for (var key in methods) {
    if (true) {
      if (typeof methods[key] !== 'function') {
        warn(
          "Method \"" + key + "\" has type \"" + (typeof methods[key]) + "\" in the component definition. " +
          "Did you reference the function correctly?",
          vm
        );
      }
      if (props && hasOwn(props, key)) {
        warn(
          ("Method \"" + key + "\" has already been defined as a prop."),
          vm
        );
      }
      if ((key in vm) && isReserved(key)) {
        warn(
          "Method \"" + key + "\" conflicts with an existing Vue instance method. " +
          "Avoid defining component methods that start with _ or $."
        );
      }
    }
    vm[key] = typeof methods[key] !== 'function' ? noop : bind(methods[key], vm);
  }
}

function initWatch (vm, watch) {
  for (var key in watch) {
    var handler = watch[key];
    if (Array.isArray(handler)) {
      for (var i = 0; i < handler.length; i++) {
        createWatcher(vm, key, handler[i]);
      }
    } else {
      createWatcher(vm, key, handler);
    }
  }
}

function createWatcher (
  vm,
  expOrFn,
  handler,
  options
) {
  if (isPlainObject(handler)) {
    options = handler;
    handler = handler.handler;
  }
  if (typeof handler === 'string') {
    handler = vm[handler];
  }
  return vm.$watch(expOrFn, handler, options)
}

function stateMixin (Vue) {
  // flow somehow has problems with directly declared definition object
  // when using Object.defineProperty, so we have to procedurally build up
  // the object here.
  var dataDef = {};
  dataDef.get = function () { return this._data };
  var propsDef = {};
  propsDef.get = function () { return this._props };
  if (true) {
    dataDef.set = function () {
      warn(
        'Avoid replacing instance root $data. ' +
        'Use nested data properties instead.',
        this
      );
    };
    propsDef.set = function () {
      warn("$props is readonly.", this);
    };
  }
  Object.defineProperty(Vue.prototype, '$data', dataDef);
  Object.defineProperty(Vue.prototype, '$props', propsDef);

  Vue.prototype.$set = set;
  Vue.prototype.$delete = del;

  Vue.prototype.$watch = function (
    expOrFn,
    cb,
    options
  ) {
    var vm = this;
    if (isPlainObject(cb)) {
      return createWatcher(vm, expOrFn, cb, options)
    }
    options = options || {};
    options.user = true;
    var watcher = new Watcher(vm, expOrFn, cb, options);
    if (options.immediate) {
      try {
        cb.call(vm, watcher.value);
      } catch (error) {
        handleError(error, vm, ("callback for immediate watcher \"" + (watcher.expression) + "\""));
      }
    }
    return function unwatchFn () {
      watcher.teardown();
    }
  };
}

/*  */

var uid$3 = 0;

function initMixin (Vue) {
  Vue.prototype._init = function (options) {
    var vm = this;
    // a uid
    vm._uid = uid$3++;

    var startTag, endTag;
    /* istanbul ignore if */
    if ( true && config.performance && mark) {
      startTag = "vue-perf-start:" + (vm._uid);
      endTag = "vue-perf-end:" + (vm._uid);
      mark(startTag);
    }

    // a flag to avoid this being observed
    vm._isVue = true;
    // merge options
    if (options && options._isComponent) {
      // optimize internal component instantiation
      // since dynamic options merging is pretty slow, and none of the
      // internal component options needs special treatment.
      initInternalComponent(vm, options);
    } else {
      vm.$options = mergeOptions(
        resolveConstructorOptions(vm.constructor),
        options || {},
        vm
      );
    }
    /* istanbul ignore else */
    if (true) {
      initProxy(vm);
    } else {}
    // expose real self
    vm._self = vm;
    initLifecycle(vm);
    initEvents(vm);
    initRender(vm);
    callHook(vm, 'beforeCreate');
    !vm._$fallback && initInjections(vm); // resolve injections before data/props  
    initState(vm);
    !vm._$fallback && initProvide(vm); // resolve provide after data/props
    !vm._$fallback && callHook(vm, 'created');      

    /* istanbul ignore if */
    if ( true && config.performance && mark) {
      vm._name = formatComponentName(vm, false);
      mark(endTag);
      measure(("vue " + (vm._name) + " init"), startTag, endTag);
    }

    if (vm.$options.el) {
      vm.$mount(vm.$options.el);
    }
  };
}

function initInternalComponent (vm, options) {
  var opts = vm.$options = Object.create(vm.constructor.options);
  // doing this because it's faster than dynamic enumeration.
  var parentVnode = options._parentVnode;
  opts.parent = options.parent;
  opts._parentVnode = parentVnode;

  var vnodeComponentOptions = parentVnode.componentOptions;
  opts.propsData = vnodeComponentOptions.propsData;
  opts._parentListeners = vnodeComponentOptions.listeners;
  opts._renderChildren = vnodeComponentOptions.children;
  opts._componentTag = vnodeComponentOptions.tag;

  if (options.render) {
    opts.render = options.render;
    opts.staticRenderFns = options.staticRenderFns;
  }
}

function resolveConstructorOptions (Ctor) {
  var options = Ctor.options;
  if (Ctor.super) {
    var superOptions = resolveConstructorOptions(Ctor.super);
    var cachedSuperOptions = Ctor.superOptions;
    if (superOptions !== cachedSuperOptions) {
      // super option changed,
      // need to resolve new options.
      Ctor.superOptions = superOptions;
      // check if there are any late-modified/attached options (#4976)
      var modifiedOptions = resolveModifiedOptions(Ctor);
      // update base extend options
      if (modifiedOptions) {
        extend(Ctor.extendOptions, modifiedOptions);
      }
      options = Ctor.options = mergeOptions(superOptions, Ctor.extendOptions);
      if (options.name) {
        options.components[options.name] = Ctor;
      }
    }
  }
  return options
}

function resolveModifiedOptions (Ctor) {
  var modified;
  var latest = Ctor.options;
  var sealed = Ctor.sealedOptions;
  for (var key in latest) {
    if (latest[key] !== sealed[key]) {
      if (!modified) { modified = {}; }
      modified[key] = latest[key];
    }
  }
  return modified
}

function Vue (options) {
  if ( true &&
    !(this instanceof Vue)
  ) {
    warn('Vue is a constructor and should be called with the `new` keyword');
  }
  this._init(options);
}

initMixin(Vue);
stateMixin(Vue);
eventsMixin(Vue);
lifecycleMixin(Vue);
renderMixin(Vue);

/*  */

function initUse (Vue) {
  Vue.use = function (plugin) {
    var installedPlugins = (this._installedPlugins || (this._installedPlugins = []));
    if (installedPlugins.indexOf(plugin) > -1) {
      return this
    }

    // additional parameters
    var args = toArray(arguments, 1);
    args.unshift(this);
    if (typeof plugin.install === 'function') {
      plugin.install.apply(plugin, args);
    } else if (typeof plugin === 'function') {
      plugin.apply(null, args);
    }
    installedPlugins.push(plugin);
    return this
  };
}

/*  */

function initMixin$1 (Vue) {
  Vue.mixin = function (mixin) {
    this.options = mergeOptions(this.options, mixin);
    return this
  };
}

/*  */

function initExtend (Vue) {
  /**
   * Each instance constructor, including Vue, has a unique
   * cid. This enables us to create wrapped "child
   * constructors" for prototypal inheritance and cache them.
   */
  Vue.cid = 0;
  var cid = 1;

  /**
   * Class inheritance
   */
  Vue.extend = function (extendOptions) {
    extendOptions = extendOptions || {};
    var Super = this;
    var SuperId = Super.cid;
    var cachedCtors = extendOptions._Ctor || (extendOptions._Ctor = {});
    if (cachedCtors[SuperId]) {
      return cachedCtors[SuperId]
    }

    var name = extendOptions.name || Super.options.name;
    if ( true && name) {
      validateComponentName(name);
    }

    var Sub = function VueComponent (options) {
      this._init(options);
    };
    Sub.prototype = Object.create(Super.prototype);
    Sub.prototype.constructor = Sub;
    Sub.cid = cid++;
    Sub.options = mergeOptions(
      Super.options,
      extendOptions
    );
    Sub['super'] = Super;

    // For props and computed properties, we define the proxy getters on
    // the Vue instances at extension time, on the extended prototype. This
    // avoids Object.defineProperty calls for each instance created.
    if (Sub.options.props) {
      initProps$1(Sub);
    }
    if (Sub.options.computed) {
      initComputed$1(Sub);
    }

    // allow further extension/mixin/plugin usage
    Sub.extend = Super.extend;
    Sub.mixin = Super.mixin;
    Sub.use = Super.use;

    // create asset registers, so extended classes
    // can have their private assets too.
    ASSET_TYPES.forEach(function (type) {
      Sub[type] = Super[type];
    });
    // enable recursive self-lookup
    if (name) {
      Sub.options.components[name] = Sub;
    }

    // keep a reference to the super options at extension time.
    // later at instantiation we can check if Super's options have
    // been updated.
    Sub.superOptions = Super.options;
    Sub.extendOptions = extendOptions;
    Sub.sealedOptions = extend({}, Sub.options);

    // cache constructor
    cachedCtors[SuperId] = Sub;
    return Sub
  };
}

function initProps$1 (Comp) {
  var props = Comp.options.props;
  for (var key in props) {
    proxy(Comp.prototype, "_props", key);
  }
}

function initComputed$1 (Comp) {
  var computed = Comp.options.computed;
  for (var key in computed) {
    defineComputed(Comp.prototype, key, computed[key]);
  }
}

/*  */

function initAssetRegisters (Vue) {
  /**
   * Create asset registration methods.
   */
  ASSET_TYPES.forEach(function (type) {
    Vue[type] = function (
      id,
      definition
    ) {
      if (!definition) {
        return this.options[type + 's'][id]
      } else {
        /* istanbul ignore if */
        if ( true && type === 'component') {
          validateComponentName(id);
        }
        if (type === 'component' && isPlainObject(definition)) {
          definition.name = definition.name || id;
          definition = this.options._base.extend(definition);
        }
        if (type === 'directive' && typeof definition === 'function') {
          definition = { bind: definition, update: definition };
        }
        this.options[type + 's'][id] = definition;
        return definition
      }
    };
  });
}

/*  */



function getComponentName (opts) {
  return opts && (opts.Ctor.options.name || opts.tag)
}

function matches (pattern, name) {
  if (Array.isArray(pattern)) {
    return pattern.indexOf(name) > -1
  } else if (typeof pattern === 'string') {
    return pattern.split(',').indexOf(name) > -1
  } else if (isRegExp(pattern)) {
    return pattern.test(name)
  }
  /* istanbul ignore next */
  return false
}

function pruneCache (keepAliveInstance, filter) {
  var cache = keepAliveInstance.cache;
  var keys = keepAliveInstance.keys;
  var _vnode = keepAliveInstance._vnode;
  for (var key in cache) {
    var cachedNode = cache[key];
    if (cachedNode) {
      var name = getComponentName(cachedNode.componentOptions);
      if (name && !filter(name)) {
        pruneCacheEntry(cache, key, keys, _vnode);
      }
    }
  }
}

function pruneCacheEntry (
  cache,
  key,
  keys,
  current
) {
  var cached$$1 = cache[key];
  if (cached$$1 && (!current || cached$$1.tag !== current.tag)) {
    cached$$1.componentInstance.$destroy();
  }
  cache[key] = null;
  remove(keys, key);
}

var patternTypes = [String, RegExp, Array];

var KeepAlive = {
  name: 'keep-alive',
  abstract: true,

  props: {
    include: patternTypes,
    exclude: patternTypes,
    max: [String, Number]
  },

  created: function created () {
    this.cache = Object.create(null);
    this.keys = [];
  },

  destroyed: function destroyed () {
    for (var key in this.cache) {
      pruneCacheEntry(this.cache, key, this.keys);
    }
  },

  mounted: function mounted () {
    var this$1 = this;

    this.$watch('include', function (val) {
      pruneCache(this$1, function (name) { return matches(val, name); });
    });
    this.$watch('exclude', function (val) {
      pruneCache(this$1, function (name) { return !matches(val, name); });
    });
  },

  render: function render () {
    var slot = this.$slots.default;
    var vnode = getFirstComponentChild(slot);
    var componentOptions = vnode && vnode.componentOptions;
    if (componentOptions) {
      // check pattern
      var name = getComponentName(componentOptions);
      var ref = this;
      var include = ref.include;
      var exclude = ref.exclude;
      if (
        // not included
        (include && (!name || !matches(include, name))) ||
        // excluded
        (exclude && name && matches(exclude, name))
      ) {
        return vnode
      }

      var ref$1 = this;
      var cache = ref$1.cache;
      var keys = ref$1.keys;
      var key = vnode.key == null
        // same constructor may get registered as different local components
        // so cid alone is not enough (#3269)
        ? componentOptions.Ctor.cid + (componentOptions.tag ? ("::" + (componentOptions.tag)) : '')
        : vnode.key;
      if (cache[key]) {
        vnode.componentInstance = cache[key].componentInstance;
        // make current key freshest
        remove(keys, key);
        keys.push(key);
      } else {
        cache[key] = vnode;
        keys.push(key);
        // prune oldest entry
        if (this.max && keys.length > parseInt(this.max)) {
          pruneCacheEntry(cache, keys[0], keys, this._vnode);
        }
      }

      vnode.data.keepAlive = true;
    }
    return vnode || (slot && slot[0])
  }
};

var builtInComponents = {
  KeepAlive: KeepAlive
};

/*  */

function initGlobalAPI (Vue) {
  // config
  var configDef = {};
  configDef.get = function () { return config; };
  if (true) {
    configDef.set = function () {
      warn(
        'Do not replace the Vue.config object, set individual fields instead.'
      );
    };
  }
  Object.defineProperty(Vue, 'config', configDef);

  // exposed util methods.
  // NOTE: these are not considered part of the public API - avoid relying on
  // them unless you are aware of the risk.
  Vue.util = {
    warn: warn,
    extend: extend,
    mergeOptions: mergeOptions,
    defineReactive: defineReactive$$1
  };

  Vue.set = set;
  Vue.delete = del;
  Vue.nextTick = nextTick;

  // 2.6 explicit observable API
  Vue.observable = function (obj) {
    observe(obj);
    return obj
  };

  Vue.options = Object.create(null);
  ASSET_TYPES.forEach(function (type) {
    Vue.options[type + 's'] = Object.create(null);
  });

  // this is used to identify the "base" constructor to extend all plain-object
  // components with in Weex's multi-instance scenarios.
  Vue.options._base = Vue;

  extend(Vue.options.components, builtInComponents);

  initUse(Vue);
  initMixin$1(Vue);
  initExtend(Vue);
  initAssetRegisters(Vue);
}

initGlobalAPI(Vue);

Object.defineProperty(Vue.prototype, '$isServer', {
  get: isServerRendering
});

Object.defineProperty(Vue.prototype, '$ssrContext', {
  get: function get () {
    /* istanbul ignore next */
    return this.$vnode && this.$vnode.ssrContext
  }
});

// expose FunctionalRenderContext for ssr runtime helper installation
Object.defineProperty(Vue, 'FunctionalRenderContext', {
  value: FunctionalRenderContext
});

Vue.version = '2.6.11';

/**
 * https://raw.githubusercontent.com/Tencent/westore/master/packages/westore/utils/diff.js
 */
var ARRAYTYPE = '[object Array]';
var OBJECTTYPE = '[object Object]';
var NULLTYPE = '[object Null]';
var UNDEFINEDTYPE = '[object Undefined]';
// const FUNCTIONTYPE = '[object Function]'

function diff(current, pre) {
    var result = {};
    syncKeys(current, pre);
    _diff(current, pre, '', result);
    return result
}

function syncKeys(current, pre) {
    if (current === pre) { return }
    var rootCurrentType = type(current);
    var rootPreType = type(pre);
    if (rootCurrentType == OBJECTTYPE && rootPreType == OBJECTTYPE) {
        if(Object.keys(current).length >= Object.keys(pre).length){
            for (var key in pre) {
                var currentValue = current[key];
                if (currentValue === undefined) {
                    current[key] = null;
                } else {
                    syncKeys(currentValue, pre[key]);
                }
            }
        }
    } else if (rootCurrentType == ARRAYTYPE && rootPreType == ARRAYTYPE) {
        if (current.length >= pre.length) {
            pre.forEach(function (item, index) {
                syncKeys(current[index], item);
            });
        }
    }
}

function nullOrUndefined(currentType, preType) {
    if(
        (currentType === NULLTYPE || currentType === UNDEFINEDTYPE) && 
        (preType === NULLTYPE || preType === UNDEFINEDTYPE)
    ) {
        return false
    }
    return true
}

function _diff(current, pre, path, result) {
    if (current === pre) { return }
    var rootCurrentType = type(current);
    var rootPreType = type(pre);
    if (rootCurrentType == OBJECTTYPE) {
        if (rootPreType != OBJECTTYPE || Object.keys(current).length < Object.keys(pre).length) {
            setResult(result, path, current);
        } else {
            var loop = function ( key ) {
                var currentValue = current[key];
                var preValue = pre[key];
                var currentType = type(currentValue);
                var preType = type(preValue);
                if (currentType != ARRAYTYPE && currentType != OBJECTTYPE) {
                    if (currentValue !== pre[key] && nullOrUndefined(currentType, preType)) {
                        setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                    }
                } else if (currentType == ARRAYTYPE) {
                    if (preType != ARRAYTYPE) {
                        setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                    } else {
                        if (currentValue.length < preValue.length) {
                            setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                        } else {
                            currentValue.forEach(function (item, index) {
                                _diff(item, preValue[index], (path == '' ? '' : path + ".") + key + '[' + index + ']', result);
                            });
                        }
                    }
                } else if (currentType == OBJECTTYPE) {
                    if (preType != OBJECTTYPE || Object.keys(currentValue).length < Object.keys(preValue).length) {
                        setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                    } else {
                        for (var subKey in currentValue) {
                            _diff(currentValue[subKey], preValue[subKey], (path == '' ? '' : path + ".") + key + '.' + subKey, result);
                        }
                    }
                }
            };

            for (var key in current) loop( key );
        }
    } else if (rootCurrentType == ARRAYTYPE) {
        if (rootPreType != ARRAYTYPE) {
            setResult(result, path, current);
        } else {
            if (current.length < pre.length) {
                setResult(result, path, current);
            } else {
                current.forEach(function (item, index) {
                    _diff(item, pre[index], path + '[' + index + ']', result);
                });
            }
        }
    } else {
        setResult(result, path, current);
    }
}

function setResult(result, k, v) {
    // if (type(v) != FUNCTIONTYPE) {
        result[k] = v;
    // }
}

function type(obj) {
    return Object.prototype.toString.call(obj)
}

/*  */

function flushCallbacks$1(vm) {
    if (vm.__next_tick_callbacks && vm.__next_tick_callbacks.length) {
        if (Object({"NODE_ENV":"development","VUE_APP_DARK_MODE":"false","VUE_APP_NAME":"e考监控","VUE_APP_PLATFORM":"mp-weixin","BASE_URL":"/"}).VUE_APP_DEBUG) {
            var mpInstance = vm.$scope;
            console.log('[' + (+new Date) + '][' + (mpInstance.is || mpInstance.route) + '][' + vm._uid +
                ']:flushCallbacks[' + vm.__next_tick_callbacks.length + ']');
        }
        var copies = vm.__next_tick_callbacks.slice(0);
        vm.__next_tick_callbacks.length = 0;
        for (var i = 0; i < copies.length; i++) {
            copies[i]();
        }
    }
}

function hasRenderWatcher(vm) {
    return queue.find(function (watcher) { return vm._watcher === watcher; })
}

function nextTick$1(vm, cb) {
    //1.nextTick 之前 已 setData 且 setData 还未回调完成
    //2.nextTick 之前存在 render watcher
    if (!vm.__next_tick_pending && !hasRenderWatcher(vm)) {
        if(Object({"NODE_ENV":"development","VUE_APP_DARK_MODE":"false","VUE_APP_NAME":"e考监控","VUE_APP_PLATFORM":"mp-weixin","BASE_URL":"/"}).VUE_APP_DEBUG){
            var mpInstance = vm.$scope;
            console.log('[' + (+new Date) + '][' + (mpInstance.is || mpInstance.route) + '][' + vm._uid +
                ']:nextVueTick');
        }
        return nextTick(cb, vm)
    }else{
        if(Object({"NODE_ENV":"development","VUE_APP_DARK_MODE":"false","VUE_APP_NAME":"e考监控","VUE_APP_PLATFORM":"mp-weixin","BASE_URL":"/"}).VUE_APP_DEBUG){
            var mpInstance$1 = vm.$scope;
            console.log('[' + (+new Date) + '][' + (mpInstance$1.is || mpInstance$1.route) + '][' + vm._uid +
                ']:nextMPTick');
        }
    }
    var _resolve;
    if (!vm.__next_tick_callbacks) {
        vm.__next_tick_callbacks = [];
    }
    vm.__next_tick_callbacks.push(function () {
        if (cb) {
            try {
                cb.call(vm);
            } catch (e) {
                handleError(e, vm, 'nextTick');
            }
        } else if (_resolve) {
            _resolve(vm);
        }
    });
    // $flow-disable-line
    if (!cb && typeof Promise !== 'undefined') {
        return new Promise(function (resolve) {
            _resolve = resolve;
        })
    }
}

/*  */

function clearInstance(key, value) {
  // 简易去除 Vue 和小程序组件实例
  if (value) {
    if (value._isVue || value.__v_isMPComponent) {
      return {}
    }
  }
  return value
}

function cloneWithData(vm) {
  // 确保当前 vm 所有数据被同步
  var ret = Object.create(null);
  var dataKeys = [].concat(
    Object.keys(vm._data || {}),
    Object.keys(vm._computedWatchers || {}));

  dataKeys.reduce(function(ret, key) {
    ret[key] = vm[key];
    return ret
  }, ret);

  // vue-composition-api
  var compositionApiState = vm.__composition_api_state__ || vm.__secret_vfa_state__;
  var rawBindings = compositionApiState && compositionApiState.rawBindings;
  if (rawBindings) {
    Object.keys(rawBindings).forEach(function (key) {
      ret[key] = vm[key];
    });
  }

  //TODO 需要把无用数据处理掉，比如 list=>l0 则 list 需要移除，否则多传输一份数据
  Object.assign(ret, vm.$mp.data || {});
  if (
    Array.isArray(vm.$options.behaviors) &&
    vm.$options.behaviors.indexOf('uni://form-field') !== -1
  ) { //form-field
    ret['name'] = vm.name;
    ret['value'] = vm.value;
  }

  return JSON.parse(JSON.stringify(ret, clearInstance))
}

var patch = function(oldVnode, vnode) {
  var this$1 = this;

  if (vnode === null) { //destroy
    return
  }
  if (this.mpType === 'page' || this.mpType === 'component') {
    var mpInstance = this.$scope;
    var data = Object.create(null);
    try {
      data = cloneWithData(this);
    } catch (err) {
      console.error(err);
    }
    data.__webviewId__ = mpInstance.data.__webviewId__;
    var mpData = Object.create(null);
    Object.keys(data).forEach(function (key) { //仅同步 data 中有的数据
      mpData[key] = mpInstance.data[key];
    });
    var diffData = this.$shouldDiffData === false ? data : diff(data, mpData);
    if (Object.keys(diffData).length) {
      if (Object({"NODE_ENV":"development","VUE_APP_DARK_MODE":"false","VUE_APP_NAME":"e考监控","VUE_APP_PLATFORM":"mp-weixin","BASE_URL":"/"}).VUE_APP_DEBUG) {
        console.log('[' + (+new Date) + '][' + (mpInstance.is || mpInstance.route) + '][' + this._uid +
          ']差量更新',
          JSON.stringify(diffData));
      }
      this.__next_tick_pending = true;
      mpInstance.setData(diffData, function () {
        this$1.__next_tick_pending = false;
        flushCallbacks$1(this$1);
      });
    } else {
      flushCallbacks$1(this);
    }
  }
};

/*  */

function createEmptyRender() {

}

function mountComponent$1(
  vm,
  el,
  hydrating
) {
  if (!vm.mpType) {//main.js 中的 new Vue
    return vm
  }
  if (vm.mpType === 'app') {
    vm.$options.render = createEmptyRender;
  }
  if (!vm.$options.render) {
    vm.$options.render = createEmptyRender;
    if (true) {
      /* istanbul ignore if */
      if ((vm.$options.template && vm.$options.template.charAt(0) !== '#') ||
        vm.$options.el || el) {
        warn(
          'You are using the runtime-only build of Vue where the template ' +
          'compiler is not available. Either pre-compile the templates into ' +
          'render functions, or use the compiler-included build.',
          vm
        );
      } else {
        warn(
          'Failed to mount component: template or render function not defined.',
          vm
        );
      }
    }
  }
  
  !vm._$fallback && callHook(vm, 'beforeMount');

  var updateComponent = function () {
    vm._update(vm._render(), hydrating);
  };

  // we set this to vm._watcher inside the watcher's constructor
  // since the watcher's initial patch may call $forceUpdate (e.g. inside child
  // component's mounted hook), which relies on vm._watcher being already defined
  new Watcher(vm, updateComponent, noop, {
    before: function before() {
      if (vm._isMounted && !vm._isDestroyed) {
        callHook(vm, 'beforeUpdate');
      }
    }
  }, true /* isRenderWatcher */);
  hydrating = false;
  return vm
}

/*  */

function renderClass (
  staticClass,
  dynamicClass
) {
  if (isDef(staticClass) || isDef(dynamicClass)) {
    return concat(staticClass, stringifyClass(dynamicClass))
  }
  /* istanbul ignore next */
  return ''
}

function concat (a, b) {
  return a ? b ? (a + ' ' + b) : a : (b || '')
}

function stringifyClass (value) {
  if (Array.isArray(value)) {
    return stringifyArray(value)
  }
  if (isObject(value)) {
    return stringifyObject(value)
  }
  if (typeof value === 'string') {
    return value
  }
  /* istanbul ignore next */
  return ''
}

function stringifyArray (value) {
  var res = '';
  var stringified;
  for (var i = 0, l = value.length; i < l; i++) {
    if (isDef(stringified = stringifyClass(value[i])) && stringified !== '') {
      if (res) { res += ' '; }
      res += stringified;
    }
  }
  return res
}

function stringifyObject (value) {
  var res = '';
  for (var key in value) {
    if (value[key]) {
      if (res) { res += ' '; }
      res += key;
    }
  }
  return res
}

/*  */

var parseStyleText = cached(function (cssText) {
  var res = {};
  var listDelimiter = /;(?![^(]*\))/g;
  var propertyDelimiter = /:(.+)/;
  cssText.split(listDelimiter).forEach(function (item) {
    if (item) {
      var tmp = item.split(propertyDelimiter);
      tmp.length > 1 && (res[tmp[0].trim()] = tmp[1].trim());
    }
  });
  return res
});

// normalize possible array / string values into Object
function normalizeStyleBinding (bindingStyle) {
  if (Array.isArray(bindingStyle)) {
    return toObject(bindingStyle)
  }
  if (typeof bindingStyle === 'string') {
    return parseStyleText(bindingStyle)
  }
  return bindingStyle
}

/*  */

var MP_METHODS = ['createSelectorQuery', 'createIntersectionObserver', 'selectAllComponents', 'selectComponent'];

function getTarget(obj, path) {
  var parts = path.split('.');
  var key = parts[0];
  if (key.indexOf('__$n') === 0) { //number index
    key = parseInt(key.replace('__$n', ''));
  }
  if (parts.length === 1) {
    return obj[key]
  }
  return getTarget(obj[key], parts.slice(1).join('.'))
}

function internalMixin(Vue) {

  Vue.config.errorHandler = function(err, vm, info) {
    Vue.util.warn(("Error in " + info + ": \"" + (err.toString()) + "\""), vm);
    console.error(err);
    /* eslint-disable no-undef */
    var app = typeof getApp === 'function' && getApp();
    if (app && app.onError) {
      app.onError(err);
    }
  };

  var oldEmit = Vue.prototype.$emit;

  Vue.prototype.$emit = function(event) {
    if (this.$scope && event) {
      var triggerEvent = this.$scope['_triggerEvent'] || this.$scope['triggerEvent'];
      if (triggerEvent) {
        try {
          triggerEvent.call(this.$scope, event, {
            __args__: toArray(arguments, 1)
          });
        } catch (error) {

        }
      }
    }
    return oldEmit.apply(this, arguments)
  };

  Vue.prototype.$nextTick = function(fn) {
    return nextTick$1(this, fn)
  };

  MP_METHODS.forEach(function (method) {
    Vue.prototype[method] = function(args) {
      if (this.$scope && this.$scope[method]) {
        return this.$scope[method](args)
      }
      // mp-alipay
      if (typeof my === 'undefined') {
        return
      }
      if (method === 'createSelectorQuery') {
        /* eslint-disable no-undef */
        return my.createSelectorQuery(args)
      } else if (method === 'createIntersectionObserver') {
        /* eslint-disable no-undef */
        return my.createIntersectionObserver(args)
      }
      // TODO mp-alipay 暂不支持 selectAllComponents,selectComponent
    };
  });

  Vue.prototype.__init_provide = initProvide;

  Vue.prototype.__init_injections = initInjections;

  Vue.prototype.__call_hook = function(hook, args) {
    var vm = this;
    // #7573 disable dep collection when invoking lifecycle hooks
    pushTarget();
    var handlers = vm.$options[hook];
    var info = hook + " hook";
    var ret;
    if (handlers) {
      for (var i = 0, j = handlers.length; i < j; i++) {
        ret = invokeWithErrorHandling(handlers[i], vm, args ? [args] : null, vm, info);
      }
    }
    if (vm._hasHookEvent) {
      vm.$emit('hook:' + hook, args);
    }
    popTarget();
    return ret
  };

  Vue.prototype.__set_model = function(target, key, value, modifiers) {
    if (Array.isArray(modifiers)) {
      if (modifiers.indexOf('trim') !== -1) {
        value = value.trim();
      }
      if (modifiers.indexOf('number') !== -1) {
        value = this._n(value);
      }
    }
    if (!target) {
      target = this;
    }
    // 解决动态属性添加
    Vue.set(target, key, value);
  };

  Vue.prototype.__set_sync = function(target, key, value) {
    if (!target) {
      target = this;
    }
    // 解决动态属性添加
    Vue.set(target, key, value);
  };

  Vue.prototype.__get_orig = function(item) {
    if (isPlainObject(item)) {
      return item['$orig'] || item
    }
    return item
  };

  Vue.prototype.__get_value = function(dataPath, target) {
    return getTarget(target || this, dataPath)
  };


  Vue.prototype.__get_class = function(dynamicClass, staticClass) {
    return renderClass(staticClass, dynamicClass)
  };

  Vue.prototype.__get_style = function(dynamicStyle, staticStyle) {
    if (!dynamicStyle && !staticStyle) {
      return ''
    }
    var dynamicStyleObj = normalizeStyleBinding(dynamicStyle);
    var styleObj = staticStyle ? extend(staticStyle, dynamicStyleObj) : dynamicStyleObj;
    return Object.keys(styleObj).map(function (name) { return ((hyphenate(name)) + ":" + (styleObj[name])); }).join(';')
  };

  Vue.prototype.__map = function(val, iteratee) {
    //TODO 暂不考虑 string
    var ret, i, l, keys, key;
    if (Array.isArray(val)) {
      ret = new Array(val.length);
      for (i = 0, l = val.length; i < l; i++) {
        ret[i] = iteratee(val[i], i);
      }
      return ret
    } else if (isObject(val)) {
      keys = Object.keys(val);
      ret = Object.create(null);
      for (i = 0, l = keys.length; i < l; i++) {
        key = keys[i];
        ret[key] = iteratee(val[key], key, i);
      }
      return ret
    } else if (typeof val === 'number') {
      ret = new Array(val);
      for (i = 0, l = val; i < l; i++) {
        // 第一个参数暂时仍和小程序一致
        ret[i] = iteratee(i, i);
      }
      return ret
    }
    return []
  };

}

/*  */

var LIFECYCLE_HOOKS$1 = [
    //App
    'onLaunch',
    'onShow',
    'onHide',
    'onUniNViewMessage',
    'onPageNotFound',
    'onThemeChange',
    'onError',
    'onUnhandledRejection',
    //Page
    'onInit',
    'onLoad',
    // 'onShow',
    'onReady',
    // 'onHide',
    'onUnload',
    'onPullDownRefresh',
    'onReachBottom',
    'onTabItemTap',
    'onAddToFavorites',
    'onShareTimeline',
    'onShareAppMessage',
    'onResize',
    'onPageScroll',
    'onNavigationBarButtonTap',
    'onBackPress',
    'onNavigationBarSearchInputChanged',
    'onNavigationBarSearchInputConfirmed',
    'onNavigationBarSearchInputClicked',
    'onUploadDouyinVideo',
    'onNFCReadMessage',
    //Component
    // 'onReady', // 兼容旧版本，应该移除该事件
    'onPageShow',
    'onPageHide',
    'onPageResize'
];
function lifecycleMixin$1(Vue) {

    //fixed vue-class-component
    var oldExtend = Vue.extend;
    Vue.extend = function(extendOptions) {
        extendOptions = extendOptions || {};

        var methods = extendOptions.methods;
        if (methods) {
            Object.keys(methods).forEach(function (methodName) {
                if (LIFECYCLE_HOOKS$1.indexOf(methodName)!==-1) {
                    extendOptions[methodName] = methods[methodName];
                    delete methods[methodName];
                }
            });
        }

        return oldExtend.call(this, extendOptions)
    };

    var strategies = Vue.config.optionMergeStrategies;
    var mergeHook = strategies.created;
    LIFECYCLE_HOOKS$1.forEach(function (hook) {
        strategies[hook] = mergeHook;
    });

    Vue.prototype.__lifecycle_hooks__ = LIFECYCLE_HOOKS$1;
}

/*  */

// install platform patch function
Vue.prototype.__patch__ = patch;

// public mount method
Vue.prototype.$mount = function(
    el ,
    hydrating 
) {
    return mountComponent$1(this, el, hydrating)
};

lifecycleMixin$1(Vue);
internalMixin(Vue);

/*  */

/* harmony default export */ __webpack_exports__["default"] = (Vue);

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../webpack/buildin/global.js */ 3)))

/***/ }),
/* 26 */
/*!*******************************************!*\
  !*** E:/公司项目/e考监控/Api-Example/pages.json ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),
/* 27 */,
/* 28 */,
/* 29 */,
/* 30 */
/*!*****************************************************************************!*\
  !*** E:/公司项目/e考监控/Api-Example/node_modules/aegis-weex-sdk/lib/aegis.min.js ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;var _typeof = __webpack_require__(/*! @babel/runtime/helpers/typeof */ 13);
/**
 *  ======================================================================
 * @tencent/aegis-weex-sdk@1.24.45 (c) 2021 Tencent Application Monitor.
 * Author pumpkincai.
 * Last Release Time Wed Dec 29 2021 23:00:35 GMT+0800 (GMT+08:00).
 * Released under the MIT License.
 * Thanks for supporting TAM & Aegis!
 * ======================================================================
 **/
!function (e, t) {
  "object" == ( false ? undefined : _typeof(exports)) && "undefined" != typeof module ? module.exports = t() :  true ? !(__WEBPACK_AMD_DEFINE_FACTORY__ = (t),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) :
				__WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : undefined;
}(this, function () {
  "use strict";

  var _n = function n(e, t) {
      return (_n = Object.setPrototypeOf || {
        __proto__: []
      } instanceof Array && function (e, t) {
        e.__proto__ = t;
      } || function (e, t) {
        for (var n in t) {
          Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
        }
      })(e, t);
    },
    _u = function u() {
      return (_u = Object.assign || function (e) {
        for (var t, n = 1, i = arguments.length; n < i; n++) {
          for (var o in t = arguments[n]) {
            Object.prototype.hasOwnProperty.call(t, o) && (e[o] = t[o]);
          }
        }
        return e;
      }).apply(this, arguments);
    };
  function s(e, s, l, u) {
    return new (l = l || Promise)(function (n, t) {
      function i(e) {
        try {
          r(u.next(e));
        } catch (e) {
          t(e);
        }
      }
      function o(e) {
        try {
          r(u.throw(e));
        } catch (e) {
          t(e);
        }
      }
      function r(e) {
        var t;
        e.done ? n(e.value) : ((t = e.value) instanceof l ? t : new l(function (e) {
          e(t);
        })).then(i, o);
      }
      r((u = u.apply(e, s || [])).next());
    });
  }
  function a(n, i) {
    var o,
      r,
      s,
      l = {
        label: 0,
        sent: function sent() {
          if (1 & s[0]) throw s[1];
          return s[1];
        },
        trys: [],
        ops: []
      },
      e = {
        next: t(0),
        throw: t(1),
        return: t(2)
      };
    return "function" == typeof Symbol && (e[Symbol.iterator] = function () {
      return this;
    }), e;
    function t(t) {
      return function (e) {
        return function (t) {
          if (o) throw new TypeError("Generator is already executing.");
          for (; l;) {
            try {
              if (o = 1, r && (s = 2 & t[0] ? r.return : t[0] ? r.throw || ((s = r.return) && s.call(r), 0) : r.next) && !(s = s.call(r, t[1])).done) return s;
              switch (r = 0, (t = s ? [2 & t[0], s.value] : t)[0]) {
                case 0:
                case 1:
                  s = t;
                  break;
                case 4:
                  return l.label++, {
                    value: t[1],
                    done: !1
                  };
                case 5:
                  l.label++, r = t[1], t = [0];
                  continue;
                case 7:
                  t = l.ops.pop(), l.trys.pop();
                  continue;
                default:
                  if (!((s = 0 < (s = l.trys).length && s[s.length - 1]) || 6 !== t[0] && 2 !== t[0])) {
                    l = 0;
                    continue;
                  }
                  if (3 === t[0] && (!s || t[1] > s[0] && t[1] < s[3])) {
                    l.label = t[1];
                    break;
                  }
                  if (6 === t[0] && l.label < s[1]) {
                    l.label = s[1], s = t;
                    break;
                  }
                  if (s && l.label < s[2]) {
                    l.label = s[2], l.ops.push(t);
                    break;
                  }
                  s[2] && l.ops.pop(), l.trys.pop();
                  continue;
              }
              t = i.call(n, l);
            } catch (e) {
              t = [6, e], r = 0;
            } finally {
              o = s = 0;
            }
          }
          if (5 & t[0]) throw t[1];
          return {
            value: t[0] ? t[1] : void 0,
            done: !0
          };
        }([t, e]);
      };
    }
  }
  function c() {
    for (var e = 0, t = 0, n = arguments.length; t < n; t++) {
      e += arguments[t].length;
    }
    for (var i = Array(e), o = 0, t = 0; t < n; t++) {
      for (var r = arguments[t], s = 0, l = r.length; s < l; s++, o++) {
        i[o] = r[s];
      }
    }
    return i;
  }
  Object.assign || Object.defineProperty(Object, "assign", {
    enumerable: !1,
    configurable: !0,
    writable: !0,
    value: function value(e) {
      if (null == e) throw new TypeError("Cannot convert first argument to object");
      for (var t = Object(e), n = 1; n < arguments.length; n++) {
        if (null != (i = arguments[n])) for (var i = Object(i), o = Object.keys(Object(i)), r = 0, s = o.length; r < s; r++) {
          var l = o[r],
            u = Object.getOwnPropertyDescriptor(i, l);
          null != u && u.enumerable && (t[l] = i[l]);
        }
      }
      return t;
    }
  });
  var f,
    i = (e.prototype.indexOf = function (e, t) {
      for (var n = 0; n < e.length; n++) {
        if (e[n].callback === t) return n;
      }
      return -1;
    }, e.prototype.on = function (e, t, n) {
      if (void 0 === n && (n = 0), this) {
        var i = this.eventsList[e];
        return (i || (this.eventsList[e] = [], i = this.eventsList[e]), -1 !== this.indexOf(i, t)) ? this : (i.push({
          name: e,
          type: n || 0,
          callback: t
        }), this);
      }
    }, e.prototype.one = function (e, t) {
      this.on(e, t, 1);
    }, e.prototype.remove = function (e, t) {
      if (this) {
        var n = this.eventsList[e];
        if (!n) return null;
        if (t) return n.length && (t = this.indexOf(n, t), n.splice(t, 1)), this;
        try {
          delete this.eventsList[e];
        } catch (e) {}
        return null;
      }
    }, e.prototype.clear = function () {
      this.eventsList = {};
    }, e),
    o = function o(t) {
      try {
        return encodeURIComponent(decodeURIComponent(t));
      } catch (e) {
        return t;
      }
    };
  function e() {
    var s = this;
    this.emit = function (e, t) {
      if (s) {
        var n;
        if (null != (i = s.eventsList[e]) && i.length) for (var i = i.slice(), o = 0; o < i.length; o++) {
          n = i[o];
          try {
            var r = n.callback.apply(s, [t]);
            if (1 === n.type && s.remove(e, n.callback), !1 === r) break;
          } catch (e) {
            throw e;
          }
        }
        return s;
      }
    }, this.eventsList = {};
  }
  (B = f = f || {})[B.number = -1] = "number", B.string = "";
  function p(e) {
    return "string" == typeof e && e.startsWith("//") ? "undefined" != typeof location && "https:" === location.protocol : /^https/.test(e);
  }
  var d,
    h,
    g,
    v = ["application/octet-stream", "application/xhtml+xml", "application/xml", "application/pdf", "application/pkcs12", "application/javascript", "application/ecmascript", "application/vnd.mspowerpoint", "application/ogg", "text/html", "text/css", "text/javascript", "image", "audio", "video"],
    y = ["ret", "retcode", "code", "errcode"],
    r = function r(e) {
      if ("string" == typeof e) return e;
      try {
        return (JSON.stringify(e, (i = [], o = [], function (e, t) {
          if (t instanceof Error) return "Error.message: " + t.message + " \n  Error.stack: " + t.stack;
          if ("object" == _typeof(t) && null !== t) {
            var n = i.indexOf(t);
            if (-1 !== n) return "[Circular " + o[n] + "]";
            i.push(t), o.push(e || "root");
          }
          return t;
        }), 4) || "undefined").replace(/"/gim, "");
      } catch (e) {
        return "error happen when aegis stringify: \n " + e.message + " \n " + e.stack;
      }
      var i, o;
    },
    m = function m(n, i) {
      void 0 === i && (i = 3);
      var o,
        r,
        s,
        l = "";
      return Array.isArray(n) ? (l += "[", o = n.length, n.forEach(function (e, t) {
        l += "object" == _typeof(e) && 1 < i ? m(e, i - 1) : O(e), l += t === o - 1 ? "" : ",";
      }), l += "]") : n instanceof Object ? (l = "{", r = Object.keys(n), s = r.length, r.forEach(function (e, t) {
        "object" == _typeof(n[e]) && 1 < i ? l += '"' + e + '":' + m(n[e], i - 1) : l += b(e, n[e]), l += t === s - 1 || t < s - 1 && void 0 === n[r[t + 1]] ? "" : ",";
      }), l += "}") : l += n, l;
    },
    b = function b(e, t) {
      var n = _typeof(t),
        i = "";
      return "string" == n || "object" == n ? i += '"' + e + '":"' + t + '"' : "function" == typeof t ? i += '"' + e + '":"function ' + t.name + '"' : "symbol" == _typeof(t) ? i += '"' + e + '":"symbol"' : "number" != typeof t && "boolean" != n || (i += '"' + e + '": ' + t), i;
    },
    O = function O(e) {
      var t = _typeof(e);
      return "" + ("undefined" == t || "symbol" == t || "function" == t ? "null" : "string" == t || "object" == t ? '"' + e + '"' : e);
    },
    w = (t.prototype.sourceURL = function () {
      return this.data.url;
    }, t.prototype.status = function () {
      return Number(this.data.status);
    }, t.prototype.headers = function () {
      var n = {};
      return this.data.headers.forEach(function (e, t) {
        n[t] = e;
      }), n;
    }, t);
  function t(e, t) {
    this.type = "fetch", this.data = e || {}, this.data.response = t;
  }
  (j = d = d || {}).INFO_ALL = "-1", j.API_RESPONSE = "1", j.INFO = "2", j.ERROR = "4", j.PROMISE_ERROR = "8", j.AJAX_ERROR = "16", j.SCRIPT_ERROR = "32", j.IMAGE_ERROR = "64", j.CSS_ERROR = "128", j.CONSOLE_ERROR = "256", j.MEDIA_ERROR = "512", j.RET_ERROR = "1024", j.REPORT = "2048", j.PV = "4096", j.EVENT = "8192", j.PAGE_NOT_FOUND_ERROR = "16384", (M = {})[M.android = 1] = "android", M[M.ios = 2] = "ios", M[M.windows = 3] = "windows", M[M.macos = 4] = "macos", M[M.linux = 5] = "linux", M[M.devtools = 6] = "devtools", M[M.other = 100] = "other", (V = {})[V.unknown = 100] = "unknown", V[V.wifi = 1] = "wifi", V[V.net2g = 2] = "net2g", V[V.net3g = 3] = "net3g", V[V.net4g = 4] = "net4g", V[V.net5g = 5] = "net5g", V[V.net6g = 6] = "net6g", (B = h = h || {}).LOG = "log", B.SPEED = "speed", B.PERFORMANCE = "performance", B.OFFLINE = "offline", B.WHITE_LIST = "whiteList", B.VITALS = "vitals", B.PV = "pv", B.CUSTOM_PV = "customPV", B.EVENT = "event", B.CUSTOM = "custom", B.SDK_ERROR = "sdkError", B.SET_DATA = "setData", (j = g = g || {}).production = "production", j.gray = "gray", j.pre = "pre", j.daily = "daily", j.local = "local", j.test = "test", j.others = "others";
  function l(e) {
    return e.filter(function (n, i) {
      return "static" !== n.type || !e.find(function (e, t) {
        return i !== t && n.url === e.url && 200 === n.status;
      });
    });
  }
  function x(e) {
    e.level === d.INFO_ALL && (e.level = d.INFO);
  }
  function R(o) {
    return function (e, t) {
      o.lifeCycle.emit("modifyRequest", e);
      var n = o.config.modifyRequest;
      if ("function" == typeof n) try {
        var i = n(e);
        "object" == _typeof(i) && "url" in i && (e = i);
      } catch (e) {
        console.error(e);
      }
      t(e);
    };
  }
  function E(i) {
    return function (e, t) {
      i.lifeCycle.emit("afterRequest", e);
      var n = i.config.afterRequest;
      "function" == typeof n && !1 === n(e) || t(e);
    };
  }
  function P() {}
  function L(t, n) {
    Object.getOwnPropertyNames(t).forEach(function (e) {
      "function" == typeof t[e] && "constructor" !== e && (n ? n[e] = "sendPipeline" === e ? function () {
        return function () {};
      } : function () {} : t[e] = function () {});
    });
  }
  function T(n) {
    var t,
      e = this,
      i = (r = n.config).uin,
      o = r.id,
      r = r.offlineLogLimit;
    i && o && (t = new G({
      limit: void 0 === r ? 2e4 : r
    }), n.lifeCycle.on("beforeWrite", function (e) {
      t.save2Offline(e = void 0 === e ? [] : e, n.config);
    }), n.getOfflineLog = function () {
      var e = t.getLogs({
        uin: i,
        id: o
      });
      return t.clearLogs(), e;
    }, n.uploadOfflineLogs = function (t) {
      return s(e, void 0, void 0, function () {
        return a(this, function (e) {
          return t = Array.isArray(t) ? t : [t], [2, H(n, t)];
        });
      });
    });
  }
  var S,
    C,
    j,
    U = function U(i, o) {
      return function (e, t) {
        var n = o.logCreated;
        if ("function" != typeof n) return i("beforeWrite", e), t(e);
        e = e.filter(function (e) {
          return !1 !== n(e);
        });
        return i("beforeWrite", e), t(e);
      };
    },
    A = function A(e, n) {
      var i,
        o = [],
        r = e.config;
      return e.lifeCycle.on("destroy", function () {
        o.length = 0;
      }), function (e, t) {
        if (o.push(e), n && o.length >= n) return o = l(o), t(o.splice(0, o.length)), void (i && clearTimeout(i));
        i && clearTimeout(i), i = setTimeout(function () {
          i = null, 0 < (o = l(o)).length && t(o.splice(0, o.length));
        }, r.delay);
      };
    },
    I = function I(e, t) {
      return Array.isArray(e) ? t(e.map(function (e) {
        return _u(_u({}, e), {
          msg: "string" == typeof e.msg ? e.msg : [].concat(e.msg).map(r).join(" ")
        });
      })) : t(_u(_u({}, e), {
        msg: "string" == typeof e.msg ? e.msg : r(e.msg)
      }));
    },
    _ = function _(s) {
      var e,
        l = !1,
        u = !1,
        t = !1,
        a = [];
      return s.lifeCycle.on("onConfigChange", function () {
        e && clearTimeout(e), e = setTimeout(function () {
          var e, n;
          !t && s.config && (t = !0, e = s.config.whiteListUrl, (n = void 0 === e ? "" : e) && s.sendPipeline([function (e, t) {
            t({
              url: n,
              type: h.WHITE_LIST,
              success: function success(e) {
                u = !0;
                try {
                  var t = e.data || JSON.parse(e),
                    n = t.retcode,
                    i = t.result,
                    o = void 0 === i ? {} : i;
                  if (0 === n) {
                    if (l = o.is_in_white_list, s.isWhiteList = l, o.shutdown) return void s.destroy();
                    0 <= o.rate && o.rate <= 1 && (s.config.random = o.rate, s.isGetSample = !1);
                  }
                  s.isWhiteList && a.length ? F(s)(a.splice(0), function () {}) : !s.isWhiteList && a.length && (a.length = 0);
                  var r = s.config.onWhitelist;
                  "function" == typeof r && r(l);
                } catch (e) {}
              },
              fail: function fail(e) {
                "403 forbidden" === e && s.destroy(), u = !0;
              }
            });
          }], h.WHITE_LIST)(null), t = !1);
        }, s.config.uin ? 50 : 500);
      }), s.lifeCycle.on("destroy", function () {
        a.length = 0;
      }), function (e, t) {
        var n;
        l || null !== (n = null === (n = s.config) || void 0 === n ? void 0 : n.api) && void 0 !== n && n.reportRequest ? t(e.concat(a.splice(0)).map(function (e) {
          return x(e), e;
        })) : (e = e.filter(function (e) {
          return e.level !== d.INFO && e.level !== d.API_RESPONSE ? (x(e), !0) : (u || (a.push(e), 200 <= a.length && (a.length = 200)), !1);
        })).length && t(e);
      };
    },
    N = function N(i) {
      return setTimeout(function () {
        var e = i.config.pvUrl,
          n = void 0 === e ? "" : e;
        n && i.sendPipeline([function (e, t) {
          t({
            url: n,
            type: h.PV,
            fail: function fail(e) {
              "403 forbidden" === e && i.destroy();
            }
          });
        }], h.PV)(null);
      }, 100), function (e, t) {
        t(e);
      };
    },
    k = function k(o) {
      var r = {};
      return function (e, t) {
        var n, i;
        o.speedSample ? (i = "object" == _typeof(o.repeat) ? o.repeat : {
          repeat: o.repeat
        }, n = +i.speed || +i.repeat || 5, Array.isArray(e) ? (i = e.filter(function (e) {
          var t = !r[e.url] || r[e.url] < n;
          return r[e.url] = 1 + ~~r[e.url], t;
        })).length && t(i) : (!r[e.url] || r[e.url] < n) && (r[e.url] = 1 + ~~r[e.url], t(e))) : t(e);
      };
    },
    D = function D(i) {
      var o = {};
      return function (e, t) {
        var n = "number" == typeof i.repeat ? i.repeat : 5;
        if (0 === n) return t(e);
        t(e.filter(function (e) {
          return e.level !== d.ERROR && e.level !== d.PROMISE_ERROR && e.level !== d.AJAX_ERROR && e.level !== d.SCRIPT_ERROR && e.level !== d.IMAGE_ERROR && e.level !== d.CSS_ERROR && e.level !== d.MEDIA_ERROR || (o[e.msg] = o[e.msg] || 0, o[e.msg] += 1, !(o[e.msg] > n));
        }));
      };
    },
    F = function F(i) {
      return function (e) {
        return i.sendPipeline([function (e, n) {
          return n({
            url: i.config.url || "",
            data: (t = e, (t = Array.isArray(t) ? t : [t]).map(function (t, n) {
              return Object.getOwnPropertyNames(t).map(function (e) {
                return o(e) + "[" + n + "]=" + (void 0 === t[e] ? "" : o(t[e]));
              }).join("&");
            }).join("&") + (t.length ? "&count=" + t.length : "")),
            method: "post",
            contentType: "application/x-www-form-urlencoded",
            type: h.LOG,
            log: e,
            requestConfig: {
              timeout: 5e3
            },
            success: function success() {
              var t = i.config.onReport;
              "function" == typeof t && e.forEach(function (e) {
                t(e);
              }), "function" == typeof n && n([]);
            },
            fail: function fail(e) {
              "403 forbidden" === e && i.destroy();
            }
          });
          var t;
        }], h.LOG)(e);
      };
    },
    q = function q(n) {
      if (!n || !n.reduce || !n.length) throw new TypeError("createPipeline need at least one function param");
      return 1 === n.length ? function (e, t) {
        n[0](e, t || P);
      } : n.reduce(function (n, i) {
        return function (e, t) {
          return void 0 === t && (t = P), n(e, function (e) {
            return null == i ? void 0 : i(e, t);
          });
        };
      });
    },
    M = (Object.defineProperty(te.prototype, "__version__", {
      get: function get() {
        return console.warn("__version__ has discard, please use version"), "1.24.45";
      },
      enumerable: !1,
      configurable: !0
    }), Object.defineProperty(te.prototype, "LogType", {
      get: function get() {
        return console.warn("LogType has discard, please use logType"), d;
      },
      enumerable: !1,
      configurable: !0
    }), te.prototype.init = function (e) {
      this.setConfig(e);
      for (var t = 0; t < te.installedPlugins.length; t++) {
        try {
          te.installedPlugins[t].patch(this);
        } catch (e) {
          this.sendSDKError(e);
        }
      }
      this.lifeCycle.emit("onInited");
    }, te.prototype.setConfig = function (e) {
      Object.assign(this.config, e);
      var t = this.config,
        n = t.id,
        i = t.uin,
        o = t.version,
        r = t.ext1,
        s = t.ext2,
        l = t.ext3,
        e = t.aid,
        t = t.env,
        u = void 0 === t ? "production" : t,
        t = this.bean.id !== n || this.bean.uin !== i || this.bean.aid !== e;
      return this.bean.id = n || "", this.bean.uin = i || "", this.bean.version = o || "1.24.45", this.bean.aid = e || "", this.bean.env = function () {
        switch (u) {
          case g.production:
          case g.gray:
          case g.pre:
          case g.daily:
          case g.local:
          case g.others:
            return 1;
          default:
            return;
        }
      }() ? u : g.others, r && (this.bean.ext1 = encodeURIComponent(r)), s && (this.bean.ext2 = encodeURIComponent(s)), l && (this.bean.ext3 = encodeURIComponent(l)), t && this.lifeCycle.emit("onConfigChange", this.config), this.config;
    }, te.use = function (e) {
      -1 === te.installedPlugins.indexOf(e) && e.aegisPlugin && te.installedPlugins.push(e);
    }, te.unuse = function (e) {
      e = te.installedPlugins.indexOf(e);
      -1 !== e && te.installedPlugins.splice(e, 1);
    }, te.prototype.info = function () {
      for (var e = [], t = 0; t < arguments.length; t++) {
        e[t] = arguments[t];
      }
      var n = {
        level: d.INFO,
        msg: e
      };
      1 === e.length && e[0].msg && Object.assign(n, _u({}, e[0]), {
        level: d.INFO
      }), this.normalLogPipeline(n);
    }, te.prototype.infoAll = function () {
      for (var e = [], t = 0; t < arguments.length; t++) {
        e[t] = arguments[t];
      }
      var n = {
        level: d.INFO_ALL,
        msg: e
      };
      1 === e.length && e[0].msg && Object.assign(n, _u({}, e[0]), {
        level: d.INFO_ALL
      }), this.normalLogPipeline(n);
    }, te.prototype.report = function () {
      for (var e = [], t = 0; t < arguments.length; t++) {
        e[t] = arguments[t];
      }
      var n = {
        level: d.REPORT,
        msg: e
      };
      1 === e.length && e[0].msg && Object.assign(n, _u({}, e[0])), this.normalLogPipeline(n);
    }, te.prototype.error = function () {
      for (var e = [], t = 0; t < arguments.length; t++) {
        e[t] = arguments[t];
      }
      var n = {
        level: d.ERROR,
        msg: e
      };
      1 === e.length && e[0].msg && Object.assign(n, _u({}, e[0]), {
        level: d.ERROR
      }), this.normalLogPipeline(n);
    }, te.prototype.speedLogPipeline = function (e) {
      throw new Error('You need to override "speedLogPipeline" method');
    }, te.prototype.reportPv = function (n) {
      var i,
        o = this;
      n && (console.warn("reportPv is deprecated, please use reportEvent"), i = "" + Object.getOwnPropertyNames(this.bean).filter(function (e) {
        return "id" !== e;
      }).map(function (e) {
        return e + "=" + o.bean[e];
      }).join("&"), this.sendPipeline([function (e, t) {
        t({
          url: o.config.url + "/" + n + "?" + i,
          addBean: !1,
          type: h.CUSTOM_PV,
          fail: function fail(e) {
            "403 forbidden" === e && o.destroy();
          }
        });
      }], h.CUSTOM_PV)(null));
    }, te.prototype.reportEvent = function (e) {
      e && ((e = "string" == typeof e ? {
        name: e,
        ext1: this.config.ext1 || "",
        ext2: this.config.ext2 || "",
        ext3: this.config.ext3 || ""
      } : e).name ? this.eventPipeline(e) : console.warn("reportEvent params error"));
    }, te.prototype.reportTime = function (e, t) {
      if ("object" == _typeof(e)) return this.reportT(e);
      "string" == typeof e ? "number" == typeof t ? t < 0 || 6e4 < t ? console.warn("reportTime: duration must between 0 and 60000") : this.submitCustomTime(e, t) : console.warn("reportTime: second param must be number") : console.warn("reportTime: first param must be a string");
    }, te.prototype.reportT = function (e) {
      var t = e.name,
        n = e.duration,
        i = e.ext1,
        o = void 0 === i ? "" : i,
        r = e.ext2,
        i = void 0 === r ? "" : r,
        r = e.ext3,
        r = void 0 === r ? "" : r,
        e = e.from;
      if ("string" == typeof t && "number" == typeof n && "string" == typeof o && "string" == typeof i && "string" == typeof r) {
        if (!(n < 0 || 6e4 < n)) return this.submitCustomTime(t, n, o, i, r, void 0 === e ? "" : e);
        console.warn("reportTime: duration must between 0 and 60000");
      } else console.warn("reportTime: params error");
    }, te.prototype.time = function (e) {
      "string" == typeof e ? this.timeMap[e] ? console.warn("Timer " + e + " already exists") : this.timeMap[e] = Date.now() : console.warn("time: first param must be a string");
    }, te.prototype.timeEnd = function (e) {
      "string" == typeof e ? this.timeMap[e] ? (this.submitCustomTime(e, Date.now() - this.timeMap[e]), delete this.timeMap[e]) : console.warn("Timer " + e + " does not exist") : console.warn("timeEnd: first param must be a string");
    }, te.prototype.submitCustomTime = function (e, t, n, i, o, r) {
      this.customTimePipeline({
        name: e,
        duration: t,
        ext1: n || this.config.ext1,
        ext2: i || this.config.ext2,
        ext3: o || this.config.ext3,
        from: r || void 0
      });
    }, te.prototype.extendBean = function (e, t) {
      this.bean[e] = t;
    }, te.prototype.sendPipeline = function (e, t) {
      var n,
        r,
        s,
        l = this;
      return q(c([function (e, t) {
        if ("number" != typeof n.config.random && (console.warn("random must in [0, 1], default is 1."), n.config.random = 1), !n.isHidden || !n.isGetSample) if (n.isGetSample) n.isHidden || t(e);else {
          if (n.isGetSample = !0, Math.random() < n.config.random) return n.isHidden = !1, t(e);
          n.isHidden = !0;
        }
      }, (s = t, function (e, t) {
        var n = Array.isArray(e),
          i = n ? e : [e];
        r.lifeCycle.emit("beforeRequest", e);
        var o = r.config.beforeRequest;
        (i = "function" == typeof o ? i.map(function (e) {
          try {
            var t = o({
              logs: e,
              logType: s
            });
            return (null == t ? void 0 : t.logType) === s && null != t && t.logs ? t.logs : !1 !== t && e;
          } catch (t) {
            return e;
          }
        }).filter(function (e) {
          return !1 !== e;
        }) : i).length && (i = function (e, t) {
          if (!Array.isArray(e) || e.length <= 1) return e;
          var n = [],
            i = [];
          return !(i = "string" == typeof t ? [t] : t) || i.length <= 0 || (i.forEach(function (t) {
            e.forEach(function (e) {
              null != e && e[t] && n.push(t);
            });
          }), 0 < n.length && (e = e.map(function (e) {
            var t = {};
            return n.forEach(function (e) {
              t[e] = "";
            }), _u(_u({}, t), e);
          }))), e;
        }(i, ["ext1", "ext2", "ext3"]), t(n ? i : i[0]));
      })], e, [R(r = n = this), function (i, o) {
        l.request(i, function () {
          for (var e, t = [], n = 0; n < arguments.length; n++) {
            t[n] = arguments[n];
          }
          o({
            isErr: !1,
            result: t,
            logType: null == i ? void 0 : i.type,
            logs: null == i ? void 0 : i.log
          }), null === (e = null == i ? void 0 : i.success) || void 0 === e || e.call.apply(e, c([i], t));
        }, function () {
          for (var e, t = [], n = 0; n < arguments.length; n++) {
            t[n] = arguments[n];
          }
          o({
            isErr: !0,
            result: t,
            logType: null == i ? void 0 : i.type,
            logs: null == i ? void 0 : i.log
          }), null === (e = null == i ? void 0 : i.fail) || void 0 === e || e.call.apply(e, c([i], t));
        });
      }, E(this)]));
    }, te.prototype.send = function (e, o, r) {
      var t = this;
      return q([R(this), function (n, i) {
        t.request(n, function () {
          for (var e = [], t = 0; t < arguments.length; t++) {
            e[t] = arguments[t];
          }
          i({
            isErr: !1,
            result: e,
            logType: n.type,
            logs: n.log
          }), null == o || o.apply(void 0, e);
        }, function () {
          for (var e = [], t = 0; t < arguments.length; t++) {
            e[t] = arguments[t];
          }
          i({
            isErr: !0,
            result: e,
            logType: n.type,
            logs: n.log
          }), null == r || r.apply(void 0, e);
        });
      }, E(this)])(e);
    }, te.prototype.request = function (e, t, n) {
      throw new Error('You need to override "request" method');
    }, te.prototype.sendSDKError = function (e) {
      var n = this;
      this.sendPipeline([function (e, t) {
        t({
          url: n.config.url + "?id=1085&msg[0]=" + encodeURIComponent(r(e)) + "&level[0]=2&from=" + n.config.id + "&count=1&version=" + n.config.id + "(1.24.45)",
          addBean: !1,
          method: "get",
          type: h.SDK_ERROR,
          log: e
        });
      }], h.SDK_ERROR)(e);
    }, te.prototype.destroy = function (e) {
      void 0 === e && (e = !1);
      var t,
        n,
        i = te.instances.indexOf(this);
      -1 !== i && te.instances.splice(i, 1);
      for (var o = te.installedPlugins.length - 1; 0 <= o; o--) {
        try {
          te.installedPlugins[o].unpatch(this);
        } catch (e) {
          this.sendSDKError(e);
        }
      }
      if (this.lifeCycle.emit("destroy"), this.lifeCycle.clear(), e) t = this, n = Object.getOwnPropertyDescriptors(t), Object.keys(n).forEach(function (e) {
        n[e].writable && (t[e] = null);
      }), Object.setPrototypeOf(this, null);else {
        for (var r = this; r.constructor !== Object && L(r, this), r = Object.getPrototypeOf(r);) {
          ;
        }
        0 === te.instances.length && (e = Object.getPrototypeOf(this).constructor, L(e), L(te));
      }
    }, te.version = "1.24.45", te.instances = [], te.logType = d, te.environment = g, te.installedPlugins = [], te),
    V = (ee.prototype.patch = function (e) {
      this.canUse(e) && this.exist(e) && (this.instances.push(e), this.triggerInit(e), this.triggerOnNewAegis(e));
    }, ee.prototype.unpatch = function (e) {
      e = this.instances.indexOf(e);
      -1 !== e && this.instances.splice(e, 1);
    }, ee.prototype.countInstance = function () {
      return this.instances.length;
    }, ee.prototype.uninstall = function () {
      var e;
      null === (e = null === (e = this.option) || void 0 === e ? void 0 : e.destroy) || void 0 === e || e.apply(this);
    }, ee.prototype.walk = function (n) {
      var i = this;
      this.instances.forEach(function (e) {
        var t = i.canUse(e);
        t && n(e, t);
      });
    }, ee.prototype.canUse = function (e) {
      e = this.getConfig(e);
      return !(!e || "object" != _typeof(e)) || !!e;
    }, ee.prototype.getConfig = function (e) {
      return null === (e = e.config) || void 0 === e ? void 0 : e[this.name];
    }, ee.prototype.exist = function (e) {
      return -1 === this.instances.indexOf(e);
    }, ee.prototype.triggerInit = function (e) {
      var t;
      this.inited || (this.inited = !0, null === (t = null === (t = this.option) || void 0 === t ? void 0 : t.init) || void 0 === t || t.call(this.option, this.getConfig(e)));
    }, ee.prototype.triggerOnNewAegis = function (e) {
      var t;
      null === (t = null === (t = this.option) || void 0 === t ? void 0 : t.onNewAegis) || void 0 === t || t.call(this.option, e, this.getConfig(e));
    }, ee),
    H = function H(l, u) {
      return s(this, void 0, void 0, function () {
        var t, n, i, o, r, s;
        return a(this, function (e) {
          return t = l.config, r = l.bean, n = t.offlineUrl, i = t.id, o = t.uin, r = (r || {}).aid, s = void 0 === r ? "" : r, (o || s) && i && l.send({
            url: n + "/offlineAuto",
            type: h.OFFLINE,
            log: h.OFFLINE
          }, function (e) {
            e = (null == e ? void 0 : e.data).secretKey;
            if (e) try {
              l.send({
                url: n + "/offlineLog",
                data: {
                  logs: u,
                  secretKey: e,
                  id: i,
                  uin: o,
                  aid: s
                },
                contentType: "application/json",
                method: "post",
                type: h.OFFLINE,
                log: h.OFFLINE
              });
            } catch (e) {
              console.error(e);
            }
          }, function (e) {
            console.error(e);
          }), [2];
        });
      });
    },
    G = function G(e) {
      var i = this,
        e = (void 0 === e ? {} : e).limit,
        e = void 0 === e ? 2e4 : e;
      this.offlineBuffer = [], this.clearLogs = function () {
        i.offlineBuffer = [];
      }, this.save2Offline = function (e, t) {
        e = (e = Array.isArray(e) ? e : [e]).map(function (e) {
          return "string" == typeof e && (e = {
            msg: e
          }), Object.assign({
            id: t.id,
            uin: t.uin,
            time: +Date.now(),
            version: t.version
          }, e);
        });
        i.offlineBuffer = i.offlineBuffer.concat(e).slice(0, i.limitSize);
      }, this.getLogs = function (t, e) {
        var n = i.offlineBuffer.filter(function (e) {
          return e.id === t.id && e.uin === t.uin;
        });
        return null == e || e(n), n;
      }, this.limitSize = e;
    },
    J = new V({
      name: "offlineLog",
      onNewAegis: function onNewAegis(t) {
        T(t), t.lifeCycle.on("onConfigChange", function (e) {
          T(t);
        });
      }
    }),
    B = "undefined" != typeof weex && (null === weex || void 0 === weex ? void 0 : weex.requireModule("stream")),
    W = null == B ? void 0 : B.fetch,
    K = function K(e) {
      var t = e.ok,
        n = e.status,
        i = e.statusText,
        o = e.data,
        r = e.headers;
      return {
        ok: t,
        status: n,
        statusText: i,
        body: o,
        headers: r,
        clone: function clone() {
          return K(e);
        },
        text: function text() {
          return new Promise(function (e) {
            return e(o);
          });
        },
        json: function json() {
          return new Promise(function (e, t) {
            try {
              e(JSON.parse(o));
            } catch (e) {
              t(new Error("response body is not JSON"));
            }
          });
        }
      };
    },
    $ = function $(e, i) {
      return void 0 === i && (i = {}), new Promise(function (t, n) {
        W || n(new Error("no available fetch found!"));
        try {
          W(_u(_u({}, i), {
            url: e,
            type: "text"
          }), function (e) {
            return t(K(e));
          });
        } catch (e) {
          n(e);
        }
      });
    },
    X = [],
    z = {},
    Y = weex,
    B = (_n(C = Q, j = S = M), C.prototype = null === j ? Object.create(j) : (Z.prototype = j.prototype, new Z()), Q.prototype.getPlatform = function () {
      var e = ((null === (e = null === (e = null === (e = this.weex) || void 0 === e ? void 0 : e.config) || void 0 === e ? void 0 : e.env) || void 0 === e ? void 0 : e.osName) || "").toLowerCase();
      return "android" === e ? 1 : "ios" === e ? 2 : 100;
    }, Q.prototype.initOfflineLog = function () {
      Q.use(J);
    }, Object.defineProperty(Q.prototype, "getBean", {
      get: function get() {
        var t = this;
        return Object.getOwnPropertyNames(this.bean).map(function (e) {
          return e + "=" + t.bean[e];
        }).join("&");
      },
      enumerable: !1,
      configurable: !0
    }), Q.prototype.reportSpeed = function (e) {
      var t, n, i, o, r;
      this.send({
        url: "" + this.config.speedUrl,
        method: "post",
        data: (t = e, n = this.bean, o = {
          fetch: [],
          static: []
        }, r = {}, Array.isArray(t) ? t.forEach(function (e) {
          var t;
          null === (t = o[e.type]) || void 0 === t || t.push(e);
        }) : null === (i = o[t.type]) || void 0 === i || i.push(t), r.payload = JSON.stringify(_u({
          duration: o
        }, n)), r),
        contentType: "application/json",
        type: h.SPEED,
        log: e
      });
    }, Q.prototype.retcode = function (e) {
      var t = this;
      "object" == _typeof(e) ? e.url ? 400 <= Object.keys(z).length || (z[e.url] || (z[e.url] = 0), z[e.url] += 1, 3 < z[e.url]) || (e = Object.assign({}, {
        url: "",
        isHttps: !0,
        method: "GET",
        type: "fetch",
        duration: 0,
        ret: 0,
        status: 200
      }, e), X.push(e), 1 === X.length && setTimeout(function () {
        t.reportSpeed(X), X = [];
      }, 1e3)) : console.error("param url can not be empty!") : console.error("retcode params should be an objcet");
    }, Q.prototype.uploadLogs = function (e, t) {
      this.lifeCycle.emit("uploadLogs", e = void 0 === e ? {} : e, t = void 0 === t ? {} : t);
    }, Q.prototype.getOfflineLog = function () {}, Q.prototype.uploadOfflineLogs = function (e) {}, Q.sessionID = "session-" + Date.now(), Q),
    M = new V({
      name: "aid",
      onNewAegis: function onNewAegis(t) {
        var e,
          n = this;
        this.aid && !0 !== this.aid ? (t.bean.aid = this.aid, t.config.aid = this.aid) : e = setTimeout(function () {
          n.getAid(function (e) {
            n.aid = e, t.bean.aid = n.aid, t.config.aid = n.aid;
          }), clearTimeout(e);
        }, 0);
      },
      getAid: function getAid(n) {
        return s(this, void 0, void 0, function () {
          var t;
          return a(this, function (e) {
            switch (e.label) {
              case 0:
                return e.trys.push([0, 2,, 3]), [4, localStorage.getItem("AEGIS_ID")];
              case 1:
                return (t = e.sent()) || (t = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (e) {
                  var t = 16 * Math.random() | 0;
                  return ("x" === e ? t : 3 & t | 8).toString(16);
                }), localStorage.setItem("AEGIS_ID", t)), null == n || n(t || ""), [3, 3];
              case 2:
                return e.sent(), null == n || n(""), [3, 3];
              case 3:
                return [2];
            }
          });
        });
      }
    }),
    j = new V({
      name: "onError",
      listening: !1,
      init: function init() {
        this.startListen();
      },
      startListen: function startListen() {
        var t = this;
        if (!this.listening) {
          this.listening = !0;
          try {
            var e = Vue;
            if (void 0 === e || !e) return;
            var n = e.config.errorHandler;
            e.config.errorHandler = function (e) {
              e && t.publishErrorLog({
                msg: e,
                level: d.ERROR
              }), "function" == typeof n && n(e);
            };
          } catch (t) {
            this.publishErrorLog({
              msg: t,
              level: d.ERROR
            });
          }
        }
      },
      publishErrorLog: function publishErrorLog(t) {
        this.$walk(function (e) {
          e.normalLogPipeline(t);
        });
      }
    }),
    V = new V({
      name: "reportApiSpeed",
      override: !1,
      fetch: null,
      weexFetch: null,
      onNewAegis: function onNewAegis(e) {
        var t, n;
        this.override || (this.override = !0, this.fetch = e.fetch, this.weexFetch = e.weexFetch, t = (n = this.overrideFetch(e.config)).fetch, n = n.weexFetch, e.fetch = t, e.weexFetch = n);
      },
      publishSpeed: function publishSpeed(t) {
        this.$walk(function (e) {
          e.speedLogPipeline(t);
        });
      },
      publishNormalLog: function publishNormalLog(t) {
        this.$walk(function (e) {
          e.normalLogPipeline(t);
        });
      },
      overrideFetch: function overrideFetch(a) {
        var c = this;
        if ("function" == typeof this.fetch) {
          var e = this.fetch,
            t = this.weexFetch,
            l = function l(o, r, s, _l) {
              var e, u, t, n, i;
              a.hostUrl && -1 < s.indexOf(a.hostUrl) || (u = {
                url: s,
                isHttps: p(s),
                method: (null == _l ? void 0 : _l.method) || "get",
                duration: r,
                type: "fetch",
                status: o.status
              }, "fetch" !== (n = "function" == typeof (null === (e = a.api) || void 0 === e ? void 0 : e.resourceTypeHandler) ? null === (e = a.api) || void 0 === e ? void 0 : e.resourceTypeHandler(o.url) : n) && "static" !== n && (t = "", o.headers && (t = o.headers["Content-Type"] || o.headers["content-type"]), n = o.ok && "string" == typeof t && (i = t, v.some(function (e) {
                return -1 !== i.indexOf(e);
              })) ? "static" : "fetch"), "fetch" === n ? o.clone().text().then(function (e) {
                var t = null === (i = a.api) || void 0 === i ? void 0 : i.apiDetail,
                  n = function (e, t, n) {
                    var i;
                    try {
                      if ("function" == typeof (null == t ? void 0 : t.retCodeHandler)) {
                        var o = t.retCodeHandler(e, null == n ? void 0 : n.url, null == n ? void 0 : n.ctx) || {};
                        return {
                          code: void 0 === (r = o.code) ? "unknown" : r,
                          isErr: o.isErr
                        };
                      }
                      "string" == typeof e && (e = JSON.parse(e)), "function" == typeof (null === (i = null == t ? void 0 : t.ret) || void 0 === i ? void 0 : i.join) && (y = [].concat(t.ret.map(function (e) {
                        return e.toLowerCase();
                      })));
                      var r,
                        s = Object.getOwnPropertyNames(e).filter(function (e) {
                          return -1 !== y.indexOf(e.toLowerCase());
                        });
                      return s.length ? {
                        code: "" + (r = Number(e[s[0]])),
                        isErr: 0 !== r
                      } : {
                        code: "unknown",
                        isErr: !1
                      };
                    } catch (e) {
                      return {
                        code: "unknown",
                        isErr: !1
                      };
                    }
                  }(e, a.api, {
                    url: o.url,
                    ctx: o
                  }) || {},
                  i = n.code,
                  n = n.isErr,
                  t = "req url: " + s + " \n                              \nreq method: " + ((null == _l ? void 0 : _l.method) || "get") + " \n                              \nreq param: " + (t ? m(null == _l ? void 0 : _l.body) : "") + " \n                              \nres duration: " + r + " \n                              \nres status: " + (o.status || 0) + "\n                              \nres retcode: " + i + "\n                              \nres data: " + (t ? e : "");
                c.publishNormalLog({
                  msg: t,
                  level: d.API_RESPONSE
                }), u.payload = new w(o, e), u.ret = i, u.isErr = +n, n && c.publishNormalLog({
                  msg: t,
                  level: d.RET_ERROR
                });
              }) : Object.assign(u, {
                type: "static",
                urlQuery: (t = o.url, n = !0, "string" == typeof t ? t.split("?")[n ? 1 : 0] || "" : t),
                domainLookup: f.number,
                connectTime: f.number
              }), c.publishSpeed(u));
            },
            n = function n(e, t, _n2, i) {
              if (a.hostUrl && -1 < _n2.indexOf(a.hostUrl)) throw e;
              var o = {
                url: _n2,
                isHttps: p(_n2),
                method: (null == i ? void 0 : i.method) || "get",
                duration: t,
                type: "fetch",
                status: 0
              };
              c.publishSpeed(o);
              o = null === (o = a.api) || void 0 === o ? void 0 : o.apiDetail, i = "AJAX_ERROR: " + e + "\n              \nres status: 0\n              \nres duration: " + t + "\n              \nreq url: " + _n2 + "\n              \nreq method: " + ((null == i ? void 0 : i.method) || "get") + "\n              \nreq param: " + (o ? m(null == i ? void 0 : i.body) : "");
              throw c.publishNormalLog({
                msg: i,
                level: d.AJAX_ERROR
              }), e;
            };
          return {
            fetch: function fetch(i, o) {
              var r = Date.now();
              return e(i, o).then(function (e) {
                var t = Date.now() - r,
                  n = e;
                try {
                  l(n, t, i, o);
                } catch (e) {}
                return e;
              }).catch(function (e) {
                var t = Date.now() - r;
                try {
                  n(e, t, i, o);
                } catch (e) {}
                throw e;
              });
            },
            weexFetch: function weexFetch(i, o, e) {
              var r = i.url,
                s = Date.now();
              try {
                t(i, function (e) {
                  var t = Date.now() - s,
                    n = K(e);
                  try {
                    l(n, t, r, i);
                  } catch (e) {}
                  "function" == typeof o && o(e);
                }, e);
              } catch (o) {
                e = Date.now() - s;
                try {
                  n(o, e, r, i);
                } catch (i) {}
                throw o;
              }
            }
          };
        }
      }
    });
  function Q(e) {
    var t,
      n,
      s = S.call(this, e) || this;
    s.weex = Y, s.originRequest = $, s.fetch = $, s.weexFetch = W, s.speedLogPipeline = q([k(s.config), A(s), function (e, t) {
      s.lifeCycle.emit("beforeReportSpeed", e);
      var n = s.config.beforeReportSpeed;
      if ((e = "function" == typeof n ? e.filter(function (e) {
        return !1 !== n(e);
      }) : e).length) return t(e);
    }, function (e) {
      s.reportSpeed(e);
    }]), s.request = function (i, o, r) {
      var e;
      i && "string" == typeof i.url && "" !== i.url && s.bean.id && (e = i.url, !1 !== i.addBean && (e = e + (-1 === e.indexOf("?") ? "?" : "&") + s.getBean), ("get" === (i.method || "get").toLocaleLowerCase() ? s.originRequest(function (e, n) {
        if ("string" != typeof e) return "";
        if ("object" == _typeof(n) && n) {
          var t = Object.getOwnPropertyNames(n).map(function (e) {
            var t = n[e];
            return e + "=" + ("string" == typeof t ? encodeURIComponent(t) : encodeURIComponent(JSON.stringify(t)));
          }).join("&").replace(/eval/gi, "evaI");
          return e + (-1 === e.indexOf("?") ? "?" : "&") + t;
        }
        return e;
      }(e, i.data), _u({
        method: "GET",
        headers: {}
      }, i.requestConfig)) : ("string" == typeof i.data ? i.data = i.data.replace(/eval/gi, "evaI") : i.data = JSON.stringify(i.data), s.originRequest(e, _u({
        method: "POST",
        body: i.data,
        headers: i.contentType ? {
          "Content-Type": i.contentType
        } : {}
      }, i.requestConfig)))).then(function (e) {
        var t, n;
        String(e.status).match(/^20\d+/g) ? null == o || o(e) : null == r || r(e), null === (n = (t = s.config).reqCallback) || void 0 === n || n.call(t, e, i);
      }).catch(function (e) {
        null == r || r(e);
      }));
    };
    try {
      e.offlineLog && s.initOfflineLog(), s.init(e), s.extendBean("sessionId", Q.sessionID), s.extendBean("platform", s.getPlatform()), s.extendBean("from", encodeURIComponent(e.pageUrl || (null === (n = null === (t = s.weex) || void 0 === t ? void 0 : t.config) || void 0 === n ? void 0 : n.bundleUrl) || "")), s.extendBean("referer", encodeURIComponent(e.referrer || ""));
    } catch (e) {
      console.warn(e), console.log("%cThe above error occurred in the process of initializing Aegis, which will affect your normal use of Aegis.\nIt is recommended that you contact aegis-helper for feedback and thank you for your support.", "color: red"), s.sendSDKError(e);
    }
    return s;
  }
  function Z() {
    this.constructor = C;
  }
  function ee(e) {
    this.aegisPlugin = !0, this.name = "", this.instances = [], this.inited = !1, e.$walk = this.walk.bind(this), e.$getConfig = this.getConfig.bind(this), this.option = e, this.name = e.name;
  }
  function te(e) {
    var n,
      t,
      o = this;
    this.isGetSample = !1, this.isHidden = !1, this.config = {
      version: 0,
      delay: 1e3,
      onError: !0,
      repeat: 5,
      random: 1,
      aid: !0,
      device: !0,
      pagePerformance: !0,
      webVitals: !0,
      speedSample: !0,
      hostUrl: "https://aegis.qq.com",
      env: "production",
      url: "",
      offlineUrl: "",
      whiteListUrl: "",
      pvUrl: "",
      speedUrl: "",
      customTimeUrl: "",
      performanceUrl: "",
      webVitalsUrl: "",
      eventUrl: ""
    }, this.isWhiteList = !1, this.lifeCycle = new i(), this.bean = {}, this.normalLogPipeline = q([A(this, 5), I, function (e, t) {
      var i = n.config;
      t(e = e.map(function (e) {
        var t,
          n = i.maxLength || 204800;
        try {
          if (!e.msg || e.msg.length <= n) return e;
          e.msg = null === (t = e.msg) || void 0 === t ? void 0 : t.substring(0, n);
        } catch (t) {
          e.msg = r(e.msg).substring(0, i.maxLength);
        }
        return e;
      }));
    }, D((n = this).config), U(this.lifeCycle.emit, this.config), N(this), _(this), function (e, t) {
      var n = JSON.parse(JSON.stringify(e));
      o.lifeCycle.emit("beforeReport", n);
      var i = o.config.beforeReport;
      (e = "function" == typeof i ? e.filter(function (e) {
        return !1 !== i(e);
      }) : e).length && t(e);
    }, F(this)]), this.eventPipeline = q([A(this, 5), function (e) {
      o.sendPipeline([function (e, t) {
        var n = e.map(function (e) {
          return {
            name: e.name,
            ext1: e.ext1 || o.config.ext1 || "",
            ext2: e.ext2 || o.config.ext2 || "",
            ext3: e.ext3 || o.config.ext3 || ""
          };
        });
        t({
          url: o.config.eventUrl + "?payload=" + encodeURIComponent(JSON.stringify(n)),
          type: h.EVENT,
          log: e,
          fail: function fail(e) {
            "403 forbidden" === e && o.destroy();
          }
        });
      }], h.EVENT)(e);
    }]), this.timeMap = {}, this.customTimePipeline = q([A(this, 5), function (e) {
      return o.sendPipeline([function (e, t) {
        t({
          url: o.config.customTimeUrl + "?payload=" + encodeURIComponent(JSON.stringify({
            custom: e
          })),
          type: h.CUSTOM,
          log: e,
          fail: function fail(e) {
            "403 forbidden" === e && o.destroy();
          }
        });
      }], h.CUSTOM)(e);
    }]), this.config = (t = this.config, void 0 === (e = e.hostUrl) && (e = "https://aegis.qq.com"), t.url = t.url || e + "/collect", t.offlineUrl = t.offlineUrl || e + "/offline", t.whiteListUrl = t.whiteListUrl || e + "/collect/whitelist", t.pvUrl = t.pvUrl || e + "/collect/pv", t.eventUrl = t.eventUrl || e + "/collect/events", t.speedUrl = t.speedUrl || e + "/speed", t.customTimeUrl = t.customTimeUrl || e + "/speed/custom", t.performanceUrl = t.performanceUrl || e + "/speed/performance", t.webVitalsUrl = t.webVitalsUrl || e + "/speed/webvitals", t.setDataReportUrl = t.SetDataReportUrl || e + "/speed/miniProgramData", t), te.instances.push(this);
  }
  return B.use(j), B.use(V), B.use(M), B;
});

/***/ }),
/* 31 */,
/* 32 */,
/* 33 */
/*!**********************************************************************************************************!*\
  !*** ./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/runtime/componentNormalizer.js ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode, /* vue-cli only */
  components, // fixed by xxxxxx auto components
  renderjs // fixed by xxxxxx renderjs
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // fixed by xxxxxx auto components
  if (components) {
    if (!options.components) {
      options.components = {}
    }
    var hasOwn = Object.prototype.hasOwnProperty
    for (var name in components) {
      if (hasOwn.call(components, name) && !hasOwn.call(options.components, name)) {
        options.components[name] = components[name]
      }
    }
  }
  // fixed by xxxxxx renderjs
  if (renderjs) {
    if(typeof renderjs.beforeCreate === 'function'){
			renderjs.beforeCreate = [renderjs.beforeCreate]
		}
    (renderjs.beforeCreate || (renderjs.beforeCreate = [])).unshift(function() {
      this[renderjs.__module] = this
    });
    (options.mixins || (options.mixins = [])).push(renderjs)
  }

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),
/* 34 */,
/* 35 */,
/* 36 */,
/* 37 */,
/* 38 */,
/* 39 */,
/* 40 */,
/* 41 */,
/* 42 */,
/* 43 */,
/* 44 */,
/* 45 */,
/* 46 */,
/* 47 */,
/* 48 */
/*!****************************************************************************!*\
  !*** E:/公司项目/e考监控/Api-Example/uni_modules/Lyuan-SignalR/js_sdk/signalr.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(uni) {var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;var _typeof = __webpack_require__(/*! @babel/runtime/helpers/typeof */ 13);
/** 
 * @overview ASP.NET Core SignalR JavaScript Client.
 * @version 0.0.0-DEV_BUILD.
 * @license
 * Copyright (c) .NET Foundation. All rights reserved.
 * Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
 */
(function (global, factory) {
  ( false ? undefined : _typeof(exports)) === "object" && typeof module !== "undefined" ? module.exports = factory() :  true ? !(__WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) :
				__WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : undefined;
})(this, function () {
  "use strict";

  function unwrapExports(x) {
    return x && x.__esModule && Object.prototype.hasOwnProperty.call(x, "default") ? x["default"] : x;
  }
  function createCommonjsModule(fn, module) {
    return module = {
      exports: {}
    }, fn(module, module.exports), module.exports;
  }
  var _extendStatics = function extendStatics(d, b) {
    _extendStatics = Object.setPrototypeOf || {
      __proto__: []
    } instanceof Array && function (d, b) {
      d.__proto__ = b;
    } || function (d, b) {
      for (var p in b) {
        if (b.hasOwnProperty(p)) d[p] = b[p];
      }
    };
    return _extendStatics(d, b);
  };
  function __extends(d, b) {
    _extendStatics(d, b);
    function __() {
      this.constructor = d;
    }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
  }
  var _assign = function __assign() {
    _assign = Object.assign || function __assign(t) {
      for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) {
          if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
      }
      return t;
    };
    return _assign.apply(this, arguments);
  };
  function __rest(s, e) {
    var t = {};
    for (var p in s) {
      if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
    }
    if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
      if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
    }
    return t;
  }
  function __decorate(decorators, target, key, desc) {
    var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
    if ((typeof Reflect === "undefined" ? "undefined" : _typeof(Reflect)) === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
      if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    }
    return c > 3 && r && Object.defineProperty(target, key, r), r;
  }
  function __param(paramIndex, decorator) {
    return function (target, key) {
      decorator(target, key, paramIndex);
    };
  }
  function __metadata(metadataKey, metadataValue) {
    if ((typeof Reflect === "undefined" ? "undefined" : _typeof(Reflect)) === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
  }
  function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P ? value : new P(function (resolve) {
        resolve(value);
      });
    }
    return new (P || (P = Promise))(function (resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  }
  function __generator(thisArg, body) {
    var _ = {
        label: 0,
        sent: function sent() {
          if (t[0] & 1) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      },
      f,
      y,
      t,
      g;
    return g = {
      next: verb(0),
      throw: verb(1),
      return: verb(2)
    }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
      return this;
    }), g;
    function verb(n) {
      return function (v) {
        return step([n, v]);
      };
    }
    function step(op) {
      if (f) throw new TypeError("Generator is already executing.");
      while (_) {
        try {
          if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          if (y = 0, t) op = [op[0] & 2, t.value];
          switch (op[0]) {
            case 0:
            case 1:
              t = op;
              break;
            case 4:
              _.label++;
              return {
                value: op[1],
                done: false
              };
            case 5:
              _.label++;
              y = op[1];
              op = [0];
              continue;
            case 7:
              op = _.ops.pop();
              _.trys.pop();
              continue;
            default:
              if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                _ = 0;
                continue;
              }
              if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
                _.label = op[1];
                break;
              }
              if (op[0] === 6 && _.label < t[1]) {
                _.label = t[1];
                t = op;
                break;
              }
              if (t && _.label < t[2]) {
                _.label = t[2];
                _.ops.push(op);
                break;
              }
              if (t[2]) _.ops.pop();
              _.trys.pop();
              continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [6, e];
          y = 0;
        } finally {
          f = t = 0;
        }
      }
      if (op[0] & 5) throw op[1];
      return {
        value: op[0] ? op[1] : void 0,
        done: true
      };
    }
  }
  function __createBinding(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
  }
  function __exportStar(m, exports) {
    for (var p in m) {
      if (p !== "default" && !exports.hasOwnProperty(p)) exports[p] = m[p];
    }
  }
  function __values(o) {
    var s = typeof Symbol === "function" && Symbol.iterator,
      m = s && o[s],
      i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
      next: function next() {
        if (o && i >= o.length) o = void 0;
        return {
          value: o && o[i++],
          done: !o
        };
      }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
  }
  function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o),
      r,
      ar = [],
      e;
    try {
      while ((n === void 0 || n-- > 0) && !(r = i.next()).done) {
        ar.push(r.value);
      }
    } catch (error) {
      e = {
        error: error
      };
    } finally {
      try {
        if (r && !r.done && (m = i["return"])) m.call(i);
      } finally {
        if (e) throw e.error;
      }
    }
    return ar;
  }
  function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++) {
      ar = ar.concat(__read(arguments[i]));
    }
    return ar;
  }
  function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) {
      s += arguments[i].length;
    }
    for (var r = Array(s), k = 0, i = 0; i < il; i++) {
      for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++) {
        r[k] = a[j];
      }
    }
    return r;
  }
  function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
  }
  function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []),
      i,
      q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () {
      return this;
    }, i;
    function verb(n) {
      if (g[n]) i[n] = function (v) {
        return new Promise(function (a, b) {
          q.push([n, v, a, b]) > 1 || resume(n, v);
        });
      };
    }
    function resume(n, v) {
      try {
        step(g[n](v));
      } catch (e) {
        settle(q[0][3], e);
      }
    }
    function step(r) {
      r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r);
    }
    function fulfill(value) {
      resume("next", value);
    }
    function reject(value) {
      resume("throw", value);
    }
    function settle(f, v) {
      if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]);
    }
  }
  function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) {
      throw e;
    }), verb("return"), i[Symbol.iterator] = function () {
      return this;
    }, i;
    function verb(n, f) {
      i[n] = o[n] ? function (v) {
        return (p = !p) ? {
          value: __await(o[n](v)),
          done: n === "return"
        } : f ? f(v) : v;
      } : f;
    }
  }
  function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator],
      i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () {
      return this;
    }, i);
    function verb(n) {
      i[n] = o[n] && function (v) {
        return new Promise(function (resolve, reject) {
          v = o[n](v), settle(resolve, reject, v.done, v.value);
        });
      };
    }
    function settle(resolve, reject, d, v) {
      Promise.resolve(v).then(function (v) {
        resolve({
          value: v,
          done: d
        });
      }, reject);
    }
  }
  function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) {
      Object.defineProperty(cooked, "raw", {
        value: raw
      });
    } else {
      cooked.raw = raw;
    }
    return cooked;
  }
  function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) {
      if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    }
    result.default = mod;
    return result;
  }
  function __importDefault(mod) {
    return mod && mod.__esModule ? mod : {
      default: mod
    };
  }
  function __classPrivateFieldGet(receiver, privateMap) {
    if (!privateMap.has(receiver)) {
      throw new TypeError("attempted to get private field on non-instance");
    }
    return privateMap.get(receiver);
  }
  function __classPrivateFieldSet(receiver, privateMap, value) {
    if (!privateMap.has(receiver)) {
      throw new TypeError("attempted to set private field on non-instance");
    }
    privateMap.set(receiver, value);
    return value;
  }
  var tslib_1 = Object.freeze({
    __extends: __extends,
    get __assign() {
      return _assign;
    },
    __rest: __rest,
    __decorate: __decorate,
    __param: __param,
    __metadata: __metadata,
    __awaiter: __awaiter,
    __generator: __generator,
    __createBinding: __createBinding,
    __exportStar: __exportStar,
    __values: __values,
    __read: __read,
    __spread: __spread,
    __spreadArrays: __spreadArrays,
    __await: __await,
    __asyncGenerator: __asyncGenerator,
    __asyncDelegator: __asyncDelegator,
    __asyncValues: __asyncValues,
    __makeTemplateObject: __makeTemplateObject,
    __importStar: __importStar,
    __importDefault: __importDefault,
    __classPrivateFieldGet: __classPrivateFieldGet,
    __classPrivateFieldSet: __classPrivateFieldSet
  });
  var Errors = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var HttpError = function (_super) {
      tslib_1.__extends(HttpError, _super);
      function HttpError(errorMessage, statusCode) {
        var _newTarget = this.constructor;
        var _this = this;
        var trueProto = _newTarget.prototype;
        _this = _super.call(this, errorMessage) || this;
        _this.statusCode = statusCode;
        _this.__proto__ = trueProto;
        return _this;
      }
      return HttpError;
    }(Error);
    exports.HttpError = HttpError;
    var TimeoutError = function (_super) {
      tslib_1.__extends(TimeoutError, _super);
      function TimeoutError(errorMessage) {
        var _newTarget = this.constructor;
        if (errorMessage === void 0) {
          errorMessage = "A timeout occurred.";
        }
        var _this = this;
        var trueProto = _newTarget.prototype;
        _this = _super.call(this, errorMessage) || this;
        _this.__proto__ = trueProto;
        return _this;
      }
      return TimeoutError;
    }(Error);
    exports.TimeoutError = TimeoutError;
  });
  unwrapExports(Errors);
  var Errors_1 = Errors.HttpError;
  var Errors_2 = Errors.TimeoutError;
  var ILogger = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var LogLevel;
    (function (LogLevel) {
      LogLevel[LogLevel["Trace"] = 0] = "Trace";
      LogLevel[LogLevel["Debug"] = 1] = "Debug";
      LogLevel[LogLevel["Information"] = 2] = "Information";
      LogLevel[LogLevel["Warning"] = 3] = "Warning";
      LogLevel[LogLevel["Error"] = 4] = "Error";
      LogLevel[LogLevel["Critical"] = 5] = "Critical";
      LogLevel[LogLevel["None"] = 6] = "None";
    })(LogLevel = exports.LogLevel || (exports.LogLevel = {}));
  });
  unwrapExports(ILogger);
  var ILogger_1 = ILogger.LogLevel;
  var HttpClient_1 = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var HttpResponse = function () {
      function HttpResponse(statusCode, statusText, content) {
        this.statusCode = statusCode;
        this.statusText = statusText;
        this.content = content;
      }
      return HttpResponse;
    }();
    exports.HttpResponse = HttpResponse;
    var HttpClient = function () {
      function HttpClient() {}
      HttpClient.prototype.get = function (url, options) {
        return this.send(tslib_1.__assign({}, options, {
          method: "GET",
          url: url
        }));
      };
      HttpClient.prototype.post = function (url, options) {
        return this.send(tslib_1.__assign({}, options, {
          method: "POST",
          url: url
        }));
      };
      HttpClient.prototype.delete = function (url, options) {
        return this.send(tslib_1.__assign({}, options, {
          method: "DELETE",
          url: url
        }));
      };
      return HttpClient;
    }();
    exports.HttpClient = HttpClient;
    var DefaultHttpClient = function (_super) {
      tslib_1.__extends(DefaultHttpClient, _super);
      function DefaultHttpClient(logger) {
        var _this = _super.call(this) || this;
        _this.logger = logger;
        return _this;
      }
      DefaultHttpClient.prototype.send = function (request) {
        var _this = this;
        return new Promise(function (resolve, reject) {
          var headers = {
            "X-Requested-With": "XMLHttpRequest"
          };
          var responseType = "text";
          if (request.headers) {
            Object.keys(request.headers).forEach(function (header) {
              headers[header] = request.headers[header];
            });
          }
          if (request.responseType) {
            responseType = request.responseType;
          }
          uni.request({
            url: request.url,
            data: request.content || "",
            header: headers,
            method: request.method,
            responseType: responseType,
            withCredentials: true,
            timeout: request.timeout || 5e3,
            success: function success(res) {
              if (request.abortSignal) {
                request.abortSignal.onabort = null;
              }
              var statusText = res.errMsg.split(":")[1];
              var responseText = JSON.stringify(res.data);
              if (_typeof(res.data) == "object") {
                responseText = JSON.stringify(res.data);
              } else {
                responseText = res.data;
              }
              if (res.statusCode >= 200 && res.statusCode < 300) {
                resolve(new HttpResponse(res.statusCode, statusText, responseText));
              } else {
                reject(new Errors.HttpError(responseText, res.statusCode));
              }
            },
            fail: function fail(res) {
              if (res.errMsg.includes("timeout")) {
                _this.logger.log(ILogger.LogLevel.Warning, "Timeout from HTTP request.");
                reject(new Errors.TimeoutError());
              } else {
                _this.logger.log(ILogger.LogLevel.Warning, "Error from HTTP request. " + res.statusCode + ": " + res.errMsg);
                reject(new Errors.HttpError(res.errMsg, res.statusCode));
              }
            },
            complete: function complete() {}
          });
        });
      };
      return DefaultHttpClient;
    }(HttpClient);
    exports.DefaultHttpClient = DefaultHttpClient;
  });
  unwrapExports(HttpClient_1);
  var HttpClient_2 = HttpClient_1.HttpResponse;
  var HttpClient_3 = HttpClient_1.HttpClient;
  var HttpClient_4 = HttpClient_1.DefaultHttpClient;
  var TextMessageFormat_1 = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var TextMessageFormat = function () {
      function TextMessageFormat() {}
      TextMessageFormat.write = function (output) {
        return "" + output + TextMessageFormat.RecordSeparator;
      };
      TextMessageFormat.parse = function (input) {
        if (input[input.length - 1] !== TextMessageFormat.RecordSeparator) {
          throw new Error("Message is incomplete.");
        }
        var messages = input.split(TextMessageFormat.RecordSeparator);
        messages.pop();
        return messages;
      };
      TextMessageFormat.RecordSeparatorCode = 30;
      TextMessageFormat.RecordSeparator = String.fromCharCode(TextMessageFormat.RecordSeparatorCode);
      return TextMessageFormat;
    }();
    exports.TextMessageFormat = TextMessageFormat;
  });
  unwrapExports(TextMessageFormat_1);
  var TextMessageFormat_2 = TextMessageFormat_1.TextMessageFormat;
  var HandshakeProtocol_1 = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var HandshakeProtocol = function () {
      function HandshakeProtocol() {}
      HandshakeProtocol.prototype.writeHandshakeRequest = function (handshakeRequest) {
        return TextMessageFormat_1.TextMessageFormat.write(JSON.stringify(handshakeRequest));
      };
      HandshakeProtocol.prototype.parseHandshakeResponse = function (data) {
        var responseMessage;
        var messageData;
        var remainingData;
        if (data instanceof ArrayBuffer) {
          var binaryData = new Uint8Array(data);
          var separatorIndex = binaryData.indexOf(TextMessageFormat_1.TextMessageFormat.RecordSeparatorCode);
          if (separatorIndex === -1) {
            throw new Error("Message is incomplete.");
          }
          var responseLength = separatorIndex + 1;
          messageData = String.fromCharCode.apply(null, binaryData.slice(0, responseLength));
          remainingData = binaryData.byteLength > responseLength ? binaryData.slice(responseLength).buffer : null;
        } else {
          var textData = data;
          var separatorIndex = textData.indexOf(TextMessageFormat_1.TextMessageFormat.RecordSeparator);
          if (separatorIndex === -1) {
            throw new Error("Message is incomplete.");
          }
          var responseLength = separatorIndex + 1;
          messageData = textData.substring(0, responseLength);
          remainingData = textData.length > responseLength ? textData.substring(responseLength) : null;
        }
        var messages = TextMessageFormat_1.TextMessageFormat.parse(messageData);
        responseMessage = JSON.parse(messages[0]);
        return [remainingData, responseMessage];
      };
      return HandshakeProtocol;
    }();
    exports.HandshakeProtocol = HandshakeProtocol;
  });
  unwrapExports(HandshakeProtocol_1);
  var HandshakeProtocol_2 = HandshakeProtocol_1.HandshakeProtocol;
  var IHubProtocol = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var MessageType;
    (function (MessageType) {
      MessageType[MessageType["Invocation"] = 1] = "Invocation";
      MessageType[MessageType["StreamItem"] = 2] = "StreamItem";
      MessageType[MessageType["Completion"] = 3] = "Completion";
      MessageType[MessageType["StreamInvocation"] = 4] = "StreamInvocation";
      MessageType[MessageType["CancelInvocation"] = 5] = "CancelInvocation";
      MessageType[MessageType["Ping"] = 6] = "Ping";
      MessageType[MessageType["Close"] = 7] = "Close";
    })(MessageType = exports.MessageType || (exports.MessageType = {}));
  });
  unwrapExports(IHubProtocol);
  var IHubProtocol_1 = IHubProtocol.MessageType;
  var Loggers = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var NullLogger = function () {
      function NullLogger() {}
      NullLogger.prototype.log = function (logLevel, message) {};
      NullLogger.instance = new NullLogger();
      return NullLogger;
    }();
    exports.NullLogger = NullLogger;
  });
  unwrapExports(Loggers);
  var Loggers_1 = Loggers.NullLogger;
  var Utils = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Arg = function () {
      function Arg() {}
      Arg.isRequired = function (val, name) {
        if (val === null || val === undefined) {
          throw new Error("The '" + name + "' argument is required.");
        }
      };
      Arg.isIn = function (val, values, name) {
        if (!(val in values)) {
          throw new Error("Unknown " + name + " value: " + val + ".");
        }
      };
      return Arg;
    }();
    exports.Arg = Arg;
    function getDataDetail(data, includeContent) {
      var length = null;
      if (data instanceof ArrayBuffer) {
        length = "Binary data of length " + data.byteLength;
        if (includeContent) {
          length += ". Content: '" + formatArrayBuffer(data) + "'";
        }
      } else if (typeof data === "string") {
        length = "String data of length " + data.length;
        if (includeContent) {
          length += ". Content: '" + data + "'.";
        }
      }
      return length;
    }
    exports.getDataDetail = getDataDetail;
    function formatArrayBuffer(data) {
      var view = new Uint8Array(data);
      var str = "";
      view.forEach(function (num) {
        var pad = num < 16 ? "0" : "";
        str += "0x" + pad + num.toString(16) + " ";
      });
      return str.substr(0, str.length - 1);
    }
    exports.formatArrayBuffer = formatArrayBuffer;
    function sendMessage(logger, transportName, httpClient, url, accessTokenFactory, content, logMessageContent) {
      return tslib_1.__awaiter(this, void 0, void 0, function () {
        var _a, headers, token, response;
        return tslib_1.__generator(this, function (_b) {
          switch (_b.label) {
            case 0:
              return [4, accessTokenFactory()];
            case 1:
              token = _b.sent();
              if (token) {
                headers = (_a = {}, _a["Authorization"] = "Bearer " + token, _a);
              }
              logger.log(ILogger.LogLevel.Trace, "(" + transportName + " transport) sending data. " + getDataDetail(content, logMessageContent) + ".");
              return [4, httpClient.post(url, {
                content: content,
                headers: headers
              })];
            case 2:
              response = _b.sent();
              logger.log(ILogger.LogLevel.Trace, "(" + transportName + " transport) request complete. Response status: " + response.statusCode + ".");
              return [2];
          }
        });
      });
    }
    exports.sendMessage = sendMessage;
    function createLogger(logger) {
      if (logger === undefined) {
        return new ConsoleLogger(ILogger.LogLevel.Information);
      }
      if (logger === null) {
        return Loggers.NullLogger.instance;
      }
      if (logger.log) {
        return logger;
      }
      return new ConsoleLogger(logger);
    }
    exports.createLogger = createLogger;
    var Subject = function () {
      function Subject(cancelCallback) {
        this.observers = [];
        this.cancelCallback = cancelCallback;
      }
      Subject.prototype.next = function (item) {
        for (var _i = 0, _a = this.observers; _i < _a.length; _i++) {
          var observer = _a[_i];
          observer.next(item);
        }
      };
      Subject.prototype.error = function (err) {
        for (var _i = 0, _a = this.observers; _i < _a.length; _i++) {
          var observer = _a[_i];
          if (observer.error) {
            observer.error(err);
          }
        }
      };
      Subject.prototype.complete = function () {
        for (var _i = 0, _a = this.observers; _i < _a.length; _i++) {
          var observer = _a[_i];
          if (observer.complete) {
            observer.complete();
          }
        }
      };
      Subject.prototype.subscribe = function (observer) {
        this.observers.push(observer);
        return new SubjectSubscription(this, observer);
      };
      return Subject;
    }();
    exports.Subject = Subject;
    var SubjectSubscription = function () {
      function SubjectSubscription(subject, observer) {
        this.subject = subject;
        this.observer = observer;
      }
      SubjectSubscription.prototype.dispose = function () {
        var index = this.subject.observers.indexOf(this.observer);
        if (index > -1) {
          this.subject.observers.splice(index, 1);
        }
        if (this.subject.observers.length === 0) {
          this.subject.cancelCallback().catch(function (_) {});
        }
      };
      return SubjectSubscription;
    }();
    exports.SubjectSubscription = SubjectSubscription;
    var ConsoleLogger = function () {
      function ConsoleLogger(minimumLogLevel) {
        this.minimumLogLevel = minimumLogLevel;
      }
      ConsoleLogger.prototype.log = function (logLevel, message) {
        if (logLevel >= this.minimumLogLevel) {
          switch (logLevel) {
            case ILogger.LogLevel.Critical:
            case ILogger.LogLevel.Error:
              console.error(ILogger.LogLevel[logLevel] + ": " + message);
              break;
            case ILogger.LogLevel.Warning:
              console.warn(ILogger.LogLevel[logLevel] + ": " + message);
              break;
            case ILogger.LogLevel.Information:
              console.info(ILogger.LogLevel[logLevel] + ": " + message);
              break;
            default:
              console.log(ILogger.LogLevel[logLevel] + ": " + message);
              break;
          }
        }
      };
      return ConsoleLogger;
    }();
    exports.ConsoleLogger = ConsoleLogger;
  });
  unwrapExports(Utils);
  var Utils_1 = Utils.Arg;
  var Utils_2 = Utils.getDataDetail;
  var Utils_3 = Utils.formatArrayBuffer;
  var Utils_4 = Utils.sendMessage;
  var Utils_5 = Utils.createLogger;
  var Utils_6 = Utils.Subject;
  var Utils_7 = Utils.SubjectSubscription;
  var Utils_8 = Utils.ConsoleLogger;
  var HubConnection_1 = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var DEFAULT_TIMEOUT_IN_MS = 30 * 1e3;
    var HubConnection = function () {
      function HubConnection(connection, logger, protocol) {
        var _this = this;
        Utils.Arg.isRequired(connection, "connection");
        Utils.Arg.isRequired(logger, "logger");
        Utils.Arg.isRequired(protocol, "protocol");
        this.serverTimeoutInMilliseconds = DEFAULT_TIMEOUT_IN_MS;
        this.logger = logger;
        this.protocol = protocol;
        this.connection = connection;
        this.handshakeProtocol = new HandshakeProtocol_1.HandshakeProtocol();
        this.connection.onreceive = function (data) {
          return _this.processIncomingData(data);
        };
        this.connection.onclose = function (error) {
          return _this.connectionClosed(error);
        };
        this.callbacks = {};
        this.methods = {};
        this.closedCallbacks = [];
        this.id = 0;
      }
      HubConnection.create = function (connection, logger, protocol) {
        return new HubConnection(connection, logger, protocol);
      };
      HubConnection.prototype.start = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
          var handshakeRequest;
          return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
              case 0:
                handshakeRequest = {
                  protocol: this.protocol.name,
                  version: this.protocol.version
                };
                this.logger.log(ILogger.LogLevel.Debug, "Starting HubConnection.");
                this.receivedHandshakeResponse = false;
                return [4, this.connection.start(this.protocol.transferFormat)];
              case 1:
                _a.sent();
                this.logger.log(ILogger.LogLevel.Debug, "Sending handshake request.");
                return [4, this.connection.send(this.handshakeProtocol.writeHandshakeRequest(handshakeRequest))];
              case 2:
                _a.sent();
                this.logger.log(ILogger.LogLevel.Information, "Using HubProtocol '" + this.protocol.name + "'.");
                this.cleanupTimeout();
                this.configureTimeout();
                return [2];
            }
          });
        });
      };
      HubConnection.prototype.stop = function () {
        this.logger.log(ILogger.LogLevel.Debug, "Stopping HubConnection.");
        this.cleanupTimeout();
        return this.connection.stop();
      };
      HubConnection.prototype.stream = function (methodName) {
        var _this = this;
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
          args[_i - 1] = arguments[_i];
        }
        var invocationDescriptor = this.createStreamInvocation(methodName, args);
        var subject = new Utils.Subject(function () {
          var cancelInvocation = _this.createCancelInvocation(invocationDescriptor.invocationId);
          var cancelMessage = _this.protocol.writeMessage(cancelInvocation);
          delete _this.callbacks[invocationDescriptor.invocationId];
          return _this.connection.send(cancelMessage);
        });
        this.callbacks[invocationDescriptor.invocationId] = function (invocationEvent, error) {
          if (error) {
            subject.error(error);
            return;
          }
          if (invocationEvent.type === IHubProtocol.MessageType.Completion) {
            if (invocationEvent.error) {
              subject.error(new Error(invocationEvent.error));
            } else {
              subject.complete();
            }
          } else {
            subject.next(invocationEvent.item);
          }
        };
        var message = this.protocol.writeMessage(invocationDescriptor);
        this.connection.send(message).catch(function (e) {
          subject.error(e);
          delete _this.callbacks[invocationDescriptor.invocationId];
        });
        return subject;
      };
      HubConnection.prototype.send = function (methodName) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
          args[_i - 1] = arguments[_i];
        }
        var invocationDescriptor = this.createInvocation(methodName, args, true);
        var message = this.protocol.writeMessage(invocationDescriptor);
        return this.connection.send(message);
      };
      HubConnection.prototype.invoke = function (methodName) {
        var _this = this;
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
          args[_i - 1] = arguments[_i];
        }
        var invocationDescriptor = this.createInvocation(methodName, args, false);
        var p = new Promise(function (resolve, reject) {
          _this.callbacks[invocationDescriptor.invocationId * 1] = function (invocationEvent, error) {
            if (error) {
              reject(error);
              return;
            }
            if (invocationEvent.type === IHubProtocol.MessageType.Completion) {
              var completionMessage = invocationEvent;
              if (completionMessage.error) {
                reject(new Error(completionMessage.error));
              } else {
                resolve(completionMessage.result);
              }
            } else {
              reject(new Error("Unexpected message type: " + invocationEvent.type));
            }
          };
          var message = _this.protocol.writeMessage(invocationDescriptor);
          _this.connection.send(message).catch(function (e) {
            reject(e);
            delete _this.callbacks[invocationDescriptor.invocationId * 1];
          });
        });
        return p;
      };
      HubConnection.prototype.on = function (methodName, newMethod) {
        if (!methodName || !newMethod) {
          return;
        }
        methodName = methodName.toLowerCase();
        if (!this.methods[methodName]) {
          this.methods[methodName] = [];
        }
        if (this.methods[methodName].indexOf(newMethod) !== -1) {
          return;
        }
        this.methods[methodName].push(newMethod);
      };
      HubConnection.prototype.off = function (methodName, method) {
        if (!methodName) {
          return;
        }
        methodName = methodName.toLowerCase();
        var handlers = this.methods[methodName];
        if (!handlers) {
          return;
        }
        if (method) {
          var removeIdx = handlers.indexOf(method);
          if (removeIdx !== -1) {
            handlers.splice(removeIdx, 1);
            if (handlers.length === 0) {
              delete this.methods[methodName];
            }
          }
        } else {
          delete this.methods[methodName];
        }
      };
      HubConnection.prototype.onclose = function (callback) {
        if (callback) {
          this.closedCallbacks.push(callback);
        }
      };
      HubConnection.prototype.processIncomingData = function (data) {
        this.cleanupTimeout();
        if (!this.receivedHandshakeResponse) {
          data = this.processHandshakeResponse(data);
          this.receivedHandshakeResponse = true;
        }
        if (data) {
          var messages = this.protocol.parseMessages(data, this.logger);
          for (var _i = 0, messages_1 = messages; _i < messages_1.length; _i++) {
            var message = messages_1[_i];
            switch (message.type) {
              case IHubProtocol.MessageType.Invocation:
                this.invokeClientMethod(message);
                break;
              case IHubProtocol.MessageType.StreamItem:
              case IHubProtocol.MessageType.Completion:
                var callback = this.callbacks[message.invocationId];
                if (callback != null) {
                  if (message.type === IHubProtocol.MessageType.Completion) {
                    delete this.callbacks[message.invocationId];
                  }
                  callback(message);
                }
                break;
              case IHubProtocol.MessageType.Ping:
                break;
              case IHubProtocol.MessageType.Close:
                this.logger.log(ILogger.LogLevel.Information, "Close message received from server.");
                this.connection.stop(message.error ? new Error("Server returned an error on close: " + message.error) : null);
                break;
              default:
                this.logger.log(ILogger.LogLevel.Warning, "Invalid message type: " + message.type);
                break;
            }
          }
        }
        this.configureTimeout();
      };
      HubConnection.prototype.processHandshakeResponse = function (data) {
        var _a;
        var responseMessage;
        var remainingData;
        try {
          _a = this.handshakeProtocol.parseHandshakeResponse(data), remainingData = _a[0], responseMessage = _a[1];
        } catch (e) {
          var message = "Error parsing handshake response: " + e;
          this.logger.log(ILogger.LogLevel.Error, message);
          var error = new Error(message);
          this.connection.stop(error);
          throw error;
        }
        if (responseMessage.error) {
          var message = "Server returned handshake error: " + responseMessage.error;
          this.logger.log(ILogger.LogLevel.Error, message);
          this.connection.stop(new Error(message));
        } else {
          this.logger.log(ILogger.LogLevel.Debug, "Server handshake complete.");
        }
        return remainingData;
      };
      HubConnection.prototype.configureTimeout = function () {
        var _this = this;
        if (!this.connection.features || !this.connection.features.inherentKeepAlive) {
          this.timeoutHandle = setTimeout(function () {
            return _this.serverTimeout();
          }, this.serverTimeoutInMilliseconds);
        }
      };
      HubConnection.prototype.serverTimeout = function () {
        this.connection.stop(new Error("Server timeout elapsed without receiving a message from the server."));
      };
      HubConnection.prototype.invokeClientMethod = function (invocationMessage) {
        var _this = this;
        var methods = this.methods[invocationMessage.target.toLowerCase()];
        if (methods) {
          methods.forEach(function (m) {
            return m.apply(_this, invocationMessage.arguments);
          });
          if (invocationMessage.invocationId) {
            var message = "Server requested a response, which is not supported in this version of the client.";
            this.logger.log(ILogger.LogLevel.Error, message);
            this.connection.stop(new Error(message));
          }
        } else {
          this.logger.log(ILogger.LogLevel.Warning, "No client method with the name '" + invocationMessage.target + "' found.");
        }
      };
      HubConnection.prototype.connectionClosed = function (error) {
        var _this = this;
        var callbacks = this.callbacks;
        this.callbacks = {};
        Object.keys(callbacks).forEach(function (key) {
          var callback = callbacks[key];
          callback(undefined, error ? error : new Error("Invocation canceled due to connection being closed."));
        });
        this.cleanupTimeout();
        this.closedCallbacks.forEach(function (c) {
          return c.apply(_this, [error]);
        });
      };
      HubConnection.prototype.cleanupTimeout = function () {
        if (this.timeoutHandle) {
          clearTimeout(this.timeoutHandle);
        }
      };
      HubConnection.prototype.createInvocation = function (methodName, args, nonblocking) {
        if (nonblocking) {
          return {
            arguments: args,
            target: methodName,
            type: IHubProtocol.MessageType.Invocation
          };
        } else {
          var id = this.id;
          this.id++;
          return {
            arguments: args,
            invocationId: id.toString(),
            target: methodName,
            type: IHubProtocol.MessageType.Invocation
          };
        }
      };
      HubConnection.prototype.createStreamInvocation = function (methodName, args) {
        var id = this.id;
        this.id++;
        return {
          arguments: args,
          invocationId: id.toString(),
          target: methodName,
          type: IHubProtocol.MessageType.StreamInvocation
        };
      };
      HubConnection.prototype.createCancelInvocation = function (id) {
        return {
          invocationId: id,
          type: IHubProtocol.MessageType.CancelInvocation
        };
      };
      return HubConnection;
    }();
    exports.HubConnection = HubConnection;
  });
  unwrapExports(HubConnection_1);
  var HubConnection_2 = HubConnection_1.HubConnection;
  var ITransport = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var HttpTransportType;
    (function (HttpTransportType) {
      HttpTransportType[HttpTransportType["None"] = 0] = "None";
      HttpTransportType[HttpTransportType["WebSockets"] = 1] = "WebSockets";
      HttpTransportType[HttpTransportType["ServerSentEvents"] = 2] = "ServerSentEvents";
      HttpTransportType[HttpTransportType["LongPolling"] = 4] = "LongPolling";
    })(HttpTransportType = exports.HttpTransportType || (exports.HttpTransportType = {}));
    var TransferFormat;
    (function (TransferFormat) {
      TransferFormat[TransferFormat["Text"] = 1] = "Text";
      TransferFormat[TransferFormat["Binary"] = 2] = "Binary";
    })(TransferFormat = exports.TransferFormat || (exports.TransferFormat = {}));
  });
  unwrapExports(ITransport);
  var ITransport_1 = ITransport.HttpTransportType;
  var ITransport_2 = ITransport.TransferFormat;
  var AbortController_1 = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var AbortController = function () {
      function AbortController() {
        this.isAborted = false;
      }
      AbortController.prototype.abort = function () {
        if (!this.isAborted) {
          this.isAborted = true;
          if (this.onabort) {
            this.onabort();
          }
        }
      };
      Object.defineProperty(AbortController.prototype, "signal", {
        get: function get() {
          return this;
        },
        enumerable: true,
        configurable: true
      });
      Object.defineProperty(AbortController.prototype, "aborted", {
        get: function get() {
          return this.isAborted;
        },
        enumerable: true,
        configurable: true
      });
      return AbortController;
    }();
    exports.AbortController = AbortController;
  });
  unwrapExports(AbortController_1);
  var AbortController_2 = AbortController_1.AbortController;
  var LongPollingTransport_1 = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var SHUTDOWN_TIMEOUT = 5 * 1e3;
    var LongPollingTransport = function () {
      function LongPollingTransport(httpClient, accessTokenFactory, logger, logMessageContent, shutdownTimeout) {
        this.httpClient = httpClient;
        this.accessTokenFactory = accessTokenFactory || function () {
          return null;
        };
        this.logger = logger;
        this.pollAbort = new AbortController_1.AbortController();
        this.logMessageContent = logMessageContent;
        this.shutdownTimeout = shutdownTimeout || SHUTDOWN_TIMEOUT;
      }
      Object.defineProperty(LongPollingTransport.prototype, "pollAborted", {
        get: function get() {
          return this.pollAbort.aborted;
        },
        enumerable: true,
        configurable: true
      });
      LongPollingTransport.prototype.connect = function (url, transferFormat) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
          var pollOptions, token, closeError, pollUrl, response;
          return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
              case 0:
                Utils.Arg.isRequired(url, "url");
                Utils.Arg.isRequired(transferFormat, "transferFormat");
                Utils.Arg.isIn(transferFormat, ITransport.TransferFormat, "transferFormat");
                this.url = url;
                this.logger.log(ILogger.LogLevel.Trace, "(LongPolling transport) Connecting");
                if (transferFormat === ITransport.TransferFormat.Binary && typeof new XMLHttpRequest().responseType !== "string") {
                  throw new Error("Binary protocols over XmlHttpRequest not implementing advanced features are not supported.");
                }
                pollOptions = {
                  abortSignal: this.pollAbort.signal,
                  headers: {},
                  timeout: 9e4
                };
                if (transferFormat === ITransport.TransferFormat.Binary) {
                  pollOptions.responseType = "arraybuffer";
                }
                return [4, this.accessTokenFactory()];
              case 1:
                token = _a.sent();
                this.updateHeaderToken(pollOptions, token);
                pollUrl = url + "&_=" + Date.now();
                this.logger.log(ILogger.LogLevel.Trace, "(LongPolling transport) polling: " + pollUrl);
                return [4, this.httpClient.get(pollUrl, pollOptions)];
              case 2:
                response = _a.sent();
                if (response.statusCode !== 200) {
                  this.logger.log(ILogger.LogLevel.Error, "(LongPolling transport) Unexpected response code: " + response.statusCode);
                  closeError = new Errors.HttpError(response.statusText, response.statusCode);
                  this.running = false;
                } else {
                  this.running = true;
                }
                this.poll(this.url, pollOptions, closeError);
                return [2, Promise.resolve()];
            }
          });
        });
      };
      LongPollingTransport.prototype.updateHeaderToken = function (request, token) {
        if (token) {
          request.headers["Authorization"] = "Bearer " + token;
          return;
        }
        if (request.headers["Authorization"]) {
          delete request.headers["Authorization"];
        }
      };
      LongPollingTransport.prototype.poll = function (url, pollOptions, closeError) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
          var token, pollUrl, response, e_1;
          return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
              case 0:
                _a.trys.push([0,, 8, 9]);
                _a.label = 1;
              case 1:
                if (!this.running) return [3, 7];
                return [4, this.accessTokenFactory()];
              case 2:
                token = _a.sent();
                this.updateHeaderToken(pollOptions, token);
                _a.label = 3;
              case 3:
                _a.trys.push([3, 5,, 6]);
                pollUrl = url + "&_=" + Date.now();
                this.logger.log(ILogger.LogLevel.Trace, "(LongPolling transport) polling: " + pollUrl);
                return [4, this.httpClient.get(pollUrl, pollOptions)];
              case 4:
                response = _a.sent();
                if (response.statusCode === 204) {
                  this.logger.log(ILogger.LogLevel.Information, "(LongPolling transport) Poll terminated by server");
                  this.running = false;
                } else if (response.statusCode !== 200) {
                  this.logger.log(ILogger.LogLevel.Error, "(LongPolling transport) Unexpected response code: " + response.statusCode);
                  closeError = new Errors.HttpError(response.statusText, response.statusCode);
                  this.running = false;
                } else {
                  if (response.content) {
                    this.logger.log(ILogger.LogLevel.Trace, "(LongPolling transport) data received. " + Utils.getDataDetail(response.content, this.logMessageContent));
                    if (this.onreceive) {
                      this.onreceive(response.content);
                    }
                  } else {
                    this.logger.log(ILogger.LogLevel.Trace, "(LongPolling transport) Poll timed out, reissuing.");
                  }
                }
                return [3, 6];
              case 5:
                e_1 = _a.sent();
                if (!this.running) {
                  this.logger.log(ILogger.LogLevel.Trace, "(LongPolling transport) Poll errored after shutdown: " + e_1.message);
                } else {
                  if (e_1 instanceof Errors.TimeoutError) {
                    this.logger.log(ILogger.LogLevel.Trace, "(LongPolling transport) Poll timed out, reissuing.");
                  } else {
                    closeError = e_1;
                    this.running = false;
                  }
                }
                return [3, 6];
              case 6:
                return [3, 1];
              case 7:
                return [3, 9];
              case 8:
                this.stopped = true;
                if (this.shutdownTimer) {
                  clearTimeout(this.shutdownTimer);
                }
                if (this.onclose) {
                  this.logger.log(ILogger.LogLevel.Trace, "(LongPolling transport) Firing onclose event. Error: " + (closeError || "<undefined>"));
                  this.onclose(closeError);
                }
                this.logger.log(ILogger.LogLevel.Trace, "(LongPolling transport) Transport finished.");
                return [7];
              case 9:
                return [2];
            }
          });
        });
      };
      LongPollingTransport.prototype.send = function (data) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
          return tslib_1.__generator(this, function (_a) {
            if (!this.running) {
              return [2, Promise.reject(new Error("Cannot send until the transport is connected"))];
            }
            return [2, Utils.sendMessage(this.logger, "LongPolling", this.httpClient, this.url, this.accessTokenFactory, data, this.logMessageContent)];
          });
        });
      };
      LongPollingTransport.prototype.stop = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
          var deleteOptions, token, response;
          var _this = this;
          return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
              case 0:
                _a.trys.push([0,, 3, 4]);
                this.running = false;
                this.logger.log(ILogger.LogLevel.Trace, "(LongPolling transport) sending DELETE request to " + this.url + ".");
                deleteOptions = {
                  headers: {}
                };
                return [4, this.accessTokenFactory()];
              case 1:
                token = _a.sent();
                this.updateHeaderToken(deleteOptions, token);
                return [4, this.httpClient.delete(this.url, deleteOptions)];
              case 2:
                response = _a.sent();
                this.logger.log(ILogger.LogLevel.Trace, "(LongPolling transport) DELETE request accepted.");
                return [3, 4];
              case 3:
                if (!this.stopped) {
                  this.shutdownTimer = setTimeout(function () {
                    _this.logger.log(ILogger.LogLevel.Warning, "(LongPolling transport) server did not terminate after DELETE request, canceling poll.");
                    _this.pollAbort.abort();
                  }, this.shutdownTimeout);
                }
                return [7];
              case 4:
                return [2];
            }
          });
        });
      };
      return LongPollingTransport;
    }();
    exports.LongPollingTransport = LongPollingTransport;
  });
  unwrapExports(LongPollingTransport_1);
  var LongPollingTransport_2 = LongPollingTransport_1.LongPollingTransport;
  var ServerSentEventsTransport_1 = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ServerSentEventsTransport = function () {
      function ServerSentEventsTransport(httpClient, accessTokenFactory, logger, logMessageContent) {
        this.httpClient = httpClient;
        this.accessTokenFactory = accessTokenFactory || function () {
          return null;
        };
        this.logger = logger;
        this.logMessageContent = logMessageContent;
      }
      ServerSentEventsTransport.prototype.connect = function (url, transferFormat) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
          var token;
          var _this = this;
          return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
              case 0:
                Utils.Arg.isRequired(url, "url");
                Utils.Arg.isRequired(transferFormat, "transferFormat");
                Utils.Arg.isIn(transferFormat, ITransport.TransferFormat, "transferFormat");
                if (typeof EventSource === "undefined") {
                  throw new Error("'EventSource' is not supported in your environment.");
                }
                this.logger.log(ILogger.LogLevel.Trace, "(SSE transport) Connecting");
                return [4, this.accessTokenFactory()];
              case 1:
                token = _a.sent();
                if (token) {
                  url += (url.indexOf("?") < 0 ? "?" : "&") + ("access_token=" + encodeURIComponent(token));
                }
                this.url = url;
                return [2, new Promise(function (resolve, reject) {
                  var opened = false;
                  if (transferFormat !== ITransport.TransferFormat.Text) {
                    reject(new Error("The Server-Sent Events transport only supports the 'Text' transfer format"));
                  }
                  var eventSource = new EventSource(url, {
                    withCredentials: true
                  });
                  try {
                    eventSource.onmessage = function (e) {
                      if (_this.onreceive) {
                        try {
                          _this.logger.log(ILogger.LogLevel.Trace, "(SSE transport) data received. " + Utils.getDataDetail(e.data, _this.logMessageContent) + ".");
                          _this.onreceive(e.data);
                        } catch (error) {
                          if (_this.onclose) {
                            _this.onclose(error);
                          }
                          return;
                        }
                      }
                    };
                    eventSource.onerror = function (e) {
                      var error = new Error(e.message || "Error occurred");
                      if (opened) {
                        _this.close(error);
                      } else {
                        reject(error);
                      }
                    };
                    eventSource.onopen = function () {
                      _this.logger.log(ILogger.LogLevel.Information, "SSE connected to " + _this.url);
                      _this.eventSource = eventSource;
                      opened = true;
                      resolve();
                    };
                  } catch (e) {
                    return Promise.reject(e);
                  }
                })];
            }
          });
        });
      };
      ServerSentEventsTransport.prototype.send = function (data) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
          return tslib_1.__generator(this, function (_a) {
            if (!this.eventSource) {
              return [2, Promise.reject(new Error("Cannot send until the transport is connected"))];
            }
            return [2, Utils.sendMessage(this.logger, "SSE", this.httpClient, this.url, this.accessTokenFactory, data, this.logMessageContent)];
          });
        });
      };
      ServerSentEventsTransport.prototype.stop = function () {
        this.close();
        return Promise.resolve();
      };
      ServerSentEventsTransport.prototype.close = function (e) {
        if (this.eventSource) {
          this.eventSource.close();
          this.eventSource = null;
          if (this.onclose) {
            this.onclose(e);
          }
        }
      };
      return ServerSentEventsTransport;
    }();
    exports.ServerSentEventsTransport = ServerSentEventsTransport;
  });
  unwrapExports(ServerSentEventsTransport_1);
  var ServerSentEventsTransport_2 = ServerSentEventsTransport_1.ServerSentEventsTransport;
  var WebSocketTransport_1 = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var WebSocketTransport = function () {
      function WebSocketTransport(accessTokenFactory, logger, logMessageContent) {
        this.logger = logger;
        this.accessTokenFactory = accessTokenFactory || function () {
          return null;
        };
        this.logMessageContent = logMessageContent;
      }
      WebSocketTransport.prototype.connect = function (url, transferFormat) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
          var token;
          var _this = this;
          return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
              case 0:
                Utils.Arg.isRequired(url, "url");
                Utils.Arg.isRequired(transferFormat, "transferFormat");
                Utils.Arg.isIn(transferFormat, ITransport.TransferFormat, "transferFormat");
                if (typeof WebSocket === "undefined") {
                  throw new Error("'WebSocket' is not supported in your environment.");
                }
                this.logger.log(ILogger.LogLevel.Trace, "(WebSockets transport) Connecting");
                return [4, this.accessTokenFactory()];
              case 1:
                token = _a.sent();
                if (token) {
                  url += (url.indexOf("?") < 0 ? "?" : "&") + ("access_token=" + encodeURIComponent(token));
                }
                return [2, new Promise(function (resolve, reject) {
                  url = url.replace(/^http/, "ws");
                  var webSocket = uni.connectSocket({
                    url: url,
                    complete: function complete() {
                      console.log("uniapp websocket connect completed");
                    }
                  });
                  if (transferFormat === ITransport.TransferFormat.Binary) {
                    webSocket.binaryType = "arraybuffer";
                  }
                  webSocket.onOpen(function (event) {
                    _this.logger.log(ILogger.LogLevel.Information, "WebSocket connected to " + url);
                    _this.webSocket = webSocket;
                    resolve();
                  });
                  webSocket.onError(function (event) {
                    reject(event.error);
                  });
                  webSocket.onMessage(function (message) {
                    _this.logger.log(ILogger.LogLevel.Trace, "(WebSockets transport) data received. " + Utils.getDataDetail(message.data, _this.logMessageContent) + ".");
                    if (_this.onreceive) {
                      _this.onreceive(message.data);
                    }
                  });
                  webSocket.onClose(function (event) {
                    _this.logger.log(ILogger.LogLevel.Trace, "(WebSockets transport) socket closed.");
                    if (_this.onclose) {
                      if (event.wasClean === false || event.code !== 1e3) {
                        _this.onclose(new Error("Websocket closed with status code: " + event.code + " (" + event.reason + ")"));
                      } else {
                        _this.onclose();
                      }
                    }
                  });
                })];
            }
          });
        });
      };
      WebSocketTransport.prototype.send = function (data) {
        if (this.webSocket && this.webSocket.readyState === WebSocket.OPEN) {
          this.logger.log(ILogger.LogLevel.Trace, "(WebSockets transport) sending data. " + Utils.getDataDetail(data, this.logMessageContent) + ".");
          this.webSocket.send({
            data: data
          });
          return Promise.resolve();
        }
        return Promise.reject("WebSocket is not in the OPEN state");
      };
      WebSocketTransport.prototype.stop = function () {
        if (this.webSocket) {
          this.webSocket.close();
          this.webSocket = null;
        }
        return Promise.resolve();
      };
      return WebSocketTransport;
    }();
    exports.WebSocketTransport = WebSocketTransport;
  });
  unwrapExports(WebSocketTransport_1);
  var WebSocketTransport_2 = WebSocketTransport_1.WebSocketTransport;
  var HttpConnection_1 = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var MAX_REDIRECTS = 100;
    var HttpConnection = function () {
      function HttpConnection(url, options) {
        if (options === void 0) {
          options = {};
        }
        this.features = {};
        Utils.Arg.isRequired(url, "url");
        this.logger = Utils.createLogger(options.logger);
        this.baseUrl = this.resolveUrl(url);
        options = options || {};
        options.accessTokenFactory = options.accessTokenFactory || function () {
          return null;
        };
        options.logMessageContent = options.logMessageContent || false;
        this.httpClient = options.httpClient || new HttpClient_1.DefaultHttpClient(this.logger);
        this.connectionState = 2;
        this.options = options;
      }
      HttpConnection.prototype.start = function (transferFormat) {
        transferFormat = transferFormat || ITransport.TransferFormat.Binary;
        Utils.Arg.isIn(transferFormat, ITransport.TransferFormat, "transferFormat");
        this.logger.log(ILogger.LogLevel.Debug, "Starting connection with transfer format '" + ITransport.TransferFormat[transferFormat] + "'.");
        if (this.connectionState !== 2) {
          return Promise.reject(new Error("Cannot start a connection that is not in the 'Disconnected' state."));
        }
        this.connectionState = 0;
        this.startPromise = this.startInternal(transferFormat);
        return this.startPromise;
      };
      HttpConnection.prototype.send = function (data) {
        if (this.connectionState !== 1) {
          throw new Error("Cannot send data if the connection is not in the 'Connected' State.");
        }
        return this.transport.send(data);
      };
      HttpConnection.prototype.stop = function (error) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
          var e_1;
          return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
              case 0:
                this.connectionState = 2;
                _a.label = 1;
              case 1:
                _a.trys.push([1, 3,, 4]);
                return [4, this.startPromise];
              case 2:
                _a.sent();
                return [3, 4];
              case 3:
                e_1 = _a.sent();
                return [3, 4];
              case 4:
                if (!this.transport) return [3, 6];
                this.stopError = error;
                return [4, this.transport.stop()];
              case 5:
                _a.sent();
                this.transport = null;
                _a.label = 6;
              case 6:
                return [2];
            }
          });
        });
      };
      HttpConnection.prototype.startInternal = function (transferFormat) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
          var url, negotiateResponse, redirects, _loop_1, this_1, state_1, e_2;
          var _this = this;
          return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
              case 0:
                url = this.baseUrl;
                this.accessTokenFactory = this.options.accessTokenFactory;
                _a.label = 1;
              case 1:
                _a.trys.push([1, 12,, 13]);
                if (!this.options.skipNegotiation) return [3, 5];
                if (!(this.options.transport === ITransport.HttpTransportType.WebSockets)) return [3, 3];
                this.transport = this.constructTransport(ITransport.HttpTransportType.WebSockets);
                return [4, this.transport.connect(url, transferFormat)];
              case 2:
                _a.sent();
                return [3, 4];
              case 3:
                throw Error("Negotiation can only be skipped when using the WebSocket transport directly.");
              case 4:
                return [3, 11];
              case 5:
                negotiateResponse = null;
                redirects = 0;
                _loop_1 = function _loop_1() {
                  var accessToken_1;
                  return tslib_1.__generator(this, function (_a) {
                    switch (_a.label) {
                      case 0:
                        return [4, this_1.getNegotiationResponse(url)];
                      case 1:
                        negotiateResponse = _a.sent();
                        if (this_1.connectionState === 2) {
                          return [2, {
                            value: void 0
                          }];
                        }
                        if (negotiateResponse.url) {
                          url = negotiateResponse.url;
                        }
                        if (negotiateResponse.accessToken) {
                          accessToken_1 = negotiateResponse.accessToken;
                          this_1.accessTokenFactory = function () {
                            return accessToken_1;
                          };
                        }
                        redirects++;
                        return [2];
                    }
                  });
                };
                this_1 = this;
                _a.label = 6;
              case 6:
                return [5, _loop_1()];
              case 7:
                state_1 = _a.sent();
                if (_typeof(state_1) === "object") return [2, state_1.value];
                _a.label = 8;
              case 8:
                if (negotiateResponse.url && redirects < MAX_REDIRECTS) return [3, 6];
                _a.label = 9;
              case 9:
                if (redirects === MAX_REDIRECTS && negotiateResponse.url) {
                  throw Error("Negotiate redirection limit exceeded.");
                }
                return [4, this.createTransport(url, this.options.transport, negotiateResponse, transferFormat)];
              case 10:
                _a.sent();
                _a.label = 11;
              case 11:
                if (this.transport instanceof LongPollingTransport_1.LongPollingTransport) {
                  this.features.inherentKeepAlive = true;
                }
                this.transport.onreceive = this.onreceive;
                this.transport.onclose = function (e) {
                  return _this.stopConnection(e);
                };
                this.changeState(0, 1);
                return [3, 13];
              case 12:
                e_2 = _a.sent();
                this.logger.log(ILogger.LogLevel.Error, "Failed to start the connection: " + e_2);
                this.connectionState = 2;
                this.transport = null;
                throw e_2;
              case 13:
                return [2];
            }
          });
        });
      };
      HttpConnection.prototype.getNegotiationResponse = function (url) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
          var _a, token, headers, negotiateUrl, response, e_3;
          return tslib_1.__generator(this, function (_b) {
            switch (_b.label) {
              case 0:
                return [4, this.accessTokenFactory()];
              case 1:
                token = _b.sent();
                if (token) {
                  headers = (_a = {}, _a["Authorization"] = "Bearer " + token, _a);
                }
                negotiateUrl = this.resolveNegotiateUrl(url);
                this.logger.log(ILogger.LogLevel.Debug, "Sending negotiation request: " + negotiateUrl);
                _b.label = 2;
              case 2:
                _b.trys.push([2, 4,, 5]);
                return [4, this.httpClient.post(negotiateUrl, {
                  content: "",
                  headers: headers
                })];
              case 3:
                response = _b.sent();
                if (response.statusCode !== 200) {
                  throw Error("Unexpected status code returned from negotiate " + response.statusCode);
                }
                return [2, JSON.parse(response.content)];
              case 4:
                e_3 = _b.sent();
                this.logger.log(ILogger.LogLevel.Error, "Failed to complete negotiation with the server: " + e_3);
                throw e_3;
              case 5:
                return [2];
            }
          });
        });
      };
      HttpConnection.prototype.createConnectUrl = function (url, connectionId) {
        return url + (url.indexOf("?") === -1 ? "?" : "&") + ("id=" + connectionId);
      };
      HttpConnection.prototype.createTransport = function (url, requestedTransport, negotiateResponse, requestedTransferFormat) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
          var connectUrl, transports, _i, transports_1, endpoint, transport, ex_1;
          return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
              case 0:
                connectUrl = this.createConnectUrl(url, negotiateResponse.connectionId);
                if (!this.isITransport(requestedTransport)) return [3, 2];
                this.logger.log(ILogger.LogLevel.Debug, "Connection was provided an instance of ITransport, using that directly.");
                this.transport = requestedTransport;
                return [4, this.transport.connect(connectUrl, requestedTransferFormat)];
              case 1:
                _a.sent();
                this.changeState(0, 1);
                return [2];
              case 2:
                transports = negotiateResponse.availableTransports;
                _i = 0, transports_1 = transports;
                _a.label = 3;
              case 3:
                if (!(_i < transports_1.length)) return [3, 9];
                endpoint = transports_1[_i];
                this.connectionState = 0;
                transport = this.resolveTransport(endpoint, requestedTransport, requestedTransferFormat);
                if (!(typeof transport === "number")) return [3, 8];
                this.transport = this.constructTransport(transport);
                if (!(negotiateResponse.connectionId === null)) return [3, 5];
                return [4, this.getNegotiationResponse(url)];
              case 4:
                negotiateResponse = _a.sent();
                connectUrl = this.createConnectUrl(url, negotiateResponse.connectionId);
                _a.label = 5;
              case 5:
                _a.trys.push([5, 7,, 8]);
                return [4, this.transport.connect(connectUrl, requestedTransferFormat)];
              case 6:
                _a.sent();
                this.changeState(0, 1);
                return [2];
              case 7:
                ex_1 = _a.sent();
                this.logger.log(ILogger.LogLevel.Error, "Failed to start the transport '" + ITransport.HttpTransportType[transport] + "': " + ex_1);
                this.connectionState = 2;
                negotiateResponse.connectionId = null;
                return [3, 8];
              case 8:
                _i++;
                return [3, 3];
              case 9:
                throw new Error("Unable to initialize any of the available transports.");
            }
          });
        });
      };
      HttpConnection.prototype.constructTransport = function (transport) {
        switch (transport) {
          case ITransport.HttpTransportType.WebSockets:
            return new WebSocketTransport_1.WebSocketTransport(this.accessTokenFactory, this.logger, this.options.logMessageContent);
          case ITransport.HttpTransportType.ServerSentEvents:
            return new ServerSentEventsTransport_1.ServerSentEventsTransport(this.httpClient, this.accessTokenFactory, this.logger, this.options.logMessageContent);
          case ITransport.HttpTransportType.LongPolling:
            return new LongPollingTransport_1.LongPollingTransport(this.httpClient, this.accessTokenFactory, this.logger, this.options.logMessageContent);
          default:
            throw new Error("Unknown transport: " + transport + ".");
        }
      };
      HttpConnection.prototype.resolveTransport = function (endpoint, requestedTransport, requestedTransferFormat) {
        var transport = ITransport.HttpTransportType[endpoint.transport];
        if (transport === null || transport === undefined) {
          this.logger.log(ILogger.LogLevel.Debug, "Skipping transport '" + endpoint.transport + "' because it is not supported by this client.");
        } else {
          var transferFormats = endpoint.transferFormats.map(function (s) {
            return ITransport.TransferFormat[s];
          });
          if (transportMatches(requestedTransport, transport)) {
            if (transferFormats.indexOf(requestedTransferFormat) >= 0) {
              if (transport === ITransport.HttpTransportType.WebSockets && typeof WebSocket === "undefined" || transport === ITransport.HttpTransportType.ServerSentEvents && typeof EventSource === "undefined") {
                this.logger.log(ILogger.LogLevel.Debug, "Skipping transport '" + ITransport.HttpTransportType[transport] + "' because it is not supported in your environment.'");
              } else {
                this.logger.log(ILogger.LogLevel.Debug, "Selecting transport '" + ITransport.HttpTransportType[transport] + "'");
                return transport;
              }
            } else {
              this.logger.log(ILogger.LogLevel.Debug, "Skipping transport '" + ITransport.HttpTransportType[transport] + "' because it does not support the requested transfer format '" + ITransport.TransferFormat[requestedTransferFormat] + "'.");
            }
          } else {
            this.logger.log(ILogger.LogLevel.Debug, "Skipping transport '" + ITransport.HttpTransportType[transport] + "' because it was disabled by the client.");
          }
        }
        return null;
      };
      HttpConnection.prototype.isITransport = function (transport) {
        return transport && _typeof(transport) === "object" && "connect" in transport;
      };
      HttpConnection.prototype.changeState = function (from, to) {
        if (this.connectionState === from) {
          this.connectionState = to;
          return true;
        }
        return false;
      };
      HttpConnection.prototype.stopConnection = function (error) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
          return tslib_1.__generator(this, function (_a) {
            this.transport = null;
            error = this.stopError || error;
            if (error) {
              this.logger.log(ILogger.LogLevel.Error, "Connection disconnected with error '" + error + "'.");
            } else {
              this.logger.log(ILogger.LogLevel.Information, "Connection disconnected.");
            }
            this.connectionState = 2;
            if (this.onclose) {
              this.onclose(error);
            }
            return [2];
          });
        });
      };
      HttpConnection.prototype.resolveUrl = function (url) {
        if (url.lastIndexOf("https://", 0) === 0 || url.lastIndexOf("http://", 0) === 0) {
          return url;
        }
        if (typeof window === "undefined" || !window || !window.document) {
          throw new Error("Cannot resolve '" + url + "'.");
        }
        var aTag = window.document.createElement("a");
        aTag.href = url;
        this.logger.log(ILogger.LogLevel.Information, "Normalizing '" + url + "' to '" + aTag.href + "'.");
        return aTag.href;
      };
      HttpConnection.prototype.resolveNegotiateUrl = function (url) {
        var index = url.indexOf("?");
        var negotiateUrl = url.substring(0, index === -1 ? url.length : index);
        if (negotiateUrl[negotiateUrl.length - 1] !== "/") {
          negotiateUrl += "/";
        }
        negotiateUrl += "negotiate";
        negotiateUrl += index === -1 ? "" : url.substring(index);
        return negotiateUrl;
      };
      return HttpConnection;
    }();
    exports.HttpConnection = HttpConnection;
    function transportMatches(requestedTransport, actualTransport) {
      return !requestedTransport || (actualTransport & requestedTransport) !== 0;
    }
  });
  unwrapExports(HttpConnection_1);
  var HttpConnection_2 = HttpConnection_1.HttpConnection;
  var JsonHubProtocol_1 = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var JSON_HUB_PROTOCOL_NAME = "json";
    var JsonHubProtocol = function () {
      function JsonHubProtocol() {
        this.name = JSON_HUB_PROTOCOL_NAME;
        this.version = 1;
        this.transferFormat = ITransport.TransferFormat.Text;
      }
      JsonHubProtocol.prototype.parseMessages = function (input, logger) {
        if (typeof input !== "string") {
          throw new Error("Invalid input for JSON hub protocol. Expected a string.");
        }
        if (!input) {
          return [];
        }
        if (logger === null) {
          logger = Loggers.NullLogger.instance;
        }
        var messages = TextMessageFormat_1.TextMessageFormat.parse(input);
        var hubMessages = [];
        for (var _i = 0, messages_1 = messages; _i < messages_1.length; _i++) {
          var message = messages_1[_i];
          var parsedMessage = JSON.parse(message);
          if (typeof parsedMessage.type !== "number") {
            throw new Error("Invalid payload.");
          }
          switch (parsedMessage.type) {
            case IHubProtocol.MessageType.Invocation:
              this.isInvocationMessage(parsedMessage);
              break;
            case IHubProtocol.MessageType.StreamItem:
              this.isStreamItemMessage(parsedMessage);
              break;
            case IHubProtocol.MessageType.Completion:
              this.isCompletionMessage(parsedMessage);
              break;
            case IHubProtocol.MessageType.Ping:
              break;
            case IHubProtocol.MessageType.Close:
              break;
            default:
              logger.log(ILogger.LogLevel.Information, "Unknown message type '" + parsedMessage.type + "' ignored.");
              continue;
          }
          hubMessages.push(parsedMessage);
        }
        return hubMessages;
      };
      JsonHubProtocol.prototype.writeMessage = function (message) {
        return TextMessageFormat_1.TextMessageFormat.write(JSON.stringify(message));
      };
      JsonHubProtocol.prototype.isInvocationMessage = function (message) {
        this.assertNotEmptyString(message.target, "Invalid payload for Invocation message.");
        if (message.invocationId !== undefined) {
          this.assertNotEmptyString(message.invocationId, "Invalid payload for Invocation message.");
        }
      };
      JsonHubProtocol.prototype.isStreamItemMessage = function (message) {
        this.assertNotEmptyString(message.invocationId, "Invalid payload for StreamItem message.");
        if (message.item === undefined) {
          throw new Error("Invalid payload for StreamItem message.");
        }
      };
      JsonHubProtocol.prototype.isCompletionMessage = function (message) {
        if (message.result && message.error) {
          throw new Error("Invalid payload for Completion message.");
        }
        if (!message.result && message.error) {
          this.assertNotEmptyString(message.error, "Invalid payload for Completion message.");
        }
        this.assertNotEmptyString(message.invocationId, "Invalid payload for Completion message.");
      };
      JsonHubProtocol.prototype.assertNotEmptyString = function (value, errorMessage) {
        if (typeof value !== "string" || value === "") {
          throw new Error(errorMessage);
        }
      };
      return JsonHubProtocol;
    }();
    exports.JsonHubProtocol = JsonHubProtocol;
  });
  unwrapExports(JsonHubProtocol_1);
  var JsonHubProtocol_2 = JsonHubProtocol_1.JsonHubProtocol;
  var HubConnectionBuilder_1 = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var HubConnectionBuilder = function () {
      function HubConnectionBuilder() {}
      HubConnectionBuilder.prototype.configureLogging = function (logging) {
        Utils.Arg.isRequired(logging, "logging");
        if (isLogger(logging)) {
          this.logger = logging;
        } else {
          this.logger = new Utils.ConsoleLogger(logging);
        }
        return this;
      };
      HubConnectionBuilder.prototype.withUrl = function (url, transportTypeOrOptions) {
        Utils.Arg.isRequired(url, "url");
        this.url = url;
        if (_typeof(transportTypeOrOptions) === "object") {
          this.httpConnectionOptions = transportTypeOrOptions;
        } else {
          this.httpConnectionOptions = {
            transport: transportTypeOrOptions
          };
        }
        return this;
      };
      HubConnectionBuilder.prototype.withHubProtocol = function (protocol) {
        Utils.Arg.isRequired(protocol, "protocol");
        this.protocol = protocol;
        return this;
      };
      HubConnectionBuilder.prototype.build = function () {
        var httpConnectionOptions = this.httpConnectionOptions || {};
        if (httpConnectionOptions.logger === undefined) {
          httpConnectionOptions.logger = this.logger;
        }
        if (!this.url) {
          throw new Error("The 'HubConnectionBuilder.withUrl' method must be called before building the connection.");
        }
        var connection = new HttpConnection_1.HttpConnection(this.url, httpConnectionOptions);
        return HubConnection_1.HubConnection.create(connection, this.logger || Loggers.NullLogger.instance, this.protocol || new JsonHubProtocol_1.JsonHubProtocol());
      };
      return HubConnectionBuilder;
    }();
    exports.HubConnectionBuilder = HubConnectionBuilder;
    function isLogger(logger) {
      return logger.log !== undefined;
    }
  });
  unwrapExports(HubConnectionBuilder_1);
  var HubConnectionBuilder_2 = HubConnectionBuilder_1.HubConnectionBuilder;
  var cjs = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.VERSION = "0.0.0-DEV_BUILD";
    exports.HttpError = Errors.HttpError;
    exports.TimeoutError = Errors.TimeoutError;
    exports.DefaultHttpClient = HttpClient_1.DefaultHttpClient;
    exports.HttpClient = HttpClient_1.HttpClient;
    exports.HttpResponse = HttpClient_1.HttpResponse;
    exports.HubConnection = HubConnection_1.HubConnection;
    exports.HubConnectionBuilder = HubConnectionBuilder_1.HubConnectionBuilder;
    exports.MessageType = IHubProtocol.MessageType;
    exports.LogLevel = ILogger.LogLevel;
    exports.HttpTransportType = ITransport.HttpTransportType;
    exports.TransferFormat = ITransport.TransferFormat;
    exports.NullLogger = Loggers.NullLogger;
    exports.JsonHubProtocol = JsonHubProtocol_1.JsonHubProtocol;
  });
  unwrapExports(cjs);
  var cjs_1 = cjs.VERSION;
  var cjs_2 = cjs.HttpError;
  var cjs_3 = cjs.TimeoutError;
  var cjs_4 = cjs.DefaultHttpClient;
  var cjs_5 = cjs.HttpClient;
  var cjs_6 = cjs.HttpResponse;
  var cjs_7 = cjs.HubConnection;
  var cjs_8 = cjs.HubConnectionBuilder;
  var cjs_9 = cjs.MessageType;
  var cjs_10 = cjs.LogLevel;
  var cjs_11 = cjs.HttpTransportType;
  var cjs_12 = cjs.TransferFormat;
  var cjs_13 = cjs.NullLogger;
  var cjs_14 = cjs.JsonHubProtocol;
  var browserIndex = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    if (!Uint8Array.prototype.indexOf) {
      Object.defineProperty(Uint8Array.prototype, "indexOf", {
        value: Array.prototype.indexOf,
        writable: true
      });
    }
    if (!Uint8Array.prototype.slice) {
      Object.defineProperty(Uint8Array.prototype, "slice", {
        value: Array.prototype.slice,
        writable: true
      });
    }
    if (!Uint8Array.prototype.forEach) {
      Object.defineProperty(Uint8Array.prototype, "forEach", {
        value: Array.prototype.forEach,
        writable: true
      });
    }
    tslib_1.__exportStar(cjs, exports);
  });
  var browserIndex$1 = unwrapExports(browserIndex);
  return browserIndex$1;
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js */ 2)["default"]))

/***/ }),
/* 49 */
/*!********************************************************!*\
  !*** E:/公司项目/e考监控/Api-Example/TrtcCloud/permission.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
/**
 * 本模块封装了Android、iOS的应用权限判断、打开应用权限设置界面、以及位置系统服务是否开启
 */
var isIos;

// 判断推送权限是否开启
function judgeIosPermissionPush() {
  var result = false;
  var UIApplication = plus.ios.import("UIApplication");
  var app = UIApplication.sharedApplication();
  var enabledTypes = 0;
  if (app.currentUserNotificationSettings) {
    var settings = app.currentUserNotificationSettings();
    enabledTypes = settings.plusGetAttribute("types");
    console.log("enabledTypes1:" + enabledTypes);
    if (enabledTypes == 0) {
      console.log("推送权限没有开启");
    } else {
      result = true;
      console.log("已经开启推送功能!");
    }
    plus.ios.deleteObject(settings);
  } else {
    enabledTypes = app.enabledRemoteNotificationTypes();
    if (enabledTypes == 0) {
      console.log("推送权限没有开启!");
    } else {
      result = true;
      console.log("已经开启推送功能!");
    }
    console.log("enabledTypes2:" + enabledTypes);
  }
  plus.ios.deleteObject(app);
  plus.ios.deleteObject(UIApplication);
  return result;
}

// 判断定位权限是否开启
function judgeIosPermissionLocation() {
  var result = false;
  var cllocationManger = plus.ios.import("CLLocationManager");
  var status = cllocationManger.authorizationStatus();
  result = status != 2;
  console.log("定位权限开启：" + result);
  // 以下代码判断了手机设备的定位是否关闭，推荐另行使用方法 checkSystemEnableLocation
  /* var enable = cllocationManger.locationServicesEnabled();
  var status = cllocationManger.authorizationStatus();
  console.log("enable:" + enable);
  console.log("status:" + status);
  if (enable && status != 2) {
    result = true;
    console.log("手机定位服务已开启且已授予定位权限");
  } else {
    console.log("手机系统的定位没有打开或未给予定位权限");
  } */
  plus.ios.deleteObject(cllocationManger);
  return result;
}
// 判断麦克风权限是否开启
function judgeIosPermissionRecord() {
  var result = false;
  var avaudiosession = plus.ios.import("AVAudioSession");
  var avaudio = avaudiosession.sharedInstance();
  var permissionStatus = avaudio.recordPermission();
  console.log("permissionStatus:" + permissionStatus);
  if (permissionStatus == 1684369017 || permissionStatus == 1970168948) {
    console.log("麦克风权限没有开启");
  } else {
    result = true;
    console.log("麦克风权限已经开启");
  }
  plus.ios.deleteObject(avaudiosession);
  return result;
}

// 判断相机权限是否开启
function judgeIosPermissionCamera() {
  var result = false;
  var AVCaptureDevice = plus.ios.import("AVCaptureDevice");
  var authStatus = AVCaptureDevice.authorizationStatusForMediaType('vide');
  console.log("authStatus:" + authStatus);
  if (authStatus == 3) {
    result = true;
    console.log("相机权限已经开启");
  } else {
    console.log("相机权限没有开启");
  }
  plus.ios.deleteObject(AVCaptureDevice);
  return result;
}

// 判断相册权限是否开启
function judgeIosPermissionPhotoLibrary() {
  var result = false;
  var PHPhotoLibrary = plus.ios.import("PHPhotoLibrary");
  var authStatus = PHPhotoLibrary.authorizationStatus();
  console.log("authStatus:" + authStatus);
  if (authStatus == 3) {
    result = true;
    console.log("相册权限已经开启");
  } else {
    console.log("相册权限没有开启");
  }
  plus.ios.deleteObject(PHPhotoLibrary);
  return result;
}

// 判断通讯录权限是否开启
function judgeIosPermissionContact() {
  var result = false;
  var CNContactStore = plus.ios.import("CNContactStore");
  var cnAuthStatus = CNContactStore.authorizationStatusForEntityType(0);
  if (cnAuthStatus == 3) {
    result = true;
    console.log("通讯录权限已经开启");
  } else {
    console.log("通讯录权限没有开启");
  }
  plus.ios.deleteObject(CNContactStore);
  return result;
}

// 判断日历权限是否开启
function judgeIosPermissionCalendar() {
  var result = false;
  var EKEventStore = plus.ios.import("EKEventStore");
  var ekAuthStatus = EKEventStore.authorizationStatusForEntityType(0);
  if (ekAuthStatus == 3) {
    result = true;
    console.log("日历权限已经开启");
  } else {
    console.log("日历权限没有开启");
  }
  plus.ios.deleteObject(EKEventStore);
  return result;
}

// 判断备忘录权限是否开启
function judgeIosPermissionMemo() {
  var result = false;
  var EKEventStore = plus.ios.import("EKEventStore");
  var ekAuthStatus = EKEventStore.authorizationStatusForEntityType(1);
  if (ekAuthStatus == 3) {
    result = true;
    console.log("备忘录权限已经开启");
  } else {
    console.log("备忘录权限没有开启");
  }
  plus.ios.deleteObject(EKEventStore);
  return result;
}

// Android权限查询
function requestAndroidPermission(permissionID) {
  return new Promise(function (resolve, reject) {
    plus.android.requestPermissions([permissionID],
    // 理论上支持多个权限同时查询，但实际上本函数封装只处理了一个权限的情况。有需要的可自行扩展封装
    function (resultObj) {
      var result = 0;
      for (var i = 0; i < resultObj.granted.length; i++) {
        var grantedPermission = resultObj.granted[i];
        console.log('已获取的权限：' + grantedPermission);
        result = 1;
      }
      for (var i = 0; i < resultObj.deniedPresent.length; i++) {
        var deniedPresentPermission = resultObj.deniedPresent[i];
        console.log('拒绝本次申请的权限：' + deniedPresentPermission);
        result = 0;
      }
      for (var i = 0; i < resultObj.deniedAlways.length; i++) {
        var deniedAlwaysPermission = resultObj.deniedAlways[i];
        console.log('永久拒绝申请的权限：' + deniedAlwaysPermission);
        result = -1;
      }
      resolve(result);
      // 若所需权限被拒绝,则打开APP设置界面,可以在APP设置界面打开相应权限
      // if (result != 1) {
      // gotoAppPermissionSetting()
      // }
    }, function (error) {
      console.log('申请权限错误：' + error.code + " = " + error.message);
      resolve({
        code: error.code,
        message: error.message
      });
    });
  });
}

// 使用一个方法，根据参数判断权限
function judgeIosPermission(permissionID) {
  if (permissionID == "location") {
    return judgeIosPermissionLocation();
  } else if (permissionID == "camera") {
    return judgeIosPermissionCamera();
  } else if (permissionID == "photoLibrary") {
    return judgeIosPermissionPhotoLibrary();
  } else if (permissionID == "record") {
    return judgeIosPermissionRecord();
  } else if (permissionID == "push") {
    return judgeIosPermissionPush();
  } else if (permissionID == "contact") {
    return judgeIosPermissionContact();
  } else if (permissionID == "calendar") {
    return judgeIosPermissionCalendar();
  } else if (permissionID == "memo") {
    return judgeIosPermissionMemo();
  }
  return false;
}

// 跳转到**应用**的权限页面
function gotoAppPermissionSetting() {
  if (isIos) {
    var UIApplication = plus.ios.import("UIApplication");
    var application2 = UIApplication.sharedApplication();
    var NSURL2 = plus.ios.import("NSURL");
    // var setting2 = NSURL2.URLWithString("prefs:root=LOCATION_SERVICES");		
    var setting2 = NSURL2.URLWithString("app-settings:");
    application2.openURL(setting2);
    plus.ios.deleteObject(setting2);
    plus.ios.deleteObject(NSURL2);
    plus.ios.deleteObject(application2);
  } else {
    // console.log(plus.device.vendor);
    var Intent = plus.android.importClass("android.content.Intent");
    var Settings = plus.android.importClass("android.provider.Settings");
    var Uri = plus.android.importClass("android.net.Uri");
    var mainActivity = plus.android.runtimeMainActivity();
    var intent = new Intent();
    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
    var uri = Uri.fromParts("package", mainActivity.getPackageName(), null);
    intent.setData(uri);
    mainActivity.startActivity(intent);
  }
}

// 检查系统的设备服务是否开启
// var checkSystemEnableLocation = async function () {
function checkSystemEnableLocation() {
  if (isIos) {
    var result = false;
    var cllocationManger = plus.ios.import("CLLocationManager");
    var result = cllocationManger.locationServicesEnabled();
    console.log("系统定位开启:" + result);
    plus.ios.deleteObject(cllocationManger);
    return result;
  } else {
    var context = plus.android.importClass("android.content.Context");
    var locationManager = plus.android.importClass("android.location.LocationManager");
    var main = plus.android.runtimeMainActivity();
    var mainSvr = main.getSystemService(context.LOCATION_SERVICE);
    var result = mainSvr.isProviderEnabled(locationManager.GPS_PROVIDER);
    console.log("系统定位开启:" + result);
    return result;
  }
}

//  module.exports = {
//    judgeIosPermission: judgeIosPermission,
//    requestAndroidPermission: requestAndroidPermission,
//    checkSystemEnableLocation: checkSystemEnableLocation,
//    gotoAppPermissionSetting: gotoAppPermissionSetting
//  }

// HBuilder 选择 vue3 时, 上面的打包无法通过 import 进行引入
var _default = {
  judgeIosPermission: judgeIosPermission,
  requestAndroidPermission: requestAndroidPermission,
  checkSystemEnableLocation: checkSystemEnableLocation,
  gotoAppPermissionSetting: gotoAppPermissionSetting
};
exports.default = _default;

/***/ }),
/* 50 */
/*!*******************************************************!*\
  !*** E:/公司项目/e考监控/Api-Example/TrtcCloud/lib/index.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ 4);
Object.defineProperty(exports, "__esModule", {
  value: true
});
var _exportNames = {};
exports.default = void 0;
var _classCallCheck2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ 23));
var _createClass2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/createClass */ 24));
var _TrtcCloudImpl = _interopRequireDefault(__webpack_require__(/*! ./TrtcCloudImpl */ 51));
var _TrtcDefines = __webpack_require__(/*! ./TrtcDefines */ 55);
Object.keys(_TrtcDefines).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _TrtcDefines[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _TrtcDefines[key];
    }
  });
});
var version = '1.2.0';
/**
 * TrtcCloud
 *
 * @class TrtcCloud
 */
var TrtcCloud = /*#__PURE__*/function () {
  function TrtcCloud() {
    (0, _classCallCheck2.default)(this, TrtcCloud);
  }
  (0, _createClass2.default)(TrtcCloud, [{
    key: "on",
    value:
    /**
     * 设置 TrtcCloud 事件监听
     *
     * @param {String} event 事件名称
     * @param {Function} callback 事件回调
     * @memberof TrtcCloud
     *
     * @example
     * this.trtcCloud = TrtcCloud.createInstance(); // 创建 trtcCloud 实例
     * this.trtcCloud.on('onEnterRoom', (res) => {});
     */
    function on(event, callback) {
      return _TrtcCloudImpl.default._getInstance().on(event, callback);
    }
    /**
     * 取消事件绑定<br>
     *
     * @param {String} event 事件名称，传入通配符 '*' 会解除所有事件绑定。
     * @memberof TrtcCloud
     * @example
     * this.trtcCloud.off('onEnterRoom');
     *
     * this.trtcCloud.off('*'); // 取消所有绑定的事件
     */
  }, {
    key: "off",
    value: function off(event) {
      return _TrtcCloudImpl.default._getInstance().off(event);
    }
    /**
     * 进房<br>
     * 调用接口后，您会收到来自 TRTCCallback 中的 [onEnterRoom(result)]{@link TRTCCallback#onEnterRoom} 回调
     * 如果加入成功，result 会是一个正数（result > 0），表示加入房间所消耗的时间，单位是毫秒（ms）。<br>
     * 如果加入失败，result 会是一个负数（result < 0），表示进房失败的错误码。
     *
     * * 参数 scene 的枚举值如下：
     * - {@link TRTCAppSceneVideoCall}：<br>
     *          视频通话场景，支持720P、1080P高清画质，单个房间最多支持300人同时在线，最高支持50人同时发言。<br>
     *          适合：[1对1视频通话]、[300人视频会议]、[在线问诊]、[视频聊天]、[远程面试]等。<br>
     * - {@link TRTCAppSceneAudioCall}：<br>
     *          语音通话场景，支持 48kHz，支持双声道。单个房间最多支持300人同时在线，最高支持50人同时发言。<br>
     *          适合：[1对1语音通话]、[300人语音会议]、[语音聊天]、[语音会议]、[在线狼人杀]等。<br>
     * - {@link TRTCAppSceneLIVE}：<br>
     *          视频互动直播，支持平滑上下麦，切换过程无需等待，主播延时小于300ms；支持十万级别观众同时播放，播放延时低至1000ms。<br>
     *          适合：[视频低延时直播]、[十万人互动课堂]、[视频直播 PK]、[视频相亲房]、[互动课堂]、[远程培训]、[超大型会议]等。<br>
     * - {@link TRTCAppSceneVoiceChatRoom}：<br>
     *          语音互动直播，支持平滑上下麦，切换过程无需等待，主播延时小于300ms；支持十万级别观众同时播放，播放延时低至1000ms。<br>
     *          适合：[语音低延时直播]、[语音直播连麦]、[语聊房]、[K 歌房]、[FM 电台]等。<br>
     *
     * **Note:**
     * 1. 当 scene 选择为 TRTCAppSceneLIVE 或 TRTCAppSceneVoiceChatRoom 时，您必须通过 TRTCParams 中的 role 字段指定当前用户的角色。
     * 2. 不管进房是否成功，enterRoom 都必须与 exitRoom 配对使用，在调用 `exitRoom` 前再次调用 `enterRoom` 函数会导致不可预期的错误问题。
     *
     * @param {TRTCParams} params - 进房参数
     * @param {Number} params.sdkAppId      - 应用标识（必填）
     * @param {String} params.userId        - 用户标识（必填）
     * @param {String} params.userSig       - 用户签名（必填）
     * @param {Number} params.roomId        - 房间号码, roomId 和 strRoomId 必须填一个, 若您选用 strRoomId，则 roomId 需要填写为0。
     * @param {String} params.strRoomId     - 字符串房间号码 [选填]，在同一个房间内的用户可以看到彼此并进行视频通话, roomId 和 strRoomId 必须填一个。若两者都填，则优先选择 roomId
     * @param {TRTCRoleType} params.role    - 直播场景下的角色，默认值：主播
     * - TRTCRoleAnchor: 主播，可以上行视频和音频，一个房间里最多支持50个主播同时上行音视频。
     * - TRTCRoleAudience: 观众，只能观看，不能上行视频和音频，一个房间里的观众人数没有上限。
     * @param {String=} params.privateMapKey - 房间签名（非必填）
     * @param {String=} params.businessInfo  - 业务数据（非必填）
     * @param {String=} params.streamId      - 自定义 CDN 播放地址（非必填）
     * @param {String=} params.userDefineRecordId - 设置云端录制完成后的回调消息中的 "userdefinerecordid" 字段内容，便于您更方便的识别录制回调（非必填）
     * @param {TRTCAppScene} scene 应用场景，目前支持视频通话（TRTCAppSceneVideoCall）、语音通话（TRTCAppSceneAudioCall）、在线直播（TRTCAppSceneLIVE）、语音聊天室（VTRTCAppSceneVoiceChatRoom）四种场景，
     * 详见 [TrtcDefines] 中 TRTCAppScene 参数定义
     *
     * @memberof TrtcCloud
     * @example
     * import { TRTCAppScene } from '@/TrtcCloud/lib/TrtcDefines';
     * this.trtcCloud = TrtcCloud.createInstance(); // 创建实例，只需创建一次
     * const params = {
     *   sdkAppId: 0,
     *   userId: 'xxx',
     *   roomId: 12345,
     *   userSig: 'xxx'
     * };
     * this.trtcCloud.enterRoom(params, TRTCAppScene.TRTCAppSceneVideoCall);
     */
  }, {
    key: "enterRoom",
    value: function enterRoom(params, scene) {
      return _TrtcCloudImpl.default._getInstance().enterRoom(params, scene);
    }
    /**
     * 退房<br>
     * 执行退出房间的相关逻辑释放资源后，SDK 会通过 `onExitRoom()` 回调通知到您
     *
     * **Note:**
     * 1. 如果您要再次调用 `enterRoom()` 或者切换到其它的音视频 SDK，请等待 `onExitRoom()` 回调到来后再执行相关操作，否则可能会遇到如摄像头、麦克风设备被强占等各种异常问题。
     *
     * @memberof TrtcCloud
     * @example
     * this.trtcCloud.exitRoom();
     */
  }, {
    key: "exitRoom",
    value: function exitRoom() {
      return _TrtcCloudImpl.default._getInstance().exitRoom();
    }
    /**
     * 切换角色，仅适用于直播场景（TRTCAppSceneLIVE 和 TRTCAppSceneVoiceChatRoom）
     *
     * 在直播场景下，一个用户可能需要在“观众”和“主播”之间来回切换。
     * 您可以在进房前通过 TRTCParams 中的 role 字段确定角色，也可以通过 switchRole 在进房后切换角色。
     *
     * @param {TRTCRoleType} role - 目标角色，默认为主播
     * - TRTCRoleAnchor: 主播，可以上行视频和音频，一个房间里最多支持50个主播同时上行音视频。
     * - TRTCRoleAudience: 观众，只能观看，不能上行视频和音频，一个房间里的观众人数没有上限。
     *
     * @memberof TrtcCloud
     * @example
     * import { TRTCRoleType } from '@/TrtcCloud/lib/TrtcDefines';
     * this.trtcCloud.switchRole(TRTCRoleType.TRTCRoleAudience);
     */
  }, {
    key: "switchRole",
    value: function switchRole(role) {
      return _TrtcCloudImpl.default._getInstance().switchRole(role);
    }
    /**
     * 开启本地视频的预览画面<br>
     * 当开始渲染首帧摄像头画面时，您会收到 `onFirstVideoFrame(null)` 回调
     *
     * @param {Boolean} isFrontCamera 前置、后置摄像头，true：前置摄像头；false：后置摄像头，**默认为 true**
     * @param {String=} viewId 用于承载视频画面的渲染控件，使用原生插件中的 TRTCCloudUniPlugin-TXLocalViewComponent component，需要提供 viewId 属性值，例如 viewId=userId
     * @memberof TrtcCloud
     * @example
     * // 预览本地画面
     * const viewId = this.userId;
     * this.trtcCloud.startLocalPreview(true, viewId);
     */
  }, {
    key: "startLocalPreview",
    value: function startLocalPreview() {
      var isFrontCamera = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
      var viewId = arguments.length > 1 ? arguments[1] : undefined;
      return _TrtcCloudImpl.default._getInstance().startLocalPreview(isFrontCamera, viewId);
    }
    /**
     * 设置视频编码器的编码参数
     * - 该设置能够决定远端用户看到的画面质量，同时也能决定云端录制出的视频文件的画面质量。
     * @param {TRTCVideoEncParam} param 用于设置视频编码器的相关参数
     * @memberof TrtcCloud
     * @example
     *
     * import { TRTCVideoResolution, TRTCVideoResolutionMode, TRTCVideoEncParam } from '@/TrtcCloud/lib/TrtcDefines';
     * const videoResolution = TRTCVideoResolution.TRTCVideoResolution_480_360;
     * const videoResolutionMode = TRTCVideoResolutionMode.TRTCVideoResolutionModeLandscape; // 横屏采集
     * const videoFps = 15;
     * const videoBitrate = 900;
     * const minVideoBitrate = 200;
     * const enableAdjustRes = false;
     * // const param = new TRTCVideoEncParam(videoResolution, videoResolutionMode, videoFps, videoBitrate, minVideoBitrate, enableAdjustRes); // v1.1.0 方式
     *
     * const param = { // v1.2.0 以上版本支持的方式
     *  videoResolution,
     *  videoResolutionMode,
     *  videoFps,
     *  videoBitrate,
     *  minVideoBitrate,
     *  enableAdjustRes,
     * };
     *
     * this.trtcCloud.setVideoEncoderParam(param);
     */
  }, {
    key: "setVideoEncoderParam",
    value: function setVideoEncoderParam(param) {
      return _TrtcCloudImpl.default._getInstance().setVideoEncoderParam(param);
    }
    /**
     * 切换前置或后置摄像头
     *
     * @param {Boolean} isFrontCamera 前置、后置摄像头，true：前置摄像头；false：后置摄像头
     * @memberof TrtcCloud
     * @example
     * // 切换前置或后置摄像头
     * const isFrontCamera = true;
     * this.trtcCloud.switchCamera(isFrontCamera);
     */
  }, {
    key: "switchCamera",
    value: function switchCamera(isFrontCamera) {
      return _TrtcCloudImpl.default._getInstance().switchCamera(isFrontCamera);
    }
    /**
     * 停止本地视频采集及预览
     *
     * @memberof TrtcCloud
     * @example
     * this.trtcCloud.stopLocalPreview();
     */
  }, {
    key: "stopLocalPreview",
    value: function stopLocalPreview() {
      return _TrtcCloudImpl.default._getInstance().stopLocalPreview();
    }
    /**
     * 设置本地画面的渲染参数，可设置的参数包括有：画面的旋转角度、填充模式以及左右镜像等。
     * @param {TRTCRenderParams} params - 本地图像的参数
     * @param {TRTCVideoRotation} params.rotation - 图像的顺时针旋转角度，支持90、180以及270旋转角度，默认值：TRTCVideoRotation.TRTCVideoRotation_0
     * @param {TRTCVideoFillMode} params.fillMode - 视频画面填充模式，填充（画面可能会被拉伸裁剪）或适应（画面可能会有黑边），默认值：TRTCVideoFillMode.TRTCVideoFillMode_Fill
     * @param {TRTCVideoMirrorType} params.mirrorType - 画面镜像模式，默认值：TRTCVideoMirrorType.TRTCVideoMirrorType_Auto
     *
     * @memberof TrtcCloud
     * @example
     * import { TRTCVideoRotation, TRTCVideoFillMode, TRTCVideoMirrorType } from '@/TrtcCloud/lib/TrtcDefines';
     * const renderParams = {
     *  rotation: TRTCVideoRotation.TRTCVideoRotation_0,
     *  fillMode: TRTCVideoFillMode.TRTCVideoFillMode_Fill,
     *  mirrorType: TRTCVideoMirrorType.TRTCVideoMirrorType_Auto
     * };
     * this.trtcCloud.setLocalRenderParams(renderParams);
     */
  }, {
    key: "setLocalRenderParams",
    value: function setLocalRenderParams(params) {
      return _TrtcCloudImpl.default._getInstance().setLocalRenderParams(params);
    }
    /**
     * 暂停/恢复发布本地的视频流
     *
     * 该接口可以暂停（或恢复）发布本地的视频画面，暂停之后，同一房间中的其他用户将无法继续看到自己画面。 该接口在指定 TRTCVideoStreamTypeBig 时等效于 start/stopLocalPreview 这两个接口，但具有更好的响应速度。 因为 start/stopLocalPreview 需要打开和关闭摄像头，而打开和关闭摄像头都是硬件设备相关的操作，非常耗时。 相比之下，muteLocalVideo 只需要在软件层面对数据流进行暂停或者放行即可，因此效率更高，也更适合需要频繁打开关闭的场景。 当暂停/恢复发布指定 TRTCVideoStreamTypeBig 后，同一房间中的其他用户将会收到 onUserVideoAvailable 回调通知。 当暂停/恢复发布指定 TRTCVideoStreamTypeSub 后，同一房间中的其他用户将会收到 onUserSubStreamAvailable 回调通知。
     * @param {TRTCVideoStreamType} streamType 要暂停/恢复的视频流类型（仅支持 TRTCVideoStreamTypeBig 和 TRTCVideoStreamTypeSub）
     * @param {Boolean} mute - true：屏蔽；false：开启，默认值：false
     *
     * @memberof TrtcCloud
     * @example
     * this.trtcCloud.muteLocalVideo(TRTCVideoStreamType.TRTCVideoStreamTypeBig, true);
     */
  }, {
    key: "muteLocalVideo",
    value: function muteLocalVideo(streamType, mute) {
      return _TrtcCloudImpl.default._getInstance().muteLocalVideo(streamType, mute);
    }
    /**
     * 显示远端视频或辅流<br>
     *
     * @param {String} userId 指定远端用户的 userId
     * @param {TRTCVideoStreamType} streamType 指定要观看 userId 的视频流类型
     * - 高清大画面：TRTCVideoStreamType.TRTCVideoStreamTypeBig
     * - 低清小画面：TRTCVideoStreamType.TRTCVideoStreamTypeSmall
     * - 辅流（屏幕分享）：TRTCVideoStreamType.TRTCVideoStreamTypeSub
     * @param {String} viewId 用于承载视频画面的渲染控件，使用原生插件中的 TRTCCloudUniPlugin-TXRemoteViewComponent component，需要提供 viewId 属性值，例如 viewId=userId
     * @memberof TrtcCloud
     * @example
     * import { TRTCVideoStreamType } from '@/TrtcCloud/lib/TrtcDefines';
     * const viewId = this.remoteUserId;
     * this.trtcCloud.startRemoteView(userId, TRTCVideoStreamType.TRTCVideoStreamTypeBig, viewId);
     */
  }, {
    key: "startRemoteView",
    value: function startRemoteView(userId, streamType, viewId) {
      return _TrtcCloudImpl.default._getInstance().startRemoteView(userId, streamType, viewId);
    }
    /**
     * 停止显示远端视频画面，同时不再拉取该远端用户的视频数据流<br>
     * 指定要停止观看的 userId 的视频流类型
     *
     * @param {String} userId 指定的远端用户 ID
     * @param {TRTCVideoStreamType} streamType
     * - 高清大画面：TRTCVideoStreamType.TRTCVideoStreamTypeBig
     * - 低清小画面：TRTCVideoStreamType.TRTCVideoStreamTypeSmall
     * - 辅流（屏幕分享）：TRTCVideoStreamType.TRTCVideoStreamTypeSub
     * @memberof TrtcCloud
     * @example
     * import { TRTCVideoStreamType } from '@/TrtcCloud/lib/TrtcDefines';
     * this.trtcCloud.stopRemoteView(remoteUserId, TRTCVideoStreamType.TRTCVideoStreamTypeBig);
     */
  }, {
    key: "stopRemoteView",
    value: function stopRemoteView(userId, streamType) {
      return _TrtcCloudImpl.default._getInstance().stopRemoteView(userId, streamType);
    }
    /**
     * 设置远端画面的渲染参数，可设置的参数包括有：画面的旋转角度、填充模式以及左右镜像等。
     * @param {String} userId 远端用户 ID
     * @param {TRTCVideoStreamType} streamType 可以设置为主路画面（TRTCVideoStreamTypeBig）或辅路画面（TRTCVideoStreamTypeSub）
     * @param {TRTCRenderParams} params - 图像的参数
     * @param {TRTCVideoRotation} params.rotation - 图像的顺时针旋转角度，支持90、180以及270旋转角度，默认值：TRTCVideoRotation.TRTCVideoRotation_0
     * @param {TRTCVideoFillMode} params.fillMode - 视频画面填充模式，填充（画面可能会被拉伸裁剪）或适应（画面可能会有黑边），默认值：TRTCVideoFillMode.TRTCVideoFillMode_Fill
     * @param {TRTCVideoMirrorType} params.mirrorType - 画面镜像模式，默认值：TRTCVideoMirrorType.TRTCVideoMirrorType_Auto
     * @memberof TrtcCloud
     * @example
     * import { TRTCVideoRotation, TRTCVideoFillMode, TRTCVideoMirrorType } from '@/TrtcCloud/lib/TrtcDefines';
     * const renderParams = {
     *  rotation: TRTCVideoRotation.TRTCVideoRotation_0,
     *  fillMode: TRTCVideoFillMode.TRTCVideoFillMode_Fill,
     *  mirrorType: TRTCVideoMirrorType.TRTCVideoMirrorType_Auto
     * };
     * this.trtcCloud.setRemoteRenderParams(userId, TRTCVideoStreamType.TRTCVideoStreamTypeBig, renderParams);
     */
  }, {
    key: "setRemoteRenderParams",
    value: function setRemoteRenderParams(userId, streamType, params) {}
    /**
     * 视频画面截图
     *
     * 您可以通过本接口截取本地的视频画面，远端用户的主路画面以及远端用户的辅路（屏幕分享）画面。
     * @param {String | null} userId 用户 ID，如指定 null 表示截取本地的视频画面
     * @param {TRTCVideoStreamType} streamType 视频流类型，可选择截取主路画面（TRTCVideoStreamTypeBig，常用于摄像头）或辅路画面（TRTCVideoStreamTypeSub，常用于屏幕分享）
     *
     * @memberof TrtcCloud
     * @example
     * import { TRTCVideoStreamType } from '@/TrtcCloud/lib/TrtcDefines';
     * this.trtcCloud.snapshotVideo(null, TRTCVideoStreamType.TRTCVideoStreamTypeBig); // 截取本地画面
     * this.trtcCloud.snapshotVideo(this.remoteUserId, TRTCVideoStreamType.TRTCVideoStreamTypeBig); // 截取远端指定用户画面
     */
  }, {
    key: "snapshotVideo",
    value: function snapshotVideo(userId, streamType, listener) {
      return _TrtcCloudImpl.default._getInstance().snapshotVideo(userId, streamType, listener);
    }
    /**
     * 开启本地音频的采集和上行, 并设置音频质量<br>
     * 该函数会启动麦克风采集，并将音频数据传输给房间里的其他用户。 SDK 不会默认开启本地音频采集和上行，您需要调用该函数开启，否则房间里的其他用户将无法听到您的声音<br>
     * 主播端的音质越高，观众端的听感越好，但传输所依赖的带宽也就越高，在带宽有限的场景下也更容易出现卡顿
     *
     * @param {TRTCAudioQuality} quality 声音音质
     * - TRTCAudioQualitySpeech，流畅：采样率：16k；单声道；音频裸码率：16kbps；适合语音通话为主的场景，比如在线会议，语音通话。
     * - TRTCAudioQualityDefault，默认：采样率：48k；单声道；音频裸码率：50kbps；SDK 默认的音频质量，如无特殊需求推荐选择之。
     * - TRTCAudioQualityMusic，高音质：采样率：48k；双声道 + 全频带；音频裸码率：128kbps；适合需要高保真传输音乐的场景，比如在线K歌、音乐直播等
     * @memberof TrtcCloud
     * @example
     * import { TRTCAudioQuality } from '@/TrtcCloud/lib/TrtcDefines';
     * this.trtcCloud.startLocalAudio(TRTCAudioQuality.TRTCAudioQualityDefault);
     */
  }, {
    key: "startLocalAudio",
    value: function startLocalAudio(quality) {
      return _TrtcCloudImpl.default._getInstance().startLocalAudio(quality);
    }
    /**
     * 关闭本地音频的采集和上行<br>
     * 当关闭本地音频的采集和上行，房间里的其它成员会收到 `onUserAudioAvailable(false)` 回调通知
     *
     * @memberof TrtcCloud
     * @example
     * this.trtcCloud.stopLocalAudio();
     */
  }, {
    key: "stopLocalAudio",
    value: function stopLocalAudio() {
      return _TrtcCloudImpl.default._getInstance().stopLocalAudio();
    }
    /**
     * 静音本地的音频
     *
     * 当静音本地音频后，房间里的其它成员会收到 onUserAudioAvailable(false) 回调通知。
     * 与 stopLocalAudio 不同之处在于，muteLocalAudio 并不会停止发送音视频数据，而是会继续发送码率极低的静音包。
     * 在对录制质量要求很高的场景中，选择 muteLocalAudio 是更好的选择，能录制出兼容性更好的 MP4 文件。
     * 这是由于 MP4 等视频文件格式，对于音频的连续性是要求很高的，简单粗暴地 stopLocalAudio 会导致录制出的 MP4 不易播放。
     *
     * @param {Boolean} mute - true：屏蔽；false：开启，默认值：false
     *
     * @memberof TrtcCloud
     * @example
     * this.trtcCloud.muteLocalAudio(true);
     */
  }, {
    key: "muteLocalAudio",
    value: function muteLocalAudio(mute) {
      return _TrtcCloudImpl.default._getInstance().muteLocalAudio(mute);
    }
    /**
     * 静音掉某一个用户的声音，同时不再拉取该远端用户的音频数据流
     *
     * @param {String}  userId - 用户 ID
     * @param {Boolean} mute   - true：静音；false：非静音
     *
     * @memberof TrtcCloud
     * @example
     * this.trtcCloud.muteRemoteAudio('denny', true);
     */
  }, {
    key: "muteRemoteAudio",
    value: function muteRemoteAudio(userId, mute) {
      return _TrtcCloudImpl.default._getInstance().muteRemoteAudio(userId, mute);
    }
    /**
     * 静音掉所有用户的声音，同时不再拉取该远端用户的音频数据流
     *
     * @param {Boolean} mute - true：静音；false：非静音
     *
     * @memberof TrtcCloud
     * @example
     * this.trtcCloud.muteAllRemoteAudio(true);
     */
  }, {
    key: "muteAllRemoteAudio",
    value: function muteAllRemoteAudio(mute) {
      return _TrtcCloudImpl.default._getInstance().muteAllRemoteAudio(mute);
    }
    /**
     * 设置音频路由
     *
     * 设置“音频路由”，即设置声音是从手机的扬声器还是从听筒中播放出来，因此该接口仅适用于手机等移动端设备。 手机有两个扬声器：一个是位于手机顶部的听筒，一个是位于手机底部的立体声扬声器。
     * 设置音频路由为听筒时，声音比较小，只有将耳朵凑近才能听清楚，隐私性较好，适合用于接听电话。 设置音频路由为扬声器时，声音比较大，不用将手机贴脸也能听清，因此可以实现“免提”的功能。
     *
     * @param {TRTCAudioRoute} route 音频路由，即声音由哪里输出（扬声器、听筒）, 默认值：TRTCAudioRoute.TRTCAudioRouteSpeaker（扬声器）,
     * @memberof TrtcCloud
     * @example
     * import { TRTCAudioRoute } from '@/TrtcCloud/lib/TrtcDefines';
     * this.trtcCloud.setAudioRoute(TRTCAudioRoute.TRTCAudioRouteSpeaker); // TRTCAudioRoute.TRTCAudioRouteEarpiece (听筒)
     */
  }, {
    key: "setAudioRoute",
    value: function setAudioRoute(route) {
      return _TrtcCloudImpl.default._getInstance().setAudioRoute(route);
    }
    /**
     * 启用或关闭音量大小提示
     *
     * 开启此功能后，SDK 会在 onUserVoiceVolume() 中反馈对每一路声音音量大小值的评估。
     *
     * **Note:**
     * - 如需打开此功能，请在 startLocalAudio 之前调用才可以生效。
     *
     * @param {Number} interval - 设置 onUserVoiceVolume 回调的触发间隔，单位为ms，最小间隔为100ms，如果小于等于0则会关闭回调，建议设置为300ms
     * @memberof TrtcCloud
     * @example
     * this.trtcCloud.enableAudioVolumeEvaluation(300);
     */
  }, {
    key: "enableAudioVolumeEvaluation",
    value: function enableAudioVolumeEvaluation(interval) {
      return _TrtcCloudImpl.default._getInstance().enableAudioVolumeEvaluation(interval);
    }
    /////////////////////////////////////////////////////////////////////////////////
    //
    //                      屏幕分享
    //
    /////////////////////////////////////////////////////////////////////////////////
    /**
     * 设置屏幕分享（即辅路）的视频编码参数
     *
     * 该接口可以设定远端用户所看到的屏幕分享（即辅路）的画面质量，同时也能决定云端录制出的视频文件中屏幕分享的画面质量。 请注意如下两个接口的差异：
     *  - setVideoEncoderParam 用于设置主路画面（TRTCVideoStreamTypeBig，一般用于摄像头）的视频编码参数。
     *  - setSubStreamEncoderParam 用于设置辅路画面（TRTCVideoStreamTypeSub，一般用于屏幕分享）的视频编码参数。
     *
     * **Note:**
     *  - 即使您使用主路传输屏幕分享（在调用 startScreenCapture 时设置 type=TRTCVideoStreamTypeBig），依然要使用 setSubStreamEncoderParam 设定屏幕分享的编码参数，而不要使用 setVideoEncoderParam
     * @param {TRTCVideoEncParam} param	辅流编码参数，详情请参考 TRTCVideoEncParam。
     * @memberof TrtcCloud
     * @example
     * const params = {
     *   videoResolution: TRTCVideoResolution.TRTCVideoResolution_640_360,
     *   videoResolutionMode: TRTCVideoResolutionMode.TRTCVideoResolutionModePortrait,
     *   videoFps: 15,
     *   videoBitrate: 900,
     *   minVideoBitrate: 200,
     *   enableAdjustRes: false,
     * };
     * this.trtcCloud.setSubStreamEncoderParam(params);
     */
  }, {
    key: "setSubStreamEncoderParam",
    value: function setSubStreamEncoderParam(param) {
      return _TrtcCloudImpl.default._getInstance().setSubStreamEncoderParam(param);
    }
    /**
     * 启动屏幕分享
     *
     * **Note:**
     *  - 一个用户同时最多只能上传一条主路（TRTCVideoStreamTypeBig）画面和一条辅路（TRTCVideoStreamTypeSub）画面，
     * 默认情况下，屏幕分享使用辅路画面，如果使用主路画面，建议您提前停止摄像头采集（stopLocalPreview）避免相互冲突。
     *  - **仅支持 iOS 13.0 及以上系统，进行应用内的屏幕分享**
     *
     * @param {TRTCVideoStreamType} streamType 屏幕分享使用的线路，可以设置为主路（TRTCVideoStreamTypeBig）或者辅路（TRTCVideoStreamTypeSub），推荐使用
     * @param {TRTCVideoEncParam} encParams 屏幕分享的画面编码参数，可以设置为 null，表示让 SDK 选择最佳的编码参数（分辨率、码率等）。即使在调用 startScreenCapture 时设置 type=TRTCVideoStreamTypeBig，依然可以使用此接口更新屏幕分享的编码参数。
     * @memberof TrtcCloud
     * @example
     * import { TRTCVideoResolution, TRTCVideoResolutionMode, TRTCVideoStreamType} from '@/TrtcCloud/lib/TrtcDefines';
     * const encParams = {
     *   videoResolution: TRTCVideoResolution.TRTCVideoResolution_640_360,
     *   videoResolutionMode: TRTCVideoResolutionMode.TRTCVideoResolutionModePortrait,
     *   videoFps: 15,
     *   videoBitrate: 900,
     *   minVideoBitrate: 200,
     *   enableAdjustRes: false,
     * };
     * this.trtcCloud.startScreenCapture(TRTCVideoStreamType.TRTCVideoStreamTypeSub, encParams);
     */
  }, {
    key: "startScreenCapture",
    value: function startScreenCapture() {
      var streamType = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : _TrtcDefines.TRTCVideoStreamType.TRTCVideoStreamTypeSub;
      var encParams = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      return _TrtcCloudImpl.default._getInstance().startScreenCapture(streamType, encParams);
    }
    /**
     * 停止屏幕分享
     * @memberof TrtcCloud
     * @example
     * this.trtcCloud.stopScreenCapture();
     */
  }, {
    key: "stopScreenCapture",
    value: function stopScreenCapture() {
      return _TrtcCloudImpl.default._getInstance().stopScreenCapture();
    }
    /**
     * 暂停屏幕分享
     * @memberof TrtcCloud
     * @example
     * this.trtcCloud.pauseScreenCapture();
     */
  }, {
    key: "pauseScreenCapture",
    value: function pauseScreenCapture() {
      return _TrtcCloudImpl.default._getInstance().pauseScreenCapture();
    }
    /**
     * 恢复屏幕分享
     * @memberof TrtcCloud
     * @example
     * this.trtcCloud.resumeScreenCapture();
     */
  }, {
    key: "resumeScreenCapture",
    value: function resumeScreenCapture() {
      return _TrtcCloudImpl.default._getInstance().resumeScreenCapture();
    }
    /////////////////////////////////////////////////////////////////////////////////
    //
    //                      美颜 + 水印
    //
    /////////////////////////////////////////////////////////////////////////////////
    /**
     * 设置美颜（磨皮）算法
     * TRTC 内置多种不同的磨皮算法，您可以选择最适合您产品定位的方案
     *
     * **Note:**
     * - 设置美颜前，先调用 `setBeautyLevel` 设置美颜级别。否则美颜级别为 0 表示关闭美颜
     *
     * @param {TRTCBeautyStyle} beautyStyle 美颜风格，TRTCBeautyStyleSmooth：光滑；TRTCBeautyStyleNature：自然；TRTCBeautyStylePitu：优图
     * @memberof TrtcCloud
     * @example
     * import { TRTCBeautyStyle } from '@/TrtcCloud/lib/TrtcDefines';
     * const beautyLevel = 5; // 美颜级别，取值范围0 - 9； 0表示关闭，9表示效果最明显。
     * this.trtcCloud.setBeautyLevel(beautyLevel);
     * this.trtcCloud.setBeautyStyle(TRTCBeautyStyle.TRTCBeautyStyleSmooth);
     */
  }, {
    key: "setBeautyStyle",
    value: function setBeautyStyle(beautyStyle) {
      return _TrtcCloudImpl.default._getInstance().setBeautyStyle(beautyStyle);
    }
    /**
     * 设置美颜级别
     * @param {Number} beautyLevel	美颜级别，取值范围0 - 9； 0表示关闭，9表示效果最明显。
     *
     * @memberof TrtcCloud
     * @example
     * const beautyLevel = 5; // 美颜级别，取值范围0 - 9； 0表示关闭，9表示效果最明显。
     * this.trtcCloud.setBeautyLevel(beautyLevel);
     */
  }, {
    key: "setBeautyLevel",
    value: function setBeautyLevel(beautyLevel) {
      return _TrtcCloudImpl.default._getInstance().setBeautyLevel(beautyLevel);
    }
    /////////////////////////////////////////////////////////////////////////////////
    //
    //                      背景音效
    //
    /////////////////////////////////////////////////////////////////////////////////
    /**
     * 开始播放背景音乐
     * 每个音乐都需要您指定具体的 ID，您可以通过该 ID 对音乐的开始、停止、音量等进行设置。<br>
     * **Note:**
     * - 如果要多次播放同一首背景音乐，请不要每次播放都分配一个新的 ID，我们推荐使用相同的 ID。
     * - 若您希望同时播放多首不同的音乐，请为不同的音乐分配不同的 ID 进行播放。
     * - 如果使用同一个 ID 播放不同音乐，SDK 会先停止播放旧的音乐，再播放新的音乐。
     *
     * **Note:**<br>
     * 在 uni-app 中 path 如何获取。
     * - 使用 cdn 地址，例如：`path = https://web.sdk.qcloud.com/component/TUIKit/assets/uni-app/calling-bell-1.mp3;`
     * - 使用本地绝对路径。
     *     1. 通过 [uni.saveFile](https://zh.uniapp.dcloud.io/api/file/file.html#savefile) 获取保存后的相对路径（建议这种路径）。
     *     2. 将上一步的相对路径转成绝对路径，[plus.io.convertLocalFileSystemURL](https://www.html5plus.org/doc/zh_cn/io.html#plus.io.convertLocalFileSystemURL)。
     *
     * @param {AudioMusicParam} musicParam 音乐参数
     * @param {Number} musicParam.id 音乐 ID
     * @param {String} musicParam.path 音效文件的完整路径或 URL 地址。支持的音频格式包括 MP3、AAC、M4A、WAV
     * @param {Number} musicParam.loopCount 音乐循环播放的次数。取值范围为0 - 任意正整数，默认值：0。0表示播放音乐一次；1表示播放音乐两次；以此类推
     * @param {Boolean} musicParam.publish 是否将音乐传到远端。true：音乐在本地播放的同时，远端用户也能听到该音乐；false：主播只能在本地听到该音乐，远端观众听不到。默认值：false。
     * @param {Boolean} musicParam.isShortFile 播放的是否为短音乐文件。true：需要重复播放的短音乐文件；false：正常的音乐文件。默认值：false
     * @param {Number} musicParam.startTimeMS 音乐开始播放时间点，单位: 毫秒。
     * @param {Number} musicParam.endTimeMS 音乐结束播放时间点，单位: 毫秒，0 表示播放至文件结尾。
     * @memberof TrtcCloud
     * @example
     * import { AudioMusicParam } from '@/TrtcCloud/lib/TrtcDefines';
     * const musicParam = {
     *  id: 1,
     *  path: '',
     *  loopCount: 1,
     *  publish: true,
     *  isShortFile: false,
     *  startTimeMS: 0,
     *  endTimeMS: 0,
     * };
     * this.trtcCloud.startPlayMusic(musicParam);
     */
  }, {
    key: "startPlayMusic",
    value: function startPlayMusic(musicParam) {
      return _TrtcCloudImpl.default._getInstance().startPlayMusic(musicParam);
    }
    /**
     * 停止播放背景音乐
     * @param {Number} id	音乐 ID
     *
     * @memberof TrtcCloud
     * @example
     * const musicId = 5;
     * this.trtcCloud.stopPlayMusic(musicId);
     */
  }, {
    key: "stopPlayMusic",
    value: function stopPlayMusic(id) {
      return _TrtcCloudImpl.default._getInstance().stopPlayMusic(id);
    }
    /**
     * 暂停播放背景音乐
     * @param {Number} id	音乐 ID
     * @memberof TrtcCloud
     * @example
     * const musicId = 5;
     * this.trtcCloud.pausePlayMusic(musicId);
     */
  }, {
    key: "pausePlayMusic",
    value: function pausePlayMusic(id) {
      return _TrtcCloudImpl.default._getInstance().pausePlayMusic(id);
    }
    /**
     * 恢复播放背景音乐
     * @param {Number} id	音乐 ID
     * @memberof TrtcCloud
     * @example
     * const musicId = 5;
     * this.trtcCloud.resumePlayMusic(musicId);
     */
  }, {
    key: "resumePlayMusic",
    value: function resumePlayMusic(id) {
      return _TrtcCloudImpl.default._getInstance().resumePlayMusic(id);
    }
    /////////////////////////////////////////////////////////////////////////////////
    //
    //                       设置 TRTCCallback 回调
    //
    /////////////////////////////////////////////////////////////////////////////////
    /**
     * 设置 TrtcCloud 回调
     *
     * @example
     * // 创建/使用/销毁 TrtcCloud 对象的示例代码：
     * import TrtcCloud from '@/TrtcCloud/lib/index';
     * this.trtcCloud = new TrtcCloud();
     *
     * // 添加事件监听的方法，事件关键字详见下方”通用事件回调“
     * this.trtcCloud.on('onEnterRoom', (result) => {
     *   if (result > 0) {
     *     console.log(`enter room success, spend ${result}ms`);
     *   } else {
     *     console.log(`enter room failed, error code = ${result}`);
     *   }
     * });
     *
     * @namespace TRTCCallback
     */
    /////////////////////////////////////////////////////////////////////////////////
    //
    //                      （一）事件回调
    //
    /////////////////////////////////////////////////////////////////////////////////
    /**
     * 错误回调，表示 SDK 不可恢复的错误，一定要监听并分情况给用户适当的界面提示<br>
     * @event TRTCCallback#onError
     * @param {Number} code 错误码，[详见](https://cloud.tencent.com/document/product/647/38308#.E9.94.99.E8.AF.AF.E7.A0.81.E8.A1.A8)
     * @param {String} message 错误信息
     * @param {Object} extraInfo 扩展信息字段，个别错误码可能会带额外的信息帮助定位问题
     */
  }, {
    key: "onError",
    value: function onError(code, message, extraInfo) {}
    /**
     * 警告回调，用于告知您一些非严重性问题，例如出现卡顿或者可恢复的解码失败<br>
     * @event TRTCCallback#onWarning
     * @param {Number} code 警告码，[详见](https://cloud.tencent.com/document/product/647/38308#.E8.AD.A6.E5.91.8A.E7.A0.81.E8.A1.A8)
     * @param {String} message 警告信息
     * @param {Object} extraInfo 扩展信息字段，个别警告码可能会带额外的信息帮助定位问题
     */
  }, {
    key: "onWarning",
    value: function onWarning(code, message, extraInfo) {}
    /**
     * 进房后的回调<br>
     * 调用 `enterRoom()` 接口执行进房操作后，会收到 `onEnterRoom(result)` 回调<br>
     * 如果加入成功，result 会是一个正数（result > 0），代表加入房间的时间消耗，单位是毫秒（ms）。<br>
     * 如果加入失败，result 会是一个负数（result < 0），代表进房失败的错误码。
     *
     * @event TRTCCallback#onEnterRoom
     * @param {Number} result 进房耗时
     */
  }, {
    key: "onEnterRoom",
    value: function onEnterRoom(result) {}
    /**
     * 离开房间的事件回调<br>
     * 调用 `exitRoom()` 接口会执行退出房间的相关逻辑，例如释放音视频设备资源和编解码器资源等。待资源释放完毕，会通过 `onExitRoom()` 回调通知到您<br>
     *
     * **Note:**
     * - 如果您要再次调用 `enterRoom()` 或者切换到其他的音视频 SDK，请等待 `onExitRoom()` 回调到来之后再执行相关操作。 否则可能会遇到音频设备被占用等各种异常问题
     *
     * @event TRTCCallback#onExitRoom
     * @param {Number} reason 离开房间原因，0：主动调用 exitRoom 退房；1：被服务器踢出当前房间；2：当前房间整个被解散
     */
  }, {
    key: "onExitRoom",
    value: function onExitRoom(reason) {}
    /**
     * 切换角色的事件回调<br>
     * 调用 TRTCCloud 中的 switchRole() 接口会切换主播和观众的角色，该操作会伴随一个线路切换的过程， 待 SDK 切换完成后，会抛出 onSwitchRole() 事件回调
     *
     * @event TRTCCallback#onSwitchRole
     * @param {Number} code 错误码，[详见](https://cloud.tencent.com/document/product/647/38308#.E8.AD.A6.E5.91.8A.E7.A0.81.E8.A1.A8)
     * @param {String} message 错误信息
     */
  }, {
    key: "onSwitchRole",
    value: function onSwitchRole(code, message) {}
    /**
     * 开始渲染本地或远程用户的首帧画面<br>
     * 如果 userId 为 null，代表开始渲染本地采集的摄像头画面，需要您先调用 `startLocalPreview` 触发。 如果 userId 不为 null，代表开始渲染远程用户的首帧画面，需要您先调用 `startRemoteView` 触发<br>
     * 只有当您调用 `startLocalPreview()、startRemoteView() 或 startRemoteSubStreamView()` 之后，才会触发该回调
     *
     * @event TRTCCallback#onFirstVideoFrame
     * @param {String} userId 本地或远程用户 ID，如果 userId === null 代表本地，userId !== null 代表远程
     * @param {TRTCVideoStreamType} streamType 视频流类型：摄像头或屏幕分享
     * @param {Number} width 画面宽度
     * @param {Number} height 画面高度
     */
  }, {
    key: "onFirstVideoFrame",
    value: function onFirstVideoFrame(userId, streamType, width, height) {}
    /**
     * 开始播放远程用户的首帧音频（本地声音暂不通知）<br>
     * 如果 userId 为 null，代表开始渲染本地采集的摄像头画面，需要您先调用 `startLocalPreview` 触发。 如果 userId 不为 null，代表开始渲染远程用户的首帧画面，需要您先调用 `startRemoteView` 触发<br>
     * 只有当您调用 `startLocalPreview()、startRemoteView() 或 startRemoteSubStreamView()` 之后，才会触发该回调
     *
     * @event TRTCCallback#onFirstAudioFrame
     * @param {String} userId 远程用户 ID
     */
  }, {
    key: "onFirstAudioFrame",
    value: function onFirstAudioFrame(userId) {}
    /**
     * 截图完成时回调<br>
     * @event TRTCCallback#onSnapshotComplete
     * @param {String} base64Data 截图对应的 base64 数据
     * @param {String} message 错误信息
     */
  }, {
    key: "onSnapshotComplete",
    value: function onSnapshotComplete(base64Data, message) {}
    /**
     * 麦克风准备就绪
     */
  }, {
    key: "onMicDidReady",
    value: function onMicDidReady() {}
    /**
     * 摄像头准备就绪
     */
  }, {
    key: "onCameraDidReady",
    value: function onCameraDidReady() {}
    /**
     * 网络质量：该回调每2秒触发一次，统计当前网络的上行和下行质量<br>
     * userId 为本地用户 ID 代表自己当前的视频质量
     *
     * @param {String} localQuality 上行网络质量
     * @param {String} remoteQuality 下行网络质量
     */
  }, {
    key: "onNetworkQuality",
    value: function onNetworkQuality(localQuality, remoteList) {}
    /**
     * 有用户加入当前房间<br>
     * 出于性能方面的考虑，在两种不同的应用场景下，该通知的行为会有差别：<br>
     * 通话场景（TRTCAppScene.TRTCAppSceneVideoCall 和 TRTCAppScene.TRTCAppSceneAudioCall）：该场景下用户没有角色的区别，任何用户进入房间都会触发该通知。<br>
     * 直播场景（TRTCAppScene.TRTCAppSceneLIVE 和 TRTCAppScene.TRTCAppSceneVoiceChatRoom ）：该场景不限制观众的数量，如果任何用户进出都抛出回调会引起很大的性能损耗，所以该场景下只有主播进入房间时才会触发该通知，观众进入房间不会触发该通知
     *
     * @event TRTCCallback#onRemoteUserEnterRoom
     * @param {String} userId 用户标识 ID
     */
  }, {
    key: "onRemoteUserEnterRoom",
    value: function onRemoteUserEnterRoom(userId) {}
    /**
     * 有用户离开当前房间<br>
     * 与 onRemoteUserEnterRoom 相对应，在两种不同的应用场景下，该通知的行为会有差别：<br>
     * 通话场景（TRTCAppScene.TRTCAppSceneVideoCall 和 TRTCAppScene.TRTCAppSceneAudioCall）：该场景下用户没有角色的区别，任何用户进入房间都会触发该通知。<br>
     * 直播场景（TRTCAppScene.TRTCAppSceneLIVE 和 TRTCAppScene.TRTCAppSceneVoiceChatRoom ）：该场景不限制观众的数量，如果任何用户进出都抛出回调会引起很大的性能损耗，所以该场景下只有主播进入房间时才会触发该通知，观众进入房间不会触发该通知
     *
     * @event TRTCCallback#onRemoteUserLeaveRoom
     * @param {String} userId 用户标识 ID
     * @param {Number} reason 离开原因，0 表示用户主动退出房间，1 表示用户超时退出，2 表示被踢出房间
     */
  }, {
    key: "onRemoteUserLeaveRoom",
    value: function onRemoteUserLeaveRoom(userId, reason) {}
    /**
     * 首帧本地音频数据已经被送出<br>
     * 在 `enterRoom()` 并 `startLocalAudio()` 成功后开始麦克风采集，并将采集到的声音进行编码。 当 SDK 成功向云端送出第一帧音频数据后，会抛出这个回调事件
     *
     * @event TRTCCallback#onSendFirstLocalAudioFrame
     */
  }, {
    key: "onSendFirstLocalAudioFrame",
    value: function onSendFirstLocalAudioFrame() {}
    /**
     * 首帧本地视频数据已经被送出<br>
     * SDK 会在 `enterRoom()` 并 `startLocalPreview()` 成功后开始摄像头采集，并将采集到的画面进行编码。 当 SDK 成功向云端送出第一帧视频数据后，会抛出这个回调事件
     *
     * @event TRTCCallback#onSendFirstLocalVideoFrame
     * @param {TRTCVideoStreamType} streamType 视频流类型，大画面、小画面或辅流画面（屏幕分享）
     */
  }, {
    key: "onSendFirstLocalVideoFrame",
    value: function onSendFirstLocalVideoFrame(streamType) {}
    /**
     * 技术指标统计回调<br>
     * 如果您是熟悉音视频领域相关术语，可以通过这个回调获取 SDK 的所有技术指标。 如果您是首次开发音视频相关项目，可以只关注 `onNetworkQuality` 回调
     *
     * **Note:**
     * - 每 2 秒回调一次
     *
     * @param {Object} statics 状态数据
     */
  }, {
    key: "onStatistics",
    value: function onStatistics(statics) {}
    /**
     * 远端用户是否存在可播放的音频数据<br>
     * @event TRTCCallback#onUserAudioAvailable
     * @param {String} userId 用户标识 ID
     * @param {Boolean} available 声音是否开启
     */
  }, {
    key: "onUserAudioAvailable",
    value: function onUserAudioAvailable(userId, available) {}
    /**
     * 远端用户是否存在可播放的主路画面（一般用于摄像头）<br>
     * 当您收到 `onUserVideoAvailable(userId, true)` 通知时，表示该路画面已经有可用的视频数据帧到达。 此时，您需要调用 `startRemoteView(userId)` 接口加载该用户的远程画面。 然后，您会收到名为 onFirstVideoFrame(userid) 的首帧画面渲染回调。<br>
     * 当您收到 `onUserVideoAvailable(userId, false)` 通知时，表示该路远程画面已经被关闭，可能由于该用户调用了 `muteLocalVideo()` 或 `stopLocalPreview()`。
     *
     * @event TRTCCallback#onUserVideoAvailable
     * @param {String} userId 用户标识 ID
     * @param {Boolean} available 画面是否开启
     */
  }, {
    key: "onUserVideoAvailable",
    value: function onUserVideoAvailable(userId, available) {}
    /**
     * 用于提示音量大小的回调，包括每个 userId 的音量和远端总音量<br>
     * SDK 可以评估每一路音频的音量大小，并每隔一段时间抛出该事件回调，您可以根据音量大小在 UI 上做出相应的提示，比如“波形图”或“音量槽”。 要完成这个功能， 您需要先调用 enableAudioVolumeEvaluation 开启这个能力并设定事件抛出的时间间隔。 需要补充说明的是，无论当前房间中是否有人说话，SDK 都会按照您设定的时间间隔定时抛出此事件回调，只不过当没有人说话时，userVolumes 为空，totalVolume 为 0。
     *
     * **Note:**
     * - userVolumes 为一个数组，对于数组中的每一个元素，当 userId 为空时表示本地麦克风采集的音量大小，当 userId 不为空时代表远端用户的音量大小
     *
     * @event TRTCCallback#onUserVoiceVolume
     * @param {Array} userVolumes 是一个数组，用于承载所有正在说话的用户的音量大小，取值范围 0 - 100
     * @param {Number} totalVolume 所有远端用户的总音量大小, 取值范围 0 - 100
     */
  }, {
    key: "onUserVoiceVolume",
    value: function onUserVoiceVolume(userVolumes, totalVolume) {}
    /**
     * 屏幕分享开启的事件回调<br>
     * 当您通过 startScreenCapture 等相关接口启动屏幕分享时，SDK 便会抛出此事件回调
     * @event TRTCCallback#onScreenCaptureStarted
     */
  }, {
    key: "onScreenCaptureStarted",
    value: function onScreenCaptureStarted() {}
    /**
     * 屏幕分享停止的事件回调<br>
     * 当您通过 stopScreenCapture 停止屏幕分享时，SDK 便会抛出此事件回调
     * @event TRTCCallback#onScreenCaptureStopped
     * @param {Number} reason 停止原因，0：用户主动停止；1：屏幕窗口关闭导致停止；2：表示屏幕分享的显示屏状态变更（如接口被拔出、投影模式变更等）
     */
  }, {
    key: "onScreenCaptureStopped",
    value: function onScreenCaptureStopped(reason) {}
    /**
     * 屏幕分享停止的事件回调<br>
     * 当您通过 pauseScreenCapture 停止屏幕分享时，SDK 便会抛出此事件回调
     * @event TRTCCallback#onScreenCapturePaused
     * @param {Number} reason 停止原因，0：用户主动停止；1：屏幕窗口关闭导致停止；2：表示屏幕分享的显示屏状态变更（如接口被拔出、投影模式变更等）
     */
  }, {
    key: "onScreenCapturePaused",
    value: function onScreenCapturePaused(reason) {}
    /**
     * 屏幕分享恢复的事件回调<br>
     * 当您通过 resumeScreenCapture 恢复屏幕分享时，SDK 便会抛出此事件回调
     * @event TRTCCallback#onScreenCaptureResumed
     */
  }, {
    key: "onScreenCaptureResumed",
    value: function onScreenCaptureResumed() {}
    /**
     * 某远端用户发布/取消了辅路视频画面<br>
     * “辅路画面”一般被用于承载屏幕分享的画面。当您收到 onUserSubStreamAvailable(userId, true) 通知时，表示该路画面已经有可播放的视频帧到达。 此时，您需要调用 startRemoteView 接口订阅该用户的远程画面，订阅成功后，您会继续收到该用户的首帧画面渲染回调 onFirstVideoFrame(userId)
     *
     * **Note:**
     * - 拉取 Web 端（用 [WebRTC](https://web.sdk.qcloud.com/trtc/webrtc/doc/zh-cn/index.html) 实现屏幕分享）的屏幕分享，收不到 onUserSubStreamAvailable 事件。因为 [WebRTC](https://web.sdk.qcloud.com/trtc/webrtc/doc/zh-cn/index.html) 推的屏幕分享也是主流
     * @param {String} userId 用户 ID
     * @param {Boolean} available 是否可用，true 表示辅流可用
     * @event TRTCCallback#onUserSubStreamAvailable
     */
  }, {
    key: "onUserSubStreamAvailable",
    value: function onUserSubStreamAvailable(userId, available) {}
    /**
     * 用户视频大小发生改变回调。<br>
     * 当您收到 onUserVideoSizeChanged(userId, streamtype, newWidth, newHeight) 通知时，表示该路画面大小发生了调整，调整的原因可能是该用户调用了 setVideoEncoderParam 或者 setSubStreamEncoderParam 重新设置了画面尺寸。
     * @param {String} userId 用户 ID
     * @param {TRTCVideoStreamType} streamType 视频流类型，仅支持 TRTCVideoStreamTypeBig 和 TRTCVideoStreamTypeSub
     * @param {Number} newWidth 视频流的宽度（像素）
     * @param {Number} newHeight 视频流的高度（像素）
     * @event TRTCCallback#onUserVideoSizeChanged
     */
  }, {
    key: "onUserVideoSizeChanged",
    value: function onUserVideoSizeChanged(userId, streamType, newWidth, newHeight) {}
    /**
     * 背景音乐开始播放
     * @param {Number} id 播放的 id
     * @param {Number} errCode 播放的状态码
     * @event TRTCCallback#onStart
     */
  }, {
    key: "onStart",
    value: function onStart(id, errCode) {}
    /**
     * 背景音乐的播放进度
     * @param {Number} id 播放的 id
     * @param {Number} curPtsMS 当前播放的位置
     * @param {Number} durationMS 当前音频总时长
     * @event TRTCCallback#onPlayProgress
     */
  }, {
    key: "onPlayProgress",
    value: function onPlayProgress(id, curPtsMS, durationMS) {}
    /**
     * 背景音乐已经播放完毕
     * @param {Number} id 播放的 id
     * @param {Number} errCode 播放结束的状态码
     * @event TRTCCallback#onComplete
     */
  }, {
    key: "onComplete",
    value: function onComplete(id, errCode) {}
  }], [{
    key: "createInstance",
    value:
    /**
     * 创建 TrtcCloud 单例
     *
     * @static
     * @memberof TrtcCloud
     * @example
     * TrtcCloud.createInstance();
     */
    function createInstance() {
      console.log('----------------------------------------------------------------');
      console.log("                        SDK ".concat(version, "                    "));
      console.log('----------------------------------------------------------------');
      return _TrtcCloudImpl.default._createInstance();
    }
    /**
     * 销毁 TrtcCloud 单例
     *
     * @static
     * @memberof TrtcCloud
     * @example
     * TrtcCloud.destroyInstance();
     */
  }, {
    key: "destroyInstance",
    value: function destroyInstance() {
      return _TrtcCloudImpl.default._destroyInstance();
    }
  }]);
  return TrtcCloud;
}();
exports.default = TrtcCloud;

/***/ }),
/* 51 */
/*!***************************************************************!*\
  !*** E:/公司项目/e考监控/Api-Example/TrtcCloud/lib/TrtcCloudImpl.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(uni) {

var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ 4);
var _typeof3 = __webpack_require__(/*! @babel/runtime/helpers/typeof */ 13);
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _regenerator = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/regenerator */ 52));
var _typeof2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/typeof */ 13));
var _classCallCheck2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ 23));
var _createClass2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/createClass */ 24));
var _constants = __webpack_require__(/*! ./constants */ 54);
var _TrtcDefines = __webpack_require__(/*! ./TrtcDefines */ 55);
var _TrtcCode = _interopRequireWildcard(__webpack_require__(/*! ./TrtcCode */ 56));
function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }
function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof3(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }
var __awaiter = void 0 && (void 0).__awaiter || function (thisArg, _arguments, P, generator) {
  function adopt(value) {
    return value instanceof P ? value : new P(function (resolve) {
      resolve(value);
    });
  }
  return new (P || (P = Promise))(function (resolve, reject) {
    function fulfilled(value) {
      try {
        step(generator.next(value));
      } catch (e) {
        reject(e);
      }
    }
    function rejected(value) {
      try {
        step(generator["throw"](value));
      } catch (e) {
        reject(e);
      }
    }
    function step(result) {
      result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
    }
    step((generator = generator.apply(thisArg, _arguments || [])).next());
  });
};
var TrtcNativeTrtcCloudModule = uni.requireNativePlugin('TRTCCloudUniPlugin-TRTCCloudImpl');
var TXAudioEffectManagerModule = uni.requireNativePlugin('TRTCCloudUniPlugin-TRTCCloudImpl-TXAudioEffectManagerModule');
var TrtcEvent = uni.requireNativePlugin('globalEvent');
var trtcCloud = null; // trtcCloud 单例
var TrtcCloudImpl = /*#__PURE__*/function () {
  function TrtcCloudImpl() {
    (0, _classCallCheck2.default)(this, TrtcCloudImpl);
    this.listenersMap_ = new Map();
  }
  (0, _createClass2.default)(TrtcCloudImpl, [{
    key: "on",
    value:
    // 截图保存
    // async saveImage_(base64Data) {
    //   return new Promise((resolve, reject) => {
    //     let bitmap = new plus.nativeObj.Bitmap();
    //     bitmap.loadBase64Data(base64Data, () => {
    //       const url = "_doc/" + new Date().getTime() + ".png";  // url为时间戳命名方式
    //       console.log('saveHeadImgFile', url);
    //       bitmap.save(url, { overwrite: true }, (i) => {
    //         uni.saveImageToPhotosAlbum({
    //           filePath: url,
    //           success: function() {
    //             uni.showToast({
    //               title: '图片保存成功',
    //               icon: 'none'
    //             })
    //             bitmap.clear();
    //             resolve({ code: 0, message: '图片保存成功' });
    //           }
    //         });
    //       }, (e) => {
    //         uni.showToast({
    //           title: '图片保存失败, 请重新截图',
    //           icon: 'none'
    //         })
    //         bitmap.clear();
    //         resolve({ code: -1, message: '图片保存失败, 请重新截图' });
    //       });
    //     });
    //   });
    // }
    function on(event, callback) {
      var _this = this;
      if ((0, _typeof2.default)(event) !== _constants.NAME.STRING || (0, _typeof2.default)(callback) !== _constants.NAME.FUNCTION) {
        throw new _TrtcCode.default({
          code: _TrtcCode.TXLiteJSError.INVALID_PARAMETER,
          message: "".concat(_constants.NAME.LOG_PREFIX, " please check the on method parameter types. event type is a ").concat((0, _typeof2.default)(event), "; callback type is a ").concat((0, _typeof2.default)(callback))
        });
      }
      var nativeListener = function nativeListener(res) {
        return __awaiter(_this, void 0, void 0, /*#__PURE__*/_regenerator.default.mark(function _callee() {
          var _res$data, data, code, message, extraInfo, result, reason, userId, streamType, width, height, _userId, localQuality, remoteQuality, _userId2, _userId3, _reason, _streamType, statics, _userId4, available, _userId5, _available, userVolumes, totalVolume, _userId6, _available2;
          return _regenerator.default.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  _res$data = res.data, data = _res$data === void 0 ? [] : _res$data;
                  code = data[0];
                  message = data[1] || '';
                  extraInfo = data[2] || {};
                  _context.t0 = event;
                  _context.next = _context.t0 === 'onEnterRoom' ? 7 : _context.t0 === 'onExitRoom' ? 10 : _context.t0 === 'onFirstVideoFrame' ? 13 : _context.t0 === 'onFirstAudioFrame' ? 19 : _context.t0 === 'onMicDidReady' ? 22 : _context.t0 === 'onCameraDidReady' ? 24 : _context.t0 === 'onNetworkQuality' ? 26 : _context.t0 === 'onRemoteUserEnterRoom' ? 30 : _context.t0 === 'onRemoteUserLeaveRoom' ? 33 : _context.t0 === 'onSendFirstLocalAudioFrame' ? 37 : _context.t0 === 'onSendFirstLocalVideoFrame' ? 39 : _context.t0 === 'onStatistics' ? 42 : _context.t0 === 'onUserAudioAvailable' ? 45 : _context.t0 === 'onUserVideoAvailable' ? 49 : _context.t0 === 'onUserVoiceVolume' ? 53 : _context.t0 === 'onSwitchRole' ? 57 : _context.t0 === 'onScreenCaptureStarted' ? 59 : _context.t0 === 'onScreenCapturePaused' ? 61 : _context.t0 === 'onScreenCaptureResumed' ? 63 : _context.t0 === 'onScreenCaptureStopped' ? 65 : _context.t0 === 'onUserSubStreamAvailable' ? 67 : _context.t0 === 'onSnapshotComplete' ? 71 : _context.t0 === 'onUserVideoSizeChanged' ? 73 : _context.t0 === 'onStart' ? 75 : _context.t0 === 'onPlayProgress' ? 77 : _context.t0 === 'onComplete' ? 79 : _context.t0 === 'onError' ? 81 : 84;
                  break;
                case 7:
                  result = code;
                  callback(result);
                  return _context.abrupt("break", 85);
                case 10:
                  reason = code;
                  callback(reason);
                  return _context.abrupt("break", 85);
                case 13:
                  userId = code;
                  streamType = data[1] || 0;
                  width = data[2] || 0;
                  height = data[3] || 0;
                  callback({
                    userId: userId,
                    streamType: streamType,
                    width: width,
                    height: height
                  });
                  return _context.abrupt("break", 85);
                case 19:
                  _userId = code || '';
                  callback(_userId);
                  return _context.abrupt("break", 85);
                case 22:
                  callback();
                  return _context.abrupt("break", 85);
                case 24:
                  callback();
                  return _context.abrupt("break", 85);
                case 26:
                  localQuality = data[0];
                  remoteQuality = data[1];
                  callback({
                    localQuality: localQuality,
                    remoteQuality: remoteQuality
                  });
                  return _context.abrupt("break", 85);
                case 30:
                  _userId2 = code || '';
                  callback(_userId2);
                  return _context.abrupt("break", 85);
                case 33:
                  _userId3 = code || '';
                  _reason = message;
                  callback({
                    userId: _userId3,
                    reason: _reason
                  });
                  return _context.abrupt("break", 85);
                case 37:
                  callback();
                  return _context.abrupt("break", 85);
                case 39:
                  _streamType = code;
                  callback(_streamType);
                  return _context.abrupt("break", 85);
                case 42:
                  statics = data[0] || {};
                  callback(statics);
                  return _context.abrupt("break", 85);
                case 45:
                  _userId4 = code || '';
                  available = message;
                  callback({
                    userId: _userId4,
                    available: available
                  });
                  return _context.abrupt("break", 85);
                case 49:
                  _userId5 = code || '';
                  _available = message;
                  callback({
                    userId: _userId5,
                    available: _available
                  });
                  return _context.abrupt("break", 85);
                case 53:
                  userVolumes = data[0];
                  totalVolume = data[1];
                  callback({
                    userVolumes: userVolumes,
                    totalVolume: totalVolume
                  });
                  return _context.abrupt("break", 85);
                case 57:
                  callback({
                    code: code,
                    message: message
                  });
                  return _context.abrupt("break", 85);
                case 59:
                  callback({
                    code: code,
                    message: message
                  });
                  return _context.abrupt("break", 85);
                case 61:
                  callback({
                    code: code,
                    message: message
                  });
                  return _context.abrupt("break", 85);
                case 63:
                  callback({
                    code: code,
                    message: message
                  });
                  return _context.abrupt("break", 85);
                case 65:
                  callback({
                    code: code,
                    message: message
                  });
                  return _context.abrupt("break", 85);
                case 67:
                  _userId6 = code || '';
                  _available2 = message;
                  callback({
                    userId: _userId6,
                    available: _available2
                  });
                  return _context.abrupt("break", 85);
                case 71:
                  // base64 直接保存到本地图库
                  // const { code: snapShotCode, message: msg } = await this.saveImage_(code);
                  // callback({ snapShotCode, message: msg });
                  callback({
                    base64Data: code,
                    message: message
                  });
                  return _context.abrupt("break", 85);
                case 73:
                  callback(data);
                  return _context.abrupt("break", 85);
                case 75:
                  callback({
                    id: code,
                    errCode: message
                  });
                  return _context.abrupt("break", 85);
                case 77:
                  callback({
                    id: code,
                    curPtsMS: message,
                    durationMS: extraInfo
                  });
                  return _context.abrupt("break", 85);
                case 79:
                  callback({
                    id: code,
                    errCode: message
                  });
                  return _context.abrupt("break", 85);
                case 81:
                  console.error("onError: ".concat(code, ", ").concat(message, ", ").concat(extraInfo));
                  callback((0, _TrtcCode.generateError_)({
                    message: message
                  }, code, extraInfo));
                  return _context.abrupt("break", 85);
                case 84:
                  callback({
                    code: code,
                    message: message,
                    extraInfo: extraInfo
                  });
                case 85:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee);
        }));
      };
      this.listenersMap_.set(event, nativeListener); // 多次设置同一个事件时，后面的 callback 覆盖前面
      TrtcEvent.addEventListener(event, nativeListener);
    }
  }, {
    key: "off",
    value: function off(event) {
      if ((0, _typeof2.default)(event) !== _constants.NAME.STRING) {
        throw new _TrtcCode.default({
          code: _TrtcCode.TXLiteJSError.INVALID_PARAMETER,
          message: "".concat(_constants.NAME.LOG_PREFIX, " please check the off method parameter types. event type is a ").concat((0, _typeof2.default)(event), " not a ").concat(_constants.NAME.STRING)
        });
      }
      try {
        if (event === '*') {
          this.listenersMap_.forEach(function (value, key) {
            TrtcEvent.removeEventListener(key, value);
          });
          this.listenersMap_.clear();
        } else {
          TrtcEvent.removeEventListener(event, this.listenersMap_.get(event));
          this.listenersMap_.delete(event);
        }
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
  }, {
    key: "enterRoom",
    value: function enterRoom(params, scene) {
      if (scene !== _TrtcDefines.TRTCAppScene.TRTCAppSceneVideoCall && scene !== _TrtcDefines.TRTCAppScene.TRTCAppSceneLIVE && scene !== _TrtcDefines.TRTCAppScene.TRTCAppSceneAudioCall && scene !== _TrtcDefines.TRTCAppScene.TRTCAppSceneVoiceChatRoom) {
        throw new _TrtcCode.default({
          code: _TrtcCode.TXLiteJSError.INVALID_PARAMETER,
          message: "".concat(_constants.NAME.LOG_PREFIX, " please check the enterRoom method parameters. scene is not of TRTCAppScene")
        });
      }
      try {
        var enterRoomParams = Object.assign(Object.assign({}, params), {
          role: params.role || _TrtcDefines.TRTCRoleType.TRTCRoleAnchor,
          appScene: scene
        });
        TrtcNativeTrtcCloudModule.enterRoom(enterRoomParams);
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
  }, {
    key: "exitRoom",
    value: function exitRoom() {
      try {
        TrtcNativeTrtcCloudModule.exitRoom();
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
  }, {
    key: "switchRole",
    value: function switchRole(role) {
      if (role !== _TrtcDefines.TRTCRoleType.TRTCRoleAnchor && role !== _TrtcDefines.TRTCRoleType.TRTCRoleAudience) {
        throw new _TrtcCode.default({
          code: _TrtcCode.TXLiteJSError.INVALID_PARAMETER,
          message: "".concat(_constants.NAME.LOG_PREFIX, " please check the switchRole method parameter. role is not of TRTCRoleType")
        });
      }
      try {
        role && TrtcNativeTrtcCloudModule.switchRole(role);
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
  }, {
    key: "startLocalPreview",
    value: function startLocalPreview() {
      var isFrontCamera = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
      var viewId = arguments.length > 1 ? arguments[1] : undefined;
      if ((0, _typeof2.default)(isFrontCamera) !== _constants.NAME.BOOLEAN || !viewId || (0, _typeof2.default)(viewId) !== _constants.NAME.STRING) {
        throw new _TrtcCode.default({
          code: _TrtcCode.TXLiteJSError.INVALID_PARAMETER,
          message: "".concat(_constants.NAME.LOG_PREFIX, " please check the startLocalPreview method parameters")
        });
      }
      try {
        var param = {
          isFrontCamera: !!isFrontCamera
        };
        param = viewId ? Object.assign(Object.assign({}, param), {
          userId: viewId
        }) : param;
        TrtcNativeTrtcCloudModule.startLocalPreview(param);
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
  }, {
    key: "setVideoEncoderParam",
    value: function setVideoEncoderParam(param) {
      try {
        TrtcNativeTrtcCloudModule.setVideoEncoderParam(param);
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
  }, {
    key: "stopLocalPreview",
    value: function stopLocalPreview() {
      try {
        TrtcNativeTrtcCloudModule.stopLocalPreview();
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
  }, {
    key: "switchCamera",
    value: function switchCamera(isFrontCamera) {
      if ((0, _typeof2.default)(isFrontCamera) !== _constants.NAME.BOOLEAN) {
        throw new _TrtcCode.default({
          code: _TrtcCode.TXLiteJSError.INVALID_PARAMETER,
          message: "".concat(_constants.NAME.LOG_PREFIX, " please check the switchCamera method parameter")
        });
      }
      try {
        TrtcNativeTrtcCloudModule.switchCamera(isFrontCamera);
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
  }, {
    key: "setLocalRenderParams",
    value: function setLocalRenderParams(params) {
      try {
        var _params$rotation = params.rotation,
          rotation = _params$rotation === void 0 ? _TrtcDefines.TRTCVideoRotation.TRTCVideoRotation_0 : _params$rotation,
          _params$fillMode = params.fillMode,
          fillMode = _params$fillMode === void 0 ? _TrtcDefines.TRTCVideoFillMode.TRTCVideoFillMode_Fill : _params$fillMode,
          _params$mirrorType = params.mirrorType,
          mirrorType = _params$mirrorType === void 0 ? _TrtcDefines.TRTCVideoMirrorType.TRTCVideoMirrorType_Auto : _params$mirrorType;
        TrtcNativeTrtcCloudModule.setLocalRenderParams({
          rotation: rotation,
          fillMode: fillMode,
          mirrorType: mirrorType
        });
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
  }, {
    key: "muteLocalVideo",
    value: function muteLocalVideo(streamType, mute) {
      if (streamType !== _TrtcDefines.TRTCVideoStreamType.TRTCVideoStreamTypeBig && streamType !== _TrtcDefines.TRTCVideoStreamType.TRTCVideoStreamTypeSub || (0, _typeof2.default)(mute) !== _constants.NAME.BOOLEAN) {
        throw new _TrtcCode.default({
          code: _TrtcCode.TXLiteJSError.INVALID_PARAMETER,
          message: "".concat(_constants.NAME.LOG_PREFIX, " please check the muteLocalVideo method parameters")
        });
      }
      try {
        TrtcNativeTrtcCloudModule.muteLocalVideo({
          streamType: streamType,
          mute: !!mute
        });
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
  }, {
    key: "startRemoteView",
    value: function startRemoteView(userId, streamType, viewId) {
      if (!userId || streamType !== _TrtcDefines.TRTCVideoStreamType.TRTCVideoStreamTypeBig && streamType !== _TrtcDefines.TRTCVideoStreamType.TRTCVideoStreamTypeSmall && streamType !== _TrtcDefines.TRTCVideoStreamType.TRTCVideoStreamTypeSub || !viewId) {
        throw new _TrtcCode.default({
          code: _TrtcCode.TXLiteJSError.INVALID_PARAMETER,
          message: "".concat(_constants.NAME.LOG_PREFIX, " please check the startRemoteView method parameters")
        });
      }
      try {
        TrtcNativeTrtcCloudModule.startRemoteView({
          userId: userId,
          streamType: streamType,
          viewId: viewId
        });
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
  }, {
    key: "stopRemoteView",
    value: function stopRemoteView(userId, streamType) {
      if (!userId || streamType !== _TrtcDefines.TRTCVideoStreamType.TRTCVideoStreamTypeBig && streamType !== _TrtcDefines.TRTCVideoStreamType.TRTCVideoStreamTypeSmall && streamType !== _TrtcDefines.TRTCVideoStreamType.TRTCVideoStreamTypeSub) {
        throw new _TrtcCode.default({
          code: _TrtcCode.TXLiteJSError.INVALID_PARAMETER,
          message: "".concat(_constants.NAME.LOG_PREFIX, " please check the stopRemoteView method parameters")
        });
      }
      try {
        TrtcNativeTrtcCloudModule.stopRemoteView({
          userId: userId,
          streamType: streamType
        });
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
    // 远端渲染设置
  }, {
    key: "setRemoteRenderParams",
    value: function setRemoteRenderParams(userId, streamType, params) {
      try {
        if (!userId || streamType !== _TrtcDefines.TRTCVideoStreamType.TRTCVideoStreamTypeBig && streamType !== _TrtcDefines.TRTCVideoStreamType.TRTCVideoStreamTypeSub) {
          throw new _TrtcCode.default({
            code: _TrtcCode.TXLiteJSError.INVALID_PARAMETER,
            message: "".concat(_constants.NAME.LOG_PREFIX, " please check the snapshotVideo method parameters")
          });
        }
        var _params$rotation2 = params.rotation,
          rotation = _params$rotation2 === void 0 ? _TrtcDefines.TRTCVideoRotation.TRTCVideoRotation_0 : _params$rotation2,
          _params$fillMode2 = params.fillMode,
          fillMode = _params$fillMode2 === void 0 ? _TrtcDefines.TRTCVideoFillMode.TRTCVideoFillMode_Fill : _params$fillMode2,
          _params$mirrorType2 = params.mirrorType,
          mirrorType = _params$mirrorType2 === void 0 ? _TrtcDefines.TRTCVideoMirrorType.TRTCVideoMirrorType_Auto : _params$mirrorType2;
        TrtcNativeTrtcCloudModule.setRemoteRenderParams({
          userId: userId,
          streamType: streamType,
          rotation: rotation,
          fillMode: fillMode,
          mirrorType: mirrorType
        });
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
    // 截图
  }, {
    key: "snapshotVideo",
    value: function snapshotVideo(userId, streamType) {
      if (streamType !== _TrtcDefines.TRTCVideoStreamType.TRTCVideoStreamTypeBig && streamType !== _TrtcDefines.TRTCVideoStreamType.TRTCVideoStreamTypeSub) {
        throw new _TrtcCode.default({
          code: _TrtcCode.TXLiteJSError.INVALID_PARAMETER,
          message: "".concat(_constants.NAME.LOG_PREFIX, " please check the snapshotVideo method parameters")
        });
      }
      try {
        TrtcNativeTrtcCloudModule.snapshotVideo({
          userId: userId || null,
          streamType: streamType
        });
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
  }, {
    key: "startLocalAudio",
    value: function startLocalAudio() {
      var quality = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : _TrtcDefines.TRTCAudioQuality.TRTCAudioQualityDefault;
      if (quality !== _TrtcDefines.TRTCAudioQuality.TRTCAudioQualitySpeech && quality !== _TrtcDefines.TRTCAudioQuality.TRTCAudioQualityDefault && quality !== _TrtcDefines.TRTCAudioQuality.TRTCAudioQualityMusic) {
        throw new _TrtcCode.default({
          code: _TrtcCode.TXLiteJSError.INVALID_PARAMETER,
          message: "".concat(_constants.NAME.LOG_PREFIX, " please check the startLocalAudio method parameters")
        });
      }
      try {
        TrtcNativeTrtcCloudModule.startLocalAudio(quality);
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
  }, {
    key: "stopLocalAudio",
    value: function stopLocalAudio() {
      try {
        TrtcNativeTrtcCloudModule.stopLocalAudio();
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
  }, {
    key: "muteLocalAudio",
    value: function muteLocalAudio(mute) {
      if ((0, _typeof2.default)(mute) !== _constants.NAME.BOOLEAN) {
        throw new _TrtcCode.default({
          code: _TrtcCode.TXLiteJSError.INVALID_PARAMETER,
          message: "".concat(_constants.NAME.LOG_PREFIX, " please check the muteLocalAudio method parameters, mute type is a ").concat((0, _typeof2.default)(mute), " not a ").concat(_constants.NAME.BOOLEAN)
        });
      }
      try {
        TrtcNativeTrtcCloudModule.muteLocalAudio(!!mute);
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
  }, {
    key: "muteRemoteAudio",
    value: function muteRemoteAudio(userId, mute) {
      if ((0, _typeof2.default)(mute) !== _constants.NAME.BOOLEAN || !userId) {
        throw new _TrtcCode.default({
          code: _TrtcCode.TXLiteJSError.INVALID_PARAMETER,
          message: "".concat(_constants.NAME.LOG_PREFIX, " please check the muteRemoteAudio method parameters")
        });
      }
      try {
        TrtcNativeTrtcCloudModule.muteRemoteAudio({
          userId: userId,
          mute: !!mute
        });
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
  }, {
    key: "muteAllRemoteAudio",
    value: function muteAllRemoteAudio(mute) {
      if ((0, _typeof2.default)(mute) !== _constants.NAME.BOOLEAN) {
        throw new _TrtcCode.default({
          code: _TrtcCode.TXLiteJSError.INVALID_PARAMETER,
          message: "".concat(_constants.NAME.LOG_PREFIX, " please check the muteAllRemoteAudio method parameters, mute type is a ").concat((0, _typeof2.default)(mute), " not a ").concat(_constants.NAME.BOOLEAN)
        });
      }
      try {
        TrtcNativeTrtcCloudModule.muteAllRemoteAudio(!!mute);
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
  }, {
    key: "setAudioRoute",
    value: function setAudioRoute(route) {
      if (route !== _TrtcDefines.TRTCAudioRoute.TRTCAudioRouteSpeaker && route !== _TrtcDefines.TRTCAudioRoute.TRTCAudioRouteEarpiece) {
        throw new _TrtcCode.default({
          code: _TrtcCode.TXLiteJSError.INVALID_PARAMETER,
          message: "".concat(_constants.NAME.LOG_PREFIX, " please check the setAudioRoute method parameter, route is not of TRTCAudioRoute")
        });
      }
      try {
        TrtcNativeTrtcCloudModule.setAudioRoute(route);
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
  }, {
    key: "enableAudioVolumeEvaluation",
    value: function enableAudioVolumeEvaluation(interval) {
      if ((0, _typeof2.default)(interval) !== _constants.NAME.NUMBER) {
        throw new _TrtcCode.default({
          code: _TrtcCode.TXLiteJSError.INVALID_PARAMETER,
          message: "".concat(_constants.NAME.LOG_PREFIX, " please check the enableAudioVolumeEvaluation method parameter, interval type is a ").concat((0, _typeof2.default)(interval), " not a ").concat(_constants.NAME.NUMBER)
        });
      }
      try {
        interval > 0 && TrtcNativeTrtcCloudModule.enableAudioVolumeEvaluation(interval);
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
    // ///////////////////////////////////////////////////////////////////////////////
    //
    //                      美颜 + 水印
    //
    // ///////////////////////////////////////////////////////////////////////////////
  }, {
    key: "setBeautyStyle",
    value: function setBeautyStyle(beautyStyle) {
      if (beautyStyle !== _TrtcDefines.TRTCBeautyStyle.TRTCBeautyStyleSmooth && beautyStyle !== _TrtcDefines.TRTCBeautyStyle.TRTCBeautyStyleNature && beautyStyle !== _TrtcDefines.TRTCBeautyStyle.TRTCBeautyStylePitu) {
        throw new _TrtcCode.default({
          code: _TrtcCode.TXLiteJSError.INVALID_PARAMETER,
          message: "".concat(_constants.NAME.LOG_PREFIX, " please check the setBeautyStyle method parameter, beautyStyle is not of TRTCBeautyStyle")
        });
      }
      try {
        TrtcNativeTrtcCloudModule.setBeautyStyle(beautyStyle);
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
  }, {
    key: "setBeautyLevel",
    value: function setBeautyLevel(beautyLevel) {
      if ((0, _typeof2.default)(beautyLevel) !== _constants.NAME.NUMBER || beautyLevel < 0 || beautyLevel > 9) {
        throw new _TrtcCode.default({
          code: _TrtcCode.TXLiteJSError.INVALID_PARAMETER,
          message: "".concat(_constants.NAME.LOG_PREFIX, " please check the setBeautyLevel method parameter, beautyLevel should in the range 0-9")
        });
      }
      try {
        TrtcNativeTrtcCloudModule.setBeautyLevel(beautyLevel);
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
    // ///////////////////////////////////////////////////////////////////////////////
    //
    //                      背景音效
    //
    // ///////////////////////////////////////////////////////////////////////////////
  }, {
    key: "startPlayMusic",
    value: function startPlayMusic(musicParam) {
      try {
        TXAudioEffectManagerModule.startPlayMusic(musicParam);
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
  }, {
    key: "stopPlayMusic",
    value: function stopPlayMusic(id) {
      try {
        TXAudioEffectManagerModule.stopPlayMusic(id);
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
  }, {
    key: "pausePlayMusic",
    value: function pausePlayMusic(id) {
      try {
        TXAudioEffectManagerModule.pausePlayMusic(id);
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
  }, {
    key: "resumePlayMusic",
    value: function resumePlayMusic(id) {
      try {
        TXAudioEffectManagerModule.resumePlayMusic(id);
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
    // ///////////////////////////////////////////////////////////////////////////////
    //
    //                      屏幕分享
    //
    // ///////////////////////////////////////////////////////////////////////////////
  }, {
    key: "setSubStreamEncoderParam",
    value: function setSubStreamEncoderParam(param) {
      try {
        TrtcNativeTrtcCloudModule.setSubStreamEncoderParam(param);
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
  }, {
    key: "startScreenCapture",
    value: function startScreenCapture() {
      var streamType = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : _TrtcDefines.TRTCVideoStreamType.TRTCVideoStreamTypeSub;
      var encParams = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      try {
        var platform = uni.getSystemInfoSync().platform;
        if (streamType !== _TrtcDefines.TRTCVideoStreamType.TRTCVideoStreamTypeSub && streamType !== _TrtcDefines.TRTCVideoStreamType.TRTCVideoStreamTypeBig) {
          streamType = _TrtcDefines.TRTCVideoStreamType.TRTCVideoStreamTypeSub;
        }
        var screenCaptureParams = Object.assign({
          streamType: streamType
        }, encParams);
        if (platform === _constants.NAME.ANDROID) {
          TrtcNativeTrtcCloudModule.startScreenCapture(screenCaptureParams);
        }
        if (platform === _constants.NAME.IOS) {
          // 开始应用内的屏幕分享（仅支持 iOS 13.0 及以上系统）
          TrtcNativeTrtcCloudModule.startScreenCaptureInApp(screenCaptureParams);
          // if (shareSource === TRTCShareSource.InApp) {
          //   TrtcNativeTrtcCloudModule.startScreenCaptureInApp(screenCaptureParams);
          // }
          // // 开始全系统的屏幕分享（仅支持 iOS 11.0 及以上系统）
          // if (shareSource === TRTCShareSource.ByReplaykit) {
          //   TrtcNativeTrtcCloudModule.startScreenCaptureByReplaykit({ ...screenCaptureParams, appGroup: null });
          // }
        }
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
  }, {
    key: "stopScreenCapture",
    value: function stopScreenCapture() {
      try {
        TrtcNativeTrtcCloudModule.stopScreenCapture();
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
  }, {
    key: "pauseScreenCapture",
    value: function pauseScreenCapture() {
      try {
        TrtcNativeTrtcCloudModule.pauseScreenCapture();
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
  }, {
    key: "resumeScreenCapture",
    value: function resumeScreenCapture() {
      try {
        TrtcNativeTrtcCloudModule.resumeScreenCapture();
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
  }], [{
    key: "_createInstance",
    value: function _createInstance() {
      try {
        if (trtcCloud) {
          return trtcCloud;
        }
        TrtcNativeTrtcCloudModule.sharedInstance();
        trtcCloud = new TrtcCloudImpl();
        return trtcCloud;
      } catch (error) {
        throw (0, _TrtcCode.generateError_)(error);
      }
    }
  }, {
    key: "_getInstance",
    value: function _getInstance() {
      if (trtcCloud) {
        return trtcCloud;
      }
      throw new _TrtcCode.default({
        code: _TrtcCode.TXLiteJSError.INVALID_OPERATION,
        message: 'get trtcCloud failed, please create trtcCloud first'
      });
    }
  }, {
    key: "_destroyInstance",
    value: function _destroyInstance() {
      try {
        trtcCloud = null;
        TrtcNativeTrtcCloudModule.destroySharedInstance();
      } catch (error) {
        throw new _TrtcCode.default({
          code: error.code || _TrtcCode.TXLiteJSError.UNKNOWN,
          message: error.message,
          name: error.name
        });
      }
    }
  }]);
  return TrtcCloudImpl;
}();
exports.default = TrtcCloudImpl;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js */ 2)["default"]))

/***/ }),
/* 52 */
/*!************************************************************************************************!*\
  !*** ./node_modules/@dcloudio/vue-cli-plugin-uni/packages/@babel/runtime/regenerator/index.js ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// TODO(Babel 8): Remove this file.

var runtime = __webpack_require__(/*! @babel/runtime/helpers/regeneratorRuntime */ 53)();
module.exports = runtime;

/***/ }),
/* 53 */
/*!*******************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/regeneratorRuntime.js ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__(/*! ./typeof.js */ 13)["default"];
function _regeneratorRuntime() {
  "use strict";

  /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */
  module.exports = _regeneratorRuntime = function _regeneratorRuntime() {
    return exports;
  }, module.exports.__esModule = true, module.exports["default"] = module.exports;
  var exports = {},
    Op = Object.prototype,
    hasOwn = Op.hasOwnProperty,
    defineProperty = Object.defineProperty || function (obj, key, desc) {
      obj[key] = desc.value;
    },
    $Symbol = "function" == typeof Symbol ? Symbol : {},
    iteratorSymbol = $Symbol.iterator || "@@iterator",
    asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator",
    toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";
  function define(obj, key, value) {
    return Object.defineProperty(obj, key, {
      value: value,
      enumerable: !0,
      configurable: !0,
      writable: !0
    }), obj[key];
  }
  try {
    define({}, "");
  } catch (err) {
    define = function define(obj, key, value) {
      return obj[key] = value;
    };
  }
  function wrap(innerFn, outerFn, self, tryLocsList) {
    var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator,
      generator = Object.create(protoGenerator.prototype),
      context = new Context(tryLocsList || []);
    return defineProperty(generator, "_invoke", {
      value: makeInvokeMethod(innerFn, self, context)
    }), generator;
  }
  function tryCatch(fn, obj, arg) {
    try {
      return {
        type: "normal",
        arg: fn.call(obj, arg)
      };
    } catch (err) {
      return {
        type: "throw",
        arg: err
      };
    }
  }
  exports.wrap = wrap;
  var ContinueSentinel = {};
  function Generator() {}
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}
  var IteratorPrototype = {};
  define(IteratorPrototype, iteratorSymbol, function () {
    return this;
  });
  var getProto = Object.getPrototypeOf,
    NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  NativeIteratorPrototype && NativeIteratorPrototype !== Op && hasOwn.call(NativeIteratorPrototype, iteratorSymbol) && (IteratorPrototype = NativeIteratorPrototype);
  var Gp = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(IteratorPrototype);
  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function (method) {
      define(prototype, method, function (arg) {
        return this._invoke(method, arg);
      });
    });
  }
  function AsyncIterator(generator, PromiseImpl) {
    function invoke(method, arg, resolve, reject) {
      var record = tryCatch(generator[method], generator, arg);
      if ("throw" !== record.type) {
        var result = record.arg,
          value = result.value;
        return value && "object" == _typeof(value) && hasOwn.call(value, "__await") ? PromiseImpl.resolve(value.__await).then(function (value) {
          invoke("next", value, resolve, reject);
        }, function (err) {
          invoke("throw", err, resolve, reject);
        }) : PromiseImpl.resolve(value).then(function (unwrapped) {
          result.value = unwrapped, resolve(result);
        }, function (error) {
          return invoke("throw", error, resolve, reject);
        });
      }
      reject(record.arg);
    }
    var previousPromise;
    defineProperty(this, "_invoke", {
      value: function value(method, arg) {
        function callInvokeWithMethodAndArg() {
          return new PromiseImpl(function (resolve, reject) {
            invoke(method, arg, resolve, reject);
          });
        }
        return previousPromise = previousPromise ? previousPromise.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg();
      }
    });
  }
  function makeInvokeMethod(innerFn, self, context) {
    var state = "suspendedStart";
    return function (method, arg) {
      if ("executing" === state) throw new Error("Generator is already running");
      if ("completed" === state) {
        if ("throw" === method) throw arg;
        return doneResult();
      }
      for (context.method = method, context.arg = arg;;) {
        var delegate = context.delegate;
        if (delegate) {
          var delegateResult = maybeInvokeDelegate(delegate, context);
          if (delegateResult) {
            if (delegateResult === ContinueSentinel) continue;
            return delegateResult;
          }
        }
        if ("next" === context.method) context.sent = context._sent = context.arg;else if ("throw" === context.method) {
          if ("suspendedStart" === state) throw state = "completed", context.arg;
          context.dispatchException(context.arg);
        } else "return" === context.method && context.abrupt("return", context.arg);
        state = "executing";
        var record = tryCatch(innerFn, self, context);
        if ("normal" === record.type) {
          if (state = context.done ? "completed" : "suspendedYield", record.arg === ContinueSentinel) continue;
          return {
            value: record.arg,
            done: context.done
          };
        }
        "throw" === record.type && (state = "completed", context.method = "throw", context.arg = record.arg);
      }
    };
  }
  function maybeInvokeDelegate(delegate, context) {
    var methodName = context.method,
      method = delegate.iterator[methodName];
    if (undefined === method) return context.delegate = null, "throw" === methodName && delegate.iterator["return"] && (context.method = "return", context.arg = undefined, maybeInvokeDelegate(delegate, context), "throw" === context.method) || "return" !== methodName && (context.method = "throw", context.arg = new TypeError("The iterator does not provide a '" + methodName + "' method")), ContinueSentinel;
    var record = tryCatch(method, delegate.iterator, context.arg);
    if ("throw" === record.type) return context.method = "throw", context.arg = record.arg, context.delegate = null, ContinueSentinel;
    var info = record.arg;
    return info ? info.done ? (context[delegate.resultName] = info.value, context.next = delegate.nextLoc, "return" !== context.method && (context.method = "next", context.arg = undefined), context.delegate = null, ContinueSentinel) : info : (context.method = "throw", context.arg = new TypeError("iterator result is not an object"), context.delegate = null, ContinueSentinel);
  }
  function pushTryEntry(locs) {
    var entry = {
      tryLoc: locs[0]
    };
    1 in locs && (entry.catchLoc = locs[1]), 2 in locs && (entry.finallyLoc = locs[2], entry.afterLoc = locs[3]), this.tryEntries.push(entry);
  }
  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal", delete record.arg, entry.completion = record;
  }
  function Context(tryLocsList) {
    this.tryEntries = [{
      tryLoc: "root"
    }], tryLocsList.forEach(pushTryEntry, this), this.reset(!0);
  }
  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) return iteratorMethod.call(iterable);
      if ("function" == typeof iterable.next) return iterable;
      if (!isNaN(iterable.length)) {
        var i = -1,
          next = function next() {
            for (; ++i < iterable.length;) {
              if (hasOwn.call(iterable, i)) return next.value = iterable[i], next.done = !1, next;
            }
            return next.value = undefined, next.done = !0, next;
          };
        return next.next = next;
      }
    }
    return {
      next: doneResult
    };
  }
  function doneResult() {
    return {
      value: undefined,
      done: !0
    };
  }
  return GeneratorFunction.prototype = GeneratorFunctionPrototype, defineProperty(Gp, "constructor", {
    value: GeneratorFunctionPrototype,
    configurable: !0
  }), defineProperty(GeneratorFunctionPrototype, "constructor", {
    value: GeneratorFunction,
    configurable: !0
  }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, toStringTagSymbol, "GeneratorFunction"), exports.isGeneratorFunction = function (genFun) {
    var ctor = "function" == typeof genFun && genFun.constructor;
    return !!ctor && (ctor === GeneratorFunction || "GeneratorFunction" === (ctor.displayName || ctor.name));
  }, exports.mark = function (genFun) {
    return Object.setPrototypeOf ? Object.setPrototypeOf(genFun, GeneratorFunctionPrototype) : (genFun.__proto__ = GeneratorFunctionPrototype, define(genFun, toStringTagSymbol, "GeneratorFunction")), genFun.prototype = Object.create(Gp), genFun;
  }, exports.awrap = function (arg) {
    return {
      __await: arg
    };
  }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, asyncIteratorSymbol, function () {
    return this;
  }), exports.AsyncIterator = AsyncIterator, exports.async = function (innerFn, outerFn, self, tryLocsList, PromiseImpl) {
    void 0 === PromiseImpl && (PromiseImpl = Promise);
    var iter = new AsyncIterator(wrap(innerFn, outerFn, self, tryLocsList), PromiseImpl);
    return exports.isGeneratorFunction(outerFn) ? iter : iter.next().then(function (result) {
      return result.done ? result.value : iter.next();
    });
  }, defineIteratorMethods(Gp), define(Gp, toStringTagSymbol, "Generator"), define(Gp, iteratorSymbol, function () {
    return this;
  }), define(Gp, "toString", function () {
    return "[object Generator]";
  }), exports.keys = function (val) {
    var object = Object(val),
      keys = [];
    for (var key in object) {
      keys.push(key);
    }
    return keys.reverse(), function next() {
      for (; keys.length;) {
        var key = keys.pop();
        if (key in object) return next.value = key, next.done = !1, next;
      }
      return next.done = !0, next;
    };
  }, exports.values = values, Context.prototype = {
    constructor: Context,
    reset: function reset(skipTempReset) {
      if (this.prev = 0, this.next = 0, this.sent = this._sent = undefined, this.done = !1, this.delegate = null, this.method = "next", this.arg = undefined, this.tryEntries.forEach(resetTryEntry), !skipTempReset) for (var name in this) {
        "t" === name.charAt(0) && hasOwn.call(this, name) && !isNaN(+name.slice(1)) && (this[name] = undefined);
      }
    },
    stop: function stop() {
      this.done = !0;
      var rootRecord = this.tryEntries[0].completion;
      if ("throw" === rootRecord.type) throw rootRecord.arg;
      return this.rval;
    },
    dispatchException: function dispatchException(exception) {
      if (this.done) throw exception;
      var context = this;
      function handle(loc, caught) {
        return record.type = "throw", record.arg = exception, context.next = loc, caught && (context.method = "next", context.arg = undefined), !!caught;
      }
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i],
          record = entry.completion;
        if ("root" === entry.tryLoc) return handle("end");
        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc"),
            hasFinally = hasOwn.call(entry, "finallyLoc");
          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0);
            if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc);
          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0);
          } else {
            if (!hasFinally) throw new Error("try statement without catch or finally");
            if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc);
          }
        }
      }
    },
    abrupt: function abrupt(type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc <= this.prev && hasOwn.call(entry, "finallyLoc") && this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }
      finallyEntry && ("break" === type || "continue" === type) && finallyEntry.tryLoc <= arg && arg <= finallyEntry.finallyLoc && (finallyEntry = null);
      var record = finallyEntry ? finallyEntry.completion : {};
      return record.type = type, record.arg = arg, finallyEntry ? (this.method = "next", this.next = finallyEntry.finallyLoc, ContinueSentinel) : this.complete(record);
    },
    complete: function complete(record, afterLoc) {
      if ("throw" === record.type) throw record.arg;
      return "break" === record.type || "continue" === record.type ? this.next = record.arg : "return" === record.type ? (this.rval = this.arg = record.arg, this.method = "return", this.next = "end") : "normal" === record.type && afterLoc && (this.next = afterLoc), ContinueSentinel;
    },
    finish: function finish(finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) return this.complete(entry.completion, entry.afterLoc), resetTryEntry(entry), ContinueSentinel;
      }
    },
    "catch": function _catch(tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;
          if ("throw" === record.type) {
            var thrown = record.arg;
            resetTryEntry(entry);
          }
          return thrown;
        }
      }
      throw new Error("illegal catch attempt");
    },
    delegateYield: function delegateYield(iterable, resultName, nextLoc) {
      return this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      }, "next" === this.method && (this.arg = undefined), ContinueSentinel;
    }
  }, exports;
}
module.exports = _regeneratorRuntime, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 54 */
/*!***********************************************************!*\
  !*** E:/公司项目/e考监控/Api-Example/TrtcCloud/lib/constants.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.errorCodeUrl = exports.NAME = void 0;
var NAME = {
  ANDROID: 'android',
  IOS: 'ios',
  STRING: 'string',
  FUNCTION: 'function',
  BOOLEAN: 'boolean',
  NUMBER: 'number',
  LOG_PREFIX: '【UniApp-JS】'
};
exports.NAME = NAME;
var errorCodeUrl = 'https://web.sdk.qcloud.com/trtc/uniapp/doc/zh-cn/ErrorCode.html';
exports.errorCodeUrl = errorCodeUrl;

/***/ }),
/* 55 */
/*!*************************************************************!*\
  !*** E:/公司项目/e考监控/Api-Example/TrtcCloud/lib/TrtcDefines.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ 4);
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TRTCVideoStreamType = exports.TRTCVideoRotation = exports.TRTCVideoResolutionMode = exports.TRTCVideoResolution = exports.TRTCVideoMirrorType = exports.TRTCVideoFillMode = exports.TRTCVideoEncParam = exports.TRTCShareSource = exports.TRTCRoleType = exports.TRTCRenderParams = exports.TRTCParams = exports.TRTCBeautyStyle = exports.TRTCAudioRoute = exports.TRTCAudioQuality = exports.TRTCAppScene = exports.AudioMusicParam = void 0;
var _createClass2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/createClass */ 24));
var _classCallCheck2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ 23));
/**
 * TRTC 关键类型定义<br>
 * @description 分辨率、质量等级等枚举和常量值的定义
 */
/////////////////////////////////////////////////////////////////////////////////
//
//                    【（一）视频相关枚举值定义】
//
/////////////////////////////////////////////////////////////////////////////////
/**
 * 视频分辨率<br>
 * 此处仅定义横屏分辨率（如 640 × 360），如需使用竖屏分辨率（如 360 × 640），需要同时指定 VideoResolutionMode 为 Portrait
 * @enum {Number}
 */
var TRTCVideoResolution_HACK_JSDOC = {
  /** 宽高比 1:1；分辨率 120x120；建议码率（VideoCall）80kbps; 建议码率（LIVE）120kbps */
  TRTCVideoResolution_120_120: 1,
  /** 宽高比 1:1 分辨率 160x160；建议码率（VideoCall）100kbps; 建议码率（LIVE）150kbps */
  TRTCVideoResolution_160_160: 3,
  /** 宽高比 1:1；分辨率 270x270；建议码率（VideoCall）200kbps; 建议码率（LIVE）300kbps */
  TRTCVideoResolution_270_270: 5,
  /** 宽高比 1:1；分辨率 480x480；建议码率（VideoCall）350kbps; 建议码率（LIVE）500kbps */
  TRTCVideoResolution_480_480: 7,
  /** 宽高比4:3；分辨率 160x120；建议码率（VideoCall）100kbps; 建议码率（LIVE）150kbps */
  TRTCVideoResolution_160_120: 50,
  /** 宽高比 4:3；分辨率 240x180；建议码率（VideoCall）150kbps; 建议码率（LIVE）250kbps */
  TRTCVideoResolution_240_180: 52,
  /** 宽高比 4:3；分辨率 280x210；建议码率（VideoCall）200kbps; 建议码率（LIVE）300kbps */
  TRTCVideoResolution_280_210: 54,
  /** 宽高比 4:3；分辨率 320x240；建议码率（VideoCall）250kbps; 建议码率（LIVE）375kbps */
  TRTCVideoResolution_320_240: 56,
  /** 宽高比 4:3；分辨率 400x300；建议码率（VideoCall）300kbps; 建议码率（LIVE）450kbps */
  TRTCVideoResolution_400_300: 58,
  /** 宽高比 4:3；分辨率 480x360；建议码率（VideoCall）400kbps; 建议码率（LIVE）600kbps */
  TRTCVideoResolution_480_360: 60,
  /** 宽高比 4:3；分辨率 640x480；建议码率（VideoCall）600kbps; 建议码率（LIVE）900kbps */
  TRTCVideoResolution_640_480: 62,
  /** 宽高比 4:3；分辨率 960x720；建议码率（VideoCall）1000kbps; 建议码率（LIVE）1500kbps */
  TRTCVideoResolution_960_720: 64,
  /** 宽高比 16:9；分辨率 160x90；建议码率（VideoCall）150kbps; 建议码率（LIVE）250kbps */
  TRTCVideoResolution_160_90: 100,
  /** 宽高比 16:9；分辨率 256x144；建议码率（VideoCall）200kbps; 建议码率（LIVE）300kbps */
  TRTCVideoResolution_256_144: 102,
  /** 宽高比 16:9；分辨率 320x180；建议码率（VideoCall）250kbps; 建议码率（LIVE）400kbps */
  TRTCVideoResolution_320_180: 104,
  /** 宽高比 16:9；分辨率 480x270；建议码率（VideoCall）350kbps; 建议码率（LIVE）550kbps */
  TRTCVideoResolution_480_270: 106,
  /** 宽高比 16:9；分辨率 640x360；建议码率（VideoCall）500kbps; 建议码率（LIVE）900kbps */
  TRTCVideoResolution_640_360: 108,
  /** 宽高比 16:9；分辨率 960x540；建议码率（VideoCall）850kbps; 建议码率（LIVE）1300kbps */
  TRTCVideoResolution_960_540: 110,
  /** 宽高比 16:9；分辨率 1280x720；建议码率（VideoCall）1200kbps; 建议码率（LIVE）1800kbps */
  TRTCVideoResolution_1280_720: 112,
  /** 宽高比 16:9；分辨率 1920x1080；建议码率（VideoCall）2000kbps; 建议码率（LIVE）3000kbps */
  TRTCVideoResolution_1920_1080: 114
};
var TRTCVideoResolution;
exports.TRTCVideoResolution = TRTCVideoResolution;
(function (TRTCVideoResolution) {
  TRTCVideoResolution[TRTCVideoResolution["TRTCVideoResolution_120_120"] = 1] = "TRTCVideoResolution_120_120";
  TRTCVideoResolution[TRTCVideoResolution["TRTCVideoResolution_160_160"] = 3] = "TRTCVideoResolution_160_160";
  TRTCVideoResolution[TRTCVideoResolution["TRTCVideoResolution_270_270"] = 5] = "TRTCVideoResolution_270_270";
  TRTCVideoResolution[TRTCVideoResolution["TRTCVideoResolution_480_480"] = 7] = "TRTCVideoResolution_480_480";
  TRTCVideoResolution[TRTCVideoResolution["TRTCVideoResolution_160_120"] = 50] = "TRTCVideoResolution_160_120";
  TRTCVideoResolution[TRTCVideoResolution["TRTCVideoResolution_240_180"] = 52] = "TRTCVideoResolution_240_180";
  TRTCVideoResolution[TRTCVideoResolution["TRTCVideoResolution_280_210"] = 54] = "TRTCVideoResolution_280_210";
  TRTCVideoResolution[TRTCVideoResolution["TRTCVideoResolution_320_240"] = 56] = "TRTCVideoResolution_320_240";
  TRTCVideoResolution[TRTCVideoResolution["TRTCVideoResolution_400_300"] = 58] = "TRTCVideoResolution_400_300";
  TRTCVideoResolution[TRTCVideoResolution["TRTCVideoResolution_480_360"] = 60] = "TRTCVideoResolution_480_360";
  TRTCVideoResolution[TRTCVideoResolution["TRTCVideoResolution_640_480"] = 62] = "TRTCVideoResolution_640_480";
  TRTCVideoResolution[TRTCVideoResolution["TRTCVideoResolution_960_720"] = 64] = "TRTCVideoResolution_960_720";
  TRTCVideoResolution[TRTCVideoResolution["TRTCVideoResolution_160_90"] = 100] = "TRTCVideoResolution_160_90";
  TRTCVideoResolution[TRTCVideoResolution["TRTCVideoResolution_256_144"] = 102] = "TRTCVideoResolution_256_144";
  TRTCVideoResolution[TRTCVideoResolution["TRTCVideoResolution_320_180"] = 104] = "TRTCVideoResolution_320_180";
  TRTCVideoResolution[TRTCVideoResolution["TRTCVideoResolution_480_270"] = 106] = "TRTCVideoResolution_480_270";
  TRTCVideoResolution[TRTCVideoResolution["TRTCVideoResolution_640_360"] = 108] = "TRTCVideoResolution_640_360";
  TRTCVideoResolution[TRTCVideoResolution["TRTCVideoResolution_960_540"] = 110] = "TRTCVideoResolution_960_540";
  TRTCVideoResolution[TRTCVideoResolution["TRTCVideoResolution_1280_720"] = 112] = "TRTCVideoResolution_1280_720";
  TRTCVideoResolution[TRTCVideoResolution["TRTCVideoResolution_1920_1080"] = 114] = "TRTCVideoResolution_1920_1080";
})(TRTCVideoResolution || (exports.TRTCVideoResolution = TRTCVideoResolution = {}));
/**
 * 视频分辨率模式<br>
 * TRTCVideoResolution 中仅定义了横屏分辨率（如 640 × 360），如需使用竖屏分辨率（如 360 × 640），需要同时指定 TRTCVideoResolutionMode 为 Portrait
 * @enum {Number}
 */
var TRTCVideoResolutionMode_HACK_JSDOC = {
  /** 横屏分辨率 */
  TRTCVideoResolutionModeLandscape: 0,
  /** 竖屏分辨率 */
  TRTCVideoResolutionModePortrait: 1
};
var TRTCVideoResolutionMode;
exports.TRTCVideoResolutionMode = TRTCVideoResolutionMode;
(function (TRTCVideoResolutionMode) {
  TRTCVideoResolutionMode[TRTCVideoResolutionMode["TRTCVideoResolutionModeLandscape"] = 0] = "TRTCVideoResolutionModeLandscape";
  TRTCVideoResolutionMode[TRTCVideoResolutionMode["TRTCVideoResolutionModePortrait"] = 1] = "TRTCVideoResolutionModePortrait";
})(TRTCVideoResolutionMode || (exports.TRTCVideoResolutionMode = TRTCVideoResolutionMode = {}));
;
/**
 * 视频流类型<br>
 * TRTC 内部有三种不同的音视频流，分别是：
 * - 高清大画面：一般用来传输摄像头的视频数据
 * - 低清小画面：小画面和大画面的内容相互，但是分辨率和码率都比大画面低，因此清晰度也更低
 * - 辅流画面：一般用于屏幕分享，同一时间在同一个房间中只允许一个用户发布辅流视频，其他用户必须要等该用户关闭之后才能发布自己的辅流
 *
 * **Note:**
 * - 不支持单独开启低清小画面，小画面必须依附于大画面而存在，SDK 会自动设定低清小画面的分辨率和码率
 * @enum {Number}
 */
var TRTCVideoStreamType_HACK_JSDOC = {
  /** 大画面视频流 */
  TRTCVideoStreamTypeBig: 0,
  /** 小画面视频流 */
  TRTCVideoStreamTypeSmall: 1,
  /** 辅流（屏幕分享） */
  TRTCVideoStreamTypeSub: 2
};
var TRTCVideoStreamType;
exports.TRTCVideoStreamType = TRTCVideoStreamType;
(function (TRTCVideoStreamType) {
  TRTCVideoStreamType[TRTCVideoStreamType["TRTCVideoStreamTypeBig"] = 0] = "TRTCVideoStreamTypeBig";
  TRTCVideoStreamType[TRTCVideoStreamType["TRTCVideoStreamTypeSmall"] = 1] = "TRTCVideoStreamTypeSmall";
  TRTCVideoStreamType[TRTCVideoStreamType["TRTCVideoStreamTypeSub"] = 2] = "TRTCVideoStreamTypeSub";
})(TRTCVideoStreamType || (exports.TRTCVideoStreamType = TRTCVideoStreamType = {}));
/**
 * 视频画面填充模式<br>
 * 如果画面的显示分辨率不等于画面的原始分辨率，就需要您设置画面的填充模式:
 * - TRTCVideoFillMode_Fill，图像铺满屏幕，超出显示视窗的视频部分将被截掉，所以画面显示可能不完整。
 * - TRTCVideoFillMode_Fit，图像长边填满屏幕，短边区域会被填充黑色，但画面的内容肯定是完整的。
 * @enum {Number}
 */
var TRTCVideoFillMode_HACK_JSDOC = {
  /** 图像铺满屏幕，超出显示视窗的视频部分将被截掉 */
  TRTCVideoFillMode_Fill: 0,
  /** 图像长边填满屏幕，短边区域会被填充黑色 */
  TRTCVideoFillMode_Fit: 1
};
var TRTCVideoFillMode;
exports.TRTCVideoFillMode = TRTCVideoFillMode;
(function (TRTCVideoFillMode) {
  TRTCVideoFillMode[TRTCVideoFillMode["TRTCVideoFillMode_Fill"] = 0] = "TRTCVideoFillMode_Fill";
  TRTCVideoFillMode[TRTCVideoFillMode["TRTCVideoFillMode_Fit"] = 1] = "TRTCVideoFillMode_Fit";
})(TRTCVideoFillMode || (exports.TRTCVideoFillMode = TRTCVideoFillMode = {}));
;
/**
 * 视频画面旋转方向<br>
 * TRTC SDK 提供了对本地和远程画面的旋转角度设置 API，如下的旋转角度都是指顺时针方向的。
 * @enum {Number}
 */
var TRTCVideoRotation_HACK_JSDOC = {
  /** 顺时针旋转0度 */
  TRTCVideoRotation_0: 0,
  /** 顺时针旋转90度 */
  TRTCVideoRotation_90: 1,
  /** 顺时针旋转180度 */
  TRTCVideoRotation_180: 2,
  /** 顺时针旋转270度 */
  TRTCVideoRotation_270: 3
};
var TRTCVideoRotation;
exports.TRTCVideoRotation = TRTCVideoRotation;
(function (TRTCVideoRotation) {
  TRTCVideoRotation[TRTCVideoRotation["TRTCVideoRotation_0"] = 0] = "TRTCVideoRotation_0";
  TRTCVideoRotation[TRTCVideoRotation["TRTCVideoRotation_90"] = 1] = "TRTCVideoRotation_90";
  TRTCVideoRotation[TRTCVideoRotation["TRTCVideoRotation_180"] = 2] = "TRTCVideoRotation_180";
  TRTCVideoRotation[TRTCVideoRotation["TRTCVideoRotation_270"] = 3] = "TRTCVideoRotation_270";
})(TRTCVideoRotation || (exports.TRTCVideoRotation = TRTCVideoRotation = {}));
/**
 * 画面渲染镜像类型<br>
 * TRTC 的画面镜像提供下列设置模式
 * @enum {Number}
 */
var TRTCVideoMirrorType_HACK_JSDOC = {
  /** 只适用于移动端， 本地预览时，前置摄像头镜像，后置摄像头不镜像 */
  TRTCVideoMirrorType_Auto: 0,
  /** 所有画面均镜像 */
  TRTCVideoMirrorType_Enable: 1,
  /** 所有画面均不镜像 */
  TRTCVideoMirrorType_Disable: 2
};
var TRTCVideoMirrorType;
exports.TRTCVideoMirrorType = TRTCVideoMirrorType;
(function (TRTCVideoMirrorType) {
  TRTCVideoMirrorType[TRTCVideoMirrorType["TRTCVideoMirrorType_Auto"] = 0] = "TRTCVideoMirrorType_Auto";
  TRTCVideoMirrorType[TRTCVideoMirrorType["TRTCVideoMirrorType_Enable"] = 1] = "TRTCVideoMirrorType_Enable";
  TRTCVideoMirrorType[TRTCVideoMirrorType["TRTCVideoMirrorType_Disable"] = 2] = "TRTCVideoMirrorType_Disable";
})(TRTCVideoMirrorType || (exports.TRTCVideoMirrorType = TRTCVideoMirrorType = {}));
/**
 * 美颜（磨皮）算法<br>
 * TRTC SDK 内置了多种不同的磨皮算法，您可以选择最适合您产品定位的方案。
 * @enum {Number}
 */
var TRTCBeautyStyle_HACK_JSDOC = {
  /** 光滑，算法比较激进，磨皮效果比较明显，适用于秀场直播 */
  TRTCBeautyStyleSmooth: 0,
  /** 自然，算法更多地保留了面部细节，磨皮效果更加自然，适用于绝大多数直播场景 */
  TRTCBeautyStyleNature: 1,
  /** 优图，由优图实验室提供，磨皮效果介于光滑和自然之间，比光滑保留更多皮肤细节，比自然磨皮程度更高 */
  TRTCBeautyStylePitu: 2
};
var TRTCBeautyStyle;
exports.TRTCBeautyStyle = TRTCBeautyStyle;
(function (TRTCBeautyStyle) {
  TRTCBeautyStyle[TRTCBeautyStyle["TRTCBeautyStyleSmooth"] = 0] = "TRTCBeautyStyleSmooth";
  TRTCBeautyStyle[TRTCBeautyStyle["TRTCBeautyStyleNature"] = 1] = "TRTCBeautyStyleNature";
  TRTCBeautyStyle[TRTCBeautyStyle["TRTCBeautyStylePitu"] = 2] = "TRTCBeautyStylePitu";
})(TRTCBeautyStyle || (exports.TRTCBeautyStyle = TRTCBeautyStyle = {}));
/**
 * 背景音效<br>
 * @enum {Number}
 */
var AudioMusicParam = /*#__PURE__*/(0, _createClass2.default)(function AudioMusicParam(id, path, loopCount, publish, isShortFile, startTimeMS, endTimeMS) {
  (0, _classCallCheck2.default)(this, AudioMusicParam);
  this.id = id;
  this.path = path;
  this.loopCount = loopCount;
  this.publish = publish;
  this.isShortFile = isShortFile;
  this.startTimeMS = startTimeMS;
  this.endTimeMS = endTimeMS;
}); /////////////////////////////////////////////////////////////////////////////////
//
//                    【（二）网络相关枚举值定义】
//
/////////////////////////////////////////////////////////////////////////////////
/**
 * 应用场景<br>
 * TRTC 可用于视频会议和在线直播等多种应用场景，针对不同的应用场景，TRTC SDK 的内部会进行不同的优化配置：
 * - TRTCAppSceneVideoCall    ：视频通话场景，适合[1对1视频通话]、[300人视频会议]、[在线问诊]、[视频聊天]、[远程面试]等。
 * - TRTCAppSceneLIVE         ：视频互动直播，适合[视频低延时直播]、[十万人互动课堂]、[视频直播 PK]、[视频相亲房]、[互动课堂]、[远程培训]、[超大型会议]等。
 * - TRTCAppSceneAudioCall    ：语音通话场景，适合[1对1语音通话]、[300人语音会议]、[语音聊天]、[语音会议]、[在线狼人杀]等。
 * - TRTCAppSceneVoiceChatRoom：语音互动直播，适合：[语音低延时直播]、[语音直播连麦]、[语聊房]、[K 歌房]、[FM 电台]等。
 * @enum {Number}
 */
exports.AudioMusicParam = AudioMusicParam;
var TRTCAppScene_HACK_JSDOC = {
  /**
   * 视频通话场景，支持720P、1080P高清画质，单个房间最多支持300人同时在线，最高支持50人同时发言。<br>
   * 适合：[视频低延时直播]、[十万人互动课堂]、[视频直播 PK]、[视频相亲房]、[互动课堂]、[远程培训]、[超大型会议]等。<br>
   * 注意：此场景下，您必须通过 TRTCParams 中的 role 字段指定当前用户的角色。
   */
  TRTCAppSceneVideoCall: 0,
  /**
   * 视频互动直播，支持平滑上下麦，切换过程无需等待，主播延时小于300ms；支持十万级别观众同时播放，播放延时低至1000ms。<br>
   * 在线直播场景，内部编码器和网络协议优化侧重性能和兼容性，性能和清晰度表现更佳。
   */
  TRTCAppSceneLIVE: 1,
  /**
   * 语音通话场景，支持 48kHz，支持双声道。单个房间最多支持300人同时在线，最高支持50人同时发言。<br>
   * 适合：[1对1语音通话]、[300人语音会议]、[语音聊天]、[语音会议]、[在线狼人杀]等。
   */
  TRTCAppSceneAudioCall: 2,
  /**
   * 语音互动直播，支持平滑上下麦，切换过程无需等待，主播延时小于300ms；支持十万级别观众同时播放，播放延时低至1000ms。<br>
   * 适合：[语音低延时直播]、[语音直播连麦]、[语聊房]、[K 歌房]、[FM 电台]等。<br>
   * 注意：此场景下，您必须通过 TRTCParams 中的 role 字段指定当前用户的角色。
   */
  TRTCAppSceneVoiceChatRoom: 3
};
var TRTCAppScene;
exports.TRTCAppScene = TRTCAppScene;
(function (TRTCAppScene) {
  TRTCAppScene[TRTCAppScene["TRTCAppSceneVideoCall"] = 0] = "TRTCAppSceneVideoCall";
  TRTCAppScene[TRTCAppScene["TRTCAppSceneLIVE"] = 1] = "TRTCAppSceneLIVE";
  TRTCAppScene[TRTCAppScene["TRTCAppSceneAudioCall"] = 2] = "TRTCAppSceneAudioCall";
  TRTCAppScene[TRTCAppScene["TRTCAppSceneVoiceChatRoom"] = 3] = "TRTCAppSceneVoiceChatRoom";
})(TRTCAppScene || (exports.TRTCAppScene = TRTCAppScene = {}));
/**
 * 角色，仅适用于直播场景（TRTCAppSceneLIVE 和 TRTCAppSceneVoiceChatRoom）<br>
 * 在直播场景中，多数用户只是观众，只有个别用户是主播，这种角色区分可以有利于 TRTC 进行更好的定向优化。
 * - Anchor：主播，可以上行视频和音频，一个房间里最多支持50个主播同时上行音视频。
 * - Audience：观众，只能观看，不能上行视频和音频，一个房间里的观众人数没有上限。
 *
 * @enum {Number}
 */
var TRTCRoleType_HACK_JSDOC = {
  /** 主播 */
  TRTCRoleAnchor: 20,
  /** 观众 */
  TRTCRoleAudience: 21
};
var TRTCRoleType;
exports.TRTCRoleType = TRTCRoleType;
(function (TRTCRoleType) {
  TRTCRoleType[TRTCRoleType["TRTCRoleAnchor"] = 20] = "TRTCRoleAnchor";
  TRTCRoleType[TRTCRoleType["TRTCRoleAudience"] = 21] = "TRTCRoleAudience";
})(TRTCRoleType || (exports.TRTCRoleType = TRTCRoleType = {}));
/////////////////////////////////////////////////////////////////////////////////
//
//                    【（三）音频相关枚举值定义】
//
/////////////////////////////////////////////////////////////////////////////////
/**
 * 音频质量<br>
 * @enum {Number}
 */
var TRTCAudioQuality_HACK_JSDOC = {
  /** 人声模式：适用于以人声沟通为主的应用场景，该模式下音频传输的抗性较强，TRTC 会通过各种人声处理技术保障在弱网络环境下的流畅度最佳 */
  TRTCAudioQualitySpeech: 1,
  /** 标准模式（或者默认模式）：介于 Speech 和 Music 之间的档位，对音乐的还原度比人声模式要好，但传输数据量比音乐模式要低很多，对各种场景均有不错的适应性，如无特殊需求推荐选择之。 */
  TRTCAudioQualityDefault: 2,
  /** 音乐模式：适用于对声乐要求很苛刻的场景，该模式下音频传输的数据量很大，TRTC 会通过各项技术确保音乐信号在各频段均能获得高保真的细节还原度 */
  TRTCAudioQualityMusic: 3
};
var TRTCAudioQuality;
exports.TRTCAudioQuality = TRTCAudioQuality;
(function (TRTCAudioQuality) {
  TRTCAudioQuality[TRTCAudioQuality["TRTCAudioQualitySpeech"] = 1] = "TRTCAudioQualitySpeech";
  TRTCAudioQuality[TRTCAudioQuality["TRTCAudioQualityDefault"] = 2] = "TRTCAudioQualityDefault";
  TRTCAudioQuality[TRTCAudioQuality["TRTCAudioQualityMusic"] = 3] = "TRTCAudioQualityMusic";
})(TRTCAudioQuality || (exports.TRTCAudioQuality = TRTCAudioQuality = {}));
/////////////////////////////////////////////////////////////////////////////////
//
//                      【（四）TRTC 核心类型定义】
//
/////////////////////////////////////////////////////////////////////////////////
/**
 * 进房相关参数<br>
 * 只有该参数填写正确，才能顺利调用 enterRoom 进入 roomId 所指定的音视频房间。
 * @param {Number}       sdkAppId      - 【字段含义】应用标识（必填），腾讯视频云基于 sdkAppId 完成计费统计。<br>
 *                                       【推荐取值】在腾讯云 [TRTC 控制台](https://console.cloud.tencent.com/rav/) 中创建应用，之后可以在账号信息页面中得到该 ID。<br>
 * @param {String}       userId        - 【字段含义】用户标识（必填）。当前用户的 userId，相当于用户名，UTF-8编码。<br>
 *                                       【推荐取值】如果一个用户在您的账号系统中的 ID 为“abc”，则 userId 即可设置为“abc”。<br>
 * @param {String}       userSig       - 【字段含义】用户签名（必填），当前 userId 对应的验证签名，相当于登录密码。<br>
 *                                       【推荐取值】请参考 [如何计算UserSig](https://cloud.tencent.com/document/product/647/17275)。<br>
 * @param {Number}       roomId        - 【字段含义】房间号码（必填），指定房间号，在同一个房间里的用户（userId）可以彼此看到对方并进行视频通话, roomId 和 strRoomId 必须填一个, 若您选用 strRoomId，则 roomId 需要填写为0。<br>
 *                                       【推荐取值】您可以随意指定，但请不要重复，如果您的用户账号 ID 是数字类型的，可以直接用创建者的用户 ID 作为 roomId。<br>
 * @param {String}       strRoomId     - 【字段含义】字符串房间号码（选填），roomId 和 strRoomId 必须填一个。若两者都填，则优先选择 roomId。<br>
 *                                       【推荐取值】您可以随意指定，但请不要重复。<br>
 * @param {TRTCRoleType} role          - 【字段含义】直播场景下的角色，仅适用于直播场景（TRTCAppSceneLIVE 和 TRTCAppSceneVoiceChatRoom），视频通话场景下指定无效。<br>
 *                                       【推荐取值】默认值：主播（TRTCRoleAnchor）<br>
 * @param {String}       privateMapKey - 【字段含义】房间签名（非必填），如果您希望某个房间只能让特定的某些 userId 进入，就需要使用 privateMapKey 进行权限保护。<br>
 *                                       【推荐取值】仅建议有高级别安全需求的客户使用，参考文档：[进房权限保护](https://cloud.tencent.com/document/product/647/32240)<br>
 * @param {String}       businessInfo  - 【字段含义】业务数据（非必填），某些非常用的高级特性才需要用到此字段。<br>
 *                                       【推荐取值】不建议使用<br>
 * @param {String}       streamId      - 【字段含义】绑定腾讯云直播 CDN 流 ID[非必填]，设置之后，您就可以在腾讯云直播 CDN 上通过标准直播方案（FLV或HLS）播放该用户的音视频流。<br>
 *                                       【推荐取值】限制长度为64字节，可以不填写，一种推荐的方案是使用 “sdkappid_roomid_userid_main” 作为 streamid，这样比较好辨认且不会在您的多个应用中发生冲突。<br>
 *                                       【特殊说明】要使用腾讯云直播 CDN，您需要先在[控制台](https://console.cloud.tencent.com/trtc/) 中的功能配置页开启“启动自动旁路直播”开关。<br>
 *                                       【参考文档】[CDN 旁路直播](https://cloud.tencent.com/document/product/647/16826)。
 * @param {String}       userDefineRecordId - 【字段含义】设置云端录制完成后的回调消息中的 "userdefinerecordid"  字段内容，便于您更方便的识别录制回调。<br>
 *                                            【推荐取值】限制长度为64字节，只允许包含大小写英文字母（a-zA-Z）、数字（0-9）及下划线和连词符。<br>
 *                                            【参考文档】[云端录制](https://cloud.tencent.com/document/product/647/16823)。
 */
var TRTCParams = /*#__PURE__*/(0, _createClass2.default)(function TRTCParams(sdkAppId, userId, roomId, userSig, strRoomId, privateMapKey, role, businessInfo, streamId, userDefineRecordId) {
  (0, _classCallCheck2.default)(this, TRTCParams);
  this.sdkAppId = sdkAppId;
  this.userId = userId;
  this.roomId = roomId;
  this.userSig = userSig;
  this.strRoomId = strRoomId;
  this.privateMapKey = privateMapKey;
  this.role = role;
  this.businessInfo = businessInfo;
  this.streamId = streamId;
  this.userDefineRecordId = userDefineRecordId;
});
/**
 * 视频编码参数<br>
 * 该设置决定了远端用户看到的画面质量（同时也是云端录制出的视频文件的画面质量）。
 * @param {TRTCVideoResolution}     videoResolution - 【字段含义】 视频分辨率<br>
 *                                                    【推荐取值】 <br>
 *                                                     - 视频通话建议选择360 × 640及以下分辨率，resMode 选择 Portrait。<br>
 *                                                     - 手机直播建议选择 540 × 960，resMode 选择 Portrait。<br>
 *                                                     - Window 和 iMac 建议选择 640 × 360 及以上分辨率，resMode 选择 Landscape。
 *                                                    【特别说明】 TRTCVideoResolution 默认只能横屏模式的分辨率，例如640 × 360。<br>
 *                                                                如需使用竖屏分辨率，请指定 resMode 为 Portrait，例如640 × 360结合 Portrait 则为360 × 640。<br>
 * @param {TRTCVideoResolutionMode} resMode         - 【字段含义】分辨率模式（横屏分辨率 - 竖屏分辨率）<br>
 *                                                    【推荐取值】手机直播建议选择 Portrait，Window 和 Mac 建议选择 Landscape。<br>
 *                                                    【特别说明】如果 videoResolution 指定分辨率 640 × 360，resMode 指定模式为 Portrait，则最终编码出的分辨率为360 × 640。<br>
 * @param {Number}                  videoFps        - 【字段含义】视频采集帧率<br>
 *                                                    【推荐取值】15fps 或 20fps，10fps 以下会有轻微卡顿感，5fps 以下卡顿感明显，20fps 以上的帧率则过于浪费（电影的帧率也只有 24fps）。<br>
 *                                                    【特别说明】很多 Android 手机的前置摄像头并不支持15fps以上的采集帧率，部分过于突出美颜功能的 Android 手机前置摄像头的采集帧率可能低于10fps。<br>
 * @param {Number}                  videoBitrate    - 【字段含义】视频上行码率<br>
 *                                                    【推荐取值】推荐设置请参考本文件前半部分 TRTCVideoResolution 定义处的注释说明<br>
 *                                                    【特别说明】码率太低会导致视频中有很多的马赛克<br>
 * @param {Number}                  minVideoBitrate  -【字段含义】最低视频码率，SDK 会在网络不佳的情况下主动降低视频码率，最低会降至 minVideoBitrate 所设定的数值。
 *                                                    【推荐取值】<br>
 *                                                      - 如果您追求“允许卡顿但要保持清晰”的效果，可以设置 minVideoBitrate 为 videoBitrate 的 60%；
 *                                                      - 如果您追求“允许模糊但要保持流畅”的效果，可以设置 minVideoBitrate 为 200kbps；
 *                                                      - 如果您将 videoBitrate 和 minVideoBitrate 设置为同一个值，等价于关闭 SDK 的自适应调节能力；
 *                                                      - 默认值：0，此时最低码率由 SDK 根据分辨率情况，自动设置合适的数值。<br>
 *                                                    【特别说明】<br>
 *                                                     - 当您把分辨率设置的比较高时，minVideoBitrate 不适合设置的太低，否则会出现画面模糊和大范围的马赛克宏块。
 *                                                       比如把分辨率设置为 720p，把码率设置为 200kbps，那么编码出的画面将会出现大范围区域性马赛克。
 * @param {Boolean}                 enableAdjustRes - 【字段含义】是否允许调整分辨率<br>
 *                                                    【推荐取值】 <br>
 *                                                     - 手机直播建议选择 NO。<br>
 *                                                     - 视频通话模式，若更关注流畅性，建议选择 YES，此时若遇到带宽有限的弱网，SDK 会自动降低分辨率以保障更好的流畅度（仅针对 TRTCVideoStreamTypeBig 生效）。
 *                                                     - 默认值：NO。<br>
 *                                                    【特别说明】若有录制需求，选择 YES 时，请确保通话过程中，调整分辨率不会影响您的录制效果。<br>
 */
exports.TRTCParams = TRTCParams;
var TRTCVideoEncParam = /*#__PURE__*/(0, _createClass2.default)(function TRTCVideoEncParam() {
  var videoResolution = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : TRTCVideoResolution.TRTCVideoResolution_640_360;
  var resMode = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : TRTCVideoResolutionMode.TRTCVideoResolutionModePortrait;
  var videoFps = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 15;
  var videoBitrate = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 550;
  var minVideoBitrate = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 0;
  var enableAdjustRes = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : false;
  (0, _classCallCheck2.default)(this, TRTCVideoEncParam);
  this.videoResolution = videoResolution;
  this.videoResolutionMode = resMode;
  this.videoFps = videoFps;
  this.videoBitrate = videoBitrate;
  this.minVideoBitrate = minVideoBitrate;
  this.enableAdjustRes = enableAdjustRes;
});
exports.TRTCVideoEncParam = TRTCVideoEncParam;
;
/**
 * 画面渲染参数<br>
 * 您可以通过设置此参数来控制画面的旋转、填充、镜像模式
 * @param {TRTCVideoRotation} rotation  - 【字段含义】视频画面旋转方向
 * @param {TRTCVideoFillMode} fillMode  - 【字段含义】视频画面填充模式
 * @param {TRTCVideoMirrorType} mirrorType  - 【字段含义】画面渲染镜像类型
 */
var TRTCRenderParams = /*#__PURE__*/(0, _createClass2.default)(function TRTCRenderParams() {
  var rotation = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : TRTCVideoRotation.TRTCVideoRotation_0;
  var fillMode = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : TRTCVideoFillMode.TRTCVideoFillMode_Fit;
  var mirrorType = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : TRTCVideoMirrorType.TRTCVideoMirrorType_Disable;
  (0, _classCallCheck2.default)(this, TRTCRenderParams);
  this.rotation = rotation;
  this.fillMode = fillMode;
  this.mirrorType = mirrorType;
});
/**
 * 音频路由（即声音的播放模式）<br>
 * @enum {Number}
 */
exports.TRTCRenderParams = TRTCRenderParams;
var TRTCAudioRoute_HACK_JSDOC = {
  /** 使用扬声器播放（即“免提”），扬声器位于手机底部，声音偏大，适合外放音乐 */
  TRTCAudioRouteSpeaker: 0,
  /** 使用听筒播放，听筒位于手机顶部，声音偏小，适合需要保护隐私的通话场景 */
  TRTCAudioRouteEarpiece: 1
};
var TRTCAudioRoute;
exports.TRTCAudioRoute = TRTCAudioRoute;
(function (TRTCAudioRoute) {
  TRTCAudioRoute[TRTCAudioRoute["TRTCAudioRouteSpeaker"] = 0] = "TRTCAudioRouteSpeaker";
  TRTCAudioRoute[TRTCAudioRoute["TRTCAudioRouteEarpiece"] = 1] = "TRTCAudioRouteEarpiece";
})(TRTCAudioRoute || (exports.TRTCAudioRoute = TRTCAudioRoute = {}));
/////////////////////////////////////////////////////////////////////////////////
//
//                    【其它参数】
//
/////////////////////////////////////////////////////////////////////////////////
var TRTCShareSource;
exports.TRTCShareSource = TRTCShareSource;
(function (TRTCShareSource) {
  TRTCShareSource["InApp"] = "InApp";
  TRTCShareSource["ByReplaykit"] = "ByReplaykit";
})(TRTCShareSource || (exports.TRTCShareSource = TRTCShareSource = {}));

/***/ }),
/* 56 */
/*!**********************************************************!*\
  !*** E:/公司项目/e考监控/Api-Example/TrtcCloud/lib/TrtcCode.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ 4);
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.TXLiteJSError = exports.TXLiteAVWarning = exports.TXLiteAVError = void 0;
exports.generateError_ = generateError_;
var _classCallCheck2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ 23));
var _createClass2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/createClass */ 24));
var _inherits2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/inherits */ 57));
var _possibleConstructorReturn2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/possibleConstructorReturn */ 58));
var _getPrototypeOf2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/getPrototypeOf */ 60));
var _wrapNativeSuper2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/wrapNativeSuper */ 61));
var _constants = __webpack_require__(/*! ./constants */ 54);
function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = (0, _getPrototypeOf2.default)(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = (0, _getPrototypeOf2.default)(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return (0, _possibleConstructorReturn2.default)(this, result); }; }
function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
/**
 * @namespace ErrorCode
 *
 * @description 错误码、警告码和事件列表
 */
/////////////////////////////////////////////////////////////////////////////////
//
//                     （一）错误码（严重）
//
/////////////////////////////////////////////////////////////////////////////////
/**
 * @memberof ErrorCode
 * @typedef 错误码（严重）
 * @description SDK 错误码（严重）对照表
 * | 符号 | 值 | 含义 |
 * |---|---|---|
 * |ERR_NULL|0|无错误|
 * |ERR_ROOM_ENTER_FAIL|-3301|进入房间失败|
 * |ERR_ENTER_ROOM_PARAM_NULL|-3316|进房参数为空，请检查 enterRoom:appScene: 接口调用是否传入有效的 param|
 * |ERR_SDK_APPID_INVALID|-3317|进房参数 sdkAppId 错误|
 * |ERR_ROOM_ID_INVALID|-3318|进房参数 roomId 错误|
 * |ERR_USER_ID_INVALID|-3319|进房参数 userID 不正确|
 * |ERR_USER_SIG_INVALID|-3320|进房参数 userSig 不正确|
 * |ERR_ROOM_REQUEST_ENTER_ROOM_TIMEOUT|-3308|请求进房超时，请检查网络|
 * |ERR_SERVER_INFO_SERVICE_SUSPENDED|-100013|服务不可用。请检查：套餐包剩余分钟数是否大于0，腾讯云账号是否欠费|
 * |ERR_ROOM_REQUEST_QUIT_ROOM_TIMEOUT|-3325|请求退房超时|
 * |ERR_CAMERA_START_FAIL|-1301|打开摄像头失败，例如在 Windows 或 Mac 设备，摄像头的配置程序（驱动程序）异常，禁用后重新启用设备，或者重启机器，或者更新配置程序|
 * |ERR_CAMERA_NOT_AUTHORIZED|-1314|摄像头设备未授权，通常在移动设备出现，可能是权限被用户拒绝了|
 * |ERR_CAMERA_SET_PARAM_FAIL|-1315|摄像头参数设置出错（参数不支持或其它）|
 * |ERR_CAMERA_OCCUPY|-1316|摄像头正在被占用中，可尝试打开其他摄像头|
 * |ERR_MIC_START_FAIL|-1302|打开麦克风失败，例如在 Windows 或 Mac 设备，麦克风的配置程序（驱动程序）异常，禁用后重新启用设备，或者重启机器，或者更新配置程序|
 * |ERR_MIC_NOT_AUTHORIZED|-1317|麦克风设备未授权，通常在移动设备出现，可能是权限被用户拒绝了|
 * |ERR_MIC_SET_PARAM_FAIL|-1318|麦克风设置参数失败|
 * |ERR_MIC_OCCUPY|-1319|麦克风正在被占用中，例如移动设备正在通话时，打开麦克风会失败|
 * |ERR_MIC_STOP_FAIL|-1320|停止麦克风失败|
 * |ERR_SPEAKER_START_FAIL|-1321|打开扬声器失败，例如在 Windows 或 Mac 设备，扬声器的配置程序（驱动程序）异常，禁用后重新启用设备，或者重启机器，或者更新配置程序|
 * |ERR_SPEAKER_SET_PARAM_FAIL|-1322|扬声器设置参数失败|
 * |ERR_SPEAKER_STOP_FAIL|-1323|停止扬声器失败|
 * |ERR_SCREEN_CAPTURE_START_FAIL|-1308|开始录屏失败，如果在移动设备出现，可能是权限被用户拒绝了，如果在 Windows 或 Mac 系统的设备出现，请检查录屏接口的参数是否符合要求|
 * |ERR_SCREEN_CAPTURE_UNSURPORT|-1309|录屏失败，在 Android 平台，需要5.0以上的系统|
 * |ERR_SERVER_CENTER_NO_PRIVILEDGE_PUSH_SUB_VIDEO|-102015|没有权限上行辅路|
 * |ERR_SERVER_CENTER_ANOTHER_USER_PUSH_SUB_VIDEO|-102016|其他用户正在上行辅路|
 * |ERR_VIDEO_ENCODE_FAIL|-1303|视频帧编码失败，例如 iOS 设备切换到其他应用时，硬编码器可能被系统释放，再切换回来时，硬编码器重启前，可能会抛出|
 * |ERR_UNSUPPORTED_RESOLUTION|-1305|不支持的视频分辨率|
 * |ERR_AUDIO_ENCODE_FAIL|-1304|音频帧编码失败，例如传入自定义音频数据，SDK 无法处理|
 * |ERR_UNSUPPORTED_SAMPLERATE|-1306|不支持的音频采样率|
 * |ERR_PIXEL_FORMAT_UNSUPPORTED|-1327|设置的 pixel format 不支持|
 * |ERR_BUFFER_TYPE_UNSUPPORTED|-1328|设置的 buffer type 不支持|
 * |ERR_PUBLISH_CDN_STREAM_REQUEST_TIME_OUT|-3321|旁路转推请求超时|
 * |ERR_CLOUD_MIX_TRANSCODING_REQUEST_TIME_OUT|-3322|云端混流请求超时|
 * |ERR_PUBLISH_CDN_STREAM_SERVER_FAILED|-3323|旁路转推回包异常|
 * |ERR_CLOUD_MIX_TRANSCODING_SERVER_FAILED|-3324|云端混流回包异常|
 * |ERR_ROOM_REQUEST_START_PUBLISHING_TIMEOUT|-3333|开始向腾讯云的直播 CDN 推流信令超时|
 * |ERR_ROOM_REQUEST_START_PUBLISHING_ERROR|-3334|开始向腾讯云的直播 CDN 推流信令异常|
 * |ERR_ROOM_REQUEST_STOP_PUBLISHING_TIMEOUT|-3335|停止向腾讯云的直播 CDN 推流信令超时|
 * |ERR_ROOM_REQUEST_STOP_PUBLISHING_ERROR|-3336|停止向腾讯云的直播 CDN 推流信令异常|
 * |ERR_ROOM_REQUEST_CONN_ROOM_TIMEOUT|-3326|请求连麦超时|
 * |ERR_ROOM_REQUEST_DISCONN_ROOM_TIMEOUT|-3327|请求退出连麦超时|
 * |ERR_ROOM_REQUEST_CONN_ROOM_INVALID_PARAM|-3328|无效参数|
 * |ERR_CONNECT_OTHER_ROOM_AS_AUDIENCE|-3330|当前是观众角色，不能请求或断开跨房连麦，需要先 switchRole() 到主播|
 * |ERR_SERVER_CENTER_CONN_ROOM_NOT_SUPPORT|-102031|不支持跨房间连麦|
 * |ERR_SERVER_CENTER_CONN_ROOM_REACH_MAX_NUM|-102032|达到跨房间连麦上限|
 * |ERR_SERVER_CENTER_CONN_ROOM_REACH_MAX_RETRY_TIMES|-102033|跨房间连麦重试次数耗尽|
 * |ERR_SERVER_CENTER_CONN_ROOM_REQ_TIMEOUT|-102034|跨房间连麦请求超时|
 * |ERR_SERVER_CENTER_CONN_ROOM_REQ|-102035|跨房间连麦请求格式错误|
 * |ERR_SERVER_CENTER_CONN_ROOM_NO_SIG|-102036|跨房间连麦无签名|
 * |ERR_SERVER_CENTER_CONN_ROOM_DECRYPT_SIG|-102037|跨房间连麦签名解密失败|
 * |ERR_SERVER_CENTER_CONN_ROOM_NO_KEY|-102038|未找到跨房间连麦签名解密密钥|
 * |ERR_SERVER_CENTER_CONN_ROOM_PARSE_SIG|-102039|跨房间连麦签名解析错误|
 * |ERR_SERVER_CENTER_CONN_ROOM_INVALID_SIG_TIME|-102040|跨房间连麦签名时间戳错误|
 * |ERR_SERVER_CENTER_CONN_ROOM_SIG_GROUPID|-102041|跨房间连麦签名不匹配|
 * |ERR_SERVER_CENTER_CONN_ROOM_NOT_CONNED|-102042|本房间无连麦|
 * |ERR_SERVER_CENTER_CONN_ROOM_USER_NOT_CONNED|-102043|本用户未发起连麦|
 * |ERR_SERVER_CENTER_CONN_ROOM_FAILED|-102044|跨房间连麦失败|
 * |ERR_SERVER_CENTER_CONN_ROOM_CANCEL_FAILED|-102045|取消跨房间连麦失败|
 * |ERR_SERVER_CENTER_CONN_ROOM_CONNED_ROOM_NOT_EXIST|-102046|被连麦房间不存在|
 * |ERR_SERVER_CENTER_CONN_ROOM_CONNED_REACH_MAX_ROOM|-102047|被连麦房间达到连麦上限|
 * |ERR_SERVER_CENTER_CONN_ROOM_CONNED_USER_NOT_EXIST|-102048|被连麦用户不存在|
 * |ERR_SERVER_CENTER_CONN_ROOM_CONNED_USER_DELETED|-102049|被连麦用户已被删除|
 * |ERR_SERVER_CENTER_CONN_ROOM_CONNED_USER_FULL|-102050|被连麦用户达到资源上限|
 * |ERR_SERVER_CENTER_CONN_ROOM_INVALID_SEQ|-102051|连麦请求序号错乱|
 */
var TXLiteAVError = {
  /** 无错误 */
  ERR_NULL: 0,
  /** 进入房间失败 */
  ERR_ROOM_ENTER_FAIL: -3301,
  /** 进房参数为空，请检查 enterRoom:appScene: 接口调用是否传入有效的 param */
  ERR_ENTER_ROOM_PARAM_NULL: -3316,
  /** 进房参数 sdkAppId 错误 */
  ERR_SDK_APPID_INVALID: -3317,
  /** 进房参数 roomId 错误 */
  ERR_ROOM_ID_INVALID: -3318,
  /** 进房参数 userID 不正确 */
  ERR_USER_ID_INVALID: -3319,
  /** 进房参数 userSig 不正确 */
  ERR_USER_SIG_INVALID: -3320,
  /** 请求进房超时，请检查网络 */
  ERR_ROOM_REQUEST_ENTER_ROOM_TIMEOUT: -3308,
  /** 服务不可用。请检查：套餐包剩余分钟数是否大于0，腾讯云账号是否欠费 */
  ERR_SERVER_INFO_SERVICE_SUSPENDED: -100013,
  /** 请求退房超时 */
  ERR_ROOM_REQUEST_QUIT_ROOM_TIMEOUT: -3325,
  /** 打开摄像头失败，例如在 Windows 或 Mac 设备，摄像头的配置程序（驱动程序）异常，禁用后重新启用设备，或者重启机器，或者更新配置程序 */
  ERR_CAMERA_START_FAIL: -1301,
  /** 摄像头设备未授权，通常在移动设备出现，可能是权限被用户拒绝了 */
  ERR_CAMERA_NOT_AUTHORIZED: -1314,
  /** 摄像头参数设置出错（参数不支持或其它） */
  ERR_CAMERA_SET_PARAM_FAIL: -1315,
  /** 摄像头正在被占用中，可尝试打开其他摄像头 */
  ERR_CAMERA_OCCUPY: -1316,
  /** 打开麦克风失败，例如在 Windows 或 Mac 设备，麦克风的配置程序（驱动程序）异常，禁用后重新启用设备，或者重启机器，或者更新配置程序 */
  ERR_MIC_START_FAIL: -1302,
  /** 麦克风设备未授权，通常在移动设备出现，可能是权限被用户拒绝了 */
  ERR_MIC_NOT_AUTHORIZED: -1317,
  /** 麦克风设置参数失败 */
  ERR_MIC_SET_PARAM_FAIL: -1318,
  /** 麦克风正在被占用中，例如移动设备正在通话时，打开麦克风会失败 */
  ERR_MIC_OCCUPY: -1319,
  /** 停止麦克风失败 */
  ERR_MIC_STOP_FAIL: -1320,
  /** 打开扬声器失败，例如在 Windows 或 Mac 设备，扬声器的配置程序（驱动程序）异常，禁用后重新启用设备，或者重启机器，或者更新配置程序 */
  ERR_SPEAKER_START_FAIL: -1321,
  /** 扬声器设置参数失败 */
  ERR_SPEAKER_SET_PARAM_FAIL: -1322,
  /** 停止扬声器失败 */
  ERR_SPEAKER_STOP_FAIL: -1323,
  /** 开始录屏失败，如果在移动设备出现，可能是权限被用户拒绝了，如果在 Windows 或 Mac 系统的设备出现，请检查录屏接口的参数是否符合要求 */
  ERR_SCREEN_CAPTURE_START_FAIL: -1308,
  /** 录屏失败，在 Android 平台，需要5.0以上的系统 */
  ERR_SCREEN_CAPTURE_UNSURPORT: -1309,
  /** 没有权限上行辅路 */
  ERR_SERVER_CENTER_NO_PRIVILEDGE_PUSH_SUB_VIDEO: -102015,
  /** 其他用户正在上行辅路 */
  ERR_SERVER_CENTER_ANOTHER_USER_PUSH_SUB_VIDEO: -102016,
  /** 视频帧编码失败，例如 iOS 设备切换到其他应用时，硬编码器可能被系统释放，再切换回来时，硬编码器重启前，可能会抛出 */
  ERR_VIDEO_ENCODE_FAIL: -1303,
  /** 音频帧编码失败，例如传入自定义音频数据，SDK 无法处理 */
  ERR_AUDIO_ENCODE_FAIL: -1304,
  /** 不支持的视频分辨率 */
  ERR_UNSUPPORTED_RESOLUTION: -1305,
  /** 不支持的音频采样率 */
  ERR_UNSUPPORTED_SAMPLERATE: -1306,
  /** 设置的 pixel format 不支持 */
  ERR_PIXEL_FORMAT_UNSUPPORTED: -1327,
  /** 设置的 buffer type 不支持 */
  ERR_BUFFER_TYPE_UNSUPPORTED: -1328,
  /** 旁路转推请求超时 */
  ERR_PUBLISH_CDN_STREAM_REQUEST_TIME_OUT: -3321,
  /** 云端混流请求超时 */
  ERR_CLOUD_MIX_TRANSCODING_REQUEST_TIME_OUT: -3322,
  /** 旁路转推回包异常 */
  ERR_PUBLISH_CDN_STREAM_SERVER_FAILED: -3323,
  /** 云端混流回包异常 */
  ERR_CLOUD_MIX_TRANSCODING_SERVER_FAILED: -3324,
  /** 开始向腾讯云的直播 CDN 推流信令超时 */
  ERR_ROOM_REQUEST_START_PUBLISHING_TIMEOUT: -3333,
  /** 开始向腾讯云的直播 CDN 推流信令异常 */
  ERR_ROOM_REQUEST_START_PUBLISHING_ERROR: -3334,
  /** 停止向腾讯云的直播 CDN 推流信令超时 */
  ERR_ROOM_REQUEST_STOP_PUBLISHING_TIMEOUT: -3335,
  /** 停止向腾讯云的直播 CDN 推流信令异常 */
  ERR_ROOM_REQUEST_STOP_PUBLISHING_ERROR: -3336,
  /** 请求连麦超时 */
  ERR_ROOM_REQUEST_CONN_ROOM_TIMEOUT: -3326,
  /** 请求退出连麦超时 */
  ERR_ROOM_REQUEST_DISCONN_ROOM_TIMEOUT: -3327,
  /** 无效参数 */
  ERR_ROOM_REQUEST_CONN_ROOM_INVALID_PARAM: -3328,
  /** 当前是观众角色，不能请求或断开跨房连麦，需要先 switchRole() 到主播 */
  ERR_CONNECT_OTHER_ROOM_AS_AUDIENCE: -3330,
  /** 不支持跨房间连麦 */
  ERR_SERVER_CENTER_CONN_ROOM_NOT_SUPPORT: -102031,
  /** 达到跨房间连麦上限 */
  ERR_SERVER_CENTER_CONN_ROOM_REACH_MAX_NUM: -102032,
  /** 跨房间连麦重试次数耗尽 */
  ERR_SERVER_CENTER_CONN_ROOM_REACH_MAX_RETRY_TIMES: -102033,
  /** 跨房间连麦请求超时 */
  ERR_SERVER_CENTER_CONN_ROOM_REQ_TIMEOUT: -102034,
  /** 跨房间连麦请求格式错误 */
  ERR_SERVER_CENTER_CONN_ROOM_REQ: -102035,
  /** 跨房间连麦无签名 */
  ERR_SERVER_CENTER_CONN_ROOM_NO_SIG: -102036,
  /** 跨房间连麦签名解密失败 */
  ERR_SERVER_CENTER_CONN_ROOM_DECRYPT_SIG: -102037,
  /** 未找到跨房间连麦签名解密密钥 */
  ERR_SERVER_CENTER_CONN_ROOM_NO_KEY: -102038,
  /** 跨房间连麦签名解析错误 */
  ERR_SERVER_CENTER_CONN_ROOM_PARSE_SIG: -102039,
  /** 跨房间连麦签名时间戳错误 */
  ERR_SERVER_CENTER_CONN_ROOM_INVALID_SIG_TIME: -102040,
  /** 跨房间连麦签名不匹配 */
  ERR_SERVER_CENTER_CONN_ROOM_SIG_GROUPID: -102041,
  /** 本房间无连麦 */
  ERR_SERVER_CENTER_CONN_ROOM_NOT_CONNED: -102042,
  /** 本用户未发起连麦 */
  ERR_SERVER_CENTER_CONN_ROOM_USER_NOT_CONNED: -102043,
  /** 跨房间连麦失败 */
  ERR_SERVER_CENTER_CONN_ROOM_FAILED: -102044,
  /** 取消跨房间连麦失败 */
  ERR_SERVER_CENTER_CONN_ROOM_CANCEL_FAILED: -102045,
  /** 被连麦房间不存在 */
  ERR_SERVER_CENTER_CONN_ROOM_CONNED_ROOM_NOT_EXIST: -102046,
  /** 被连麦房间达到连麦上限 */
  ERR_SERVER_CENTER_CONN_ROOM_CONNED_REACH_MAX_ROOM: -102047,
  /** 被连麦用户不存在 */
  ERR_SERVER_CENTER_CONN_ROOM_CONNED_USER_NOT_EXIST: -102048,
  /** 被连麦用户已被删除 */
  ERR_SERVER_CENTER_CONN_ROOM_CONNED_USER_DELETED: -102049,
  /** 被连麦用户达到资源上限 */
  ERR_SERVER_CENTER_CONN_ROOM_CONNED_USER_FULL: -102050,
  /** 连麦请求序号错乱 */
  ERR_SERVER_CENTER_CONN_ROOM_INVALID_SEQ: -102051,
  /** 直播，推流出现网络断开，且经过多次重试无法恢复 */
  ERR_RTMP_PUSH_NET_DISCONNECT: -1307,
  /** 直播，推流地址非法，例如不是 RTMP 协议的地址 */
  ERR_RTMP_PUSH_INVALID_ADDRESS: -1313,
  /** 直播，连接推流服务器失败（若支持智能选路，IP 全部失败） */
  ERR_RTMP_PUSH_NET_ALLADDRESS_FAIL: -1324,
  /** 直播，网络不可用，请确认 WiFi、移动数据或者有线网络是否正常 */
  ERR_RTMP_PUSH_NO_NETWORK: -1325,
  /** 直播，服务器拒绝连接请求，可能是该推流地址已经被占用，或者 TXSecret 校验失败，或者是过期了，或者是欠费了 */
  ERR_RTMP_PUSH_SERVER_REFUSE: -1326,
  /** 直播，网络断连，且经多次重连抢救无效，可以放弃治疗，更多重试请自行重启播放 */
  ERR_PLAY_LIVE_STREAM_NET_DISCONNECT: -2301,
  /** 直播，获取加速拉流的地址失败 */
  ERR_GET_RTMP_ACC_URL_FAIL: -2302,
  /** 播放的文件不存在 */
  ERR_FILE_NOT_FOUND: -2303,
  /** H265 解码失败 */
  ERR_HEVC_DECODE_FAIL: -2304,
  /** 点播，音视频流解密失败 */
  ERR_VOD_DECRYPT_FAIL: -2305,
  /** 点播，获取点播文件信息失败 */
  ERR_GET_VODFILE_MEDIAINFO_FAIL: -2306,
  /** 直播，切流失败（切流可以播放不同画面大小的视频） */
  ERR_PLAY_LIVE_STREAM_SWITCH_FAIL: -2307,
  /** 直播，服务器拒绝连接请求 */
  ERR_PLAY_LIVE_STREAM_SERVER_REFUSE: -2308,
  /** 直播，RTMPACC 低延时拉流失败，且经过多次重试无法恢复 */
  ERR_RTMP_ACC_FETCH_STREAM_FAIL: -2309,
  /** 心跳失败，客户端定时向服务器发送数据包，告诉服务器自己活着，这个错误通常是发包超时 */
  ERR_ROOM_HEARTBEAT_FAIL: -3302,
  /** 拉取接口机服务器地址失败 */
  ERR_ROOM_REQUEST_IP_FAIL: -3303,
  /** 连接接口机服务器失败 */
  ERR_ROOM_CONNECT_FAIL: -3304,
  /** 请求视频位失败 */
  ERR_ROOM_REQUEST_AVSEAT_FAIL: -3305,
  /** 请求 token https 超时，请检查网络是否正常，或网络防火墙是否放行 https 访问 official.opensso.tencent-cloud.com:443 */
  ERR_ROOM_REQUEST_TOKEN_HTTPS_TIMEOUT: -3306,
  /** 请求 IP 和 sig 超时，请检查网络是否正常，或网络防火墙是否放行 UDP 访问下列 IP 和域名 query.tencent-cloud.com:8000 162.14.23.140:8000 162.14.7.49:8000 */
  ERR_ROOM_REQUEST_IP_TIMEOUT: -3307,
  /** 请求视频位超时 */
  ERR_ROOM_REQUEST_VIDEO_FLAG_TIMEOUT: -3309,
  /** 请求视频数据超时 */
  ERR_ROOM_REQUEST_VIDEO_DATA_ROOM_TIMEOUT: -3310,
  /** 请求修改视频能力项超时 */
  ERR_ROOM_REQUEST_CHANGE_ABILITY_TIMEOUT: -3311,
  /** 请求状态上报超时 */
  ERR_ROOM_REQUEST_STATUS_REPORT_TIMEOUT: -3312,
  /** 请求关闭视频超时 */
  ERR_ROOM_REQUEST_CLOSE_VIDEO_TIMEOUT: -3313,
  /** 请求接收视频项超时 */
  ERR_ROOM_REQUEST_SET_RECEIVE_TIMEOUT: -3314,
  /** 请求 token 无效参数，请检查 TRTCParams.userSig 是否填写正确 */
  ERR_ROOM_REQUEST_TOKEN_INVALID_PARAMETER: -3315,
  /** 请求 AES TOKEN 时，server 返回的内容是空的 */
  ERR_ROOM_REQUEST_AES_TOKEN_RETURN_ERROR: -3329,
  /** 请求接口机 IP 返回的列表为空的 */
  ERR_ACCIP_LIST_EMPTY: -3331,
  /** 请求发送 Json 信令超时 */
  ERR_ROOM_REQUEST_SEND_JSON_CMD_TIMEOUT: -3332,
  // Info 服务器（查询接口机 IP）, 服务器错误码，数值范围[-100000, -110000]
  /** server 解包错误，可能请求数据被篡改 */
  ERR_SERVER_INFO_UNPACKING_ERROR: -100000,
  /** TOKEN 错误 */
  ERR_SERVER_INFO_TOKEN_ERROR: -100001,
  /** 分配接口机错误 */
  ERR_SERVER_INFO_ALLOCATE_ACCESS_FAILED: -100002,
  /** 生成签名错误 */
  ERR_SERVER_INFO_GENERATE_SIGN_FAILED: -100003,
  /** https token 超时 */
  ERR_SERVER_INFO_TOKEN_TIMEOUT: -100004,
  /** 无效的命令字 */
  ERR_SERVER_INFO_INVALID_COMMAND: -100005,
  /** 权限位校验失败 */
  ERR_SERVER_INFO_PRIVILEGE_FLAG_ERROR: -100006,
  /** https 请求时，生成加密 key 错误 */
  ERR_SERVER_INFO_GENERATE_KEN_ERROR: -100007,
  /** https 请求时，生成 token 错误 */
  ERR_SERVER_INFO_GENERATE_TOKEN_ERROR: -100008,
  /** 数据库查询失败（房间相关存储信息） */
  ERR_SERVER_INFO_DATABASE: -100009,
  /** 房间号错误 */
  ERR_SERVER_INFO_BAD_ROOMID: -100010,
  /** 场景或角色错误 */
  ERR_SERVER_INFO_BAD_SCENE_OR_ROLE: -100011,
  /** 房间号转换出错 */
  ERR_SERVER_INFO_ROOMID_EXCHANGE_FAILED: -100012,
  /** 房间号非法 */
  ERR_SERVER_INFO_STRGROUP_HAS_INVALID_CHARS: -100014,
  /** 非法SDKAppid */
  ERR_SERVER_INFO_LACK_SDKAPPID: -100015,
  /** 无效请求, 旧版 0x1 要求带 Token; ECDH 要求带 ECDH Publich Key; 两个都没有就按报错 */
  ERR_SERVER_INFO_INVALID: -100016,
  /** 生成公钥失败 */
  ERR_SERVER_INFO_ECDH_GET_KEY: -100017,
  /** 获取tinyid失败 */
  ERR_SERVER_INFO_ECDH_GET_TINYID: -100018,
  // Access 接口机
  /** token 过期 */
  ERR_SERVER_ACC_TOKEN_TIMEOUT: -101000,
  /** 签名错误 */
  ERR_SERVER_ACC_SIGN_ERROR: -101001,
  /** 签名超时 */
  ERR_SERVER_ACC_SIGN_TIMEOUT: -101002,
  /** 房间不存在 */
  ERR_SERVER_ACC_ROOM_NOT_EXIST: -101003,
  /** 后台房间标识 roomId 错误 */
  ERR_SERVER_ACC_ROOMID: -101004,
  /** 后台用户位置标识 locationId 错误 */
  ERR_SERVER_ACC_LOCATIONID: -101005,
  // center 服务器（信令和流控处理等任务）
  /** 后台错误 */
  ERR_SERVER_CENTER_SYSTEM_ERROR: -102000,
  /** 无效的房间 Id */
  ERR_SERVER_CENTER_INVALID_ROOMID: -102001,
  /** 创建房间失败 */
  ERR_SERVER_CENTER_CREATE_ROOM_FAILED: -102002,
  /** 签名错误 */
  ERR_SERVER_CENTER_SIGN_ERROR: -102003,
  /** 签名过期 */
  ERR_SERVER_CENTER_SIGN_TIMEOUT: -102004,
  /** 房间不存在 */
  ERR_SERVER_CENTER_ROOM_NOT_EXIST: -102005,
  /** 房间添加用户失败 */
  ERR_SERVER_CENTER_ADD_USER_FAILED: -102006,
  /** 查找用户失败 */
  ERR_SERVER_CENTER_FIND_USER_FAILED: -102007,
  /** 频繁切换终端 */
  ERR_SERVER_CENTER_SWITCH_TERMINATION_FREQUENTLY: -102008,
  /** locationid 错误 */
  ERR_SERVER_CENTER_LOCATION_NOT_EXIST: -102009,
  /** 没有权限创建房间 */
  ERR_SERVER_CENTER_NO_PRIVILEDGE_CREATE_ROOM: -102010,
  /** 没有权限进入房间 */
  ERR_SERVER_CENTER_NO_PRIVILEDGE_ENTER_ROOM: -102011,
  /** 辅路抢视频位、申请辅路请求类型参数错误 */
  ERR_SERVER_CENTER_INVALID_PARAMETER_SUB_VIDEO: -102012,
  /** 没有权限上视频 */
  ERR_SERVER_CENTER_NO_PRIVILEDGE_PUSH_VIDEO: -102013,
  /** 没有空闲路由表 */
  ERR_SERVER_CENTER_ROUTE_TABLE_ERROR: -102014,
  /** 当前用户没有上行辅路 */
  ERR_SERVER_CENTER_NOT_PUSH_SUB_VIDEO: -102017,
  /** 用户被删除状态 */
  ERR_SERVER_CENTER_USER_WAS_DELETED: -102018,
  /** 没有权限请求视频 */
  ERR_SERVER_CENTER_NO_PRIVILEDGE_REQUEST_VIDEO: -102019,
  /** 进房参数 bussInfo 错误 */
  ERR_SERVER_CENTER_INVALID_PARAMETER: -102023,
  /** 请求 I 帧未知 opType */
  ERR_SERVER_CENTER_I_FRAME_UNKNOW_TYPE: -102024,
  /** 请求 I 帧包格式错误 */
  ERR_SERVER_CENTER_I_FRAME_INVALID_PACKET: -102025,
  /** 请求 I 帧目标用户不存在 */
  ERR_SERVER_CENTER_I_FRAME_DEST_USER_NOT_EXIST: -102026,
  /** 请求 I 帧房间用户太多 */
  ERR_SERVER_CENTER_I_FRAME_ROOM_TOO_BIG: -102027,
  /** 请求 I 帧参数错误 */
  ERR_SERVER_CENTER_I_FRAME_RPS_INVALID_PARAMETER: -102028,
  /** 房间号非法 */
  ERR_SERVER_CENTER_INVALID_ROOM_ID: -102029,
  /** 房间号超过限制 */
  ERR_SERVER_CENTER_ROOM_ID_TOO_LONG: -102030,
  /** 房间满员 */
  ERR_SERVER_CENTER_ROOM_FULL: -102052,
  /** json串解析失败 */
  ERR_SERVER_CENTER_DECODE_JSON_FAIL: -102053,
  /** 未定义命令字 */
  ERR_SERVER_CENTER_UNKNOWN_SUB_CMD: -102054,
  /** 未定义角色 */
  ERR_SERVER_CENTER_INVALID_ROLE: -102055,
  /** 代理机超出限制 */
  ERR_SERVER_CENTER_REACH_PROXY_MAX: -102056,
  //add by sunlitwang begin
  /** 无法保存用户自定义recordId */
  ERR_SERVER_CENTER_RECORDID_STORE: -102057,
  /** Protobuf序列化错误 */
  ERR_SERVER_CENTER_PB_SERIALIZE: -102058,
  // https://cloud.tencent.com/document/product/269/1671#.E5.B8.90.E5.8F.B7.E7.B3.BB.E7.BB.9F , 帐号系统, 主要是70000 - 79999之间.
  // 在请求 token 过程中，出现账号错误，SSO 返回的错误码，原为正数，现将其转换为负数。
  /** sig 过期，请尝试重新生成。如果是刚生成，就过期，请检查有效期填写的是否过小，或者填的 0 */
  ERR_SERVER_SSO_SIG_EXPIRED: -70001,
  /** sig 校验失败，请确认下 sig 内容是否被截断，如缓冲区长度不够导致的内容截断 */
  ERR_SERVER_SSO_SIG_VERIFICATION_FAILED_1: -70003,
  /** sig 校验失败，请确认下 sig 内容是否被截断，如缓冲区长度不够导致的内容截断 */
  ERR_SERVER_SSO_SIG_VERIFICATION_FAILED_2: -70004,
  /** sig 校验失败，可用工具自行验证生成的 sig 是否正确 */
  ERR_SERVER_SSO_SIG_VERIFICATION_FAILED_3: -70005,
  /** sig 校验失败，可用工具自行验证生成的 sig 是否正确 */
  ERR_SERVER_SSO_SIG_VERIFICATION_FAILED_4: -70006,
  /** sig 校验失败，可用工具自行验证生成的 sig 是否正确 */
  ERR_SERVER_SSO_SIG_VERIFICATION_FAILED_5: -70007,
  /** sig 校验失败，可用工具自行验证生成的 sig 是否正确 */
  ERR_SERVER_SSO_SIG_VERIFICATION_FAILED_6: -70008,
  /** 用业务公钥验证 sig 失败，请确认生成的 usersig 使用的私钥和 sdkAppId 是否对应 */
  ERR_SERVER_SSO_SIG_VERIFICATION_FAILED_7: -70009,
  /** sig 校验失败，可用工具自行验证生成的 sig 是否正确 */
  ERR_SERVER_SSO_SIG_VERIFICATION_FAILED_8: -70010,
  /** sig 中 identifier 与请求时的 identifier 不匹配，请检查登录时填写的 identifier 与 sig 中的是否一致 */
  ERR_SERVER_SSO_SIG_VERIFICATION_ID_NOT_MATCH: -70013,
  /** sig 中 sdkAppId 与请求时的 sdkAppId 不匹配，请检查登录时填写的 sdkAppId 与 sig 中的是否一致 */
  ERR_SERVER_SSO_APPID_NOT_MATCH: -70014,
  /** 内部第三方票据验证超时，请重试，如多次重试不成功，请@TLS 帐号支持，QQ 3268519604 */
  ERR_SERVER_SSO_VERIFICATION_EXPIRED: -70017,
  /** 内部第三方票据验证超时，请重试，如多次重试不成功，请@TLS 帐号支持，QQ 3268519604 */
  ERR_SERVER_SSO_VERIFICATION_FAILED: -70018,
  /** sdkAppId 未找到，请确认是否已经在腾讯云上配置 */
  ERR_SERVER_SSO_APPID_NOT_FOUND: -70020,
  /** 帐号已被拉入黑名单，请联系 TLS 帐号支持 QQ 3268519604 */
  ERR_SERVER_SSO_ACCOUNT_IN_BLACKLIST: -70051,
  /** usersig 已经失效，请重新生成，再次尝试 */
  ERR_SERVER_SSO_SIG_INVALID: -70052,
  /** 安全原因被限制 */
  ERR_SERVER_SSO_LIMITED_BY_SECURITY: -70114,
  /** 登录状态无效，请使用 usersig 重新鉴权 */
  ERR_SERVER_SSO_INVALID_LOGIN_STATUS: -70221,
  /** sdkAppId 填写错误 */
  ERR_SERVER_SSO_APPID_ERROR: -70252,
  /** 票据校验失败，请检查各项参数是否正确 */
  ERR_SERVER_SSO_TICKET_VERIFICATION_FAILED: -70346,
  /** 票据因过期原因校验失败 */
  ERR_SERVER_SSO_TICKET_EXPIRED: -70347,
  /** 创建账号数量超过已购买预付费数量限制 */
  ERR_SERVER_SSO_ACCOUNT_EXCEED_PURCHASES: -70398,
  /** 服务器内部错误，请重试 */
  ERR_SERVER_SSO_INTERNAL_ERROR: -70500
};
/////////////////////////////////////////////////////////////////////////////////
//
//                     （二）错误码（警告）
//
/////////////////////////////////////////////////////////////////////////////////
/**
 * @memberof ErrorCode
 * @typedef 错误码（警告）
 * @description SDK 错误码（警告）对照表
 * | 符号 | 值 | 含义 |
 * |---|---|---|
 * |WARNING_HW_ENCODER_START_FAIL|1103|硬编码启动出现问题，自动切换到软编码|
 * |WARNING_VIDEO_ENCODER_SW_TO_HW|1107|当前 CPU 使用率太高，无法满足软件编码需求，自动切换到硬件编码|
 * |WARNING_INSUFFICIENT_CAPTURE_FPS|1108|摄像头采集帧率不足，部分自带美颜算法的 Android 手机上会出现|
 * |WARNING_SW_ENCODER_START_FAIL|1109|软编码启动失败|
 * |WARNING_REDUCE_CAPTURE_RESOLUTION|1110|摄像头采集分辨率被降低，以满足当前帧率和性能最优解。|
 * |WARNING_VIDEO_FRAME_DECODE_FAIL|2101|当前视频帧解码失败|
 * |WARNING_AUDIO_FRAME_DECODE_FAIL|2102|当前音频帧解码失败|
 * |WARNING_VIDEO_PLAY_LAG|2105|当前视频播放出现卡顿|
 * |WARNING_HW_DECODER_START_FAIL|2106|硬解启动失败，采用软解码|
 * |WARNING_VIDEO_DECODER_HW_TO_SW|2108|当前流硬解第一个 I 帧失败，SDK 自动切软解|
 * |WARNING_SW_DECODER_START_FAIL|2109|软解码器启动失败|
 * |WARNING_VIDEO_RENDER_FAIL|2110|视频渲染失败|
 * |WARNING_AUDIO_RECORDING_WRITE_FAIL|7001|音频录制写入文件失败|
 * |WARNING_ROOM_DISCONNECT|5101|网络断开连接|
 * |WARNING_IGNORE_UPSTREAM_FOR_AUDIENCE|6001|当前是观众角色，忽略上行音视频数据|
 */
exports.TXLiteAVError = TXLiteAVError;
var TXLiteAVWarning = {
  /** 硬编码启动出现问题，自动切换到软编码 */
  WARNING_HW_ENCODER_START_FAIL: 1103,
  /** 当前 CPU 使用率太高，无法满足软件编码需求，自动切换到硬件编码 */
  WARNING_VIDEO_ENCODER_SW_TO_HW: 1107,
  /** 摄像头采集帧率不足，部分自带美颜算法的 Android 手机上会出现 */
  WARNING_INSUFFICIENT_CAPTURE_FPS: 1108,
  /** 软编码启动失败 */
  WARNING_SW_ENCODER_START_FAIL: 1109,
  /** 摄像头采集分辨率被降低，以满足当前帧率和性能最优解。 */
  WARNING_REDUCE_CAPTURE_RESOLUTION: 1110,
  /** 当前视频帧解码失败 */
  WARNING_VIDEO_FRAME_DECODE_FAIL: 2101,
  /** 当前音频帧解码失败 */
  WARNING_AUDIO_FRAME_DECODE_FAIL: 2102,
  /** 当前视频播放出现卡顿 */
  WARNING_VIDEO_PLAY_LAG: 2105,
  /** 硬解启动失败，采用软解码 */
  WARNING_HW_DECODER_START_FAIL: 2106,
  /** 当前流硬解第一个 I 帧失败，SDK 自动切软解 */
  WARNING_VIDEO_DECODER_HW_TO_SW: 2108,
  /** 软解码器启动失败 */
  WARNING_SW_DECODER_START_FAIL: 2109,
  /** 视频渲染失败 */
  WARNING_VIDEO_RENDER_FAIL: 2110,
  /** 音频录制写入文件失败 */
  WARNING_AUDIO_RECORDING_WRITE_FAIL: 7001,
  /** 网络断开连接 */
  WARNING_ROOM_DISCONNECT: 5101,
  /** 当前是观众角色，忽略上行音视频数据 */
  WARNING_IGNORE_UPSTREAM_FOR_AUDIENCE: 6001,
  /** 网络状况不佳：上行带宽太小，上传数据受阻 */
  WARNING_NET_BUSY: 1101,
  /** 直播，网络断连, 已启动自动重连（自动重连连续失败超过三次会放弃） */
  WARNING_RTMP_SERVER_RECONNECT: 1102,
  /** 直播，网络断连, 已启动自动重连（自动重连连续失败超过三次会放弃） */
  WARNING_LIVE_STREAM_SERVER_RECONNECT: 2103,
  /** 网络来包不稳：可能是下行带宽不足，或由于主播端出流不均匀 */
  WARNING_RECV_DATA_LAG: 2104,
  /** 直播，DNS 解析失败 */
  WARNING_RTMP_DNS_FAIL: 3001,
  /** 直播，服务器连接失败 */
  WARNING_RTMP_SEVER_CONN_FAIL: 3002,
  /** 直播，与 RTMP 服务器握手失败 */
  WARNING_RTMP_SHAKE_FAIL: 3003,
  /** 直播，服务器主动断开 */
  WARNING_RTMP_SERVER_BREAK_CONNECT: 3004,
  /** 直播，RTMP 读/写失败，将会断开连接 */
  WARNING_RTMP_READ_WRITE_FAIL: 3005,
  /** 直播，RTMP 写失败（SDK 内部错误码，不会对外抛出） */
  WARNING_RTMP_WRITE_FAIL: 3006,
  /** 直播，RTMP 读失败（SDK 内部错误码，不会对外抛出） */
  WARNING_RTMP_READ_FAIL: 3007,
  /** 直播，超过30s 没有数据发送，主动断开连接 */
  WARNING_RTMP_NO_DATA: 3008,
  /** 直播，connect 服务器调用失败（SDK 内部错误码，不会对外抛出） */
  WARNING_PLAY_LIVE_STREAM_INFO_CONNECT_FAIL: 3009,
  /** 直播，连接失败，该流地址无视频（SDK 内部错误码，不会对外抛出） */
  WARNING_NO_STEAM_SOURCE_FAIL: 3010,
  /** 网络断连，已启动自动重连 */
  WARNING_ROOM_RECONNECT: 5102,
  /** 网络状况不佳：上行带宽太小，上传数据受阻 */
  WARNING_ROOM_NET_BUSY: 5103
};
/////////////////////////////////////////////////////////////////////////////////
//
//                     （三）JS 封装层抛出的异常（严重）
//
/////////////////////////////////////////////////////////////////////////////////
/**
 * @namespace ErrorCode
 * @description 错误码
 */
exports.TXLiteAVWarning = TXLiteAVWarning;
var TXLiteJSError = {
  /**
   * 未知错误
   * @default 0xFFFF
   * @memberof module:ErrorCode
   */
  UNKNOWN: 0xffff,
  /**
   * 无效参数
   *
   * @default 0x1000
   * @memberof module:ErrorCode
   */
  INVALID_PARAMETER: 0x1000,
  /**
   * 非法操作
   *
   * @default 0x1001
   * @memberof module:ErrorCode
   */
  INVALID_OPERATION: 0x1001
};
exports.TXLiteJSError = TXLiteJSError;
var getErrorName = function getErrorName(code) {
  for (var key in TXLiteJSError) {
    if (TXLiteJSError[key] === code) {
      return key;
    }
  }
  return 'UNKNOWN';
};
/**
 * TrtcError 错误对象<br>
 * @extends Error
 * @namespace ErrorCode
 */
var TrtcError = /*#__PURE__*/function (_Error) {
  (0, _inherits2.default)(TrtcError, _Error);
  var _super = _createSuper(TrtcError);
  function TrtcError(_ref) {
    var _this;
    var _ref$code = _ref.code,
      code = _ref$code === void 0 ? TXLiteJSError.UNKNOWN : _ref$code,
      message = _ref.message,
      extraInfo = _ref.extraInfo;
    (0, _classCallCheck2.default)(this, TrtcError);
    if (extraInfo) {
      var tempError = {
        errCode: code,
        errMsg: message,
        extraInfo: Object.assign(Object.assign({}, extraInfo), {
          errCodeUrl: _constants.errorCodeUrl
        })
      };
      _this = _super.call(this, JSON.stringify(tempError));
    } else {
      _this = _super.call(this, message + " <".concat(getErrorName(code), " 0x").concat(code.toString(16), ">. Refer to: ").concat(_constants.errorCodeUrl));
    }
    _this.errCode = code;
    _this.errMsg = message;
    _this.extraInfo = Object.assign(Object.assign({}, extraInfo), {
      errCodeUrl: _constants.errorCodeUrl
    });
    return (0, _possibleConstructorReturn2.default)(_this);
  }
  /**
   * 获取错误码<br>
   * 详细错误码列表参见 {@link module:ErrorCode ErrorCode}
   * @memberof TrtcError
   */
  (0, _createClass2.default)(TrtcError, [{
    key: "getCode",
    value: function getCode() {
      return this.errCode;
    }
  }]);
  return TrtcError;
}( /*#__PURE__*/(0, _wrapNativeSuper2.default)(Error));
var _default = TrtcError;
exports.default = _default;
function generateError_(error) {
  var code = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : TXLiteJSError.UNKNOWN;
  var extraInfo = arguments.length > 2 ? arguments[2] : undefined;
  return new TrtcError({
    code: error.code || code,
    message: "".concat(_constants.NAME.LOG_PREFIX).concat(error.message),
    extraInfo: extraInfo
  });
}
;

/***/ }),
/* 57 */
/*!*********************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/inherits.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var setPrototypeOf = __webpack_require__(/*! ./setPrototypeOf.js */ 16);
function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }
  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  Object.defineProperty(subClass, "prototype", {
    writable: false
  });
  if (superClass) setPrototypeOf(subClass, superClass);
}
module.exports = _inherits, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 58 */
/*!**************************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__(/*! ./typeof.js */ 13)["default"];
var assertThisInitialized = __webpack_require__(/*! ./assertThisInitialized.js */ 59);
function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  } else if (call !== void 0) {
    throw new TypeError("Derived constructors may only return object or undefined");
  }
  return assertThisInitialized(self);
}
module.exports = _possibleConstructorReturn, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 59 */
/*!**********************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/assertThisInitialized.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }
  return self;
}
module.exports = _assertThisInitialized, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 60 */
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/getPrototypeOf.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _getPrototypeOf(o) {
  module.exports = _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  }, module.exports.__esModule = true, module.exports["default"] = module.exports;
  return _getPrototypeOf(o);
}
module.exports = _getPrototypeOf, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 61 */
/*!****************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/wrapNativeSuper.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var getPrototypeOf = __webpack_require__(/*! ./getPrototypeOf.js */ 60);
var setPrototypeOf = __webpack_require__(/*! ./setPrototypeOf.js */ 16);
var isNativeFunction = __webpack_require__(/*! ./isNativeFunction.js */ 62);
var construct = __webpack_require__(/*! ./construct.js */ 15);
function _wrapNativeSuper(Class) {
  var _cache = typeof Map === "function" ? new Map() : undefined;
  module.exports = _wrapNativeSuper = function _wrapNativeSuper(Class) {
    if (Class === null || !isNativeFunction(Class)) return Class;
    if (typeof Class !== "function") {
      throw new TypeError("Super expression must either be null or a function");
    }
    if (typeof _cache !== "undefined") {
      if (_cache.has(Class)) return _cache.get(Class);
      _cache.set(Class, Wrapper);
    }
    function Wrapper() {
      return construct(Class, arguments, getPrototypeOf(this).constructor);
    }
    Wrapper.prototype = Object.create(Class.prototype, {
      constructor: {
        value: Wrapper,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
    return setPrototypeOf(Wrapper, Class);
  }, module.exports.__esModule = true, module.exports["default"] = module.exports;
  return _wrapNativeSuper(Class);
}
module.exports = _wrapNativeSuper, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 62 */
/*!*****************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/isNativeFunction.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _isNativeFunction(fn) {
  return Function.toString.call(fn).indexOf("[native code]") !== -1;
}
module.exports = _isNativeFunction, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 63 */
/*!*************************************************************!*\
  !*** E:/公司项目/e考监控/Api-Example/debug/GenerateTestUserSig.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ 4);
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _libGenerateTestUsersigEsMin = _interopRequireDefault(__webpack_require__(/*! ./lib-generate-test-usersig-es.min.js */ 64));
/**
 * 腾讯云 SDKAppId，需要替换为您自己账号下的 SDKAppId。
 *
 * 进入腾讯云实时音视频[控制台](https://console.cloud.tencent.com/rav ) 创建应用，即可看到 SDKAppId，
 * 它是腾讯云用于区分客户的唯一标识。
 */

var SDKAPPID = 1400790383;
/**
 * 签名过期时间，建议不要设置的过短
 * <p>
 * 时间单位：秒
 * 默认时间：7 x 24 x 60 x 60 = 604800 = 7 天
 */

var EXPIRETIME = 604800;
/**
 * 计算签名用的加密密钥，获取步骤如下：
 *
 * step1. 进入腾讯云实时音视频[控制台](https://console.cloud.tencent.com/rav )，如果还没有应用就创建一个，
 * step2. 单击“应用配置”进入基础配置页面，并进一步找到“帐号体系集成”部分。
 * step3. 点击“查看密钥”按钮，就可以看到计算 UserSig 使用的加密的密钥了，请将其拷贝并复制到如下的变量中
 *
 * 注意：该方案仅适用于调试Demo，正式上线前请将 UserSig 计算代码和密钥迁移到您的后台服务器上，以避免加密密钥泄露导致的流量盗用。
 * 文档：https://cloud.tencent.com/document/product/647/17275#Server
 */

var SECRETKEY = '9c8719393bcae45adde034cf282c01b8b8c8ea31a3bb4431bff49f8449ba40d7';
/*
 * Module:   GenerateTestUserSig
 *
 * Function: 用于生成测试用的 UserSig，UserSig 是腾讯云为其云服务设计的一种安全保护签名。
 *           其计算方法是对 SDKAppID、UserID 和 EXPIRETIME 进行加密，加密算法为 HMAC-SHA256。
 *
 * Attention: 请不要将如下代码发布到您的线上正式版本的 App 中，原因如下：
 *
 *            本文件中的代码虽然能够正确计算出 UserSig，但仅适合快速调通 SDK 的基本功能，不适合线上产品，
 *            这是因为客户端代码中的 SECRETKEY 很容易被反编译逆向破解，尤其是 Web 端的代码被破解的难度几乎为零。
 *            一旦您的密钥泄露，攻击者就可以计算出正确的 UserSig 来盗用您的腾讯云流量。
 *
 *            正确的做法是将 UserSig 的计算代码和加密密钥放在您的业务服务器上，然后由 App 按需向您的服务器获取实时算出的 UserSig。
 *            由于破解服务器的成本要高于破解客户端 App，所以服务器计算的方案能够更好地保护您的加密密钥。
 *
 * Reference：https://cloud.tencent.com/document/product/647/17275#Server
 */

function genTestUserSig(userID) {
  var generator = new _libGenerateTestUsersigEsMin.default(SDKAPPID, SECRETKEY, EXPIRETIME);
  var userSig = generator.genTestUserSig(userID);
  return {
    sdkAppId: SDKAPPID,
    userSig: userSig
  };
}

// module.exports = {
//   genTestUserSig,
// };

// HBuilder 选择 vue3 时, 上面的打包无法通过 import 进行引入
var _default = genTestUserSig;
exports.default = _default;

/***/ }),
/* 64 */
/*!**************************************************************************!*\
  !*** E:/公司项目/e考监控/Api-Example/debug/lib-generate-test-usersig-es.min.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ 4);
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _classCallCheck2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ 23));
var _createClass2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/createClass */ 24));
var _typeof2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/typeof */ 13));
/*eslint-disable*/
var e = "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {},
  t = [],
  r = [],
  n = "undefined" != typeof Uint8Array ? Uint8Array : Array,
  i = !1;
function o() {
  i = !0;
  for (var e = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", n = 0, o = e.length; n < o; ++n) {
    t[n] = e[n], r[e.charCodeAt(n)] = n;
  }
  r["-".charCodeAt(0)] = 62, r["_".charCodeAt(0)] = 63;
}
function a(e, r, n) {
  for (var i, o, a = [], s = r; s < n; s += 3) {
    i = (e[s] << 16) + (e[s + 1] << 8) + e[s + 2], a.push(t[(o = i) >> 18 & 63] + t[o >> 12 & 63] + t[o >> 6 & 63] + t[63 & o]);
  }
  return a.join("");
}
function s(e) {
  var r;
  i || o();
  for (var n = e.length, s = n % 3, h = "", l = [], f = 0, c = n - s; f < c; f += 16383) {
    l.push(a(e, f, f + 16383 > c ? c : f + 16383));
  }
  return 1 === s ? (r = e[n - 1], h += t[r >> 2], h += t[r << 4 & 63], h += "==") : 2 === s && (r = (e[n - 2] << 8) + e[n - 1], h += t[r >> 10], h += t[r >> 4 & 63], h += t[r << 2 & 63], h += "="), l.push(h), l.join("");
}
function h(e, t, r, n, i) {
  var o,
    a,
    s = 8 * i - n - 1,
    h = (1 << s) - 1,
    l = h >> 1,
    f = -7,
    c = r ? i - 1 : 0,
    u = r ? -1 : 1,
    d = e[t + c];
  for (c += u, o = d & (1 << -f) - 1, d >>= -f, f += s; f > 0; o = 256 * o + e[t + c], c += u, f -= 8) {
    ;
  }
  for (a = o & (1 << -f) - 1, o >>= -f, f += n; f > 0; a = 256 * a + e[t + c], c += u, f -= 8) {
    ;
  }
  if (0 === o) o = 1 - l;else {
    if (o === h) return a ? NaN : 1 / 0 * (d ? -1 : 1);
    a += Math.pow(2, n), o -= l;
  }
  return (d ? -1 : 1) * a * Math.pow(2, o - n);
}
function l(e, t, r, n, i, o) {
  var a,
    s,
    h,
    l = 8 * o - i - 1,
    f = (1 << l) - 1,
    c = f >> 1,
    u = 23 === i ? Math.pow(2, -24) - Math.pow(2, -77) : 0,
    d = n ? 0 : o - 1,
    p = n ? 1 : -1,
    _ = t < 0 || 0 === t && 1 / t < 0 ? 1 : 0;
  for (t = Math.abs(t), isNaN(t) || t === 1 / 0 ? (s = isNaN(t) ? 1 : 0, a = f) : (a = Math.floor(Math.log(t) / Math.LN2), t * (h = Math.pow(2, -a)) < 1 && (a--, h *= 2), (t += a + c >= 1 ? u / h : u * Math.pow(2, 1 - c)) * h >= 2 && (a++, h /= 2), a + c >= f ? (s = 0, a = f) : a + c >= 1 ? (s = (t * h - 1) * Math.pow(2, i), a += c) : (s = t * Math.pow(2, c - 1) * Math.pow(2, i), a = 0)); i >= 8; e[r + d] = 255 & s, d += p, s /= 256, i -= 8) {
    ;
  }
  for (a = a << i | s, l += i; l > 0; e[r + d] = 255 & a, d += p, a /= 256, l -= 8) {
    ;
  }
  e[r + d - p] |= 128 * _;
}
var f = {}.toString,
  c = Array.isArray || function (e) {
    return "[object Array]" == f.call(e);
  };
function u() {
  return p.TYPED_ARRAY_SUPPORT ? 2147483647 : 1073741823;
}
function d(e, t) {
  if (u() < t) throw new RangeError("Invalid typed array length");
  return p.TYPED_ARRAY_SUPPORT ? (e = new Uint8Array(t)).__proto__ = p.prototype : (null === e && (e = new p(t)), e.length = t), e;
}
function p(e, t, r) {
  if (!(p.TYPED_ARRAY_SUPPORT || this instanceof p)) return new p(e, t, r);
  if ("number" == typeof e) {
    if ("string" == typeof t) throw new Error("If encoding is specified then the first argument must be a string");
    return v(this, e);
  }
  return _(this, e, t, r);
}
function _(e, t, r, n) {
  if ("number" == typeof t) throw new TypeError('"value" argument must not be a number');
  return "undefined" != typeof ArrayBuffer && t instanceof ArrayBuffer ? function (e, t, r, n) {
    if (t.byteLength, r < 0 || t.byteLength < r) throw new RangeError("'offset' is out of bounds");
    if (t.byteLength < r + (n || 0)) throw new RangeError("'length' is out of bounds");
    t = void 0 === r && void 0 === n ? new Uint8Array(t) : void 0 === n ? new Uint8Array(t, r) : new Uint8Array(t, r, n);
    p.TYPED_ARRAY_SUPPORT ? (e = t).__proto__ = p.prototype : e = w(e, t);
    return e;
  }(e, t, r, n) : "string" == typeof t ? function (e, t, r) {
    "string" == typeof r && "" !== r || (r = "utf8");
    if (!p.isEncoding(r)) throw new TypeError('"encoding" must be a valid string encoding');
    var n = 0 | m(t, r),
      i = (e = d(e, n)).write(t, r);
    i !== n && (e = e.slice(0, i));
    return e;
  }(e, t, r) : function (e, t) {
    if (y(t)) {
      var r = 0 | b(t.length);
      return 0 === (e = d(e, r)).length ? e : (t.copy(e, 0, 0, r), e);
    }
    if (t) {
      if ("undefined" != typeof ArrayBuffer && t.buffer instanceof ArrayBuffer || "length" in t) return "number" != typeof t.length || (n = t.length) != n ? d(e, 0) : w(e, t);
      if ("Buffer" === t.type && c(t.data)) return w(e, t.data);
    }
    var n;
    throw new TypeError("First argument must be a string, Buffer, ArrayBuffer, Array, or array-like object.");
  }(e, t);
}
function g(e) {
  if ("number" != typeof e) throw new TypeError('"size" argument must be a number');
  if (e < 0) throw new RangeError('"size" argument must not be negative');
}
function v(e, t) {
  if (g(t), e = d(e, t < 0 ? 0 : 0 | b(t)), !p.TYPED_ARRAY_SUPPORT) for (var r = 0; r < t; ++r) {
    e[r] = 0;
  }
  return e;
}
function w(e, t) {
  var r = t.length < 0 ? 0 : 0 | b(t.length);
  e = d(e, r);
  for (var n = 0; n < r; n += 1) {
    e[n] = 255 & t[n];
  }
  return e;
}
function b(e) {
  if (e >= u()) throw new RangeError("Attempt to allocate Buffer larger than maximum size: 0x" + u().toString(16) + " bytes");
  return 0 | e;
}
function y(e) {
  return !(null == e || !e._isBuffer);
}
function m(e, t) {
  if (y(e)) return e.length;
  if ("undefined" != typeof ArrayBuffer && "function" == typeof ArrayBuffer.isView && (ArrayBuffer.isView(e) || e instanceof ArrayBuffer)) return e.byteLength;
  "string" != typeof e && (e = "" + e);
  var r = e.length;
  if (0 === r) return 0;
  for (var n = !1;;) {
    switch (t) {
      case "ascii":
      case "latin1":
      case "binary":
        return r;
      case "utf8":
      case "utf-8":
      case void 0:
        return q(e).length;
      case "ucs2":
      case "ucs-2":
      case "utf16le":
      case "utf-16le":
        return 2 * r;
      case "hex":
        return r >>> 1;
      case "base64":
        return V(e).length;
      default:
        if (n) return q(e).length;
        t = ("" + t).toLowerCase(), n = !0;
    }
  }
}
function k(e, t, r) {
  var n = !1;
  if ((void 0 === t || t < 0) && (t = 0), t > this.length) return "";
  if ((void 0 === r || r > this.length) && (r = this.length), r <= 0) return "";
  if ((r >>>= 0) <= (t >>>= 0)) return "";
  for (e || (e = "utf8");;) {
    switch (e) {
      case "hex":
        return O(this, t, r);
      case "utf8":
      case "utf-8":
        return C(this, t, r);
      case "ascii":
        return I(this, t, r);
      case "latin1":
      case "binary":
        return P(this, t, r);
      case "base64":
        return M(this, t, r);
      case "ucs2":
      case "ucs-2":
      case "utf16le":
      case "utf-16le":
        return U(this, t, r);
      default:
        if (n) throw new TypeError("Unknown encoding: " + e);
        e = (e + "").toLowerCase(), n = !0;
    }
  }
}
function E(e, t, r) {
  var n = e[t];
  e[t] = e[r], e[r] = n;
}
function S(e, t, r, n, i) {
  if (0 === e.length) return -1;
  if ("string" == typeof r ? (n = r, r = 0) : r > 2147483647 ? r = 2147483647 : r < -2147483648 && (r = -2147483648), r = +r, isNaN(r) && (r = i ? 0 : e.length - 1), r < 0 && (r = e.length + r), r >= e.length) {
    if (i) return -1;
    r = e.length - 1;
  } else if (r < 0) {
    if (!i) return -1;
    r = 0;
  }
  if ("string" == typeof t && (t = p.from(t, n)), y(t)) return 0 === t.length ? -1 : x(e, t, r, n, i);
  if ("number" == typeof t) return t &= 255, p.TYPED_ARRAY_SUPPORT && "function" == typeof Uint8Array.prototype.indexOf ? i ? Uint8Array.prototype.indexOf.call(e, t, r) : Uint8Array.prototype.lastIndexOf.call(e, t, r) : x(e, [t], r, n, i);
  throw new TypeError("val must be string, number or Buffer");
}
function x(e, t, r, n, i) {
  var o,
    a = 1,
    s = e.length,
    h = t.length;
  if (void 0 !== n && ("ucs2" === (n = String(n).toLowerCase()) || "ucs-2" === n || "utf16le" === n || "utf-16le" === n)) {
    if (e.length < 2 || t.length < 2) return -1;
    a = 2, s /= 2, h /= 2, r /= 2;
  }
  function l(e, t) {
    return 1 === a ? e[t] : e.readUInt16BE(t * a);
  }
  if (i) {
    var f = -1;
    for (o = r; o < s; o++) {
      if (l(e, o) === l(t, -1 === f ? 0 : o - f)) {
        if (-1 === f && (f = o), o - f + 1 === h) return f * a;
      } else -1 !== f && (o -= o - f), f = -1;
    }
  } else for (r + h > s && (r = s - h), o = r; o >= 0; o--) {
    for (var c = !0, u = 0; u < h; u++) {
      if (l(e, o + u) !== l(t, u)) {
        c = !1;
        break;
      }
    }
    if (c) return o;
  }
  return -1;
}
function R(e, t, r, n) {
  r = Number(r) || 0;
  var i = e.length - r;
  n ? (n = Number(n)) > i && (n = i) : n = i;
  var o = t.length;
  if (o % 2 != 0) throw new TypeError("Invalid hex string");
  n > o / 2 && (n = o / 2);
  for (var a = 0; a < n; ++a) {
    var s = parseInt(t.substr(2 * a, 2), 16);
    if (isNaN(s)) return a;
    e[r + a] = s;
  }
  return a;
}
function A(e, t, r, n) {
  return G(q(t, e.length - r), e, r, n);
}
function B(e, t, r, n) {
  return G(function (e) {
    for (var t = [], r = 0; r < e.length; ++r) {
      t.push(255 & e.charCodeAt(r));
    }
    return t;
  }(t), e, r, n);
}
function z(e, t, r, n) {
  return B(e, t, r, n);
}
function L(e, t, r, n) {
  return G(V(t), e, r, n);
}
function T(e, t, r, n) {
  return G(function (e, t) {
    for (var r, n, i, o = [], a = 0; a < e.length && !((t -= 2) < 0); ++a) {
      r = e.charCodeAt(a), n = r >> 8, i = r % 256, o.push(i), o.push(n);
    }
    return o;
  }(t, e.length - r), e, r, n);
}
function M(e, t, r) {
  return 0 === t && r === e.length ? s(e) : s(e.slice(t, r));
}
function C(e, t, r) {
  r = Math.min(e.length, r);
  for (var n = [], i = t; i < r;) {
    var o,
      a,
      s,
      h,
      l = e[i],
      f = null,
      c = l > 239 ? 4 : l > 223 ? 3 : l > 191 ? 2 : 1;
    if (i + c <= r) switch (c) {
      case 1:
        l < 128 && (f = l);
        break;
      case 2:
        128 == (192 & (o = e[i + 1])) && (h = (31 & l) << 6 | 63 & o) > 127 && (f = h);
        break;
      case 3:
        o = e[i + 1], a = e[i + 2], 128 == (192 & o) && 128 == (192 & a) && (h = (15 & l) << 12 | (63 & o) << 6 | 63 & a) > 2047 && (h < 55296 || h > 57343) && (f = h);
        break;
      case 4:
        o = e[i + 1], a = e[i + 2], s = e[i + 3], 128 == (192 & o) && 128 == (192 & a) && 128 == (192 & s) && (h = (15 & l) << 18 | (63 & o) << 12 | (63 & a) << 6 | 63 & s) > 65535 && h < 1114112 && (f = h);
    }
    null === f ? (f = 65533, c = 1) : f > 65535 && (f -= 65536, n.push(f >>> 10 & 1023 | 55296), f = 56320 | 1023 & f), n.push(f), i += c;
  }
  return function (e) {
    var t = e.length;
    if (t <= D) return String.fromCharCode.apply(String, e);
    var r = "",
      n = 0;
    for (; n < t;) {
      r += String.fromCharCode.apply(String, e.slice(n, n += D));
    }
    return r;
  }(n);
}
p.TYPED_ARRAY_SUPPORT = void 0 === e.TYPED_ARRAY_SUPPORT || e.TYPED_ARRAY_SUPPORT, p.poolSize = 8192, p._augment = function (e) {
  return e.__proto__ = p.prototype, e;
}, p.from = function (e, t, r) {
  return _(null, e, t, r);
}, p.TYPED_ARRAY_SUPPORT && (p.prototype.__proto__ = Uint8Array.prototype, p.__proto__ = Uint8Array), p.alloc = function (e, t, r) {
  return function (e, t, r, n) {
    return g(t), t <= 0 ? d(e, t) : void 0 !== r ? "string" == typeof n ? d(e, t).fill(r, n) : d(e, t).fill(r) : d(e, t);
  }(null, e, t, r);
}, p.allocUnsafe = function (e) {
  return v(null, e);
}, p.allocUnsafeSlow = function (e) {
  return v(null, e);
}, p.isBuffer = $, p.compare = function (e, t) {
  if (!y(e) || !y(t)) throw new TypeError("Arguments must be Buffers");
  if (e === t) return 0;
  for (var r = e.length, n = t.length, i = 0, o = Math.min(r, n); i < o; ++i) {
    if (e[i] !== t[i]) {
      r = e[i], n = t[i];
      break;
    }
  }
  return r < n ? -1 : n < r ? 1 : 0;
}, p.isEncoding = function (e) {
  switch (String(e).toLowerCase()) {
    case "hex":
    case "utf8":
    case "utf-8":
    case "ascii":
    case "latin1":
    case "binary":
    case "base64":
    case "ucs2":
    case "ucs-2":
    case "utf16le":
    case "utf-16le":
      return !0;
    default:
      return !1;
  }
}, p.concat = function (e, t) {
  if (!c(e)) throw new TypeError('"list" argument must be an Array of Buffers');
  if (0 === e.length) return p.alloc(0);
  var r;
  if (void 0 === t) for (t = 0, r = 0; r < e.length; ++r) {
    t += e[r].length;
  }
  var n = p.allocUnsafe(t),
    i = 0;
  for (r = 0; r < e.length; ++r) {
    var o = e[r];
    if (!y(o)) throw new TypeError('"list" argument must be an Array of Buffers');
    o.copy(n, i), i += o.length;
  }
  return n;
}, p.byteLength = m, p.prototype._isBuffer = !0, p.prototype.swap16 = function () {
  var e = this.length;
  if (e % 2 != 0) throw new RangeError("Buffer size must be a multiple of 16-bits");
  for (var t = 0; t < e; t += 2) {
    E(this, t, t + 1);
  }
  return this;
}, p.prototype.swap32 = function () {
  var e = this.length;
  if (e % 4 != 0) throw new RangeError("Buffer size must be a multiple of 32-bits");
  for (var t = 0; t < e; t += 4) {
    E(this, t, t + 3), E(this, t + 1, t + 2);
  }
  return this;
}, p.prototype.swap64 = function () {
  var e = this.length;
  if (e % 8 != 0) throw new RangeError("Buffer size must be a multiple of 64-bits");
  for (var t = 0; t < e; t += 8) {
    E(this, t, t + 7), E(this, t + 1, t + 6), E(this, t + 2, t + 5), E(this, t + 3, t + 4);
  }
  return this;
}, p.prototype.toString = function () {
  var e = 0 | this.length;
  return 0 === e ? "" : 0 === arguments.length ? C(this, 0, e) : k.apply(this, arguments);
}, p.prototype.equals = function (e) {
  if (!y(e)) throw new TypeError("Argument must be a Buffer");
  return this === e || 0 === p.compare(this, e);
}, p.prototype.inspect = function () {
  var e = "";
  return this.length > 0 && (e = this.toString("hex", 0, 50).match(/.{2}/g).join(" "), this.length > 50 && (e += " ... ")), "<Buffer " + e + ">";
}, p.prototype.compare = function (e, t, r, n, i) {
  if (!y(e)) throw new TypeError("Argument must be a Buffer");
  if (void 0 === t && (t = 0), void 0 === r && (r = e ? e.length : 0), void 0 === n && (n = 0), void 0 === i && (i = this.length), t < 0 || r > e.length || n < 0 || i > this.length) throw new RangeError("out of range index");
  if (n >= i && t >= r) return 0;
  if (n >= i) return -1;
  if (t >= r) return 1;
  if (this === e) return 0;
  for (var o = (i >>>= 0) - (n >>>= 0), a = (r >>>= 0) - (t >>>= 0), s = Math.min(o, a), h = this.slice(n, i), l = e.slice(t, r), f = 0; f < s; ++f) {
    if (h[f] !== l[f]) {
      o = h[f], a = l[f];
      break;
    }
  }
  return o < a ? -1 : a < o ? 1 : 0;
}, p.prototype.includes = function (e, t, r) {
  return -1 !== this.indexOf(e, t, r);
}, p.prototype.indexOf = function (e, t, r) {
  return S(this, e, t, r, !0);
}, p.prototype.lastIndexOf = function (e, t, r) {
  return S(this, e, t, r, !1);
}, p.prototype.write = function (e, t, r, n) {
  if (void 0 === t) n = "utf8", r = this.length, t = 0;else if (void 0 === r && "string" == typeof t) n = t, r = this.length, t = 0;else {
    if (!isFinite(t)) throw new Error("Buffer.write(string, encoding, offset[, length]) is no longer supported");
    t |= 0, isFinite(r) ? (r |= 0, void 0 === n && (n = "utf8")) : (n = r, r = void 0);
  }
  var i = this.length - t;
  if ((void 0 === r || r > i) && (r = i), e.length > 0 && (r < 0 || t < 0) || t > this.length) throw new RangeError("Attempt to write outside buffer bounds");
  n || (n = "utf8");
  for (var o = !1;;) {
    switch (n) {
      case "hex":
        return R(this, e, t, r);
      case "utf8":
      case "utf-8":
        return A(this, e, t, r);
      case "ascii":
        return B(this, e, t, r);
      case "latin1":
      case "binary":
        return z(this, e, t, r);
      case "base64":
        return L(this, e, t, r);
      case "ucs2":
      case "ucs-2":
      case "utf16le":
      case "utf-16le":
        return T(this, e, t, r);
      default:
        if (o) throw new TypeError("Unknown encoding: " + n);
        n = ("" + n).toLowerCase(), o = !0;
    }
  }
}, p.prototype.toJSON = function () {
  return {
    type: "Buffer",
    data: Array.prototype.slice.call(this._arr || this, 0)
  };
};
var D = 4096;
function I(e, t, r) {
  var n = "";
  r = Math.min(e.length, r);
  for (var i = t; i < r; ++i) {
    n += String.fromCharCode(127 & e[i]);
  }
  return n;
}
function P(e, t, r) {
  var n = "";
  r = Math.min(e.length, r);
  for (var i = t; i < r; ++i) {
    n += String.fromCharCode(e[i]);
  }
  return n;
}
function O(e, t, r) {
  var n = e.length;
  (!t || t < 0) && (t = 0), (!r || r < 0 || r > n) && (r = n);
  for (var i = "", o = t; o < r; ++o) {
    i += X(e[o]);
  }
  return i;
}
function U(e, t, r) {
  for (var n = e.slice(t, r), i = "", o = 0; o < n.length; o += 2) {
    i += String.fromCharCode(n[o] + 256 * n[o + 1]);
  }
  return i;
}
function H(e, t, r) {
  if (e % 1 != 0 || e < 0) throw new RangeError("offset is not uint");
  if (e + t > r) throw new RangeError("Trying to access beyond buffer length");
}
function F(e, t, r, n, i, o) {
  if (!y(e)) throw new TypeError('"buffer" argument must be a Buffer instance');
  if (t > i || t < o) throw new RangeError('"value" argument is out of bounds');
  if (r + n > e.length) throw new RangeError("Index out of range");
}
function N(e, t, r, n) {
  t < 0 && (t = 65535 + t + 1);
  for (var i = 0, o = Math.min(e.length - r, 2); i < o; ++i) {
    e[r + i] = (t & 255 << 8 * (n ? i : 1 - i)) >>> 8 * (n ? i : 1 - i);
  }
}
function Z(e, t, r, n) {
  t < 0 && (t = 4294967295 + t + 1);
  for (var i = 0, o = Math.min(e.length - r, 4); i < o; ++i) {
    e[r + i] = t >>> 8 * (n ? i : 3 - i) & 255;
  }
}
function j(e, t, r, n, i, o) {
  if (r + n > e.length) throw new RangeError("Index out of range");
  if (r < 0) throw new RangeError("Index out of range");
}
function W(e, t, r, n, i) {
  return i || j(e, 0, r, 4), l(e, t, r, n, 23, 4), r + 4;
}
function Y(e, t, r, n, i) {
  return i || j(e, 0, r, 8), l(e, t, r, n, 52, 8), r + 8;
}
p.prototype.slice = function (e, t) {
  var r,
    n = this.length;
  if ((e = ~~e) < 0 ? (e += n) < 0 && (e = 0) : e > n && (e = n), (t = void 0 === t ? n : ~~t) < 0 ? (t += n) < 0 && (t = 0) : t > n && (t = n), t < e && (t = e), p.TYPED_ARRAY_SUPPORT) (r = this.subarray(e, t)).__proto__ = p.prototype;else {
    var i = t - e;
    r = new p(i, void 0);
    for (var o = 0; o < i; ++o) {
      r[o] = this[o + e];
    }
  }
  return r;
}, p.prototype.readUIntLE = function (e, t, r) {
  e |= 0, t |= 0, r || H(e, t, this.length);
  for (var n = this[e], i = 1, o = 0; ++o < t && (i *= 256);) {
    n += this[e + o] * i;
  }
  return n;
}, p.prototype.readUIntBE = function (e, t, r) {
  e |= 0, t |= 0, r || H(e, t, this.length);
  for (var n = this[e + --t], i = 1; t > 0 && (i *= 256);) {
    n += this[e + --t] * i;
  }
  return n;
}, p.prototype.readUInt8 = function (e, t) {
  return t || H(e, 1, this.length), this[e];
}, p.prototype.readUInt16LE = function (e, t) {
  return t || H(e, 2, this.length), this[e] | this[e + 1] << 8;
}, p.prototype.readUInt16BE = function (e, t) {
  return t || H(e, 2, this.length), this[e] << 8 | this[e + 1];
}, p.prototype.readUInt32LE = function (e, t) {
  return t || H(e, 4, this.length), (this[e] | this[e + 1] << 8 | this[e + 2] << 16) + 16777216 * this[e + 3];
}, p.prototype.readUInt32BE = function (e, t) {
  return t || H(e, 4, this.length), 16777216 * this[e] + (this[e + 1] << 16 | this[e + 2] << 8 | this[e + 3]);
}, p.prototype.readIntLE = function (e, t, r) {
  e |= 0, t |= 0, r || H(e, t, this.length);
  for (var n = this[e], i = 1, o = 0; ++o < t && (i *= 256);) {
    n += this[e + o] * i;
  }
  return n >= (i *= 128) && (n -= Math.pow(2, 8 * t)), n;
}, p.prototype.readIntBE = function (e, t, r) {
  e |= 0, t |= 0, r || H(e, t, this.length);
  for (var n = t, i = 1, o = this[e + --n]; n > 0 && (i *= 256);) {
    o += this[e + --n] * i;
  }
  return o >= (i *= 128) && (o -= Math.pow(2, 8 * t)), o;
}, p.prototype.readInt8 = function (e, t) {
  return t || H(e, 1, this.length), 128 & this[e] ? -1 * (255 - this[e] + 1) : this[e];
}, p.prototype.readInt16LE = function (e, t) {
  t || H(e, 2, this.length);
  var r = this[e] | this[e + 1] << 8;
  return 32768 & r ? 4294901760 | r : r;
}, p.prototype.readInt16BE = function (e, t) {
  t || H(e, 2, this.length);
  var r = this[e + 1] | this[e] << 8;
  return 32768 & r ? 4294901760 | r : r;
}, p.prototype.readInt32LE = function (e, t) {
  return t || H(e, 4, this.length), this[e] | this[e + 1] << 8 | this[e + 2] << 16 | this[e + 3] << 24;
}, p.prototype.readInt32BE = function (e, t) {
  return t || H(e, 4, this.length), this[e] << 24 | this[e + 1] << 16 | this[e + 2] << 8 | this[e + 3];
}, p.prototype.readFloatLE = function (e, t) {
  return t || H(e, 4, this.length), h(this, e, !0, 23, 4);
}, p.prototype.readFloatBE = function (e, t) {
  return t || H(e, 4, this.length), h(this, e, !1, 23, 4);
}, p.prototype.readDoubleLE = function (e, t) {
  return t || H(e, 8, this.length), h(this, e, !0, 52, 8);
}, p.prototype.readDoubleBE = function (e, t) {
  return t || H(e, 8, this.length), h(this, e, !1, 52, 8);
}, p.prototype.writeUIntLE = function (e, t, r, n) {
  (e = +e, t |= 0, r |= 0, n) || F(this, e, t, r, Math.pow(2, 8 * r) - 1, 0);
  var i = 1,
    o = 0;
  for (this[t] = 255 & e; ++o < r && (i *= 256);) {
    this[t + o] = e / i & 255;
  }
  return t + r;
}, p.prototype.writeUIntBE = function (e, t, r, n) {
  (e = +e, t |= 0, r |= 0, n) || F(this, e, t, r, Math.pow(2, 8 * r) - 1, 0);
  var i = r - 1,
    o = 1;
  for (this[t + i] = 255 & e; --i >= 0 && (o *= 256);) {
    this[t + i] = e / o & 255;
  }
  return t + r;
}, p.prototype.writeUInt8 = function (e, t, r) {
  return e = +e, t |= 0, r || F(this, e, t, 1, 255, 0), p.TYPED_ARRAY_SUPPORT || (e = Math.floor(e)), this[t] = 255 & e, t + 1;
}, p.prototype.writeUInt16LE = function (e, t, r) {
  return e = +e, t |= 0, r || F(this, e, t, 2, 65535, 0), p.TYPED_ARRAY_SUPPORT ? (this[t] = 255 & e, this[t + 1] = e >>> 8) : N(this, e, t, !0), t + 2;
}, p.prototype.writeUInt16BE = function (e, t, r) {
  return e = +e, t |= 0, r || F(this, e, t, 2, 65535, 0), p.TYPED_ARRAY_SUPPORT ? (this[t] = e >>> 8, this[t + 1] = 255 & e) : N(this, e, t, !1), t + 2;
}, p.prototype.writeUInt32LE = function (e, t, r) {
  return e = +e, t |= 0, r || F(this, e, t, 4, 4294967295, 0), p.TYPED_ARRAY_SUPPORT ? (this[t + 3] = e >>> 24, this[t + 2] = e >>> 16, this[t + 1] = e >>> 8, this[t] = 255 & e) : Z(this, e, t, !0), t + 4;
}, p.prototype.writeUInt32BE = function (e, t, r) {
  return e = +e, t |= 0, r || F(this, e, t, 4, 4294967295, 0), p.TYPED_ARRAY_SUPPORT ? (this[t] = e >>> 24, this[t + 1] = e >>> 16, this[t + 2] = e >>> 8, this[t + 3] = 255 & e) : Z(this, e, t, !1), t + 4;
}, p.prototype.writeIntLE = function (e, t, r, n) {
  if (e = +e, t |= 0, !n) {
    var i = Math.pow(2, 8 * r - 1);
    F(this, e, t, r, i - 1, -i);
  }
  var o = 0,
    a = 1,
    s = 0;
  for (this[t] = 255 & e; ++o < r && (a *= 256);) {
    e < 0 && 0 === s && 0 !== this[t + o - 1] && (s = 1), this[t + o] = (e / a >> 0) - s & 255;
  }
  return t + r;
}, p.prototype.writeIntBE = function (e, t, r, n) {
  if (e = +e, t |= 0, !n) {
    var i = Math.pow(2, 8 * r - 1);
    F(this, e, t, r, i - 1, -i);
  }
  var o = r - 1,
    a = 1,
    s = 0;
  for (this[t + o] = 255 & e; --o >= 0 && (a *= 256);) {
    e < 0 && 0 === s && 0 !== this[t + o + 1] && (s = 1), this[t + o] = (e / a >> 0) - s & 255;
  }
  return t + r;
}, p.prototype.writeInt8 = function (e, t, r) {
  return e = +e, t |= 0, r || F(this, e, t, 1, 127, -128), p.TYPED_ARRAY_SUPPORT || (e = Math.floor(e)), e < 0 && (e = 255 + e + 1), this[t] = 255 & e, t + 1;
}, p.prototype.writeInt16LE = function (e, t, r) {
  return e = +e, t |= 0, r || F(this, e, t, 2, 32767, -32768), p.TYPED_ARRAY_SUPPORT ? (this[t] = 255 & e, this[t + 1] = e >>> 8) : N(this, e, t, !0), t + 2;
}, p.prototype.writeInt16BE = function (e, t, r) {
  return e = +e, t |= 0, r || F(this, e, t, 2, 32767, -32768), p.TYPED_ARRAY_SUPPORT ? (this[t] = e >>> 8, this[t + 1] = 255 & e) : N(this, e, t, !1), t + 2;
}, p.prototype.writeInt32LE = function (e, t, r) {
  return e = +e, t |= 0, r || F(this, e, t, 4, 2147483647, -2147483648), p.TYPED_ARRAY_SUPPORT ? (this[t] = 255 & e, this[t + 1] = e >>> 8, this[t + 2] = e >>> 16, this[t + 3] = e >>> 24) : Z(this, e, t, !0), t + 4;
}, p.prototype.writeInt32BE = function (e, t, r) {
  return e = +e, t |= 0, r || F(this, e, t, 4, 2147483647, -2147483648), e < 0 && (e = 4294967295 + e + 1), p.TYPED_ARRAY_SUPPORT ? (this[t] = e >>> 24, this[t + 1] = e >>> 16, this[t + 2] = e >>> 8, this[t + 3] = 255 & e) : Z(this, e, t, !1), t + 4;
}, p.prototype.writeFloatLE = function (e, t, r) {
  return W(this, e, t, !0, r);
}, p.prototype.writeFloatBE = function (e, t, r) {
  return W(this, e, t, !1, r);
}, p.prototype.writeDoubleLE = function (e, t, r) {
  return Y(this, e, t, !0, r);
}, p.prototype.writeDoubleBE = function (e, t, r) {
  return Y(this, e, t, !1, r);
}, p.prototype.copy = function (e, t, r, n) {
  if (r || (r = 0), n || 0 === n || (n = this.length), t >= e.length && (t = e.length), t || (t = 0), n > 0 && n < r && (n = r), n === r) return 0;
  if (0 === e.length || 0 === this.length) return 0;
  if (t < 0) throw new RangeError("targetStart out of bounds");
  if (r < 0 || r >= this.length) throw new RangeError("sourceStart out of bounds");
  if (n < 0) throw new RangeError("sourceEnd out of bounds");
  n > this.length && (n = this.length), e.length - t < n - r && (n = e.length - t + r);
  var i,
    o = n - r;
  if (this === e && r < t && t < n) for (i = o - 1; i >= 0; --i) {
    e[i + t] = this[i + r];
  } else if (o < 1e3 || !p.TYPED_ARRAY_SUPPORT) for (i = 0; i < o; ++i) {
    e[i + t] = this[i + r];
  } else Uint8Array.prototype.set.call(e, this.subarray(r, r + o), t);
  return o;
}, p.prototype.fill = function (e, t, r, n) {
  if ("string" == typeof e) {
    if ("string" == typeof t ? (n = t, t = 0, r = this.length) : "string" == typeof r && (n = r, r = this.length), 1 === e.length) {
      var i = e.charCodeAt(0);
      i < 256 && (e = i);
    }
    if (void 0 !== n && "string" != typeof n) throw new TypeError("encoding must be a string");
    if ("string" == typeof n && !p.isEncoding(n)) throw new TypeError("Unknown encoding: " + n);
  } else "number" == typeof e && (e &= 255);
  if (t < 0 || this.length < t || this.length < r) throw new RangeError("Out of range index");
  if (r <= t) return this;
  var o;
  if (t >>>= 0, r = void 0 === r ? this.length : r >>> 0, e || (e = 0), "number" == typeof e) for (o = t; o < r; ++o) {
    this[o] = e;
  } else {
    var a = y(e) ? e : q(new p(e, n).toString()),
      s = a.length;
    for (o = 0; o < r - t; ++o) {
      this[o + t] = a[o % s];
    }
  }
  return this;
};
var K = /[^+\/0-9A-Za-z-_]/g;
function X(e) {
  return e < 16 ? "0" + e.toString(16) : e.toString(16);
}
function q(e, t) {
  var r;
  t = t || 1 / 0;
  for (var n = e.length, i = null, o = [], a = 0; a < n; ++a) {
    if ((r = e.charCodeAt(a)) > 55295 && r < 57344) {
      if (!i) {
        if (r > 56319) {
          (t -= 3) > -1 && o.push(239, 191, 189);
          continue;
        }
        if (a + 1 === n) {
          (t -= 3) > -1 && o.push(239, 191, 189);
          continue;
        }
        i = r;
        continue;
      }
      if (r < 56320) {
        (t -= 3) > -1 && o.push(239, 191, 189), i = r;
        continue;
      }
      r = 65536 + (i - 55296 << 10 | r - 56320);
    } else i && (t -= 3) > -1 && o.push(239, 191, 189);
    if (i = null, r < 128) {
      if ((t -= 1) < 0) break;
      o.push(r);
    } else if (r < 2048) {
      if ((t -= 2) < 0) break;
      o.push(r >> 6 | 192, 63 & r | 128);
    } else if (r < 65536) {
      if ((t -= 3) < 0) break;
      o.push(r >> 12 | 224, r >> 6 & 63 | 128, 63 & r | 128);
    } else {
      if (!(r < 1114112)) throw new Error("Invalid code point");
      if ((t -= 4) < 0) break;
      o.push(r >> 18 | 240, r >> 12 & 63 | 128, r >> 6 & 63 | 128, 63 & r | 128);
    }
  }
  return o;
}
function V(e) {
  return function (e) {
    var t, a, s, h, l, f;
    i || o();
    var c = e.length;
    if (c % 4 > 0) throw new Error("Invalid string. Length must be a multiple of 4");
    l = "=" === e[c - 2] ? 2 : "=" === e[c - 1] ? 1 : 0, f = new n(3 * c / 4 - l), s = l > 0 ? c - 4 : c;
    var u = 0;
    for (t = 0, a = 0; t < s; t += 4, a += 3) {
      h = r[e.charCodeAt(t)] << 18 | r[e.charCodeAt(t + 1)] << 12 | r[e.charCodeAt(t + 2)] << 6 | r[e.charCodeAt(t + 3)], f[u++] = h >> 16 & 255, f[u++] = h >> 8 & 255, f[u++] = 255 & h;
    }
    return 2 === l ? (h = r[e.charCodeAt(t)] << 2 | r[e.charCodeAt(t + 1)] >> 4, f[u++] = 255 & h) : 1 === l && (h = r[e.charCodeAt(t)] << 10 | r[e.charCodeAt(t + 1)] << 4 | r[e.charCodeAt(t + 2)] >> 2, f[u++] = h >> 8 & 255, f[u++] = 255 & h), f;
  }(function (e) {
    if ((e = function (e) {
      return e.trim ? e.trim() : e.replace(/^\s+|\s+$/g, "");
    }(e).replace(K, "")).length < 2) return "";
    for (; e.length % 4 != 0;) {
      e += "=";
    }
    return e;
  }(e));
}
function G(e, t, r, n) {
  for (var i = 0; i < n && !(i + r >= t.length || i >= e.length); ++i) {
    t[i + r] = e[i];
  }
  return i;
}
function $(e) {
  return null != e && (!!e._isBuffer || J(e) || function (e) {
    return "function" == typeof e.readFloatLE && "function" == typeof e.slice && J(e.slice(0, 0));
  }(e));
}
function J(e) {
  return !!e.constructor && "function" == typeof e.constructor.isBuffer && e.constructor.isBuffer(e);
}
"undefined" != typeof globalThis ? globalThis : "undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self && self;
function Q(e, t) {
  return e(t = {
    exports: {}
  }, t.exports), t.exports;
}
var ee = Q(function (e, t) {
    var r;
    e.exports = (r = r || function (e, t) {
      var r = Object.create || function () {
          function e() {}
          return function (t) {
            var r;
            return e.prototype = t, r = new e(), e.prototype = null, r;
          };
        }(),
        n = {},
        i = n.lib = {},
        o = i.Base = {
          extend: function extend(e) {
            var t = r(this);
            return e && t.mixIn(e), t.hasOwnProperty("init") && this.init !== t.init || (t.init = function () {
              t.$super.init.apply(this, arguments);
            }), t.init.prototype = t, t.$super = this, t;
          },
          create: function create() {
            var e = this.extend();
            return e.init.apply(e, arguments), e;
          },
          init: function init() {},
          mixIn: function mixIn(e) {
            for (var t in e) {
              e.hasOwnProperty(t) && (this[t] = e[t]);
            }
            e.hasOwnProperty("toString") && (this.toString = e.toString);
          },
          clone: function clone() {
            return this.init.prototype.extend(this);
          }
        },
        a = i.WordArray = o.extend({
          init: function init(e, t) {
            e = this.words = e || [], this.sigBytes = null != t ? t : 4 * e.length;
          },
          toString: function toString(e) {
            return (e || h).stringify(this);
          },
          concat: function concat(e) {
            var t = this.words,
              r = e.words,
              n = this.sigBytes,
              i = e.sigBytes;
            if (this.clamp(), n % 4) for (var o = 0; o < i; o++) {
              var a = r[o >>> 2] >>> 24 - o % 4 * 8 & 255;
              t[n + o >>> 2] |= a << 24 - (n + o) % 4 * 8;
            } else for (var o = 0; o < i; o += 4) {
              t[n + o >>> 2] = r[o >>> 2];
            }
            return this.sigBytes += i, this;
          },
          clamp: function clamp() {
            var t = this.words,
              r = this.sigBytes;
            t[r >>> 2] &= 4294967295 << 32 - r % 4 * 8, t.length = e.ceil(r / 4);
          },
          clone: function clone() {
            var e = o.clone.call(this);
            return e.words = this.words.slice(0), e;
          },
          random: function random(t) {
            for (var r, n = [], i = function i(t) {
                var t = t,
                  r = 987654321,
                  n = 4294967295;
                return function () {
                  var i = ((r = 36969 * (65535 & r) + (r >> 16) & n) << 16) + (t = 18e3 * (65535 & t) + (t >> 16) & n) & n;
                  return i /= 4294967296, (i += .5) * (e.random() > .5 ? 1 : -1);
                };
              }, o = 0; o < t; o += 4) {
              var s = i(4294967296 * (r || e.random()));
              r = 987654071 * s(), n.push(4294967296 * s() | 0);
            }
            return new a.init(n, t);
          }
        }),
        s = n.enc = {},
        h = s.Hex = {
          stringify: function stringify(e) {
            for (var t = e.words, r = e.sigBytes, n = [], i = 0; i < r; i++) {
              var o = t[i >>> 2] >>> 24 - i % 4 * 8 & 255;
              n.push((o >>> 4).toString(16)), n.push((15 & o).toString(16));
            }
            return n.join("");
          },
          parse: function parse(e) {
            for (var t = e.length, r = [], n = 0; n < t; n += 2) {
              r[n >>> 3] |= parseInt(e.substr(n, 2), 16) << 24 - n % 8 * 4;
            }
            return new a.init(r, t / 2);
          }
        },
        l = s.Latin1 = {
          stringify: function stringify(e) {
            for (var t = e.words, r = e.sigBytes, n = [], i = 0; i < r; i++) {
              var o = t[i >>> 2] >>> 24 - i % 4 * 8 & 255;
              n.push(String.fromCharCode(o));
            }
            return n.join("");
          },
          parse: function parse(e) {
            for (var t = e.length, r = [], n = 0; n < t; n++) {
              r[n >>> 2] |= (255 & e.charCodeAt(n)) << 24 - n % 4 * 8;
            }
            return new a.init(r, t);
          }
        },
        f = s.Utf8 = {
          stringify: function stringify(e) {
            try {
              return decodeURIComponent(escape(l.stringify(e)));
            } catch (e) {
              throw new Error("Malformed UTF-8 data");
            }
          },
          parse: function parse(e) {
            return l.parse(unescape(encodeURIComponent(e)));
          }
        },
        c = i.BufferedBlockAlgorithm = o.extend({
          reset: function reset() {
            this._data = new a.init(), this._nDataBytes = 0;
          },
          _append: function _append(e) {
            "string" == typeof e && (e = f.parse(e)), this._data.concat(e), this._nDataBytes += e.sigBytes;
          },
          _process: function _process(t) {
            var r = this._data,
              n = r.words,
              i = r.sigBytes,
              o = this.blockSize,
              s = 4 * o,
              h = i / s,
              l = (h = t ? e.ceil(h) : e.max((0 | h) - this._minBufferSize, 0)) * o,
              f = e.min(4 * l, i);
            if (l) {
              for (var c = 0; c < l; c += o) {
                this._doProcessBlock(n, c);
              }
              var u = n.splice(0, l);
              r.sigBytes -= f;
            }
            return new a.init(u, f);
          },
          clone: function clone() {
            var e = o.clone.call(this);
            return e._data = this._data.clone(), e;
          },
          _minBufferSize: 0
        }),
        u = (i.Hasher = c.extend({
          cfg: o.extend(),
          init: function init(e) {
            this.cfg = this.cfg.extend(e), this.reset();
          },
          reset: function reset() {
            c.reset.call(this), this._doReset();
          },
          update: function update(e) {
            return this._append(e), this._process(), this;
          },
          finalize: function finalize(e) {
            e && this._append(e);
            var t = this._doFinalize();
            return t;
          },
          blockSize: 16,
          _createHelper: function _createHelper(e) {
            return function (t, r) {
              return new e.init(r).finalize(t);
            };
          },
          _createHmacHelper: function _createHmacHelper(e) {
            return function (t, r) {
              return new u.HMAC.init(e, r).finalize(t);
            };
          }
        }), n.algo = {});
      return n;
    }(Math), r);
  }),
  te = (Q(function (e, t) {
    var r, n, i, o, a, s;
    e.exports = (i = (n = r = ee).lib, o = i.Base, a = i.WordArray, (s = n.x64 = {}).Word = o.extend({
      init: function init(e, t) {
        this.high = e, this.low = t;
      }
    }), s.WordArray = o.extend({
      init: function init(e, t) {
        e = this.words = e || [], this.sigBytes = null != t ? t : 8 * e.length;
      },
      toX32: function toX32() {
        for (var e = this.words, t = e.length, r = [], n = 0; n < t; n++) {
          var i = e[n];
          r.push(i.high), r.push(i.low);
        }
        return a.create(r, this.sigBytes);
      },
      clone: function clone() {
        for (var e = o.clone.call(this), t = e.words = this.words.slice(0), r = t.length, n = 0; n < r; n++) {
          t[n] = t[n].clone();
        }
        return e;
      }
    }), r);
  }), Q(function (e, t) {
    var r;
    e.exports = (r = ee, function () {
      if ("function" == typeof ArrayBuffer) {
        var e = r.lib.WordArray,
          t = e.init;
        (e.init = function (e) {
          if (e instanceof ArrayBuffer && (e = new Uint8Array(e)), (e instanceof Int8Array || "undefined" != typeof Uint8ClampedArray && e instanceof Uint8ClampedArray || e instanceof Int16Array || e instanceof Uint16Array || e instanceof Int32Array || e instanceof Uint32Array || e instanceof Float32Array || e instanceof Float64Array) && (e = new Uint8Array(e.buffer, e.byteOffset, e.byteLength)), e instanceof Uint8Array) {
            for (var r = e.byteLength, n = [], i = 0; i < r; i++) {
              n[i >>> 2] |= e[i] << 24 - i % 4 * 8;
            }
            t.call(this, n, r);
          } else t.apply(this, arguments);
        }).prototype = e;
      }
    }(), r.lib.WordArray);
  }), Q(function (e, t) {
    var r;
    e.exports = (r = ee, function () {
      var e = r,
        t = e.lib.WordArray,
        n = e.enc;
      function i(e) {
        return e << 8 & 4278255360 | e >>> 8 & 16711935;
      }
      n.Utf16 = n.Utf16BE = {
        stringify: function stringify(e) {
          for (var t = e.words, r = e.sigBytes, n = [], i = 0; i < r; i += 2) {
            var o = t[i >>> 2] >>> 16 - i % 4 * 8 & 65535;
            n.push(String.fromCharCode(o));
          }
          return n.join("");
        },
        parse: function parse(e) {
          for (var r = e.length, n = [], i = 0; i < r; i++) {
            n[i >>> 1] |= e.charCodeAt(i) << 16 - i % 2 * 16;
          }
          return t.create(n, 2 * r);
        }
      }, n.Utf16LE = {
        stringify: function stringify(e) {
          for (var t = e.words, r = e.sigBytes, n = [], o = 0; o < r; o += 2) {
            var a = i(t[o >>> 2] >>> 16 - o % 4 * 8 & 65535);
            n.push(String.fromCharCode(a));
          }
          return n.join("");
        },
        parse: function parse(e) {
          for (var r = e.length, n = [], o = 0; o < r; o++) {
            n[o >>> 1] |= i(e.charCodeAt(o) << 16 - o % 2 * 16);
          }
          return t.create(n, 2 * r);
        }
      };
    }(), r.enc.Utf16);
  }), Q(function (e, t) {
    var r, n, i;
    e.exports = (i = (n = r = ee).lib.WordArray, n.enc.Base64 = {
      stringify: function stringify(e) {
        var t = e.words,
          r = e.sigBytes,
          n = this._map;
        e.clamp();
        for (var i = [], o = 0; o < r; o += 3) {
          for (var a = (t[o >>> 2] >>> 24 - o % 4 * 8 & 255) << 16 | (t[o + 1 >>> 2] >>> 24 - (o + 1) % 4 * 8 & 255) << 8 | t[o + 2 >>> 2] >>> 24 - (o + 2) % 4 * 8 & 255, s = 0; s < 4 && o + .75 * s < r; s++) {
            i.push(n.charAt(a >>> 6 * (3 - s) & 63));
          }
        }
        var h = n.charAt(64);
        if (h) for (; i.length % 4;) {
          i.push(h);
        }
        return i.join("");
      },
      parse: function parse(e) {
        var t = e.length,
          r = this._map,
          n = this._reverseMap;
        if (!n) {
          n = this._reverseMap = [];
          for (var o = 0; o < r.length; o++) {
            n[r.charCodeAt(o)] = o;
          }
        }
        var a = r.charAt(64);
        if (a) {
          var s = e.indexOf(a);
          -1 !== s && (t = s);
        }
        return function (e, t, r) {
          for (var n = [], o = 0, a = 0; a < t; a++) {
            if (a % 4) {
              var s = r[e.charCodeAt(a - 1)] << a % 4 * 2,
                h = r[e.charCodeAt(a)] >>> 6 - a % 4 * 2;
              n[o >>> 2] |= (s | h) << 24 - o % 4 * 8, o++;
            }
          }
          return i.create(n, o);
        }(e, t, n);
      },
      _map: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
    }, r.enc.Base64);
  }), Q(function (e, t) {
    var r;
    e.exports = (r = ee, function (e) {
      var t = r,
        n = t.lib,
        i = n.WordArray,
        o = n.Hasher,
        a = t.algo,
        s = [];
      !function () {
        for (var t = 0; t < 64; t++) {
          s[t] = 4294967296 * e.abs(e.sin(t + 1)) | 0;
        }
      }();
      var h = a.MD5 = o.extend({
        _doReset: function _doReset() {
          this._hash = new i.init([1732584193, 4023233417, 2562383102, 271733878]);
        },
        _doProcessBlock: function _doProcessBlock(e, t) {
          for (var r = 0; r < 16; r++) {
            var n = t + r,
              i = e[n];
            e[n] = 16711935 & (i << 8 | i >>> 24) | 4278255360 & (i << 24 | i >>> 8);
          }
          var o = this._hash.words,
            a = e[t + 0],
            h = e[t + 1],
            d = e[t + 2],
            p = e[t + 3],
            _ = e[t + 4],
            g = e[t + 5],
            v = e[t + 6],
            w = e[t + 7],
            b = e[t + 8],
            y = e[t + 9],
            m = e[t + 10],
            k = e[t + 11],
            E = e[t + 12],
            S = e[t + 13],
            x = e[t + 14],
            R = e[t + 15],
            A = o[0],
            B = o[1],
            z = o[2],
            L = o[3];
          A = l(A, B, z, L, a, 7, s[0]), L = l(L, A, B, z, h, 12, s[1]), z = l(z, L, A, B, d, 17, s[2]), B = l(B, z, L, A, p, 22, s[3]), A = l(A, B, z, L, _, 7, s[4]), L = l(L, A, B, z, g, 12, s[5]), z = l(z, L, A, B, v, 17, s[6]), B = l(B, z, L, A, w, 22, s[7]), A = l(A, B, z, L, b, 7, s[8]), L = l(L, A, B, z, y, 12, s[9]), z = l(z, L, A, B, m, 17, s[10]), B = l(B, z, L, A, k, 22, s[11]), A = l(A, B, z, L, E, 7, s[12]), L = l(L, A, B, z, S, 12, s[13]), z = l(z, L, A, B, x, 17, s[14]), A = f(A, B = l(B, z, L, A, R, 22, s[15]), z, L, h, 5, s[16]), L = f(L, A, B, z, v, 9, s[17]), z = f(z, L, A, B, k, 14, s[18]), B = f(B, z, L, A, a, 20, s[19]), A = f(A, B, z, L, g, 5, s[20]), L = f(L, A, B, z, m, 9, s[21]), z = f(z, L, A, B, R, 14, s[22]), B = f(B, z, L, A, _, 20, s[23]), A = f(A, B, z, L, y, 5, s[24]), L = f(L, A, B, z, x, 9, s[25]), z = f(z, L, A, B, p, 14, s[26]), B = f(B, z, L, A, b, 20, s[27]), A = f(A, B, z, L, S, 5, s[28]), L = f(L, A, B, z, d, 9, s[29]), z = f(z, L, A, B, w, 14, s[30]), A = c(A, B = f(B, z, L, A, E, 20, s[31]), z, L, g, 4, s[32]), L = c(L, A, B, z, b, 11, s[33]), z = c(z, L, A, B, k, 16, s[34]), B = c(B, z, L, A, x, 23, s[35]), A = c(A, B, z, L, h, 4, s[36]), L = c(L, A, B, z, _, 11, s[37]), z = c(z, L, A, B, w, 16, s[38]), B = c(B, z, L, A, m, 23, s[39]), A = c(A, B, z, L, S, 4, s[40]), L = c(L, A, B, z, a, 11, s[41]), z = c(z, L, A, B, p, 16, s[42]), B = c(B, z, L, A, v, 23, s[43]), A = c(A, B, z, L, y, 4, s[44]), L = c(L, A, B, z, E, 11, s[45]), z = c(z, L, A, B, R, 16, s[46]), A = u(A, B = c(B, z, L, A, d, 23, s[47]), z, L, a, 6, s[48]), L = u(L, A, B, z, w, 10, s[49]), z = u(z, L, A, B, x, 15, s[50]), B = u(B, z, L, A, g, 21, s[51]), A = u(A, B, z, L, E, 6, s[52]), L = u(L, A, B, z, p, 10, s[53]), z = u(z, L, A, B, m, 15, s[54]), B = u(B, z, L, A, h, 21, s[55]), A = u(A, B, z, L, b, 6, s[56]), L = u(L, A, B, z, R, 10, s[57]), z = u(z, L, A, B, v, 15, s[58]), B = u(B, z, L, A, S, 21, s[59]), A = u(A, B, z, L, _, 6, s[60]), L = u(L, A, B, z, k, 10, s[61]), z = u(z, L, A, B, d, 15, s[62]), B = u(B, z, L, A, y, 21, s[63]), o[0] = o[0] + A | 0, o[1] = o[1] + B | 0, o[2] = o[2] + z | 0, o[3] = o[3] + L | 0;
        },
        _doFinalize: function _doFinalize() {
          var t = this._data,
            r = t.words,
            n = 8 * this._nDataBytes,
            i = 8 * t.sigBytes;
          r[i >>> 5] |= 128 << 24 - i % 32;
          var o = e.floor(n / 4294967296),
            a = n;
          r[15 + (i + 64 >>> 9 << 4)] = 16711935 & (o << 8 | o >>> 24) | 4278255360 & (o << 24 | o >>> 8), r[14 + (i + 64 >>> 9 << 4)] = 16711935 & (a << 8 | a >>> 24) | 4278255360 & (a << 24 | a >>> 8), t.sigBytes = 4 * (r.length + 1), this._process();
          for (var s = this._hash, h = s.words, l = 0; l < 4; l++) {
            var f = h[l];
            h[l] = 16711935 & (f << 8 | f >>> 24) | 4278255360 & (f << 24 | f >>> 8);
          }
          return s;
        },
        clone: function clone() {
          var e = o.clone.call(this);
          return e._hash = this._hash.clone(), e;
        }
      });
      function l(e, t, r, n, i, o, a) {
        var s = e + (t & r | ~t & n) + i + a;
        return (s << o | s >>> 32 - o) + t;
      }
      function f(e, t, r, n, i, o, a) {
        var s = e + (t & n | r & ~n) + i + a;
        return (s << o | s >>> 32 - o) + t;
      }
      function c(e, t, r, n, i, o, a) {
        var s = e + (t ^ r ^ n) + i + a;
        return (s << o | s >>> 32 - o) + t;
      }
      function u(e, t, r, n, i, o, a) {
        var s = e + (r ^ (t | ~n)) + i + a;
        return (s << o | s >>> 32 - o) + t;
      }
      t.MD5 = o._createHelper(h), t.HmacMD5 = o._createHmacHelper(h);
    }(Math), r.MD5);
  }), Q(function (e, t) {
    var r, n, i, o, a, s, h, l;
    e.exports = (i = (n = r = ee).lib, o = i.WordArray, a = i.Hasher, s = n.algo, h = [], l = s.SHA1 = a.extend({
      _doReset: function _doReset() {
        this._hash = new o.init([1732584193, 4023233417, 2562383102, 271733878, 3285377520]);
      },
      _doProcessBlock: function _doProcessBlock(e, t) {
        for (var r = this._hash.words, n = r[0], i = r[1], o = r[2], a = r[3], s = r[4], l = 0; l < 80; l++) {
          if (l < 16) h[l] = 0 | e[t + l];else {
            var f = h[l - 3] ^ h[l - 8] ^ h[l - 14] ^ h[l - 16];
            h[l] = f << 1 | f >>> 31;
          }
          var c = (n << 5 | n >>> 27) + s + h[l];
          c += l < 20 ? 1518500249 + (i & o | ~i & a) : l < 40 ? 1859775393 + (i ^ o ^ a) : l < 60 ? (i & o | i & a | o & a) - 1894007588 : (i ^ o ^ a) - 899497514, s = a, a = o, o = i << 30 | i >>> 2, i = n, n = c;
        }
        r[0] = r[0] + n | 0, r[1] = r[1] + i | 0, r[2] = r[2] + o | 0, r[3] = r[3] + a | 0, r[4] = r[4] + s | 0;
      },
      _doFinalize: function _doFinalize() {
        var e = this._data,
          t = e.words,
          r = 8 * this._nDataBytes,
          n = 8 * e.sigBytes;
        return t[n >>> 5] |= 128 << 24 - n % 32, t[14 + (n + 64 >>> 9 << 4)] = Math.floor(r / 4294967296), t[15 + (n + 64 >>> 9 << 4)] = r, e.sigBytes = 4 * t.length, this._process(), this._hash;
      },
      clone: function clone() {
        var e = a.clone.call(this);
        return e._hash = this._hash.clone(), e;
      }
    }), n.SHA1 = a._createHelper(l), n.HmacSHA1 = a._createHmacHelper(l), r.SHA1);
  }), Q(function (e, t) {
    var r;
    e.exports = (r = ee, function (e) {
      var t = r,
        n = t.lib,
        i = n.WordArray,
        o = n.Hasher,
        a = t.algo,
        s = [],
        h = [];
      !function () {
        function t(t) {
          for (var r = e.sqrt(t), n = 2; n <= r; n++) {
            if (!(t % n)) return !1;
          }
          return !0;
        }
        function r(e) {
          return 4294967296 * (e - (0 | e)) | 0;
        }
        for (var n = 2, i = 0; i < 64;) {
          t(n) && (i < 8 && (s[i] = r(e.pow(n, .5))), h[i] = r(e.pow(n, 1 / 3)), i++), n++;
        }
      }();
      var l = [],
        f = a.SHA256 = o.extend({
          _doReset: function _doReset() {
            this._hash = new i.init(s.slice(0));
          },
          _doProcessBlock: function _doProcessBlock(e, t) {
            for (var r = this._hash.words, n = r[0], i = r[1], o = r[2], a = r[3], s = r[4], f = r[5], c = r[6], u = r[7], d = 0; d < 64; d++) {
              if (d < 16) l[d] = 0 | e[t + d];else {
                var p = l[d - 15],
                  _ = (p << 25 | p >>> 7) ^ (p << 14 | p >>> 18) ^ p >>> 3,
                  g = l[d - 2],
                  v = (g << 15 | g >>> 17) ^ (g << 13 | g >>> 19) ^ g >>> 10;
                l[d] = _ + l[d - 7] + v + l[d - 16];
              }
              var w = n & i ^ n & o ^ i & o,
                b = (n << 30 | n >>> 2) ^ (n << 19 | n >>> 13) ^ (n << 10 | n >>> 22),
                y = u + ((s << 26 | s >>> 6) ^ (s << 21 | s >>> 11) ^ (s << 7 | s >>> 25)) + (s & f ^ ~s & c) + h[d] + l[d];
              u = c, c = f, f = s, s = a + y | 0, a = o, o = i, i = n, n = y + (b + w) | 0;
            }
            r[0] = r[0] + n | 0, r[1] = r[1] + i | 0, r[2] = r[2] + o | 0, r[3] = r[3] + a | 0, r[4] = r[4] + s | 0, r[5] = r[5] + f | 0, r[6] = r[6] + c | 0, r[7] = r[7] + u | 0;
          },
          _doFinalize: function _doFinalize() {
            var t = this._data,
              r = t.words,
              n = 8 * this._nDataBytes,
              i = 8 * t.sigBytes;
            return r[i >>> 5] |= 128 << 24 - i % 32, r[14 + (i + 64 >>> 9 << 4)] = e.floor(n / 4294967296), r[15 + (i + 64 >>> 9 << 4)] = n, t.sigBytes = 4 * r.length, this._process(), this._hash;
          },
          clone: function clone() {
            var e = o.clone.call(this);
            return e._hash = this._hash.clone(), e;
          }
        });
      t.SHA256 = o._createHelper(f), t.HmacSHA256 = o._createHmacHelper(f);
    }(Math), r.SHA256);
  }), Q(function (e, t) {
    var r, n, i, o, a, s;
    e.exports = (i = (n = r = ee).lib.WordArray, o = n.algo, a = o.SHA256, s = o.SHA224 = a.extend({
      _doReset: function _doReset() {
        this._hash = new i.init([3238371032, 914150663, 812702999, 4144912697, 4290775857, 1750603025, 1694076839, 3204075428]);
      },
      _doFinalize: function _doFinalize() {
        var e = a._doFinalize.call(this);
        return e.sigBytes -= 4, e;
      }
    }), n.SHA224 = a._createHelper(s), n.HmacSHA224 = a._createHmacHelper(s), r.SHA224);
  }), Q(function (e, t) {
    var r;
    e.exports = (r = ee, function () {
      var e = r,
        t = e.lib.Hasher,
        n = e.x64,
        i = n.Word,
        o = n.WordArray,
        a = e.algo;
      function s() {
        return i.create.apply(i, arguments);
      }
      var h = [s(1116352408, 3609767458), s(1899447441, 602891725), s(3049323471, 3964484399), s(3921009573, 2173295548), s(961987163, 4081628472), s(1508970993, 3053834265), s(2453635748, 2937671579), s(2870763221, 3664609560), s(3624381080, 2734883394), s(310598401, 1164996542), s(607225278, 1323610764), s(1426881987, 3590304994), s(1925078388, 4068182383), s(2162078206, 991336113), s(2614888103, 633803317), s(3248222580, 3479774868), s(3835390401, 2666613458), s(4022224774, 944711139), s(264347078, 2341262773), s(604807628, 2007800933), s(770255983, 1495990901), s(1249150122, 1856431235), s(1555081692, 3175218132), s(1996064986, 2198950837), s(2554220882, 3999719339), s(2821834349, 766784016), s(2952996808, 2566594879), s(3210313671, 3203337956), s(3336571891, 1034457026), s(3584528711, 2466948901), s(113926993, 3758326383), s(338241895, 168717936), s(666307205, 1188179964), s(773529912, 1546045734), s(1294757372, 1522805485), s(1396182291, 2643833823), s(1695183700, 2343527390), s(1986661051, 1014477480), s(2177026350, 1206759142), s(2456956037, 344077627), s(2730485921, 1290863460), s(2820302411, 3158454273), s(3259730800, 3505952657), s(3345764771, 106217008), s(3516065817, 3606008344), s(3600352804, 1432725776), s(4094571909, 1467031594), s(275423344, 851169720), s(430227734, 3100823752), s(506948616, 1363258195), s(659060556, 3750685593), s(883997877, 3785050280), s(958139571, 3318307427), s(1322822218, 3812723403), s(1537002063, 2003034995), s(1747873779, 3602036899), s(1955562222, 1575990012), s(2024104815, 1125592928), s(2227730452, 2716904306), s(2361852424, 442776044), s(2428436474, 593698344), s(2756734187, 3733110249), s(3204031479, 2999351573), s(3329325298, 3815920427), s(3391569614, 3928383900), s(3515267271, 566280711), s(3940187606, 3454069534), s(4118630271, 4000239992), s(116418474, 1914138554), s(174292421, 2731055270), s(289380356, 3203993006), s(460393269, 320620315), s(685471733, 587496836), s(852142971, 1086792851), s(1017036298, 365543100), s(1126000580, 2618297676), s(1288033470, 3409855158), s(1501505948, 4234509866), s(1607167915, 987167468), s(1816402316, 1246189591)],
        l = [];
      !function () {
        for (var e = 0; e < 80; e++) {
          l[e] = s();
        }
      }();
      var f = a.SHA512 = t.extend({
        _doReset: function _doReset() {
          this._hash = new o.init([new i.init(1779033703, 4089235720), new i.init(3144134277, 2227873595), new i.init(1013904242, 4271175723), new i.init(2773480762, 1595750129), new i.init(1359893119, 2917565137), new i.init(2600822924, 725511199), new i.init(528734635, 4215389547), new i.init(1541459225, 327033209)]);
        },
        _doProcessBlock: function _doProcessBlock(e, t) {
          for (var r = this._hash.words, n = r[0], i = r[1], o = r[2], a = r[3], s = r[4], f = r[5], c = r[6], u = r[7], d = n.high, p = n.low, _ = i.high, g = i.low, v = o.high, w = o.low, b = a.high, y = a.low, m = s.high, k = s.low, E = f.high, S = f.low, x = c.high, R = c.low, A = u.high, B = u.low, z = d, L = p, T = _, M = g, C = v, D = w, I = b, P = y, O = m, U = k, H = E, F = S, N = x, Z = R, j = A, W = B, Y = 0; Y < 80; Y++) {
            var K = l[Y];
            if (Y < 16) var X = K.high = 0 | e[t + 2 * Y],
              q = K.low = 0 | e[t + 2 * Y + 1];else {
              var V = l[Y - 15],
                G = V.high,
                $ = V.low,
                J = (G >>> 1 | $ << 31) ^ (G >>> 8 | $ << 24) ^ G >>> 7,
                Q = ($ >>> 1 | G << 31) ^ ($ >>> 8 | G << 24) ^ ($ >>> 7 | G << 25),
                ee = l[Y - 2],
                te = ee.high,
                re = ee.low,
                ne = (te >>> 19 | re << 13) ^ (te << 3 | re >>> 29) ^ te >>> 6,
                ie = (re >>> 19 | te << 13) ^ (re << 3 | te >>> 29) ^ (re >>> 6 | te << 26),
                oe = l[Y - 7],
                ae = oe.high,
                se = oe.low,
                he = l[Y - 16],
                le = he.high,
                fe = he.low;
              X = (X = (X = J + ae + ((q = Q + se) >>> 0 < Q >>> 0 ? 1 : 0)) + ne + ((q += ie) >>> 0 < ie >>> 0 ? 1 : 0)) + le + ((q += fe) >>> 0 < fe >>> 0 ? 1 : 0), K.high = X, K.low = q;
            }
            var ce,
              ue = O & H ^ ~O & N,
              de = U & F ^ ~U & Z,
              pe = z & T ^ z & C ^ T & C,
              _e = L & M ^ L & D ^ M & D,
              ge = (z >>> 28 | L << 4) ^ (z << 30 | L >>> 2) ^ (z << 25 | L >>> 7),
              ve = (L >>> 28 | z << 4) ^ (L << 30 | z >>> 2) ^ (L << 25 | z >>> 7),
              we = (O >>> 14 | U << 18) ^ (O >>> 18 | U << 14) ^ (O << 23 | U >>> 9),
              be = (U >>> 14 | O << 18) ^ (U >>> 18 | O << 14) ^ (U << 23 | O >>> 9),
              ye = h[Y],
              me = ye.high,
              ke = ye.low,
              Ee = j + we + ((ce = W + be) >>> 0 < W >>> 0 ? 1 : 0),
              Se = ve + _e;
            j = N, W = Z, N = H, Z = F, H = O, F = U, O = I + (Ee = (Ee = (Ee = Ee + ue + ((ce += de) >>> 0 < de >>> 0 ? 1 : 0)) + me + ((ce += ke) >>> 0 < ke >>> 0 ? 1 : 0)) + X + ((ce += q) >>> 0 < q >>> 0 ? 1 : 0)) + ((U = P + ce | 0) >>> 0 < P >>> 0 ? 1 : 0) | 0, I = C, P = D, C = T, D = M, T = z, M = L, z = Ee + (ge + pe + (Se >>> 0 < ve >>> 0 ? 1 : 0)) + ((L = ce + Se | 0) >>> 0 < ce >>> 0 ? 1 : 0) | 0;
          }
          p = n.low = p + L, n.high = d + z + (p >>> 0 < L >>> 0 ? 1 : 0), g = i.low = g + M, i.high = _ + T + (g >>> 0 < M >>> 0 ? 1 : 0), w = o.low = w + D, o.high = v + C + (w >>> 0 < D >>> 0 ? 1 : 0), y = a.low = y + P, a.high = b + I + (y >>> 0 < P >>> 0 ? 1 : 0), k = s.low = k + U, s.high = m + O + (k >>> 0 < U >>> 0 ? 1 : 0), S = f.low = S + F, f.high = E + H + (S >>> 0 < F >>> 0 ? 1 : 0), R = c.low = R + Z, c.high = x + N + (R >>> 0 < Z >>> 0 ? 1 : 0), B = u.low = B + W, u.high = A + j + (B >>> 0 < W >>> 0 ? 1 : 0);
        },
        _doFinalize: function _doFinalize() {
          var e = this._data,
            t = e.words,
            r = 8 * this._nDataBytes,
            n = 8 * e.sigBytes;
          return t[n >>> 5] |= 128 << 24 - n % 32, t[30 + (n + 128 >>> 10 << 5)] = Math.floor(r / 4294967296), t[31 + (n + 128 >>> 10 << 5)] = r, e.sigBytes = 4 * t.length, this._process(), this._hash.toX32();
        },
        clone: function clone() {
          var e = t.clone.call(this);
          return e._hash = this._hash.clone(), e;
        },
        blockSize: 32
      });
      e.SHA512 = t._createHelper(f), e.HmacSHA512 = t._createHmacHelper(f);
    }(), r.SHA512);
  }), Q(function (e, t) {
    var r, n, i, o, a, s, h, l;
    e.exports = (i = (n = r = ee).x64, o = i.Word, a = i.WordArray, s = n.algo, h = s.SHA512, l = s.SHA384 = h.extend({
      _doReset: function _doReset() {
        this._hash = new a.init([new o.init(3418070365, 3238371032), new o.init(1654270250, 914150663), new o.init(2438529370, 812702999), new o.init(355462360, 4144912697), new o.init(1731405415, 4290775857), new o.init(2394180231, 1750603025), new o.init(3675008525, 1694076839), new o.init(1203062813, 3204075428)]);
      },
      _doFinalize: function _doFinalize() {
        var e = h._doFinalize.call(this);
        return e.sigBytes -= 16, e;
      }
    }), n.SHA384 = h._createHelper(l), n.HmacSHA384 = h._createHmacHelper(l), r.SHA384);
  }), Q(function (e, t) {
    var r;
    e.exports = (r = ee, function (e) {
      var t = r,
        n = t.lib,
        i = n.WordArray,
        o = n.Hasher,
        a = t.x64.Word,
        s = t.algo,
        h = [],
        l = [],
        f = [];
      !function () {
        for (var e = 1, t = 0, r = 0; r < 24; r++) {
          h[e + 5 * t] = (r + 1) * (r + 2) / 2 % 64;
          var n = (2 * e + 3 * t) % 5;
          e = t % 5, t = n;
        }
        for (e = 0; e < 5; e++) {
          for (t = 0; t < 5; t++) {
            l[e + 5 * t] = t + (2 * e + 3 * t) % 5 * 5;
          }
        }
        for (var i = 1, o = 0; o < 24; o++) {
          for (var s = 0, c = 0, u = 0; u < 7; u++) {
            if (1 & i) {
              var d = (1 << u) - 1;
              d < 32 ? c ^= 1 << d : s ^= 1 << d - 32;
            }
            128 & i ? i = i << 1 ^ 113 : i <<= 1;
          }
          f[o] = a.create(s, c);
        }
      }();
      var c = [];
      !function () {
        for (var e = 0; e < 25; e++) {
          c[e] = a.create();
        }
      }();
      var u = s.SHA3 = o.extend({
        cfg: o.cfg.extend({
          outputLength: 512
        }),
        _doReset: function _doReset() {
          for (var e = this._state = [], t = 0; t < 25; t++) {
            e[t] = new a.init();
          }
          this.blockSize = (1600 - 2 * this.cfg.outputLength) / 32;
        },
        _doProcessBlock: function _doProcessBlock(e, t) {
          for (var r = this._state, n = this.blockSize / 2, i = 0; i < n; i++) {
            var o = e[t + 2 * i],
              a = e[t + 2 * i + 1];
            o = 16711935 & (o << 8 | o >>> 24) | 4278255360 & (o << 24 | o >>> 8), a = 16711935 & (a << 8 | a >>> 24) | 4278255360 & (a << 24 | a >>> 8), (B = r[i]).high ^= a, B.low ^= o;
          }
          for (var s = 0; s < 24; s++) {
            for (var u = 0; u < 5; u++) {
              for (var d = 0, p = 0, _ = 0; _ < 5; _++) {
                d ^= (B = r[u + 5 * _]).high, p ^= B.low;
              }
              var g = c[u];
              g.high = d, g.low = p;
            }
            for (u = 0; u < 5; u++) {
              var v = c[(u + 4) % 5],
                w = c[(u + 1) % 5],
                b = w.high,
                y = w.low;
              for (d = v.high ^ (b << 1 | y >>> 31), p = v.low ^ (y << 1 | b >>> 31), _ = 0; _ < 5; _++) {
                (B = r[u + 5 * _]).high ^= d, B.low ^= p;
              }
            }
            for (var m = 1; m < 25; m++) {
              var k = (B = r[m]).high,
                E = B.low,
                S = h[m];
              S < 32 ? (d = k << S | E >>> 32 - S, p = E << S | k >>> 32 - S) : (d = E << S - 32 | k >>> 64 - S, p = k << S - 32 | E >>> 64 - S);
              var x = c[l[m]];
              x.high = d, x.low = p;
            }
            var R = c[0],
              A = r[0];
            for (R.high = A.high, R.low = A.low, u = 0; u < 5; u++) {
              for (_ = 0; _ < 5; _++) {
                var B = r[m = u + 5 * _],
                  z = c[m],
                  L = c[(u + 1) % 5 + 5 * _],
                  T = c[(u + 2) % 5 + 5 * _];
                B.high = z.high ^ ~L.high & T.high, B.low = z.low ^ ~L.low & T.low;
              }
            }
            B = r[0];
            var M = f[s];
            B.high ^= M.high, B.low ^= M.low;
          }
        },
        _doFinalize: function _doFinalize() {
          var t = this._data,
            r = t.words,
            n = (this._nDataBytes, 8 * t.sigBytes),
            o = 32 * this.blockSize;
          r[n >>> 5] |= 1 << 24 - n % 32, r[(e.ceil((n + 1) / o) * o >>> 5) - 1] |= 128, t.sigBytes = 4 * r.length, this._process();
          for (var a = this._state, s = this.cfg.outputLength / 8, h = s / 8, l = [], f = 0; f < h; f++) {
            var c = a[f],
              u = c.high,
              d = c.low;
            u = 16711935 & (u << 8 | u >>> 24) | 4278255360 & (u << 24 | u >>> 8), d = 16711935 & (d << 8 | d >>> 24) | 4278255360 & (d << 24 | d >>> 8), l.push(d), l.push(u);
          }
          return new i.init(l, s);
        },
        clone: function clone() {
          for (var e = o.clone.call(this), t = e._state = this._state.slice(0), r = 0; r < 25; r++) {
            t[r] = t[r].clone();
          }
          return e;
        }
      });
      t.SHA3 = o._createHelper(u), t.HmacSHA3 = o._createHmacHelper(u);
    }(Math), r.SHA3);
  }), Q(function (e, t) {
    var r;
    e.exports = (r = ee, function (e) {
      var t = r,
        n = t.lib,
        i = n.WordArray,
        o = n.Hasher,
        a = t.algo,
        s = i.create([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 7, 4, 13, 1, 10, 6, 15, 3, 12, 0, 9, 5, 2, 14, 11, 8, 3, 10, 14, 4, 9, 15, 8, 1, 2, 7, 0, 6, 13, 11, 5, 12, 1, 9, 11, 10, 0, 8, 12, 4, 13, 3, 7, 15, 14, 5, 6, 2, 4, 0, 5, 9, 7, 12, 2, 10, 14, 1, 3, 8, 11, 6, 15, 13]),
        h = i.create([5, 14, 7, 0, 9, 2, 11, 4, 13, 6, 15, 8, 1, 10, 3, 12, 6, 11, 3, 7, 0, 13, 5, 10, 14, 15, 8, 12, 4, 9, 1, 2, 15, 5, 1, 3, 7, 14, 6, 9, 11, 8, 12, 2, 10, 0, 4, 13, 8, 6, 4, 1, 3, 11, 15, 0, 5, 12, 2, 13, 9, 7, 10, 14, 12, 15, 10, 4, 1, 5, 8, 7, 6, 2, 13, 14, 0, 3, 9, 11]),
        l = i.create([11, 14, 15, 12, 5, 8, 7, 9, 11, 13, 14, 15, 6, 7, 9, 8, 7, 6, 8, 13, 11, 9, 7, 15, 7, 12, 15, 9, 11, 7, 13, 12, 11, 13, 6, 7, 14, 9, 13, 15, 14, 8, 13, 6, 5, 12, 7, 5, 11, 12, 14, 15, 14, 15, 9, 8, 9, 14, 5, 6, 8, 6, 5, 12, 9, 15, 5, 11, 6, 8, 13, 12, 5, 12, 13, 14, 11, 8, 5, 6]),
        f = i.create([8, 9, 9, 11, 13, 15, 15, 5, 7, 7, 8, 11, 14, 14, 12, 6, 9, 13, 15, 7, 12, 8, 9, 11, 7, 7, 12, 7, 6, 15, 13, 11, 9, 7, 15, 11, 8, 6, 6, 14, 12, 13, 5, 14, 13, 13, 7, 5, 15, 5, 8, 11, 14, 14, 6, 14, 6, 9, 12, 9, 12, 5, 15, 8, 8, 5, 12, 9, 12, 5, 14, 6, 8, 13, 6, 5, 15, 13, 11, 11]),
        c = i.create([0, 1518500249, 1859775393, 2400959708, 2840853838]),
        u = i.create([1352829926, 1548603684, 1836072691, 2053994217, 0]),
        d = a.RIPEMD160 = o.extend({
          _doReset: function _doReset() {
            this._hash = i.create([1732584193, 4023233417, 2562383102, 271733878, 3285377520]);
          },
          _doProcessBlock: function _doProcessBlock(e, t) {
            for (var r = 0; r < 16; r++) {
              var n = t + r,
                i = e[n];
              e[n] = 16711935 & (i << 8 | i >>> 24) | 4278255360 & (i << 24 | i >>> 8);
            }
            var o,
              a,
              d,
              y,
              m,
              k,
              E,
              S,
              x,
              R,
              A,
              B = this._hash.words,
              z = c.words,
              L = u.words,
              T = s.words,
              M = h.words,
              C = l.words,
              D = f.words;
            for (k = o = B[0], E = a = B[1], S = d = B[2], x = y = B[3], R = m = B[4], r = 0; r < 80; r += 1) {
              A = o + e[t + T[r]] | 0, A += r < 16 ? p(a, d, y) + z[0] : r < 32 ? _(a, d, y) + z[1] : r < 48 ? g(a, d, y) + z[2] : r < 64 ? v(a, d, y) + z[3] : w(a, d, y) + z[4], A = (A = b(A |= 0, C[r])) + m | 0, o = m, m = y, y = b(d, 10), d = a, a = A, A = k + e[t + M[r]] | 0, A += r < 16 ? w(E, S, x) + L[0] : r < 32 ? v(E, S, x) + L[1] : r < 48 ? g(E, S, x) + L[2] : r < 64 ? _(E, S, x) + L[3] : p(E, S, x) + L[4], A = (A = b(A |= 0, D[r])) + R | 0, k = R, R = x, x = b(S, 10), S = E, E = A;
            }
            A = B[1] + d + x | 0, B[1] = B[2] + y + R | 0, B[2] = B[3] + m + k | 0, B[3] = B[4] + o + E | 0, B[4] = B[0] + a + S | 0, B[0] = A;
          },
          _doFinalize: function _doFinalize() {
            var e = this._data,
              t = e.words,
              r = 8 * this._nDataBytes,
              n = 8 * e.sigBytes;
            t[n >>> 5] |= 128 << 24 - n % 32, t[14 + (n + 64 >>> 9 << 4)] = 16711935 & (r << 8 | r >>> 24) | 4278255360 & (r << 24 | r >>> 8), e.sigBytes = 4 * (t.length + 1), this._process();
            for (var i = this._hash, o = i.words, a = 0; a < 5; a++) {
              var s = o[a];
              o[a] = 16711935 & (s << 8 | s >>> 24) | 4278255360 & (s << 24 | s >>> 8);
            }
            return i;
          },
          clone: function clone() {
            var e = o.clone.call(this);
            return e._hash = this._hash.clone(), e;
          }
        });
      function p(e, t, r) {
        return e ^ t ^ r;
      }
      function _(e, t, r) {
        return e & t | ~e & r;
      }
      function g(e, t, r) {
        return (e | ~t) ^ r;
      }
      function v(e, t, r) {
        return e & r | t & ~r;
      }
      function w(e, t, r) {
        return e ^ (t | ~r);
      }
      function b(e, t) {
        return e << t | e >>> 32 - t;
      }
      t.RIPEMD160 = o._createHelper(d), t.HmacRIPEMD160 = o._createHmacHelper(d);
    }(), r.RIPEMD160);
  }), Q(function (e, t) {
    var r, n, i, o, a, s;
    e.exports = (n = (r = ee).lib, i = n.Base, o = r.enc, a = o.Utf8, s = r.algo, void (s.HMAC = i.extend({
      init: function init(e, t) {
        e = this._hasher = new e.init(), "string" == typeof t && (t = a.parse(t));
        var r = e.blockSize,
          n = 4 * r;
        t.sigBytes > n && (t = e.finalize(t)), t.clamp();
        for (var i = this._oKey = t.clone(), o = this._iKey = t.clone(), s = i.words, h = o.words, l = 0; l < r; l++) {
          s[l] ^= 1549556828, h[l] ^= 909522486;
        }
        i.sigBytes = o.sigBytes = n, this.reset();
      },
      reset: function reset() {
        var e = this._hasher;
        e.reset(), e.update(this._iKey);
      },
      update: function update(e) {
        return this._hasher.update(e), this;
      },
      finalize: function finalize(e) {
        var t = this._hasher,
          r = t.finalize(e);
        t.reset();
        var n = t.finalize(this._oKey.clone().concat(r));
        return n;
      }
    })));
  }), Q(function (e, t) {
    var r, n, i, o, a, s, h, l, f;
    e.exports = (i = (n = r = ee).lib, o = i.Base, a = i.WordArray, s = n.algo, h = s.SHA1, l = s.HMAC, f = s.PBKDF2 = o.extend({
      cfg: o.extend({
        keySize: 4,
        hasher: h,
        iterations: 1
      }),
      init: function init(e) {
        this.cfg = this.cfg.extend(e);
      },
      compute: function compute(e, t) {
        for (var r = this.cfg, n = l.create(r.hasher, e), i = a.create(), o = a.create([1]), s = i.words, h = o.words, f = r.keySize, c = r.iterations; s.length < f;) {
          var u = n.update(t).finalize(o);
          n.reset();
          for (var d = u.words, p = d.length, _ = u, g = 1; g < c; g++) {
            _ = n.finalize(_), n.reset();
            for (var v = _.words, w = 0; w < p; w++) {
              d[w] ^= v[w];
            }
          }
          i.concat(u), h[0]++;
        }
        return i.sigBytes = 4 * f, i;
      }
    }), n.PBKDF2 = function (e, t, r) {
      return f.create(r).compute(e, t);
    }, r.PBKDF2);
  }), Q(function (e, t) {
    var r, n, i, o, a, s, h, l;
    e.exports = (i = (n = r = ee).lib, o = i.Base, a = i.WordArray, s = n.algo, h = s.MD5, l = s.EvpKDF = o.extend({
      cfg: o.extend({
        keySize: 4,
        hasher: h,
        iterations: 1
      }),
      init: function init(e) {
        this.cfg = this.cfg.extend(e);
      },
      compute: function compute(e, t) {
        for (var r = this.cfg, n = r.hasher.create(), i = a.create(), o = i.words, s = r.keySize, h = r.iterations; o.length < s;) {
          l && n.update(l);
          var l = n.update(e).finalize(t);
          n.reset();
          for (var f = 1; f < h; f++) {
            l = n.finalize(l), n.reset();
          }
          i.concat(l);
        }
        return i.sigBytes = 4 * s, i;
      }
    }), n.EvpKDF = function (e, t, r) {
      return l.create(r).compute(e, t);
    }, r.EvpKDF);
  }), Q(function (e, t) {
    var r, n, i, o, a, s, h, l, f, c, u, d, p, _, g, v, w, b, y, m, k, E, S, x;
    e.exports = void ((r = ee).lib.Cipher || (i = r, o = i.lib, a = o.Base, s = o.WordArray, h = o.BufferedBlockAlgorithm, l = i.enc, l.Utf8, f = l.Base64, c = i.algo, u = c.EvpKDF, d = o.Cipher = h.extend({
      cfg: a.extend(),
      createEncryptor: function createEncryptor(e, t) {
        return this.create(this._ENC_XFORM_MODE, e, t);
      },
      createDecryptor: function createDecryptor(e, t) {
        return this.create(this._DEC_XFORM_MODE, e, t);
      },
      init: function init(e, t, r) {
        this.cfg = this.cfg.extend(r), this._xformMode = e, this._key = t, this.reset();
      },
      reset: function reset() {
        h.reset.call(this), this._doReset();
      },
      process: function process(e) {
        return this._append(e), this._process();
      },
      finalize: function finalize(e) {
        e && this._append(e);
        var t = this._doFinalize();
        return t;
      },
      keySize: 4,
      ivSize: 4,
      _ENC_XFORM_MODE: 1,
      _DEC_XFORM_MODE: 2,
      _createHelper: function () {
        function e(e) {
          return "string" == typeof e ? x : k;
        }
        return function (t) {
          return {
            encrypt: function encrypt(r, n, i) {
              return e(n).encrypt(t, r, n, i);
            },
            decrypt: function decrypt(r, n, i) {
              return e(n).decrypt(t, r, n, i);
            }
          };
        };
      }()
    }), o.StreamCipher = d.extend({
      _doFinalize: function _doFinalize() {
        var e = this._process(!0);
        return e;
      },
      blockSize: 1
    }), p = i.mode = {}, _ = o.BlockCipherMode = a.extend({
      createEncryptor: function createEncryptor(e, t) {
        return this.Encryptor.create(e, t);
      },
      createDecryptor: function createDecryptor(e, t) {
        return this.Decryptor.create(e, t);
      },
      init: function init(e, t) {
        this._cipher = e, this._iv = t;
      }
    }), g = p.CBC = function () {
      var e = _.extend();
      function t(e, t, r) {
        var i = this._iv;
        if (i) {
          var o = i;
          this._iv = n;
        } else var o = this._prevBlock;
        for (var a = 0; a < r; a++) {
          e[t + a] ^= o[a];
        }
      }
      return e.Encryptor = e.extend({
        processBlock: function processBlock(e, r) {
          var n = this._cipher,
            i = n.blockSize;
          t.call(this, e, r, i), n.encryptBlock(e, r), this._prevBlock = e.slice(r, r + i);
        }
      }), e.Decryptor = e.extend({
        processBlock: function processBlock(e, r) {
          var n = this._cipher,
            i = n.blockSize,
            o = e.slice(r, r + i);
          n.decryptBlock(e, r), t.call(this, e, r, i), this._prevBlock = o;
        }
      }), e;
    }(), v = i.pad = {}, w = v.Pkcs7 = {
      pad: function pad(e, t) {
        for (var r = 4 * t, n = r - e.sigBytes % r, i = n << 24 | n << 16 | n << 8 | n, o = [], a = 0; a < n; a += 4) {
          o.push(i);
        }
        var h = s.create(o, n);
        e.concat(h);
      },
      unpad: function unpad(e) {
        var t = 255 & e.words[e.sigBytes - 1 >>> 2];
        e.sigBytes -= t;
      }
    }, o.BlockCipher = d.extend({
      cfg: d.cfg.extend({
        mode: g,
        padding: w
      }),
      reset: function reset() {
        d.reset.call(this);
        var e = this.cfg,
          t = e.iv,
          r = e.mode;
        if (this._xformMode == this._ENC_XFORM_MODE) var n = r.createEncryptor;else {
          var n = r.createDecryptor;
          this._minBufferSize = 1;
        }
        this._mode && this._mode.__creator == n ? this._mode.init(this, t && t.words) : (this._mode = n.call(r, this, t && t.words), this._mode.__creator = n);
      },
      _doProcessBlock: function _doProcessBlock(e, t) {
        this._mode.processBlock(e, t);
      },
      _doFinalize: function _doFinalize() {
        var e = this.cfg.padding;
        if (this._xformMode == this._ENC_XFORM_MODE) {
          e.pad(this._data, this.blockSize);
          var t = this._process(!0);
        } else {
          var t = this._process(!0);
          e.unpad(t);
        }
        return t;
      },
      blockSize: 4
    }), b = o.CipherParams = a.extend({
      init: function init(e) {
        this.mixIn(e);
      },
      toString: function toString(e) {
        return (e || this.formatter).stringify(this);
      }
    }), y = i.format = {}, m = y.OpenSSL = {
      stringify: function stringify(e) {
        var t = e.ciphertext,
          r = e.salt;
        if (r) var n = s.create([1398893684, 1701076831]).concat(r).concat(t);else var n = t;
        return n.toString(f);
      },
      parse: function parse(e) {
        var t = f.parse(e),
          r = t.words;
        if (1398893684 == r[0] && 1701076831 == r[1]) {
          var n = s.create(r.slice(2, 4));
          r.splice(0, 4), t.sigBytes -= 16;
        }
        return b.create({
          ciphertext: t,
          salt: n
        });
      }
    }, k = o.SerializableCipher = a.extend({
      cfg: a.extend({
        format: m
      }),
      encrypt: function encrypt(e, t, r, n) {
        n = this.cfg.extend(n);
        var i = e.createEncryptor(r, n),
          o = i.finalize(t),
          a = i.cfg;
        return b.create({
          ciphertext: o,
          key: r,
          iv: a.iv,
          algorithm: e,
          mode: a.mode,
          padding: a.padding,
          blockSize: e.blockSize,
          formatter: n.format
        });
      },
      decrypt: function decrypt(e, t, r, n) {
        n = this.cfg.extend(n), t = this._parse(t, n.format);
        var i = e.createDecryptor(r, n).finalize(t.ciphertext);
        return i;
      },
      _parse: function _parse(e, t) {
        return "string" == typeof e ? t.parse(e, this) : e;
      }
    }), E = i.kdf = {}, S = E.OpenSSL = {
      execute: function execute(e, t, r, n) {
        n || (n = s.random(8));
        var i = u.create({
            keySize: t + r
          }).compute(e, n),
          o = s.create(i.words.slice(t), 4 * r);
        return i.sigBytes = 4 * t, b.create({
          key: i,
          iv: o,
          salt: n
        });
      }
    }, x = o.PasswordBasedCipher = k.extend({
      cfg: k.cfg.extend({
        kdf: S
      }),
      encrypt: function encrypt(e, t, r, n) {
        var i = (n = this.cfg.extend(n)).kdf.execute(r, e.keySize, e.ivSize);
        n.iv = i.iv;
        var o = k.encrypt.call(this, e, t, i.key, n);
        return o.mixIn(i), o;
      },
      decrypt: function decrypt(e, t, r, n) {
        n = this.cfg.extend(n), t = this._parse(t, n.format);
        var i = n.kdf.execute(r, e.keySize, e.ivSize, t.salt);
        n.iv = i.iv;
        var o = k.decrypt.call(this, e, t, i.key, n);
        return o;
      }
    })));
  }), Q(function (e, t) {
    var r;
    e.exports = ((r = ee).mode.CFB = function () {
      var e = r.lib.BlockCipherMode.extend();
      function t(e, t, r, n) {
        var i = this._iv;
        if (i) {
          var o = i.slice(0);
          this._iv = void 0;
        } else o = this._prevBlock;
        n.encryptBlock(o, 0);
        for (var a = 0; a < r; a++) {
          e[t + a] ^= o[a];
        }
      }
      return e.Encryptor = e.extend({
        processBlock: function processBlock(e, r) {
          var n = this._cipher,
            i = n.blockSize;
          t.call(this, e, r, i, n), this._prevBlock = e.slice(r, r + i);
        }
      }), e.Decryptor = e.extend({
        processBlock: function processBlock(e, r) {
          var n = this._cipher,
            i = n.blockSize,
            o = e.slice(r, r + i);
          t.call(this, e, r, i, n), this._prevBlock = o;
        }
      }), e;
    }(), r.mode.CFB);
  }), Q(function (e, t) {
    var r, n, i;
    e.exports = ((r = ee).mode.CTR = (n = r.lib.BlockCipherMode.extend(), i = n.Encryptor = n.extend({
      processBlock: function processBlock(e, t) {
        var r = this._cipher,
          n = r.blockSize,
          i = this._iv,
          o = this._counter;
        i && (o = this._counter = i.slice(0), this._iv = void 0);
        var a = o.slice(0);
        r.encryptBlock(a, 0), o[n - 1] = o[n - 1] + 1 | 0;
        for (var s = 0; s < n; s++) {
          e[t + s] ^= a[s];
        }
      }
    }), n.Decryptor = i, n), r.mode.CTR);
  }), Q(function (e, t) {
    var r;
    e.exports = ((r = ee).mode.CTRGladman = function () {
      var e = r.lib.BlockCipherMode.extend();
      function t(e) {
        if (255 == (e >> 24 & 255)) {
          var t = e >> 16 & 255,
            r = e >> 8 & 255,
            n = 255 & e;
          255 === t ? (t = 0, 255 === r ? (r = 0, 255 === n ? n = 0 : ++n) : ++r) : ++t, e = 0, e += t << 16, e += r << 8, e += n;
        } else e += 1 << 24;
        return e;
      }
      var n = e.Encryptor = e.extend({
        processBlock: function processBlock(e, r) {
          var n = this._cipher,
            i = n.blockSize,
            o = this._iv,
            a = this._counter;
          o && (a = this._counter = o.slice(0), this._iv = void 0), function (e) {
            0 === (e[0] = t(e[0])) && (e[1] = t(e[1]));
          }(a);
          var s = a.slice(0);
          n.encryptBlock(s, 0);
          for (var h = 0; h < i; h++) {
            e[r + h] ^= s[h];
          }
        }
      });
      return e.Decryptor = n, e;
    }(), r.mode.CTRGladman);
  }), Q(function (e, t) {
    var r, n, i;
    e.exports = ((r = ee).mode.OFB = (n = r.lib.BlockCipherMode.extend(), i = n.Encryptor = n.extend({
      processBlock: function processBlock(e, t) {
        var r = this._cipher,
          n = r.blockSize,
          i = this._iv,
          o = this._keystream;
        i && (o = this._keystream = i.slice(0), this._iv = void 0), r.encryptBlock(o, 0);
        for (var a = 0; a < n; a++) {
          e[t + a] ^= o[a];
        }
      }
    }), n.Decryptor = i, n), r.mode.OFB);
  }), Q(function (e, t) {
    var r, n;
    e.exports = ((r = ee).mode.ECB = ((n = r.lib.BlockCipherMode.extend()).Encryptor = n.extend({
      processBlock: function processBlock(e, t) {
        this._cipher.encryptBlock(e, t);
      }
    }), n.Decryptor = n.extend({
      processBlock: function processBlock(e, t) {
        this._cipher.decryptBlock(e, t);
      }
    }), n), r.mode.ECB);
  }), Q(function (e, t) {
    var r;
    e.exports = ((r = ee).pad.AnsiX923 = {
      pad: function pad(e, t) {
        var r = e.sigBytes,
          n = 4 * t,
          i = n - r % n,
          o = r + i - 1;
        e.clamp(), e.words[o >>> 2] |= i << 24 - o % 4 * 8, e.sigBytes += i;
      },
      unpad: function unpad(e) {
        var t = 255 & e.words[e.sigBytes - 1 >>> 2];
        e.sigBytes -= t;
      }
    }, r.pad.Ansix923);
  }), Q(function (e, t) {
    var r;
    e.exports = ((r = ee).pad.Iso10126 = {
      pad: function pad(e, t) {
        var n = 4 * t,
          i = n - e.sigBytes % n;
        e.concat(r.lib.WordArray.random(i - 1)).concat(r.lib.WordArray.create([i << 24], 1));
      },
      unpad: function unpad(e) {
        var t = 255 & e.words[e.sigBytes - 1 >>> 2];
        e.sigBytes -= t;
      }
    }, r.pad.Iso10126);
  }), Q(function (e, t) {
    var r;
    e.exports = ((r = ee).pad.Iso97971 = {
      pad: function pad(e, t) {
        e.concat(r.lib.WordArray.create([2147483648], 1)), r.pad.ZeroPadding.pad(e, t);
      },
      unpad: function unpad(e) {
        r.pad.ZeroPadding.unpad(e), e.sigBytes--;
      }
    }, r.pad.Iso97971);
  }), Q(function (e, t) {
    var r;
    e.exports = ((r = ee).pad.ZeroPadding = {
      pad: function pad(e, t) {
        var r = 4 * t;
        e.clamp(), e.sigBytes += r - (e.sigBytes % r || r);
      },
      unpad: function unpad(e) {
        for (var t = e.words, r = e.sigBytes - 1; !(t[r >>> 2] >>> 24 - r % 4 * 8 & 255);) {
          r--;
        }
        e.sigBytes = r + 1;
      }
    }, r.pad.ZeroPadding);
  }), Q(function (e, t) {
    var r;
    e.exports = ((r = ee).pad.NoPadding = {
      pad: function pad() {},
      unpad: function unpad() {}
    }, r.pad.NoPadding);
  }), Q(function (e, t) {
    var r, n, i, o;
    e.exports = (i = (n = r = ee).lib.CipherParams, o = n.enc.Hex, n.format.Hex = {
      stringify: function stringify(e) {
        return e.ciphertext.toString(o);
      },
      parse: function parse(e) {
        var t = o.parse(e);
        return i.create({
          ciphertext: t
        });
      }
    }, r.format.Hex);
  }), Q(function (e, t) {
    var r;
    e.exports = (r = ee, function () {
      var e = r,
        t = e.lib.BlockCipher,
        n = e.algo,
        i = [],
        o = [],
        a = [],
        s = [],
        h = [],
        l = [],
        f = [],
        c = [],
        u = [],
        d = [];
      !function () {
        for (var e = [], t = 0; t < 256; t++) {
          e[t] = t < 128 ? t << 1 : t << 1 ^ 283;
        }
        var r = 0,
          n = 0;
        for (t = 0; t < 256; t++) {
          var p = n ^ n << 1 ^ n << 2 ^ n << 3 ^ n << 4;
          p = p >>> 8 ^ 255 & p ^ 99, i[r] = p, o[p] = r;
          var _ = e[r],
            g = e[_],
            v = e[g],
            w = 257 * e[p] ^ 16843008 * p;
          a[r] = w << 24 | w >>> 8, s[r] = w << 16 | w >>> 16, h[r] = w << 8 | w >>> 24, l[r] = w, w = 16843009 * v ^ 65537 * g ^ 257 * _ ^ 16843008 * r, f[p] = w << 24 | w >>> 8, c[p] = w << 16 | w >>> 16, u[p] = w << 8 | w >>> 24, d[p] = w, r ? (r = _ ^ e[e[e[v ^ _]]], n ^= e[e[n]]) : r = n = 1;
        }
      }();
      var p = [0, 1, 2, 4, 8, 16, 32, 64, 128, 27, 54],
        _ = n.AES = t.extend({
          _doReset: function _doReset() {
            if (!this._nRounds || this._keyPriorReset !== this._key) {
              for (var e = this._keyPriorReset = this._key, t = e.words, r = e.sigBytes / 4, n = 4 * ((this._nRounds = r + 6) + 1), o = this._keySchedule = [], a = 0; a < n; a++) {
                if (a < r) o[a] = t[a];else {
                  var s = o[a - 1];
                  a % r ? r > 6 && a % r == 4 && (s = i[s >>> 24] << 24 | i[s >>> 16 & 255] << 16 | i[s >>> 8 & 255] << 8 | i[255 & s]) : (s = i[(s = s << 8 | s >>> 24) >>> 24] << 24 | i[s >>> 16 & 255] << 16 | i[s >>> 8 & 255] << 8 | i[255 & s], s ^= p[a / r | 0] << 24), o[a] = o[a - r] ^ s;
                }
              }
              for (var h = this._invKeySchedule = [], l = 0; l < n; l++) {
                a = n - l, s = l % 4 ? o[a] : o[a - 4], h[l] = l < 4 || a <= 4 ? s : f[i[s >>> 24]] ^ c[i[s >>> 16 & 255]] ^ u[i[s >>> 8 & 255]] ^ d[i[255 & s]];
              }
            }
          },
          encryptBlock: function encryptBlock(e, t) {
            this._doCryptBlock(e, t, this._keySchedule, a, s, h, l, i);
          },
          decryptBlock: function decryptBlock(e, t) {
            var r = e[t + 1];
            e[t + 1] = e[t + 3], e[t + 3] = r, this._doCryptBlock(e, t, this._invKeySchedule, f, c, u, d, o), r = e[t + 1], e[t + 1] = e[t + 3], e[t + 3] = r;
          },
          _doCryptBlock: function _doCryptBlock(e, t, r, n, i, o, a, s) {
            for (var h = this._nRounds, l = e[t] ^ r[0], f = e[t + 1] ^ r[1], c = e[t + 2] ^ r[2], u = e[t + 3] ^ r[3], d = 4, p = 1; p < h; p++) {
              var _ = n[l >>> 24] ^ i[f >>> 16 & 255] ^ o[c >>> 8 & 255] ^ a[255 & u] ^ r[d++],
                g = n[f >>> 24] ^ i[c >>> 16 & 255] ^ o[u >>> 8 & 255] ^ a[255 & l] ^ r[d++],
                v = n[c >>> 24] ^ i[u >>> 16 & 255] ^ o[l >>> 8 & 255] ^ a[255 & f] ^ r[d++],
                w = n[u >>> 24] ^ i[l >>> 16 & 255] ^ o[f >>> 8 & 255] ^ a[255 & c] ^ r[d++];
              l = _, f = g, c = v, u = w;
            }
            _ = (s[l >>> 24] << 24 | s[f >>> 16 & 255] << 16 | s[c >>> 8 & 255] << 8 | s[255 & u]) ^ r[d++], g = (s[f >>> 24] << 24 | s[c >>> 16 & 255] << 16 | s[u >>> 8 & 255] << 8 | s[255 & l]) ^ r[d++], v = (s[c >>> 24] << 24 | s[u >>> 16 & 255] << 16 | s[l >>> 8 & 255] << 8 | s[255 & f]) ^ r[d++], w = (s[u >>> 24] << 24 | s[l >>> 16 & 255] << 16 | s[f >>> 8 & 255] << 8 | s[255 & c]) ^ r[d++], e[t] = _, e[t + 1] = g, e[t + 2] = v, e[t + 3] = w;
          },
          keySize: 8
        });
      e.AES = t._createHelper(_);
    }(), r.AES);
  }), Q(function (e, t) {
    var r;
    e.exports = (r = ee, function () {
      var e = r,
        t = e.lib,
        n = t.WordArray,
        i = t.BlockCipher,
        o = e.algo,
        a = [57, 49, 41, 33, 25, 17, 9, 1, 58, 50, 42, 34, 26, 18, 10, 2, 59, 51, 43, 35, 27, 19, 11, 3, 60, 52, 44, 36, 63, 55, 47, 39, 31, 23, 15, 7, 62, 54, 46, 38, 30, 22, 14, 6, 61, 53, 45, 37, 29, 21, 13, 5, 28, 20, 12, 4],
        s = [14, 17, 11, 24, 1, 5, 3, 28, 15, 6, 21, 10, 23, 19, 12, 4, 26, 8, 16, 7, 27, 20, 13, 2, 41, 52, 31, 37, 47, 55, 30, 40, 51, 45, 33, 48, 44, 49, 39, 56, 34, 53, 46, 42, 50, 36, 29, 32],
        h = [1, 2, 4, 6, 8, 10, 12, 14, 15, 17, 19, 21, 23, 25, 27, 28],
        l = [{
          0: 8421888,
          268435456: 32768,
          536870912: 8421378,
          805306368: 2,
          1073741824: 512,
          1342177280: 8421890,
          1610612736: 8389122,
          1879048192: 8388608,
          2147483648: 514,
          2415919104: 8389120,
          2684354560: 33280,
          2952790016: 8421376,
          3221225472: 32770,
          3489660928: 8388610,
          3758096384: 0,
          4026531840: 33282,
          134217728: 0,
          402653184: 8421890,
          671088640: 33282,
          939524096: 32768,
          1207959552: 8421888,
          1476395008: 512,
          1744830464: 8421378,
          2013265920: 2,
          2281701376: 8389120,
          2550136832: 33280,
          2818572288: 8421376,
          3087007744: 8389122,
          3355443200: 8388610,
          3623878656: 32770,
          3892314112: 514,
          4160749568: 8388608,
          1: 32768,
          268435457: 2,
          536870913: 8421888,
          805306369: 8388608,
          1073741825: 8421378,
          1342177281: 33280,
          1610612737: 512,
          1879048193: 8389122,
          2147483649: 8421890,
          2415919105: 8421376,
          2684354561: 8388610,
          2952790017: 33282,
          3221225473: 514,
          3489660929: 8389120,
          3758096385: 32770,
          4026531841: 0,
          134217729: 8421890,
          402653185: 8421376,
          671088641: 8388608,
          939524097: 512,
          1207959553: 32768,
          1476395009: 8388610,
          1744830465: 2,
          2013265921: 33282,
          2281701377: 32770,
          2550136833: 8389122,
          2818572289: 514,
          3087007745: 8421888,
          3355443201: 8389120,
          3623878657: 0,
          3892314113: 33280,
          4160749569: 8421378
        }, {
          0: 1074282512,
          16777216: 16384,
          33554432: 524288,
          50331648: 1074266128,
          67108864: 1073741840,
          83886080: 1074282496,
          100663296: 1073758208,
          117440512: 16,
          134217728: 540672,
          150994944: 1073758224,
          167772160: 1073741824,
          184549376: 540688,
          201326592: 524304,
          218103808: 0,
          234881024: 16400,
          251658240: 1074266112,
          8388608: 1073758208,
          25165824: 540688,
          41943040: 16,
          58720256: 1073758224,
          75497472: 1074282512,
          92274688: 1073741824,
          109051904: 524288,
          125829120: 1074266128,
          142606336: 524304,
          159383552: 0,
          176160768: 16384,
          192937984: 1074266112,
          209715200: 1073741840,
          226492416: 540672,
          243269632: 1074282496,
          260046848: 16400,
          268435456: 0,
          285212672: 1074266128,
          301989888: 1073758224,
          318767104: 1074282496,
          335544320: 1074266112,
          352321536: 16,
          369098752: 540688,
          385875968: 16384,
          402653184: 16400,
          419430400: 524288,
          436207616: 524304,
          452984832: 1073741840,
          469762048: 540672,
          486539264: 1073758208,
          503316480: 1073741824,
          520093696: 1074282512,
          276824064: 540688,
          293601280: 524288,
          310378496: 1074266112,
          327155712: 16384,
          343932928: 1073758208,
          360710144: 1074282512,
          377487360: 16,
          394264576: 1073741824,
          411041792: 1074282496,
          427819008: 1073741840,
          444596224: 1073758224,
          461373440: 524304,
          478150656: 0,
          494927872: 16400,
          511705088: 1074266128,
          528482304: 540672
        }, {
          0: 260,
          1048576: 0,
          2097152: 67109120,
          3145728: 65796,
          4194304: 65540,
          5242880: 67108868,
          6291456: 67174660,
          7340032: 67174400,
          8388608: 67108864,
          9437184: 67174656,
          10485760: 65792,
          11534336: 67174404,
          12582912: 67109124,
          13631488: 65536,
          14680064: 4,
          15728640: 256,
          524288: 67174656,
          1572864: 67174404,
          2621440: 0,
          3670016: 67109120,
          4718592: 67108868,
          5767168: 65536,
          6815744: 65540,
          7864320: 260,
          8912896: 4,
          9961472: 256,
          11010048: 67174400,
          12058624: 65796,
          13107200: 65792,
          14155776: 67109124,
          15204352: 67174660,
          16252928: 67108864,
          16777216: 67174656,
          17825792: 65540,
          18874368: 65536,
          19922944: 67109120,
          20971520: 256,
          22020096: 67174660,
          23068672: 67108868,
          24117248: 0,
          25165824: 67109124,
          26214400: 67108864,
          27262976: 4,
          28311552: 65792,
          29360128: 67174400,
          30408704: 260,
          31457280: 65796,
          32505856: 67174404,
          17301504: 67108864,
          18350080: 260,
          19398656: 67174656,
          20447232: 0,
          21495808: 65540,
          22544384: 67109120,
          23592960: 256,
          24641536: 67174404,
          25690112: 65536,
          26738688: 67174660,
          27787264: 65796,
          28835840: 67108868,
          29884416: 67109124,
          30932992: 67174400,
          31981568: 4,
          33030144: 65792
        }, {
          0: 2151682048,
          65536: 2147487808,
          131072: 4198464,
          196608: 2151677952,
          262144: 0,
          327680: 4198400,
          393216: 2147483712,
          458752: 4194368,
          524288: 2147483648,
          589824: 4194304,
          655360: 64,
          720896: 2147487744,
          786432: 2151678016,
          851968: 4160,
          917504: 4096,
          983040: 2151682112,
          32768: 2147487808,
          98304: 64,
          163840: 2151678016,
          229376: 2147487744,
          294912: 4198400,
          360448: 2151682112,
          425984: 0,
          491520: 2151677952,
          557056: 4096,
          622592: 2151682048,
          688128: 4194304,
          753664: 4160,
          819200: 2147483648,
          884736: 4194368,
          950272: 4198464,
          1015808: 2147483712,
          1048576: 4194368,
          1114112: 4198400,
          1179648: 2147483712,
          1245184: 0,
          1310720: 4160,
          1376256: 2151678016,
          1441792: 2151682048,
          1507328: 2147487808,
          1572864: 2151682112,
          1638400: 2147483648,
          1703936: 2151677952,
          1769472: 4198464,
          1835008: 2147487744,
          1900544: 4194304,
          1966080: 64,
          2031616: 4096,
          1081344: 2151677952,
          1146880: 2151682112,
          1212416: 0,
          1277952: 4198400,
          1343488: 4194368,
          1409024: 2147483648,
          1474560: 2147487808,
          1540096: 64,
          1605632: 2147483712,
          1671168: 4096,
          1736704: 2147487744,
          1802240: 2151678016,
          1867776: 4160,
          1933312: 2151682048,
          1998848: 4194304,
          2064384: 4198464
        }, {
          0: 128,
          4096: 17039360,
          8192: 262144,
          12288: 536870912,
          16384: 537133184,
          20480: 16777344,
          24576: 553648256,
          28672: 262272,
          32768: 16777216,
          36864: 537133056,
          40960: 536871040,
          45056: 553910400,
          49152: 553910272,
          53248: 0,
          57344: 17039488,
          61440: 553648128,
          2048: 17039488,
          6144: 553648256,
          10240: 128,
          14336: 17039360,
          18432: 262144,
          22528: 537133184,
          26624: 553910272,
          30720: 536870912,
          34816: 537133056,
          38912: 0,
          43008: 553910400,
          47104: 16777344,
          51200: 536871040,
          55296: 553648128,
          59392: 16777216,
          63488: 262272,
          65536: 262144,
          69632: 128,
          73728: 536870912,
          77824: 553648256,
          81920: 16777344,
          86016: 553910272,
          90112: 537133184,
          94208: 16777216,
          98304: 553910400,
          102400: 553648128,
          106496: 17039360,
          110592: 537133056,
          114688: 262272,
          118784: 536871040,
          122880: 0,
          126976: 17039488,
          67584: 553648256,
          71680: 16777216,
          75776: 17039360,
          79872: 537133184,
          83968: 536870912,
          88064: 17039488,
          92160: 128,
          96256: 553910272,
          100352: 262272,
          104448: 553910400,
          108544: 0,
          112640: 553648128,
          116736: 16777344,
          120832: 262144,
          124928: 537133056,
          129024: 536871040
        }, {
          0: 268435464,
          256: 8192,
          512: 270532608,
          768: 270540808,
          1024: 268443648,
          1280: 2097152,
          1536: 2097160,
          1792: 268435456,
          2048: 0,
          2304: 268443656,
          2560: 2105344,
          2816: 8,
          3072: 270532616,
          3328: 2105352,
          3584: 8200,
          3840: 270540800,
          128: 270532608,
          384: 270540808,
          640: 8,
          896: 2097152,
          1152: 2105352,
          1408: 268435464,
          1664: 268443648,
          1920: 8200,
          2176: 2097160,
          2432: 8192,
          2688: 268443656,
          2944: 270532616,
          3200: 0,
          3456: 270540800,
          3712: 2105344,
          3968: 268435456,
          4096: 268443648,
          4352: 270532616,
          4608: 270540808,
          4864: 8200,
          5120: 2097152,
          5376: 268435456,
          5632: 268435464,
          5888: 2105344,
          6144: 2105352,
          6400: 0,
          6656: 8,
          6912: 270532608,
          7168: 8192,
          7424: 268443656,
          7680: 270540800,
          7936: 2097160,
          4224: 8,
          4480: 2105344,
          4736: 2097152,
          4992: 268435464,
          5248: 268443648,
          5504: 8200,
          5760: 270540808,
          6016: 270532608,
          6272: 270540800,
          6528: 270532616,
          6784: 8192,
          7040: 2105352,
          7296: 2097160,
          7552: 0,
          7808: 268435456,
          8064: 268443656
        }, {
          0: 1048576,
          16: 33555457,
          32: 1024,
          48: 1049601,
          64: 34604033,
          80: 0,
          96: 1,
          112: 34603009,
          128: 33555456,
          144: 1048577,
          160: 33554433,
          176: 34604032,
          192: 34603008,
          208: 1025,
          224: 1049600,
          240: 33554432,
          8: 34603009,
          24: 0,
          40: 33555457,
          56: 34604032,
          72: 1048576,
          88: 33554433,
          104: 33554432,
          120: 1025,
          136: 1049601,
          152: 33555456,
          168: 34603008,
          184: 1048577,
          200: 1024,
          216: 34604033,
          232: 1,
          248: 1049600,
          256: 33554432,
          272: 1048576,
          288: 33555457,
          304: 34603009,
          320: 1048577,
          336: 33555456,
          352: 34604032,
          368: 1049601,
          384: 1025,
          400: 34604033,
          416: 1049600,
          432: 1,
          448: 0,
          464: 34603008,
          480: 33554433,
          496: 1024,
          264: 1049600,
          280: 33555457,
          296: 34603009,
          312: 1,
          328: 33554432,
          344: 1048576,
          360: 1025,
          376: 34604032,
          392: 33554433,
          408: 34603008,
          424: 0,
          440: 34604033,
          456: 1049601,
          472: 1024,
          488: 33555456,
          504: 1048577
        }, {
          0: 134219808,
          1: 131072,
          2: 134217728,
          3: 32,
          4: 131104,
          5: 134350880,
          6: 134350848,
          7: 2048,
          8: 134348800,
          9: 134219776,
          10: 133120,
          11: 134348832,
          12: 2080,
          13: 0,
          14: 134217760,
          15: 133152,
          2147483648: 2048,
          2147483649: 134350880,
          2147483650: 134219808,
          2147483651: 134217728,
          2147483652: 134348800,
          2147483653: 133120,
          2147483654: 133152,
          2147483655: 32,
          2147483656: 134217760,
          2147483657: 2080,
          2147483658: 131104,
          2147483659: 134350848,
          2147483660: 0,
          2147483661: 134348832,
          2147483662: 134219776,
          2147483663: 131072,
          16: 133152,
          17: 134350848,
          18: 32,
          19: 2048,
          20: 134219776,
          21: 134217760,
          22: 134348832,
          23: 131072,
          24: 0,
          25: 131104,
          26: 134348800,
          27: 134219808,
          28: 134350880,
          29: 133120,
          30: 2080,
          31: 134217728,
          2147483664: 131072,
          2147483665: 2048,
          2147483666: 134348832,
          2147483667: 133152,
          2147483668: 32,
          2147483669: 134348800,
          2147483670: 134217728,
          2147483671: 134219808,
          2147483672: 134350880,
          2147483673: 134217760,
          2147483674: 134219776,
          2147483675: 0,
          2147483676: 133120,
          2147483677: 2080,
          2147483678: 131104,
          2147483679: 134350848
        }],
        f = [4160749569, 528482304, 33030144, 2064384, 129024, 8064, 504, 2147483679],
        c = o.DES = i.extend({
          _doReset: function _doReset() {
            for (var e = this._key.words, t = [], r = 0; r < 56; r++) {
              var n = a[r] - 1;
              t[r] = e[n >>> 5] >>> 31 - n % 32 & 1;
            }
            for (var i = this._subKeys = [], o = 0; o < 16; o++) {
              var l = i[o] = [],
                f = h[o];
              for (r = 0; r < 24; r++) {
                l[r / 6 | 0] |= t[(s[r] - 1 + f) % 28] << 31 - r % 6, l[4 + (r / 6 | 0)] |= t[28 + (s[r + 24] - 1 + f) % 28] << 31 - r % 6;
              }
              for (l[0] = l[0] << 1 | l[0] >>> 31, r = 1; r < 7; r++) {
                l[r] = l[r] >>> 4 * (r - 1) + 3;
              }
              l[7] = l[7] << 5 | l[7] >>> 27;
            }
            var c = this._invSubKeys = [];
            for (r = 0; r < 16; r++) {
              c[r] = i[15 - r];
            }
          },
          encryptBlock: function encryptBlock(e, t) {
            this._doCryptBlock(e, t, this._subKeys);
          },
          decryptBlock: function decryptBlock(e, t) {
            this._doCryptBlock(e, t, this._invSubKeys);
          },
          _doCryptBlock: function _doCryptBlock(e, t, r) {
            this._lBlock = e[t], this._rBlock = e[t + 1], u.call(this, 4, 252645135), u.call(this, 16, 65535), d.call(this, 2, 858993459), d.call(this, 8, 16711935), u.call(this, 1, 1431655765);
            for (var n = 0; n < 16; n++) {
              for (var i = r[n], o = this._lBlock, a = this._rBlock, s = 0, h = 0; h < 8; h++) {
                s |= l[h][((a ^ i[h]) & f[h]) >>> 0];
              }
              this._lBlock = a, this._rBlock = o ^ s;
            }
            var c = this._lBlock;
            this._lBlock = this._rBlock, this._rBlock = c, u.call(this, 1, 1431655765), d.call(this, 8, 16711935), d.call(this, 2, 858993459), u.call(this, 16, 65535), u.call(this, 4, 252645135), e[t] = this._lBlock, e[t + 1] = this._rBlock;
          },
          keySize: 2,
          ivSize: 2,
          blockSize: 2
        });
      function u(e, t) {
        var r = (this._lBlock >>> e ^ this._rBlock) & t;
        this._rBlock ^= r, this._lBlock ^= r << e;
      }
      function d(e, t) {
        var r = (this._rBlock >>> e ^ this._lBlock) & t;
        this._lBlock ^= r, this._rBlock ^= r << e;
      }
      e.DES = i._createHelper(c);
      var p = o.TripleDES = i.extend({
        _doReset: function _doReset() {
          var e = this._key.words;
          this._des1 = c.createEncryptor(n.create(e.slice(0, 2))), this._des2 = c.createEncryptor(n.create(e.slice(2, 4))), this._des3 = c.createEncryptor(n.create(e.slice(4, 6)));
        },
        encryptBlock: function encryptBlock(e, t) {
          this._des1.encryptBlock(e, t), this._des2.decryptBlock(e, t), this._des3.encryptBlock(e, t);
        },
        decryptBlock: function decryptBlock(e, t) {
          this._des3.decryptBlock(e, t), this._des2.encryptBlock(e, t), this._des1.decryptBlock(e, t);
        },
        keySize: 6,
        ivSize: 2,
        blockSize: 2
      });
      e.TripleDES = i._createHelper(p);
    }(), r.TripleDES);
  }), Q(function (e, t) {
    var r;
    e.exports = (r = ee, function () {
      var e = r,
        t = e.lib.StreamCipher,
        n = e.algo,
        i = n.RC4 = t.extend({
          _doReset: function _doReset() {
            for (var e = this._key, t = e.words, r = e.sigBytes, n = this._S = [], i = 0; i < 256; i++) {
              n[i] = i;
            }
            i = 0;
            for (var o = 0; i < 256; i++) {
              var a = i % r,
                s = t[a >>> 2] >>> 24 - a % 4 * 8 & 255;
              o = (o + n[i] + s) % 256;
              var h = n[i];
              n[i] = n[o], n[o] = h;
            }
            this._i = this._j = 0;
          },
          _doProcessBlock: function _doProcessBlock(e, t) {
            e[t] ^= o.call(this);
          },
          keySize: 8,
          ivSize: 0
        });
      function o() {
        for (var e = this._S, t = this._i, r = this._j, n = 0, i = 0; i < 4; i++) {
          r = (r + e[t = (t + 1) % 256]) % 256;
          var o = e[t];
          e[t] = e[r], e[r] = o, n |= e[(e[t] + e[r]) % 256] << 24 - 8 * i;
        }
        return this._i = t, this._j = r, n;
      }
      e.RC4 = t._createHelper(i);
      var a = n.RC4Drop = i.extend({
        cfg: i.cfg.extend({
          drop: 192
        }),
        _doReset: function _doReset() {
          i._doReset.call(this);
          for (var e = this.cfg.drop; e > 0; e--) {
            o.call(this);
          }
        }
      });
      e.RC4Drop = t._createHelper(a);
    }(), r.RC4);
  }), Q(function (e, t) {
    var r;
    e.exports = (r = ee, function () {
      var e = r,
        t = e.lib.StreamCipher,
        n = e.algo,
        i = [],
        o = [],
        a = [],
        s = n.Rabbit = t.extend({
          _doReset: function _doReset() {
            for (var e = this._key.words, t = this.cfg.iv, r = 0; r < 4; r++) {
              e[r] = 16711935 & (e[r] << 8 | e[r] >>> 24) | 4278255360 & (e[r] << 24 | e[r] >>> 8);
            }
            var n = this._X = [e[0], e[3] << 16 | e[2] >>> 16, e[1], e[0] << 16 | e[3] >>> 16, e[2], e[1] << 16 | e[0] >>> 16, e[3], e[2] << 16 | e[1] >>> 16],
              i = this._C = [e[2] << 16 | e[2] >>> 16, 4294901760 & e[0] | 65535 & e[1], e[3] << 16 | e[3] >>> 16, 4294901760 & e[1] | 65535 & e[2], e[0] << 16 | e[0] >>> 16, 4294901760 & e[2] | 65535 & e[3], e[1] << 16 | e[1] >>> 16, 4294901760 & e[3] | 65535 & e[0]];
            for (this._b = 0, r = 0; r < 4; r++) {
              h.call(this);
            }
            for (r = 0; r < 8; r++) {
              i[r] ^= n[r + 4 & 7];
            }
            if (t) {
              var o = t.words,
                a = o[0],
                s = o[1],
                l = 16711935 & (a << 8 | a >>> 24) | 4278255360 & (a << 24 | a >>> 8),
                f = 16711935 & (s << 8 | s >>> 24) | 4278255360 & (s << 24 | s >>> 8),
                c = l >>> 16 | 4294901760 & f,
                u = f << 16 | 65535 & l;
              for (i[0] ^= l, i[1] ^= c, i[2] ^= f, i[3] ^= u, i[4] ^= l, i[5] ^= c, i[6] ^= f, i[7] ^= u, r = 0; r < 4; r++) {
                h.call(this);
              }
            }
          },
          _doProcessBlock: function _doProcessBlock(e, t) {
            var r = this._X;
            h.call(this), i[0] = r[0] ^ r[5] >>> 16 ^ r[3] << 16, i[1] = r[2] ^ r[7] >>> 16 ^ r[5] << 16, i[2] = r[4] ^ r[1] >>> 16 ^ r[7] << 16, i[3] = r[6] ^ r[3] >>> 16 ^ r[1] << 16;
            for (var n = 0; n < 4; n++) {
              i[n] = 16711935 & (i[n] << 8 | i[n] >>> 24) | 4278255360 & (i[n] << 24 | i[n] >>> 8), e[t + n] ^= i[n];
            }
          },
          blockSize: 4,
          ivSize: 2
        });
      function h() {
        for (var e = this._X, t = this._C, r = 0; r < 8; r++) {
          o[r] = t[r];
        }
        for (t[0] = t[0] + 1295307597 + this._b | 0, t[1] = t[1] + 3545052371 + (t[0] >>> 0 < o[0] >>> 0 ? 1 : 0) | 0, t[2] = t[2] + 886263092 + (t[1] >>> 0 < o[1] >>> 0 ? 1 : 0) | 0, t[3] = t[3] + 1295307597 + (t[2] >>> 0 < o[2] >>> 0 ? 1 : 0) | 0, t[4] = t[4] + 3545052371 + (t[3] >>> 0 < o[3] >>> 0 ? 1 : 0) | 0, t[5] = t[5] + 886263092 + (t[4] >>> 0 < o[4] >>> 0 ? 1 : 0) | 0, t[6] = t[6] + 1295307597 + (t[5] >>> 0 < o[5] >>> 0 ? 1 : 0) | 0, t[7] = t[7] + 3545052371 + (t[6] >>> 0 < o[6] >>> 0 ? 1 : 0) | 0, this._b = t[7] >>> 0 < o[7] >>> 0 ? 1 : 0, r = 0; r < 8; r++) {
          var n = e[r] + t[r],
            i = 65535 & n,
            s = n >>> 16,
            h = ((i * i >>> 17) + i * s >>> 15) + s * s,
            l = ((4294901760 & n) * n | 0) + ((65535 & n) * n | 0);
          a[r] = h ^ l;
        }
        e[0] = a[0] + (a[7] << 16 | a[7] >>> 16) + (a[6] << 16 | a[6] >>> 16) | 0, e[1] = a[1] + (a[0] << 8 | a[0] >>> 24) + a[7] | 0, e[2] = a[2] + (a[1] << 16 | a[1] >>> 16) + (a[0] << 16 | a[0] >>> 16) | 0, e[3] = a[3] + (a[2] << 8 | a[2] >>> 24) + a[1] | 0, e[4] = a[4] + (a[3] << 16 | a[3] >>> 16) + (a[2] << 16 | a[2] >>> 16) | 0, e[5] = a[5] + (a[4] << 8 | a[4] >>> 24) + a[3] | 0, e[6] = a[6] + (a[5] << 16 | a[5] >>> 16) + (a[4] << 16 | a[4] >>> 16) | 0, e[7] = a[7] + (a[6] << 8 | a[6] >>> 24) + a[5] | 0;
      }
      e.Rabbit = t._createHelper(s);
    }(), r.Rabbit);
  }), Q(function (e, t) {
    var r;
    e.exports = (r = ee, function () {
      var e = r,
        t = e.lib.StreamCipher,
        n = e.algo,
        i = [],
        o = [],
        a = [],
        s = n.RabbitLegacy = t.extend({
          _doReset: function _doReset() {
            var e = this._key.words,
              t = this.cfg.iv,
              r = this._X = [e[0], e[3] << 16 | e[2] >>> 16, e[1], e[0] << 16 | e[3] >>> 16, e[2], e[1] << 16 | e[0] >>> 16, e[3], e[2] << 16 | e[1] >>> 16],
              n = this._C = [e[2] << 16 | e[2] >>> 16, 4294901760 & e[0] | 65535 & e[1], e[3] << 16 | e[3] >>> 16, 4294901760 & e[1] | 65535 & e[2], e[0] << 16 | e[0] >>> 16, 4294901760 & e[2] | 65535 & e[3], e[1] << 16 | e[1] >>> 16, 4294901760 & e[3] | 65535 & e[0]];
            this._b = 0;
            for (var i = 0; i < 4; i++) {
              h.call(this);
            }
            for (i = 0; i < 8; i++) {
              n[i] ^= r[i + 4 & 7];
            }
            if (t) {
              var o = t.words,
                a = o[0],
                s = o[1],
                l = 16711935 & (a << 8 | a >>> 24) | 4278255360 & (a << 24 | a >>> 8),
                f = 16711935 & (s << 8 | s >>> 24) | 4278255360 & (s << 24 | s >>> 8),
                c = l >>> 16 | 4294901760 & f,
                u = f << 16 | 65535 & l;
              for (n[0] ^= l, n[1] ^= c, n[2] ^= f, n[3] ^= u, n[4] ^= l, n[5] ^= c, n[6] ^= f, n[7] ^= u, i = 0; i < 4; i++) {
                h.call(this);
              }
            }
          },
          _doProcessBlock: function _doProcessBlock(e, t) {
            var r = this._X;
            h.call(this), i[0] = r[0] ^ r[5] >>> 16 ^ r[3] << 16, i[1] = r[2] ^ r[7] >>> 16 ^ r[5] << 16, i[2] = r[4] ^ r[1] >>> 16 ^ r[7] << 16, i[3] = r[6] ^ r[3] >>> 16 ^ r[1] << 16;
            for (var n = 0; n < 4; n++) {
              i[n] = 16711935 & (i[n] << 8 | i[n] >>> 24) | 4278255360 & (i[n] << 24 | i[n] >>> 8), e[t + n] ^= i[n];
            }
          },
          blockSize: 4,
          ivSize: 2
        });
      function h() {
        for (var e = this._X, t = this._C, r = 0; r < 8; r++) {
          o[r] = t[r];
        }
        for (t[0] = t[0] + 1295307597 + this._b | 0, t[1] = t[1] + 3545052371 + (t[0] >>> 0 < o[0] >>> 0 ? 1 : 0) | 0, t[2] = t[2] + 886263092 + (t[1] >>> 0 < o[1] >>> 0 ? 1 : 0) | 0, t[3] = t[3] + 1295307597 + (t[2] >>> 0 < o[2] >>> 0 ? 1 : 0) | 0, t[4] = t[4] + 3545052371 + (t[3] >>> 0 < o[3] >>> 0 ? 1 : 0) | 0, t[5] = t[5] + 886263092 + (t[4] >>> 0 < o[4] >>> 0 ? 1 : 0) | 0, t[6] = t[6] + 1295307597 + (t[5] >>> 0 < o[5] >>> 0 ? 1 : 0) | 0, t[7] = t[7] + 3545052371 + (t[6] >>> 0 < o[6] >>> 0 ? 1 : 0) | 0, this._b = t[7] >>> 0 < o[7] >>> 0 ? 1 : 0, r = 0; r < 8; r++) {
          var n = e[r] + t[r],
            i = 65535 & n,
            s = n >>> 16,
            h = ((i * i >>> 17) + i * s >>> 15) + s * s,
            l = ((4294901760 & n) * n | 0) + ((65535 & n) * n | 0);
          a[r] = h ^ l;
        }
        e[0] = a[0] + (a[7] << 16 | a[7] >>> 16) + (a[6] << 16 | a[6] >>> 16) | 0, e[1] = a[1] + (a[0] << 8 | a[0] >>> 24) + a[7] | 0, e[2] = a[2] + (a[1] << 16 | a[1] >>> 16) + (a[0] << 16 | a[0] >>> 16) | 0, e[3] = a[3] + (a[2] << 8 | a[2] >>> 24) + a[1] | 0, e[4] = a[4] + (a[3] << 16 | a[3] >>> 16) + (a[2] << 16 | a[2] >>> 16) | 0, e[5] = a[5] + (a[4] << 8 | a[4] >>> 24) + a[3] | 0, e[6] = a[6] + (a[5] << 16 | a[5] >>> 16) + (a[4] << 16 | a[4] >>> 16) | 0, e[7] = a[7] + (a[6] << 8 | a[6] >>> 24) + a[5] | 0;
      }
      e.RabbitLegacy = t._createHelper(s);
    }(), r.RabbitLegacy);
  }), Q(function (e, t) {
    e.exports = ee;
  }));
function re() {
  throw new Error("setTimeout has not been defined");
}
function ne() {
  throw new Error("clearTimeout has not been defined");
}
var ie = re,
  oe = ne;
function ae(e) {
  if (ie === setTimeout) return setTimeout(e, 0);
  if ((ie === re || !ie) && setTimeout) return ie = setTimeout, setTimeout(e, 0);
  try {
    return ie(e, 0);
  } catch (t) {
    try {
      return ie.call(null, e, 0);
    } catch (t) {
      return ie.call(this, e, 0);
    }
  }
}
"function" == typeof e.setTimeout && (ie = setTimeout), "function" == typeof e.clearTimeout && (oe = clearTimeout);
var se,
  he = [],
  le = !1,
  fe = -1;
function ce() {
  le && se && (le = !1, se.length ? he = se.concat(he) : fe = -1, he.length && ue());
}
function ue() {
  if (!le) {
    var e = ae(ce);
    le = !0;
    for (var t = he.length; t;) {
      for (se = he, he = []; ++fe < t;) {
        se && se[fe].run();
      }
      fe = -1, t = he.length;
    }
    se = null, le = !1, function (e) {
      if (oe === clearTimeout) return clearTimeout(e);
      if ((oe === ne || !oe) && clearTimeout) return oe = clearTimeout, clearTimeout(e);
      try {
        oe(e);
      } catch (t) {
        try {
          return oe.call(null, e);
        } catch (t) {
          return oe.call(this, e);
        }
      }
    }(e);
  }
}
function de(e) {
  var t = new Array(arguments.length - 1);
  if (arguments.length > 1) for (var r = 1; r < arguments.length; r++) {
    t[r - 1] = arguments[r];
  }
  he.push(new pe(e, t)), 1 !== he.length || le || ae(ue);
}
function pe(e, t) {
  this.fun = e, this.array = t;
}
pe.prototype.run = function () {
  this.fun.apply(null, this.array);
};
var _e = {};
_e.now || _e.mozNow || _e.msNow || _e.oNow || _e.webkitNow;
function ge() {}
function ve() {
  ve.init.call(this);
}
function we(e) {
  return void 0 === e._maxListeners ? ve.defaultMaxListeners : e._maxListeners;
}
function be(e, t, r) {
  if (t) e.call(r);else for (var n = e.length, i = Ae(e, n), o = 0; o < n; ++o) {
    i[o].call(r);
  }
}
function ye(e, t, r, n) {
  if (t) e.call(r, n);else for (var i = e.length, o = Ae(e, i), a = 0; a < i; ++a) {
    o[a].call(r, n);
  }
}
function me(e, t, r, n, i) {
  if (t) e.call(r, n, i);else for (var o = e.length, a = Ae(e, o), s = 0; s < o; ++s) {
    a[s].call(r, n, i);
  }
}
function ke(e, t, r, n, i, o) {
  if (t) e.call(r, n, i, o);else for (var a = e.length, s = Ae(e, a), h = 0; h < a; ++h) {
    s[h].call(r, n, i, o);
  }
}
function Ee(e, t, r, n) {
  if (t) e.apply(r, n);else for (var i = e.length, o = Ae(e, i), a = 0; a < i; ++a) {
    o[a].apply(r, n);
  }
}
function Se(e, t, r, n) {
  var i, o, a, s;
  if ("function" != typeof r) throw new TypeError('"listener" argument must be a function');
  if ((o = e._events) ? (o.newListener && (e.emit("newListener", t, r.listener ? r.listener : r), o = e._events), a = o[t]) : (o = e._events = new ge(), e._eventsCount = 0), a) {
    if ("function" == typeof a ? a = o[t] = n ? [r, a] : [a, r] : n ? a.unshift(r) : a.push(r), !a.warned && (i = we(e)) && i > 0 && a.length > i) {
      a.warned = !0;
      var h = new Error("Possible EventEmitter memory leak detected. " + a.length + " " + t + " listeners added. Use emitter.setMaxListeners() to increase limit");
      h.name = "MaxListenersExceededWarning", h.emitter = e, h.type = t, h.count = a.length, s = h, "function" == typeof console.warn ? console.warn(s) : console.log(s);
    }
  } else a = o[t] = r, ++e._eventsCount;
  return e;
}
function xe(e, t, r) {
  var n = !1;
  function i() {
    e.removeListener(t, i), n || (n = !0, r.apply(e, arguments));
  }
  return i.listener = r, i;
}
function Re(e) {
  var t = this._events;
  if (t) {
    var r = t[e];
    if ("function" == typeof r) return 1;
    if (r) return r.length;
  }
  return 0;
}
function Ae(e, t) {
  for (var r = new Array(t); t--;) {
    r[t] = e[t];
  }
  return r;
}
ge.prototype = Object.create(null), ve.EventEmitter = ve, ve.usingDomains = !1, ve.prototype.domain = void 0, ve.prototype._events = void 0, ve.prototype._maxListeners = void 0, ve.defaultMaxListeners = 10, ve.init = function () {
  this.domain = null, ve.usingDomains && (void 0).active && (void 0).Domain, this._events && this._events !== Object.getPrototypeOf(this)._events || (this._events = new ge(), this._eventsCount = 0), this._maxListeners = this._maxListeners || void 0;
}, ve.prototype.setMaxListeners = function (e) {
  if ("number" != typeof e || e < 0 || isNaN(e)) throw new TypeError('"n" argument must be a positive number');
  return this._maxListeners = e, this;
}, ve.prototype.getMaxListeners = function () {
  return we(this);
}, ve.prototype.emit = function (e) {
  var t,
    r,
    n,
    i,
    o,
    a,
    s,
    h = "error" === e;
  if (a = this._events) h = h && null == a.error;else if (!h) return !1;
  if (s = this.domain, h) {
    if (t = arguments[1], !s) {
      if (t instanceof Error) throw t;
      var l = new Error('Uncaught, unspecified "error" event. (' + t + ")");
      throw l.context = t, l;
    }
    return t || (t = new Error('Uncaught, unspecified "error" event')), t.domainEmitter = this, t.domain = s, t.domainThrown = !1, s.emit("error", t), !1;
  }
  if (!(r = a[e])) return !1;
  var f = "function" == typeof r;
  switch (n = arguments.length) {
    case 1:
      be(r, f, this);
      break;
    case 2:
      ye(r, f, this, arguments[1]);
      break;
    case 3:
      me(r, f, this, arguments[1], arguments[2]);
      break;
    case 4:
      ke(r, f, this, arguments[1], arguments[2], arguments[3]);
      break;
    default:
      for (i = new Array(n - 1), o = 1; o < n; o++) {
        i[o - 1] = arguments[o];
      }
      Ee(r, f, this, i);
  }
  return !0;
}, ve.prototype.addListener = function (e, t) {
  return Se(this, e, t, !1);
}, ve.prototype.on = ve.prototype.addListener, ve.prototype.prependListener = function (e, t) {
  return Se(this, e, t, !0);
}, ve.prototype.once = function (e, t) {
  if ("function" != typeof t) throw new TypeError('"listener" argument must be a function');
  return this.on(e, xe(this, e, t)), this;
}, ve.prototype.prependOnceListener = function (e, t) {
  if ("function" != typeof t) throw new TypeError('"listener" argument must be a function');
  return this.prependListener(e, xe(this, e, t)), this;
}, ve.prototype.removeListener = function (e, t) {
  var r, n, i, o, a;
  if ("function" != typeof t) throw new TypeError('"listener" argument must be a function');
  if (!(n = this._events)) return this;
  if (!(r = n[e])) return this;
  if (r === t || r.listener && r.listener === t) 0 == --this._eventsCount ? this._events = new ge() : (delete n[e], n.removeListener && this.emit("removeListener", e, r.listener || t));else if ("function" != typeof r) {
    for (i = -1, o = r.length; o-- > 0;) {
      if (r[o] === t || r[o].listener && r[o].listener === t) {
        a = r[o].listener, i = o;
        break;
      }
    }
    if (i < 0) return this;
    if (1 === r.length) {
      if (r[0] = void 0, 0 == --this._eventsCount) return this._events = new ge(), this;
      delete n[e];
    } else !function (e, t) {
      for (var r = t, n = r + 1, i = e.length; n < i; r += 1, n += 1) {
        e[r] = e[n];
      }
      e.pop();
    }(r, i);
    n.removeListener && this.emit("removeListener", e, a || t);
  }
  return this;
}, ve.prototype.removeAllListeners = function (e) {
  var t, r;
  if (!(r = this._events)) return this;
  if (!r.removeListener) return 0 === arguments.length ? (this._events = new ge(), this._eventsCount = 0) : r[e] && (0 == --this._eventsCount ? this._events = new ge() : delete r[e]), this;
  if (0 === arguments.length) {
    for (var n, i = Object.keys(r), o = 0; o < i.length; ++o) {
      "removeListener" !== (n = i[o]) && this.removeAllListeners(n);
    }
    return this.removeAllListeners("removeListener"), this._events = new ge(), this._eventsCount = 0, this;
  }
  if ("function" == typeof (t = r[e])) this.removeListener(e, t);else if (t) do {
    this.removeListener(e, t[t.length - 1]);
  } while (t[0]);
  return this;
}, ve.prototype.listeners = function (e) {
  var t,
    r = this._events;
  return r && (t = r[e]) ? "function" == typeof t ? [t.listener || t] : function (e) {
    for (var t = new Array(e.length), r = 0; r < t.length; ++r) {
      t[r] = e[r].listener || e[r];
    }
    return t;
  }(t) : [];
}, ve.listenerCount = function (e, t) {
  return "function" == typeof e.listenerCount ? e.listenerCount(t) : Re.call(e, t);
}, ve.prototype.listenerCount = Re, ve.prototype.eventNames = function () {
  return this._eventsCount > 0 ? Reflect.ownKeys(this._events) : [];
};
var Be = "function" == typeof Object.create ? function (e, t) {
    e.super_ = t, e.prototype = Object.create(t.prototype, {
      constructor: {
        value: e,
        enumerable: !1,
        writable: !0,
        configurable: !0
      }
    });
  } : function (e, t) {
    e.super_ = t;
    var r = function r() {};
    r.prototype = t.prototype, e.prototype = new r(), e.prototype.constructor = e;
  },
  ze = /%[sdj%]/g;
function Le(e) {
  if (!Ze(e)) {
    for (var t = [], r = 0; r < arguments.length; r++) {
      t.push(De(arguments[r]));
    }
    return t.join(" ");
  }
  r = 1;
  for (var n = arguments, i = n.length, o = String(e).replace(ze, function (e) {
      if ("%%" === e) return "%";
      if (r >= i) return e;
      switch (e) {
        case "%s":
          return String(n[r++]);
        case "%d":
          return Number(n[r++]);
        case "%j":
          try {
            return JSON.stringify(n[r++]);
          } catch (e) {
            return "[Circular]";
          }
        default:
          return e;
      }
    }), a = n[r]; r < i; a = n[++r]) {
    Ne(a) || !Ye(a) ? o += " " + a : o += " " + De(a);
  }
  return o;
}
function Te(t, r) {
  if (je(e.process)) return function () {
    return Te(t, r).apply(this, arguments);
  };
  var n = !1;
  return function () {
    return n || (console.error(r), n = !0), t.apply(this, arguments);
  };
}
var Me,
  Ce = {};
function De(e, t) {
  var r = {
    seen: [],
    stylize: Pe
  };
  return arguments.length >= 3 && (r.depth = arguments[2]), arguments.length >= 4 && (r.colors = arguments[3]), Fe(t) ? r.showHidden = t : t && function (e, t) {
    if (!t || !Ye(t)) return e;
    var r = Object.keys(t),
      n = r.length;
    for (; n--;) {
      e[r[n]] = t[r[n]];
    }
  }(r, t), je(r.showHidden) && (r.showHidden = !1), je(r.depth) && (r.depth = 2), je(r.colors) && (r.colors = !1), je(r.customInspect) && (r.customInspect = !0), r.colors && (r.stylize = Ie), Oe(r, e, r.depth);
}
function Ie(e, t) {
  var r = De.styles[t];
  return r ? "[" + De.colors[r][0] + "m" + e + "[" + De.colors[r][1] + "m" : e;
}
function Pe(e, t) {
  return e;
}
function Oe(e, t, r) {
  if (e.customInspect && t && qe(t.inspect) && t.inspect !== De && (!t.constructor || t.constructor.prototype !== t)) {
    var n = t.inspect(r, e);
    return Ze(n) || (n = Oe(e, n, r)), n;
  }
  var i = function (e, t) {
    if (je(t)) return e.stylize("undefined", "undefined");
    if (Ze(t)) {
      var r = "'" + JSON.stringify(t).replace(/^"|"$/g, "").replace(/'/g, "\\'").replace(/\\"/g, '"') + "'";
      return e.stylize(r, "string");
    }
    if (n = t, "number" == typeof n) return e.stylize("" + t, "number");
    var n;
    if (Fe(t)) return e.stylize("" + t, "boolean");
    if (Ne(t)) return e.stylize("null", "null");
  }(e, t);
  if (i) return i;
  var o = Object.keys(t),
    a = function (e) {
      var t = {};
      return e.forEach(function (e, r) {
        t[e] = !0;
      }), t;
    }(o);
  if (e.showHidden && (o = Object.getOwnPropertyNames(t)), Xe(t) && (o.indexOf("message") >= 0 || o.indexOf("description") >= 0)) return Ue(t);
  if (0 === o.length) {
    if (qe(t)) {
      var s = t.name ? ": " + t.name : "";
      return e.stylize("[Function" + s + "]", "special");
    }
    if (We(t)) return e.stylize(RegExp.prototype.toString.call(t), "regexp");
    if (Ke(t)) return e.stylize(Date.prototype.toString.call(t), "date");
    if (Xe(t)) return Ue(t);
  }
  var h,
    l,
    f = "",
    c = !1,
    u = ["{", "}"];
  (h = t, Array.isArray(h) && (c = !0, u = ["[", "]"]), qe(t)) && (f = " [Function" + (t.name ? ": " + t.name : "") + "]");
  return We(t) && (f = " " + RegExp.prototype.toString.call(t)), Ke(t) && (f = " " + Date.prototype.toUTCString.call(t)), Xe(t) && (f = " " + Ue(t)), 0 !== o.length || c && 0 != t.length ? r < 0 ? We(t) ? e.stylize(RegExp.prototype.toString.call(t), "regexp") : e.stylize("[Object]", "special") : (e.seen.push(t), l = c ? function (e, t, r, n, i) {
    for (var o = [], a = 0, s = t.length; a < s; ++a) {
      Ge(t, String(a)) ? o.push(He(e, t, r, n, String(a), !0)) : o.push("");
    }
    return i.forEach(function (i) {
      i.match(/^\d+$/) || o.push(He(e, t, r, n, i, !0));
    }), o;
  }(e, t, r, a, o) : o.map(function (n) {
    return He(e, t, r, a, n, c);
  }), e.seen.pop(), function (e, t, r) {
    if (e.reduce(function (e, t) {
      return t.indexOf("\n"), e + t.replace(/\u001b\[\d\d?m/g, "").length + 1;
    }, 0) > 60) return r[0] + ("" === t ? "" : t + "\n ") + " " + e.join(",\n  ") + " " + r[1];
    return r[0] + t + " " + e.join(", ") + " " + r[1];
  }(l, f, u)) : u[0] + f + u[1];
}
function Ue(e) {
  return "[" + Error.prototype.toString.call(e) + "]";
}
function He(e, t, r, n, i, o) {
  var a, s, h;
  if ((h = Object.getOwnPropertyDescriptor(t, i) || {
    value: t[i]
  }).get ? s = h.set ? e.stylize("[Getter/Setter]", "special") : e.stylize("[Getter]", "special") : h.set && (s = e.stylize("[Setter]", "special")), Ge(n, i) || (a = "[" + i + "]"), s || (e.seen.indexOf(h.value) < 0 ? (s = Ne(r) ? Oe(e, h.value, null) : Oe(e, h.value, r - 1)).indexOf("\n") > -1 && (s = o ? s.split("\n").map(function (e) {
    return "  " + e;
  }).join("\n").substr(2) : "\n" + s.split("\n").map(function (e) {
    return "   " + e;
  }).join("\n")) : s = e.stylize("[Circular]", "special")), je(a)) {
    if (o && i.match(/^\d+$/)) return s;
    (a = JSON.stringify("" + i)).match(/^"([a-zA-Z_][a-zA-Z_0-9]*)"$/) ? (a = a.substr(1, a.length - 2), a = e.stylize(a, "name")) : (a = a.replace(/'/g, "\\'").replace(/\\"/g, '"').replace(/(^"|"$)/g, "'"), a = e.stylize(a, "string"));
  }
  return a + ": " + s;
}
function Fe(e) {
  return "boolean" == typeof e;
}
function Ne(e) {
  return null === e;
}
function Ze(e) {
  return "string" == typeof e;
}
function je(e) {
  return void 0 === e;
}
function We(e) {
  return Ye(e) && "[object RegExp]" === Ve(e);
}
function Ye(e) {
  return "object" == (0, _typeof2.default)(e) && null !== e;
}
function Ke(e) {
  return Ye(e) && "[object Date]" === Ve(e);
}
function Xe(e) {
  return Ye(e) && ("[object Error]" === Ve(e) || e instanceof Error);
}
function qe(e) {
  return "function" == typeof e;
}
function Ve(e) {
  return Object.prototype.toString.call(e);
}
function Ge(e, t) {
  return Object.prototype.hasOwnProperty.call(e, t);
}
function $e() {
  this.head = null, this.tail = null, this.length = 0;
}
De.colors = {
  bold: [1, 22],
  italic: [3, 23],
  underline: [4, 24],
  inverse: [7, 27],
  white: [37, 39],
  grey: [90, 39],
  black: [30, 39],
  blue: [34, 39],
  cyan: [36, 39],
  green: [32, 39],
  magenta: [35, 39],
  red: [31, 39],
  yellow: [33, 39]
}, De.styles = {
  special: "cyan",
  number: "yellow",
  boolean: "yellow",
  undefined: "grey",
  null: "bold",
  string: "green",
  date: "magenta",
  regexp: "red"
}, $e.prototype.push = function (e) {
  var t = {
    data: e,
    next: null
  };
  this.length > 0 ? this.tail.next = t : this.head = t, this.tail = t, ++this.length;
}, $e.prototype.unshift = function (e) {
  var t = {
    data: e,
    next: this.head
  };
  0 === this.length && (this.tail = t), this.head = t, ++this.length;
}, $e.prototype.shift = function () {
  if (0 !== this.length) {
    var e = this.head.data;
    return 1 === this.length ? this.head = this.tail = null : this.head = this.head.next, --this.length, e;
  }
}, $e.prototype.clear = function () {
  this.head = this.tail = null, this.length = 0;
}, $e.prototype.join = function (e) {
  if (0 === this.length) return "";
  for (var t = this.head, r = "" + t.data; t = t.next;) {
    r += e + t.data;
  }
  return r;
}, $e.prototype.concat = function (e) {
  if (0 === this.length) return p.alloc(0);
  if (1 === this.length) return this.head.data;
  for (var t = p.allocUnsafe(e >>> 0), r = this.head, n = 0; r;) {
    r.data.copy(t, n), n += r.data.length, r = r.next;
  }
  return t;
};
var Je = p.isEncoding || function (e) {
  switch (e && e.toLowerCase()) {
    case "hex":
    case "utf8":
    case "utf-8":
    case "ascii":
    case "binary":
    case "base64":
    case "ucs2":
    case "ucs-2":
    case "utf16le":
    case "utf-16le":
    case "raw":
      return !0;
    default:
      return !1;
  }
};
function Qe(e) {
  switch (this.encoding = (e || "utf8").toLowerCase().replace(/[-_]/, ""), function (e) {
    if (e && !Je(e)) throw new Error("Unknown encoding: " + e);
  }(e), this.encoding) {
    case "utf8":
      this.surrogateSize = 3;
      break;
    case "ucs2":
    case "utf16le":
      this.surrogateSize = 2, this.detectIncompleteChar = tt;
      break;
    case "base64":
      this.surrogateSize = 3, this.detectIncompleteChar = rt;
      break;
    default:
      return void (this.write = et);
  }
  this.charBuffer = new p(6), this.charReceived = 0, this.charLength = 0;
}
function et(e) {
  return e.toString(this.encoding);
}
function tt(e) {
  this.charReceived = e.length % 2, this.charLength = this.charReceived ? 2 : 0;
}
function rt(e) {
  this.charReceived = e.length % 3, this.charLength = this.charReceived ? 3 : 0;
}
Qe.prototype.write = function (e) {
  for (var t = ""; this.charLength;) {
    var r = e.length >= this.charLength - this.charReceived ? this.charLength - this.charReceived : e.length;
    if (e.copy(this.charBuffer, this.charReceived, 0, r), this.charReceived += r, this.charReceived < this.charLength) return "";
    if (e = e.slice(r, e.length), !((i = (t = this.charBuffer.slice(0, this.charLength).toString(this.encoding)).charCodeAt(t.length - 1)) >= 55296 && i <= 56319)) {
      if (this.charReceived = this.charLength = 0, 0 === e.length) return t;
      break;
    }
    this.charLength += this.surrogateSize, t = "";
  }
  this.detectIncompleteChar(e);
  var n = e.length;
  this.charLength && (e.copy(this.charBuffer, 0, e.length - this.charReceived, n), n -= this.charReceived);
  var i;
  n = (t += e.toString(this.encoding, 0, n)).length - 1;
  if ((i = t.charCodeAt(n)) >= 55296 && i <= 56319) {
    var o = this.surrogateSize;
    return this.charLength += o, this.charReceived += o, this.charBuffer.copy(this.charBuffer, o, 0, o), e.copy(this.charBuffer, 0, 0, o), t.substring(0, n);
  }
  return t;
}, Qe.prototype.detectIncompleteChar = function (e) {
  for (var t = e.length >= 3 ? 3 : e.length; t > 0; t--) {
    var r = e[e.length - t];
    if (1 == t && r >> 5 == 6) {
      this.charLength = 2;
      break;
    }
    if (t <= 2 && r >> 4 == 14) {
      this.charLength = 3;
      break;
    }
    if (t <= 3 && r >> 3 == 30) {
      this.charLength = 4;
      break;
    }
  }
  this.charReceived = t;
}, Qe.prototype.end = function (e) {
  var t = "";
  if (e && e.length && (t = this.write(e)), this.charReceived) {
    var r = this.charReceived,
      n = this.charBuffer,
      i = this.encoding;
    t += n.slice(0, r).toString(i);
  }
  return t;
}, ot.ReadableState = it;
var nt = function (e) {
  je(Me) && (Me = ""), e = e.toUpperCase(), Ce[e] || (new RegExp("\\b" + e + "\\b", "i").test(Me) ? Ce[e] = function () {
    var t = Le.apply(null, arguments);
    console.error("%s %d: %s", e, 0, t);
  } : Ce[e] = function () {});
  return Ce[e];
}("stream");
function it(e, t) {
  e = e || {}, this.objectMode = !!e.objectMode, t instanceof Ct && (this.objectMode = this.objectMode || !!e.readableObjectMode);
  var r = e.highWaterMark,
    n = this.objectMode ? 16 : 16384;
  this.highWaterMark = r || 0 === r ? r : n, this.highWaterMark = ~~this.highWaterMark, this.buffer = new $e(), this.length = 0, this.pipes = null, this.pipesCount = 0, this.flowing = null, this.ended = !1, this.endEmitted = !1, this.reading = !1, this.sync = !0, this.needReadable = !1, this.emittedReadable = !1, this.readableListening = !1, this.resumeScheduled = !1, this.defaultEncoding = e.defaultEncoding || "utf8", this.ranOut = !1, this.awaitDrain = 0, this.readingMore = !1, this.decoder = null, this.encoding = null, e.encoding && (this.decoder = new Qe(e.encoding), this.encoding = e.encoding);
}
function ot(e) {
  if (!(this instanceof ot)) return new ot(e);
  this._readableState = new it(e, this), this.readable = !0, e && "function" == typeof e.read && (this._read = e.read), ve.call(this);
}
function at(e, t, r, n, i) {
  var o = function (e, t) {
    var r = null;
    $(t) || "string" == typeof t || null == t || e.objectMode || (r = new TypeError("Invalid non-string/buffer chunk"));
    return r;
  }(t, r);
  if (o) e.emit("error", o);else if (null === r) t.reading = !1, function (e, t) {
    if (t.ended) return;
    if (t.decoder) {
      var r = t.decoder.end();
      r && r.length && (t.buffer.push(r), t.length += t.objectMode ? 1 : r.length);
    }
    t.ended = !0, lt(e);
  }(e, t);else if (t.objectMode || r && r.length > 0) {
    if (t.ended && !i) {
      var a = new Error("stream.push() after EOF");
      e.emit("error", a);
    } else if (t.endEmitted && i) {
      var s = new Error("stream.unshift() after end event");
      e.emit("error", s);
    } else {
      var h;
      !t.decoder || i || n || (r = t.decoder.write(r), h = !t.objectMode && 0 === r.length), i || (t.reading = !1), h || (t.flowing && 0 === t.length && !t.sync ? (e.emit("data", r), e.read(0)) : (t.length += t.objectMode ? 1 : r.length, i ? t.buffer.unshift(r) : t.buffer.push(r), t.needReadable && lt(e))), function (e, t) {
        t.readingMore || (t.readingMore = !0, de(ct, e, t));
      }(e, t);
    }
  } else i || (t.reading = !1);
  return function (e) {
    return !e.ended && (e.needReadable || e.length < e.highWaterMark || 0 === e.length);
  }(t);
}
Be(ot, ve), ot.prototype.push = function (e, t) {
  var r = this._readableState;
  return r.objectMode || "string" != typeof e || (t = t || r.defaultEncoding) !== r.encoding && (e = p.from(e, t), t = ""), at(this, r, e, t, !1);
}, ot.prototype.unshift = function (e) {
  return at(this, this._readableState, e, "", !0);
}, ot.prototype.isPaused = function () {
  return !1 === this._readableState.flowing;
}, ot.prototype.setEncoding = function (e) {
  return this._readableState.decoder = new Qe(e), this._readableState.encoding = e, this;
};
var st = 8388608;
function ht(e, t) {
  return e <= 0 || 0 === t.length && t.ended ? 0 : t.objectMode ? 1 : e != e ? t.flowing && t.length ? t.buffer.head.data.length : t.length : (e > t.highWaterMark && (t.highWaterMark = function (e) {
    return e >= st ? e = st : (e--, e |= e >>> 1, e |= e >>> 2, e |= e >>> 4, e |= e >>> 8, e |= e >>> 16, e++), e;
  }(e)), e <= t.length ? e : t.ended ? t.length : (t.needReadable = !0, 0));
}
function lt(e) {
  var t = e._readableState;
  t.needReadable = !1, t.emittedReadable || (nt("emitReadable", t.flowing), t.emittedReadable = !0, t.sync ? de(ft, e) : ft(e));
}
function ft(e) {
  nt("emit readable"), e.emit("readable"), pt(e);
}
function ct(e, t) {
  for (var r = t.length; !t.reading && !t.flowing && !t.ended && t.length < t.highWaterMark && (nt("maybeReadMore read 0"), e.read(0), r !== t.length);) {
    r = t.length;
  }
  t.readingMore = !1;
}
function ut(e) {
  nt("readable nexttick read 0"), e.read(0);
}
function dt(e, t) {
  t.reading || (nt("resume read 0"), e.read(0)), t.resumeScheduled = !1, t.awaitDrain = 0, e.emit("resume"), pt(e), t.flowing && !t.reading && e.read(0);
}
function pt(e) {
  var t = e._readableState;
  for (nt("flow", t.flowing); t.flowing && null !== e.read();) {
    ;
  }
}
function _t(e, t) {
  return 0 === t.length ? null : (t.objectMode ? r = t.buffer.shift() : !e || e >= t.length ? (r = t.decoder ? t.buffer.join("") : 1 === t.buffer.length ? t.buffer.head.data : t.buffer.concat(t.length), t.buffer.clear()) : r = function (e, t, r) {
    var n;
    e < t.head.data.length ? (n = t.head.data.slice(0, e), t.head.data = t.head.data.slice(e)) : n = e === t.head.data.length ? t.shift() : r ? function (e, t) {
      var r = t.head,
        n = 1,
        i = r.data;
      e -= i.length;
      for (; r = r.next;) {
        var o = r.data,
          a = e > o.length ? o.length : e;
        if (a === o.length ? i += o : i += o.slice(0, e), 0 === (e -= a)) {
          a === o.length ? (++n, r.next ? t.head = r.next : t.head = t.tail = null) : (t.head = r, r.data = o.slice(a));
          break;
        }
        ++n;
      }
      return t.length -= n, i;
    }(e, t) : function (e, t) {
      var r = p.allocUnsafe(e),
        n = t.head,
        i = 1;
      n.data.copy(r), e -= n.data.length;
      for (; n = n.next;) {
        var o = n.data,
          a = e > o.length ? o.length : e;
        if (o.copy(r, r.length - e, 0, a), 0 === (e -= a)) {
          a === o.length ? (++i, n.next ? t.head = n.next : t.head = t.tail = null) : (t.head = n, n.data = o.slice(a));
          break;
        }
        ++i;
      }
      return t.length -= i, r;
    }(e, t);
    return n;
  }(e, t.buffer, t.decoder), r);
  var r;
}
function gt(e) {
  var t = e._readableState;
  if (t.length > 0) throw new Error('"endReadable()" called on non-empty stream');
  t.endEmitted || (t.ended = !0, de(vt, t, e));
}
function vt(e, t) {
  e.endEmitted || 0 !== e.length || (e.endEmitted = !0, t.readable = !1, t.emit("end"));
}
function wt(e, t) {
  for (var r = 0, n = e.length; r < n; r++) {
    if (e[r] === t) return r;
  }
  return -1;
}
function bt() {}
function yt(e, t, r) {
  this.chunk = e, this.encoding = t, this.callback = r, this.next = null;
}
function mt(e, t) {
  Object.defineProperty(this, "buffer", {
    get: Te(function () {
      return this.getBuffer();
    }, "_writableState.buffer is deprecated. Use _writableState.getBuffer instead.")
  }), e = e || {}, this.objectMode = !!e.objectMode, t instanceof Ct && (this.objectMode = this.objectMode || !!e.writableObjectMode);
  var r = e.highWaterMark,
    n = this.objectMode ? 16 : 16384;
  this.highWaterMark = r || 0 === r ? r : n, this.highWaterMark = ~~this.highWaterMark, this.needDrain = !1, this.ending = !1, this.ended = !1, this.finished = !1;
  var i = !1 === e.decodeStrings;
  this.decodeStrings = !i, this.defaultEncoding = e.defaultEncoding || "utf8", this.length = 0, this.writing = !1, this.corked = 0, this.sync = !0, this.bufferProcessing = !1, this.onwrite = function (e) {
    !function (e, t) {
      var r = e._writableState,
        n = r.sync,
        i = r.writecb;
      if (function (e) {
        e.writing = !1, e.writecb = null, e.length -= e.writelen, e.writelen = 0;
      }(r), t) !function (e, t, r, n, i) {
        --t.pendingcb, r ? de(i, n) : i(n);
        e._writableState.errorEmitted = !0, e.emit("error", n);
      }(e, r, n, t, i);else {
        var o = Rt(r);
        o || r.corked || r.bufferProcessing || !r.bufferedRequest || xt(e, r), n ? de(St, e, r, o, i) : St(e, r, o, i);
      }
    }(t, e);
  }, this.writecb = null, this.writelen = 0, this.bufferedRequest = null, this.lastBufferedRequest = null, this.pendingcb = 0, this.prefinished = !1, this.errorEmitted = !1, this.bufferedRequestCount = 0, this.corkedRequestsFree = new zt(this);
}
function kt(e) {
  if (!(this instanceof kt || this instanceof Ct)) return new kt(e);
  this._writableState = new mt(e, this), this.writable = !0, e && ("function" == typeof e.write && (this._write = e.write), "function" == typeof e.writev && (this._writev = e.writev)), ve.call(this);
}
function Et(e, t, r, n, i, o, a) {
  t.writelen = n, t.writecb = a, t.writing = !0, t.sync = !0, r ? e._writev(i, t.onwrite) : e._write(i, o, t.onwrite), t.sync = !1;
}
function St(e, t, r, n) {
  r || function (e, t) {
    0 === t.length && t.needDrain && (t.needDrain = !1, e.emit("drain"));
  }(e, t), t.pendingcb--, n(), Bt(e, t);
}
function xt(e, t) {
  t.bufferProcessing = !0;
  var r = t.bufferedRequest;
  if (e._writev && r && r.next) {
    var n = t.bufferedRequestCount,
      i = new Array(n),
      o = t.corkedRequestsFree;
    o.entry = r;
    for (var a = 0; r;) {
      i[a] = r, r = r.next, a += 1;
    }
    Et(e, t, !0, t.length, i, "", o.finish), t.pendingcb++, t.lastBufferedRequest = null, o.next ? (t.corkedRequestsFree = o.next, o.next = null) : t.corkedRequestsFree = new zt(t);
  } else {
    for (; r;) {
      var s = r.chunk,
        h = r.encoding,
        l = r.callback;
      if (Et(e, t, !1, t.objectMode ? 1 : s.length, s, h, l), r = r.next, t.writing) break;
    }
    null === r && (t.lastBufferedRequest = null);
  }
  t.bufferedRequestCount = 0, t.bufferedRequest = r, t.bufferProcessing = !1;
}
function Rt(e) {
  return e.ending && 0 === e.length && null === e.bufferedRequest && !e.finished && !e.writing;
}
function At(e, t) {
  t.prefinished || (t.prefinished = !0, e.emit("prefinish"));
}
function Bt(e, t) {
  var r = Rt(t);
  return r && (0 === t.pendingcb ? (At(e, t), t.finished = !0, e.emit("finish")) : At(e, t)), r;
}
function zt(e) {
  var t = this;
  this.next = null, this.entry = null, this.finish = function (r) {
    var n = t.entry;
    for (t.entry = null; n;) {
      var i = n.callback;
      e.pendingcb--, i(r), n = n.next;
    }
    e.corkedRequestsFree ? e.corkedRequestsFree.next = t : e.corkedRequestsFree = t;
  };
}
ot.prototype.read = function (e) {
  nt("read", e), e = parseInt(e, 10);
  var t = this._readableState,
    r = e;
  if (0 !== e && (t.emittedReadable = !1), 0 === e && t.needReadable && (t.length >= t.highWaterMark || t.ended)) return nt("read: emitReadable", t.length, t.ended), 0 === t.length && t.ended ? gt(this) : lt(this), null;
  if (0 === (e = ht(e, t)) && t.ended) return 0 === t.length && gt(this), null;
  var n,
    i = t.needReadable;
  return nt("need readable", i), (0 === t.length || t.length - e < t.highWaterMark) && nt("length less than watermark", i = !0), t.ended || t.reading ? nt("reading or ended", i = !1) : i && (nt("do read"), t.reading = !0, t.sync = !0, 0 === t.length && (t.needReadable = !0), this._read(t.highWaterMark), t.sync = !1, t.reading || (e = ht(r, t))), null === (n = e > 0 ? _t(e, t) : null) ? (t.needReadable = !0, e = 0) : t.length -= e, 0 === t.length && (t.ended || (t.needReadable = !0), r !== e && t.ended && gt(this)), null !== n && this.emit("data", n), n;
}, ot.prototype._read = function (e) {
  this.emit("error", new Error("not implemented"));
}, ot.prototype.pipe = function (e, t) {
  var r = this,
    n = this._readableState;
  switch (n.pipesCount) {
    case 0:
      n.pipes = e;
      break;
    case 1:
      n.pipes = [n.pipes, e];
      break;
    default:
      n.pipes.push(e);
  }
  n.pipesCount += 1, nt("pipe count=%d opts=%j", n.pipesCount, t);
  var i = !t || !1 !== t.end ? a : l;
  function o(e) {
    nt("onunpipe"), e === r && l();
  }
  function a() {
    nt("onend"), e.end();
  }
  n.endEmitted ? de(i) : r.once("end", i), e.on("unpipe", o);
  var s = function (e) {
    return function () {
      var t = e._readableState;
      nt("pipeOnDrain", t.awaitDrain), t.awaitDrain && t.awaitDrain--, 0 === t.awaitDrain && e.listeners("data").length && (t.flowing = !0, pt(e));
    };
  }(r);
  e.on("drain", s);
  var h = !1;
  function l() {
    nt("cleanup"), e.removeListener("close", d), e.removeListener("finish", p), e.removeListener("drain", s), e.removeListener("error", u), e.removeListener("unpipe", o), r.removeListener("end", a), r.removeListener("end", l), r.removeListener("data", c), h = !0, !n.awaitDrain || e._writableState && !e._writableState.needDrain || s();
  }
  var f = !1;
  function c(t) {
    nt("ondata"), f = !1, !1 !== e.write(t) || f || ((1 === n.pipesCount && n.pipes === e || n.pipesCount > 1 && -1 !== wt(n.pipes, e)) && !h && (nt("false write response, pause", r._readableState.awaitDrain), r._readableState.awaitDrain++, f = !0), r.pause());
  }
  function u(t) {
    var r;
    nt("onerror", t), _(), e.removeListener("error", u), 0 === (r = "error", e.listeners(r).length) && e.emit("error", t);
  }
  function d() {
    e.removeListener("finish", p), _();
  }
  function p() {
    nt("onfinish"), e.removeListener("close", d), _();
  }
  function _() {
    nt("unpipe"), r.unpipe(e);
  }
  return r.on("data", c), function (e, t, r) {
    if ("function" == typeof e.prependListener) return e.prependListener(t, r);
    e._events && e._events[t] ? Array.isArray(e._events[t]) ? e._events[t].unshift(r) : e._events[t] = [r, e._events[t]] : e.on(t, r);
  }(e, "error", u), e.once("close", d), e.once("finish", p), e.emit("pipe", r), n.flowing || (nt("pipe resume"), r.resume()), e;
}, ot.prototype.unpipe = function (e) {
  var t = this._readableState;
  if (0 === t.pipesCount) return this;
  if (1 === t.pipesCount) return e && e !== t.pipes ? this : (e || (e = t.pipes), t.pipes = null, t.pipesCount = 0, t.flowing = !1, e && e.emit("unpipe", this), this);
  if (!e) {
    var r = t.pipes,
      n = t.pipesCount;
    t.pipes = null, t.pipesCount = 0, t.flowing = !1;
    for (var i = 0; i < n; i++) {
      r[i].emit("unpipe", this);
    }
    return this;
  }
  var o = wt(t.pipes, e);
  return -1 === o ? this : (t.pipes.splice(o, 1), t.pipesCount -= 1, 1 === t.pipesCount && (t.pipes = t.pipes[0]), e.emit("unpipe", this), this);
}, ot.prototype.on = function (e, t) {
  var r = ve.prototype.on.call(this, e, t);
  if ("data" === e) !1 !== this._readableState.flowing && this.resume();else if ("readable" === e) {
    var n = this._readableState;
    n.endEmitted || n.readableListening || (n.readableListening = n.needReadable = !0, n.emittedReadable = !1, n.reading ? n.length && lt(this) : de(ut, this));
  }
  return r;
}, ot.prototype.addListener = ot.prototype.on, ot.prototype.resume = function () {
  var e = this._readableState;
  return e.flowing || (nt("resume"), e.flowing = !0, function (e, t) {
    t.resumeScheduled || (t.resumeScheduled = !0, de(dt, e, t));
  }(this, e)), this;
}, ot.prototype.pause = function () {
  return nt("call pause flowing=%j", this._readableState.flowing), !1 !== this._readableState.flowing && (nt("pause"), this._readableState.flowing = !1, this.emit("pause")), this;
}, ot.prototype.wrap = function (e) {
  var t = this._readableState,
    r = !1,
    n = this;
  for (var i in e.on("end", function () {
    if (nt("wrapped end"), t.decoder && !t.ended) {
      var e = t.decoder.end();
      e && e.length && n.push(e);
    }
    n.push(null);
  }), e.on("data", function (i) {
    (nt("wrapped data"), t.decoder && (i = t.decoder.write(i)), t.objectMode && null == i) || (t.objectMode || i && i.length) && (n.push(i) || (r = !0, e.pause()));
  }), e) {
    void 0 === this[i] && "function" == typeof e[i] && (this[i] = function (t) {
      return function () {
        return e[t].apply(e, arguments);
      };
    }(i));
  }
  return function (e, t) {
    for (var r = 0, n = e.length; r < n; r++) {
      t(e[r], r);
    }
  }(["error", "close", "destroy", "pause", "resume"], function (t) {
    e.on(t, n.emit.bind(n, t));
  }), n._read = function (t) {
    nt("wrapped _read", t), r && (r = !1, e.resume());
  }, n;
}, ot._fromList = _t, kt.WritableState = mt, Be(kt, ve), mt.prototype.getBuffer = function () {
  for (var e = this.bufferedRequest, t = []; e;) {
    t.push(e), e = e.next;
  }
  return t;
}, kt.prototype.pipe = function () {
  this.emit("error", new Error("Cannot pipe, not readable"));
}, kt.prototype.write = function (e, t, r) {
  var n = this._writableState,
    i = !1;
  return "function" == typeof t && (r = t, t = null), p.isBuffer(e) ? t = "buffer" : t || (t = n.defaultEncoding), "function" != typeof r && (r = bt), n.ended ? function (e, t) {
    var r = new Error("write after end");
    e.emit("error", r), de(t, r);
  }(this, r) : function (e, t, r, n) {
    var i = !0,
      o = !1;
    return null === r ? o = new TypeError("May not write null values to stream") : p.isBuffer(r) || "string" == typeof r || void 0 === r || t.objectMode || (o = new TypeError("Invalid non-string/buffer chunk")), o && (e.emit("error", o), de(n, o), i = !1), i;
  }(this, n, e, r) && (n.pendingcb++, i = function (e, t, r, n, i) {
    r = function (e, t, r) {
      return e.objectMode || !1 === e.decodeStrings || "string" != typeof t || (t = p.from(t, r)), t;
    }(t, r, n), p.isBuffer(r) && (n = "buffer");
    var o = t.objectMode ? 1 : r.length;
    t.length += o;
    var a = t.length < t.highWaterMark;
    a || (t.needDrain = !0);
    if (t.writing || t.corked) {
      var s = t.lastBufferedRequest;
      t.lastBufferedRequest = new yt(r, n, i), s ? s.next = t.lastBufferedRequest : t.bufferedRequest = t.lastBufferedRequest, t.bufferedRequestCount += 1;
    } else Et(e, t, !1, o, r, n, i);
    return a;
  }(this, n, e, t, r)), i;
}, kt.prototype.cork = function () {
  this._writableState.corked++;
}, kt.prototype.uncork = function () {
  var e = this._writableState;
  e.corked && (e.corked--, e.writing || e.corked || e.finished || e.bufferProcessing || !e.bufferedRequest || xt(this, e));
}, kt.prototype.setDefaultEncoding = function (e) {
  if ("string" == typeof e && (e = e.toLowerCase()), !(["hex", "utf8", "utf-8", "ascii", "binary", "base64", "ucs2", "ucs-2", "utf16le", "utf-16le", "raw"].indexOf((e + "").toLowerCase()) > -1)) throw new TypeError("Unknown encoding: " + e);
  return this._writableState.defaultEncoding = e, this;
}, kt.prototype._write = function (e, t, r) {
  r(new Error("not implemented"));
}, kt.prototype._writev = null, kt.prototype.end = function (e, t, r) {
  var n = this._writableState;
  "function" == typeof e ? (r = e, e = null, t = null) : "function" == typeof t && (r = t, t = null), null != e && this.write(e, t), n.corked && (n.corked = 1, this.uncork()), n.ending || n.finished || function (e, t, r) {
    t.ending = !0, Bt(e, t), r && (t.finished ? de(r) : e.once("finish", r));
    t.ended = !0, e.writable = !1;
  }(this, n, r);
}, Be(Ct, ot);
for (var Lt = Object.keys(kt.prototype), Tt = 0; Tt < Lt.length; Tt++) {
  var Mt = Lt[Tt];
  Ct.prototype[Mt] || (Ct.prototype[Mt] = kt.prototype[Mt]);
}
function Ct(e) {
  if (!(this instanceof Ct)) return new Ct(e);
  ot.call(this, e), kt.call(this, e), e && !1 === e.readable && (this.readable = !1), e && !1 === e.writable && (this.writable = !1), this.allowHalfOpen = !0, e && !1 === e.allowHalfOpen && (this.allowHalfOpen = !1), this.once("end", Dt);
}
function Dt() {
  this.allowHalfOpen || this._writableState.ended || de(It, this);
}
function It(e) {
  e.end();
}
function Pt(e) {
  this.afterTransform = function (t, r) {
    return function (e, t, r) {
      var n = e._transformState;
      n.transforming = !1;
      var i = n.writecb;
      if (!i) return e.emit("error", new Error("no writecb in Transform class"));
      n.writechunk = null, n.writecb = null, null != r && e.push(r);
      i(t);
      var o = e._readableState;
      o.reading = !1, (o.needReadable || o.length < o.highWaterMark) && e._read(o.highWaterMark);
    }(e, t, r);
  }, this.needTransform = !1, this.transforming = !1, this.writecb = null, this.writechunk = null, this.writeencoding = null;
}
function Ot(e) {
  if (!(this instanceof Ot)) return new Ot(e);
  Ct.call(this, e), this._transformState = new Pt(this);
  var t = this;
  this._readableState.needReadable = !0, this._readableState.sync = !1, e && ("function" == typeof e.transform && (this._transform = e.transform), "function" == typeof e.flush && (this._flush = e.flush)), this.once("prefinish", function () {
    "function" == typeof this._flush ? this._flush(function (e) {
      Ut(t, e);
    }) : Ut(t);
  });
}
function Ut(e, t) {
  if (t) return e.emit("error", t);
  var r = e._writableState,
    n = e._transformState;
  if (r.length) throw new Error("Calling transform done when ws.length != 0");
  if (n.transforming) throw new Error("Calling transform done when still transforming");
  return e.push(null);
}
function Ht(e) {
  if (!(this instanceof Ht)) return new Ht(e);
  Ot.call(this, e);
}
function Ft() {
  ve.call(this);
}
Be(Ot, Ct), Ot.prototype.push = function (e, t) {
  return this._transformState.needTransform = !1, Ct.prototype.push.call(this, e, t);
}, Ot.prototype._transform = function (e, t, r) {
  throw new Error("Not implemented");
}, Ot.prototype._write = function (e, t, r) {
  var n = this._transformState;
  if (n.writecb = r, n.writechunk = e, n.writeencoding = t, !n.transforming) {
    var i = this._readableState;
    (n.needTransform || i.needReadable || i.length < i.highWaterMark) && this._read(i.highWaterMark);
  }
}, Ot.prototype._read = function (e) {
  var t = this._transformState;
  null !== t.writechunk && t.writecb && !t.transforming ? (t.transforming = !0, this._transform(t.writechunk, t.writeencoding, t.afterTransform)) : t.needTransform = !0;
}, Be(Ht, Ot), Ht.prototype._transform = function (e, t, r) {
  r(null, e);
}, Be(Ft, ve), Ft.Readable = ot, Ft.Writable = kt, Ft.Duplex = Ct, Ft.Transform = Ot, Ft.PassThrough = Ht, Ft.Stream = Ft, Ft.prototype.pipe = function (e, t) {
  var r = this;
  function n(t) {
    e.writable && !1 === e.write(t) && r.pause && r.pause();
  }
  function i() {
    r.readable && r.resume && r.resume();
  }
  r.on("data", n), e.on("drain", i), e._isStdio || t && !1 === t.end || (r.on("end", a), r.on("close", s));
  var o = !1;
  function a() {
    o || (o = !0, e.end());
  }
  function s() {
    o || (o = !0, "function" == typeof e.destroy && e.destroy());
  }
  function h(e) {
    if (l(), 0 === ve.listenerCount(this, "error")) throw e;
  }
  function l() {
    r.removeListener("data", n), e.removeListener("drain", i), r.removeListener("end", a), r.removeListener("close", s), r.removeListener("error", h), e.removeListener("error", h), r.removeListener("end", l), r.removeListener("close", l), e.removeListener("close", l);
  }
  return r.on("error", h), e.on("error", h), r.on("end", l), r.on("close", l), e.on("close", l), e.emit("pipe", r), e;
};
var Nt = {
  2: "need dictionary",
  1: "stream end",
  0: "",
  "-1": "file error",
  "-2": "stream error",
  "-3": "data error",
  "-4": "insufficient memory",
  "-5": "buffer error",
  "-6": "incompatible version"
};
function Zt() {
  this.input = null, this.next_in = 0, this.avail_in = 0, this.total_in = 0, this.output = null, this.next_out = 0, this.avail_out = 0, this.total_out = 0, this.msg = "", this.state = null, this.data_type = 2, this.adler = 0;
}
function jt(e, t, r, n, i) {
  if (t.subarray && e.subarray) e.set(t.subarray(r, r + n), i);else for (var o = 0; o < n; o++) {
    e[i + o] = t[r + o];
  }
}
var Wt = Uint8Array,
  Yt = Uint16Array,
  Kt = Int32Array,
  Xt = 4,
  qt = 0,
  Vt = 1,
  Gt = 2;
function $t(e) {
  for (var t = e.length; --t >= 0;) {
    e[t] = 0;
  }
}
var Jt = 0,
  Qt = 1,
  er = 2,
  tr = 29,
  rr = 256,
  nr = rr + 1 + tr,
  ir = 30,
  or = 19,
  ar = 2 * nr + 1,
  sr = 15,
  hr = 16,
  lr = 7,
  fr = 256,
  cr = 16,
  ur = 17,
  dr = 18,
  pr = [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 0],
  _r = [0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13],
  gr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 3, 7],
  vr = [16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15],
  wr = new Array(2 * (nr + 2));
$t(wr);
var br = new Array(2 * ir);
$t(br);
var yr = new Array(512);
$t(yr);
var mr = new Array(256);
$t(mr);
var kr = new Array(tr);
$t(kr);
var Er,
  Sr,
  xr,
  Rr = new Array(ir);
function Ar(e, t, r, n, i) {
  this.static_tree = e, this.extra_bits = t, this.extra_base = r, this.elems = n, this.max_length = i, this.has_stree = e && e.length;
}
function Br(e, t) {
  this.dyn_tree = e, this.max_code = 0, this.stat_desc = t;
}
function zr(e) {
  return e < 256 ? yr[e] : yr[256 + (e >>> 7)];
}
function Lr(e, t) {
  e.pending_buf[e.pending++] = 255 & t, e.pending_buf[e.pending++] = t >>> 8 & 255;
}
function Tr(e, t, r) {
  e.bi_valid > hr - r ? (e.bi_buf |= t << e.bi_valid & 65535, Lr(e, e.bi_buf), e.bi_buf = t >> hr - e.bi_valid, e.bi_valid += r - hr) : (e.bi_buf |= t << e.bi_valid & 65535, e.bi_valid += r);
}
function Mr(e, t, r) {
  Tr(e, r[2 * t], r[2 * t + 1]);
}
function Cr(e, t) {
  var r = 0;
  do {
    r |= 1 & e, e >>>= 1, r <<= 1;
  } while (--t > 0);
  return r >>> 1;
}
function Dr(e, t, r) {
  var n,
    i,
    o = new Array(sr + 1),
    a = 0;
  for (n = 1; n <= sr; n++) {
    o[n] = a = a + r[n - 1] << 1;
  }
  for (i = 0; i <= t; i++) {
    var s = e[2 * i + 1];
    0 !== s && (e[2 * i] = Cr(o[s]++, s));
  }
}
function Ir(e) {
  var t;
  for (t = 0; t < nr; t++) {
    e.dyn_ltree[2 * t] = 0;
  }
  for (t = 0; t < ir; t++) {
    e.dyn_dtree[2 * t] = 0;
  }
  for (t = 0; t < or; t++) {
    e.bl_tree[2 * t] = 0;
  }
  e.dyn_ltree[2 * fr] = 1, e.opt_len = e.static_len = 0, e.last_lit = e.matches = 0;
}
function Pr(e) {
  e.bi_valid > 8 ? Lr(e, e.bi_buf) : e.bi_valid > 0 && (e.pending_buf[e.pending++] = e.bi_buf), e.bi_buf = 0, e.bi_valid = 0;
}
function Or(e, t, r, n) {
  var i = 2 * t,
    o = 2 * r;
  return e[i] < e[o] || e[i] === e[o] && n[t] <= n[r];
}
function Ur(e, t, r) {
  for (var n = e.heap[r], i = r << 1; i <= e.heap_len && (i < e.heap_len && Or(t, e.heap[i + 1], e.heap[i], e.depth) && i++, !Or(t, n, e.heap[i], e.depth));) {
    e.heap[r] = e.heap[i], r = i, i <<= 1;
  }
  e.heap[r] = n;
}
function Hr(e, t, r) {
  var n,
    i,
    o,
    a,
    s = 0;
  if (0 !== e.last_lit) do {
    n = e.pending_buf[e.d_buf + 2 * s] << 8 | e.pending_buf[e.d_buf + 2 * s + 1], i = e.pending_buf[e.l_buf + s], s++, 0 === n ? Mr(e, i, t) : (Mr(e, (o = mr[i]) + rr + 1, t), 0 !== (a = pr[o]) && Tr(e, i -= kr[o], a), Mr(e, o = zr(--n), r), 0 !== (a = _r[o]) && Tr(e, n -= Rr[o], a));
  } while (s < e.last_lit);
  Mr(e, fr, t);
}
function Fr(e, t) {
  var r,
    n,
    i,
    o = t.dyn_tree,
    a = t.stat_desc.static_tree,
    s = t.stat_desc.has_stree,
    h = t.stat_desc.elems,
    l = -1;
  for (e.heap_len = 0, e.heap_max = ar, r = 0; r < h; r++) {
    0 !== o[2 * r] ? (e.heap[++e.heap_len] = l = r, e.depth[r] = 0) : o[2 * r + 1] = 0;
  }
  for (; e.heap_len < 2;) {
    o[2 * (i = e.heap[++e.heap_len] = l < 2 ? ++l : 0)] = 1, e.depth[i] = 0, e.opt_len--, s && (e.static_len -= a[2 * i + 1]);
  }
  for (t.max_code = l, r = e.heap_len >> 1; r >= 1; r--) {
    Ur(e, o, r);
  }
  i = h;
  do {
    r = e.heap[1], e.heap[1] = e.heap[e.heap_len--], Ur(e, o, 1), n = e.heap[1], e.heap[--e.heap_max] = r, e.heap[--e.heap_max] = n, o[2 * i] = o[2 * r] + o[2 * n], e.depth[i] = (e.depth[r] >= e.depth[n] ? e.depth[r] : e.depth[n]) + 1, o[2 * r + 1] = o[2 * n + 1] = i, e.heap[1] = i++, Ur(e, o, 1);
  } while (e.heap_len >= 2);
  e.heap[--e.heap_max] = e.heap[1], function (e, t) {
    var r,
      n,
      i,
      o,
      a,
      s,
      h = t.dyn_tree,
      l = t.max_code,
      f = t.stat_desc.static_tree,
      c = t.stat_desc.has_stree,
      u = t.stat_desc.extra_bits,
      d = t.stat_desc.extra_base,
      p = t.stat_desc.max_length,
      _ = 0;
    for (o = 0; o <= sr; o++) {
      e.bl_count[o] = 0;
    }
    for (h[2 * e.heap[e.heap_max] + 1] = 0, r = e.heap_max + 1; r < ar; r++) {
      (o = h[2 * h[2 * (n = e.heap[r]) + 1] + 1] + 1) > p && (o = p, _++), h[2 * n + 1] = o, n > l || (e.bl_count[o]++, a = 0, n >= d && (a = u[n - d]), s = h[2 * n], e.opt_len += s * (o + a), c && (e.static_len += s * (f[2 * n + 1] + a)));
    }
    if (0 !== _) {
      do {
        for (o = p - 1; 0 === e.bl_count[o];) {
          o--;
        }
        e.bl_count[o]--, e.bl_count[o + 1] += 2, e.bl_count[p]--, _ -= 2;
      } while (_ > 0);
      for (o = p; 0 !== o; o--) {
        for (n = e.bl_count[o]; 0 !== n;) {
          (i = e.heap[--r]) > l || (h[2 * i + 1] !== o && (e.opt_len += (o - h[2 * i + 1]) * h[2 * i], h[2 * i + 1] = o), n--);
        }
      }
    }
  }(e, t), Dr(o, l, e.bl_count);
}
function Nr(e, t, r) {
  var n,
    i,
    o = -1,
    a = t[1],
    s = 0,
    h = 7,
    l = 4;
  for (0 === a && (h = 138, l = 3), t[2 * (r + 1) + 1] = 65535, n = 0; n <= r; n++) {
    i = a, a = t[2 * (n + 1) + 1], ++s < h && i === a || (s < l ? e.bl_tree[2 * i] += s : 0 !== i ? (i !== o && e.bl_tree[2 * i]++, e.bl_tree[2 * cr]++) : s <= 10 ? e.bl_tree[2 * ur]++ : e.bl_tree[2 * dr]++, s = 0, o = i, 0 === a ? (h = 138, l = 3) : i === a ? (h = 6, l = 3) : (h = 7, l = 4));
  }
}
function Zr(e, t, r) {
  var n,
    i,
    o = -1,
    a = t[1],
    s = 0,
    h = 7,
    l = 4;
  for (0 === a && (h = 138, l = 3), n = 0; n <= r; n++) {
    if (i = a, a = t[2 * (n + 1) + 1], !(++s < h && i === a)) {
      if (s < l) do {
        Mr(e, i, e.bl_tree);
      } while (0 != --s);else 0 !== i ? (i !== o && (Mr(e, i, e.bl_tree), s--), Mr(e, cr, e.bl_tree), Tr(e, s - 3, 2)) : s <= 10 ? (Mr(e, ur, e.bl_tree), Tr(e, s - 3, 3)) : (Mr(e, dr, e.bl_tree), Tr(e, s - 11, 7));
      s = 0, o = i, 0 === a ? (h = 138, l = 3) : i === a ? (h = 6, l = 3) : (h = 7, l = 4);
    }
  }
}
$t(Rr);
var jr = !1;
function Wr(e) {
  jr || (!function () {
    var e,
      t,
      r,
      n,
      i,
      o = new Array(sr + 1);
    for (r = 0, n = 0; n < tr - 1; n++) {
      for (kr[n] = r, e = 0; e < 1 << pr[n]; e++) {
        mr[r++] = n;
      }
    }
    for (mr[r - 1] = n, i = 0, n = 0; n < 16; n++) {
      for (Rr[n] = i, e = 0; e < 1 << _r[n]; e++) {
        yr[i++] = n;
      }
    }
    for (i >>= 7; n < ir; n++) {
      for (Rr[n] = i << 7, e = 0; e < 1 << _r[n] - 7; e++) {
        yr[256 + i++] = n;
      }
    }
    for (t = 0; t <= sr; t++) {
      o[t] = 0;
    }
    for (e = 0; e <= 143;) {
      wr[2 * e + 1] = 8, e++, o[8]++;
    }
    for (; e <= 255;) {
      wr[2 * e + 1] = 9, e++, o[9]++;
    }
    for (; e <= 279;) {
      wr[2 * e + 1] = 7, e++, o[7]++;
    }
    for (; e <= 287;) {
      wr[2 * e + 1] = 8, e++, o[8]++;
    }
    for (Dr(wr, nr + 1, o), e = 0; e < ir; e++) {
      br[2 * e + 1] = 5, br[2 * e] = Cr(e, 5);
    }
    Er = new Ar(wr, pr, rr + 1, nr, sr), Sr = new Ar(br, _r, 0, ir, sr), xr = new Ar(new Array(0), gr, 0, or, lr);
  }(), jr = !0), e.l_desc = new Br(e.dyn_ltree, Er), e.d_desc = new Br(e.dyn_dtree, Sr), e.bl_desc = new Br(e.bl_tree, xr), e.bi_buf = 0, e.bi_valid = 0, Ir(e);
}
function Yr(e, t, r, n) {
  Tr(e, (Jt << 1) + (n ? 1 : 0), 3), function (e, t, r, n) {
    Pr(e), n && (Lr(e, r), Lr(e, ~r)), jt(e.pending_buf, e.window, t, r, e.pending), e.pending += r;
  }(e, t, r, !0);
}
function Kr(e) {
  Tr(e, Qt << 1, 3), Mr(e, fr, wr), function (e) {
    16 === e.bi_valid ? (Lr(e, e.bi_buf), e.bi_buf = 0, e.bi_valid = 0) : e.bi_valid >= 8 && (e.pending_buf[e.pending++] = 255 & e.bi_buf, e.bi_buf >>= 8, e.bi_valid -= 8);
  }(e);
}
function Xr(e, t, r, n) {
  var i,
    o,
    a = 0;
  e.level > 0 ? (e.strm.data_type === Gt && (e.strm.data_type = function (e) {
    var t,
      r = 4093624447;
    for (t = 0; t <= 31; t++, r >>>= 1) {
      if (1 & r && 0 !== e.dyn_ltree[2 * t]) return qt;
    }
    if (0 !== e.dyn_ltree[18] || 0 !== e.dyn_ltree[20] || 0 !== e.dyn_ltree[26]) return Vt;
    for (t = 32; t < rr; t++) {
      if (0 !== e.dyn_ltree[2 * t]) return Vt;
    }
    return qt;
  }(e)), Fr(e, e.l_desc), Fr(e, e.d_desc), a = function (e) {
    var t;
    for (Nr(e, e.dyn_ltree, e.l_desc.max_code), Nr(e, e.dyn_dtree, e.d_desc.max_code), Fr(e, e.bl_desc), t = or - 1; t >= 3 && 0 === e.bl_tree[2 * vr[t] + 1]; t--) {
      ;
    }
    return e.opt_len += 3 * (t + 1) + 5 + 5 + 4, t;
  }(e), i = e.opt_len + 3 + 7 >>> 3, (o = e.static_len + 3 + 7 >>> 3) <= i && (i = o)) : i = o = r + 5, r + 4 <= i && -1 !== t ? Yr(e, t, r, n) : e.strategy === Xt || o === i ? (Tr(e, (Qt << 1) + (n ? 1 : 0), 3), Hr(e, wr, br)) : (Tr(e, (er << 1) + (n ? 1 : 0), 3), function (e, t, r, n) {
    var i;
    for (Tr(e, t - 257, 5), Tr(e, r - 1, 5), Tr(e, n - 4, 4), i = 0; i < n; i++) {
      Tr(e, e.bl_tree[2 * vr[i] + 1], 3);
    }
    Zr(e, e.dyn_ltree, t - 1), Zr(e, e.dyn_dtree, r - 1);
  }(e, e.l_desc.max_code + 1, e.d_desc.max_code + 1, a + 1), Hr(e, e.dyn_ltree, e.dyn_dtree)), Ir(e), n && Pr(e);
}
function qr(e, t, r) {
  return e.pending_buf[e.d_buf + 2 * e.last_lit] = t >>> 8 & 255, e.pending_buf[e.d_buf + 2 * e.last_lit + 1] = 255 & t, e.pending_buf[e.l_buf + e.last_lit] = 255 & r, e.last_lit++, 0 === t ? e.dyn_ltree[2 * r]++ : (e.matches++, t--, e.dyn_ltree[2 * (mr[r] + rr + 1)]++, e.dyn_dtree[2 * zr(t)]++), e.last_lit === e.lit_bufsize - 1;
}
function Vr(e, t, r, n) {
  for (var i = 65535 & e | 0, o = e >>> 16 & 65535 | 0, a = 0; 0 !== r;) {
    r -= a = r > 2e3 ? 2e3 : r;
    do {
      o = o + (i = i + t[n++] | 0) | 0;
    } while (--a);
    i %= 65521, o %= 65521;
  }
  return i | o << 16 | 0;
}
var Gr = function () {
  for (var e, t = [], r = 0; r < 256; r++) {
    e = r;
    for (var n = 0; n < 8; n++) {
      e = 1 & e ? 3988292384 ^ e >>> 1 : e >>> 1;
    }
    t[r] = e;
  }
  return t;
}();
function $r(e, t, r, n) {
  var i = Gr,
    o = n + r;
  e ^= -1;
  for (var a = n; a < o; a++) {
    e = e >>> 8 ^ i[255 & (e ^ t[a])];
  }
  return -1 ^ e;
}
var Jr,
  Qr = 0,
  en = 1,
  tn = 3,
  rn = 4,
  nn = 5,
  on = 0,
  an = 1,
  sn = -2,
  hn = -3,
  ln = -5,
  fn = -1,
  cn = 1,
  un = 2,
  dn = 3,
  pn = 4,
  _n = 2,
  gn = 8,
  vn = 9,
  wn = 286,
  bn = 30,
  yn = 19,
  mn = 2 * wn + 1,
  kn = 15,
  En = 3,
  Sn = 258,
  xn = Sn + En + 1,
  Rn = 32,
  An = 42,
  Bn = 69,
  zn = 73,
  Ln = 91,
  Tn = 103,
  Mn = 113,
  Cn = 666,
  Dn = 1,
  In = 2,
  Pn = 3,
  On = 4,
  Un = 3;
function Hn(e, t) {
  return e.msg = Nt[t], t;
}
function Fn(e) {
  return (e << 1) - (e > 4 ? 9 : 0);
}
function Nn(e) {
  for (var t = e.length; --t >= 0;) {
    e[t] = 0;
  }
}
function Zn(e) {
  var t = e.state,
    r = t.pending;
  r > e.avail_out && (r = e.avail_out), 0 !== r && (jt(e.output, t.pending_buf, t.pending_out, r, e.next_out), e.next_out += r, t.pending_out += r, e.total_out += r, e.avail_out -= r, t.pending -= r, 0 === t.pending && (t.pending_out = 0));
}
function jn(e, t) {
  Xr(e, e.block_start >= 0 ? e.block_start : -1, e.strstart - e.block_start, t), e.block_start = e.strstart, Zn(e.strm);
}
function Wn(e, t) {
  e.pending_buf[e.pending++] = t;
}
function Yn(e, t) {
  e.pending_buf[e.pending++] = t >>> 8 & 255, e.pending_buf[e.pending++] = 255 & t;
}
function Kn(e, t) {
  var r,
    n,
    i = e.max_chain_length,
    o = e.strstart,
    a = e.prev_length,
    s = e.nice_match,
    h = e.strstart > e.w_size - xn ? e.strstart - (e.w_size - xn) : 0,
    l = e.window,
    f = e.w_mask,
    c = e.prev,
    u = e.strstart + Sn,
    d = l[o + a - 1],
    p = l[o + a];
  e.prev_length >= e.good_match && (i >>= 2), s > e.lookahead && (s = e.lookahead);
  do {
    if (l[(r = t) + a] === p && l[r + a - 1] === d && l[r] === l[o] && l[++r] === l[o + 1]) {
      o += 2, r++;
      do {} while (l[++o] === l[++r] && l[++o] === l[++r] && l[++o] === l[++r] && l[++o] === l[++r] && l[++o] === l[++r] && l[++o] === l[++r] && l[++o] === l[++r] && l[++o] === l[++r] && o < u);
      if (n = Sn - (u - o), o = u - Sn, n > a) {
        if (e.match_start = t, a = n, n >= s) break;
        d = l[o + a - 1], p = l[o + a];
      }
    }
  } while ((t = c[t & f]) > h && 0 != --i);
  return a <= e.lookahead ? a : e.lookahead;
}
function Xn(e) {
  var t,
    r,
    n,
    i,
    o,
    a,
    s,
    h,
    l,
    f,
    c = e.w_size;
  do {
    if (i = e.window_size - e.lookahead - e.strstart, e.strstart >= c + (c - xn)) {
      jt(e.window, e.window, c, c, 0), e.match_start -= c, e.strstart -= c, e.block_start -= c, t = r = e.hash_size;
      do {
        n = e.head[--t], e.head[t] = n >= c ? n - c : 0;
      } while (--r);
      t = r = c;
      do {
        n = e.prev[--t], e.prev[t] = n >= c ? n - c : 0;
      } while (--r);
      i += c;
    }
    if (0 === e.strm.avail_in) break;
    if (a = e.strm, s = e.window, h = e.strstart + e.lookahead, l = i, f = void 0, (f = a.avail_in) > l && (f = l), r = 0 === f ? 0 : (a.avail_in -= f, jt(s, a.input, a.next_in, f, h), 1 === a.state.wrap ? a.adler = Vr(a.adler, s, f, h) : 2 === a.state.wrap && (a.adler = $r(a.adler, s, f, h)), a.next_in += f, a.total_in += f, f), e.lookahead += r, e.lookahead + e.insert >= En) for (o = e.strstart - e.insert, e.ins_h = e.window[o], e.ins_h = (e.ins_h << e.hash_shift ^ e.window[o + 1]) & e.hash_mask; e.insert && (e.ins_h = (e.ins_h << e.hash_shift ^ e.window[o + En - 1]) & e.hash_mask, e.prev[o & e.w_mask] = e.head[e.ins_h], e.head[e.ins_h] = o, o++, e.insert--, !(e.lookahead + e.insert < En));) {
      ;
    }
  } while (e.lookahead < xn && 0 !== e.strm.avail_in);
}
function qn(e, t) {
  for (var r, n;;) {
    if (e.lookahead < xn) {
      if (Xn(e), e.lookahead < xn && t === Qr) return Dn;
      if (0 === e.lookahead) break;
    }
    if (r = 0, e.lookahead >= En && (e.ins_h = (e.ins_h << e.hash_shift ^ e.window[e.strstart + En - 1]) & e.hash_mask, r = e.prev[e.strstart & e.w_mask] = e.head[e.ins_h], e.head[e.ins_h] = e.strstart), 0 !== r && e.strstart - r <= e.w_size - xn && (e.match_length = Kn(e, r)), e.match_length >= En) {
      if (n = qr(e, e.strstart - e.match_start, e.match_length - En), e.lookahead -= e.match_length, e.match_length <= e.max_lazy_match && e.lookahead >= En) {
        e.match_length--;
        do {
          e.strstart++, e.ins_h = (e.ins_h << e.hash_shift ^ e.window[e.strstart + En - 1]) & e.hash_mask, r = e.prev[e.strstart & e.w_mask] = e.head[e.ins_h], e.head[e.ins_h] = e.strstart;
        } while (0 != --e.match_length);
        e.strstart++;
      } else e.strstart += e.match_length, e.match_length = 0, e.ins_h = e.window[e.strstart], e.ins_h = (e.ins_h << e.hash_shift ^ e.window[e.strstart + 1]) & e.hash_mask;
    } else n = qr(e, 0, e.window[e.strstart]), e.lookahead--, e.strstart++;
    if (n && (jn(e, !1), 0 === e.strm.avail_out)) return Dn;
  }
  return e.insert = e.strstart < En - 1 ? e.strstart : En - 1, t === rn ? (jn(e, !0), 0 === e.strm.avail_out ? Pn : On) : e.last_lit && (jn(e, !1), 0 === e.strm.avail_out) ? Dn : In;
}
function Vn(e, t) {
  for (var r, n, i;;) {
    if (e.lookahead < xn) {
      if (Xn(e), e.lookahead < xn && t === Qr) return Dn;
      if (0 === e.lookahead) break;
    }
    if (r = 0, e.lookahead >= En && (e.ins_h = (e.ins_h << e.hash_shift ^ e.window[e.strstart + En - 1]) & e.hash_mask, r = e.prev[e.strstart & e.w_mask] = e.head[e.ins_h], e.head[e.ins_h] = e.strstart), e.prev_length = e.match_length, e.prev_match = e.match_start, e.match_length = En - 1, 0 !== r && e.prev_length < e.max_lazy_match && e.strstart - r <= e.w_size - xn && (e.match_length = Kn(e, r), e.match_length <= 5 && (e.strategy === cn || e.match_length === En && e.strstart - e.match_start > 4096) && (e.match_length = En - 1)), e.prev_length >= En && e.match_length <= e.prev_length) {
      i = e.strstart + e.lookahead - En, n = qr(e, e.strstart - 1 - e.prev_match, e.prev_length - En), e.lookahead -= e.prev_length - 1, e.prev_length -= 2;
      do {
        ++e.strstart <= i && (e.ins_h = (e.ins_h << e.hash_shift ^ e.window[e.strstart + En - 1]) & e.hash_mask, r = e.prev[e.strstart & e.w_mask] = e.head[e.ins_h], e.head[e.ins_h] = e.strstart);
      } while (0 != --e.prev_length);
      if (e.match_available = 0, e.match_length = En - 1, e.strstart++, n && (jn(e, !1), 0 === e.strm.avail_out)) return Dn;
    } else if (e.match_available) {
      if ((n = qr(e, 0, e.window[e.strstart - 1])) && jn(e, !1), e.strstart++, e.lookahead--, 0 === e.strm.avail_out) return Dn;
    } else e.match_available = 1, e.strstart++, e.lookahead--;
  }
  return e.match_available && (n = qr(e, 0, e.window[e.strstart - 1]), e.match_available = 0), e.insert = e.strstart < En - 1 ? e.strstart : En - 1, t === rn ? (jn(e, !0), 0 === e.strm.avail_out ? Pn : On) : e.last_lit && (jn(e, !1), 0 === e.strm.avail_out) ? Dn : In;
}
function Gn(e, t, r, n, i) {
  this.good_length = e, this.max_lazy = t, this.nice_length = r, this.max_chain = n, this.func = i;
}
function $n() {
  this.strm = null, this.status = 0, this.pending_buf = null, this.pending_buf_size = 0, this.pending_out = 0, this.pending = 0, this.wrap = 0, this.gzhead = null, this.gzindex = 0, this.method = gn, this.last_flush = -1, this.w_size = 0, this.w_bits = 0, this.w_mask = 0, this.window = null, this.window_size = 0, this.prev = null, this.head = null, this.ins_h = 0, this.hash_size = 0, this.hash_bits = 0, this.hash_mask = 0, this.hash_shift = 0, this.block_start = 0, this.match_length = 0, this.prev_match = 0, this.match_available = 0, this.strstart = 0, this.match_start = 0, this.lookahead = 0, this.prev_length = 0, this.max_chain_length = 0, this.max_lazy_match = 0, this.level = 0, this.strategy = 0, this.good_match = 0, this.nice_match = 0, this.dyn_ltree = new Yt(2 * mn), this.dyn_dtree = new Yt(2 * (2 * bn + 1)), this.bl_tree = new Yt(2 * (2 * yn + 1)), Nn(this.dyn_ltree), Nn(this.dyn_dtree), Nn(this.bl_tree), this.l_desc = null, this.d_desc = null, this.bl_desc = null, this.bl_count = new Yt(kn + 1), this.heap = new Yt(2 * wn + 1), Nn(this.heap), this.heap_len = 0, this.heap_max = 0, this.depth = new Yt(2 * wn + 1), Nn(this.depth), this.l_buf = 0, this.lit_bufsize = 0, this.last_lit = 0, this.d_buf = 0, this.opt_len = 0, this.static_len = 0, this.matches = 0, this.insert = 0, this.bi_buf = 0, this.bi_valid = 0;
}
function Jn(e) {
  var t,
    r = function (e) {
      var t;
      return e && e.state ? (e.total_in = e.total_out = 0, e.data_type = _n, (t = e.state).pending = 0, t.pending_out = 0, t.wrap < 0 && (t.wrap = -t.wrap), t.status = t.wrap ? An : Mn, e.adler = 2 === t.wrap ? 0 : 1, t.last_flush = Qr, Wr(t), on) : Hn(e, sn);
    }(e);
  return r === on && ((t = e.state).window_size = 2 * t.w_size, Nn(t.head), t.max_lazy_match = Jr[t.level].max_lazy, t.good_match = Jr[t.level].good_length, t.nice_match = Jr[t.level].nice_length, t.max_chain_length = Jr[t.level].max_chain, t.strstart = 0, t.block_start = 0, t.lookahead = 0, t.insert = 0, t.match_length = t.prev_length = En - 1, t.match_available = 0, t.ins_h = 0), r;
}
function Qn(e, t) {
  var r, n, i, o;
  if (!e || !e.state || t > nn || t < 0) return e ? Hn(e, sn) : sn;
  if (n = e.state, !e.output || !e.input && 0 !== e.avail_in || n.status === Cn && t !== rn) return Hn(e, 0 === e.avail_out ? ln : sn);
  if (n.strm = e, r = n.last_flush, n.last_flush = t, n.status === An) if (2 === n.wrap) e.adler = 0, Wn(n, 31), Wn(n, 139), Wn(n, 8), n.gzhead ? (Wn(n, (n.gzhead.text ? 1 : 0) + (n.gzhead.hcrc ? 2 : 0) + (n.gzhead.extra ? 4 : 0) + (n.gzhead.name ? 8 : 0) + (n.gzhead.comment ? 16 : 0)), Wn(n, 255 & n.gzhead.time), Wn(n, n.gzhead.time >> 8 & 255), Wn(n, n.gzhead.time >> 16 & 255), Wn(n, n.gzhead.time >> 24 & 255), Wn(n, 9 === n.level ? 2 : n.strategy >= un || n.level < 2 ? 4 : 0), Wn(n, 255 & n.gzhead.os), n.gzhead.extra && n.gzhead.extra.length && (Wn(n, 255 & n.gzhead.extra.length), Wn(n, n.gzhead.extra.length >> 8 & 255)), n.gzhead.hcrc && (e.adler = $r(e.adler, n.pending_buf, n.pending, 0)), n.gzindex = 0, n.status = Bn) : (Wn(n, 0), Wn(n, 0), Wn(n, 0), Wn(n, 0), Wn(n, 0), Wn(n, 9 === n.level ? 2 : n.strategy >= un || n.level < 2 ? 4 : 0), Wn(n, Un), n.status = Mn);else {
    var a = gn + (n.w_bits - 8 << 4) << 8;
    a |= (n.strategy >= un || n.level < 2 ? 0 : n.level < 6 ? 1 : 6 === n.level ? 2 : 3) << 6, 0 !== n.strstart && (a |= Rn), a += 31 - a % 31, n.status = Mn, Yn(n, a), 0 !== n.strstart && (Yn(n, e.adler >>> 16), Yn(n, 65535 & e.adler)), e.adler = 1;
  }
  if (n.status === Bn) if (n.gzhead.extra) {
    for (i = n.pending; n.gzindex < (65535 & n.gzhead.extra.length) && (n.pending !== n.pending_buf_size || (n.gzhead.hcrc && n.pending > i && (e.adler = $r(e.adler, n.pending_buf, n.pending - i, i)), Zn(e), i = n.pending, n.pending !== n.pending_buf_size));) {
      Wn(n, 255 & n.gzhead.extra[n.gzindex]), n.gzindex++;
    }
    n.gzhead.hcrc && n.pending > i && (e.adler = $r(e.adler, n.pending_buf, n.pending - i, i)), n.gzindex === n.gzhead.extra.length && (n.gzindex = 0, n.status = zn);
  } else n.status = zn;
  if (n.status === zn) if (n.gzhead.name) {
    i = n.pending;
    do {
      if (n.pending === n.pending_buf_size && (n.gzhead.hcrc && n.pending > i && (e.adler = $r(e.adler, n.pending_buf, n.pending - i, i)), Zn(e), i = n.pending, n.pending === n.pending_buf_size)) {
        o = 1;
        break;
      }
      o = n.gzindex < n.gzhead.name.length ? 255 & n.gzhead.name.charCodeAt(n.gzindex++) : 0, Wn(n, o);
    } while (0 !== o);
    n.gzhead.hcrc && n.pending > i && (e.adler = $r(e.adler, n.pending_buf, n.pending - i, i)), 0 === o && (n.gzindex = 0, n.status = Ln);
  } else n.status = Ln;
  if (n.status === Ln) if (n.gzhead.comment) {
    i = n.pending;
    do {
      if (n.pending === n.pending_buf_size && (n.gzhead.hcrc && n.pending > i && (e.adler = $r(e.adler, n.pending_buf, n.pending - i, i)), Zn(e), i = n.pending, n.pending === n.pending_buf_size)) {
        o = 1;
        break;
      }
      o = n.gzindex < n.gzhead.comment.length ? 255 & n.gzhead.comment.charCodeAt(n.gzindex++) : 0, Wn(n, o);
    } while (0 !== o);
    n.gzhead.hcrc && n.pending > i && (e.adler = $r(e.adler, n.pending_buf, n.pending - i, i)), 0 === o && (n.status = Tn);
  } else n.status = Tn;
  if (n.status === Tn && (n.gzhead.hcrc ? (n.pending + 2 > n.pending_buf_size && Zn(e), n.pending + 2 <= n.pending_buf_size && (Wn(n, 255 & e.adler), Wn(n, e.adler >> 8 & 255), e.adler = 0, n.status = Mn)) : n.status = Mn), 0 !== n.pending) {
    if (Zn(e), 0 === e.avail_out) return n.last_flush = -1, on;
  } else if (0 === e.avail_in && Fn(t) <= Fn(r) && t !== rn) return Hn(e, ln);
  if (n.status === Cn && 0 !== e.avail_in) return Hn(e, ln);
  if (0 !== e.avail_in || 0 !== n.lookahead || t !== Qr && n.status !== Cn) {
    var s = n.strategy === un ? function (e, t) {
      for (var r;;) {
        if (0 === e.lookahead && (Xn(e), 0 === e.lookahead)) {
          if (t === Qr) return Dn;
          break;
        }
        if (e.match_length = 0, r = qr(e, 0, e.window[e.strstart]), e.lookahead--, e.strstart++, r && (jn(e, !1), 0 === e.strm.avail_out)) return Dn;
      }
      return e.insert = 0, t === rn ? (jn(e, !0), 0 === e.strm.avail_out ? Pn : On) : e.last_lit && (jn(e, !1), 0 === e.strm.avail_out) ? Dn : In;
    }(n, t) : n.strategy === dn ? function (e, t) {
      for (var r, n, i, o, a = e.window;;) {
        if (e.lookahead <= Sn) {
          if (Xn(e), e.lookahead <= Sn && t === Qr) return Dn;
          if (0 === e.lookahead) break;
        }
        if (e.match_length = 0, e.lookahead >= En && e.strstart > 0 && (n = a[i = e.strstart - 1]) === a[++i] && n === a[++i] && n === a[++i]) {
          o = e.strstart + Sn;
          do {} while (n === a[++i] && n === a[++i] && n === a[++i] && n === a[++i] && n === a[++i] && n === a[++i] && n === a[++i] && n === a[++i] && i < o);
          e.match_length = Sn - (o - i), e.match_length > e.lookahead && (e.match_length = e.lookahead);
        }
        if (e.match_length >= En ? (r = qr(e, 1, e.match_length - En), e.lookahead -= e.match_length, e.strstart += e.match_length, e.match_length = 0) : (r = qr(e, 0, e.window[e.strstart]), e.lookahead--, e.strstart++), r && (jn(e, !1), 0 === e.strm.avail_out)) return Dn;
      }
      return e.insert = 0, t === rn ? (jn(e, !0), 0 === e.strm.avail_out ? Pn : On) : e.last_lit && (jn(e, !1), 0 === e.strm.avail_out) ? Dn : In;
    }(n, t) : Jr[n.level].func(n, t);
    if (s !== Pn && s !== On || (n.status = Cn), s === Dn || s === Pn) return 0 === e.avail_out && (n.last_flush = -1), on;
    if (s === In && (t === en ? Kr(n) : t !== nn && (Yr(n, 0, 0, !1), t === tn && (Nn(n.head), 0 === n.lookahead && (n.strstart = 0, n.block_start = 0, n.insert = 0))), Zn(e), 0 === e.avail_out)) return n.last_flush = -1, on;
  }
  return t !== rn ? on : n.wrap <= 0 ? an : (2 === n.wrap ? (Wn(n, 255 & e.adler), Wn(n, e.adler >> 8 & 255), Wn(n, e.adler >> 16 & 255), Wn(n, e.adler >> 24 & 255), Wn(n, 255 & e.total_in), Wn(n, e.total_in >> 8 & 255), Wn(n, e.total_in >> 16 & 255), Wn(n, e.total_in >> 24 & 255)) : (Yn(n, e.adler >>> 16), Yn(n, 65535 & e.adler)), Zn(e), n.wrap > 0 && (n.wrap = -n.wrap), 0 !== n.pending ? on : an);
}
Jr = [new Gn(0, 0, 0, 0, function (e, t) {
  var r = 65535;
  for (r > e.pending_buf_size - 5 && (r = e.pending_buf_size - 5);;) {
    if (e.lookahead <= 1) {
      if (Xn(e), 0 === e.lookahead && t === Qr) return Dn;
      if (0 === e.lookahead) break;
    }
    e.strstart += e.lookahead, e.lookahead = 0;
    var n = e.block_start + r;
    if ((0 === e.strstart || e.strstart >= n) && (e.lookahead = e.strstart - n, e.strstart = n, jn(e, !1), 0 === e.strm.avail_out)) return Dn;
    if (e.strstart - e.block_start >= e.w_size - xn && (jn(e, !1), 0 === e.strm.avail_out)) return Dn;
  }
  return e.insert = 0, t === rn ? (jn(e, !0), 0 === e.strm.avail_out ? Pn : On) : (e.strstart > e.block_start && (jn(e, !1), e.strm.avail_out), Dn);
}), new Gn(4, 4, 8, 4, qn), new Gn(4, 5, 16, 8, qn), new Gn(4, 6, 32, 32, qn), new Gn(4, 4, 16, 16, Vn), new Gn(8, 16, 32, 32, Vn), new Gn(8, 16, 128, 128, Vn), new Gn(8, 32, 128, 256, Vn), new Gn(32, 128, 258, 1024, Vn), new Gn(32, 258, 258, 4096, Vn)];
var ei = 30,
  ti = 12;
function ri(e, t) {
  var r, n, i, o, a, s, h, l, f, c, u, d, p, _, g, v, w, b, y, m, k, E, S, x, R;
  r = e.state, n = e.next_in, x = e.input, i = n + (e.avail_in - 5), o = e.next_out, R = e.output, a = o - (t - e.avail_out), s = o + (e.avail_out - 257), h = r.dmax, l = r.wsize, f = r.whave, c = r.wnext, u = r.window, d = r.hold, p = r.bits, _ = r.lencode, g = r.distcode, v = (1 << r.lenbits) - 1, w = (1 << r.distbits) - 1;
  e: do {
    p < 15 && (d += x[n++] << p, p += 8, d += x[n++] << p, p += 8), b = _[d & v];
    t: for (;;) {
      if (d >>>= y = b >>> 24, p -= y, 0 === (y = b >>> 16 & 255)) R[o++] = 65535 & b;else {
        if (!(16 & y)) {
          if (0 == (64 & y)) {
            b = _[(65535 & b) + (d & (1 << y) - 1)];
            continue t;
          }
          if (32 & y) {
            r.mode = ti;
            break e;
          }
          e.msg = "invalid literal/length code", r.mode = ei;
          break e;
        }
        m = 65535 & b, (y &= 15) && (p < y && (d += x[n++] << p, p += 8), m += d & (1 << y) - 1, d >>>= y, p -= y), p < 15 && (d += x[n++] << p, p += 8, d += x[n++] << p, p += 8), b = g[d & w];
        r: for (;;) {
          if (d >>>= y = b >>> 24, p -= y, !(16 & (y = b >>> 16 & 255))) {
            if (0 == (64 & y)) {
              b = g[(65535 & b) + (d & (1 << y) - 1)];
              continue r;
            }
            e.msg = "invalid distance code", r.mode = ei;
            break e;
          }
          if (k = 65535 & b, p < (y &= 15) && (d += x[n++] << p, (p += 8) < y && (d += x[n++] << p, p += 8)), (k += d & (1 << y) - 1) > h) {
            e.msg = "invalid distance too far back", r.mode = ei;
            break e;
          }
          if (d >>>= y, p -= y, k > (y = o - a)) {
            if ((y = k - y) > f && r.sane) {
              e.msg = "invalid distance too far back", r.mode = ei;
              break e;
            }
            if (E = 0, S = u, 0 === c) {
              if (E += l - y, y < m) {
                m -= y;
                do {
                  R[o++] = u[E++];
                } while (--y);
                E = o - k, S = R;
              }
            } else if (c < y) {
              if (E += l + c - y, (y -= c) < m) {
                m -= y;
                do {
                  R[o++] = u[E++];
                } while (--y);
                if (E = 0, c < m) {
                  m -= y = c;
                  do {
                    R[o++] = u[E++];
                  } while (--y);
                  E = o - k, S = R;
                }
              }
            } else if (E += c - y, y < m) {
              m -= y;
              do {
                R[o++] = u[E++];
              } while (--y);
              E = o - k, S = R;
            }
            for (; m > 2;) {
              R[o++] = S[E++], R[o++] = S[E++], R[o++] = S[E++], m -= 3;
            }
            m && (R[o++] = S[E++], m > 1 && (R[o++] = S[E++]));
          } else {
            E = o - k;
            do {
              R[o++] = R[E++], R[o++] = R[E++], R[o++] = R[E++], m -= 3;
            } while (m > 2);
            m && (R[o++] = R[E++], m > 1 && (R[o++] = R[E++]));
          }
          break;
        }
      }
      break;
    }
  } while (n < i && o < s);
  n -= m = p >> 3, d &= (1 << (p -= m << 3)) - 1, e.next_in = n, e.next_out = o, e.avail_in = n < i ? i - n + 5 : 5 - (n - i), e.avail_out = o < s ? s - o + 257 : 257 - (o - s), r.hold = d, r.bits = p;
}
var ni = 15,
  ii = 852,
  oi = 592,
  ai = 0,
  si = 1,
  hi = 2,
  li = [3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 15, 17, 19, 23, 27, 31, 35, 43, 51, 59, 67, 83, 99, 115, 131, 163, 195, 227, 258, 0, 0],
  fi = [16, 16, 16, 16, 16, 16, 16, 16, 17, 17, 17, 17, 18, 18, 18, 18, 19, 19, 19, 19, 20, 20, 20, 20, 21, 21, 21, 21, 16, 72, 78],
  ci = [1, 2, 3, 4, 5, 7, 9, 13, 17, 25, 33, 49, 65, 97, 129, 193, 257, 385, 513, 769, 1025, 1537, 2049, 3073, 4097, 6145, 8193, 12289, 16385, 24577, 0, 0],
  ui = [16, 16, 16, 16, 17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23, 23, 24, 24, 25, 25, 26, 26, 27, 27, 28, 28, 29, 29, 64, 64];
function di(e, t, r, n, i, o, a, s) {
  var h,
    l,
    f,
    c,
    u,
    d,
    p,
    _,
    g,
    v = s.bits,
    w = 0,
    b = 0,
    y = 0,
    m = 0,
    k = 0,
    E = 0,
    S = 0,
    x = 0,
    R = 0,
    A = 0,
    B = null,
    z = 0,
    L = new Yt(ni + 1),
    T = new Yt(ni + 1),
    M = null,
    C = 0;
  for (w = 0; w <= ni; w++) {
    L[w] = 0;
  }
  for (b = 0; b < n; b++) {
    L[t[r + b]]++;
  }
  for (k = v, m = ni; m >= 1 && 0 === L[m]; m--) {
    ;
  }
  if (k > m && (k = m), 0 === m) return i[o++] = 20971520, i[o++] = 20971520, s.bits = 1, 0;
  for (y = 1; y < m && 0 === L[y]; y++) {
    ;
  }
  for (k < y && (k = y), x = 1, w = 1; w <= ni; w++) {
    if (x <<= 1, (x -= L[w]) < 0) return -1;
  }
  if (x > 0 && (e === ai || 1 !== m)) return -1;
  for (T[1] = 0, w = 1; w < ni; w++) {
    T[w + 1] = T[w] + L[w];
  }
  for (b = 0; b < n; b++) {
    0 !== t[r + b] && (a[T[t[r + b]]++] = b);
  }
  if (e === ai ? (B = M = a, d = 19) : e === si ? (B = li, z -= 257, M = fi, C -= 257, d = 256) : (B = ci, M = ui, d = -1), A = 0, b = 0, w = y, u = o, E = k, S = 0, f = -1, c = (R = 1 << k) - 1, e === si && R > ii || e === hi && R > oi) return 1;
  for (;;) {
    p = w - S, a[b] < d ? (_ = 0, g = a[b]) : a[b] > d ? (_ = M[C + a[b]], g = B[z + a[b]]) : (_ = 96, g = 0), h = 1 << w - S, y = l = 1 << E;
    do {
      i[u + (A >> S) + (l -= h)] = p << 24 | _ << 16 | g | 0;
    } while (0 !== l);
    for (h = 1 << w - 1; A & h;) {
      h >>= 1;
    }
    if (0 !== h ? (A &= h - 1, A += h) : A = 0, b++, 0 == --L[w]) {
      if (w === m) break;
      w = t[r + a[b]];
    }
    if (w > k && (A & c) !== f) {
      for (0 === S && (S = k), u += y, x = 1 << (E = w - S); E + S < m && !((x -= L[E + S]) <= 0);) {
        E++, x <<= 1;
      }
      if (R += 1 << E, e === si && R > ii || e === hi && R > oi) return 1;
      i[f = A & c] = k << 24 | E << 16 | u - o | 0;
    }
  }
  return 0 !== A && (i[u + A] = w - S << 24 | 64 << 16 | 0), s.bits = k, 0;
}
var pi = 0,
  _i = 1,
  gi = 2,
  vi = 4,
  wi = 5,
  bi = 6,
  yi = 0,
  mi = 1,
  ki = 2,
  Ei = -2,
  Si = -3,
  xi = -4,
  Ri = -5,
  Ai = 8,
  Bi = 1,
  zi = 2,
  Li = 3,
  Ti = 4,
  Mi = 5,
  Ci = 6,
  Di = 7,
  Ii = 8,
  Pi = 9,
  Oi = 10,
  Ui = 11,
  Hi = 12,
  Fi = 13,
  Ni = 14,
  Zi = 15,
  ji = 16,
  Wi = 17,
  Yi = 18,
  Ki = 19,
  Xi = 20,
  qi = 21,
  Vi = 22,
  Gi = 23,
  $i = 24,
  Ji = 25,
  Qi = 26,
  eo = 27,
  to = 28,
  ro = 29,
  no = 30,
  io = 31,
  oo = 32,
  ao = 852,
  so = 592;
function ho(e) {
  return (e >>> 24 & 255) + (e >>> 8 & 65280) + ((65280 & e) << 8) + ((255 & e) << 24);
}
function lo() {
  this.mode = 0, this.last = !1, this.wrap = 0, this.havedict = !1, this.flags = 0, this.dmax = 0, this.check = 0, this.total = 0, this.head = null, this.wbits = 0, this.wsize = 0, this.whave = 0, this.wnext = 0, this.window = null, this.hold = 0, this.bits = 0, this.length = 0, this.offset = 0, this.extra = 0, this.lencode = null, this.distcode = null, this.lenbits = 0, this.distbits = 0, this.ncode = 0, this.nlen = 0, this.ndist = 0, this.have = 0, this.next = null, this.lens = new Yt(320), this.work = new Yt(288), this.lendyn = null, this.distdyn = null, this.sane = 0, this.back = 0, this.was = 0;
}
function fo(e) {
  var t;
  return e && e.state ? ((t = e.state).wsize = 0, t.whave = 0, t.wnext = 0, function (e) {
    var t;
    return e && e.state ? (t = e.state, e.total_in = e.total_out = t.total = 0, e.msg = "", t.wrap && (e.adler = 1 & t.wrap), t.mode = Bi, t.last = 0, t.havedict = 0, t.dmax = 32768, t.head = null, t.hold = 0, t.bits = 0, t.lencode = t.lendyn = new Kt(ao), t.distcode = t.distdyn = new Kt(so), t.sane = 1, t.back = -1, yi) : Ei;
  }(e)) : Ei;
}
function co(e, t) {
  var r, n;
  return e ? (n = new lo(), e.state = n, n.window = null, (r = function (e, t) {
    var r, n;
    return e && e.state ? (n = e.state, t < 0 ? (r = 0, t = -t) : (r = 1 + (t >> 4), t < 48 && (t &= 15)), t && (t < 8 || t > 15) ? Ei : (null !== n.window && n.wbits !== t && (n.window = null), n.wrap = r, n.wbits = t, fo(e))) : Ei;
  }(e, t)) !== yi && (e.state = null), r) : Ei;
}
var uo,
  po,
  _o = !0;
function go(e) {
  if (_o) {
    var t;
    for (uo = new Kt(512), po = new Kt(32), t = 0; t < 144;) {
      e.lens[t++] = 8;
    }
    for (; t < 256;) {
      e.lens[t++] = 9;
    }
    for (; t < 280;) {
      e.lens[t++] = 7;
    }
    for (; t < 288;) {
      e.lens[t++] = 8;
    }
    for (di(_i, e.lens, 0, 288, uo, 0, e.work, {
      bits: 9
    }), t = 0; t < 32;) {
      e.lens[t++] = 5;
    }
    di(gi, e.lens, 0, 32, po, 0, e.work, {
      bits: 5
    }), _o = !1;
  }
  e.lencode = uo, e.lenbits = 9, e.distcode = po, e.distbits = 5;
}
function vo(e, t) {
  var r,
    n,
    i,
    o,
    a,
    s,
    h,
    l,
    f,
    c,
    u,
    d,
    p,
    _,
    g,
    v,
    w,
    b,
    y,
    m,
    k,
    E,
    S,
    x,
    R = 0,
    A = new Wt(4),
    B = [16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15];
  if (!e || !e.state || !e.output || !e.input && 0 !== e.avail_in) return Ei;
  (r = e.state).mode === Hi && (r.mode = Fi), a = e.next_out, i = e.output, h = e.avail_out, o = e.next_in, n = e.input, s = e.avail_in, l = r.hold, f = r.bits, c = s, u = h, E = yi;
  e: for (;;) {
    switch (r.mode) {
      case Bi:
        if (0 === r.wrap) {
          r.mode = Fi;
          break;
        }
        for (; f < 16;) {
          if (0 === s) break e;
          s--, l += n[o++] << f, f += 8;
        }
        if (2 & r.wrap && 35615 === l) {
          r.check = 0, A[0] = 255 & l, A[1] = l >>> 8 & 255, r.check = $r(r.check, A, 2, 0), l = 0, f = 0, r.mode = zi;
          break;
        }
        if (r.flags = 0, r.head && (r.head.done = !1), !(1 & r.wrap) || (((255 & l) << 8) + (l >> 8)) % 31) {
          e.msg = "incorrect header check", r.mode = no;
          break;
        }
        if ((15 & l) !== Ai) {
          e.msg = "unknown compression method", r.mode = no;
          break;
        }
        if (f -= 4, k = 8 + (15 & (l >>>= 4)), 0 === r.wbits) r.wbits = k;else if (k > r.wbits) {
          e.msg = "invalid window size", r.mode = no;
          break;
        }
        r.dmax = 1 << k, e.adler = r.check = 1, r.mode = 512 & l ? Oi : Hi, l = 0, f = 0;
        break;
      case zi:
        for (; f < 16;) {
          if (0 === s) break e;
          s--, l += n[o++] << f, f += 8;
        }
        if (r.flags = l, (255 & r.flags) !== Ai) {
          e.msg = "unknown compression method", r.mode = no;
          break;
        }
        if (57344 & r.flags) {
          e.msg = "unknown header flags set", r.mode = no;
          break;
        }
        r.head && (r.head.text = l >> 8 & 1), 512 & r.flags && (A[0] = 255 & l, A[1] = l >>> 8 & 255, r.check = $r(r.check, A, 2, 0)), l = 0, f = 0, r.mode = Li;
      case Li:
        for (; f < 32;) {
          if (0 === s) break e;
          s--, l += n[o++] << f, f += 8;
        }
        r.head && (r.head.time = l), 512 & r.flags && (A[0] = 255 & l, A[1] = l >>> 8 & 255, A[2] = l >>> 16 & 255, A[3] = l >>> 24 & 255, r.check = $r(r.check, A, 4, 0)), l = 0, f = 0, r.mode = Ti;
      case Ti:
        for (; f < 16;) {
          if (0 === s) break e;
          s--, l += n[o++] << f, f += 8;
        }
        r.head && (r.head.xflags = 255 & l, r.head.os = l >> 8), 512 & r.flags && (A[0] = 255 & l, A[1] = l >>> 8 & 255, r.check = $r(r.check, A, 2, 0)), l = 0, f = 0, r.mode = Mi;
      case Mi:
        if (1024 & r.flags) {
          for (; f < 16;) {
            if (0 === s) break e;
            s--, l += n[o++] << f, f += 8;
          }
          r.length = l, r.head && (r.head.extra_len = l), 512 & r.flags && (A[0] = 255 & l, A[1] = l >>> 8 & 255, r.check = $r(r.check, A, 2, 0)), l = 0, f = 0;
        } else r.head && (r.head.extra = null);
        r.mode = Ci;
      case Ci:
        if (1024 & r.flags && ((d = r.length) > s && (d = s), d && (r.head && (k = r.head.extra_len - r.length, r.head.extra || (r.head.extra = new Array(r.head.extra_len)), jt(r.head.extra, n, o, d, k)), 512 & r.flags && (r.check = $r(r.check, n, d, o)), s -= d, o += d, r.length -= d), r.length)) break e;
        r.length = 0, r.mode = Di;
      case Di:
        if (2048 & r.flags) {
          if (0 === s) break e;
          d = 0;
          do {
            k = n[o + d++], r.head && k && r.length < 65536 && (r.head.name += String.fromCharCode(k));
          } while (k && d < s);
          if (512 & r.flags && (r.check = $r(r.check, n, d, o)), s -= d, o += d, k) break e;
        } else r.head && (r.head.name = null);
        r.length = 0, r.mode = Ii;
      case Ii:
        if (4096 & r.flags) {
          if (0 === s) break e;
          d = 0;
          do {
            k = n[o + d++], r.head && k && r.length < 65536 && (r.head.comment += String.fromCharCode(k));
          } while (k && d < s);
          if (512 & r.flags && (r.check = $r(r.check, n, d, o)), s -= d, o += d, k) break e;
        } else r.head && (r.head.comment = null);
        r.mode = Pi;
      case Pi:
        if (512 & r.flags) {
          for (; f < 16;) {
            if (0 === s) break e;
            s--, l += n[o++] << f, f += 8;
          }
          if (l !== (65535 & r.check)) {
            e.msg = "header crc mismatch", r.mode = no;
            break;
          }
          l = 0, f = 0;
        }
        r.head && (r.head.hcrc = r.flags >> 9 & 1, r.head.done = !0), e.adler = r.check = 0, r.mode = Hi;
        break;
      case Oi:
        for (; f < 32;) {
          if (0 === s) break e;
          s--, l += n[o++] << f, f += 8;
        }
        e.adler = r.check = ho(l), l = 0, f = 0, r.mode = Ui;
      case Ui:
        if (0 === r.havedict) return e.next_out = a, e.avail_out = h, e.next_in = o, e.avail_in = s, r.hold = l, r.bits = f, ki;
        e.adler = r.check = 1, r.mode = Hi;
      case Hi:
        if (t === wi || t === bi) break e;
      case Fi:
        if (r.last) {
          l >>>= 7 & f, f -= 7 & f, r.mode = eo;
          break;
        }
        for (; f < 3;) {
          if (0 === s) break e;
          s--, l += n[o++] << f, f += 8;
        }
        switch (r.last = 1 & l, f -= 1, 3 & (l >>>= 1)) {
          case 0:
            r.mode = Ni;
            break;
          case 1:
            if (go(r), r.mode = Xi, t === bi) {
              l >>>= 2, f -= 2;
              break e;
            }
            break;
          case 2:
            r.mode = Wi;
            break;
          case 3:
            e.msg = "invalid block type", r.mode = no;
        }
        l >>>= 2, f -= 2;
        break;
      case Ni:
        for (l >>>= 7 & f, f -= 7 & f; f < 32;) {
          if (0 === s) break e;
          s--, l += n[o++] << f, f += 8;
        }
        if ((65535 & l) != (l >>> 16 ^ 65535)) {
          e.msg = "invalid stored block lengths", r.mode = no;
          break;
        }
        if (r.length = 65535 & l, l = 0, f = 0, r.mode = Zi, t === bi) break e;
      case Zi:
        r.mode = ji;
      case ji:
        if (d = r.length) {
          if (d > s && (d = s), d > h && (d = h), 0 === d) break e;
          jt(i, n, o, d, a), s -= d, o += d, h -= d, a += d, r.length -= d;
          break;
        }
        r.mode = Hi;
        break;
      case Wi:
        for (; f < 14;) {
          if (0 === s) break e;
          s--, l += n[o++] << f, f += 8;
        }
        if (r.nlen = 257 + (31 & l), l >>>= 5, f -= 5, r.ndist = 1 + (31 & l), l >>>= 5, f -= 5, r.ncode = 4 + (15 & l), l >>>= 4, f -= 4, r.nlen > 286 || r.ndist > 30) {
          e.msg = "too many length or distance symbols", r.mode = no;
          break;
        }
        r.have = 0, r.mode = Yi;
      case Yi:
        for (; r.have < r.ncode;) {
          for (; f < 3;) {
            if (0 === s) break e;
            s--, l += n[o++] << f, f += 8;
          }
          r.lens[B[r.have++]] = 7 & l, l >>>= 3, f -= 3;
        }
        for (; r.have < 19;) {
          r.lens[B[r.have++]] = 0;
        }
        if (r.lencode = r.lendyn, r.lenbits = 7, S = {
          bits: r.lenbits
        }, E = di(pi, r.lens, 0, 19, r.lencode, 0, r.work, S), r.lenbits = S.bits, E) {
          e.msg = "invalid code lengths set", r.mode = no;
          break;
        }
        r.have = 0, r.mode = Ki;
      case Ki:
        for (; r.have < r.nlen + r.ndist;) {
          for (; v = (R = r.lencode[l & (1 << r.lenbits) - 1]) >>> 16 & 255, w = 65535 & R, !((g = R >>> 24) <= f);) {
            if (0 === s) break e;
            s--, l += n[o++] << f, f += 8;
          }
          if (w < 16) l >>>= g, f -= g, r.lens[r.have++] = w;else {
            if (16 === w) {
              for (x = g + 2; f < x;) {
                if (0 === s) break e;
                s--, l += n[o++] << f, f += 8;
              }
              if (l >>>= g, f -= g, 0 === r.have) {
                e.msg = "invalid bit length repeat", r.mode = no;
                break;
              }
              k = r.lens[r.have - 1], d = 3 + (3 & l), l >>>= 2, f -= 2;
            } else if (17 === w) {
              for (x = g + 3; f < x;) {
                if (0 === s) break e;
                s--, l += n[o++] << f, f += 8;
              }
              f -= g, k = 0, d = 3 + (7 & (l >>>= g)), l >>>= 3, f -= 3;
            } else {
              for (x = g + 7; f < x;) {
                if (0 === s) break e;
                s--, l += n[o++] << f, f += 8;
              }
              f -= g, k = 0, d = 11 + (127 & (l >>>= g)), l >>>= 7, f -= 7;
            }
            if (r.have + d > r.nlen + r.ndist) {
              e.msg = "invalid bit length repeat", r.mode = no;
              break;
            }
            for (; d--;) {
              r.lens[r.have++] = k;
            }
          }
        }
        if (r.mode === no) break;
        if (0 === r.lens[256]) {
          e.msg = "invalid code -- missing end-of-block", r.mode = no;
          break;
        }
        if (r.lenbits = 9, S = {
          bits: r.lenbits
        }, E = di(_i, r.lens, 0, r.nlen, r.lencode, 0, r.work, S), r.lenbits = S.bits, E) {
          e.msg = "invalid literal/lengths set", r.mode = no;
          break;
        }
        if (r.distbits = 6, r.distcode = r.distdyn, S = {
          bits: r.distbits
        }, E = di(gi, r.lens, r.nlen, r.ndist, r.distcode, 0, r.work, S), r.distbits = S.bits, E) {
          e.msg = "invalid distances set", r.mode = no;
          break;
        }
        if (r.mode = Xi, t === bi) break e;
      case Xi:
        r.mode = qi;
      case qi:
        if (s >= 6 && h >= 258) {
          e.next_out = a, e.avail_out = h, e.next_in = o, e.avail_in = s, r.hold = l, r.bits = f, ri(e, u), a = e.next_out, i = e.output, h = e.avail_out, o = e.next_in, n = e.input, s = e.avail_in, l = r.hold, f = r.bits, r.mode === Hi && (r.back = -1);
          break;
        }
        for (r.back = 0; v = (R = r.lencode[l & (1 << r.lenbits) - 1]) >>> 16 & 255, w = 65535 & R, !((g = R >>> 24) <= f);) {
          if (0 === s) break e;
          s--, l += n[o++] << f, f += 8;
        }
        if (v && 0 == (240 & v)) {
          for (b = g, y = v, m = w; v = (R = r.lencode[m + ((l & (1 << b + y) - 1) >> b)]) >>> 16 & 255, w = 65535 & R, !(b + (g = R >>> 24) <= f);) {
            if (0 === s) break e;
            s--, l += n[o++] << f, f += 8;
          }
          l >>>= b, f -= b, r.back += b;
        }
        if (l >>>= g, f -= g, r.back += g, r.length = w, 0 === v) {
          r.mode = Qi;
          break;
        }
        if (32 & v) {
          r.back = -1, r.mode = Hi;
          break;
        }
        if (64 & v) {
          e.msg = "invalid literal/length code", r.mode = no;
          break;
        }
        r.extra = 15 & v, r.mode = Vi;
      case Vi:
        if (r.extra) {
          for (x = r.extra; f < x;) {
            if (0 === s) break e;
            s--, l += n[o++] << f, f += 8;
          }
          r.length += l & (1 << r.extra) - 1, l >>>= r.extra, f -= r.extra, r.back += r.extra;
        }
        r.was = r.length, r.mode = Gi;
      case Gi:
        for (; v = (R = r.distcode[l & (1 << r.distbits) - 1]) >>> 16 & 255, w = 65535 & R, !((g = R >>> 24) <= f);) {
          if (0 === s) break e;
          s--, l += n[o++] << f, f += 8;
        }
        if (0 == (240 & v)) {
          for (b = g, y = v, m = w; v = (R = r.distcode[m + ((l & (1 << b + y) - 1) >> b)]) >>> 16 & 255, w = 65535 & R, !(b + (g = R >>> 24) <= f);) {
            if (0 === s) break e;
            s--, l += n[o++] << f, f += 8;
          }
          l >>>= b, f -= b, r.back += b;
        }
        if (l >>>= g, f -= g, r.back += g, 64 & v) {
          e.msg = "invalid distance code", r.mode = no;
          break;
        }
        r.offset = w, r.extra = 15 & v, r.mode = $i;
      case $i:
        if (r.extra) {
          for (x = r.extra; f < x;) {
            if (0 === s) break e;
            s--, l += n[o++] << f, f += 8;
          }
          r.offset += l & (1 << r.extra) - 1, l >>>= r.extra, f -= r.extra, r.back += r.extra;
        }
        if (r.offset > r.dmax) {
          e.msg = "invalid distance too far back", r.mode = no;
          break;
        }
        r.mode = Ji;
      case Ji:
        if (0 === h) break e;
        if (d = u - h, r.offset > d) {
          if ((d = r.offset - d) > r.whave && r.sane) {
            e.msg = "invalid distance too far back", r.mode = no;
            break;
          }
          d > r.wnext ? (d -= r.wnext, p = r.wsize - d) : p = r.wnext - d, d > r.length && (d = r.length), _ = r.window;
        } else _ = i, p = a - r.offset, d = r.length;
        d > h && (d = h), h -= d, r.length -= d;
        do {
          i[a++] = _[p++];
        } while (--d);
        0 === r.length && (r.mode = qi);
        break;
      case Qi:
        if (0 === h) break e;
        i[a++] = r.length, h--, r.mode = qi;
        break;
      case eo:
        if (r.wrap) {
          for (; f < 32;) {
            if (0 === s) break e;
            s--, l |= n[o++] << f, f += 8;
          }
          if (u -= h, e.total_out += u, r.total += u, u && (e.adler = r.check = r.flags ? $r(r.check, i, u, a - u) : Vr(r.check, i, u, a - u)), u = h, (r.flags ? l : ho(l)) !== r.check) {
            e.msg = "incorrect data check", r.mode = no;
            break;
          }
          l = 0, f = 0;
        }
        r.mode = to;
      case to:
        if (r.wrap && r.flags) {
          for (; f < 32;) {
            if (0 === s) break e;
            s--, l += n[o++] << f, f += 8;
          }
          if (l !== (4294967295 & r.total)) {
            e.msg = "incorrect length check", r.mode = no;
            break;
          }
          l = 0, f = 0;
        }
        r.mode = ro;
      case ro:
        E = mi;
        break e;
      case no:
        E = Si;
        break e;
      case io:
        return xi;
      case oo:
      default:
        return Ei;
    }
  }
  return e.next_out = a, e.avail_out = h, e.next_in = o, e.avail_in = s, r.hold = l, r.bits = f, (r.wsize || u !== e.avail_out && r.mode < no && (r.mode < eo || t !== vi)) && function (e, t, r, n) {
    var i,
      o = e.state;
    null === o.window && (o.wsize = 1 << o.wbits, o.wnext = 0, o.whave = 0, o.window = new Wt(o.wsize)), n >= o.wsize ? (jt(o.window, t, r - o.wsize, o.wsize, 0), o.wnext = 0, o.whave = o.wsize) : ((i = o.wsize - o.wnext) > n && (i = n), jt(o.window, t, r - n, i, o.wnext), (n -= i) ? (jt(o.window, t, r - n, n, 0), o.wnext = n, o.whave = o.wsize) : (o.wnext += i, o.wnext === o.wsize && (o.wnext = 0), o.whave < o.wsize && (o.whave += i)));
  }(e, e.output, e.next_out, u - e.avail_out), c -= e.avail_in, u -= e.avail_out, e.total_in += c, e.total_out += u, r.total += u, r.wrap && u && (e.adler = r.check = r.flags ? $r(r.check, i, u, e.next_out - u) : Vr(r.check, i, u, e.next_out - u)), e.data_type = r.bits + (r.last ? 64 : 0) + (r.mode === Hi ? 128 : 0) + (r.mode === Xi || r.mode === Zi ? 256 : 0), (0 === c && 0 === u || t === vi) && E === yi && (E = Ri), E;
}
var wo,
  bo = 1,
  yo = 7;
function mo(e) {
  if (e < bo || e > yo) throw new TypeError("Bad argument");
  this.mode = e, this.init_done = !1, this.write_in_progress = !1, this.pending_close = !1, this.windowBits = 0, this.level = 0, this.memLevel = 0, this.strategy = 0, this.dictionary = null;
}
function ko(e, t) {
  for (var r = 0; r < e.length; r++) {
    this[t + r] = e[r];
  }
}
mo.prototype.init = function (e, t, r, n, i) {
  var o;
  switch (this.windowBits = e, this.level = t, this.memLevel = r, this.strategy = n, 3 !== this.mode && 4 !== this.mode || (this.windowBits += 16), this.mode === yo && (this.windowBits += 32), 5 !== this.mode && 6 !== this.mode || (this.windowBits = -this.windowBits), this.strm = new Zt(), this.mode) {
    case bo:
    case 3:
    case 5:
      o = function (e, t, r, n, i, o) {
        if (!e) return sn;
        var a = 1;
        if (t === fn && (t = 6), n < 0 ? (a = 0, n = -n) : n > 15 && (a = 2, n -= 16), i < 1 || i > vn || r !== gn || n < 8 || n > 15 || t < 0 || t > 9 || o < 0 || o > pn) return Hn(e, sn);
        8 === n && (n = 9);
        var s = new $n();
        return e.state = s, s.strm = e, s.wrap = a, s.gzhead = null, s.w_bits = n, s.w_size = 1 << s.w_bits, s.w_mask = s.w_size - 1, s.hash_bits = i + 7, s.hash_size = 1 << s.hash_bits, s.hash_mask = s.hash_size - 1, s.hash_shift = ~~((s.hash_bits + En - 1) / En), s.window = new Wt(2 * s.w_size), s.head = new Yt(s.hash_size), s.prev = new Yt(s.w_size), s.lit_bufsize = 1 << i + 6, s.pending_buf_size = 4 * s.lit_bufsize, s.pending_buf = new Wt(s.pending_buf_size), s.d_buf = 1 * s.lit_bufsize, s.l_buf = 3 * s.lit_bufsize, s.level = t, s.strategy = o, s.method = r, Jn(e);
      }(this.strm, this.level, 8, this.windowBits, this.memLevel, this.strategy);
      break;
    case 2:
    case 4:
    case 6:
    case yo:
      o = co(this.strm, this.windowBits);
      break;
    default:
      throw new Error("Unknown mode " + this.mode);
  }
  0 === o ? (this.write_in_progress = !1, this.init_done = !0) : this._error(o);
}, mo.prototype.params = function () {
  throw new Error("deflateParams Not supported");
}, mo.prototype._writeCheck = function () {
  if (!this.init_done) throw new Error("write before init");
  if (0 === this.mode) throw new Error("already finalized");
  if (this.write_in_progress) throw new Error("write already in progress");
  if (this.pending_close) throw new Error("close is pending");
}, mo.prototype.write = function (e, t, r, n, i, o, a) {
  this._writeCheck(), this.write_in_progress = !0;
  var s = this;
  return de(function () {
    s.write_in_progress = !1;
    var h = s._write(e, t, r, n, i, o, a);
    s.callback(h[0], h[1]), s.pending_close && s.close();
  }), this;
}, mo.prototype.writeSync = function (e, t, r, n, i, o, a) {
  return this._writeCheck(), this._write(e, t, r, n, i, o, a);
}, mo.prototype._write = function (e, t, r, n, i, o, a) {
  if (this.write_in_progress = !0, 0 !== e && 1 !== e && 2 !== e && 3 !== e && 4 !== e && 5 !== e) throw new Error("Invalid flush value");
  null == t && (t = new p(0), n = 0, r = 0), i._set ? i.set = i._set : i.set = ko;
  var s,
    h = this.strm;
  switch (h.avail_in = n, h.input = t, h.next_in = r, h.avail_out = a, h.output = i, h.next_out = o, this.mode) {
    case bo:
    case 3:
    case 5:
      s = Qn(h, e);
      break;
    case yo:
    case 2:
    case 4:
    case 6:
      s = vo(h, e);
      break;
    default:
      throw new Error("Unknown mode " + this.mode);
  }
  return 1 !== s && 0 !== s && this._error(s), this.write_in_progress = !1, [h.avail_in, h.avail_out];
}, mo.prototype.close = function () {
  this.write_in_progress ? this.pending_close = !0 : (this.pending_close = !1, this.mode === bo || 3 === this.mode || 5 === this.mode ? function (e) {
    var t;
    e && e.state && ((t = e.state.status) !== An && t !== Bn && t !== zn && t !== Ln && t !== Tn && t !== Mn && t !== Cn ? Hn(e, sn) : (e.state = null, t === Mn && Hn(e, hn)));
  }(this.strm) : function (e) {
    if (!e || !e.state) return Ei;
    var t = e.state;
    t.window && (t.window = null), e.state = null;
  }(this.strm), this.mode = 0);
}, mo.prototype.reset = function () {
  switch (this.mode) {
    case bo:
    case 5:
      wo = Jn(this.strm);
      break;
    case 2:
    case 6:
      wo = fo(this.strm);
  }
  0 !== wo && this._error(wo);
}, mo.prototype._error = function (e) {
  this.onerror(Nt[e] + ": " + this.strm.msg, e), this.write_in_progress = !1, this.pending_close && this.close();
};
var Eo = Object.freeze({
  NONE: 0,
  DEFLATE: bo,
  INFLATE: 2,
  GZIP: 3,
  GUNZIP: 4,
  DEFLATERAW: 5,
  INFLATERAW: 6,
  UNZIP: yo,
  Z_NO_FLUSH: 0,
  Z_PARTIAL_FLUSH: 1,
  Z_SYNC_FLUSH: 2,
  Z_FULL_FLUSH: 3,
  Z_FINISH: 4,
  Z_BLOCK: 5,
  Z_TREES: 6,
  Z_OK: 0,
  Z_STREAM_END: 1,
  Z_NEED_DICT: 2,
  Z_ERRNO: -1,
  Z_STREAM_ERROR: -2,
  Z_DATA_ERROR: -3,
  Z_BUF_ERROR: -5,
  Z_NO_COMPRESSION: 0,
  Z_BEST_SPEED: 1,
  Z_BEST_COMPRESSION: 9,
  Z_DEFAULT_COMPRESSION: -1,
  Z_FILTERED: 1,
  Z_HUFFMAN_ONLY: 2,
  Z_RLE: 3,
  Z_FIXED: 4,
  Z_DEFAULT_STRATEGY: 0,
  Z_BINARY: 0,
  Z_TEXT: 1,
  Z_UNKNOWN: 2,
  Z_DEFLATED: 8,
  Zlib: mo
});
var So = {};
Object.keys(Eo).forEach(function (e) {
  So[e] = Eo[e];
}), So.Z_MIN_WINDOWBITS = 8, So.Z_MAX_WINDOWBITS = 15, So.Z_DEFAULT_WINDOWBITS = 15, So.Z_MIN_CHUNK = 64, So.Z_MAX_CHUNK = 1 / 0, So.Z_DEFAULT_CHUNK = 16384, So.Z_MIN_MEMLEVEL = 1, So.Z_MAX_MEMLEVEL = 9, So.Z_DEFAULT_MEMLEVEL = 8, So.Z_MIN_LEVEL = -1, So.Z_MAX_LEVEL = 9, So.Z_DEFAULT_LEVEL = So.Z_DEFAULT_COMPRESSION;
var xo = {
  Z_OK: So.Z_OK,
  Z_STREAM_END: So.Z_STREAM_END,
  Z_NEED_DICT: So.Z_NEED_DICT,
  Z_ERRNO: So.Z_ERRNO,
  Z_STREAM_ERROR: So.Z_STREAM_ERROR,
  Z_DATA_ERROR: So.Z_DATA_ERROR,
  Z_MEM_ERROR: So.Z_MEM_ERROR,
  Z_BUF_ERROR: So.Z_BUF_ERROR,
  Z_VERSION_ERROR: So.Z_VERSION_ERROR
};
function Ro(e, t, r) {
  var n = [],
    i = 0;
  function o() {
    for (var t; null !== (t = e.read());) {
      n.push(t), i += t.length;
    }
    e.once("readable", o);
  }
  function a() {
    var t = p.concat(n, i);
    n = [], r(null, t), e.close();
  }
  e.on("error", function (t) {
    e.removeListener("end", a), e.removeListener("readable", o), r(t);
  }), e.on("end", a), e.end(t), o();
}
function Ao(e, t) {
  if ("string" == typeof t && (t = new p(t)), !$(t)) throw new TypeError("Not a string or buffer");
  var r = So.Z_FINISH;
  return e._processChunk(t, r);
}
function Bo(e) {
  if (!(this instanceof Bo)) return new Bo(e);
  Io.call(this, e, So.DEFLATE);
}
function zo(e) {
  if (!(this instanceof zo)) return new zo(e);
  Io.call(this, e, So.INFLATE);
}
function Lo(e) {
  if (!(this instanceof Lo)) return new Lo(e);
  Io.call(this, e, So.GZIP);
}
function To(e) {
  if (!(this instanceof To)) return new To(e);
  Io.call(this, e, So.GUNZIP);
}
function Mo(e) {
  if (!(this instanceof Mo)) return new Mo(e);
  Io.call(this, e, So.DEFLATERAW);
}
function Co(e) {
  if (!(this instanceof Co)) return new Co(e);
  Io.call(this, e, So.INFLATERAW);
}
function Do(e) {
  if (!(this instanceof Do)) return new Do(e);
  Io.call(this, e, So.UNZIP);
}
function Io(e, t) {
  if (this._opts = e = e || {}, this._chunkSize = e.chunkSize || So.Z_DEFAULT_CHUNK, Ot.call(this, e), e.flush && e.flush !== So.Z_NO_FLUSH && e.flush !== So.Z_PARTIAL_FLUSH && e.flush !== So.Z_SYNC_FLUSH && e.flush !== So.Z_FULL_FLUSH && e.flush !== So.Z_FINISH && e.flush !== So.Z_BLOCK) throw new Error("Invalid flush flag: " + e.flush);
  if (this._flushFlag = e.flush || So.Z_NO_FLUSH, e.chunkSize && (e.chunkSize < So.Z_MIN_CHUNK || e.chunkSize > So.Z_MAX_CHUNK)) throw new Error("Invalid chunk size: " + e.chunkSize);
  if (e.windowBits && (e.windowBits < So.Z_MIN_WINDOWBITS || e.windowBits > So.Z_MAX_WINDOWBITS)) throw new Error("Invalid windowBits: " + e.windowBits);
  if (e.level && (e.level < So.Z_MIN_LEVEL || e.level > So.Z_MAX_LEVEL)) throw new Error("Invalid compression level: " + e.level);
  if (e.memLevel && (e.memLevel < So.Z_MIN_MEMLEVEL || e.memLevel > So.Z_MAX_MEMLEVEL)) throw new Error("Invalid memLevel: " + e.memLevel);
  if (e.strategy && e.strategy != So.Z_FILTERED && e.strategy != So.Z_HUFFMAN_ONLY && e.strategy != So.Z_RLE && e.strategy != So.Z_FIXED && e.strategy != So.Z_DEFAULT_STRATEGY) throw new Error("Invalid strategy: " + e.strategy);
  if (e.dictionary && !$(e.dictionary)) throw new Error("Invalid dictionary: it should be a Buffer instance");
  this._binding = new So.Zlib(t);
  var r = this;
  this._hadError = !1, this._binding.onerror = function (e, t) {
    r._binding = null, r._hadError = !0;
    var n = new Error(e);
    n.errno = t, n.code = So.codes[t], r.emit("error", n);
  };
  var n = So.Z_DEFAULT_COMPRESSION;
  "number" == typeof e.level && (n = e.level);
  var i = So.Z_DEFAULT_STRATEGY;
  "number" == typeof e.strategy && (i = e.strategy), this._binding.init(e.windowBits || So.Z_DEFAULT_WINDOWBITS, n, e.memLevel || So.Z_DEFAULT_MEMLEVEL, i, e.dictionary), this._buffer = new p(this._chunkSize), this._offset = 0, this._closed = !1, this._level = n, this._strategy = i, this.once("end", this.close);
}
Object.keys(xo).forEach(function (e) {
  xo[xo[e]] = e;
}), Be(Io, Ot), Io.prototype.params = function (e, t, r) {
  if (e < So.Z_MIN_LEVEL || e > So.Z_MAX_LEVEL) throw new RangeError("Invalid compression level: " + e);
  if (t != So.Z_FILTERED && t != So.Z_HUFFMAN_ONLY && t != So.Z_RLE && t != So.Z_FIXED && t != So.Z_DEFAULT_STRATEGY) throw new TypeError("Invalid strategy: " + t);
  if (this._level !== e || this._strategy !== t) {
    var n = this;
    this.flush(So.Z_SYNC_FLUSH, function () {
      n._binding.params(e, t), n._hadError || (n._level = e, n._strategy = t, r && r());
    });
  } else de(r);
}, Io.prototype.reset = function () {
  return this._binding.reset();
}, Io.prototype._flush = function (e) {
  this._transform(new p(0), "", e);
}, Io.prototype.flush = function (e, t) {
  var r = this._writableState;
  if (("function" == typeof e || void 0 === e && !t) && (t = e, e = So.Z_FULL_FLUSH), r.ended) t && de(t);else if (r.ending) t && this.once("end", t);else if (r.needDrain) {
    var n = this;
    this.once("drain", function () {
      n.flush(t);
    });
  } else this._flushFlag = e, this.write(new p(0), "", t);
}, Io.prototype.close = function (e) {
  if (e && de(e), !this._closed) {
    this._closed = !0, this._binding.close();
    var t = this;
    de(function () {
      t.emit("close");
    });
  }
}, Io.prototype._transform = function (e, t, r) {
  var n,
    i = this._writableState,
    o = (i.ending || i.ended) && (!e || i.length === e.length);
  if (null === !e && !$(e)) return r(new Error("invalid input"));
  o ? n = So.Z_FINISH : (n = this._flushFlag, e.length >= i.length && (this._flushFlag = this._opts.flush || So.Z_NO_FLUSH)), this._processChunk(e, n, r);
}, Io.prototype._processChunk = function (e, t, r) {
  var n = e && e.length,
    i = this._chunkSize - this._offset,
    o = 0,
    a = this,
    s = "function" == typeof r;
  if (!s) {
    var h,
      l = [],
      f = 0;
    this.on("error", function (e) {
      h = e;
    });
    do {
      var c = this._binding.writeSync(t, e, o, n, this._buffer, this._offset, i);
    } while (!this._hadError && _(c[0], c[1]));
    if (this._hadError) throw h;
    var u = p.concat(l, f);
    return this.close(), u;
  }
  var d = this._binding.write(t, e, o, n, this._buffer, this._offset, i);
  function _(h, c) {
    if (!a._hadError) {
      var u = i - c;
      if (function (e, t) {
        if (!e) throw new Error(t);
      }(u >= 0, "have should not go down"), u > 0) {
        var d = a._buffer.slice(a._offset, a._offset + u);
        a._offset += u, s ? a.push(d) : (l.push(d), f += d.length);
      }
      if ((0 === c || a._offset >= a._chunkSize) && (i = a._chunkSize, a._offset = 0, a._buffer = new p(a._chunkSize)), 0 === c) {
        if (o += n - h, n = h, !s) return !0;
        var g = a._binding.write(t, e, o, n, a._buffer, a._offset, a._chunkSize);
        return g.callback = _, void (g.buffer = e);
      }
      if (!s) return !1;
      r();
    }
  }
  d.buffer = e, d.callback = _;
}, Be(Bo, Io), Be(zo, Io), Be(Lo, Io), Be(To, Io), Be(Mo, Io), Be(Co, Io), Be(Do, Io);
var Po = {
  codes: xo,
  createDeflate: function createDeflate(e) {
    return new Bo(e);
  },
  createInflate: function createInflate(e) {
    return new zo(e);
  },
  createDeflateRaw: function createDeflateRaw(e) {
    return new Mo(e);
  },
  createInflateRaw: function createInflateRaw(e) {
    return new Co(e);
  },
  createGzip: function createGzip(e) {
    return new Lo(e);
  },
  createGunzip: function createGunzip(e) {
    return new To(e);
  },
  createUnzip: function createUnzip(e) {
    return new Do(e);
  },
  deflate: function deflate(e, t, r) {
    return "function" == typeof t && (r = t, t = {}), Ro(new Bo(t), e, r);
  },
  deflateSync: function deflateSync(e, t) {
    return Ao(new Bo(t), e);
  },
  gzip: function gzip(e, t, r) {
    return "function" == typeof t && (r = t, t = {}), Ro(new Lo(t), e, r);
  },
  gzipSync: function gzipSync(e, t) {
    return Ao(new Lo(t), e);
  },
  deflateRaw: function deflateRaw(e, t, r) {
    return "function" == typeof t && (r = t, t = {}), Ro(new Mo(t), e, r);
  },
  deflateRawSync: function deflateRawSync(e, t) {
    return Ao(new Mo(t), e);
  },
  unzip: function unzip(e, t, r) {
    return "function" == typeof t && (r = t, t = {}), Ro(new Do(t), e, r);
  },
  unzipSync: function unzipSync(e, t) {
    return Ao(new Do(t), e);
  },
  inflate: function inflate(e, t, r) {
    return "function" == typeof t && (r = t, t = {}), Ro(new zo(t), e, r);
  },
  inflateSync: function inflateSync(e, t) {
    return Ao(new zo(t), e);
  },
  gunzip: function gunzip(e, t, r) {
    return "function" == typeof t && (r = t, t = {}), Ro(new To(t), e, r);
  },
  gunzipSync: function gunzipSync(e, t) {
    return Ao(new To(t), e);
  },
  inflateRaw: function inflateRaw(e, t, r) {
    return "function" == typeof t && (r = t, t = {}), Ro(new Co(t), e, r);
  },
  inflateRawSync: function inflateRawSync(e, t) {
    return Ao(new Co(t), e);
  },
  Deflate: Bo,
  Inflate: zo,
  Gzip: Lo,
  Gunzip: To,
  DeflateRaw: Mo,
  InflateRaw: Co,
  Unzip: Do,
  Zlib: Io
};
var _default = /*#__PURE__*/function () {
  function _default(e, t, r) {
    (0, _classCallCheck2.default)(this, _default);
    this.SDKAPPID = e, this.EXPIRETIME = r, this.PRIVATEKEY = t;
  }
  (0, _createClass2.default)(_default, [{
    key: "genTestUserSig",
    value: function genTestUserSig(e) {
      return this._isNumber(this.SDKAPPID) ? this._isString(this.PRIVATEKEY) ? this._isString(e) ? this._isNumber(this.EXPIRETIME) ? (console.log("sdkAppID=" + this.SDKAPPID + " key=" + this.PRIVATEKEY + " userID=" + e + " expire=" + this.EXPIRETIME), this.genSigWithUserbuf(e, this.EXPIRETIME, null)) : (console.error("expireTime must be a number"), "") : (console.error("userID must be a string"), "") : (console.error("privateKey must be a string"), "") : (console.error("sdkAppID must be a number"), "");
    }
  }, {
    key: "newBuffer",
    value: function newBuffer(e, t) {
      return p.from ? p.from(e, t) : new p(e, t);
    }
  }, {
    key: "unescape",
    value: function unescape(e) {
      return e.replace(/_/g, "=").replace(/\-/g, "/").replace(/\*/g, "+");
    }
  }, {
    key: "escape",
    value: function escape(e) {
      return e.replace(/\+/g, "*").replace(/\//g, "-").replace(/=/g, "_");
    }
  }, {
    key: "encode",
    value: function encode(e) {
      return this.escape(this.newBuffer(e).toString("base64"));
    }
  }, {
    key: "decode",
    value: function decode(e) {
      return this.newBuffer(this.unescape(e), "base64");
    }
  }, {
    key: "base64encode",
    value: function base64encode(e) {
      return this.newBuffer(e).toString("base64");
    }
  }, {
    key: "base64decode",
    value: function base64decode(e) {
      return this.newBuffer(e, "base64").toString();
    }
  }, {
    key: "_hmacsha256",
    value: function _hmacsha256(e, t, r, n) {
      var i = "TLS.identifier:" + e + "\n";
      i += "TLS.sdkappid:" + this.SDKAPPID + "\n", i += "TLS.time:" + t + "\n", i += "TLS.expire:" + r + "\n", null != n && (i += "TLS.userbuf:" + n + "\n");
      var o = te.HmacSHA256(i, this.PRIVATEKEY);
      return te.enc.Base64.stringify(o);
    }
  }, {
    key: "_utc",
    value: function _utc() {
      return Math.round(Date.now() / 1e3);
    }
  }, {
    key: "_isNumber",
    value: function _isNumber(e) {
      return null !== e && ("number" == typeof e && !isNaN(e - 0) || "object" == (0, _typeof2.default)(e) && e.constructor === Number);
    }
  }, {
    key: "_isString",
    value: function _isString(e) {
      return "string" == typeof e;
    }
  }, {
    key: "genSigWithUserbuf",
    value: function genSigWithUserbuf(e, t, r) {
      var n = this._utc(),
        i = {
          "TLS.ver": "2.0",
          "TLS.identifier": e,
          "TLS.sdkappid": this.SDKAPPID,
          "TLS.time": n,
          "TLS.expire": t
        },
        o = "";
      if (null != r) {
        var _a = this.base64encode(r);
        i["TLS.userbuf"] = _a, o = this._hmacsha256(e, n, t, _a);
      } else o = this._hmacsha256(e, n, t, null);
      i["TLS.sig"] = o;
      var a = JSON.stringify(i),
        s = Po.deflateSync(this.newBuffer(a)).toString("base64"),
        h = this.escape(s);
      return console.log("ret=" + h), h;
    }
  }, {
    key: "validate",
    value: function validate(e) {
      var t = this.decode(e),
        r = Po.inflateSync(t);
      console.log("validate ret=" + r);
    }
  }]);
  return _default;
}();
exports.default = _default;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! (webpack)/buildin/global.js */ 3)))

/***/ })
]]);
//# sourceMappingURL=../../.sourcemap/mp-weixin/common/vendor.js.map