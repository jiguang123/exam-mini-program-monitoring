import { resolveComponent, openBlock, createBlock, createElementBlock, createElementVNode, createVNode, withCtx, createTextVNode, toDisplayString } from "vue";
function requireNativePlugin(name) {
  return weex.requireModule(name);
}
function formatAppLog(type, filename, ...args) {
  if (uni.__log__) {
    uni.__log__(type, filename, ...args);
  } else {
    console[type].apply(console, [...args, filename]);
  }
}
var isIos;
isIos = plus.os.name == "iOS";
function judgeIosPermissionPush() {
  var result = false;
  var UIApplication = plus.ios.import("UIApplication");
  var app = UIApplication.sharedApplication();
  var enabledTypes = 0;
  if (app.currentUserNotificationSettings) {
    var settings = app.currentUserNotificationSettings();
    enabledTypes = settings.plusGetAttribute("types");
    formatAppLog("log", "at TrtcCloud/permission.js:18", "enabledTypes1:" + enabledTypes);
    if (enabledTypes == 0) {
      formatAppLog("log", "at TrtcCloud/permission.js:20", "推送权限没有开启");
    } else {
      result = true;
      formatAppLog("log", "at TrtcCloud/permission.js:23", "已经开启推送功能!");
    }
    plus.ios.deleteObject(settings);
  } else {
    enabledTypes = app.enabledRemoteNotificationTypes();
    if (enabledTypes == 0) {
      formatAppLog("log", "at TrtcCloud/permission.js:29", "推送权限没有开启!");
    } else {
      result = true;
      formatAppLog("log", "at TrtcCloud/permission.js:32", "已经开启推送功能!");
    }
    formatAppLog("log", "at TrtcCloud/permission.js:34", "enabledTypes2:" + enabledTypes);
  }
  plus.ios.deleteObject(app);
  plus.ios.deleteObject(UIApplication);
  return result;
}
function judgeIosPermissionLocation() {
  var result = false;
  var cllocationManger = plus.ios.import("CLLocationManager");
  var status = cllocationManger.authorizationStatus();
  result = status != 2;
  formatAppLog("log", "at TrtcCloud/permission.js:47", "定位权限开启：" + result);
  plus.ios.deleteObject(cllocationManger);
  return result;
}
function judgeIosPermissionRecord() {
  var result = false;
  var avaudiosession = plus.ios.import("AVAudioSession");
  var avaudio = avaudiosession.sharedInstance();
  var permissionStatus = avaudio.recordPermission();
  formatAppLog("log", "at TrtcCloud/permission.js:68", "permissionStatus:" + permissionStatus);
  if (permissionStatus == 1684369017 || permissionStatus == 1970168948) {
    formatAppLog("log", "at TrtcCloud/permission.js:70", "麦克风权限没有开启");
  } else {
    result = true;
    formatAppLog("log", "at TrtcCloud/permission.js:73", "麦克风权限已经开启");
  }
  plus.ios.deleteObject(avaudiosession);
  return result;
}
function judgeIosPermissionCamera() {
  var result = false;
  var AVCaptureDevice = plus.ios.import("AVCaptureDevice");
  var authStatus = AVCaptureDevice.authorizationStatusForMediaType("vide");
  formatAppLog("log", "at TrtcCloud/permission.js:84", "authStatus:" + authStatus);
  if (authStatus == 3) {
    result = true;
    formatAppLog("log", "at TrtcCloud/permission.js:87", "相机权限已经开启");
  } else {
    formatAppLog("log", "at TrtcCloud/permission.js:89", "相机权限没有开启");
  }
  plus.ios.deleteObject(AVCaptureDevice);
  return result;
}
function judgeIosPermissionPhotoLibrary() {
  var result = false;
  var PHPhotoLibrary = plus.ios.import("PHPhotoLibrary");
  var authStatus = PHPhotoLibrary.authorizationStatus();
  formatAppLog("log", "at TrtcCloud/permission.js:100", "authStatus:" + authStatus);
  if (authStatus == 3) {
    result = true;
    formatAppLog("log", "at TrtcCloud/permission.js:103", "相册权限已经开启");
  } else {
    formatAppLog("log", "at TrtcCloud/permission.js:105", "相册权限没有开启");
  }
  plus.ios.deleteObject(PHPhotoLibrary);
  return result;
}
function judgeIosPermissionContact() {
  var result = false;
  var CNContactStore = plus.ios.import("CNContactStore");
  var cnAuthStatus = CNContactStore.authorizationStatusForEntityType(0);
  if (cnAuthStatus == 3) {
    result = true;
    formatAppLog("log", "at TrtcCloud/permission.js:118", "通讯录权限已经开启");
  } else {
    formatAppLog("log", "at TrtcCloud/permission.js:120", "通讯录权限没有开启");
  }
  plus.ios.deleteObject(CNContactStore);
  return result;
}
function judgeIosPermissionCalendar() {
  var result = false;
  var EKEventStore = plus.ios.import("EKEventStore");
  var ekAuthStatus = EKEventStore.authorizationStatusForEntityType(0);
  if (ekAuthStatus == 3) {
    result = true;
    formatAppLog("log", "at TrtcCloud/permission.js:133", "日历权限已经开启");
  } else {
    formatAppLog("log", "at TrtcCloud/permission.js:135", "日历权限没有开启");
  }
  plus.ios.deleteObject(EKEventStore);
  return result;
}
function judgeIosPermissionMemo() {
  var result = false;
  var EKEventStore = plus.ios.import("EKEventStore");
  var ekAuthStatus = EKEventStore.authorizationStatusForEntityType(1);
  if (ekAuthStatus == 3) {
    result = true;
    formatAppLog("log", "at TrtcCloud/permission.js:148", "备忘录权限已经开启");
  } else {
    formatAppLog("log", "at TrtcCloud/permission.js:150", "备忘录权限没有开启");
  }
  plus.ios.deleteObject(EKEventStore);
  return result;
}
function requestAndroidPermission(permissionID) {
  return new Promise((resolve, reject) => {
    plus.android.requestPermissions(
      [permissionID],
      // 理论上支持多个权限同时查询，但实际上本函数封装只处理了一个权限的情况。有需要的可自行扩展封装
      function(resultObj) {
        var result = 0;
        for (var i2 = 0; i2 < resultObj.granted.length; i2++) {
          var grantedPermission = resultObj.granted[i2];
          formatAppLog("log", "at TrtcCloud/permission.js:165", "已获取的权限：" + grantedPermission);
          result = 1;
        }
        for (var i2 = 0; i2 < resultObj.deniedPresent.length; i2++) {
          var deniedPresentPermission = resultObj.deniedPresent[i2];
          formatAppLog("log", "at TrtcCloud/permission.js:170", "拒绝本次申请的权限：" + deniedPresentPermission);
          result = 0;
        }
        for (var i2 = 0; i2 < resultObj.deniedAlways.length; i2++) {
          var deniedAlwaysPermission = resultObj.deniedAlways[i2];
          formatAppLog("log", "at TrtcCloud/permission.js:175", "永久拒绝申请的权限：" + deniedAlwaysPermission);
          result = -1;
        }
        resolve(result);
      },
      function(error) {
        formatAppLog("log", "at TrtcCloud/permission.js:185", "申请权限错误：" + error.code + " = " + error.message);
        resolve({
          code: error.code,
          message: error.message
        });
      }
    );
  });
}
function judgeIosPermission(permissionID) {
  if (permissionID == "location") {
    return judgeIosPermissionLocation();
  } else if (permissionID == "camera") {
    return judgeIosPermissionCamera();
  } else if (permissionID == "photoLibrary") {
    return judgeIosPermissionPhotoLibrary();
  } else if (permissionID == "record") {
    return judgeIosPermissionRecord();
  } else if (permissionID == "push") {
    return judgeIosPermissionPush();
  } else if (permissionID == "contact") {
    return judgeIosPermissionContact();
  } else if (permissionID == "calendar") {
    return judgeIosPermissionCalendar();
  } else if (permissionID == "memo") {
    return judgeIosPermissionMemo();
  }
  return false;
}
function gotoAppPermissionSetting() {
  if (isIos) {
    var UIApplication = plus.ios.import("UIApplication");
    var application2 = UIApplication.sharedApplication();
    var NSURL2 = plus.ios.import("NSURL");
    var setting2 = NSURL2.URLWithString("app-settings:");
    application2.openURL(setting2);
    plus.ios.deleteObject(setting2);
    plus.ios.deleteObject(NSURL2);
    plus.ios.deleteObject(application2);
  } else {
    var Intent = plus.android.importClass("android.content.Intent");
    var Settings = plus.android.importClass("android.provider.Settings");
    var Uri = plus.android.importClass("android.net.Uri");
    var mainActivity = plus.android.runtimeMainActivity();
    var intent = new Intent();
    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
    var uri = Uri.fromParts("package", mainActivity.getPackageName(), null);
    intent.setData(uri);
    mainActivity.startActivity(intent);
  }
}
function checkSystemEnableLocation() {
  if (isIos) {
    var result = false;
    var cllocationManger = plus.ios.import("CLLocationManager");
    var result = cllocationManger.locationServicesEnabled();
    formatAppLog("log", "at TrtcCloud/permission.js:251", "系统定位开启:" + result);
    plus.ios.deleteObject(cllocationManger);
    return result;
  } else {
    var context = plus.android.importClass("android.content.Context");
    var locationManager = plus.android.importClass("android.location.LocationManager");
    var main = plus.android.runtimeMainActivity();
    var mainSvr = main.getSystemService(context.LOCATION_SERVICE);
    var result = mainSvr.isProviderEnabled(locationManager.GPS_PROVIDER);
    formatAppLog("log", "at TrtcCloud/permission.js:260", "系统定位开启:" + result);
    return result;
  }
}
const permision = {
  judgeIosPermission,
  requestAndroidPermission,
  checkSystemEnableLocation,
  gotoAppPermissionSetting
};
const NAME = {
  ANDROID: "android",
  IOS: "ios",
  STRING: "string",
  FUNCTION: "function",
  BOOLEAN: "boolean",
  NUMBER: "number",
  LOG_PREFIX: "【UniApp-JS】"
};
const errorCodeUrl = "https://web.sdk.qcloud.com/trtc/uniapp/doc/zh-cn/ErrorCode.html";
var TRTCVideoResolution;
(function(TRTCVideoResolution2) {
  TRTCVideoResolution2[TRTCVideoResolution2["TRTCVideoResolution_120_120"] = 1] = "TRTCVideoResolution_120_120";
  TRTCVideoResolution2[TRTCVideoResolution2["TRTCVideoResolution_160_160"] = 3] = "TRTCVideoResolution_160_160";
  TRTCVideoResolution2[TRTCVideoResolution2["TRTCVideoResolution_270_270"] = 5] = "TRTCVideoResolution_270_270";
  TRTCVideoResolution2[TRTCVideoResolution2["TRTCVideoResolution_480_480"] = 7] = "TRTCVideoResolution_480_480";
  TRTCVideoResolution2[TRTCVideoResolution2["TRTCVideoResolution_160_120"] = 50] = "TRTCVideoResolution_160_120";
  TRTCVideoResolution2[TRTCVideoResolution2["TRTCVideoResolution_240_180"] = 52] = "TRTCVideoResolution_240_180";
  TRTCVideoResolution2[TRTCVideoResolution2["TRTCVideoResolution_280_210"] = 54] = "TRTCVideoResolution_280_210";
  TRTCVideoResolution2[TRTCVideoResolution2["TRTCVideoResolution_320_240"] = 56] = "TRTCVideoResolution_320_240";
  TRTCVideoResolution2[TRTCVideoResolution2["TRTCVideoResolution_400_300"] = 58] = "TRTCVideoResolution_400_300";
  TRTCVideoResolution2[TRTCVideoResolution2["TRTCVideoResolution_480_360"] = 60] = "TRTCVideoResolution_480_360";
  TRTCVideoResolution2[TRTCVideoResolution2["TRTCVideoResolution_640_480"] = 62] = "TRTCVideoResolution_640_480";
  TRTCVideoResolution2[TRTCVideoResolution2["TRTCVideoResolution_960_720"] = 64] = "TRTCVideoResolution_960_720";
  TRTCVideoResolution2[TRTCVideoResolution2["TRTCVideoResolution_160_90"] = 100] = "TRTCVideoResolution_160_90";
  TRTCVideoResolution2[TRTCVideoResolution2["TRTCVideoResolution_256_144"] = 102] = "TRTCVideoResolution_256_144";
  TRTCVideoResolution2[TRTCVideoResolution2["TRTCVideoResolution_320_180"] = 104] = "TRTCVideoResolution_320_180";
  TRTCVideoResolution2[TRTCVideoResolution2["TRTCVideoResolution_480_270"] = 106] = "TRTCVideoResolution_480_270";
  TRTCVideoResolution2[TRTCVideoResolution2["TRTCVideoResolution_640_360"] = 108] = "TRTCVideoResolution_640_360";
  TRTCVideoResolution2[TRTCVideoResolution2["TRTCVideoResolution_960_540"] = 110] = "TRTCVideoResolution_960_540";
  TRTCVideoResolution2[TRTCVideoResolution2["TRTCVideoResolution_1280_720"] = 112] = "TRTCVideoResolution_1280_720";
  TRTCVideoResolution2[TRTCVideoResolution2["TRTCVideoResolution_1920_1080"] = 114] = "TRTCVideoResolution_1920_1080";
})(TRTCVideoResolution || (TRTCVideoResolution = {}));
var TRTCVideoResolutionMode;
(function(TRTCVideoResolutionMode2) {
  TRTCVideoResolutionMode2[TRTCVideoResolutionMode2["TRTCVideoResolutionModeLandscape"] = 0] = "TRTCVideoResolutionModeLandscape";
  TRTCVideoResolutionMode2[TRTCVideoResolutionMode2["TRTCVideoResolutionModePortrait"] = 1] = "TRTCVideoResolutionModePortrait";
})(TRTCVideoResolutionMode || (TRTCVideoResolutionMode = {}));
var TRTCVideoStreamType;
(function(TRTCVideoStreamType2) {
  TRTCVideoStreamType2[TRTCVideoStreamType2["TRTCVideoStreamTypeBig"] = 0] = "TRTCVideoStreamTypeBig";
  TRTCVideoStreamType2[TRTCVideoStreamType2["TRTCVideoStreamTypeSmall"] = 1] = "TRTCVideoStreamTypeSmall";
  TRTCVideoStreamType2[TRTCVideoStreamType2["TRTCVideoStreamTypeSub"] = 2] = "TRTCVideoStreamTypeSub";
})(TRTCVideoStreamType || (TRTCVideoStreamType = {}));
var TRTCVideoFillMode;
(function(TRTCVideoFillMode2) {
  TRTCVideoFillMode2[TRTCVideoFillMode2["TRTCVideoFillMode_Fill"] = 0] = "TRTCVideoFillMode_Fill";
  TRTCVideoFillMode2[TRTCVideoFillMode2["TRTCVideoFillMode_Fit"] = 1] = "TRTCVideoFillMode_Fit";
})(TRTCVideoFillMode || (TRTCVideoFillMode = {}));
var TRTCVideoRotation;
(function(TRTCVideoRotation2) {
  TRTCVideoRotation2[TRTCVideoRotation2["TRTCVideoRotation_0"] = 0] = "TRTCVideoRotation_0";
  TRTCVideoRotation2[TRTCVideoRotation2["TRTCVideoRotation_90"] = 1] = "TRTCVideoRotation_90";
  TRTCVideoRotation2[TRTCVideoRotation2["TRTCVideoRotation_180"] = 2] = "TRTCVideoRotation_180";
  TRTCVideoRotation2[TRTCVideoRotation2["TRTCVideoRotation_270"] = 3] = "TRTCVideoRotation_270";
})(TRTCVideoRotation || (TRTCVideoRotation = {}));
var TRTCVideoMirrorType;
(function(TRTCVideoMirrorType2) {
  TRTCVideoMirrorType2[TRTCVideoMirrorType2["TRTCVideoMirrorType_Auto"] = 0] = "TRTCVideoMirrorType_Auto";
  TRTCVideoMirrorType2[TRTCVideoMirrorType2["TRTCVideoMirrorType_Enable"] = 1] = "TRTCVideoMirrorType_Enable";
  TRTCVideoMirrorType2[TRTCVideoMirrorType2["TRTCVideoMirrorType_Disable"] = 2] = "TRTCVideoMirrorType_Disable";
})(TRTCVideoMirrorType || (TRTCVideoMirrorType = {}));
var TRTCBeautyStyle;
(function(TRTCBeautyStyle2) {
  TRTCBeautyStyle2[TRTCBeautyStyle2["TRTCBeautyStyleSmooth"] = 0] = "TRTCBeautyStyleSmooth";
  TRTCBeautyStyle2[TRTCBeautyStyle2["TRTCBeautyStyleNature"] = 1] = "TRTCBeautyStyleNature";
  TRTCBeautyStyle2[TRTCBeautyStyle2["TRTCBeautyStylePitu"] = 2] = "TRTCBeautyStylePitu";
})(TRTCBeautyStyle || (TRTCBeautyStyle = {}));
var TRTCAppScene;
(function(TRTCAppScene2) {
  TRTCAppScene2[TRTCAppScene2["TRTCAppSceneVideoCall"] = 0] = "TRTCAppSceneVideoCall";
  TRTCAppScene2[TRTCAppScene2["TRTCAppSceneLIVE"] = 1] = "TRTCAppSceneLIVE";
  TRTCAppScene2[TRTCAppScene2["TRTCAppSceneAudioCall"] = 2] = "TRTCAppSceneAudioCall";
  TRTCAppScene2[TRTCAppScene2["TRTCAppSceneVoiceChatRoom"] = 3] = "TRTCAppSceneVoiceChatRoom";
})(TRTCAppScene || (TRTCAppScene = {}));
var TRTCRoleType;
(function(TRTCRoleType2) {
  TRTCRoleType2[TRTCRoleType2["TRTCRoleAnchor"] = 20] = "TRTCRoleAnchor";
  TRTCRoleType2[TRTCRoleType2["TRTCRoleAudience"] = 21] = "TRTCRoleAudience";
})(TRTCRoleType || (TRTCRoleType = {}));
var TRTCAudioQuality;
(function(TRTCAudioQuality2) {
  TRTCAudioQuality2[TRTCAudioQuality2["TRTCAudioQualitySpeech"] = 1] = "TRTCAudioQualitySpeech";
  TRTCAudioQuality2[TRTCAudioQuality2["TRTCAudioQualityDefault"] = 2] = "TRTCAudioQualityDefault";
  TRTCAudioQuality2[TRTCAudioQuality2["TRTCAudioQualityMusic"] = 3] = "TRTCAudioQualityMusic";
})(TRTCAudioQuality || (TRTCAudioQuality = {}));
var TRTCAudioRoute;
(function(TRTCAudioRoute2) {
  TRTCAudioRoute2[TRTCAudioRoute2["TRTCAudioRouteSpeaker"] = 0] = "TRTCAudioRouteSpeaker";
  TRTCAudioRoute2[TRTCAudioRoute2["TRTCAudioRouteEarpiece"] = 1] = "TRTCAudioRouteEarpiece";
})(TRTCAudioRoute || (TRTCAudioRoute = {}));
var TRTCShareSource;
(function(TRTCShareSource2) {
  TRTCShareSource2["InApp"] = "InApp";
  TRTCShareSource2["ByReplaykit"] = "ByReplaykit";
})(TRTCShareSource || (TRTCShareSource = {}));
const TXLiteJSError = {
  /**
   * 未知错误
   * @default 0xFFFF
   * @memberof module:ErrorCode
   */
  UNKNOWN: 65535,
  /**
   * 无效参数
   *
   * @default 0x1000
   * @memberof module:ErrorCode
   */
  INVALID_PARAMETER: 4096,
  /**
   * 非法操作
   *
   * @default 0x1001
   * @memberof module:ErrorCode
   */
  INVALID_OPERATION: 4097
};
const getErrorName = function(code) {
  for (let key in TXLiteJSError) {
    if (TXLiteJSError[key] === code) {
      return key;
    }
  }
  return "UNKNOWN";
};
class TrtcError extends Error {
  constructor({ code = TXLiteJSError.UNKNOWN, message, extraInfo }) {
    if (extraInfo) {
      const tempError = {
        errCode: code,
        errMsg: message,
        extraInfo: Object.assign(Object.assign({}, extraInfo), { errCodeUrl: errorCodeUrl })
      };
      super(JSON.stringify(tempError));
    } else {
      super(message + ` <${getErrorName(code)} 0x${code.toString(16)}>. Refer to: ${errorCodeUrl}`);
    }
    this.errCode = code;
    this.errMsg = message;
    this.extraInfo = Object.assign(Object.assign({}, extraInfo), { errCodeUrl: errorCodeUrl });
  }
  /**
   * 获取错误码<br>
   * 详细错误码列表参见 {@link module:ErrorCode ErrorCode}
   * @memberof TrtcError
   */
  getCode() {
    return this.errCode;
  }
}
function generateError_(error, code = TXLiteJSError.UNKNOWN, extraInfo) {
  return new TrtcError({
    code: error.code || code,
    message: `${NAME.LOG_PREFIX}${error.message}`,
    extraInfo
  });
}
var __awaiter = globalThis && globalThis.__awaiter || function(thisArg, _arguments, P2, generator) {
  function adopt(value) {
    return value instanceof P2 ? value : new P2(function(resolve) {
      resolve(value);
    });
  }
  return new (P2 || (P2 = Promise))(function(resolve, reject) {
    function fulfilled(value) {
      try {
        step(generator.next(value));
      } catch (e2) {
        reject(e2);
      }
    }
    function rejected(value) {
      try {
        step(generator["throw"](value));
      } catch (e2) {
        reject(e2);
      }
    }
    function step(result) {
      result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
    }
    step((generator = generator.apply(thisArg, _arguments || [])).next());
  });
};
const TrtcNativeTrtcCloudModule = requireNativePlugin("TRTCCloudUniPlugin-TRTCCloudImpl");
const TXAudioEffectManagerModule = requireNativePlugin("TRTCCloudUniPlugin-TRTCCloudImpl-TXAudioEffectManagerModule");
const TrtcEvent = requireNativePlugin("globalEvent");
let trtcCloud = null;
class TrtcCloudImpl {
  constructor() {
    this.listenersMap_ = /* @__PURE__ */ new Map();
  }
  static _createInstance() {
    try {
      if (trtcCloud) {
        return trtcCloud;
      }
      TrtcNativeTrtcCloudModule.sharedInstance();
      trtcCloud = new TrtcCloudImpl();
      return trtcCloud;
    } catch (error) {
      throw generateError_(error);
    }
  }
  static _getInstance() {
    if (trtcCloud) {
      return trtcCloud;
    }
    throw new TrtcError({
      code: TXLiteJSError.INVALID_OPERATION,
      message: "get trtcCloud failed, please create trtcCloud first"
    });
  }
  static _destroyInstance() {
    try {
      trtcCloud = null;
      TrtcNativeTrtcCloudModule.destroySharedInstance();
    } catch (error) {
      throw new TrtcError({
        code: error.code || TXLiteJSError.UNKNOWN,
        message: error.message,
        name: error.name
      });
    }
  }
  // 截图保存
  // async saveImage_(base64Data) {
  //   return new Promise((resolve, reject) => {
  //     let bitmap = new plus.nativeObj.Bitmap();
  //     bitmap.loadBase64Data(base64Data, () => {
  //       const url = "_doc/" + new Date().getTime() + ".png";  // url为时间戳命名方式
  //       __f__('log','at TrtcCloud/lib/TrtcCloudImpl.js:62','saveHeadImgFile', url);
  //       bitmap.save(url, { overwrite: true }, (i) => {
  //         uni.saveImageToPhotosAlbum({
  //           filePath: url,
  //           success: function() {
  //             uni.showToast({
  //               title: '图片保存成功',
  //               icon: 'none'
  //             })
  //             bitmap.clear();
  //             resolve({ code: 0, message: '图片保存成功' });
  //           }
  //         });
  //       }, (e) => {
  //         uni.showToast({
  //           title: '图片保存失败, 请重新截图',
  //           icon: 'none'
  //         })
  //         bitmap.clear();
  //         resolve({ code: -1, message: '图片保存失败, 请重新截图' });
  //       });
  //     });
  //   });
  // }
  on(event, callback) {
    if (typeof event !== NAME.STRING || typeof callback !== NAME.FUNCTION) {
      throw new TrtcError({
        code: TXLiteJSError.INVALID_PARAMETER,
        message: `${NAME.LOG_PREFIX} please check the on method parameter types. event type is a ${typeof event}; callback type is a ${typeof callback}`
      });
    }
    const nativeListener = (res) => __awaiter(this, void 0, void 0, function* () {
      const { data = [] } = res;
      const code = data[0];
      const message = data[1] || "";
      const extraInfo = data[2] || {};
      switch (event) {
        case "onEnterRoom": {
          const result = code;
          callback(result);
          break;
        }
        case "onExitRoom": {
          const reason = code;
          callback(reason);
          break;
        }
        case "onFirstVideoFrame": {
          const userId = code;
          const streamType = data[1] || 0;
          const width = data[2] || 0;
          const height = data[3] || 0;
          callback({ userId, streamType, width, height });
          break;
        }
        case "onFirstAudioFrame": {
          const userId = code || "";
          callback(userId);
          break;
        }
        case "onMicDidReady": {
          callback();
          break;
        }
        case "onCameraDidReady": {
          callback();
          break;
        }
        case "onNetworkQuality": {
          const localQuality = data[0];
          const remoteQuality = data[1];
          callback({ localQuality, remoteQuality });
          break;
        }
        case "onRemoteUserEnterRoom": {
          const userId = code || "";
          callback(userId);
          break;
        }
        case "onRemoteUserLeaveRoom": {
          const userId = code || "";
          const reason = message;
          callback({ userId, reason });
          break;
        }
        case "onSendFirstLocalAudioFrame": {
          callback();
          break;
        }
        case "onSendFirstLocalVideoFrame": {
          const streamType = code;
          callback(streamType);
          break;
        }
        case "onStatistics": {
          const statics = data[0] || {};
          callback(statics);
          break;
        }
        case "onUserAudioAvailable": {
          const userId = code || "";
          const available = message;
          callback({ userId, available });
          break;
        }
        case "onUserVideoAvailable": {
          const userId = code || "";
          const available = message;
          callback({ userId, available });
          break;
        }
        case "onUserVoiceVolume": {
          const userVolumes = data[0];
          const totalVolume = data[1];
          callback({ userVolumes, totalVolume });
          break;
        }
        case "onSwitchRole": {
          callback({ code, message });
          break;
        }
        case "onScreenCaptureStarted": {
          callback({ code, message });
          break;
        }
        case "onScreenCapturePaused": {
          callback({ code, message });
          break;
        }
        case "onScreenCaptureResumed": {
          callback({ code, message });
          break;
        }
        case "onScreenCaptureStopped": {
          callback({ code, message });
          break;
        }
        case "onUserSubStreamAvailable": {
          const userId = code || "";
          const available = message;
          callback({ userId, available });
          break;
        }
        case "onSnapshotComplete": {
          callback({ base64Data: code, message });
          break;
        }
        case "onUserVideoSizeChanged": {
          callback(data);
          break;
        }
        case "onStart": {
          callback({ id: code, errCode: message });
          break;
        }
        case "onPlayProgress": {
          callback({ id: code, curPtsMS: message, durationMS: extraInfo });
          break;
        }
        case "onComplete": {
          callback({ id: code, errCode: message });
          break;
        }
        case "onError": {
          formatAppLog("error", "at TrtcCloud/lib/TrtcCloudImpl.js:229", `onError: ${code}, ${message}, ${extraInfo}`);
          callback(generateError_({ message }, code, extraInfo));
          break;
        }
        default: {
          callback({ code, message, extraInfo });
        }
      }
    });
    this.listenersMap_.set(event, nativeListener);
    TrtcEvent.addEventListener(event, nativeListener);
  }
  off(event) {
    if (typeof event !== NAME.STRING) {
      throw new TrtcError({
        code: TXLiteJSError.INVALID_PARAMETER,
        message: `${NAME.LOG_PREFIX} please check the off method parameter types. event type is a ${typeof event} not a ${NAME.STRING}`
      });
    }
    try {
      if (event === "*") {
        this.listenersMap_.forEach((value, key) => {
          TrtcEvent.removeEventListener(key, value);
        });
        this.listenersMap_.clear();
      } else {
        TrtcEvent.removeEventListener(event, this.listenersMap_.get(event));
        this.listenersMap_.delete(event);
      }
    } catch (error) {
      throw generateError_(error);
    }
  }
  enterRoom(params, scene) {
    if (scene !== TRTCAppScene.TRTCAppSceneVideoCall && scene !== TRTCAppScene.TRTCAppSceneLIVE && scene !== TRTCAppScene.TRTCAppSceneAudioCall && scene !== TRTCAppScene.TRTCAppSceneVoiceChatRoom) {
      throw new TrtcError({
        code: TXLiteJSError.INVALID_PARAMETER,
        message: `${NAME.LOG_PREFIX} please check the enterRoom method parameters. scene is not of TRTCAppScene`
      });
    }
    try {
      const enterRoomParams = Object.assign(Object.assign({}, params), { role: params.role || TRTCRoleType.TRTCRoleAnchor, appScene: scene });
      TrtcNativeTrtcCloudModule.enterRoom(enterRoomParams);
    } catch (error) {
      throw generateError_(error);
    }
  }
  exitRoom() {
    try {
      TrtcNativeTrtcCloudModule.exitRoom();
    } catch (error) {
      throw generateError_(error);
    }
  }
  switchRole(role) {
    if (role !== TRTCRoleType.TRTCRoleAnchor && role !== TRTCRoleType.TRTCRoleAudience) {
      throw new TrtcError({
        code: TXLiteJSError.INVALID_PARAMETER,
        message: `${NAME.LOG_PREFIX} please check the switchRole method parameter. role is not of TRTCRoleType`
      });
    }
    try {
      role && TrtcNativeTrtcCloudModule.switchRole(role);
    } catch (error) {
      throw generateError_(error);
    }
  }
  startLocalPreview(isFrontCamera = true, viewId) {
    if (typeof isFrontCamera !== NAME.BOOLEAN || !viewId || typeof viewId !== NAME.STRING) {
      throw new TrtcError({
        code: TXLiteJSError.INVALID_PARAMETER,
        message: `${NAME.LOG_PREFIX} please check the startLocalPreview method parameters`
      });
    }
    try {
      let param = { isFrontCamera: !!isFrontCamera };
      param = viewId ? Object.assign(Object.assign({}, param), { userId: viewId }) : param;
      TrtcNativeTrtcCloudModule.startLocalPreview(param);
    } catch (error) {
      throw generateError_(error);
    }
  }
  setVideoEncoderParam(param) {
    try {
      TrtcNativeTrtcCloudModule.setVideoEncoderParam(param);
    } catch (error) {
      throw generateError_(error);
    }
  }
  stopLocalPreview() {
    try {
      TrtcNativeTrtcCloudModule.stopLocalPreview();
    } catch (error) {
      throw generateError_(error);
    }
  }
  switchCamera(isFrontCamera) {
    if (typeof isFrontCamera !== NAME.BOOLEAN) {
      throw new TrtcError({
        code: TXLiteJSError.INVALID_PARAMETER,
        message: `${NAME.LOG_PREFIX} please check the switchCamera method parameter`
      });
    }
    try {
      TrtcNativeTrtcCloudModule.switchCamera(isFrontCamera);
    } catch (error) {
      throw generateError_(error);
    }
  }
  setLocalRenderParams(params) {
    try {
      const { rotation = TRTCVideoRotation.TRTCVideoRotation_0, fillMode = TRTCVideoFillMode.TRTCVideoFillMode_Fill, mirrorType = TRTCVideoMirrorType.TRTCVideoMirrorType_Auto } = params;
      TrtcNativeTrtcCloudModule.setLocalRenderParams({
        rotation,
        fillMode,
        mirrorType
      });
    } catch (error) {
      throw generateError_(error);
    }
  }
  muteLocalVideo(streamType, mute) {
    if (streamType !== TRTCVideoStreamType.TRTCVideoStreamTypeBig && streamType !== TRTCVideoStreamType.TRTCVideoStreamTypeSub || typeof mute !== NAME.BOOLEAN) {
      throw new TrtcError({
        code: TXLiteJSError.INVALID_PARAMETER,
        message: `${NAME.LOG_PREFIX} please check the muteLocalVideo method parameters`
      });
    }
    try {
      TrtcNativeTrtcCloudModule.muteLocalVideo({ streamType, mute: !!mute });
    } catch (error) {
      throw generateError_(error);
    }
  }
  startRemoteView(userId, streamType, viewId) {
    if (!userId || streamType !== TRTCVideoStreamType.TRTCVideoStreamTypeBig && streamType !== TRTCVideoStreamType.TRTCVideoStreamTypeSmall && streamType !== TRTCVideoStreamType.TRTCVideoStreamTypeSub || !viewId) {
      throw new TrtcError({
        code: TXLiteJSError.INVALID_PARAMETER,
        message: `${NAME.LOG_PREFIX} please check the startRemoteView method parameters`
      });
    }
    try {
      TrtcNativeTrtcCloudModule.startRemoteView({ userId, streamType, viewId });
    } catch (error) {
      throw generateError_(error);
    }
  }
  stopRemoteView(userId, streamType) {
    if (!userId || streamType !== TRTCVideoStreamType.TRTCVideoStreamTypeBig && streamType !== TRTCVideoStreamType.TRTCVideoStreamTypeSmall && streamType !== TRTCVideoStreamType.TRTCVideoStreamTypeSub) {
      throw new TrtcError({
        code: TXLiteJSError.INVALID_PARAMETER,
        message: `${NAME.LOG_PREFIX} please check the stopRemoteView method parameters`
      });
    }
    try {
      TrtcNativeTrtcCloudModule.stopRemoteView({ userId, streamType });
    } catch (error) {
      throw generateError_(error);
    }
  }
  // 远端渲染设置
  setRemoteRenderParams(userId, streamType, params) {
    try {
      if (!userId || streamType !== TRTCVideoStreamType.TRTCVideoStreamTypeBig && streamType !== TRTCVideoStreamType.TRTCVideoStreamTypeSub) {
        throw new TrtcError({
          code: TXLiteJSError.INVALID_PARAMETER,
          message: `${NAME.LOG_PREFIX} please check the snapshotVideo method parameters`
        });
      }
      const { rotation = TRTCVideoRotation.TRTCVideoRotation_0, fillMode = TRTCVideoFillMode.TRTCVideoFillMode_Fill, mirrorType = TRTCVideoMirrorType.TRTCVideoMirrorType_Auto } = params;
      TrtcNativeTrtcCloudModule.setRemoteRenderParams({
        userId,
        streamType,
        rotation,
        fillMode,
        mirrorType
      });
    } catch (error) {
      throw generateError_(error);
    }
  }
  // 截图
  snapshotVideo(userId, streamType) {
    if (streamType !== TRTCVideoStreamType.TRTCVideoStreamTypeBig && streamType !== TRTCVideoStreamType.TRTCVideoStreamTypeSub) {
      throw new TrtcError({
        code: TXLiteJSError.INVALID_PARAMETER,
        message: `${NAME.LOG_PREFIX} please check the snapshotVideo method parameters`
      });
    }
    try {
      TrtcNativeTrtcCloudModule.snapshotVideo({ userId: userId || null, streamType });
    } catch (error) {
      throw generateError_(error);
    }
  }
  startLocalAudio(quality = TRTCAudioQuality.TRTCAudioQualityDefault) {
    if (quality !== TRTCAudioQuality.TRTCAudioQualitySpeech && quality !== TRTCAudioQuality.TRTCAudioQualityDefault && quality !== TRTCAudioQuality.TRTCAudioQualityMusic) {
      throw new TrtcError({
        code: TXLiteJSError.INVALID_PARAMETER,
        message: `${NAME.LOG_PREFIX} please check the startLocalAudio method parameters`
      });
    }
    try {
      TrtcNativeTrtcCloudModule.startLocalAudio(quality);
    } catch (error) {
      throw generateError_(error);
    }
  }
  stopLocalAudio() {
    try {
      TrtcNativeTrtcCloudModule.stopLocalAudio();
    } catch (error) {
      throw generateError_(error);
    }
  }
  muteLocalAudio(mute) {
    if (typeof mute !== NAME.BOOLEAN) {
      throw new TrtcError({
        code: TXLiteJSError.INVALID_PARAMETER,
        message: `${NAME.LOG_PREFIX} please check the muteLocalAudio method parameters, mute type is a ${typeof mute} not a ${NAME.BOOLEAN}`
      });
    }
    try {
      TrtcNativeTrtcCloudModule.muteLocalAudio(!!mute);
    } catch (error) {
      throw generateError_(error);
    }
  }
  muteRemoteAudio(userId, mute) {
    if (typeof mute !== NAME.BOOLEAN || !userId) {
      throw new TrtcError({
        code: TXLiteJSError.INVALID_PARAMETER,
        message: `${NAME.LOG_PREFIX} please check the muteRemoteAudio method parameters`
      });
    }
    try {
      TrtcNativeTrtcCloudModule.muteRemoteAudio({ userId, mute: !!mute });
    } catch (error) {
      throw generateError_(error);
    }
  }
  muteAllRemoteAudio(mute) {
    if (typeof mute !== NAME.BOOLEAN) {
      throw new TrtcError({
        code: TXLiteJSError.INVALID_PARAMETER,
        message: `${NAME.LOG_PREFIX} please check the muteAllRemoteAudio method parameters, mute type is a ${typeof mute} not a ${NAME.BOOLEAN}`
      });
    }
    try {
      TrtcNativeTrtcCloudModule.muteAllRemoteAudio(!!mute);
    } catch (error) {
      throw generateError_(error);
    }
  }
  setAudioRoute(route) {
    if (route !== TRTCAudioRoute.TRTCAudioRouteSpeaker && route !== TRTCAudioRoute.TRTCAudioRouteEarpiece) {
      throw new TrtcError({
        code: TXLiteJSError.INVALID_PARAMETER,
        message: `${NAME.LOG_PREFIX} please check the setAudioRoute method parameter, route is not of TRTCAudioRoute`
      });
    }
    try {
      TrtcNativeTrtcCloudModule.setAudioRoute(route);
    } catch (error) {
      throw generateError_(error);
    }
  }
  enableAudioVolumeEvaluation(interval) {
    if (typeof interval !== NAME.NUMBER) {
      throw new TrtcError({
        code: TXLiteJSError.INVALID_PARAMETER,
        message: `${NAME.LOG_PREFIX} please check the enableAudioVolumeEvaluation method parameter, interval type is a ${typeof interval} not a ${NAME.NUMBER}`
      });
    }
    try {
      interval > 0 && TrtcNativeTrtcCloudModule.enableAudioVolumeEvaluation(interval);
    } catch (error) {
      throw generateError_(error);
    }
  }
  // ///////////////////////////////////////////////////////////////////////////////
  //
  //                      美颜 + 水印
  //
  // ///////////////////////////////////////////////////////////////////////////////
  setBeautyStyle(beautyStyle) {
    if (beautyStyle !== TRTCBeautyStyle.TRTCBeautyStyleSmooth && beautyStyle !== TRTCBeautyStyle.TRTCBeautyStyleNature && beautyStyle !== TRTCBeautyStyle.TRTCBeautyStylePitu) {
      throw new TrtcError({
        code: TXLiteJSError.INVALID_PARAMETER,
        message: `${NAME.LOG_PREFIX} please check the setBeautyStyle method parameter, beautyStyle is not of TRTCBeautyStyle`
      });
    }
    try {
      TrtcNativeTrtcCloudModule.setBeautyStyle(beautyStyle);
    } catch (error) {
      throw generateError_(error);
    }
  }
  setBeautyLevel(beautyLevel) {
    if (typeof beautyLevel !== NAME.NUMBER || (beautyLevel < 0 || beautyLevel > 9)) {
      throw new TrtcError({
        code: TXLiteJSError.INVALID_PARAMETER,
        message: `${NAME.LOG_PREFIX} please check the setBeautyLevel method parameter, beautyLevel should in the range 0-9`
      });
    }
    try {
      TrtcNativeTrtcCloudModule.setBeautyLevel(beautyLevel);
    } catch (error) {
      throw generateError_(error);
    }
  }
  // ///////////////////////////////////////////////////////////////////////////////
  //
  //                      背景音效
  //
  // ///////////////////////////////////////////////////////////////////////////////
  startPlayMusic(musicParam) {
    try {
      TXAudioEffectManagerModule.startPlayMusic(musicParam);
    } catch (error) {
      throw generateError_(error);
    }
  }
  stopPlayMusic(id) {
    try {
      TXAudioEffectManagerModule.stopPlayMusic(id);
    } catch (error) {
      throw generateError_(error);
    }
  }
  pausePlayMusic(id) {
    try {
      TXAudioEffectManagerModule.pausePlayMusic(id);
    } catch (error) {
      throw generateError_(error);
    }
  }
  resumePlayMusic(id) {
    try {
      TXAudioEffectManagerModule.resumePlayMusic(id);
    } catch (error) {
      throw generateError_(error);
    }
  }
  // ///////////////////////////////////////////////////////////////////////////////
  //
  //                      屏幕分享
  //
  // ///////////////////////////////////////////////////////////////////////////////
  setSubStreamEncoderParam(param) {
    try {
      TrtcNativeTrtcCloudModule.setSubStreamEncoderParam(param);
    } catch (error) {
      throw generateError_(error);
    }
  }
  startScreenCapture(streamType = TRTCVideoStreamType.TRTCVideoStreamTypeSub, encParams = null) {
    try {
      let platform = uni.getSystemInfoSync().platform;
      if (streamType !== TRTCVideoStreamType.TRTCVideoStreamTypeSub && streamType !== TRTCVideoStreamType.TRTCVideoStreamTypeBig) {
        streamType = TRTCVideoStreamType.TRTCVideoStreamTypeSub;
      }
      const screenCaptureParams = Object.assign({ streamType }, encParams);
      if (platform === NAME.ANDROID) {
        TrtcNativeTrtcCloudModule.startScreenCapture(screenCaptureParams);
      }
      if (platform === NAME.IOS) {
        TrtcNativeTrtcCloudModule.startScreenCaptureInApp(screenCaptureParams);
      }
    } catch (error) {
      throw generateError_(error);
    }
  }
  stopScreenCapture() {
    try {
      TrtcNativeTrtcCloudModule.stopScreenCapture();
    } catch (error) {
      throw generateError_(error);
    }
  }
  pauseScreenCapture() {
    try {
      TrtcNativeTrtcCloudModule.pauseScreenCapture();
    } catch (error) {
      throw generateError_(error);
    }
  }
  resumeScreenCapture() {
    try {
      TrtcNativeTrtcCloudModule.resumeScreenCapture();
    } catch (error) {
      throw generateError_(error);
    }
  }
}
const version = "1.2.0";
class TrtcCloud {
  /**
   * 创建 TrtcCloud 单例
   *
   * @static
   * @memberof TrtcCloud
   * @example
   * TrtcCloud.createInstance();
   */
  static createInstance() {
    formatAppLog("log", "at TrtcCloud/lib/index.js:20", "----------------------------------------------------------------");
    formatAppLog("log", "at TrtcCloud/lib/index.js:21", `                        SDK ${version}                    `);
    formatAppLog("log", "at TrtcCloud/lib/index.js:22", "----------------------------------------------------------------");
    return TrtcCloudImpl._createInstance();
  }
  /**
   * 销毁 TrtcCloud 单例
   *
   * @static
   * @memberof TrtcCloud
   * @example
   * TrtcCloud.destroyInstance();
   */
  static destroyInstance() {
    return TrtcCloudImpl._destroyInstance();
  }
  /**
   * 设置 TrtcCloud 事件监听
   *
   * @param {String} event 事件名称
   * @param {Function} callback 事件回调
   * @memberof TrtcCloud
   *
   * @example
   * this.trtcCloud = TrtcCloud.createInstance(); // 创建 trtcCloud 实例
   * this.trtcCloud.on('onEnterRoom', (res) => {});
   */
  on(event, callback) {
    return TrtcCloudImpl._getInstance().on(event, callback);
  }
  /**
   * 取消事件绑定<br>
   *
   * @param {String} event 事件名称，传入通配符 '*' 会解除所有事件绑定。
   * @memberof TrtcCloud
   * @example
   * this.trtcCloud.off('onEnterRoom');
   *
   * this.trtcCloud.off('*'); // 取消所有绑定的事件
   */
  off(event) {
    return TrtcCloudImpl._getInstance().off(event);
  }
  /**
   * 进房<br>
   * 调用接口后，您会收到来自 TRTCCallback 中的 [onEnterRoom(result)]{@link TRTCCallback#onEnterRoom} 回调
   * 如果加入成功，result 会是一个正数（result > 0），表示加入房间所消耗的时间，单位是毫秒（ms）。<br>
   * 如果加入失败，result 会是一个负数（result < 0），表示进房失败的错误码。
   *
   * * 参数 scene 的枚举值如下：
   * - {@link TRTCAppSceneVideoCall}：<br>
   *          视频通话场景，支持720P、1080P高清画质，单个房间最多支持300人同时在线，最高支持50人同时发言。<br>
   *          适合：[1对1视频通话]、[300人视频会议]、[在线问诊]、[视频聊天]、[远程面试]等。<br>
   * - {@link TRTCAppSceneAudioCall}：<br>
   *          语音通话场景，支持 48kHz，支持双声道。单个房间最多支持300人同时在线，最高支持50人同时发言。<br>
   *          适合：[1对1语音通话]、[300人语音会议]、[语音聊天]、[语音会议]、[在线狼人杀]等。<br>
   * - {@link TRTCAppSceneLIVE}：<br>
   *          视频互动直播，支持平滑上下麦，切换过程无需等待，主播延时小于300ms；支持十万级别观众同时播放，播放延时低至1000ms。<br>
   *          适合：[视频低延时直播]、[十万人互动课堂]、[视频直播 PK]、[视频相亲房]、[互动课堂]、[远程培训]、[超大型会议]等。<br>
   * - {@link TRTCAppSceneVoiceChatRoom}：<br>
   *          语音互动直播，支持平滑上下麦，切换过程无需等待，主播延时小于300ms；支持十万级别观众同时播放，播放延时低至1000ms。<br>
   *          适合：[语音低延时直播]、[语音直播连麦]、[语聊房]、[K 歌房]、[FM 电台]等。<br>
   *
   * **Note:**
   * 1. 当 scene 选择为 TRTCAppSceneLIVE 或 TRTCAppSceneVoiceChatRoom 时，您必须通过 TRTCParams 中的 role 字段指定当前用户的角色。
   * 2. 不管进房是否成功，enterRoom 都必须与 exitRoom 配对使用，在调用 `exitRoom` 前再次调用 `enterRoom` 函数会导致不可预期的错误问题。
   *
   * @param {TRTCParams} params - 进房参数
   * @param {Number} params.sdkAppId      - 应用标识（必填）
   * @param {String} params.userId        - 用户标识（必填）
   * @param {String} params.userSig       - 用户签名（必填）
   * @param {Number} params.roomId        - 房间号码, roomId 和 strRoomId 必须填一个, 若您选用 strRoomId，则 roomId 需要填写为0。
   * @param {String} params.strRoomId     - 字符串房间号码 [选填]，在同一个房间内的用户可以看到彼此并进行视频通话, roomId 和 strRoomId 必须填一个。若两者都填，则优先选择 roomId
   * @param {TRTCRoleType} params.role    - 直播场景下的角色，默认值：主播
   * - TRTCRoleAnchor: 主播，可以上行视频和音频，一个房间里最多支持50个主播同时上行音视频。
   * - TRTCRoleAudience: 观众，只能观看，不能上行视频和音频，一个房间里的观众人数没有上限。
   * @param {String=} params.privateMapKey - 房间签名（非必填）
   * @param {String=} params.businessInfo  - 业务数据（非必填）
   * @param {String=} params.streamId      - 自定义 CDN 播放地址（非必填）
   * @param {String=} params.userDefineRecordId - 设置云端录制完成后的回调消息中的 "userdefinerecordid" 字段内容，便于您更方便的识别录制回调（非必填）
   * @param {TRTCAppScene} scene 应用场景，目前支持视频通话（TRTCAppSceneVideoCall）、语音通话（TRTCAppSceneAudioCall）、在线直播（TRTCAppSceneLIVE）、语音聊天室（VTRTCAppSceneVoiceChatRoom）四种场景，
   * 详见 [TrtcDefines] 中 TRTCAppScene 参数定义
   *
   * @memberof TrtcCloud
   * @example
   * import { TRTCAppScene } from '@/TrtcCloud/lib/TrtcDefines';
   * this.trtcCloud = TrtcCloud.createInstance(); // 创建实例，只需创建一次
   * const params = {
   *   sdkAppId: 0,
   *   userId: 'xxx',
   *   roomId: 12345,
   *   userSig: 'xxx'
   * };
   * this.trtcCloud.enterRoom(params, TRTCAppScene.TRTCAppSceneVideoCall);
   */
  enterRoom(params, scene) {
    return TrtcCloudImpl._getInstance().enterRoom(params, scene);
  }
  /**
   * 退房<br>
   * 执行退出房间的相关逻辑释放资源后，SDK 会通过 `onExitRoom()` 回调通知到您
   *
   * **Note:**
   * 1. 如果您要再次调用 `enterRoom()` 或者切换到其它的音视频 SDK，请等待 `onExitRoom()` 回调到来后再执行相关操作，否则可能会遇到如摄像头、麦克风设备被强占等各种异常问题。
   *
   * @memberof TrtcCloud
   * @example
   * this.trtcCloud.exitRoom();
   */
  exitRoom() {
    return TrtcCloudImpl._getInstance().exitRoom();
  }
  /**
   * 切换角色，仅适用于直播场景（TRTCAppSceneLIVE 和 TRTCAppSceneVoiceChatRoom）
   *
   * 在直播场景下，一个用户可能需要在“观众”和“主播”之间来回切换。
   * 您可以在进房前通过 TRTCParams 中的 role 字段确定角色，也可以通过 switchRole 在进房后切换角色。
   *
   * @param {TRTCRoleType} role - 目标角色，默认为主播
   * - TRTCRoleAnchor: 主播，可以上行视频和音频，一个房间里最多支持50个主播同时上行音视频。
   * - TRTCRoleAudience: 观众，只能观看，不能上行视频和音频，一个房间里的观众人数没有上限。
   *
   * @memberof TrtcCloud
   * @example
   * import { TRTCRoleType } from '@/TrtcCloud/lib/TrtcDefines';
   * this.trtcCloud.switchRole(TRTCRoleType.TRTCRoleAudience);
   */
  switchRole(role) {
    return TrtcCloudImpl._getInstance().switchRole(role);
  }
  /**
   * 开启本地视频的预览画面<br>
   * 当开始渲染首帧摄像头画面时，您会收到 `onFirstVideoFrame(null)` 回调
   *
   * @param {Boolean} isFrontCamera 前置、后置摄像头，true：前置摄像头；false：后置摄像头，**默认为 true**
   * @param {String=} viewId 用于承载视频画面的渲染控件，使用原生插件中的 TRTCCloudUniPlugin-TXLocalViewComponent component，需要提供 viewId 属性值，例如 viewId=userId
   * @memberof TrtcCloud
   * @example
   * // 预览本地画面
   * const viewId = this.userId;
   * this.trtcCloud.startLocalPreview(true, viewId);
   */
  startLocalPreview(isFrontCamera = true, viewId) {
    return TrtcCloudImpl._getInstance().startLocalPreview(isFrontCamera, viewId);
  }
  /**
   * 设置视频编码器的编码参数
   * - 该设置能够决定远端用户看到的画面质量，同时也能决定云端录制出的视频文件的画面质量。
   * @param {TRTCVideoEncParam} param 用于设置视频编码器的相关参数
   * @memberof TrtcCloud
   * @example
   *
   * import { TRTCVideoResolution, TRTCVideoResolutionMode, TRTCVideoEncParam } from '@/TrtcCloud/lib/TrtcDefines';
   * const videoResolution = TRTCVideoResolution.TRTCVideoResolution_480_360;
   * const videoResolutionMode = TRTCVideoResolutionMode.TRTCVideoResolutionModeLandscape; // 横屏采集
   * const videoFps = 15;
   * const videoBitrate = 900;
   * const minVideoBitrate = 200;
   * const enableAdjustRes = false;
   * // const param = new TRTCVideoEncParam(videoResolution, videoResolutionMode, videoFps, videoBitrate, minVideoBitrate, enableAdjustRes); // v1.1.0 方式
   *
   * const param = { // v1.2.0 以上版本支持的方式
   *  videoResolution,
   *  videoResolutionMode,
   *  videoFps,
   *  videoBitrate,
   *  minVideoBitrate,
   *  enableAdjustRes,
   * };
   *
   * this.trtcCloud.setVideoEncoderParam(param);
   */
  setVideoEncoderParam(param) {
    return TrtcCloudImpl._getInstance().setVideoEncoderParam(param);
  }
  /**
   * 切换前置或后置摄像头
   *
   * @param {Boolean} isFrontCamera 前置、后置摄像头，true：前置摄像头；false：后置摄像头
   * @memberof TrtcCloud
   * @example
   * // 切换前置或后置摄像头
   * const isFrontCamera = true;
   * this.trtcCloud.switchCamera(isFrontCamera);
   */
  switchCamera(isFrontCamera) {
    return TrtcCloudImpl._getInstance().switchCamera(isFrontCamera);
  }
  /**
   * 停止本地视频采集及预览
   *
   * @memberof TrtcCloud
   * @example
   * this.trtcCloud.stopLocalPreview();
   */
  stopLocalPreview() {
    return TrtcCloudImpl._getInstance().stopLocalPreview();
  }
  /**
   * 设置本地画面的渲染参数，可设置的参数包括有：画面的旋转角度、填充模式以及左右镜像等。
   * @param {TRTCRenderParams} params - 本地图像的参数
   * @param {TRTCVideoRotation} params.rotation - 图像的顺时针旋转角度，支持90、180以及270旋转角度，默认值：TRTCVideoRotation.TRTCVideoRotation_0
   * @param {TRTCVideoFillMode} params.fillMode - 视频画面填充模式，填充（画面可能会被拉伸裁剪）或适应（画面可能会有黑边），默认值：TRTCVideoFillMode.TRTCVideoFillMode_Fill
   * @param {TRTCVideoMirrorType} params.mirrorType - 画面镜像模式，默认值：TRTCVideoMirrorType.TRTCVideoMirrorType_Auto
   *
   * @memberof TrtcCloud
   * @example
   * import { TRTCVideoRotation, TRTCVideoFillMode, TRTCVideoMirrorType } from '@/TrtcCloud/lib/TrtcDefines';
   * const renderParams = {
   *  rotation: TRTCVideoRotation.TRTCVideoRotation_0,
   *  fillMode: TRTCVideoFillMode.TRTCVideoFillMode_Fill,
   *  mirrorType: TRTCVideoMirrorType.TRTCVideoMirrorType_Auto
   * };
   * this.trtcCloud.setLocalRenderParams(renderParams);
   */
  setLocalRenderParams(params) {
    return TrtcCloudImpl._getInstance().setLocalRenderParams(params);
  }
  /**
   * 暂停/恢复发布本地的视频流
   *
   * 该接口可以暂停（或恢复）发布本地的视频画面，暂停之后，同一房间中的其他用户将无法继续看到自己画面。 该接口在指定 TRTCVideoStreamTypeBig 时等效于 start/stopLocalPreview 这两个接口，但具有更好的响应速度。 因为 start/stopLocalPreview 需要打开和关闭摄像头，而打开和关闭摄像头都是硬件设备相关的操作，非常耗时。 相比之下，muteLocalVideo 只需要在软件层面对数据流进行暂停或者放行即可，因此效率更高，也更适合需要频繁打开关闭的场景。 当暂停/恢复发布指定 TRTCVideoStreamTypeBig 后，同一房间中的其他用户将会收到 onUserVideoAvailable 回调通知。 当暂停/恢复发布指定 TRTCVideoStreamTypeSub 后，同一房间中的其他用户将会收到 onUserSubStreamAvailable 回调通知。
   * @param {TRTCVideoStreamType} streamType 要暂停/恢复的视频流类型（仅支持 TRTCVideoStreamTypeBig 和 TRTCVideoStreamTypeSub）
   * @param {Boolean} mute - true：屏蔽；false：开启，默认值：false
   *
   * @memberof TrtcCloud
   * @example
   * this.trtcCloud.muteLocalVideo(TRTCVideoStreamType.TRTCVideoStreamTypeBig, true);
   */
  muteLocalVideo(streamType, mute) {
    return TrtcCloudImpl._getInstance().muteLocalVideo(streamType, mute);
  }
  /**
   * 显示远端视频或辅流<br>
   *
   * @param {String} userId 指定远端用户的 userId
   * @param {TRTCVideoStreamType} streamType 指定要观看 userId 的视频流类型
   * - 高清大画面：TRTCVideoStreamType.TRTCVideoStreamTypeBig
   * - 低清小画面：TRTCVideoStreamType.TRTCVideoStreamTypeSmall
   * - 辅流（屏幕分享）：TRTCVideoStreamType.TRTCVideoStreamTypeSub
   * @param {String} viewId 用于承载视频画面的渲染控件，使用原生插件中的 TRTCCloudUniPlugin-TXRemoteViewComponent component，需要提供 viewId 属性值，例如 viewId=userId
   * @memberof TrtcCloud
   * @example
   * import { TRTCVideoStreamType } from '@/TrtcCloud/lib/TrtcDefines';
   * const viewId = this.remoteUserId;
   * this.trtcCloud.startRemoteView(userId, TRTCVideoStreamType.TRTCVideoStreamTypeBig, viewId);
   */
  startRemoteView(userId, streamType, viewId) {
    return TrtcCloudImpl._getInstance().startRemoteView(userId, streamType, viewId);
  }
  /**
   * 停止显示远端视频画面，同时不再拉取该远端用户的视频数据流<br>
   * 指定要停止观看的 userId 的视频流类型
   *
   * @param {String} userId 指定的远端用户 ID
   * @param {TRTCVideoStreamType} streamType
   * - 高清大画面：TRTCVideoStreamType.TRTCVideoStreamTypeBig
   * - 低清小画面：TRTCVideoStreamType.TRTCVideoStreamTypeSmall
   * - 辅流（屏幕分享）：TRTCVideoStreamType.TRTCVideoStreamTypeSub
   * @memberof TrtcCloud
   * @example
   * import { TRTCVideoStreamType } from '@/TrtcCloud/lib/TrtcDefines';
   * this.trtcCloud.stopRemoteView(remoteUserId, TRTCVideoStreamType.TRTCVideoStreamTypeBig);
   */
  stopRemoteView(userId, streamType) {
    return TrtcCloudImpl._getInstance().stopRemoteView(userId, streamType);
  }
  /**
   * 设置远端画面的渲染参数，可设置的参数包括有：画面的旋转角度、填充模式以及左右镜像等。
   * @param {String} userId 远端用户 ID
   * @param {TRTCVideoStreamType} streamType 可以设置为主路画面（TRTCVideoStreamTypeBig）或辅路画面（TRTCVideoStreamTypeSub）
   * @param {TRTCRenderParams} params - 图像的参数
   * @param {TRTCVideoRotation} params.rotation - 图像的顺时针旋转角度，支持90、180以及270旋转角度，默认值：TRTCVideoRotation.TRTCVideoRotation_0
   * @param {TRTCVideoFillMode} params.fillMode - 视频画面填充模式，填充（画面可能会被拉伸裁剪）或适应（画面可能会有黑边），默认值：TRTCVideoFillMode.TRTCVideoFillMode_Fill
   * @param {TRTCVideoMirrorType} params.mirrorType - 画面镜像模式，默认值：TRTCVideoMirrorType.TRTCVideoMirrorType_Auto
   * @memberof TrtcCloud
   * @example
   * import { TRTCVideoRotation, TRTCVideoFillMode, TRTCVideoMirrorType } from '@/TrtcCloud/lib/TrtcDefines';
   * const renderParams = {
   *  rotation: TRTCVideoRotation.TRTCVideoRotation_0,
   *  fillMode: TRTCVideoFillMode.TRTCVideoFillMode_Fill,
   *  mirrorType: TRTCVideoMirrorType.TRTCVideoMirrorType_Auto
   * };
   * this.trtcCloud.setRemoteRenderParams(userId, TRTCVideoStreamType.TRTCVideoStreamTypeBig, renderParams);
   */
  setRemoteRenderParams(userId, streamType, params) {
  }
  /**
   * 视频画面截图
   *
   * 您可以通过本接口截取本地的视频画面，远端用户的主路画面以及远端用户的辅路（屏幕分享）画面。
   * @param {String | null} userId 用户 ID，如指定 null 表示截取本地的视频画面
   * @param {TRTCVideoStreamType} streamType 视频流类型，可选择截取主路画面（TRTCVideoStreamTypeBig，常用于摄像头）或辅路画面（TRTCVideoStreamTypeSub，常用于屏幕分享）
   *
   * @memberof TrtcCloud
   * @example
   * import { TRTCVideoStreamType } from '@/TrtcCloud/lib/TrtcDefines';
   * this.trtcCloud.snapshotVideo(null, TRTCVideoStreamType.TRTCVideoStreamTypeBig); // 截取本地画面
   * this.trtcCloud.snapshotVideo(this.remoteUserId, TRTCVideoStreamType.TRTCVideoStreamTypeBig); // 截取远端指定用户画面
   */
  snapshotVideo(userId, streamType, listener) {
    return TrtcCloudImpl._getInstance().snapshotVideo(userId, streamType, listener);
  }
  /**
   * 开启本地音频的采集和上行, 并设置音频质量<br>
   * 该函数会启动麦克风采集，并将音频数据传输给房间里的其他用户。 SDK 不会默认开启本地音频采集和上行，您需要调用该函数开启，否则房间里的其他用户将无法听到您的声音<br>
   * 主播端的音质越高，观众端的听感越好，但传输所依赖的带宽也就越高，在带宽有限的场景下也更容易出现卡顿
   *
   * @param {TRTCAudioQuality} quality 声音音质
   * - TRTCAudioQualitySpeech，流畅：采样率：16k；单声道；音频裸码率：16kbps；适合语音通话为主的场景，比如在线会议，语音通话。
   * - TRTCAudioQualityDefault，默认：采样率：48k；单声道；音频裸码率：50kbps；SDK 默认的音频质量，如无特殊需求推荐选择之。
   * - TRTCAudioQualityMusic，高音质：采样率：48k；双声道 + 全频带；音频裸码率：128kbps；适合需要高保真传输音乐的场景，比如在线K歌、音乐直播等
   * @memberof TrtcCloud
   * @example
   * import { TRTCAudioQuality } from '@/TrtcCloud/lib/TrtcDefines';
   * this.trtcCloud.startLocalAudio(TRTCAudioQuality.TRTCAudioQualityDefault);
   */
  startLocalAudio(quality) {
    return TrtcCloudImpl._getInstance().startLocalAudio(quality);
  }
  /**
   * 关闭本地音频的采集和上行<br>
   * 当关闭本地音频的采集和上行，房间里的其它成员会收到 `onUserAudioAvailable(false)` 回调通知
   *
   * @memberof TrtcCloud
   * @example
   * this.trtcCloud.stopLocalAudio();
   */
  stopLocalAudio() {
    return TrtcCloudImpl._getInstance().stopLocalAudio();
  }
  /**
   * 静音本地的音频
   *
   * 当静音本地音频后，房间里的其它成员会收到 onUserAudioAvailable(false) 回调通知。
   * 与 stopLocalAudio 不同之处在于，muteLocalAudio 并不会停止发送音视频数据，而是会继续发送码率极低的静音包。
   * 在对录制质量要求很高的场景中，选择 muteLocalAudio 是更好的选择，能录制出兼容性更好的 MP4 文件。
   * 这是由于 MP4 等视频文件格式，对于音频的连续性是要求很高的，简单粗暴地 stopLocalAudio 会导致录制出的 MP4 不易播放。
   *
   * @param {Boolean} mute - true：屏蔽；false：开启，默认值：false
   *
   * @memberof TrtcCloud
   * @example
   * this.trtcCloud.muteLocalAudio(true);
   */
  muteLocalAudio(mute) {
    return TrtcCloudImpl._getInstance().muteLocalAudio(mute);
  }
  /**
   * 静音掉某一个用户的声音，同时不再拉取该远端用户的音频数据流
   *
   * @param {String}  userId - 用户 ID
   * @param {Boolean} mute   - true：静音；false：非静音
   *
   * @memberof TrtcCloud
   * @example
   * this.trtcCloud.muteRemoteAudio('denny', true);
   */
  muteRemoteAudio(userId, mute) {
    return TrtcCloudImpl._getInstance().muteRemoteAudio(userId, mute);
  }
  /**
   * 静音掉所有用户的声音，同时不再拉取该远端用户的音频数据流
   *
   * @param {Boolean} mute - true：静音；false：非静音
   *
   * @memberof TrtcCloud
   * @example
   * this.trtcCloud.muteAllRemoteAudio(true);
   */
  muteAllRemoteAudio(mute) {
    return TrtcCloudImpl._getInstance().muteAllRemoteAudio(mute);
  }
  /**
   * 设置音频路由
   *
   * 设置“音频路由”，即设置声音是从手机的扬声器还是从听筒中播放出来，因此该接口仅适用于手机等移动端设备。 手机有两个扬声器：一个是位于手机顶部的听筒，一个是位于手机底部的立体声扬声器。
   * 设置音频路由为听筒时，声音比较小，只有将耳朵凑近才能听清楚，隐私性较好，适合用于接听电话。 设置音频路由为扬声器时，声音比较大，不用将手机贴脸也能听清，因此可以实现“免提”的功能。
   *
   * @param {TRTCAudioRoute} route 音频路由，即声音由哪里输出（扬声器、听筒）, 默认值：TRTCAudioRoute.TRTCAudioRouteSpeaker（扬声器）,
   * @memberof TrtcCloud
   * @example
   * import { TRTCAudioRoute } from '@/TrtcCloud/lib/TrtcDefines';
   * this.trtcCloud.setAudioRoute(TRTCAudioRoute.TRTCAudioRouteSpeaker); // TRTCAudioRoute.TRTCAudioRouteEarpiece (听筒)
   */
  setAudioRoute(route) {
    return TrtcCloudImpl._getInstance().setAudioRoute(route);
  }
  /**
   * 启用或关闭音量大小提示
   *
   * 开启此功能后，SDK 会在 onUserVoiceVolume() 中反馈对每一路声音音量大小值的评估。
   *
   * **Note:**
   * - 如需打开此功能，请在 startLocalAudio 之前调用才可以生效。
   *
   * @param {Number} interval - 设置 onUserVoiceVolume 回调的触发间隔，单位为ms，最小间隔为100ms，如果小于等于0则会关闭回调，建议设置为300ms
   * @memberof TrtcCloud
   * @example
   * this.trtcCloud.enableAudioVolumeEvaluation(300);
   */
  enableAudioVolumeEvaluation(interval) {
    return TrtcCloudImpl._getInstance().enableAudioVolumeEvaluation(interval);
  }
  /////////////////////////////////////////////////////////////////////////////////
  //
  //                      屏幕分享
  //
  /////////////////////////////////////////////////////////////////////////////////
  /**
   * 设置屏幕分享（即辅路）的视频编码参数
   *
   * 该接口可以设定远端用户所看到的屏幕分享（即辅路）的画面质量，同时也能决定云端录制出的视频文件中屏幕分享的画面质量。 请注意如下两个接口的差异：
   *  - setVideoEncoderParam 用于设置主路画面（TRTCVideoStreamTypeBig，一般用于摄像头）的视频编码参数。
   *  - setSubStreamEncoderParam 用于设置辅路画面（TRTCVideoStreamTypeSub，一般用于屏幕分享）的视频编码参数。
   *
   * **Note:**
   *  - 即使您使用主路传输屏幕分享（在调用 startScreenCapture 时设置 type=TRTCVideoStreamTypeBig），依然要使用 setSubStreamEncoderParam 设定屏幕分享的编码参数，而不要使用 setVideoEncoderParam
   * @param {TRTCVideoEncParam} param	辅流编码参数，详情请参考 TRTCVideoEncParam。
   * @memberof TrtcCloud
   * @example
   * const params = {
   *   videoResolution: TRTCVideoResolution.TRTCVideoResolution_640_360,
   *   videoResolutionMode: TRTCVideoResolutionMode.TRTCVideoResolutionModePortrait,
   *   videoFps: 15,
   *   videoBitrate: 900,
   *   minVideoBitrate: 200,
   *   enableAdjustRes: false,
   * };
   * this.trtcCloud.setSubStreamEncoderParam(params);
   */
  setSubStreamEncoderParam(param) {
    return TrtcCloudImpl._getInstance().setSubStreamEncoderParam(param);
  }
  /**
   * 启动屏幕分享
   *
   * **Note:**
   *  - 一个用户同时最多只能上传一条主路（TRTCVideoStreamTypeBig）画面和一条辅路（TRTCVideoStreamTypeSub）画面，
   * 默认情况下，屏幕分享使用辅路画面，如果使用主路画面，建议您提前停止摄像头采集（stopLocalPreview）避免相互冲突。
   *  - **仅支持 iOS 13.0 及以上系统，进行应用内的屏幕分享**
   *
   * @param {TRTCVideoStreamType} streamType 屏幕分享使用的线路，可以设置为主路（TRTCVideoStreamTypeBig）或者辅路（TRTCVideoStreamTypeSub），推荐使用
   * @param {TRTCVideoEncParam} encParams 屏幕分享的画面编码参数，可以设置为 null，表示让 SDK 选择最佳的编码参数（分辨率、码率等）。即使在调用 startScreenCapture 时设置 type=TRTCVideoStreamTypeBig，依然可以使用此接口更新屏幕分享的编码参数。
   * @memberof TrtcCloud
   * @example
   * import { TRTCVideoResolution, TRTCVideoResolutionMode, TRTCVideoStreamType} from '@/TrtcCloud/lib/TrtcDefines';
   * const encParams = {
   *   videoResolution: TRTCVideoResolution.TRTCVideoResolution_640_360,
   *   videoResolutionMode: TRTCVideoResolutionMode.TRTCVideoResolutionModePortrait,
   *   videoFps: 15,
   *   videoBitrate: 900,
   *   minVideoBitrate: 200,
   *   enableAdjustRes: false,
   * };
   * this.trtcCloud.startScreenCapture(TRTCVideoStreamType.TRTCVideoStreamTypeSub, encParams);
   */
  startScreenCapture(streamType = TRTCVideoStreamType.TRTCVideoStreamTypeSub, encParams = null) {
    return TrtcCloudImpl._getInstance().startScreenCapture(streamType, encParams);
  }
  /**
   * 停止屏幕分享
   * @memberof TrtcCloud
   * @example
   * this.trtcCloud.stopScreenCapture();
   */
  stopScreenCapture() {
    return TrtcCloudImpl._getInstance().stopScreenCapture();
  }
  /**
   * 暂停屏幕分享
   * @memberof TrtcCloud
   * @example
   * this.trtcCloud.pauseScreenCapture();
   */
  pauseScreenCapture() {
    return TrtcCloudImpl._getInstance().pauseScreenCapture();
  }
  /**
   * 恢复屏幕分享
   * @memberof TrtcCloud
   * @example
   * this.trtcCloud.resumeScreenCapture();
   */
  resumeScreenCapture() {
    return TrtcCloudImpl._getInstance().resumeScreenCapture();
  }
  /////////////////////////////////////////////////////////////////////////////////
  //
  //                      美颜 + 水印
  //
  /////////////////////////////////////////////////////////////////////////////////
  /**
   * 设置美颜（磨皮）算法
   * TRTC 内置多种不同的磨皮算法，您可以选择最适合您产品定位的方案
   *
   * **Note:**
   * - 设置美颜前，先调用 `setBeautyLevel` 设置美颜级别。否则美颜级别为 0 表示关闭美颜
   *
   * @param {TRTCBeautyStyle} beautyStyle 美颜风格，TRTCBeautyStyleSmooth：光滑；TRTCBeautyStyleNature：自然；TRTCBeautyStylePitu：优图
   * @memberof TrtcCloud
   * @example
   * import { TRTCBeautyStyle } from '@/TrtcCloud/lib/TrtcDefines';
   * const beautyLevel = 5; // 美颜级别，取值范围0 - 9； 0表示关闭，9表示效果最明显。
   * this.trtcCloud.setBeautyLevel(beautyLevel);
   * this.trtcCloud.setBeautyStyle(TRTCBeautyStyle.TRTCBeautyStyleSmooth);
   */
  setBeautyStyle(beautyStyle) {
    return TrtcCloudImpl._getInstance().setBeautyStyle(beautyStyle);
  }
  /**
   * 设置美颜级别
   * @param {Number} beautyLevel	美颜级别，取值范围0 - 9； 0表示关闭，9表示效果最明显。
   *
   * @memberof TrtcCloud
   * @example
   * const beautyLevel = 5; // 美颜级别，取值范围0 - 9； 0表示关闭，9表示效果最明显。
   * this.trtcCloud.setBeautyLevel(beautyLevel);
   */
  setBeautyLevel(beautyLevel) {
    return TrtcCloudImpl._getInstance().setBeautyLevel(beautyLevel);
  }
  /////////////////////////////////////////////////////////////////////////////////
  //
  //                      背景音效
  //
  /////////////////////////////////////////////////////////////////////////////////
  /**
   * 开始播放背景音乐
   * 每个音乐都需要您指定具体的 ID，您可以通过该 ID 对音乐的开始、停止、音量等进行设置。<br>
   * **Note:**
   * - 如果要多次播放同一首背景音乐，请不要每次播放都分配一个新的 ID，我们推荐使用相同的 ID。
   * - 若您希望同时播放多首不同的音乐，请为不同的音乐分配不同的 ID 进行播放。
   * - 如果使用同一个 ID 播放不同音乐，SDK 会先停止播放旧的音乐，再播放新的音乐。
   *
   * **Note:**<br>
   * 在 uni-app 中 path 如何获取。
   * - 使用 cdn 地址，例如：`path = https://web.sdk.qcloud.com/component/TUIKit/assets/uni-app/calling-bell-1.mp3;`
   * - 使用本地绝对路径。
   *     1. 通过 [uni.saveFile](https://zh.uniapp.dcloud.io/api/file/file.html#savefile) 获取保存后的相对路径（建议这种路径）。
   *     2. 将上一步的相对路径转成绝对路径，[plus.io.convertLocalFileSystemURL](https://www.html5plus.org/doc/zh_cn/io.html#plus.io.convertLocalFileSystemURL)。
   *
   * @param {AudioMusicParam} musicParam 音乐参数
   * @param {Number} musicParam.id 音乐 ID
   * @param {String} musicParam.path 音效文件的完整路径或 URL 地址。支持的音频格式包括 MP3、AAC、M4A、WAV
   * @param {Number} musicParam.loopCount 音乐循环播放的次数。取值范围为0 - 任意正整数，默认值：0。0表示播放音乐一次；1表示播放音乐两次；以此类推
   * @param {Boolean} musicParam.publish 是否将音乐传到远端。true：音乐在本地播放的同时，远端用户也能听到该音乐；false：主播只能在本地听到该音乐，远端观众听不到。默认值：false。
   * @param {Boolean} musicParam.isShortFile 播放的是否为短音乐文件。true：需要重复播放的短音乐文件；false：正常的音乐文件。默认值：false
   * @param {Number} musicParam.startTimeMS 音乐开始播放时间点，单位: 毫秒。
   * @param {Number} musicParam.endTimeMS 音乐结束播放时间点，单位: 毫秒，0 表示播放至文件结尾。
   * @memberof TrtcCloud
   * @example
   * import { AudioMusicParam } from '@/TrtcCloud/lib/TrtcDefines';
   * const musicParam = {
   *  id: 1,
   *  path: '',
   *  loopCount: 1,
   *  publish: true,
   *  isShortFile: false,
   *  startTimeMS: 0,
   *  endTimeMS: 0,
   * };
   * this.trtcCloud.startPlayMusic(musicParam);
   */
  startPlayMusic(musicParam) {
    return TrtcCloudImpl._getInstance().startPlayMusic(musicParam);
  }
  /**
   * 停止播放背景音乐
   * @param {Number} id	音乐 ID
   *
   * @memberof TrtcCloud
   * @example
   * const musicId = 5;
   * this.trtcCloud.stopPlayMusic(musicId);
   */
  stopPlayMusic(id) {
    return TrtcCloudImpl._getInstance().stopPlayMusic(id);
  }
  /**
   * 暂停播放背景音乐
   * @param {Number} id	音乐 ID
   * @memberof TrtcCloud
   * @example
   * const musicId = 5;
   * this.trtcCloud.pausePlayMusic(musicId);
   */
  pausePlayMusic(id) {
    return TrtcCloudImpl._getInstance().pausePlayMusic(id);
  }
  /**
   * 恢复播放背景音乐
   * @param {Number} id	音乐 ID
   * @memberof TrtcCloud
   * @example
   * const musicId = 5;
   * this.trtcCloud.resumePlayMusic(musicId);
   */
  resumePlayMusic(id) {
    return TrtcCloudImpl._getInstance().resumePlayMusic(id);
  }
  /////////////////////////////////////////////////////////////////////////////////
  //
  //                       设置 TRTCCallback 回调
  //
  /////////////////////////////////////////////////////////////////////////////////
  /**
   * 设置 TrtcCloud 回调
   *
   * @example
   * // 创建/使用/销毁 TrtcCloud 对象的示例代码：
   * import TrtcCloud from '@/TrtcCloud/lib/index';
   * this.trtcCloud = new TrtcCloud();
   *
   * // 添加事件监听的方法，事件关键字详见下方”通用事件回调“
   * this.trtcCloud.on('onEnterRoom', (result) => {
   *   if (result > 0) {
   *     __f__('log','at TrtcCloud/lib/index.js:636',`enter room success, spend ${result}ms`);
   *   } else {
   *     __f__('log','at TrtcCloud/lib/index.js:638',`enter room failed, error code = ${result}`);
   *   }
   * });
   *
   * @namespace TRTCCallback
   */
  /////////////////////////////////////////////////////////////////////////////////
  //
  //                      （一）事件回调
  //
  /////////////////////////////////////////////////////////////////////////////////
  /**
   * 错误回调，表示 SDK 不可恢复的错误，一定要监听并分情况给用户适当的界面提示<br>
   * @event TRTCCallback#onError
   * @param {Number} code 错误码，[详见](https://cloud.tencent.com/document/product/647/38308#.E9.94.99.E8.AF.AF.E7.A0.81.E8.A1.A8)
   * @param {String} message 错误信息
   * @param {Object} extraInfo 扩展信息字段，个别错误码可能会带额外的信息帮助定位问题
   */
  onError(code, message, extraInfo) {
  }
  /**
   * 警告回调，用于告知您一些非严重性问题，例如出现卡顿或者可恢复的解码失败<br>
   * @event TRTCCallback#onWarning
   * @param {Number} code 警告码，[详见](https://cloud.tencent.com/document/product/647/38308#.E8.AD.A6.E5.91.8A.E7.A0.81.E8.A1.A8)
   * @param {String} message 警告信息
   * @param {Object} extraInfo 扩展信息字段，个别警告码可能会带额外的信息帮助定位问题
   */
  onWarning(code, message, extraInfo) {
  }
  /**
   * 进房后的回调<br>
   * 调用 `enterRoom()` 接口执行进房操作后，会收到 `onEnterRoom(result)` 回调<br>
   * 如果加入成功，result 会是一个正数（result > 0），代表加入房间的时间消耗，单位是毫秒（ms）。<br>
   * 如果加入失败，result 会是一个负数（result < 0），代表进房失败的错误码。
   *
   * @event TRTCCallback#onEnterRoom
   * @param {Number} result 进房耗时
   */
  onEnterRoom(result) {
  }
  /**
   * 离开房间的事件回调<br>
   * 调用 `exitRoom()` 接口会执行退出房间的相关逻辑，例如释放音视频设备资源和编解码器资源等。待资源释放完毕，会通过 `onExitRoom()` 回调通知到您<br>
   *
   * **Note:**
   * - 如果您要再次调用 `enterRoom()` 或者切换到其他的音视频 SDK，请等待 `onExitRoom()` 回调到来之后再执行相关操作。 否则可能会遇到音频设备被占用等各种异常问题
   *
   * @event TRTCCallback#onExitRoom
   * @param {Number} reason 离开房间原因，0：主动调用 exitRoom 退房；1：被服务器踢出当前房间；2：当前房间整个被解散
   */
  onExitRoom(reason) {
  }
  /**
   * 切换角色的事件回调<br>
   * 调用 TRTCCloud 中的 switchRole() 接口会切换主播和观众的角色，该操作会伴随一个线路切换的过程， 待 SDK 切换完成后，会抛出 onSwitchRole() 事件回调
   *
   * @event TRTCCallback#onSwitchRole
   * @param {Number} code 错误码，[详见](https://cloud.tencent.com/document/product/647/38308#.E8.AD.A6.E5.91.8A.E7.A0.81.E8.A1.A8)
   * @param {String} message 错误信息
   */
  onSwitchRole(code, message) {
  }
  /**
   * 开始渲染本地或远程用户的首帧画面<br>
   * 如果 userId 为 null，代表开始渲染本地采集的摄像头画面，需要您先调用 `startLocalPreview` 触发。 如果 userId 不为 null，代表开始渲染远程用户的首帧画面，需要您先调用 `startRemoteView` 触发<br>
   * 只有当您调用 `startLocalPreview()、startRemoteView() 或 startRemoteSubStreamView()` 之后，才会触发该回调
   *
   * @event TRTCCallback#onFirstVideoFrame
   * @param {String} userId 本地或远程用户 ID，如果 userId === null 代表本地，userId !== null 代表远程
   * @param {TRTCVideoStreamType} streamType 视频流类型：摄像头或屏幕分享
   * @param {Number} width 画面宽度
   * @param {Number} height 画面高度
   */
  onFirstVideoFrame(userId, streamType, width, height) {
  }
  /**
   * 开始播放远程用户的首帧音频（本地声音暂不通知）<br>
   * 如果 userId 为 null，代表开始渲染本地采集的摄像头画面，需要您先调用 `startLocalPreview` 触发。 如果 userId 不为 null，代表开始渲染远程用户的首帧画面，需要您先调用 `startRemoteView` 触发<br>
   * 只有当您调用 `startLocalPreview()、startRemoteView() 或 startRemoteSubStreamView()` 之后，才会触发该回调
   *
   * @event TRTCCallback#onFirstAudioFrame
   * @param {String} userId 远程用户 ID
   */
  onFirstAudioFrame(userId) {
  }
  /**
   * 截图完成时回调<br>
   * @event TRTCCallback#onSnapshotComplete
   * @param {String} base64Data 截图对应的 base64 数据
   * @param {String} message 错误信息
   */
  onSnapshotComplete(base64Data, message) {
  }
  /**
   * 麦克风准备就绪
   */
  onMicDidReady() {
  }
  /**
   * 摄像头准备就绪
   */
  onCameraDidReady() {
  }
  /**
   * 网络质量：该回调每2秒触发一次，统计当前网络的上行和下行质量<br>
   * userId 为本地用户 ID 代表自己当前的视频质量
   *
   * @param {String} localQuality 上行网络质量
   * @param {String} remoteQuality 下行网络质量
   */
  onNetworkQuality(localQuality, remoteList) {
  }
  /**
   * 有用户加入当前房间<br>
   * 出于性能方面的考虑，在两种不同的应用场景下，该通知的行为会有差别：<br>
   * 通话场景（TRTCAppScene.TRTCAppSceneVideoCall 和 TRTCAppScene.TRTCAppSceneAudioCall）：该场景下用户没有角色的区别，任何用户进入房间都会触发该通知。<br>
   * 直播场景（TRTCAppScene.TRTCAppSceneLIVE 和 TRTCAppScene.TRTCAppSceneVoiceChatRoom ）：该场景不限制观众的数量，如果任何用户进出都抛出回调会引起很大的性能损耗，所以该场景下只有主播进入房间时才会触发该通知，观众进入房间不会触发该通知
   *
   * @event TRTCCallback#onRemoteUserEnterRoom
   * @param {String} userId 用户标识 ID
   */
  onRemoteUserEnterRoom(userId) {
  }
  /**
   * 有用户离开当前房间<br>
   * 与 onRemoteUserEnterRoom 相对应，在两种不同的应用场景下，该通知的行为会有差别：<br>
   * 通话场景（TRTCAppScene.TRTCAppSceneVideoCall 和 TRTCAppScene.TRTCAppSceneAudioCall）：该场景下用户没有角色的区别，任何用户进入房间都会触发该通知。<br>
   * 直播场景（TRTCAppScene.TRTCAppSceneLIVE 和 TRTCAppScene.TRTCAppSceneVoiceChatRoom ）：该场景不限制观众的数量，如果任何用户进出都抛出回调会引起很大的性能损耗，所以该场景下只有主播进入房间时才会触发该通知，观众进入房间不会触发该通知
   *
   * @event TRTCCallback#onRemoteUserLeaveRoom
   * @param {String} userId 用户标识 ID
   * @param {Number} reason 离开原因，0 表示用户主动退出房间，1 表示用户超时退出，2 表示被踢出房间
   */
  onRemoteUserLeaveRoom(userId, reason) {
  }
  /**
   * 首帧本地音频数据已经被送出<br>
   * 在 `enterRoom()` 并 `startLocalAudio()` 成功后开始麦克风采集，并将采集到的声音进行编码。 当 SDK 成功向云端送出第一帧音频数据后，会抛出这个回调事件
   *
   * @event TRTCCallback#onSendFirstLocalAudioFrame
   */
  onSendFirstLocalAudioFrame() {
  }
  /**
   * 首帧本地视频数据已经被送出<br>
   * SDK 会在 `enterRoom()` 并 `startLocalPreview()` 成功后开始摄像头采集，并将采集到的画面进行编码。 当 SDK 成功向云端送出第一帧视频数据后，会抛出这个回调事件
   *
   * @event TRTCCallback#onSendFirstLocalVideoFrame
   * @param {TRTCVideoStreamType} streamType 视频流类型，大画面、小画面或辅流画面（屏幕分享）
   */
  onSendFirstLocalVideoFrame(streamType) {
  }
  /**
   * 技术指标统计回调<br>
   * 如果您是熟悉音视频领域相关术语，可以通过这个回调获取 SDK 的所有技术指标。 如果您是首次开发音视频相关项目，可以只关注 `onNetworkQuality` 回调
   *
   * **Note:**
   * - 每 2 秒回调一次
   *
   * @param {Object} statics 状态数据
   */
  onStatistics(statics) {
  }
  /**
   * 远端用户是否存在可播放的音频数据<br>
   * @event TRTCCallback#onUserAudioAvailable
   * @param {String} userId 用户标识 ID
   * @param {Boolean} available 声音是否开启
   */
  onUserAudioAvailable(userId, available) {
  }
  /**
   * 远端用户是否存在可播放的主路画面（一般用于摄像头）<br>
   * 当您收到 `onUserVideoAvailable(userId, true)` 通知时，表示该路画面已经有可用的视频数据帧到达。 此时，您需要调用 `startRemoteView(userId)` 接口加载该用户的远程画面。 然后，您会收到名为 onFirstVideoFrame(userid) 的首帧画面渲染回调。<br>
   * 当您收到 `onUserVideoAvailable(userId, false)` 通知时，表示该路远程画面已经被关闭，可能由于该用户调用了 `muteLocalVideo()` 或 `stopLocalPreview()`。
   *
   * @event TRTCCallback#onUserVideoAvailable
   * @param {String} userId 用户标识 ID
   * @param {Boolean} available 画面是否开启
   */
  onUserVideoAvailable(userId, available) {
  }
  /**
   * 用于提示音量大小的回调，包括每个 userId 的音量和远端总音量<br>
   * SDK 可以评估每一路音频的音量大小，并每隔一段时间抛出该事件回调，您可以根据音量大小在 UI 上做出相应的提示，比如“波形图”或“音量槽”。 要完成这个功能， 您需要先调用 enableAudioVolumeEvaluation 开启这个能力并设定事件抛出的时间间隔。 需要补充说明的是，无论当前房间中是否有人说话，SDK 都会按照您设定的时间间隔定时抛出此事件回调，只不过当没有人说话时，userVolumes 为空，totalVolume 为 0。
   *
   * **Note:**
   * - userVolumes 为一个数组，对于数组中的每一个元素，当 userId 为空时表示本地麦克风采集的音量大小，当 userId 不为空时代表远端用户的音量大小
   *
   * @event TRTCCallback#onUserVoiceVolume
   * @param {Array} userVolumes 是一个数组，用于承载所有正在说话的用户的音量大小，取值范围 0 - 100
   * @param {Number} totalVolume 所有远端用户的总音量大小, 取值范围 0 - 100
   */
  onUserVoiceVolume(userVolumes, totalVolume) {
  }
  /**
   * 屏幕分享开启的事件回调<br>
   * 当您通过 startScreenCapture 等相关接口启动屏幕分享时，SDK 便会抛出此事件回调
   * @event TRTCCallback#onScreenCaptureStarted
   */
  onScreenCaptureStarted() {
  }
  /**
   * 屏幕分享停止的事件回调<br>
   * 当您通过 stopScreenCapture 停止屏幕分享时，SDK 便会抛出此事件回调
   * @event TRTCCallback#onScreenCaptureStopped
   * @param {Number} reason 停止原因，0：用户主动停止；1：屏幕窗口关闭导致停止；2：表示屏幕分享的显示屏状态变更（如接口被拔出、投影模式变更等）
   */
  onScreenCaptureStopped(reason) {
  }
  /**
   * 屏幕分享停止的事件回调<br>
   * 当您通过 pauseScreenCapture 停止屏幕分享时，SDK 便会抛出此事件回调
   * @event TRTCCallback#onScreenCapturePaused
   * @param {Number} reason 停止原因，0：用户主动停止；1：屏幕窗口关闭导致停止；2：表示屏幕分享的显示屏状态变更（如接口被拔出、投影模式变更等）
   */
  onScreenCapturePaused(reason) {
  }
  /**
   * 屏幕分享恢复的事件回调<br>
   * 当您通过 resumeScreenCapture 恢复屏幕分享时，SDK 便会抛出此事件回调
   * @event TRTCCallback#onScreenCaptureResumed
   */
  onScreenCaptureResumed() {
  }
  /**
   * 某远端用户发布/取消了辅路视频画面<br>
   * “辅路画面”一般被用于承载屏幕分享的画面。当您收到 onUserSubStreamAvailable(userId, true) 通知时，表示该路画面已经有可播放的视频帧到达。 此时，您需要调用 startRemoteView 接口订阅该用户的远程画面，订阅成功后，您会继续收到该用户的首帧画面渲染回调 onFirstVideoFrame(userId)
   *
   * **Note:**
   * - 拉取 Web 端（用 [WebRTC](https://web.sdk.qcloud.com/trtc/webrtc/doc/zh-cn/index.html) 实现屏幕分享）的屏幕分享，收不到 onUserSubStreamAvailable 事件。因为 [WebRTC](https://web.sdk.qcloud.com/trtc/webrtc/doc/zh-cn/index.html) 推的屏幕分享也是主流
   * @param {String} userId 用户 ID
   * @param {Boolean} available 是否可用，true 表示辅流可用
   * @event TRTCCallback#onUserSubStreamAvailable
   */
  onUserSubStreamAvailable(userId, available) {
  }
  /**
   * 用户视频大小发生改变回调。<br>
   * 当您收到 onUserVideoSizeChanged(userId, streamtype, newWidth, newHeight) 通知时，表示该路画面大小发生了调整，调整的原因可能是该用户调用了 setVideoEncoderParam 或者 setSubStreamEncoderParam 重新设置了画面尺寸。
   * @param {String} userId 用户 ID
   * @param {TRTCVideoStreamType} streamType 视频流类型，仅支持 TRTCVideoStreamTypeBig 和 TRTCVideoStreamTypeSub
   * @param {Number} newWidth 视频流的宽度（像素）
   * @param {Number} newHeight 视频流的高度（像素）
   * @event TRTCCallback#onUserVideoSizeChanged
   */
  onUserVideoSizeChanged(userId, streamType, newWidth, newHeight) {
  }
  /**
   * 背景音乐开始播放
   * @param {Number} id 播放的 id
   * @param {Number} errCode 播放的状态码
   * @event TRTCCallback#onStart
   */
  onStart(id, errCode) {
  }
  /**
   * 背景音乐的播放进度
   * @param {Number} id 播放的 id
   * @param {Number} curPtsMS 当前播放的位置
   * @param {Number} durationMS 当前音频总时长
   * @event TRTCCallback#onPlayProgress
   */
  onPlayProgress(id, curPtsMS, durationMS) {
  }
  /**
   * 背景音乐已经播放完毕
   * @param {Number} id 播放的 id
   * @param {Number} errCode 播放结束的状态码
   * @event TRTCCallback#onComplete
   */
  onComplete(id, errCode) {
  }
}
var e = "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {}, t = [], r = [], n = "undefined" != typeof Uint8Array ? Uint8Array : Array, i = false;
function o() {
  i = true;
  for (var e2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", n2 = 0, o2 = e2.length; n2 < o2; ++n2)
    t[n2] = e2[n2], r[e2.charCodeAt(n2)] = n2;
  r["-".charCodeAt(0)] = 62, r["_".charCodeAt(0)] = 63;
}
function a(e2, r2, n2) {
  for (var i2, o2, a2 = [], s2 = r2; s2 < n2; s2 += 3)
    i2 = (e2[s2] << 16) + (e2[s2 + 1] << 8) + e2[s2 + 2], a2.push(t[(o2 = i2) >> 18 & 63] + t[o2 >> 12 & 63] + t[o2 >> 6 & 63] + t[63 & o2]);
  return a2.join("");
}
function s(e2) {
  var r2;
  i || o();
  for (var n2 = e2.length, s2 = n2 % 3, h2 = "", l2 = [], f2 = 0, c2 = n2 - s2; f2 < c2; f2 += 16383)
    l2.push(a(e2, f2, f2 + 16383 > c2 ? c2 : f2 + 16383));
  return 1 === s2 ? (r2 = e2[n2 - 1], h2 += t[r2 >> 2], h2 += t[r2 << 4 & 63], h2 += "==") : 2 === s2 && (r2 = (e2[n2 - 2] << 8) + e2[n2 - 1], h2 += t[r2 >> 10], h2 += t[r2 >> 4 & 63], h2 += t[r2 << 2 & 63], h2 += "="), l2.push(h2), l2.join("");
}
function h(e2, t2, r2, n2, i2) {
  var o2, a2, s2 = 8 * i2 - n2 - 1, h2 = (1 << s2) - 1, l2 = h2 >> 1, f2 = -7, c2 = r2 ? i2 - 1 : 0, u2 = r2 ? -1 : 1, d2 = e2[t2 + c2];
  for (c2 += u2, o2 = d2 & (1 << -f2) - 1, d2 >>= -f2, f2 += s2; f2 > 0; o2 = 256 * o2 + e2[t2 + c2], c2 += u2, f2 -= 8)
    ;
  for (a2 = o2 & (1 << -f2) - 1, o2 >>= -f2, f2 += n2; f2 > 0; a2 = 256 * a2 + e2[t2 + c2], c2 += u2, f2 -= 8)
    ;
  if (0 === o2)
    o2 = 1 - l2;
  else {
    if (o2 === h2)
      return a2 ? NaN : 1 / 0 * (d2 ? -1 : 1);
    a2 += Math.pow(2, n2), o2 -= l2;
  }
  return (d2 ? -1 : 1) * a2 * Math.pow(2, o2 - n2);
}
function l(e2, t2, r2, n2, i2, o2) {
  var a2, s2, h2, l2 = 8 * o2 - i2 - 1, f2 = (1 << l2) - 1, c2 = f2 >> 1, u2 = 23 === i2 ? Math.pow(2, -24) - Math.pow(2, -77) : 0, d2 = n2 ? 0 : o2 - 1, p2 = n2 ? 1 : -1, _2 = t2 < 0 || 0 === t2 && 1 / t2 < 0 ? 1 : 0;
  for (t2 = Math.abs(t2), isNaN(t2) || t2 === 1 / 0 ? (s2 = isNaN(t2) ? 1 : 0, a2 = f2) : (a2 = Math.floor(Math.log(t2) / Math.LN2), t2 * (h2 = Math.pow(2, -a2)) < 1 && (a2--, h2 *= 2), (t2 += a2 + c2 >= 1 ? u2 / h2 : u2 * Math.pow(2, 1 - c2)) * h2 >= 2 && (a2++, h2 /= 2), a2 + c2 >= f2 ? (s2 = 0, a2 = f2) : a2 + c2 >= 1 ? (s2 = (t2 * h2 - 1) * Math.pow(2, i2), a2 += c2) : (s2 = t2 * Math.pow(2, c2 - 1) * Math.pow(2, i2), a2 = 0)); i2 >= 8; e2[r2 + d2] = 255 & s2, d2 += p2, s2 /= 256, i2 -= 8)
    ;
  for (a2 = a2 << i2 | s2, l2 += i2; l2 > 0; e2[r2 + d2] = 255 & a2, d2 += p2, a2 /= 256, l2 -= 8)
    ;
  e2[r2 + d2 - p2] |= 128 * _2;
}
var f = {}.toString, c = Array.isArray || function(e2) {
  return "[object Array]" == f.call(e2);
};
function u() {
  return p.TYPED_ARRAY_SUPPORT ? 2147483647 : 1073741823;
}
function d(e2, t2) {
  if (u() < t2)
    throw new RangeError("Invalid typed array length");
  return p.TYPED_ARRAY_SUPPORT ? (e2 = new Uint8Array(t2)).__proto__ = p.prototype : (null === e2 && (e2 = new p(t2)), e2.length = t2), e2;
}
function p(e2, t2, r2) {
  if (!(p.TYPED_ARRAY_SUPPORT || this instanceof p))
    return new p(e2, t2, r2);
  if ("number" == typeof e2) {
    if ("string" == typeof t2)
      throw new Error("If encoding is specified then the first argument must be a string");
    return v(this, e2);
  }
  return _(this, e2, t2, r2);
}
function _(e2, t2, r2, n2) {
  if ("number" == typeof t2)
    throw new TypeError('"value" argument must not be a number');
  return "undefined" != typeof ArrayBuffer && t2 instanceof ArrayBuffer ? function(e3, t3, r3, n3) {
    if (t3.byteLength, r3 < 0 || t3.byteLength < r3)
      throw new RangeError("'offset' is out of bounds");
    if (t3.byteLength < r3 + (n3 || 0))
      throw new RangeError("'length' is out of bounds");
    t3 = void 0 === r3 && void 0 === n3 ? new Uint8Array(t3) : void 0 === n3 ? new Uint8Array(t3, r3) : new Uint8Array(t3, r3, n3);
    p.TYPED_ARRAY_SUPPORT ? (e3 = t3).__proto__ = p.prototype : e3 = w(e3, t3);
    return e3;
  }(e2, t2, r2, n2) : "string" == typeof t2 ? function(e3, t3, r3) {
    "string" == typeof r3 && "" !== r3 || (r3 = "utf8");
    if (!p.isEncoding(r3))
      throw new TypeError('"encoding" must be a valid string encoding');
    var n3 = 0 | m(t3, r3), i2 = (e3 = d(e3, n3)).write(t3, r3);
    i2 !== n3 && (e3 = e3.slice(0, i2));
    return e3;
  }(e2, t2, r2) : function(e3, t3) {
    if (y(t3)) {
      var r3 = 0 | b(t3.length);
      return 0 === (e3 = d(e3, r3)).length ? e3 : (t3.copy(e3, 0, 0, r3), e3);
    }
    if (t3) {
      if ("undefined" != typeof ArrayBuffer && t3.buffer instanceof ArrayBuffer || "length" in t3)
        return "number" != typeof t3.length || (n3 = t3.length) != n3 ? d(e3, 0) : w(e3, t3);
      if ("Buffer" === t3.type && c(t3.data))
        return w(e3, t3.data);
    }
    var n3;
    throw new TypeError("First argument must be a string, Buffer, ArrayBuffer, Array, or array-like object.");
  }(e2, t2);
}
function g(e2) {
  if ("number" != typeof e2)
    throw new TypeError('"size" argument must be a number');
  if (e2 < 0)
    throw new RangeError('"size" argument must not be negative');
}
function v(e2, t2) {
  if (g(t2), e2 = d(e2, t2 < 0 ? 0 : 0 | b(t2)), !p.TYPED_ARRAY_SUPPORT)
    for (var r2 = 0; r2 < t2; ++r2)
      e2[r2] = 0;
  return e2;
}
function w(e2, t2) {
  var r2 = t2.length < 0 ? 0 : 0 | b(t2.length);
  e2 = d(e2, r2);
  for (var n2 = 0; n2 < r2; n2 += 1)
    e2[n2] = 255 & t2[n2];
  return e2;
}
function b(e2) {
  if (e2 >= u())
    throw new RangeError("Attempt to allocate Buffer larger than maximum size: 0x" + u().toString(16) + " bytes");
  return 0 | e2;
}
function y(e2) {
  return !(null == e2 || !e2._isBuffer);
}
function m(e2, t2) {
  if (y(e2))
    return e2.length;
  if ("undefined" != typeof ArrayBuffer && "function" == typeof ArrayBuffer.isView && (ArrayBuffer.isView(e2) || e2 instanceof ArrayBuffer))
    return e2.byteLength;
  "string" != typeof e2 && (e2 = "" + e2);
  var r2 = e2.length;
  if (0 === r2)
    return 0;
  for (var n2 = false; ; )
    switch (t2) {
      case "ascii":
      case "latin1":
      case "binary":
        return r2;
      case "utf8":
      case "utf-8":
      case void 0:
        return q(e2).length;
      case "ucs2":
      case "ucs-2":
      case "utf16le":
      case "utf-16le":
        return 2 * r2;
      case "hex":
        return r2 >>> 1;
      case "base64":
        return V(e2).length;
      default:
        if (n2)
          return q(e2).length;
        t2 = ("" + t2).toLowerCase(), n2 = true;
    }
}
function k(e2, t2, r2) {
  var n2 = false;
  if ((void 0 === t2 || t2 < 0) && (t2 = 0), t2 > this.length)
    return "";
  if ((void 0 === r2 || r2 > this.length) && (r2 = this.length), r2 <= 0)
    return "";
  if ((r2 >>>= 0) <= (t2 >>>= 0))
    return "";
  for (e2 || (e2 = "utf8"); ; )
    switch (e2) {
      case "hex":
        return O(this, t2, r2);
      case "utf8":
      case "utf-8":
        return C(this, t2, r2);
      case "ascii":
        return I(this, t2, r2);
      case "latin1":
      case "binary":
        return P(this, t2, r2);
      case "base64":
        return M(this, t2, r2);
      case "ucs2":
      case "ucs-2":
      case "utf16le":
      case "utf-16le":
        return U(this, t2, r2);
      default:
        if (n2)
          throw new TypeError("Unknown encoding: " + e2);
        e2 = (e2 + "").toLowerCase(), n2 = true;
    }
}
function E(e2, t2, r2) {
  var n2 = e2[t2];
  e2[t2] = e2[r2], e2[r2] = n2;
}
function S(e2, t2, r2, n2, i2) {
  if (0 === e2.length)
    return -1;
  if ("string" == typeof r2 ? (n2 = r2, r2 = 0) : r2 > 2147483647 ? r2 = 2147483647 : r2 < -2147483648 && (r2 = -2147483648), r2 = +r2, isNaN(r2) && (r2 = i2 ? 0 : e2.length - 1), r2 < 0 && (r2 = e2.length + r2), r2 >= e2.length) {
    if (i2)
      return -1;
    r2 = e2.length - 1;
  } else if (r2 < 0) {
    if (!i2)
      return -1;
    r2 = 0;
  }
  if ("string" == typeof t2 && (t2 = p.from(t2, n2)), y(t2))
    return 0 === t2.length ? -1 : x(e2, t2, r2, n2, i2);
  if ("number" == typeof t2)
    return t2 &= 255, p.TYPED_ARRAY_SUPPORT && "function" == typeof Uint8Array.prototype.indexOf ? i2 ? Uint8Array.prototype.indexOf.call(e2, t2, r2) : Uint8Array.prototype.lastIndexOf.call(e2, t2, r2) : x(e2, [t2], r2, n2, i2);
  throw new TypeError("val must be string, number or Buffer");
}
function x(e2, t2, r2, n2, i2) {
  var o2, a2 = 1, s2 = e2.length, h2 = t2.length;
  if (void 0 !== n2 && ("ucs2" === (n2 = String(n2).toLowerCase()) || "ucs-2" === n2 || "utf16le" === n2 || "utf-16le" === n2)) {
    if (e2.length < 2 || t2.length < 2)
      return -1;
    a2 = 2, s2 /= 2, h2 /= 2, r2 /= 2;
  }
  function l2(e3, t3) {
    return 1 === a2 ? e3[t3] : e3.readUInt16BE(t3 * a2);
  }
  if (i2) {
    var f2 = -1;
    for (o2 = r2; o2 < s2; o2++)
      if (l2(e2, o2) === l2(t2, -1 === f2 ? 0 : o2 - f2)) {
        if (-1 === f2 && (f2 = o2), o2 - f2 + 1 === h2)
          return f2 * a2;
      } else
        -1 !== f2 && (o2 -= o2 - f2), f2 = -1;
  } else
    for (r2 + h2 > s2 && (r2 = s2 - h2), o2 = r2; o2 >= 0; o2--) {
      for (var c2 = true, u2 = 0; u2 < h2; u2++)
        if (l2(e2, o2 + u2) !== l2(t2, u2)) {
          c2 = false;
          break;
        }
      if (c2)
        return o2;
    }
  return -1;
}
function R(e2, t2, r2, n2) {
  r2 = Number(r2) || 0;
  var i2 = e2.length - r2;
  n2 ? (n2 = Number(n2)) > i2 && (n2 = i2) : n2 = i2;
  var o2 = t2.length;
  if (o2 % 2 != 0)
    throw new TypeError("Invalid hex string");
  n2 > o2 / 2 && (n2 = o2 / 2);
  for (var a2 = 0; a2 < n2; ++a2) {
    var s2 = parseInt(t2.substr(2 * a2, 2), 16);
    if (isNaN(s2))
      return a2;
    e2[r2 + a2] = s2;
  }
  return a2;
}
function A(e2, t2, r2, n2) {
  return G(q(t2, e2.length - r2), e2, r2, n2);
}
function B(e2, t2, r2, n2) {
  return G(function(e3) {
    for (var t3 = [], r3 = 0; r3 < e3.length; ++r3)
      t3.push(255 & e3.charCodeAt(r3));
    return t3;
  }(t2), e2, r2, n2);
}
function z(e2, t2, r2, n2) {
  return B(e2, t2, r2, n2);
}
function L(e2, t2, r2, n2) {
  return G(V(t2), e2, r2, n2);
}
function T(e2, t2, r2, n2) {
  return G(function(e3, t3) {
    for (var r3, n3, i2, o2 = [], a2 = 0; a2 < e3.length && !((t3 -= 2) < 0); ++a2)
      r3 = e3.charCodeAt(a2), n3 = r3 >> 8, i2 = r3 % 256, o2.push(i2), o2.push(n3);
    return o2;
  }(t2, e2.length - r2), e2, r2, n2);
}
function M(e2, t2, r2) {
  return 0 === t2 && r2 === e2.length ? s(e2) : s(e2.slice(t2, r2));
}
function C(e2, t2, r2) {
  r2 = Math.min(e2.length, r2);
  for (var n2 = [], i2 = t2; i2 < r2; ) {
    var o2, a2, s2, h2, l2 = e2[i2], f2 = null, c2 = l2 > 239 ? 4 : l2 > 223 ? 3 : l2 > 191 ? 2 : 1;
    if (i2 + c2 <= r2)
      switch (c2) {
        case 1:
          l2 < 128 && (f2 = l2);
          break;
        case 2:
          128 == (192 & (o2 = e2[i2 + 1])) && (h2 = (31 & l2) << 6 | 63 & o2) > 127 && (f2 = h2);
          break;
        case 3:
          o2 = e2[i2 + 1], a2 = e2[i2 + 2], 128 == (192 & o2) && 128 == (192 & a2) && (h2 = (15 & l2) << 12 | (63 & o2) << 6 | 63 & a2) > 2047 && (h2 < 55296 || h2 > 57343) && (f2 = h2);
          break;
        case 4:
          o2 = e2[i2 + 1], a2 = e2[i2 + 2], s2 = e2[i2 + 3], 128 == (192 & o2) && 128 == (192 & a2) && 128 == (192 & s2) && (h2 = (15 & l2) << 18 | (63 & o2) << 12 | (63 & a2) << 6 | 63 & s2) > 65535 && h2 < 1114112 && (f2 = h2);
      }
    null === f2 ? (f2 = 65533, c2 = 1) : f2 > 65535 && (f2 -= 65536, n2.push(f2 >>> 10 & 1023 | 55296), f2 = 56320 | 1023 & f2), n2.push(f2), i2 += c2;
  }
  return function(e3) {
    var t3 = e3.length;
    if (t3 <= D)
      return String.fromCharCode.apply(String, e3);
    var r3 = "", n3 = 0;
    for (; n3 < t3; )
      r3 += String.fromCharCode.apply(String, e3.slice(n3, n3 += D));
    return r3;
  }(n2);
}
p.TYPED_ARRAY_SUPPORT = void 0 === e.TYPED_ARRAY_SUPPORT || e.TYPED_ARRAY_SUPPORT, p.poolSize = 8192, p._augment = function(e2) {
  return e2.__proto__ = p.prototype, e2;
}, p.from = function(e2, t2, r2) {
  return _(null, e2, t2, r2);
}, p.TYPED_ARRAY_SUPPORT && (p.prototype.__proto__ = Uint8Array.prototype, p.__proto__ = Uint8Array), p.alloc = function(e2, t2, r2) {
  return function(e3, t3, r3, n2) {
    return g(t3), t3 <= 0 ? d(e3, t3) : void 0 !== r3 ? "string" == typeof n2 ? d(e3, t3).fill(r3, n2) : d(e3, t3).fill(r3) : d(e3, t3);
  }(null, e2, t2, r2);
}, p.allocUnsafe = function(e2) {
  return v(null, e2);
}, p.allocUnsafeSlow = function(e2) {
  return v(null, e2);
}, p.isBuffer = $, p.compare = function(e2, t2) {
  if (!y(e2) || !y(t2))
    throw new TypeError("Arguments must be Buffers");
  if (e2 === t2)
    return 0;
  for (var r2 = e2.length, n2 = t2.length, i2 = 0, o2 = Math.min(r2, n2); i2 < o2; ++i2)
    if (e2[i2] !== t2[i2]) {
      r2 = e2[i2], n2 = t2[i2];
      break;
    }
  return r2 < n2 ? -1 : n2 < r2 ? 1 : 0;
}, p.isEncoding = function(e2) {
  switch (String(e2).toLowerCase()) {
    case "hex":
    case "utf8":
    case "utf-8":
    case "ascii":
    case "latin1":
    case "binary":
    case "base64":
    case "ucs2":
    case "ucs-2":
    case "utf16le":
    case "utf-16le":
      return true;
    default:
      return false;
  }
}, p.concat = function(e2, t2) {
  if (!c(e2))
    throw new TypeError('"list" argument must be an Array of Buffers');
  if (0 === e2.length)
    return p.alloc(0);
  var r2;
  if (void 0 === t2)
    for (t2 = 0, r2 = 0; r2 < e2.length; ++r2)
      t2 += e2[r2].length;
  var n2 = p.allocUnsafe(t2), i2 = 0;
  for (r2 = 0; r2 < e2.length; ++r2) {
    var o2 = e2[r2];
    if (!y(o2))
      throw new TypeError('"list" argument must be an Array of Buffers');
    o2.copy(n2, i2), i2 += o2.length;
  }
  return n2;
}, p.byteLength = m, p.prototype._isBuffer = true, p.prototype.swap16 = function() {
  var e2 = this.length;
  if (e2 % 2 != 0)
    throw new RangeError("Buffer size must be a multiple of 16-bits");
  for (var t2 = 0; t2 < e2; t2 += 2)
    E(this, t2, t2 + 1);
  return this;
}, p.prototype.swap32 = function() {
  var e2 = this.length;
  if (e2 % 4 != 0)
    throw new RangeError("Buffer size must be a multiple of 32-bits");
  for (var t2 = 0; t2 < e2; t2 += 4)
    E(this, t2, t2 + 3), E(this, t2 + 1, t2 + 2);
  return this;
}, p.prototype.swap64 = function() {
  var e2 = this.length;
  if (e2 % 8 != 0)
    throw new RangeError("Buffer size must be a multiple of 64-bits");
  for (var t2 = 0; t2 < e2; t2 += 8)
    E(this, t2, t2 + 7), E(this, t2 + 1, t2 + 6), E(this, t2 + 2, t2 + 5), E(this, t2 + 3, t2 + 4);
  return this;
}, p.prototype.toString = function() {
  var e2 = 0 | this.length;
  return 0 === e2 ? "" : 0 === arguments.length ? C(this, 0, e2) : k.apply(this, arguments);
}, p.prototype.equals = function(e2) {
  if (!y(e2))
    throw new TypeError("Argument must be a Buffer");
  return this === e2 || 0 === p.compare(this, e2);
}, p.prototype.inspect = function() {
  var e2 = "";
  return this.length > 0 && (e2 = this.toString("hex", 0, 50).match(/.{2}/g).join(" "), this.length > 50 && (e2 += " ... ")), "<Buffer " + e2 + ">";
}, p.prototype.compare = function(e2, t2, r2, n2, i2) {
  if (!y(e2))
    throw new TypeError("Argument must be a Buffer");
  if (void 0 === t2 && (t2 = 0), void 0 === r2 && (r2 = e2 ? e2.length : 0), void 0 === n2 && (n2 = 0), void 0 === i2 && (i2 = this.length), t2 < 0 || r2 > e2.length || n2 < 0 || i2 > this.length)
    throw new RangeError("out of range index");
  if (n2 >= i2 && t2 >= r2)
    return 0;
  if (n2 >= i2)
    return -1;
  if (t2 >= r2)
    return 1;
  if (this === e2)
    return 0;
  for (var o2 = (i2 >>>= 0) - (n2 >>>= 0), a2 = (r2 >>>= 0) - (t2 >>>= 0), s2 = Math.min(o2, a2), h2 = this.slice(n2, i2), l2 = e2.slice(t2, r2), f2 = 0; f2 < s2; ++f2)
    if (h2[f2] !== l2[f2]) {
      o2 = h2[f2], a2 = l2[f2];
      break;
    }
  return o2 < a2 ? -1 : a2 < o2 ? 1 : 0;
}, p.prototype.includes = function(e2, t2, r2) {
  return -1 !== this.indexOf(e2, t2, r2);
}, p.prototype.indexOf = function(e2, t2, r2) {
  return S(this, e2, t2, r2, true);
}, p.prototype.lastIndexOf = function(e2, t2, r2) {
  return S(this, e2, t2, r2, false);
}, p.prototype.write = function(e2, t2, r2, n2) {
  if (void 0 === t2)
    n2 = "utf8", r2 = this.length, t2 = 0;
  else if (void 0 === r2 && "string" == typeof t2)
    n2 = t2, r2 = this.length, t2 = 0;
  else {
    if (!isFinite(t2))
      throw new Error("Buffer.write(string, encoding, offset[, length]) is no longer supported");
    t2 |= 0, isFinite(r2) ? (r2 |= 0, void 0 === n2 && (n2 = "utf8")) : (n2 = r2, r2 = void 0);
  }
  var i2 = this.length - t2;
  if ((void 0 === r2 || r2 > i2) && (r2 = i2), e2.length > 0 && (r2 < 0 || t2 < 0) || t2 > this.length)
    throw new RangeError("Attempt to write outside buffer bounds");
  n2 || (n2 = "utf8");
  for (var o2 = false; ; )
    switch (n2) {
      case "hex":
        return R(this, e2, t2, r2);
      case "utf8":
      case "utf-8":
        return A(this, e2, t2, r2);
      case "ascii":
        return B(this, e2, t2, r2);
      case "latin1":
      case "binary":
        return z(this, e2, t2, r2);
      case "base64":
        return L(this, e2, t2, r2);
      case "ucs2":
      case "ucs-2":
      case "utf16le":
      case "utf-16le":
        return T(this, e2, t2, r2);
      default:
        if (o2)
          throw new TypeError("Unknown encoding: " + n2);
        n2 = ("" + n2).toLowerCase(), o2 = true;
    }
}, p.prototype.toJSON = function() {
  return {
    type: "Buffer",
    data: Array.prototype.slice.call(this._arr || this, 0)
  };
};
var D = 4096;
function I(e2, t2, r2) {
  var n2 = "";
  r2 = Math.min(e2.length, r2);
  for (var i2 = t2; i2 < r2; ++i2)
    n2 += String.fromCharCode(127 & e2[i2]);
  return n2;
}
function P(e2, t2, r2) {
  var n2 = "";
  r2 = Math.min(e2.length, r2);
  for (var i2 = t2; i2 < r2; ++i2)
    n2 += String.fromCharCode(e2[i2]);
  return n2;
}
function O(e2, t2, r2) {
  var n2 = e2.length;
  (!t2 || t2 < 0) && (t2 = 0), (!r2 || r2 < 0 || r2 > n2) && (r2 = n2);
  for (var i2 = "", o2 = t2; o2 < r2; ++o2)
    i2 += X(e2[o2]);
  return i2;
}
function U(e2, t2, r2) {
  for (var n2 = e2.slice(t2, r2), i2 = "", o2 = 0; o2 < n2.length; o2 += 2)
    i2 += String.fromCharCode(n2[o2] + 256 * n2[o2 + 1]);
  return i2;
}
function H(e2, t2, r2) {
  if (e2 % 1 != 0 || e2 < 0)
    throw new RangeError("offset is not uint");
  if (e2 + t2 > r2)
    throw new RangeError("Trying to access beyond buffer length");
}
function F(e2, t2, r2, n2, i2, o2) {
  if (!y(e2))
    throw new TypeError('"buffer" argument must be a Buffer instance');
  if (t2 > i2 || t2 < o2)
    throw new RangeError('"value" argument is out of bounds');
  if (r2 + n2 > e2.length)
    throw new RangeError("Index out of range");
}
function N(e2, t2, r2, n2) {
  t2 < 0 && (t2 = 65535 + t2 + 1);
  for (var i2 = 0, o2 = Math.min(e2.length - r2, 2); i2 < o2; ++i2)
    e2[r2 + i2] = (t2 & 255 << 8 * (n2 ? i2 : 1 - i2)) >>> 8 * (n2 ? i2 : 1 - i2);
}
function Z(e2, t2, r2, n2) {
  t2 < 0 && (t2 = 4294967295 + t2 + 1);
  for (var i2 = 0, o2 = Math.min(e2.length - r2, 4); i2 < o2; ++i2)
    e2[r2 + i2] = t2 >>> 8 * (n2 ? i2 : 3 - i2) & 255;
}
function j(e2, t2, r2, n2, i2, o2) {
  if (r2 + n2 > e2.length)
    throw new RangeError("Index out of range");
  if (r2 < 0)
    throw new RangeError("Index out of range");
}
function W(e2, t2, r2, n2, i2) {
  return i2 || j(e2, 0, r2, 4), l(e2, t2, r2, n2, 23, 4), r2 + 4;
}
function Y(e2, t2, r2, n2, i2) {
  return i2 || j(e2, 0, r2, 8), l(e2, t2, r2, n2, 52, 8), r2 + 8;
}
p.prototype.slice = function(e2, t2) {
  var r2, n2 = this.length;
  if ((e2 = ~~e2) < 0 ? (e2 += n2) < 0 && (e2 = 0) : e2 > n2 && (e2 = n2), (t2 = void 0 === t2 ? n2 : ~~t2) < 0 ? (t2 += n2) < 0 && (t2 = 0) : t2 > n2 && (t2 = n2), t2 < e2 && (t2 = e2), p.TYPED_ARRAY_SUPPORT)
    (r2 = this.subarray(e2, t2)).__proto__ = p.prototype;
  else {
    var i2 = t2 - e2;
    r2 = new p(i2, void 0);
    for (var o2 = 0; o2 < i2; ++o2)
      r2[o2] = this[o2 + e2];
  }
  return r2;
}, p.prototype.readUIntLE = function(e2, t2, r2) {
  e2 |= 0, t2 |= 0, r2 || H(e2, t2, this.length);
  for (var n2 = this[e2], i2 = 1, o2 = 0; ++o2 < t2 && (i2 *= 256); )
    n2 += this[e2 + o2] * i2;
  return n2;
}, p.prototype.readUIntBE = function(e2, t2, r2) {
  e2 |= 0, t2 |= 0, r2 || H(e2, t2, this.length);
  for (var n2 = this[e2 + --t2], i2 = 1; t2 > 0 && (i2 *= 256); )
    n2 += this[e2 + --t2] * i2;
  return n2;
}, p.prototype.readUInt8 = function(e2, t2) {
  return t2 || H(e2, 1, this.length), this[e2];
}, p.prototype.readUInt16LE = function(e2, t2) {
  return t2 || H(e2, 2, this.length), this[e2] | this[e2 + 1] << 8;
}, p.prototype.readUInt16BE = function(e2, t2) {
  return t2 || H(e2, 2, this.length), this[e2] << 8 | this[e2 + 1];
}, p.prototype.readUInt32LE = function(e2, t2) {
  return t2 || H(e2, 4, this.length), (this[e2] | this[e2 + 1] << 8 | this[e2 + 2] << 16) + 16777216 * this[e2 + 3];
}, p.prototype.readUInt32BE = function(e2, t2) {
  return t2 || H(e2, 4, this.length), 16777216 * this[e2] + (this[e2 + 1] << 16 | this[e2 + 2] << 8 | this[e2 + 3]);
}, p.prototype.readIntLE = function(e2, t2, r2) {
  e2 |= 0, t2 |= 0, r2 || H(e2, t2, this.length);
  for (var n2 = this[e2], i2 = 1, o2 = 0; ++o2 < t2 && (i2 *= 256); )
    n2 += this[e2 + o2] * i2;
  return n2 >= (i2 *= 128) && (n2 -= Math.pow(2, 8 * t2)), n2;
}, p.prototype.readIntBE = function(e2, t2, r2) {
  e2 |= 0, t2 |= 0, r2 || H(e2, t2, this.length);
  for (var n2 = t2, i2 = 1, o2 = this[e2 + --n2]; n2 > 0 && (i2 *= 256); )
    o2 += this[e2 + --n2] * i2;
  return o2 >= (i2 *= 128) && (o2 -= Math.pow(2, 8 * t2)), o2;
}, p.prototype.readInt8 = function(e2, t2) {
  return t2 || H(e2, 1, this.length), 128 & this[e2] ? -1 * (255 - this[e2] + 1) : this[e2];
}, p.prototype.readInt16LE = function(e2, t2) {
  t2 || H(e2, 2, this.length);
  var r2 = this[e2] | this[e2 + 1] << 8;
  return 32768 & r2 ? 4294901760 | r2 : r2;
}, p.prototype.readInt16BE = function(e2, t2) {
  t2 || H(e2, 2, this.length);
  var r2 = this[e2 + 1] | this[e2] << 8;
  return 32768 & r2 ? 4294901760 | r2 : r2;
}, p.prototype.readInt32LE = function(e2, t2) {
  return t2 || H(e2, 4, this.length), this[e2] | this[e2 + 1] << 8 | this[e2 + 2] << 16 | this[e2 + 3] << 24;
}, p.prototype.readInt32BE = function(e2, t2) {
  return t2 || H(e2, 4, this.length), this[e2] << 24 | this[e2 + 1] << 16 | this[e2 + 2] << 8 | this[e2 + 3];
}, p.prototype.readFloatLE = function(e2, t2) {
  return t2 || H(e2, 4, this.length), h(this, e2, true, 23, 4);
}, p.prototype.readFloatBE = function(e2, t2) {
  return t2 || H(e2, 4, this.length), h(this, e2, false, 23, 4);
}, p.prototype.readDoubleLE = function(e2, t2) {
  return t2 || H(e2, 8, this.length), h(this, e2, true, 52, 8);
}, p.prototype.readDoubleBE = function(e2, t2) {
  return t2 || H(e2, 8, this.length), h(this, e2, false, 52, 8);
}, p.prototype.writeUIntLE = function(e2, t2, r2, n2) {
  (e2 = +e2, t2 |= 0, r2 |= 0, n2) || F(this, e2, t2, r2, Math.pow(2, 8 * r2) - 1, 0);
  var i2 = 1, o2 = 0;
  for (this[t2] = 255 & e2; ++o2 < r2 && (i2 *= 256); )
    this[t2 + o2] = e2 / i2 & 255;
  return t2 + r2;
}, p.prototype.writeUIntBE = function(e2, t2, r2, n2) {
  (e2 = +e2, t2 |= 0, r2 |= 0, n2) || F(this, e2, t2, r2, Math.pow(2, 8 * r2) - 1, 0);
  var i2 = r2 - 1, o2 = 1;
  for (this[t2 + i2] = 255 & e2; --i2 >= 0 && (o2 *= 256); )
    this[t2 + i2] = e2 / o2 & 255;
  return t2 + r2;
}, p.prototype.writeUInt8 = function(e2, t2, r2) {
  return e2 = +e2, t2 |= 0, r2 || F(this, e2, t2, 1, 255, 0), p.TYPED_ARRAY_SUPPORT || (e2 = Math.floor(e2)), this[t2] = 255 & e2, t2 + 1;
}, p.prototype.writeUInt16LE = function(e2, t2, r2) {
  return e2 = +e2, t2 |= 0, r2 || F(this, e2, t2, 2, 65535, 0), p.TYPED_ARRAY_SUPPORT ? (this[t2] = 255 & e2, this[t2 + 1] = e2 >>> 8) : N(this, e2, t2, true), t2 + 2;
}, p.prototype.writeUInt16BE = function(e2, t2, r2) {
  return e2 = +e2, t2 |= 0, r2 || F(this, e2, t2, 2, 65535, 0), p.TYPED_ARRAY_SUPPORT ? (this[t2] = e2 >>> 8, this[t2 + 1] = 255 & e2) : N(this, e2, t2, false), t2 + 2;
}, p.prototype.writeUInt32LE = function(e2, t2, r2) {
  return e2 = +e2, t2 |= 0, r2 || F(this, e2, t2, 4, 4294967295, 0), p.TYPED_ARRAY_SUPPORT ? (this[t2 + 3] = e2 >>> 24, this[t2 + 2] = e2 >>> 16, this[t2 + 1] = e2 >>> 8, this[t2] = 255 & e2) : Z(this, e2, t2, true), t2 + 4;
}, p.prototype.writeUInt32BE = function(e2, t2, r2) {
  return e2 = +e2, t2 |= 0, r2 || F(this, e2, t2, 4, 4294967295, 0), p.TYPED_ARRAY_SUPPORT ? (this[t2] = e2 >>> 24, this[t2 + 1] = e2 >>> 16, this[t2 + 2] = e2 >>> 8, this[t2 + 3] = 255 & e2) : Z(this, e2, t2, false), t2 + 4;
}, p.prototype.writeIntLE = function(e2, t2, r2, n2) {
  if (e2 = +e2, t2 |= 0, !n2) {
    var i2 = Math.pow(2, 8 * r2 - 1);
    F(this, e2, t2, r2, i2 - 1, -i2);
  }
  var o2 = 0, a2 = 1, s2 = 0;
  for (this[t2] = 255 & e2; ++o2 < r2 && (a2 *= 256); )
    e2 < 0 && 0 === s2 && 0 !== this[t2 + o2 - 1] && (s2 = 1), this[t2 + o2] = (e2 / a2 >> 0) - s2 & 255;
  return t2 + r2;
}, p.prototype.writeIntBE = function(e2, t2, r2, n2) {
  if (e2 = +e2, t2 |= 0, !n2) {
    var i2 = Math.pow(2, 8 * r2 - 1);
    F(this, e2, t2, r2, i2 - 1, -i2);
  }
  var o2 = r2 - 1, a2 = 1, s2 = 0;
  for (this[t2 + o2] = 255 & e2; --o2 >= 0 && (a2 *= 256); )
    e2 < 0 && 0 === s2 && 0 !== this[t2 + o2 + 1] && (s2 = 1), this[t2 + o2] = (e2 / a2 >> 0) - s2 & 255;
  return t2 + r2;
}, p.prototype.writeInt8 = function(e2, t2, r2) {
  return e2 = +e2, t2 |= 0, r2 || F(this, e2, t2, 1, 127, -128), p.TYPED_ARRAY_SUPPORT || (e2 = Math.floor(e2)), e2 < 0 && (e2 = 255 + e2 + 1), this[t2] = 255 & e2, t2 + 1;
}, p.prototype.writeInt16LE = function(e2, t2, r2) {
  return e2 = +e2, t2 |= 0, r2 || F(this, e2, t2, 2, 32767, -32768), p.TYPED_ARRAY_SUPPORT ? (this[t2] = 255 & e2, this[t2 + 1] = e2 >>> 8) : N(this, e2, t2, true), t2 + 2;
}, p.prototype.writeInt16BE = function(e2, t2, r2) {
  return e2 = +e2, t2 |= 0, r2 || F(this, e2, t2, 2, 32767, -32768), p.TYPED_ARRAY_SUPPORT ? (this[t2] = e2 >>> 8, this[t2 + 1] = 255 & e2) : N(this, e2, t2, false), t2 + 2;
}, p.prototype.writeInt32LE = function(e2, t2, r2) {
  return e2 = +e2, t2 |= 0, r2 || F(this, e2, t2, 4, 2147483647, -2147483648), p.TYPED_ARRAY_SUPPORT ? (this[t2] = 255 & e2, this[t2 + 1] = e2 >>> 8, this[t2 + 2] = e2 >>> 16, this[t2 + 3] = e2 >>> 24) : Z(this, e2, t2, true), t2 + 4;
}, p.prototype.writeInt32BE = function(e2, t2, r2) {
  return e2 = +e2, t2 |= 0, r2 || F(this, e2, t2, 4, 2147483647, -2147483648), e2 < 0 && (e2 = 4294967295 + e2 + 1), p.TYPED_ARRAY_SUPPORT ? (this[t2] = e2 >>> 24, this[t2 + 1] = e2 >>> 16, this[t2 + 2] = e2 >>> 8, this[t2 + 3] = 255 & e2) : Z(this, e2, t2, false), t2 + 4;
}, p.prototype.writeFloatLE = function(e2, t2, r2) {
  return W(this, e2, t2, true, r2);
}, p.prototype.writeFloatBE = function(e2, t2, r2) {
  return W(this, e2, t2, false, r2);
}, p.prototype.writeDoubleLE = function(e2, t2, r2) {
  return Y(this, e2, t2, true, r2);
}, p.prototype.writeDoubleBE = function(e2, t2, r2) {
  return Y(this, e2, t2, false, r2);
}, p.prototype.copy = function(e2, t2, r2, n2) {
  if (r2 || (r2 = 0), n2 || 0 === n2 || (n2 = this.length), t2 >= e2.length && (t2 = e2.length), t2 || (t2 = 0), n2 > 0 && n2 < r2 && (n2 = r2), n2 === r2)
    return 0;
  if (0 === e2.length || 0 === this.length)
    return 0;
  if (t2 < 0)
    throw new RangeError("targetStart out of bounds");
  if (r2 < 0 || r2 >= this.length)
    throw new RangeError("sourceStart out of bounds");
  if (n2 < 0)
    throw new RangeError("sourceEnd out of bounds");
  n2 > this.length && (n2 = this.length), e2.length - t2 < n2 - r2 && (n2 = e2.length - t2 + r2);
  var i2, o2 = n2 - r2;
  if (this === e2 && r2 < t2 && t2 < n2)
    for (i2 = o2 - 1; i2 >= 0; --i2)
      e2[i2 + t2] = this[i2 + r2];
  else if (o2 < 1e3 || !p.TYPED_ARRAY_SUPPORT)
    for (i2 = 0; i2 < o2; ++i2)
      e2[i2 + t2] = this[i2 + r2];
  else
    Uint8Array.prototype.set.call(e2, this.subarray(r2, r2 + o2), t2);
  return o2;
}, p.prototype.fill = function(e2, t2, r2, n2) {
  if ("string" == typeof e2) {
    if ("string" == typeof t2 ? (n2 = t2, t2 = 0, r2 = this.length) : "string" == typeof r2 && (n2 = r2, r2 = this.length), 1 === e2.length) {
      var i2 = e2.charCodeAt(0);
      i2 < 256 && (e2 = i2);
    }
    if (void 0 !== n2 && "string" != typeof n2)
      throw new TypeError("encoding must be a string");
    if ("string" == typeof n2 && !p.isEncoding(n2))
      throw new TypeError("Unknown encoding: " + n2);
  } else
    "number" == typeof e2 && (e2 &= 255);
  if (t2 < 0 || this.length < t2 || this.length < r2)
    throw new RangeError("Out of range index");
  if (r2 <= t2)
    return this;
  var o2;
  if (t2 >>>= 0, r2 = void 0 === r2 ? this.length : r2 >>> 0, e2 || (e2 = 0), "number" == typeof e2)
    for (o2 = t2; o2 < r2; ++o2)
      this[o2] = e2;
  else {
    var a2 = y(e2) ? e2 : q(new p(e2, n2).toString()), s2 = a2.length;
    for (o2 = 0; o2 < r2 - t2; ++o2)
      this[o2 + t2] = a2[o2 % s2];
  }
  return this;
};
var K = /[^+\/0-9A-Za-z-_]/g;
function X(e2) {
  return e2 < 16 ? "0" + e2.toString(16) : e2.toString(16);
}
function q(e2, t2) {
  var r2;
  t2 = t2 || 1 / 0;
  for (var n2 = e2.length, i2 = null, o2 = [], a2 = 0; a2 < n2; ++a2) {
    if ((r2 = e2.charCodeAt(a2)) > 55295 && r2 < 57344) {
      if (!i2) {
        if (r2 > 56319) {
          (t2 -= 3) > -1 && o2.push(239, 191, 189);
          continue;
        }
        if (a2 + 1 === n2) {
          (t2 -= 3) > -1 && o2.push(239, 191, 189);
          continue;
        }
        i2 = r2;
        continue;
      }
      if (r2 < 56320) {
        (t2 -= 3) > -1 && o2.push(239, 191, 189), i2 = r2;
        continue;
      }
      r2 = 65536 + (i2 - 55296 << 10 | r2 - 56320);
    } else
      i2 && (t2 -= 3) > -1 && o2.push(239, 191, 189);
    if (i2 = null, r2 < 128) {
      if ((t2 -= 1) < 0)
        break;
      o2.push(r2);
    } else if (r2 < 2048) {
      if ((t2 -= 2) < 0)
        break;
      o2.push(r2 >> 6 | 192, 63 & r2 | 128);
    } else if (r2 < 65536) {
      if ((t2 -= 3) < 0)
        break;
      o2.push(r2 >> 12 | 224, r2 >> 6 & 63 | 128, 63 & r2 | 128);
    } else {
      if (!(r2 < 1114112))
        throw new Error("Invalid code point");
      if ((t2 -= 4) < 0)
        break;
      o2.push(r2 >> 18 | 240, r2 >> 12 & 63 | 128, r2 >> 6 & 63 | 128, 63 & r2 | 128);
    }
  }
  return o2;
}
function V(e2) {
  return function(e3) {
    var t2, a2, s2, h2, l2, f2;
    i || o();
    var c2 = e3.length;
    if (c2 % 4 > 0)
      throw new Error("Invalid string. Length must be a multiple of 4");
    l2 = "=" === e3[c2 - 2] ? 2 : "=" === e3[c2 - 1] ? 1 : 0, f2 = new n(3 * c2 / 4 - l2), s2 = l2 > 0 ? c2 - 4 : c2;
    var u2 = 0;
    for (t2 = 0, a2 = 0; t2 < s2; t2 += 4, a2 += 3)
      h2 = r[e3.charCodeAt(t2)] << 18 | r[e3.charCodeAt(t2 + 1)] << 12 | r[e3.charCodeAt(t2 + 2)] << 6 | r[e3.charCodeAt(t2 + 3)], f2[u2++] = h2 >> 16 & 255, f2[u2++] = h2 >> 8 & 255, f2[u2++] = 255 & h2;
    return 2 === l2 ? (h2 = r[e3.charCodeAt(t2)] << 2 | r[e3.charCodeAt(t2 + 1)] >> 4, f2[u2++] = 255 & h2) : 1 === l2 && (h2 = r[e3.charCodeAt(t2)] << 10 | r[e3.charCodeAt(t2 + 1)] << 4 | r[e3.charCodeAt(t2 + 2)] >> 2, f2[u2++] = h2 >> 8 & 255, f2[u2++] = 255 & h2), f2;
  }(function(e3) {
    if ((e3 = function(e4) {
      return e4.trim ? e4.trim() : e4.replace(/^\s+|\s+$/g, "");
    }(e3).replace(K, "")).length < 2)
      return "";
    for (; e3.length % 4 != 0; )
      e3 += "=";
    return e3;
  }(e2));
}
function G(e2, t2, r2, n2) {
  for (var i2 = 0; i2 < n2 && !(i2 + r2 >= t2.length || i2 >= e2.length); ++i2)
    t2[i2 + r2] = e2[i2];
  return i2;
}
function $(e2) {
  return null != e2 && (!!e2._isBuffer || J(e2) || function(e3) {
    return "function" == typeof e3.readFloatLE && "function" == typeof e3.slice && J(e3.slice(0, 0));
  }(e2));
}
function J(e2) {
  return !!e2.constructor && "function" == typeof e2.constructor.isBuffer && e2.constructor.isBuffer(e2);
}
function Q(e2, t2) {
  return e2(t2 = {
    exports: {}
  }, t2.exports), t2.exports;
}
var ee = Q(function(e2, t2) {
  var r2;
  e2.exports = (r2 = r2 || function(e3, t3) {
    var r3 = Object.create || function() {
      function e4() {
      }
      return function(t4) {
        var r4;
        return e4.prototype = t4, r4 = new e4(), e4.prototype = null, r4;
      };
    }(), n2 = {}, i2 = n2.lib = {}, o2 = i2.Base = {
      extend: function(e4) {
        var t4 = r3(this);
        return e4 && t4.mixIn(e4), t4.hasOwnProperty("init") && this.init !== t4.init || (t4.init = function() {
          t4.$super.init.apply(this, arguments);
        }), t4.init.prototype = t4, t4.$super = this, t4;
      },
      create: function() {
        var e4 = this.extend();
        return e4.init.apply(e4, arguments), e4;
      },
      init: function() {
      },
      mixIn: function(e4) {
        for (var t4 in e4)
          e4.hasOwnProperty(t4) && (this[t4] = e4[t4]);
        e4.hasOwnProperty("toString") && (this.toString = e4.toString);
      },
      clone: function() {
        return this.init.prototype.extend(this);
      }
    }, a2 = i2.WordArray = o2.extend({
      init: function(e4, t4) {
        e4 = this.words = e4 || [], this.sigBytes = null != t4 ? t4 : 4 * e4.length;
      },
      toString: function(e4) {
        return (e4 || h2).stringify(this);
      },
      concat: function(e4) {
        var t4 = this.words, r4 = e4.words, n3 = this.sigBytes, i3 = e4.sigBytes;
        if (this.clamp(), n3 % 4)
          for (var o3 = 0; o3 < i3; o3++) {
            var a3 = r4[o3 >>> 2] >>> 24 - o3 % 4 * 8 & 255;
            t4[n3 + o3 >>> 2] |= a3 << 24 - (n3 + o3) % 4 * 8;
          }
        else
          for (var o3 = 0; o3 < i3; o3 += 4)
            t4[n3 + o3 >>> 2] = r4[o3 >>> 2];
        return this.sigBytes += i3, this;
      },
      clamp: function() {
        var t4 = this.words, r4 = this.sigBytes;
        t4[r4 >>> 2] &= 4294967295 << 32 - r4 % 4 * 8, t4.length = e3.ceil(r4 / 4);
      },
      clone: function() {
        var e4 = o2.clone.call(this);
        return e4.words = this.words.slice(0), e4;
      },
      random: function(t4) {
        for (var r4, n3 = [], i3 = function(t5) {
          var t5 = t5, r5 = 987654321, n4 = 4294967295;
          return function() {
            var i4 = ((r5 = 36969 * (65535 & r5) + (r5 >> 16) & n4) << 16) + (t5 = 18e3 * (65535 & t5) + (t5 >> 16) & n4) & n4;
            return i4 /= 4294967296, (i4 += 0.5) * (e3.random() > 0.5 ? 1 : -1);
          };
        }, o3 = 0; o3 < t4; o3 += 4) {
          var s3 = i3(4294967296 * (r4 || e3.random()));
          r4 = 987654071 * s3(), n3.push(4294967296 * s3() | 0);
        }
        return new a2.init(n3, t4);
      }
    }), s2 = n2.enc = {}, h2 = s2.Hex = {
      stringify: function(e4) {
        for (var t4 = e4.words, r4 = e4.sigBytes, n3 = [], i3 = 0; i3 < r4; i3++) {
          var o3 = t4[i3 >>> 2] >>> 24 - i3 % 4 * 8 & 255;
          n3.push((o3 >>> 4).toString(16)), n3.push((15 & o3).toString(16));
        }
        return n3.join("");
      },
      parse: function(e4) {
        for (var t4 = e4.length, r4 = [], n3 = 0; n3 < t4; n3 += 2)
          r4[n3 >>> 3] |= parseInt(e4.substr(n3, 2), 16) << 24 - n3 % 8 * 4;
        return new a2.init(r4, t4 / 2);
      }
    }, l2 = s2.Latin1 = {
      stringify: function(e4) {
        for (var t4 = e4.words, r4 = e4.sigBytes, n3 = [], i3 = 0; i3 < r4; i3++) {
          var o3 = t4[i3 >>> 2] >>> 24 - i3 % 4 * 8 & 255;
          n3.push(String.fromCharCode(o3));
        }
        return n3.join("");
      },
      parse: function(e4) {
        for (var t4 = e4.length, r4 = [], n3 = 0; n3 < t4; n3++)
          r4[n3 >>> 2] |= (255 & e4.charCodeAt(n3)) << 24 - n3 % 4 * 8;
        return new a2.init(r4, t4);
      }
    }, f2 = s2.Utf8 = {
      stringify: function(e4) {
        try {
          return decodeURIComponent(escape(l2.stringify(e4)));
        } catch (e5) {
          throw new Error("Malformed UTF-8 data");
        }
      },
      parse: function(e4) {
        return l2.parse(unescape(encodeURIComponent(e4)));
      }
    }, c2 = i2.BufferedBlockAlgorithm = o2.extend({
      reset: function() {
        this._data = new a2.init(), this._nDataBytes = 0;
      },
      _append: function(e4) {
        "string" == typeof e4 && (e4 = f2.parse(e4)), this._data.concat(e4), this._nDataBytes += e4.sigBytes;
      },
      _process: function(t4) {
        var r4 = this._data, n3 = r4.words, i3 = r4.sigBytes, o3 = this.blockSize, s3 = 4 * o3, h3 = i3 / s3, l3 = (h3 = t4 ? e3.ceil(h3) : e3.max((0 | h3) - this._minBufferSize, 0)) * o3, f3 = e3.min(4 * l3, i3);
        if (l3) {
          for (var c3 = 0; c3 < l3; c3 += o3)
            this._doProcessBlock(n3, c3);
          var u3 = n3.splice(0, l3);
          r4.sigBytes -= f3;
        }
        return new a2.init(u3, f3);
      },
      clone: function() {
        var e4 = o2.clone.call(this);
        return e4._data = this._data.clone(), e4;
      },
      _minBufferSize: 0
    }), u2 = (i2.Hasher = c2.extend({
      cfg: o2.extend(),
      init: function(e4) {
        this.cfg = this.cfg.extend(e4), this.reset();
      },
      reset: function() {
        c2.reset.call(this), this._doReset();
      },
      update: function(e4) {
        return this._append(e4), this._process(), this;
      },
      finalize: function(e4) {
        e4 && this._append(e4);
        var t4 = this._doFinalize();
        return t4;
      },
      blockSize: 16,
      _createHelper: function(e4) {
        return function(t4, r4) {
          return new e4.init(r4).finalize(t4);
        };
      },
      _createHmacHelper: function(e4) {
        return function(t4, r4) {
          return new u2.HMAC.init(e4, r4).finalize(t4);
        };
      }
    }), n2.algo = {});
    return n2;
  }(Math), r2);
}), te = (Q(function(e2, t2) {
  var r2, n2, i2, o2, a2, s2;
  e2.exports = (i2 = (n2 = r2 = ee).lib, o2 = i2.Base, a2 = i2.WordArray, (s2 = n2.x64 = {}).Word = o2.extend({
    init: function(e3, t3) {
      this.high = e3, this.low = t3;
    }
  }), s2.WordArray = o2.extend({
    init: function(e3, t3) {
      e3 = this.words = e3 || [], this.sigBytes = null != t3 ? t3 : 8 * e3.length;
    },
    toX32: function() {
      for (var e3 = this.words, t3 = e3.length, r3 = [], n3 = 0; n3 < t3; n3++) {
        var i3 = e3[n3];
        r3.push(i3.high), r3.push(i3.low);
      }
      return a2.create(r3, this.sigBytes);
    },
    clone: function() {
      for (var e3 = o2.clone.call(this), t3 = e3.words = this.words.slice(0), r3 = t3.length, n3 = 0; n3 < r3; n3++)
        t3[n3] = t3[n3].clone();
      return e3;
    }
  }), r2);
}), Q(function(e2, t2) {
  var r2;
  e2.exports = (r2 = ee, function() {
    if ("function" == typeof ArrayBuffer) {
      var e3 = r2.lib.WordArray, t3 = e3.init;
      (e3.init = function(e4) {
        if (e4 instanceof ArrayBuffer && (e4 = new Uint8Array(e4)), (e4 instanceof Int8Array || "undefined" != typeof Uint8ClampedArray && e4 instanceof Uint8ClampedArray || e4 instanceof Int16Array || e4 instanceof Uint16Array || e4 instanceof Int32Array || e4 instanceof Uint32Array || e4 instanceof Float32Array || e4 instanceof Float64Array) && (e4 = new Uint8Array(e4.buffer, e4.byteOffset, e4.byteLength)), e4 instanceof Uint8Array) {
          for (var r3 = e4.byteLength, n2 = [], i2 = 0; i2 < r3; i2++)
            n2[i2 >>> 2] |= e4[i2] << 24 - i2 % 4 * 8;
          t3.call(this, n2, r3);
        } else
          t3.apply(this, arguments);
      }).prototype = e3;
    }
  }(), r2.lib.WordArray);
}), Q(function(e2, t2) {
  var r2;
  e2.exports = (r2 = ee, function() {
    var e3 = r2, t3 = e3.lib.WordArray, n2 = e3.enc;
    function i2(e4) {
      return e4 << 8 & 4278255360 | e4 >>> 8 & 16711935;
    }
    n2.Utf16 = n2.Utf16BE = {
      stringify: function(e4) {
        for (var t4 = e4.words, r3 = e4.sigBytes, n3 = [], i3 = 0; i3 < r3; i3 += 2) {
          var o2 = t4[i3 >>> 2] >>> 16 - i3 % 4 * 8 & 65535;
          n3.push(String.fromCharCode(o2));
        }
        return n3.join("");
      },
      parse: function(e4) {
        for (var r3 = e4.length, n3 = [], i3 = 0; i3 < r3; i3++)
          n3[i3 >>> 1] |= e4.charCodeAt(i3) << 16 - i3 % 2 * 16;
        return t3.create(n3, 2 * r3);
      }
    }, n2.Utf16LE = {
      stringify: function(e4) {
        for (var t4 = e4.words, r3 = e4.sigBytes, n3 = [], o2 = 0; o2 < r3; o2 += 2) {
          var a2 = i2(t4[o2 >>> 2] >>> 16 - o2 % 4 * 8 & 65535);
          n3.push(String.fromCharCode(a2));
        }
        return n3.join("");
      },
      parse: function(e4) {
        for (var r3 = e4.length, n3 = [], o2 = 0; o2 < r3; o2++)
          n3[o2 >>> 1] |= i2(e4.charCodeAt(o2) << 16 - o2 % 2 * 16);
        return t3.create(n3, 2 * r3);
      }
    };
  }(), r2.enc.Utf16);
}), Q(function(e2, t2) {
  var r2, n2, i2;
  e2.exports = (i2 = (n2 = r2 = ee).lib.WordArray, n2.enc.Base64 = {
    stringify: function(e3) {
      var t3 = e3.words, r3 = e3.sigBytes, n3 = this._map;
      e3.clamp();
      for (var i3 = [], o2 = 0; o2 < r3; o2 += 3)
        for (var a2 = (t3[o2 >>> 2] >>> 24 - o2 % 4 * 8 & 255) << 16 | (t3[o2 + 1 >>> 2] >>> 24 - (o2 + 1) % 4 * 8 & 255) << 8 | t3[o2 + 2 >>> 2] >>> 24 - (o2 + 2) % 4 * 8 & 255, s2 = 0; s2 < 4 && o2 + 0.75 * s2 < r3; s2++)
          i3.push(n3.charAt(a2 >>> 6 * (3 - s2) & 63));
      var h2 = n3.charAt(64);
      if (h2)
        for (; i3.length % 4; )
          i3.push(h2);
      return i3.join("");
    },
    parse: function(e3) {
      var t3 = e3.length, r3 = this._map, n3 = this._reverseMap;
      if (!n3) {
        n3 = this._reverseMap = [];
        for (var o2 = 0; o2 < r3.length; o2++)
          n3[r3.charCodeAt(o2)] = o2;
      }
      var a2 = r3.charAt(64);
      if (a2) {
        var s2 = e3.indexOf(a2);
        -1 !== s2 && (t3 = s2);
      }
      return function(e4, t4, r4) {
        for (var n4 = [], o3 = 0, a3 = 0; a3 < t4; a3++)
          if (a3 % 4) {
            var s3 = r4[e4.charCodeAt(a3 - 1)] << a3 % 4 * 2, h2 = r4[e4.charCodeAt(a3)] >>> 6 - a3 % 4 * 2;
            n4[o3 >>> 2] |= (s3 | h2) << 24 - o3 % 4 * 8, o3++;
          }
        return i2.create(n4, o3);
      }(e3, t3, n3);
    },
    _map: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
  }, r2.enc.Base64);
}), Q(function(e2, t2) {
  var r2;
  e2.exports = (r2 = ee, function(e3) {
    var t3 = r2, n2 = t3.lib, i2 = n2.WordArray, o2 = n2.Hasher, a2 = t3.algo, s2 = [];
    !function() {
      for (var t4 = 0; t4 < 64; t4++)
        s2[t4] = 4294967296 * e3.abs(e3.sin(t4 + 1)) | 0;
    }();
    var h2 = a2.MD5 = o2.extend({
      _doReset: function() {
        this._hash = new i2.init([1732584193, 4023233417, 2562383102, 271733878]);
      },
      _doProcessBlock: function(e4, t4) {
        for (var r3 = 0; r3 < 16; r3++) {
          var n3 = t4 + r3, i3 = e4[n3];
          e4[n3] = 16711935 & (i3 << 8 | i3 >>> 24) | 4278255360 & (i3 << 24 | i3 >>> 8);
        }
        var o3 = this._hash.words, a3 = e4[t4 + 0], h3 = e4[t4 + 1], d2 = e4[t4 + 2], p2 = e4[t4 + 3], _2 = e4[t4 + 4], g2 = e4[t4 + 5], v2 = e4[t4 + 6], w2 = e4[t4 + 7], b2 = e4[t4 + 8], y2 = e4[t4 + 9], m2 = e4[t4 + 10], k2 = e4[t4 + 11], E2 = e4[t4 + 12], S2 = e4[t4 + 13], x2 = e4[t4 + 14], R2 = e4[t4 + 15], A2 = o3[0], B2 = o3[1], z2 = o3[2], L2 = o3[3];
        A2 = l2(A2, B2, z2, L2, a3, 7, s2[0]), L2 = l2(L2, A2, B2, z2, h3, 12, s2[1]), z2 = l2(z2, L2, A2, B2, d2, 17, s2[2]), B2 = l2(B2, z2, L2, A2, p2, 22, s2[3]), A2 = l2(A2, B2, z2, L2, _2, 7, s2[4]), L2 = l2(L2, A2, B2, z2, g2, 12, s2[5]), z2 = l2(z2, L2, A2, B2, v2, 17, s2[6]), B2 = l2(B2, z2, L2, A2, w2, 22, s2[7]), A2 = l2(A2, B2, z2, L2, b2, 7, s2[8]), L2 = l2(L2, A2, B2, z2, y2, 12, s2[9]), z2 = l2(z2, L2, A2, B2, m2, 17, s2[10]), B2 = l2(B2, z2, L2, A2, k2, 22, s2[11]), A2 = l2(A2, B2, z2, L2, E2, 7, s2[12]), L2 = l2(L2, A2, B2, z2, S2, 12, s2[13]), z2 = l2(z2, L2, A2, B2, x2, 17, s2[14]), A2 = f2(A2, B2 = l2(B2, z2, L2, A2, R2, 22, s2[15]), z2, L2, h3, 5, s2[16]), L2 = f2(L2, A2, B2, z2, v2, 9, s2[17]), z2 = f2(z2, L2, A2, B2, k2, 14, s2[18]), B2 = f2(B2, z2, L2, A2, a3, 20, s2[19]), A2 = f2(A2, B2, z2, L2, g2, 5, s2[20]), L2 = f2(L2, A2, B2, z2, m2, 9, s2[21]), z2 = f2(z2, L2, A2, B2, R2, 14, s2[22]), B2 = f2(B2, z2, L2, A2, _2, 20, s2[23]), A2 = f2(A2, B2, z2, L2, y2, 5, s2[24]), L2 = f2(L2, A2, B2, z2, x2, 9, s2[25]), z2 = f2(z2, L2, A2, B2, p2, 14, s2[26]), B2 = f2(B2, z2, L2, A2, b2, 20, s2[27]), A2 = f2(A2, B2, z2, L2, S2, 5, s2[28]), L2 = f2(L2, A2, B2, z2, d2, 9, s2[29]), z2 = f2(z2, L2, A2, B2, w2, 14, s2[30]), A2 = c2(A2, B2 = f2(B2, z2, L2, A2, E2, 20, s2[31]), z2, L2, g2, 4, s2[32]), L2 = c2(L2, A2, B2, z2, b2, 11, s2[33]), z2 = c2(z2, L2, A2, B2, k2, 16, s2[34]), B2 = c2(B2, z2, L2, A2, x2, 23, s2[35]), A2 = c2(A2, B2, z2, L2, h3, 4, s2[36]), L2 = c2(L2, A2, B2, z2, _2, 11, s2[37]), z2 = c2(z2, L2, A2, B2, w2, 16, s2[38]), B2 = c2(B2, z2, L2, A2, m2, 23, s2[39]), A2 = c2(A2, B2, z2, L2, S2, 4, s2[40]), L2 = c2(L2, A2, B2, z2, a3, 11, s2[41]), z2 = c2(z2, L2, A2, B2, p2, 16, s2[42]), B2 = c2(B2, z2, L2, A2, v2, 23, s2[43]), A2 = c2(A2, B2, z2, L2, y2, 4, s2[44]), L2 = c2(L2, A2, B2, z2, E2, 11, s2[45]), z2 = c2(z2, L2, A2, B2, R2, 16, s2[46]), A2 = u2(A2, B2 = c2(B2, z2, L2, A2, d2, 23, s2[47]), z2, L2, a3, 6, s2[48]), L2 = u2(L2, A2, B2, z2, w2, 10, s2[49]), z2 = u2(z2, L2, A2, B2, x2, 15, s2[50]), B2 = u2(B2, z2, L2, A2, g2, 21, s2[51]), A2 = u2(A2, B2, z2, L2, E2, 6, s2[52]), L2 = u2(L2, A2, B2, z2, p2, 10, s2[53]), z2 = u2(z2, L2, A2, B2, m2, 15, s2[54]), B2 = u2(B2, z2, L2, A2, h3, 21, s2[55]), A2 = u2(A2, B2, z2, L2, b2, 6, s2[56]), L2 = u2(L2, A2, B2, z2, R2, 10, s2[57]), z2 = u2(z2, L2, A2, B2, v2, 15, s2[58]), B2 = u2(B2, z2, L2, A2, S2, 21, s2[59]), A2 = u2(A2, B2, z2, L2, _2, 6, s2[60]), L2 = u2(L2, A2, B2, z2, k2, 10, s2[61]), z2 = u2(z2, L2, A2, B2, d2, 15, s2[62]), B2 = u2(B2, z2, L2, A2, y2, 21, s2[63]), o3[0] = o3[0] + A2 | 0, o3[1] = o3[1] + B2 | 0, o3[2] = o3[2] + z2 | 0, o3[3] = o3[3] + L2 | 0;
      },
      _doFinalize: function() {
        var t4 = this._data, r3 = t4.words, n3 = 8 * this._nDataBytes, i3 = 8 * t4.sigBytes;
        r3[i3 >>> 5] |= 128 << 24 - i3 % 32;
        var o3 = e3.floor(n3 / 4294967296), a3 = n3;
        r3[15 + (i3 + 64 >>> 9 << 4)] = 16711935 & (o3 << 8 | o3 >>> 24) | 4278255360 & (o3 << 24 | o3 >>> 8), r3[14 + (i3 + 64 >>> 9 << 4)] = 16711935 & (a3 << 8 | a3 >>> 24) | 4278255360 & (a3 << 24 | a3 >>> 8), t4.sigBytes = 4 * (r3.length + 1), this._process();
        for (var s3 = this._hash, h3 = s3.words, l3 = 0; l3 < 4; l3++) {
          var f3 = h3[l3];
          h3[l3] = 16711935 & (f3 << 8 | f3 >>> 24) | 4278255360 & (f3 << 24 | f3 >>> 8);
        }
        return s3;
      },
      clone: function() {
        var e4 = o2.clone.call(this);
        return e4._hash = this._hash.clone(), e4;
      }
    });
    function l2(e4, t4, r3, n3, i3, o3, a3) {
      var s3 = e4 + (t4 & r3 | ~t4 & n3) + i3 + a3;
      return (s3 << o3 | s3 >>> 32 - o3) + t4;
    }
    function f2(e4, t4, r3, n3, i3, o3, a3) {
      var s3 = e4 + (t4 & n3 | r3 & ~n3) + i3 + a3;
      return (s3 << o3 | s3 >>> 32 - o3) + t4;
    }
    function c2(e4, t4, r3, n3, i3, o3, a3) {
      var s3 = e4 + (t4 ^ r3 ^ n3) + i3 + a3;
      return (s3 << o3 | s3 >>> 32 - o3) + t4;
    }
    function u2(e4, t4, r3, n3, i3, o3, a3) {
      var s3 = e4 + (r3 ^ (t4 | ~n3)) + i3 + a3;
      return (s3 << o3 | s3 >>> 32 - o3) + t4;
    }
    t3.MD5 = o2._createHelper(h2), t3.HmacMD5 = o2._createHmacHelper(h2);
  }(Math), r2.MD5);
}), Q(function(e2, t2) {
  var r2, n2, i2, o2, a2, s2, h2, l2;
  e2.exports = (i2 = (n2 = r2 = ee).lib, o2 = i2.WordArray, a2 = i2.Hasher, s2 = n2.algo, h2 = [], l2 = s2.SHA1 = a2.extend({
    _doReset: function() {
      this._hash = new o2.init([1732584193, 4023233417, 2562383102, 271733878, 3285377520]);
    },
    _doProcessBlock: function(e3, t3) {
      for (var r3 = this._hash.words, n3 = r3[0], i3 = r3[1], o3 = r3[2], a3 = r3[3], s3 = r3[4], l3 = 0; l3 < 80; l3++) {
        if (l3 < 16)
          h2[l3] = 0 | e3[t3 + l3];
        else {
          var f2 = h2[l3 - 3] ^ h2[l3 - 8] ^ h2[l3 - 14] ^ h2[l3 - 16];
          h2[l3] = f2 << 1 | f2 >>> 31;
        }
        var c2 = (n3 << 5 | n3 >>> 27) + s3 + h2[l3];
        c2 += l3 < 20 ? 1518500249 + (i3 & o3 | ~i3 & a3) : l3 < 40 ? 1859775393 + (i3 ^ o3 ^ a3) : l3 < 60 ? (i3 & o3 | i3 & a3 | o3 & a3) - 1894007588 : (i3 ^ o3 ^ a3) - 899497514, s3 = a3, a3 = o3, o3 = i3 << 30 | i3 >>> 2, i3 = n3, n3 = c2;
      }
      r3[0] = r3[0] + n3 | 0, r3[1] = r3[1] + i3 | 0, r3[2] = r3[2] + o3 | 0, r3[3] = r3[3] + a3 | 0, r3[4] = r3[4] + s3 | 0;
    },
    _doFinalize: function() {
      var e3 = this._data, t3 = e3.words, r3 = 8 * this._nDataBytes, n3 = 8 * e3.sigBytes;
      return t3[n3 >>> 5] |= 128 << 24 - n3 % 32, t3[14 + (n3 + 64 >>> 9 << 4)] = Math.floor(r3 / 4294967296), t3[15 + (n3 + 64 >>> 9 << 4)] = r3, e3.sigBytes = 4 * t3.length, this._process(), this._hash;
    },
    clone: function() {
      var e3 = a2.clone.call(this);
      return e3._hash = this._hash.clone(), e3;
    }
  }), n2.SHA1 = a2._createHelper(l2), n2.HmacSHA1 = a2._createHmacHelper(l2), r2.SHA1);
}), Q(function(e2, t2) {
  var r2;
  e2.exports = (r2 = ee, function(e3) {
    var t3 = r2, n2 = t3.lib, i2 = n2.WordArray, o2 = n2.Hasher, a2 = t3.algo, s2 = [], h2 = [];
    !function() {
      function t4(t5) {
        for (var r4 = e3.sqrt(t5), n4 = 2; n4 <= r4; n4++)
          if (!(t5 % n4))
            return false;
        return true;
      }
      function r3(e4) {
        return 4294967296 * (e4 - (0 | e4)) | 0;
      }
      for (var n3 = 2, i3 = 0; i3 < 64; )
        t4(n3) && (i3 < 8 && (s2[i3] = r3(e3.pow(n3, 0.5))), h2[i3] = r3(e3.pow(n3, 1 / 3)), i3++), n3++;
    }();
    var l2 = [], f2 = a2.SHA256 = o2.extend({
      _doReset: function() {
        this._hash = new i2.init(s2.slice(0));
      },
      _doProcessBlock: function(e4, t4) {
        for (var r3 = this._hash.words, n3 = r3[0], i3 = r3[1], o3 = r3[2], a3 = r3[3], s3 = r3[4], f3 = r3[5], c2 = r3[6], u2 = r3[7], d2 = 0; d2 < 64; d2++) {
          if (d2 < 16)
            l2[d2] = 0 | e4[t4 + d2];
          else {
            var p2 = l2[d2 - 15], _2 = (p2 << 25 | p2 >>> 7) ^ (p2 << 14 | p2 >>> 18) ^ p2 >>> 3, g2 = l2[d2 - 2], v2 = (g2 << 15 | g2 >>> 17) ^ (g2 << 13 | g2 >>> 19) ^ g2 >>> 10;
            l2[d2] = _2 + l2[d2 - 7] + v2 + l2[d2 - 16];
          }
          var w2 = n3 & i3 ^ n3 & o3 ^ i3 & o3, b2 = (n3 << 30 | n3 >>> 2) ^ (n3 << 19 | n3 >>> 13) ^ (n3 << 10 | n3 >>> 22), y2 = u2 + ((s3 << 26 | s3 >>> 6) ^ (s3 << 21 | s3 >>> 11) ^ (s3 << 7 | s3 >>> 25)) + (s3 & f3 ^ ~s3 & c2) + h2[d2] + l2[d2];
          u2 = c2, c2 = f3, f3 = s3, s3 = a3 + y2 | 0, a3 = o3, o3 = i3, i3 = n3, n3 = y2 + (b2 + w2) | 0;
        }
        r3[0] = r3[0] + n3 | 0, r3[1] = r3[1] + i3 | 0, r3[2] = r3[2] + o3 | 0, r3[3] = r3[3] + a3 | 0, r3[4] = r3[4] + s3 | 0, r3[5] = r3[5] + f3 | 0, r3[6] = r3[6] + c2 | 0, r3[7] = r3[7] + u2 | 0;
      },
      _doFinalize: function() {
        var t4 = this._data, r3 = t4.words, n3 = 8 * this._nDataBytes, i3 = 8 * t4.sigBytes;
        return r3[i3 >>> 5] |= 128 << 24 - i3 % 32, r3[14 + (i3 + 64 >>> 9 << 4)] = e3.floor(n3 / 4294967296), r3[15 + (i3 + 64 >>> 9 << 4)] = n3, t4.sigBytes = 4 * r3.length, this._process(), this._hash;
      },
      clone: function() {
        var e4 = o2.clone.call(this);
        return e4._hash = this._hash.clone(), e4;
      }
    });
    t3.SHA256 = o2._createHelper(f2), t3.HmacSHA256 = o2._createHmacHelper(f2);
  }(Math), r2.SHA256);
}), Q(function(e2, t2) {
  var r2, n2, i2, o2, a2, s2;
  e2.exports = (i2 = (n2 = r2 = ee).lib.WordArray, o2 = n2.algo, a2 = o2.SHA256, s2 = o2.SHA224 = a2.extend({
    _doReset: function() {
      this._hash = new i2.init([3238371032, 914150663, 812702999, 4144912697, 4290775857, 1750603025, 1694076839, 3204075428]);
    },
    _doFinalize: function() {
      var e3 = a2._doFinalize.call(this);
      return e3.sigBytes -= 4, e3;
    }
  }), n2.SHA224 = a2._createHelper(s2), n2.HmacSHA224 = a2._createHmacHelper(s2), r2.SHA224);
}), Q(function(e2, t2) {
  var r2;
  e2.exports = (r2 = ee, function() {
    var e3 = r2, t3 = e3.lib.Hasher, n2 = e3.x64, i2 = n2.Word, o2 = n2.WordArray, a2 = e3.algo;
    function s2() {
      return i2.create.apply(i2, arguments);
    }
    var h2 = [s2(1116352408, 3609767458), s2(1899447441, 602891725), s2(3049323471, 3964484399), s2(3921009573, 2173295548), s2(961987163, 4081628472), s2(1508970993, 3053834265), s2(2453635748, 2937671579), s2(2870763221, 3664609560), s2(3624381080, 2734883394), s2(310598401, 1164996542), s2(607225278, 1323610764), s2(1426881987, 3590304994), s2(1925078388, 4068182383), s2(2162078206, 991336113), s2(2614888103, 633803317), s2(3248222580, 3479774868), s2(3835390401, 2666613458), s2(4022224774, 944711139), s2(264347078, 2341262773), s2(604807628, 2007800933), s2(770255983, 1495990901), s2(1249150122, 1856431235), s2(1555081692, 3175218132), s2(1996064986, 2198950837), s2(2554220882, 3999719339), s2(2821834349, 766784016), s2(2952996808, 2566594879), s2(3210313671, 3203337956), s2(3336571891, 1034457026), s2(3584528711, 2466948901), s2(113926993, 3758326383), s2(338241895, 168717936), s2(666307205, 1188179964), s2(773529912, 1546045734), s2(1294757372, 1522805485), s2(1396182291, 2643833823), s2(1695183700, 2343527390), s2(1986661051, 1014477480), s2(2177026350, 1206759142), s2(2456956037, 344077627), s2(2730485921, 1290863460), s2(2820302411, 3158454273), s2(3259730800, 3505952657), s2(3345764771, 106217008), s2(3516065817, 3606008344), s2(3600352804, 1432725776), s2(4094571909, 1467031594), s2(275423344, 851169720), s2(430227734, 3100823752), s2(506948616, 1363258195), s2(659060556, 3750685593), s2(883997877, 3785050280), s2(958139571, 3318307427), s2(1322822218, 3812723403), s2(1537002063, 2003034995), s2(1747873779, 3602036899), s2(1955562222, 1575990012), s2(2024104815, 1125592928), s2(2227730452, 2716904306), s2(2361852424, 442776044), s2(2428436474, 593698344), s2(2756734187, 3733110249), s2(3204031479, 2999351573), s2(3329325298, 3815920427), s2(3391569614, 3928383900), s2(3515267271, 566280711), s2(3940187606, 3454069534), s2(4118630271, 4000239992), s2(116418474, 1914138554), s2(174292421, 2731055270), s2(289380356, 3203993006), s2(460393269, 320620315), s2(685471733, 587496836), s2(852142971, 1086792851), s2(1017036298, 365543100), s2(1126000580, 2618297676), s2(1288033470, 3409855158), s2(1501505948, 4234509866), s2(1607167915, 987167468), s2(1816402316, 1246189591)], l2 = [];
    !function() {
      for (var e4 = 0; e4 < 80; e4++)
        l2[e4] = s2();
    }();
    var f2 = a2.SHA512 = t3.extend({
      _doReset: function() {
        this._hash = new o2.init([new i2.init(1779033703, 4089235720), new i2.init(3144134277, 2227873595), new i2.init(1013904242, 4271175723), new i2.init(2773480762, 1595750129), new i2.init(1359893119, 2917565137), new i2.init(2600822924, 725511199), new i2.init(528734635, 4215389547), new i2.init(1541459225, 327033209)]);
      },
      _doProcessBlock: function(e4, t4) {
        for (var r3 = this._hash.words, n3 = r3[0], i3 = r3[1], o3 = r3[2], a3 = r3[3], s3 = r3[4], f3 = r3[5], c2 = r3[6], u2 = r3[7], d2 = n3.high, p2 = n3.low, _2 = i3.high, g2 = i3.low, v2 = o3.high, w2 = o3.low, b2 = a3.high, y2 = a3.low, m2 = s3.high, k2 = s3.low, E2 = f3.high, S2 = f3.low, x2 = c2.high, R2 = c2.low, A2 = u2.high, B2 = u2.low, z2 = d2, L2 = p2, T2 = _2, M2 = g2, C2 = v2, D2 = w2, I2 = b2, P2 = y2, O2 = m2, U2 = k2, H2 = E2, F2 = S2, N2 = x2, Z2 = R2, j2 = A2, W2 = B2, Y2 = 0; Y2 < 80; Y2++) {
          var K2 = l2[Y2];
          if (Y2 < 16)
            var X2 = K2.high = 0 | e4[t4 + 2 * Y2], q2 = K2.low = 0 | e4[t4 + 2 * Y2 + 1];
          else {
            var V2 = l2[Y2 - 15], G2 = V2.high, $2 = V2.low, J2 = (G2 >>> 1 | $2 << 31) ^ (G2 >>> 8 | $2 << 24) ^ G2 >>> 7, Q2 = ($2 >>> 1 | G2 << 31) ^ ($2 >>> 8 | G2 << 24) ^ ($2 >>> 7 | G2 << 25), ee2 = l2[Y2 - 2], te2 = ee2.high, re2 = ee2.low, ne2 = (te2 >>> 19 | re2 << 13) ^ (te2 << 3 | re2 >>> 29) ^ te2 >>> 6, ie2 = (re2 >>> 19 | te2 << 13) ^ (re2 << 3 | te2 >>> 29) ^ (re2 >>> 6 | te2 << 26), oe2 = l2[Y2 - 7], ae2 = oe2.high, se2 = oe2.low, he2 = l2[Y2 - 16], le2 = he2.high, fe2 = he2.low;
            X2 = (X2 = (X2 = J2 + ae2 + ((q2 = Q2 + se2) >>> 0 < Q2 >>> 0 ? 1 : 0)) + ne2 + ((q2 += ie2) >>> 0 < ie2 >>> 0 ? 1 : 0)) + le2 + ((q2 += fe2) >>> 0 < fe2 >>> 0 ? 1 : 0), K2.high = X2, K2.low = q2;
          }
          var ce2, ue2 = O2 & H2 ^ ~O2 & N2, de2 = U2 & F2 ^ ~U2 & Z2, pe2 = z2 & T2 ^ z2 & C2 ^ T2 & C2, _e = L2 & M2 ^ L2 & D2 ^ M2 & D2, ge2 = (z2 >>> 28 | L2 << 4) ^ (z2 << 30 | L2 >>> 2) ^ (z2 << 25 | L2 >>> 7), ve2 = (L2 >>> 28 | z2 << 4) ^ (L2 << 30 | z2 >>> 2) ^ (L2 << 25 | z2 >>> 7), we2 = (O2 >>> 14 | U2 << 18) ^ (O2 >>> 18 | U2 << 14) ^ (O2 << 23 | U2 >>> 9), be2 = (U2 >>> 14 | O2 << 18) ^ (U2 >>> 18 | O2 << 14) ^ (U2 << 23 | O2 >>> 9), ye2 = h2[Y2], me2 = ye2.high, ke2 = ye2.low, Ee2 = j2 + we2 + ((ce2 = W2 + be2) >>> 0 < W2 >>> 0 ? 1 : 0), Se2 = ve2 + _e;
          j2 = N2, W2 = Z2, N2 = H2, Z2 = F2, H2 = O2, F2 = U2, O2 = I2 + (Ee2 = (Ee2 = (Ee2 = Ee2 + ue2 + ((ce2 += de2) >>> 0 < de2 >>> 0 ? 1 : 0)) + me2 + ((ce2 += ke2) >>> 0 < ke2 >>> 0 ? 1 : 0)) + X2 + ((ce2 += q2) >>> 0 < q2 >>> 0 ? 1 : 0)) + ((U2 = P2 + ce2 | 0) >>> 0 < P2 >>> 0 ? 1 : 0) | 0, I2 = C2, P2 = D2, C2 = T2, D2 = M2, T2 = z2, M2 = L2, z2 = Ee2 + (ge2 + pe2 + (Se2 >>> 0 < ve2 >>> 0 ? 1 : 0)) + ((L2 = ce2 + Se2 | 0) >>> 0 < ce2 >>> 0 ? 1 : 0) | 0;
        }
        p2 = n3.low = p2 + L2, n3.high = d2 + z2 + (p2 >>> 0 < L2 >>> 0 ? 1 : 0), g2 = i3.low = g2 + M2, i3.high = _2 + T2 + (g2 >>> 0 < M2 >>> 0 ? 1 : 0), w2 = o3.low = w2 + D2, o3.high = v2 + C2 + (w2 >>> 0 < D2 >>> 0 ? 1 : 0), y2 = a3.low = y2 + P2, a3.high = b2 + I2 + (y2 >>> 0 < P2 >>> 0 ? 1 : 0), k2 = s3.low = k2 + U2, s3.high = m2 + O2 + (k2 >>> 0 < U2 >>> 0 ? 1 : 0), S2 = f3.low = S2 + F2, f3.high = E2 + H2 + (S2 >>> 0 < F2 >>> 0 ? 1 : 0), R2 = c2.low = R2 + Z2, c2.high = x2 + N2 + (R2 >>> 0 < Z2 >>> 0 ? 1 : 0), B2 = u2.low = B2 + W2, u2.high = A2 + j2 + (B2 >>> 0 < W2 >>> 0 ? 1 : 0);
      },
      _doFinalize: function() {
        var e4 = this._data, t4 = e4.words, r3 = 8 * this._nDataBytes, n3 = 8 * e4.sigBytes;
        return t4[n3 >>> 5] |= 128 << 24 - n3 % 32, t4[30 + (n3 + 128 >>> 10 << 5)] = Math.floor(r3 / 4294967296), t4[31 + (n3 + 128 >>> 10 << 5)] = r3, e4.sigBytes = 4 * t4.length, this._process(), this._hash.toX32();
      },
      clone: function() {
        var e4 = t3.clone.call(this);
        return e4._hash = this._hash.clone(), e4;
      },
      blockSize: 32
    });
    e3.SHA512 = t3._createHelper(f2), e3.HmacSHA512 = t3._createHmacHelper(f2);
  }(), r2.SHA512);
}), Q(function(e2, t2) {
  var r2, n2, i2, o2, a2, s2, h2, l2;
  e2.exports = (i2 = (n2 = r2 = ee).x64, o2 = i2.Word, a2 = i2.WordArray, s2 = n2.algo, h2 = s2.SHA512, l2 = s2.SHA384 = h2.extend({
    _doReset: function() {
      this._hash = new a2.init([new o2.init(3418070365, 3238371032), new o2.init(1654270250, 914150663), new o2.init(2438529370, 812702999), new o2.init(355462360, 4144912697), new o2.init(1731405415, 4290775857), new o2.init(2394180231, 1750603025), new o2.init(3675008525, 1694076839), new o2.init(1203062813, 3204075428)]);
    },
    _doFinalize: function() {
      var e3 = h2._doFinalize.call(this);
      return e3.sigBytes -= 16, e3;
    }
  }), n2.SHA384 = h2._createHelper(l2), n2.HmacSHA384 = h2._createHmacHelper(l2), r2.SHA384);
}), Q(function(e2, t2) {
  var r2;
  e2.exports = (r2 = ee, function(e3) {
    var t3 = r2, n2 = t3.lib, i2 = n2.WordArray, o2 = n2.Hasher, a2 = t3.x64.Word, s2 = t3.algo, h2 = [], l2 = [], f2 = [];
    !function() {
      for (var e4 = 1, t4 = 0, r3 = 0; r3 < 24; r3++) {
        h2[e4 + 5 * t4] = (r3 + 1) * (r3 + 2) / 2 % 64;
        var n3 = (2 * e4 + 3 * t4) % 5;
        e4 = t4 % 5, t4 = n3;
      }
      for (e4 = 0; e4 < 5; e4++)
        for (t4 = 0; t4 < 5; t4++)
          l2[e4 + 5 * t4] = t4 + (2 * e4 + 3 * t4) % 5 * 5;
      for (var i3 = 1, o3 = 0; o3 < 24; o3++) {
        for (var s3 = 0, c3 = 0, u3 = 0; u3 < 7; u3++) {
          if (1 & i3) {
            var d2 = (1 << u3) - 1;
            d2 < 32 ? c3 ^= 1 << d2 : s3 ^= 1 << d2 - 32;
          }
          128 & i3 ? i3 = i3 << 1 ^ 113 : i3 <<= 1;
        }
        f2[o3] = a2.create(s3, c3);
      }
    }();
    var c2 = [];
    !function() {
      for (var e4 = 0; e4 < 25; e4++)
        c2[e4] = a2.create();
    }();
    var u2 = s2.SHA3 = o2.extend({
      cfg: o2.cfg.extend({
        outputLength: 512
      }),
      _doReset: function() {
        for (var e4 = this._state = [], t4 = 0; t4 < 25; t4++)
          e4[t4] = new a2.init();
        this.blockSize = (1600 - 2 * this.cfg.outputLength) / 32;
      },
      _doProcessBlock: function(e4, t4) {
        for (var r3 = this._state, n3 = this.blockSize / 2, i3 = 0; i3 < n3; i3++) {
          var o3 = e4[t4 + 2 * i3], a3 = e4[t4 + 2 * i3 + 1];
          o3 = 16711935 & (o3 << 8 | o3 >>> 24) | 4278255360 & (o3 << 24 | o3 >>> 8), a3 = 16711935 & (a3 << 8 | a3 >>> 24) | 4278255360 & (a3 << 24 | a3 >>> 8), (B2 = r3[i3]).high ^= a3, B2.low ^= o3;
        }
        for (var s3 = 0; s3 < 24; s3++) {
          for (var u3 = 0; u3 < 5; u3++) {
            for (var d2 = 0, p2 = 0, _2 = 0; _2 < 5; _2++)
              d2 ^= (B2 = r3[u3 + 5 * _2]).high, p2 ^= B2.low;
            var g2 = c2[u3];
            g2.high = d2, g2.low = p2;
          }
          for (u3 = 0; u3 < 5; u3++) {
            var v2 = c2[(u3 + 4) % 5], w2 = c2[(u3 + 1) % 5], b2 = w2.high, y2 = w2.low;
            for (d2 = v2.high ^ (b2 << 1 | y2 >>> 31), p2 = v2.low ^ (y2 << 1 | b2 >>> 31), _2 = 0; _2 < 5; _2++)
              (B2 = r3[u3 + 5 * _2]).high ^= d2, B2.low ^= p2;
          }
          for (var m2 = 1; m2 < 25; m2++) {
            var k2 = (B2 = r3[m2]).high, E2 = B2.low, S2 = h2[m2];
            S2 < 32 ? (d2 = k2 << S2 | E2 >>> 32 - S2, p2 = E2 << S2 | k2 >>> 32 - S2) : (d2 = E2 << S2 - 32 | k2 >>> 64 - S2, p2 = k2 << S2 - 32 | E2 >>> 64 - S2);
            var x2 = c2[l2[m2]];
            x2.high = d2, x2.low = p2;
          }
          var R2 = c2[0], A2 = r3[0];
          for (R2.high = A2.high, R2.low = A2.low, u3 = 0; u3 < 5; u3++)
            for (_2 = 0; _2 < 5; _2++) {
              var B2 = r3[m2 = u3 + 5 * _2], z2 = c2[m2], L2 = c2[(u3 + 1) % 5 + 5 * _2], T2 = c2[(u3 + 2) % 5 + 5 * _2];
              B2.high = z2.high ^ ~L2.high & T2.high, B2.low = z2.low ^ ~L2.low & T2.low;
            }
          B2 = r3[0];
          var M2 = f2[s3];
          B2.high ^= M2.high, B2.low ^= M2.low;
        }
      },
      _doFinalize: function() {
        var t4 = this._data, r3 = t4.words, n3 = (this._nDataBytes, 8 * t4.sigBytes), o3 = 32 * this.blockSize;
        r3[n3 >>> 5] |= 1 << 24 - n3 % 32, r3[(e3.ceil((n3 + 1) / o3) * o3 >>> 5) - 1] |= 128, t4.sigBytes = 4 * r3.length, this._process();
        for (var a3 = this._state, s3 = this.cfg.outputLength / 8, h3 = s3 / 8, l3 = [], f3 = 0; f3 < h3; f3++) {
          var c3 = a3[f3], u3 = c3.high, d2 = c3.low;
          u3 = 16711935 & (u3 << 8 | u3 >>> 24) | 4278255360 & (u3 << 24 | u3 >>> 8), d2 = 16711935 & (d2 << 8 | d2 >>> 24) | 4278255360 & (d2 << 24 | d2 >>> 8), l3.push(d2), l3.push(u3);
        }
        return new i2.init(l3, s3);
      },
      clone: function() {
        for (var e4 = o2.clone.call(this), t4 = e4._state = this._state.slice(0), r3 = 0; r3 < 25; r3++)
          t4[r3] = t4[r3].clone();
        return e4;
      }
    });
    t3.SHA3 = o2._createHelper(u2), t3.HmacSHA3 = o2._createHmacHelper(u2);
  }(Math), r2.SHA3);
}), Q(function(e2, t2) {
  var r2;
  e2.exports = (r2 = ee, function(e3) {
    var t3 = r2, n2 = t3.lib, i2 = n2.WordArray, o2 = n2.Hasher, a2 = t3.algo, s2 = i2.create([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 7, 4, 13, 1, 10, 6, 15, 3, 12, 0, 9, 5, 2, 14, 11, 8, 3, 10, 14, 4, 9, 15, 8, 1, 2, 7, 0, 6, 13, 11, 5, 12, 1, 9, 11, 10, 0, 8, 12, 4, 13, 3, 7, 15, 14, 5, 6, 2, 4, 0, 5, 9, 7, 12, 2, 10, 14, 1, 3, 8, 11, 6, 15, 13]), h2 = i2.create([5, 14, 7, 0, 9, 2, 11, 4, 13, 6, 15, 8, 1, 10, 3, 12, 6, 11, 3, 7, 0, 13, 5, 10, 14, 15, 8, 12, 4, 9, 1, 2, 15, 5, 1, 3, 7, 14, 6, 9, 11, 8, 12, 2, 10, 0, 4, 13, 8, 6, 4, 1, 3, 11, 15, 0, 5, 12, 2, 13, 9, 7, 10, 14, 12, 15, 10, 4, 1, 5, 8, 7, 6, 2, 13, 14, 0, 3, 9, 11]), l2 = i2.create([11, 14, 15, 12, 5, 8, 7, 9, 11, 13, 14, 15, 6, 7, 9, 8, 7, 6, 8, 13, 11, 9, 7, 15, 7, 12, 15, 9, 11, 7, 13, 12, 11, 13, 6, 7, 14, 9, 13, 15, 14, 8, 13, 6, 5, 12, 7, 5, 11, 12, 14, 15, 14, 15, 9, 8, 9, 14, 5, 6, 8, 6, 5, 12, 9, 15, 5, 11, 6, 8, 13, 12, 5, 12, 13, 14, 11, 8, 5, 6]), f2 = i2.create([8, 9, 9, 11, 13, 15, 15, 5, 7, 7, 8, 11, 14, 14, 12, 6, 9, 13, 15, 7, 12, 8, 9, 11, 7, 7, 12, 7, 6, 15, 13, 11, 9, 7, 15, 11, 8, 6, 6, 14, 12, 13, 5, 14, 13, 13, 7, 5, 15, 5, 8, 11, 14, 14, 6, 14, 6, 9, 12, 9, 12, 5, 15, 8, 8, 5, 12, 9, 12, 5, 14, 6, 8, 13, 6, 5, 15, 13, 11, 11]), c2 = i2.create([0, 1518500249, 1859775393, 2400959708, 2840853838]), u2 = i2.create([1352829926, 1548603684, 1836072691, 2053994217, 0]), d2 = a2.RIPEMD160 = o2.extend({
      _doReset: function() {
        this._hash = i2.create([1732584193, 4023233417, 2562383102, 271733878, 3285377520]);
      },
      _doProcessBlock: function(e4, t4) {
        for (var r3 = 0; r3 < 16; r3++) {
          var n3 = t4 + r3, i3 = e4[n3];
          e4[n3] = 16711935 & (i3 << 8 | i3 >>> 24) | 4278255360 & (i3 << 24 | i3 >>> 8);
        }
        var o3, a3, d3, y2, m2, k2, E2, S2, x2, R2, A2, B2 = this._hash.words, z2 = c2.words, L2 = u2.words, T2 = s2.words, M2 = h2.words, C2 = l2.words, D2 = f2.words;
        for (k2 = o3 = B2[0], E2 = a3 = B2[1], S2 = d3 = B2[2], x2 = y2 = B2[3], R2 = m2 = B2[4], r3 = 0; r3 < 80; r3 += 1)
          A2 = o3 + e4[t4 + T2[r3]] | 0, A2 += r3 < 16 ? p2(a3, d3, y2) + z2[0] : r3 < 32 ? _2(a3, d3, y2) + z2[1] : r3 < 48 ? g2(a3, d3, y2) + z2[2] : r3 < 64 ? v2(a3, d3, y2) + z2[3] : w2(a3, d3, y2) + z2[4], A2 = (A2 = b2(A2 |= 0, C2[r3])) + m2 | 0, o3 = m2, m2 = y2, y2 = b2(d3, 10), d3 = a3, a3 = A2, A2 = k2 + e4[t4 + M2[r3]] | 0, A2 += r3 < 16 ? w2(E2, S2, x2) + L2[0] : r3 < 32 ? v2(E2, S2, x2) + L2[1] : r3 < 48 ? g2(E2, S2, x2) + L2[2] : r3 < 64 ? _2(E2, S2, x2) + L2[3] : p2(E2, S2, x2) + L2[4], A2 = (A2 = b2(A2 |= 0, D2[r3])) + R2 | 0, k2 = R2, R2 = x2, x2 = b2(S2, 10), S2 = E2, E2 = A2;
        A2 = B2[1] + d3 + x2 | 0, B2[1] = B2[2] + y2 + R2 | 0, B2[2] = B2[3] + m2 + k2 | 0, B2[3] = B2[4] + o3 + E2 | 0, B2[4] = B2[0] + a3 + S2 | 0, B2[0] = A2;
      },
      _doFinalize: function() {
        var e4 = this._data, t4 = e4.words, r3 = 8 * this._nDataBytes, n3 = 8 * e4.sigBytes;
        t4[n3 >>> 5] |= 128 << 24 - n3 % 32, t4[14 + (n3 + 64 >>> 9 << 4)] = 16711935 & (r3 << 8 | r3 >>> 24) | 4278255360 & (r3 << 24 | r3 >>> 8), e4.sigBytes = 4 * (t4.length + 1), this._process();
        for (var i3 = this._hash, o3 = i3.words, a3 = 0; a3 < 5; a3++) {
          var s3 = o3[a3];
          o3[a3] = 16711935 & (s3 << 8 | s3 >>> 24) | 4278255360 & (s3 << 24 | s3 >>> 8);
        }
        return i3;
      },
      clone: function() {
        var e4 = o2.clone.call(this);
        return e4._hash = this._hash.clone(), e4;
      }
    });
    function p2(e4, t4, r3) {
      return e4 ^ t4 ^ r3;
    }
    function _2(e4, t4, r3) {
      return e4 & t4 | ~e4 & r3;
    }
    function g2(e4, t4, r3) {
      return (e4 | ~t4) ^ r3;
    }
    function v2(e4, t4, r3) {
      return e4 & r3 | t4 & ~r3;
    }
    function w2(e4, t4, r3) {
      return e4 ^ (t4 | ~r3);
    }
    function b2(e4, t4) {
      return e4 << t4 | e4 >>> 32 - t4;
    }
    t3.RIPEMD160 = o2._createHelper(d2), t3.HmacRIPEMD160 = o2._createHmacHelper(d2);
  }(), r2.RIPEMD160);
}), Q(function(e2, t2) {
  var r2, n2, i2, o2, a2, s2;
  e2.exports = (n2 = (r2 = ee).lib, i2 = n2.Base, o2 = r2.enc, a2 = o2.Utf8, s2 = r2.algo, void (s2.HMAC = i2.extend({
    init: function(e3, t3) {
      e3 = this._hasher = new e3.init(), "string" == typeof t3 && (t3 = a2.parse(t3));
      var r3 = e3.blockSize, n3 = 4 * r3;
      t3.sigBytes > n3 && (t3 = e3.finalize(t3)), t3.clamp();
      for (var i3 = this._oKey = t3.clone(), o3 = this._iKey = t3.clone(), s3 = i3.words, h2 = o3.words, l2 = 0; l2 < r3; l2++)
        s3[l2] ^= 1549556828, h2[l2] ^= 909522486;
      i3.sigBytes = o3.sigBytes = n3, this.reset();
    },
    reset: function() {
      var e3 = this._hasher;
      e3.reset(), e3.update(this._iKey);
    },
    update: function(e3) {
      return this._hasher.update(e3), this;
    },
    finalize: function(e3) {
      var t3 = this._hasher, r3 = t3.finalize(e3);
      t3.reset();
      var n3 = t3.finalize(this._oKey.clone().concat(r3));
      return n3;
    }
  })));
}), Q(function(e2, t2) {
  var r2, n2, i2, o2, a2, s2, h2, l2, f2;
  e2.exports = (i2 = (n2 = r2 = ee).lib, o2 = i2.Base, a2 = i2.WordArray, s2 = n2.algo, h2 = s2.SHA1, l2 = s2.HMAC, f2 = s2.PBKDF2 = o2.extend({
    cfg: o2.extend({
      keySize: 4,
      hasher: h2,
      iterations: 1
    }),
    init: function(e3) {
      this.cfg = this.cfg.extend(e3);
    },
    compute: function(e3, t3) {
      for (var r3 = this.cfg, n3 = l2.create(r3.hasher, e3), i3 = a2.create(), o3 = a2.create([1]), s3 = i3.words, h3 = o3.words, f3 = r3.keySize, c2 = r3.iterations; s3.length < f3; ) {
        var u2 = n3.update(t3).finalize(o3);
        n3.reset();
        for (var d2 = u2.words, p2 = d2.length, _2 = u2, g2 = 1; g2 < c2; g2++) {
          _2 = n3.finalize(_2), n3.reset();
          for (var v2 = _2.words, w2 = 0; w2 < p2; w2++)
            d2[w2] ^= v2[w2];
        }
        i3.concat(u2), h3[0]++;
      }
      return i3.sigBytes = 4 * f3, i3;
    }
  }), n2.PBKDF2 = function(e3, t3, r3) {
    return f2.create(r3).compute(e3, t3);
  }, r2.PBKDF2);
}), Q(function(e2, t2) {
  var r2, n2, i2, o2, a2, s2, h2, l2;
  e2.exports = (i2 = (n2 = r2 = ee).lib, o2 = i2.Base, a2 = i2.WordArray, s2 = n2.algo, h2 = s2.MD5, l2 = s2.EvpKDF = o2.extend({
    cfg: o2.extend({
      keySize: 4,
      hasher: h2,
      iterations: 1
    }),
    init: function(e3) {
      this.cfg = this.cfg.extend(e3);
    },
    compute: function(e3, t3) {
      for (var r3 = this.cfg, n3 = r3.hasher.create(), i3 = a2.create(), o3 = i3.words, s3 = r3.keySize, h3 = r3.iterations; o3.length < s3; ) {
        l3 && n3.update(l3);
        var l3 = n3.update(e3).finalize(t3);
        n3.reset();
        for (var f2 = 1; f2 < h3; f2++)
          l3 = n3.finalize(l3), n3.reset();
        i3.concat(l3);
      }
      return i3.sigBytes = 4 * s3, i3;
    }
  }), n2.EvpKDF = function(e3, t3, r3) {
    return l2.create(r3).compute(e3, t3);
  }, r2.EvpKDF);
}), Q(function(e2, t2) {
  var r2, n2, i2, o2, a2, s2, h2, l2, f2, c2, u2, d2, p2, _2, g2, v2, w2, b2, y2, m2, k2, E2, S2, x2;
  e2.exports = void ((r2 = ee).lib.Cipher || (i2 = r2, o2 = i2.lib, a2 = o2.Base, s2 = o2.WordArray, h2 = o2.BufferedBlockAlgorithm, l2 = i2.enc, l2.Utf8, f2 = l2.Base64, c2 = i2.algo, u2 = c2.EvpKDF, d2 = o2.Cipher = h2.extend({
    cfg: a2.extend(),
    createEncryptor: function(e3, t3) {
      return this.create(this._ENC_XFORM_MODE, e3, t3);
    },
    createDecryptor: function(e3, t3) {
      return this.create(this._DEC_XFORM_MODE, e3, t3);
    },
    init: function(e3, t3, r3) {
      this.cfg = this.cfg.extend(r3), this._xformMode = e3, this._key = t3, this.reset();
    },
    reset: function() {
      h2.reset.call(this), this._doReset();
    },
    process: function(e3) {
      return this._append(e3), this._process();
    },
    finalize: function(e3) {
      e3 && this._append(e3);
      var t3 = this._doFinalize();
      return t3;
    },
    keySize: 4,
    ivSize: 4,
    _ENC_XFORM_MODE: 1,
    _DEC_XFORM_MODE: 2,
    _createHelper: function() {
      function e3(e4) {
        return "string" == typeof e4 ? x2 : k2;
      }
      return function(t3) {
        return {
          encrypt: function(r3, n3, i3) {
            return e3(n3).encrypt(t3, r3, n3, i3);
          },
          decrypt: function(r3, n3, i3) {
            return e3(n3).decrypt(t3, r3, n3, i3);
          }
        };
      };
    }()
  }), o2.StreamCipher = d2.extend({
    _doFinalize: function() {
      var e3 = this._process(true);
      return e3;
    },
    blockSize: 1
  }), p2 = i2.mode = {}, _2 = o2.BlockCipherMode = a2.extend({
    createEncryptor: function(e3, t3) {
      return this.Encryptor.create(e3, t3);
    },
    createDecryptor: function(e3, t3) {
      return this.Decryptor.create(e3, t3);
    },
    init: function(e3, t3) {
      this._cipher = e3, this._iv = t3;
    }
  }), g2 = p2.CBC = function() {
    var e3 = _2.extend();
    function t3(e4, t4, r3) {
      var i3 = this._iv;
      if (i3) {
        var o3 = i3;
        this._iv = n2;
      } else
        var o3 = this._prevBlock;
      for (var a3 = 0; a3 < r3; a3++)
        e4[t4 + a3] ^= o3[a3];
    }
    return e3.Encryptor = e3.extend({
      processBlock: function(e4, r3) {
        var n3 = this._cipher, i3 = n3.blockSize;
        t3.call(this, e4, r3, i3), n3.encryptBlock(e4, r3), this._prevBlock = e4.slice(r3, r3 + i3);
      }
    }), e3.Decryptor = e3.extend({
      processBlock: function(e4, r3) {
        var n3 = this._cipher, i3 = n3.blockSize, o3 = e4.slice(r3, r3 + i3);
        n3.decryptBlock(e4, r3), t3.call(this, e4, r3, i3), this._prevBlock = o3;
      }
    }), e3;
  }(), v2 = i2.pad = {}, w2 = v2.Pkcs7 = {
    pad: function(e3, t3) {
      for (var r3 = 4 * t3, n3 = r3 - e3.sigBytes % r3, i3 = n3 << 24 | n3 << 16 | n3 << 8 | n3, o3 = [], a3 = 0; a3 < n3; a3 += 4)
        o3.push(i3);
      var h3 = s2.create(o3, n3);
      e3.concat(h3);
    },
    unpad: function(e3) {
      var t3 = 255 & e3.words[e3.sigBytes - 1 >>> 2];
      e3.sigBytes -= t3;
    }
  }, o2.BlockCipher = d2.extend({
    cfg: d2.cfg.extend({
      mode: g2,
      padding: w2
    }),
    reset: function() {
      d2.reset.call(this);
      var e3 = this.cfg, t3 = e3.iv, r3 = e3.mode;
      if (this._xformMode == this._ENC_XFORM_MODE)
        var n3 = r3.createEncryptor;
      else {
        var n3 = r3.createDecryptor;
        this._minBufferSize = 1;
      }
      this._mode && this._mode.__creator == n3 ? this._mode.init(this, t3 && t3.words) : (this._mode = n3.call(r3, this, t3 && t3.words), this._mode.__creator = n3);
    },
    _doProcessBlock: function(e3, t3) {
      this._mode.processBlock(e3, t3);
    },
    _doFinalize: function() {
      var e3 = this.cfg.padding;
      if (this._xformMode == this._ENC_XFORM_MODE) {
        e3.pad(this._data, this.blockSize);
        var t3 = this._process(true);
      } else {
        var t3 = this._process(true);
        e3.unpad(t3);
      }
      return t3;
    },
    blockSize: 4
  }), b2 = o2.CipherParams = a2.extend({
    init: function(e3) {
      this.mixIn(e3);
    },
    toString: function(e3) {
      return (e3 || this.formatter).stringify(this);
    }
  }), y2 = i2.format = {}, m2 = y2.OpenSSL = {
    stringify: function(e3) {
      var t3 = e3.ciphertext, r3 = e3.salt;
      if (r3)
        var n3 = s2.create([1398893684, 1701076831]).concat(r3).concat(t3);
      else
        var n3 = t3;
      return n3.toString(f2);
    },
    parse: function(e3) {
      var t3 = f2.parse(e3), r3 = t3.words;
      if (1398893684 == r3[0] && 1701076831 == r3[1]) {
        var n3 = s2.create(r3.slice(2, 4));
        r3.splice(0, 4), t3.sigBytes -= 16;
      }
      return b2.create({
        ciphertext: t3,
        salt: n3
      });
    }
  }, k2 = o2.SerializableCipher = a2.extend({
    cfg: a2.extend({
      format: m2
    }),
    encrypt: function(e3, t3, r3, n3) {
      n3 = this.cfg.extend(n3);
      var i3 = e3.createEncryptor(r3, n3), o3 = i3.finalize(t3), a3 = i3.cfg;
      return b2.create({
        ciphertext: o3,
        key: r3,
        iv: a3.iv,
        algorithm: e3,
        mode: a3.mode,
        padding: a3.padding,
        blockSize: e3.blockSize,
        formatter: n3.format
      });
    },
    decrypt: function(e3, t3, r3, n3) {
      n3 = this.cfg.extend(n3), t3 = this._parse(t3, n3.format);
      var i3 = e3.createDecryptor(r3, n3).finalize(t3.ciphertext);
      return i3;
    },
    _parse: function(e3, t3) {
      return "string" == typeof e3 ? t3.parse(e3, this) : e3;
    }
  }), E2 = i2.kdf = {}, S2 = E2.OpenSSL = {
    execute: function(e3, t3, r3, n3) {
      n3 || (n3 = s2.random(8));
      var i3 = u2.create({
        keySize: t3 + r3
      }).compute(e3, n3), o3 = s2.create(i3.words.slice(t3), 4 * r3);
      return i3.sigBytes = 4 * t3, b2.create({
        key: i3,
        iv: o3,
        salt: n3
      });
    }
  }, x2 = o2.PasswordBasedCipher = k2.extend({
    cfg: k2.cfg.extend({
      kdf: S2
    }),
    encrypt: function(e3, t3, r3, n3) {
      var i3 = (n3 = this.cfg.extend(n3)).kdf.execute(r3, e3.keySize, e3.ivSize);
      n3.iv = i3.iv;
      var o3 = k2.encrypt.call(this, e3, t3, i3.key, n3);
      return o3.mixIn(i3), o3;
    },
    decrypt: function(e3, t3, r3, n3) {
      n3 = this.cfg.extend(n3), t3 = this._parse(t3, n3.format);
      var i3 = n3.kdf.execute(r3, e3.keySize, e3.ivSize, t3.salt);
      n3.iv = i3.iv;
      var o3 = k2.decrypt.call(this, e3, t3, i3.key, n3);
      return o3;
    }
  })));
}), Q(function(e2, t2) {
  var r2;
  e2.exports = ((r2 = ee).mode.CFB = function() {
    var e3 = r2.lib.BlockCipherMode.extend();
    function t3(e4, t4, r3, n2) {
      var i2 = this._iv;
      if (i2) {
        var o2 = i2.slice(0);
        this._iv = void 0;
      } else
        o2 = this._prevBlock;
      n2.encryptBlock(o2, 0);
      for (var a2 = 0; a2 < r3; a2++)
        e4[t4 + a2] ^= o2[a2];
    }
    return e3.Encryptor = e3.extend({
      processBlock: function(e4, r3) {
        var n2 = this._cipher, i2 = n2.blockSize;
        t3.call(this, e4, r3, i2, n2), this._prevBlock = e4.slice(r3, r3 + i2);
      }
    }), e3.Decryptor = e3.extend({
      processBlock: function(e4, r3) {
        var n2 = this._cipher, i2 = n2.blockSize, o2 = e4.slice(r3, r3 + i2);
        t3.call(this, e4, r3, i2, n2), this._prevBlock = o2;
      }
    }), e3;
  }(), r2.mode.CFB);
}), Q(function(e2, t2) {
  var r2, n2, i2;
  e2.exports = ((r2 = ee).mode.CTR = (n2 = r2.lib.BlockCipherMode.extend(), i2 = n2.Encryptor = n2.extend({
    processBlock: function(e3, t3) {
      var r3 = this._cipher, n3 = r3.blockSize, i3 = this._iv, o2 = this._counter;
      i3 && (o2 = this._counter = i3.slice(0), this._iv = void 0);
      var a2 = o2.slice(0);
      r3.encryptBlock(a2, 0), o2[n3 - 1] = o2[n3 - 1] + 1 | 0;
      for (var s2 = 0; s2 < n3; s2++)
        e3[t3 + s2] ^= a2[s2];
    }
  }), n2.Decryptor = i2, n2), r2.mode.CTR);
}), Q(function(e2, t2) {
  var r2;
  e2.exports = ((r2 = ee).mode.CTRGladman = function() {
    var e3 = r2.lib.BlockCipherMode.extend();
    function t3(e4) {
      if (255 == (e4 >> 24 & 255)) {
        var t4 = e4 >> 16 & 255, r3 = e4 >> 8 & 255, n3 = 255 & e4;
        255 === t4 ? (t4 = 0, 255 === r3 ? (r3 = 0, 255 === n3 ? n3 = 0 : ++n3) : ++r3) : ++t4, e4 = 0, e4 += t4 << 16, e4 += r3 << 8, e4 += n3;
      } else
        e4 += 1 << 24;
      return e4;
    }
    var n2 = e3.Encryptor = e3.extend({
      processBlock: function(e4, r3) {
        var n3 = this._cipher, i2 = n3.blockSize, o2 = this._iv, a2 = this._counter;
        o2 && (a2 = this._counter = o2.slice(0), this._iv = void 0), function(e5) {
          0 === (e5[0] = t3(e5[0])) && (e5[1] = t3(e5[1]));
        }(a2);
        var s2 = a2.slice(0);
        n3.encryptBlock(s2, 0);
        for (var h2 = 0; h2 < i2; h2++)
          e4[r3 + h2] ^= s2[h2];
      }
    });
    return e3.Decryptor = n2, e3;
  }(), r2.mode.CTRGladman);
}), Q(function(e2, t2) {
  var r2, n2, i2;
  e2.exports = ((r2 = ee).mode.OFB = (n2 = r2.lib.BlockCipherMode.extend(), i2 = n2.Encryptor = n2.extend({
    processBlock: function(e3, t3) {
      var r3 = this._cipher, n3 = r3.blockSize, i3 = this._iv, o2 = this._keystream;
      i3 && (o2 = this._keystream = i3.slice(0), this._iv = void 0), r3.encryptBlock(o2, 0);
      for (var a2 = 0; a2 < n3; a2++)
        e3[t3 + a2] ^= o2[a2];
    }
  }), n2.Decryptor = i2, n2), r2.mode.OFB);
}), Q(function(e2, t2) {
  var r2, n2;
  e2.exports = ((r2 = ee).mode.ECB = ((n2 = r2.lib.BlockCipherMode.extend()).Encryptor = n2.extend({
    processBlock: function(e3, t3) {
      this._cipher.encryptBlock(e3, t3);
    }
  }), n2.Decryptor = n2.extend({
    processBlock: function(e3, t3) {
      this._cipher.decryptBlock(e3, t3);
    }
  }), n2), r2.mode.ECB);
}), Q(function(e2, t2) {
  var r2;
  e2.exports = ((r2 = ee).pad.AnsiX923 = {
    pad: function(e3, t3) {
      var r3 = e3.sigBytes, n2 = 4 * t3, i2 = n2 - r3 % n2, o2 = r3 + i2 - 1;
      e3.clamp(), e3.words[o2 >>> 2] |= i2 << 24 - o2 % 4 * 8, e3.sigBytes += i2;
    },
    unpad: function(e3) {
      var t3 = 255 & e3.words[e3.sigBytes - 1 >>> 2];
      e3.sigBytes -= t3;
    }
  }, r2.pad.Ansix923);
}), Q(function(e2, t2) {
  var r2;
  e2.exports = ((r2 = ee).pad.Iso10126 = {
    pad: function(e3, t3) {
      var n2 = 4 * t3, i2 = n2 - e3.sigBytes % n2;
      e3.concat(r2.lib.WordArray.random(i2 - 1)).concat(r2.lib.WordArray.create([i2 << 24], 1));
    },
    unpad: function(e3) {
      var t3 = 255 & e3.words[e3.sigBytes - 1 >>> 2];
      e3.sigBytes -= t3;
    }
  }, r2.pad.Iso10126);
}), Q(function(e2, t2) {
  var r2;
  e2.exports = ((r2 = ee).pad.Iso97971 = {
    pad: function(e3, t3) {
      e3.concat(r2.lib.WordArray.create([2147483648], 1)), r2.pad.ZeroPadding.pad(e3, t3);
    },
    unpad: function(e3) {
      r2.pad.ZeroPadding.unpad(e3), e3.sigBytes--;
    }
  }, r2.pad.Iso97971);
}), Q(function(e2, t2) {
  var r2;
  e2.exports = ((r2 = ee).pad.ZeroPadding = {
    pad: function(e3, t3) {
      var r3 = 4 * t3;
      e3.clamp(), e3.sigBytes += r3 - (e3.sigBytes % r3 || r3);
    },
    unpad: function(e3) {
      for (var t3 = e3.words, r3 = e3.sigBytes - 1; !(t3[r3 >>> 2] >>> 24 - r3 % 4 * 8 & 255); )
        r3--;
      e3.sigBytes = r3 + 1;
    }
  }, r2.pad.ZeroPadding);
}), Q(function(e2, t2) {
  var r2;
  e2.exports = ((r2 = ee).pad.NoPadding = {
    pad: function() {
    },
    unpad: function() {
    }
  }, r2.pad.NoPadding);
}), Q(function(e2, t2) {
  var r2, n2, i2, o2;
  e2.exports = (i2 = (n2 = r2 = ee).lib.CipherParams, o2 = n2.enc.Hex, n2.format.Hex = {
    stringify: function(e3) {
      return e3.ciphertext.toString(o2);
    },
    parse: function(e3) {
      var t3 = o2.parse(e3);
      return i2.create({
        ciphertext: t3
      });
    }
  }, r2.format.Hex);
}), Q(function(e2, t2) {
  var r2;
  e2.exports = (r2 = ee, function() {
    var e3 = r2, t3 = e3.lib.BlockCipher, n2 = e3.algo, i2 = [], o2 = [], a2 = [], s2 = [], h2 = [], l2 = [], f2 = [], c2 = [], u2 = [], d2 = [];
    !function() {
      for (var e4 = [], t4 = 0; t4 < 256; t4++)
        e4[t4] = t4 < 128 ? t4 << 1 : t4 << 1 ^ 283;
      var r3 = 0, n3 = 0;
      for (t4 = 0; t4 < 256; t4++) {
        var p3 = n3 ^ n3 << 1 ^ n3 << 2 ^ n3 << 3 ^ n3 << 4;
        p3 = p3 >>> 8 ^ 255 & p3 ^ 99, i2[r3] = p3, o2[p3] = r3;
        var _3 = e4[r3], g2 = e4[_3], v2 = e4[g2], w2 = 257 * e4[p3] ^ 16843008 * p3;
        a2[r3] = w2 << 24 | w2 >>> 8, s2[r3] = w2 << 16 | w2 >>> 16, h2[r3] = w2 << 8 | w2 >>> 24, l2[r3] = w2, w2 = 16843009 * v2 ^ 65537 * g2 ^ 257 * _3 ^ 16843008 * r3, f2[p3] = w2 << 24 | w2 >>> 8, c2[p3] = w2 << 16 | w2 >>> 16, u2[p3] = w2 << 8 | w2 >>> 24, d2[p3] = w2, r3 ? (r3 = _3 ^ e4[e4[e4[v2 ^ _3]]], n3 ^= e4[e4[n3]]) : r3 = n3 = 1;
      }
    }();
    var p2 = [0, 1, 2, 4, 8, 16, 32, 64, 128, 27, 54], _2 = n2.AES = t3.extend({
      _doReset: function() {
        if (!this._nRounds || this._keyPriorReset !== this._key) {
          for (var e4 = this._keyPriorReset = this._key, t4 = e4.words, r3 = e4.sigBytes / 4, n3 = 4 * ((this._nRounds = r3 + 6) + 1), o3 = this._keySchedule = [], a3 = 0; a3 < n3; a3++)
            if (a3 < r3)
              o3[a3] = t4[a3];
            else {
              var s3 = o3[a3 - 1];
              a3 % r3 ? r3 > 6 && a3 % r3 == 4 && (s3 = i2[s3 >>> 24] << 24 | i2[s3 >>> 16 & 255] << 16 | i2[s3 >>> 8 & 255] << 8 | i2[255 & s3]) : (s3 = i2[(s3 = s3 << 8 | s3 >>> 24) >>> 24] << 24 | i2[s3 >>> 16 & 255] << 16 | i2[s3 >>> 8 & 255] << 8 | i2[255 & s3], s3 ^= p2[a3 / r3 | 0] << 24), o3[a3] = o3[a3 - r3] ^ s3;
            }
          for (var h3 = this._invKeySchedule = [], l3 = 0; l3 < n3; l3++)
            a3 = n3 - l3, s3 = l3 % 4 ? o3[a3] : o3[a3 - 4], h3[l3] = l3 < 4 || a3 <= 4 ? s3 : f2[i2[s3 >>> 24]] ^ c2[i2[s3 >>> 16 & 255]] ^ u2[i2[s3 >>> 8 & 255]] ^ d2[i2[255 & s3]];
        }
      },
      encryptBlock: function(e4, t4) {
        this._doCryptBlock(e4, t4, this._keySchedule, a2, s2, h2, l2, i2);
      },
      decryptBlock: function(e4, t4) {
        var r3 = e4[t4 + 1];
        e4[t4 + 1] = e4[t4 + 3], e4[t4 + 3] = r3, this._doCryptBlock(e4, t4, this._invKeySchedule, f2, c2, u2, d2, o2), r3 = e4[t4 + 1], e4[t4 + 1] = e4[t4 + 3], e4[t4 + 3] = r3;
      },
      _doCryptBlock: function(e4, t4, r3, n3, i3, o3, a3, s3) {
        for (var h3 = this._nRounds, l3 = e4[t4] ^ r3[0], f3 = e4[t4 + 1] ^ r3[1], c3 = e4[t4 + 2] ^ r3[2], u3 = e4[t4 + 3] ^ r3[3], d3 = 4, p3 = 1; p3 < h3; p3++) {
          var _3 = n3[l3 >>> 24] ^ i3[f3 >>> 16 & 255] ^ o3[c3 >>> 8 & 255] ^ a3[255 & u3] ^ r3[d3++], g2 = n3[f3 >>> 24] ^ i3[c3 >>> 16 & 255] ^ o3[u3 >>> 8 & 255] ^ a3[255 & l3] ^ r3[d3++], v2 = n3[c3 >>> 24] ^ i3[u3 >>> 16 & 255] ^ o3[l3 >>> 8 & 255] ^ a3[255 & f3] ^ r3[d3++], w2 = n3[u3 >>> 24] ^ i3[l3 >>> 16 & 255] ^ o3[f3 >>> 8 & 255] ^ a3[255 & c3] ^ r3[d3++];
          l3 = _3, f3 = g2, c3 = v2, u3 = w2;
        }
        _3 = (s3[l3 >>> 24] << 24 | s3[f3 >>> 16 & 255] << 16 | s3[c3 >>> 8 & 255] << 8 | s3[255 & u3]) ^ r3[d3++], g2 = (s3[f3 >>> 24] << 24 | s3[c3 >>> 16 & 255] << 16 | s3[u3 >>> 8 & 255] << 8 | s3[255 & l3]) ^ r3[d3++], v2 = (s3[c3 >>> 24] << 24 | s3[u3 >>> 16 & 255] << 16 | s3[l3 >>> 8 & 255] << 8 | s3[255 & f3]) ^ r3[d3++], w2 = (s3[u3 >>> 24] << 24 | s3[l3 >>> 16 & 255] << 16 | s3[f3 >>> 8 & 255] << 8 | s3[255 & c3]) ^ r3[d3++], e4[t4] = _3, e4[t4 + 1] = g2, e4[t4 + 2] = v2, e4[t4 + 3] = w2;
      },
      keySize: 8
    });
    e3.AES = t3._createHelper(_2);
  }(), r2.AES);
}), Q(function(e2, t2) {
  var r2;
  e2.exports = (r2 = ee, function() {
    var e3 = r2, t3 = e3.lib, n2 = t3.WordArray, i2 = t3.BlockCipher, o2 = e3.algo, a2 = [57, 49, 41, 33, 25, 17, 9, 1, 58, 50, 42, 34, 26, 18, 10, 2, 59, 51, 43, 35, 27, 19, 11, 3, 60, 52, 44, 36, 63, 55, 47, 39, 31, 23, 15, 7, 62, 54, 46, 38, 30, 22, 14, 6, 61, 53, 45, 37, 29, 21, 13, 5, 28, 20, 12, 4], s2 = [14, 17, 11, 24, 1, 5, 3, 28, 15, 6, 21, 10, 23, 19, 12, 4, 26, 8, 16, 7, 27, 20, 13, 2, 41, 52, 31, 37, 47, 55, 30, 40, 51, 45, 33, 48, 44, 49, 39, 56, 34, 53, 46, 42, 50, 36, 29, 32], h2 = [1, 2, 4, 6, 8, 10, 12, 14, 15, 17, 19, 21, 23, 25, 27, 28], l2 = [{
      0: 8421888,
      268435456: 32768,
      536870912: 8421378,
      805306368: 2,
      1073741824: 512,
      1342177280: 8421890,
      1610612736: 8389122,
      1879048192: 8388608,
      2147483648: 514,
      2415919104: 8389120,
      2684354560: 33280,
      2952790016: 8421376,
      3221225472: 32770,
      3489660928: 8388610,
      3758096384: 0,
      4026531840: 33282,
      134217728: 0,
      402653184: 8421890,
      671088640: 33282,
      939524096: 32768,
      1207959552: 8421888,
      1476395008: 512,
      1744830464: 8421378,
      2013265920: 2,
      2281701376: 8389120,
      2550136832: 33280,
      2818572288: 8421376,
      3087007744: 8389122,
      3355443200: 8388610,
      3623878656: 32770,
      3892314112: 514,
      4160749568: 8388608,
      1: 32768,
      268435457: 2,
      536870913: 8421888,
      805306369: 8388608,
      1073741825: 8421378,
      1342177281: 33280,
      1610612737: 512,
      1879048193: 8389122,
      2147483649: 8421890,
      2415919105: 8421376,
      2684354561: 8388610,
      2952790017: 33282,
      3221225473: 514,
      3489660929: 8389120,
      3758096385: 32770,
      4026531841: 0,
      134217729: 8421890,
      402653185: 8421376,
      671088641: 8388608,
      939524097: 512,
      1207959553: 32768,
      1476395009: 8388610,
      1744830465: 2,
      2013265921: 33282,
      2281701377: 32770,
      2550136833: 8389122,
      2818572289: 514,
      3087007745: 8421888,
      3355443201: 8389120,
      3623878657: 0,
      3892314113: 33280,
      4160749569: 8421378
    }, {
      0: 1074282512,
      16777216: 16384,
      33554432: 524288,
      50331648: 1074266128,
      67108864: 1073741840,
      83886080: 1074282496,
      100663296: 1073758208,
      117440512: 16,
      134217728: 540672,
      150994944: 1073758224,
      167772160: 1073741824,
      184549376: 540688,
      201326592: 524304,
      218103808: 0,
      234881024: 16400,
      251658240: 1074266112,
      8388608: 1073758208,
      25165824: 540688,
      41943040: 16,
      58720256: 1073758224,
      75497472: 1074282512,
      92274688: 1073741824,
      109051904: 524288,
      125829120: 1074266128,
      142606336: 524304,
      159383552: 0,
      176160768: 16384,
      192937984: 1074266112,
      209715200: 1073741840,
      226492416: 540672,
      243269632: 1074282496,
      260046848: 16400,
      268435456: 0,
      285212672: 1074266128,
      301989888: 1073758224,
      318767104: 1074282496,
      335544320: 1074266112,
      352321536: 16,
      369098752: 540688,
      385875968: 16384,
      402653184: 16400,
      419430400: 524288,
      436207616: 524304,
      452984832: 1073741840,
      469762048: 540672,
      486539264: 1073758208,
      503316480: 1073741824,
      520093696: 1074282512,
      276824064: 540688,
      293601280: 524288,
      310378496: 1074266112,
      327155712: 16384,
      343932928: 1073758208,
      360710144: 1074282512,
      377487360: 16,
      394264576: 1073741824,
      411041792: 1074282496,
      427819008: 1073741840,
      444596224: 1073758224,
      461373440: 524304,
      478150656: 0,
      494927872: 16400,
      511705088: 1074266128,
      528482304: 540672
    }, {
      0: 260,
      1048576: 0,
      2097152: 67109120,
      3145728: 65796,
      4194304: 65540,
      5242880: 67108868,
      6291456: 67174660,
      7340032: 67174400,
      8388608: 67108864,
      9437184: 67174656,
      10485760: 65792,
      11534336: 67174404,
      12582912: 67109124,
      13631488: 65536,
      14680064: 4,
      15728640: 256,
      524288: 67174656,
      1572864: 67174404,
      2621440: 0,
      3670016: 67109120,
      4718592: 67108868,
      5767168: 65536,
      6815744: 65540,
      7864320: 260,
      8912896: 4,
      9961472: 256,
      11010048: 67174400,
      12058624: 65796,
      13107200: 65792,
      14155776: 67109124,
      15204352: 67174660,
      16252928: 67108864,
      16777216: 67174656,
      17825792: 65540,
      18874368: 65536,
      19922944: 67109120,
      20971520: 256,
      22020096: 67174660,
      23068672: 67108868,
      24117248: 0,
      25165824: 67109124,
      26214400: 67108864,
      27262976: 4,
      28311552: 65792,
      29360128: 67174400,
      30408704: 260,
      31457280: 65796,
      32505856: 67174404,
      17301504: 67108864,
      18350080: 260,
      19398656: 67174656,
      20447232: 0,
      21495808: 65540,
      22544384: 67109120,
      23592960: 256,
      24641536: 67174404,
      25690112: 65536,
      26738688: 67174660,
      27787264: 65796,
      28835840: 67108868,
      29884416: 67109124,
      30932992: 67174400,
      31981568: 4,
      33030144: 65792
    }, {
      0: 2151682048,
      65536: 2147487808,
      131072: 4198464,
      196608: 2151677952,
      262144: 0,
      327680: 4198400,
      393216: 2147483712,
      458752: 4194368,
      524288: 2147483648,
      589824: 4194304,
      655360: 64,
      720896: 2147487744,
      786432: 2151678016,
      851968: 4160,
      917504: 4096,
      983040: 2151682112,
      32768: 2147487808,
      98304: 64,
      163840: 2151678016,
      229376: 2147487744,
      294912: 4198400,
      360448: 2151682112,
      425984: 0,
      491520: 2151677952,
      557056: 4096,
      622592: 2151682048,
      688128: 4194304,
      753664: 4160,
      819200: 2147483648,
      884736: 4194368,
      950272: 4198464,
      1015808: 2147483712,
      1048576: 4194368,
      1114112: 4198400,
      1179648: 2147483712,
      1245184: 0,
      1310720: 4160,
      1376256: 2151678016,
      1441792: 2151682048,
      1507328: 2147487808,
      1572864: 2151682112,
      1638400: 2147483648,
      1703936: 2151677952,
      1769472: 4198464,
      1835008: 2147487744,
      1900544: 4194304,
      1966080: 64,
      2031616: 4096,
      1081344: 2151677952,
      1146880: 2151682112,
      1212416: 0,
      1277952: 4198400,
      1343488: 4194368,
      1409024: 2147483648,
      1474560: 2147487808,
      1540096: 64,
      1605632: 2147483712,
      1671168: 4096,
      1736704: 2147487744,
      1802240: 2151678016,
      1867776: 4160,
      1933312: 2151682048,
      1998848: 4194304,
      2064384: 4198464
    }, {
      0: 128,
      4096: 17039360,
      8192: 262144,
      12288: 536870912,
      16384: 537133184,
      20480: 16777344,
      24576: 553648256,
      28672: 262272,
      32768: 16777216,
      36864: 537133056,
      40960: 536871040,
      45056: 553910400,
      49152: 553910272,
      53248: 0,
      57344: 17039488,
      61440: 553648128,
      2048: 17039488,
      6144: 553648256,
      10240: 128,
      14336: 17039360,
      18432: 262144,
      22528: 537133184,
      26624: 553910272,
      30720: 536870912,
      34816: 537133056,
      38912: 0,
      43008: 553910400,
      47104: 16777344,
      51200: 536871040,
      55296: 553648128,
      59392: 16777216,
      63488: 262272,
      65536: 262144,
      69632: 128,
      73728: 536870912,
      77824: 553648256,
      81920: 16777344,
      86016: 553910272,
      90112: 537133184,
      94208: 16777216,
      98304: 553910400,
      102400: 553648128,
      106496: 17039360,
      110592: 537133056,
      114688: 262272,
      118784: 536871040,
      122880: 0,
      126976: 17039488,
      67584: 553648256,
      71680: 16777216,
      75776: 17039360,
      79872: 537133184,
      83968: 536870912,
      88064: 17039488,
      92160: 128,
      96256: 553910272,
      100352: 262272,
      104448: 553910400,
      108544: 0,
      112640: 553648128,
      116736: 16777344,
      120832: 262144,
      124928: 537133056,
      129024: 536871040
    }, {
      0: 268435464,
      256: 8192,
      512: 270532608,
      768: 270540808,
      1024: 268443648,
      1280: 2097152,
      1536: 2097160,
      1792: 268435456,
      2048: 0,
      2304: 268443656,
      2560: 2105344,
      2816: 8,
      3072: 270532616,
      3328: 2105352,
      3584: 8200,
      3840: 270540800,
      128: 270532608,
      384: 270540808,
      640: 8,
      896: 2097152,
      1152: 2105352,
      1408: 268435464,
      1664: 268443648,
      1920: 8200,
      2176: 2097160,
      2432: 8192,
      2688: 268443656,
      2944: 270532616,
      3200: 0,
      3456: 270540800,
      3712: 2105344,
      3968: 268435456,
      4096: 268443648,
      4352: 270532616,
      4608: 270540808,
      4864: 8200,
      5120: 2097152,
      5376: 268435456,
      5632: 268435464,
      5888: 2105344,
      6144: 2105352,
      6400: 0,
      6656: 8,
      6912: 270532608,
      7168: 8192,
      7424: 268443656,
      7680: 270540800,
      7936: 2097160,
      4224: 8,
      4480: 2105344,
      4736: 2097152,
      4992: 268435464,
      5248: 268443648,
      5504: 8200,
      5760: 270540808,
      6016: 270532608,
      6272: 270540800,
      6528: 270532616,
      6784: 8192,
      7040: 2105352,
      7296: 2097160,
      7552: 0,
      7808: 268435456,
      8064: 268443656
    }, {
      0: 1048576,
      16: 33555457,
      32: 1024,
      48: 1049601,
      64: 34604033,
      80: 0,
      96: 1,
      112: 34603009,
      128: 33555456,
      144: 1048577,
      160: 33554433,
      176: 34604032,
      192: 34603008,
      208: 1025,
      224: 1049600,
      240: 33554432,
      8: 34603009,
      24: 0,
      40: 33555457,
      56: 34604032,
      72: 1048576,
      88: 33554433,
      104: 33554432,
      120: 1025,
      136: 1049601,
      152: 33555456,
      168: 34603008,
      184: 1048577,
      200: 1024,
      216: 34604033,
      232: 1,
      248: 1049600,
      256: 33554432,
      272: 1048576,
      288: 33555457,
      304: 34603009,
      320: 1048577,
      336: 33555456,
      352: 34604032,
      368: 1049601,
      384: 1025,
      400: 34604033,
      416: 1049600,
      432: 1,
      448: 0,
      464: 34603008,
      480: 33554433,
      496: 1024,
      264: 1049600,
      280: 33555457,
      296: 34603009,
      312: 1,
      328: 33554432,
      344: 1048576,
      360: 1025,
      376: 34604032,
      392: 33554433,
      408: 34603008,
      424: 0,
      440: 34604033,
      456: 1049601,
      472: 1024,
      488: 33555456,
      504: 1048577
    }, {
      0: 134219808,
      1: 131072,
      2: 134217728,
      3: 32,
      4: 131104,
      5: 134350880,
      6: 134350848,
      7: 2048,
      8: 134348800,
      9: 134219776,
      10: 133120,
      11: 134348832,
      12: 2080,
      13: 0,
      14: 134217760,
      15: 133152,
      2147483648: 2048,
      2147483649: 134350880,
      2147483650: 134219808,
      2147483651: 134217728,
      2147483652: 134348800,
      2147483653: 133120,
      2147483654: 133152,
      2147483655: 32,
      2147483656: 134217760,
      2147483657: 2080,
      2147483658: 131104,
      2147483659: 134350848,
      2147483660: 0,
      2147483661: 134348832,
      2147483662: 134219776,
      2147483663: 131072,
      16: 133152,
      17: 134350848,
      18: 32,
      19: 2048,
      20: 134219776,
      21: 134217760,
      22: 134348832,
      23: 131072,
      24: 0,
      25: 131104,
      26: 134348800,
      27: 134219808,
      28: 134350880,
      29: 133120,
      30: 2080,
      31: 134217728,
      2147483664: 131072,
      2147483665: 2048,
      2147483666: 134348832,
      2147483667: 133152,
      2147483668: 32,
      2147483669: 134348800,
      2147483670: 134217728,
      2147483671: 134219808,
      2147483672: 134350880,
      2147483673: 134217760,
      2147483674: 134219776,
      2147483675: 0,
      2147483676: 133120,
      2147483677: 2080,
      2147483678: 131104,
      2147483679: 134350848
    }], f2 = [4160749569, 528482304, 33030144, 2064384, 129024, 8064, 504, 2147483679], c2 = o2.DES = i2.extend({
      _doReset: function() {
        for (var e4 = this._key.words, t4 = [], r3 = 0; r3 < 56; r3++) {
          var n3 = a2[r3] - 1;
          t4[r3] = e4[n3 >>> 5] >>> 31 - n3 % 32 & 1;
        }
        for (var i3 = this._subKeys = [], o3 = 0; o3 < 16; o3++) {
          var l3 = i3[o3] = [], f3 = h2[o3];
          for (r3 = 0; r3 < 24; r3++)
            l3[r3 / 6 | 0] |= t4[(s2[r3] - 1 + f3) % 28] << 31 - r3 % 6, l3[4 + (r3 / 6 | 0)] |= t4[28 + (s2[r3 + 24] - 1 + f3) % 28] << 31 - r3 % 6;
          for (l3[0] = l3[0] << 1 | l3[0] >>> 31, r3 = 1; r3 < 7; r3++)
            l3[r3] = l3[r3] >>> 4 * (r3 - 1) + 3;
          l3[7] = l3[7] << 5 | l3[7] >>> 27;
        }
        var c3 = this._invSubKeys = [];
        for (r3 = 0; r3 < 16; r3++)
          c3[r3] = i3[15 - r3];
      },
      encryptBlock: function(e4, t4) {
        this._doCryptBlock(e4, t4, this._subKeys);
      },
      decryptBlock: function(e4, t4) {
        this._doCryptBlock(e4, t4, this._invSubKeys);
      },
      _doCryptBlock: function(e4, t4, r3) {
        this._lBlock = e4[t4], this._rBlock = e4[t4 + 1], u2.call(this, 4, 252645135), u2.call(this, 16, 65535), d2.call(this, 2, 858993459), d2.call(this, 8, 16711935), u2.call(this, 1, 1431655765);
        for (var n3 = 0; n3 < 16; n3++) {
          for (var i3 = r3[n3], o3 = this._lBlock, a3 = this._rBlock, s3 = 0, h3 = 0; h3 < 8; h3++)
            s3 |= l2[h3][((a3 ^ i3[h3]) & f2[h3]) >>> 0];
          this._lBlock = a3, this._rBlock = o3 ^ s3;
        }
        var c3 = this._lBlock;
        this._lBlock = this._rBlock, this._rBlock = c3, u2.call(this, 1, 1431655765), d2.call(this, 8, 16711935), d2.call(this, 2, 858993459), u2.call(this, 16, 65535), u2.call(this, 4, 252645135), e4[t4] = this._lBlock, e4[t4 + 1] = this._rBlock;
      },
      keySize: 2,
      ivSize: 2,
      blockSize: 2
    });
    function u2(e4, t4) {
      var r3 = (this._lBlock >>> e4 ^ this._rBlock) & t4;
      this._rBlock ^= r3, this._lBlock ^= r3 << e4;
    }
    function d2(e4, t4) {
      var r3 = (this._rBlock >>> e4 ^ this._lBlock) & t4;
      this._lBlock ^= r3, this._rBlock ^= r3 << e4;
    }
    e3.DES = i2._createHelper(c2);
    var p2 = o2.TripleDES = i2.extend({
      _doReset: function() {
        var e4 = this._key.words;
        this._des1 = c2.createEncryptor(n2.create(e4.slice(0, 2))), this._des2 = c2.createEncryptor(n2.create(e4.slice(2, 4))), this._des3 = c2.createEncryptor(n2.create(e4.slice(4, 6)));
      },
      encryptBlock: function(e4, t4) {
        this._des1.encryptBlock(e4, t4), this._des2.decryptBlock(e4, t4), this._des3.encryptBlock(e4, t4);
      },
      decryptBlock: function(e4, t4) {
        this._des3.decryptBlock(e4, t4), this._des2.encryptBlock(e4, t4), this._des1.decryptBlock(e4, t4);
      },
      keySize: 6,
      ivSize: 2,
      blockSize: 2
    });
    e3.TripleDES = i2._createHelper(p2);
  }(), r2.TripleDES);
}), Q(function(e2, t2) {
  var r2;
  e2.exports = (r2 = ee, function() {
    var e3 = r2, t3 = e3.lib.StreamCipher, n2 = e3.algo, i2 = n2.RC4 = t3.extend({
      _doReset: function() {
        for (var e4 = this._key, t4 = e4.words, r3 = e4.sigBytes, n3 = this._S = [], i3 = 0; i3 < 256; i3++)
          n3[i3] = i3;
        i3 = 0;
        for (var o3 = 0; i3 < 256; i3++) {
          var a3 = i3 % r3, s2 = t4[a3 >>> 2] >>> 24 - a3 % 4 * 8 & 255;
          o3 = (o3 + n3[i3] + s2) % 256;
          var h2 = n3[i3];
          n3[i3] = n3[o3], n3[o3] = h2;
        }
        this._i = this._j = 0;
      },
      _doProcessBlock: function(e4, t4) {
        e4[t4] ^= o2.call(this);
      },
      keySize: 8,
      ivSize: 0
    });
    function o2() {
      for (var e4 = this._S, t4 = this._i, r3 = this._j, n3 = 0, i3 = 0; i3 < 4; i3++) {
        r3 = (r3 + e4[t4 = (t4 + 1) % 256]) % 256;
        var o3 = e4[t4];
        e4[t4] = e4[r3], e4[r3] = o3, n3 |= e4[(e4[t4] + e4[r3]) % 256] << 24 - 8 * i3;
      }
      return this._i = t4, this._j = r3, n3;
    }
    e3.RC4 = t3._createHelper(i2);
    var a2 = n2.RC4Drop = i2.extend({
      cfg: i2.cfg.extend({
        drop: 192
      }),
      _doReset: function() {
        i2._doReset.call(this);
        for (var e4 = this.cfg.drop; e4 > 0; e4--)
          o2.call(this);
      }
    });
    e3.RC4Drop = t3._createHelper(a2);
  }(), r2.RC4);
}), Q(function(e2, t2) {
  var r2;
  e2.exports = (r2 = ee, function() {
    var e3 = r2, t3 = e3.lib.StreamCipher, n2 = e3.algo, i2 = [], o2 = [], a2 = [], s2 = n2.Rabbit = t3.extend({
      _doReset: function() {
        for (var e4 = this._key.words, t4 = this.cfg.iv, r3 = 0; r3 < 4; r3++)
          e4[r3] = 16711935 & (e4[r3] << 8 | e4[r3] >>> 24) | 4278255360 & (e4[r3] << 24 | e4[r3] >>> 8);
        var n3 = this._X = [e4[0], e4[3] << 16 | e4[2] >>> 16, e4[1], e4[0] << 16 | e4[3] >>> 16, e4[2], e4[1] << 16 | e4[0] >>> 16, e4[3], e4[2] << 16 | e4[1] >>> 16], i3 = this._C = [e4[2] << 16 | e4[2] >>> 16, 4294901760 & e4[0] | 65535 & e4[1], e4[3] << 16 | e4[3] >>> 16, 4294901760 & e4[1] | 65535 & e4[2], e4[0] << 16 | e4[0] >>> 16, 4294901760 & e4[2] | 65535 & e4[3], e4[1] << 16 | e4[1] >>> 16, 4294901760 & e4[3] | 65535 & e4[0]];
        for (this._b = 0, r3 = 0; r3 < 4; r3++)
          h2.call(this);
        for (r3 = 0; r3 < 8; r3++)
          i3[r3] ^= n3[r3 + 4 & 7];
        if (t4) {
          var o3 = t4.words, a3 = o3[0], s3 = o3[1], l2 = 16711935 & (a3 << 8 | a3 >>> 24) | 4278255360 & (a3 << 24 | a3 >>> 8), f2 = 16711935 & (s3 << 8 | s3 >>> 24) | 4278255360 & (s3 << 24 | s3 >>> 8), c2 = l2 >>> 16 | 4294901760 & f2, u2 = f2 << 16 | 65535 & l2;
          for (i3[0] ^= l2, i3[1] ^= c2, i3[2] ^= f2, i3[3] ^= u2, i3[4] ^= l2, i3[5] ^= c2, i3[6] ^= f2, i3[7] ^= u2, r3 = 0; r3 < 4; r3++)
            h2.call(this);
        }
      },
      _doProcessBlock: function(e4, t4) {
        var r3 = this._X;
        h2.call(this), i2[0] = r3[0] ^ r3[5] >>> 16 ^ r3[3] << 16, i2[1] = r3[2] ^ r3[7] >>> 16 ^ r3[5] << 16, i2[2] = r3[4] ^ r3[1] >>> 16 ^ r3[7] << 16, i2[3] = r3[6] ^ r3[3] >>> 16 ^ r3[1] << 16;
        for (var n3 = 0; n3 < 4; n3++)
          i2[n3] = 16711935 & (i2[n3] << 8 | i2[n3] >>> 24) | 4278255360 & (i2[n3] << 24 | i2[n3] >>> 8), e4[t4 + n3] ^= i2[n3];
      },
      blockSize: 4,
      ivSize: 2
    });
    function h2() {
      for (var e4 = this._X, t4 = this._C, r3 = 0; r3 < 8; r3++)
        o2[r3] = t4[r3];
      for (t4[0] = t4[0] + 1295307597 + this._b | 0, t4[1] = t4[1] + 3545052371 + (t4[0] >>> 0 < o2[0] >>> 0 ? 1 : 0) | 0, t4[2] = t4[2] + 886263092 + (t4[1] >>> 0 < o2[1] >>> 0 ? 1 : 0) | 0, t4[3] = t4[3] + 1295307597 + (t4[2] >>> 0 < o2[2] >>> 0 ? 1 : 0) | 0, t4[4] = t4[4] + 3545052371 + (t4[3] >>> 0 < o2[3] >>> 0 ? 1 : 0) | 0, t4[5] = t4[5] + 886263092 + (t4[4] >>> 0 < o2[4] >>> 0 ? 1 : 0) | 0, t4[6] = t4[6] + 1295307597 + (t4[5] >>> 0 < o2[5] >>> 0 ? 1 : 0) | 0, t4[7] = t4[7] + 3545052371 + (t4[6] >>> 0 < o2[6] >>> 0 ? 1 : 0) | 0, this._b = t4[7] >>> 0 < o2[7] >>> 0 ? 1 : 0, r3 = 0; r3 < 8; r3++) {
        var n3 = e4[r3] + t4[r3], i3 = 65535 & n3, s3 = n3 >>> 16, h3 = ((i3 * i3 >>> 17) + i3 * s3 >>> 15) + s3 * s3, l2 = ((4294901760 & n3) * n3 | 0) + ((65535 & n3) * n3 | 0);
        a2[r3] = h3 ^ l2;
      }
      e4[0] = a2[0] + (a2[7] << 16 | a2[7] >>> 16) + (a2[6] << 16 | a2[6] >>> 16) | 0, e4[1] = a2[1] + (a2[0] << 8 | a2[0] >>> 24) + a2[7] | 0, e4[2] = a2[2] + (a2[1] << 16 | a2[1] >>> 16) + (a2[0] << 16 | a2[0] >>> 16) | 0, e4[3] = a2[3] + (a2[2] << 8 | a2[2] >>> 24) + a2[1] | 0, e4[4] = a2[4] + (a2[3] << 16 | a2[3] >>> 16) + (a2[2] << 16 | a2[2] >>> 16) | 0, e4[5] = a2[5] + (a2[4] << 8 | a2[4] >>> 24) + a2[3] | 0, e4[6] = a2[6] + (a2[5] << 16 | a2[5] >>> 16) + (a2[4] << 16 | a2[4] >>> 16) | 0, e4[7] = a2[7] + (a2[6] << 8 | a2[6] >>> 24) + a2[5] | 0;
    }
    e3.Rabbit = t3._createHelper(s2);
  }(), r2.Rabbit);
}), Q(function(e2, t2) {
  var r2;
  e2.exports = (r2 = ee, function() {
    var e3 = r2, t3 = e3.lib.StreamCipher, n2 = e3.algo, i2 = [], o2 = [], a2 = [], s2 = n2.RabbitLegacy = t3.extend({
      _doReset: function() {
        var e4 = this._key.words, t4 = this.cfg.iv, r3 = this._X = [e4[0], e4[3] << 16 | e4[2] >>> 16, e4[1], e4[0] << 16 | e4[3] >>> 16, e4[2], e4[1] << 16 | e4[0] >>> 16, e4[3], e4[2] << 16 | e4[1] >>> 16], n3 = this._C = [e4[2] << 16 | e4[2] >>> 16, 4294901760 & e4[0] | 65535 & e4[1], e4[3] << 16 | e4[3] >>> 16, 4294901760 & e4[1] | 65535 & e4[2], e4[0] << 16 | e4[0] >>> 16, 4294901760 & e4[2] | 65535 & e4[3], e4[1] << 16 | e4[1] >>> 16, 4294901760 & e4[3] | 65535 & e4[0]];
        this._b = 0;
        for (var i3 = 0; i3 < 4; i3++)
          h2.call(this);
        for (i3 = 0; i3 < 8; i3++)
          n3[i3] ^= r3[i3 + 4 & 7];
        if (t4) {
          var o3 = t4.words, a3 = o3[0], s3 = o3[1], l2 = 16711935 & (a3 << 8 | a3 >>> 24) | 4278255360 & (a3 << 24 | a3 >>> 8), f2 = 16711935 & (s3 << 8 | s3 >>> 24) | 4278255360 & (s3 << 24 | s3 >>> 8), c2 = l2 >>> 16 | 4294901760 & f2, u2 = f2 << 16 | 65535 & l2;
          for (n3[0] ^= l2, n3[1] ^= c2, n3[2] ^= f2, n3[3] ^= u2, n3[4] ^= l2, n3[5] ^= c2, n3[6] ^= f2, n3[7] ^= u2, i3 = 0; i3 < 4; i3++)
            h2.call(this);
        }
      },
      _doProcessBlock: function(e4, t4) {
        var r3 = this._X;
        h2.call(this), i2[0] = r3[0] ^ r3[5] >>> 16 ^ r3[3] << 16, i2[1] = r3[2] ^ r3[7] >>> 16 ^ r3[5] << 16, i2[2] = r3[4] ^ r3[1] >>> 16 ^ r3[7] << 16, i2[3] = r3[6] ^ r3[3] >>> 16 ^ r3[1] << 16;
        for (var n3 = 0; n3 < 4; n3++)
          i2[n3] = 16711935 & (i2[n3] << 8 | i2[n3] >>> 24) | 4278255360 & (i2[n3] << 24 | i2[n3] >>> 8), e4[t4 + n3] ^= i2[n3];
      },
      blockSize: 4,
      ivSize: 2
    });
    function h2() {
      for (var e4 = this._X, t4 = this._C, r3 = 0; r3 < 8; r3++)
        o2[r3] = t4[r3];
      for (t4[0] = t4[0] + 1295307597 + this._b | 0, t4[1] = t4[1] + 3545052371 + (t4[0] >>> 0 < o2[0] >>> 0 ? 1 : 0) | 0, t4[2] = t4[2] + 886263092 + (t4[1] >>> 0 < o2[1] >>> 0 ? 1 : 0) | 0, t4[3] = t4[3] + 1295307597 + (t4[2] >>> 0 < o2[2] >>> 0 ? 1 : 0) | 0, t4[4] = t4[4] + 3545052371 + (t4[3] >>> 0 < o2[3] >>> 0 ? 1 : 0) | 0, t4[5] = t4[5] + 886263092 + (t4[4] >>> 0 < o2[4] >>> 0 ? 1 : 0) | 0, t4[6] = t4[6] + 1295307597 + (t4[5] >>> 0 < o2[5] >>> 0 ? 1 : 0) | 0, t4[7] = t4[7] + 3545052371 + (t4[6] >>> 0 < o2[6] >>> 0 ? 1 : 0) | 0, this._b = t4[7] >>> 0 < o2[7] >>> 0 ? 1 : 0, r3 = 0; r3 < 8; r3++) {
        var n3 = e4[r3] + t4[r3], i3 = 65535 & n3, s3 = n3 >>> 16, h3 = ((i3 * i3 >>> 17) + i3 * s3 >>> 15) + s3 * s3, l2 = ((4294901760 & n3) * n3 | 0) + ((65535 & n3) * n3 | 0);
        a2[r3] = h3 ^ l2;
      }
      e4[0] = a2[0] + (a2[7] << 16 | a2[7] >>> 16) + (a2[6] << 16 | a2[6] >>> 16) | 0, e4[1] = a2[1] + (a2[0] << 8 | a2[0] >>> 24) + a2[7] | 0, e4[2] = a2[2] + (a2[1] << 16 | a2[1] >>> 16) + (a2[0] << 16 | a2[0] >>> 16) | 0, e4[3] = a2[3] + (a2[2] << 8 | a2[2] >>> 24) + a2[1] | 0, e4[4] = a2[4] + (a2[3] << 16 | a2[3] >>> 16) + (a2[2] << 16 | a2[2] >>> 16) | 0, e4[5] = a2[5] + (a2[4] << 8 | a2[4] >>> 24) + a2[3] | 0, e4[6] = a2[6] + (a2[5] << 16 | a2[5] >>> 16) + (a2[4] << 16 | a2[4] >>> 16) | 0, e4[7] = a2[7] + (a2[6] << 8 | a2[6] >>> 24) + a2[5] | 0;
    }
    e3.RabbitLegacy = t3._createHelper(s2);
  }(), r2.RabbitLegacy);
}), Q(function(e2, t2) {
  e2.exports = ee;
}));
function re() {
  throw new Error("setTimeout has not been defined");
}
function ne() {
  throw new Error("clearTimeout has not been defined");
}
var ie = re, oe = ne;
function ae(e2) {
  if (ie === setTimeout)
    return setTimeout(e2, 0);
  if ((ie === re || !ie) && setTimeout)
    return ie = setTimeout, setTimeout(e2, 0);
  try {
    return ie(e2, 0);
  } catch (t2) {
    try {
      return ie.call(null, e2, 0);
    } catch (t3) {
      return ie.call(this, e2, 0);
    }
  }
}
"function" == typeof e.setTimeout && (ie = setTimeout), "function" == typeof e.clearTimeout && (oe = clearTimeout);
var se, he = [], le = false, fe = -1;
function ce() {
  le && se && (le = false, se.length ? he = se.concat(he) : fe = -1, he.length && ue());
}
function ue() {
  if (!le) {
    var e2 = ae(ce);
    le = true;
    for (var t2 = he.length; t2; ) {
      for (se = he, he = []; ++fe < t2; )
        se && se[fe].run();
      fe = -1, t2 = he.length;
    }
    se = null, le = false, function(e3) {
      if (oe === clearTimeout)
        return clearTimeout(e3);
      if ((oe === ne || !oe) && clearTimeout)
        return oe = clearTimeout, clearTimeout(e3);
      try {
        oe(e3);
      } catch (t3) {
        try {
          return oe.call(null, e3);
        } catch (t4) {
          return oe.call(this, e3);
        }
      }
    }(e2);
  }
}
function de(e2) {
  var t2 = new Array(arguments.length - 1);
  if (arguments.length > 1)
    for (var r2 = 1; r2 < arguments.length; r2++)
      t2[r2 - 1] = arguments[r2];
  he.push(new pe(e2, t2)), 1 !== he.length || le || ae(ue);
}
function pe(e2, t2) {
  this.fun = e2, this.array = t2;
}
pe.prototype.run = function() {
  this.fun.apply(null, this.array);
};
function ge() {
}
function ve() {
  ve.init.call(this);
}
function we(e2) {
  return void 0 === e2._maxListeners ? ve.defaultMaxListeners : e2._maxListeners;
}
function be(e2, t2, r2) {
  if (t2)
    e2.call(r2);
  else
    for (var n2 = e2.length, i2 = Ae(e2, n2), o2 = 0; o2 < n2; ++o2)
      i2[o2].call(r2);
}
function ye(e2, t2, r2, n2) {
  if (t2)
    e2.call(r2, n2);
  else
    for (var i2 = e2.length, o2 = Ae(e2, i2), a2 = 0; a2 < i2; ++a2)
      o2[a2].call(r2, n2);
}
function me(e2, t2, r2, n2, i2) {
  if (t2)
    e2.call(r2, n2, i2);
  else
    for (var o2 = e2.length, a2 = Ae(e2, o2), s2 = 0; s2 < o2; ++s2)
      a2[s2].call(r2, n2, i2);
}
function ke(e2, t2, r2, n2, i2, o2) {
  if (t2)
    e2.call(r2, n2, i2, o2);
  else
    for (var a2 = e2.length, s2 = Ae(e2, a2), h2 = 0; h2 < a2; ++h2)
      s2[h2].call(r2, n2, i2, o2);
}
function Ee(e2, t2, r2, n2) {
  if (t2)
    e2.apply(r2, n2);
  else
    for (var i2 = e2.length, o2 = Ae(e2, i2), a2 = 0; a2 < i2; ++a2)
      o2[a2].apply(r2, n2);
}
function Se(e2, t2, r2, n2) {
  var i2, o2, a2, s2;
  if ("function" != typeof r2)
    throw new TypeError('"listener" argument must be a function');
  if ((o2 = e2._events) ? (o2.newListener && (e2.emit("newListener", t2, r2.listener ? r2.listener : r2), o2 = e2._events), a2 = o2[t2]) : (o2 = e2._events = new ge(), e2._eventsCount = 0), a2) {
    if ("function" == typeof a2 ? a2 = o2[t2] = n2 ? [r2, a2] : [a2, r2] : n2 ? a2.unshift(r2) : a2.push(r2), !a2.warned && (i2 = we(e2)) && i2 > 0 && a2.length > i2) {
      a2.warned = true;
      var h2 = new Error("Possible EventEmitter memory leak detected. " + a2.length + " " + t2 + " listeners added. Use emitter.setMaxListeners() to increase limit");
      h2.name = "MaxListenersExceededWarning", h2.emitter = e2, h2.type = t2, h2.count = a2.length, s2 = h2, "function" == typeof console.warn ? formatAppLog("warn", "at debug/lib-generate-test-usersig-es.min.js:3130", s2) : formatAppLog("log", "at debug/lib-generate-test-usersig-es.min.js:3130", s2);
    }
  } else
    a2 = o2[t2] = r2, ++e2._eventsCount;
  return e2;
}
function xe(e2, t2, r2) {
  var n2 = false;
  function i2() {
    e2.removeListener(t2, i2), n2 || (n2 = true, r2.apply(e2, arguments));
  }
  return i2.listener = r2, i2;
}
function Re(e2) {
  var t2 = this._events;
  if (t2) {
    var r2 = t2[e2];
    if ("function" == typeof r2)
      return 1;
    if (r2)
      return r2.length;
  }
  return 0;
}
function Ae(e2, t2) {
  for (var r2 = new Array(t2); t2--; )
    r2[t2] = e2[t2];
  return r2;
}
ge.prototype = /* @__PURE__ */ Object.create(null), ve.EventEmitter = ve, ve.usingDomains = false, ve.prototype.domain = void 0, ve.prototype._events = void 0, ve.prototype._maxListeners = void 0, ve.defaultMaxListeners = 10, ve.init = function() {
  this.domain = null, ve.usingDomains && (void 0).active && (void 0).Domain, this._events && this._events !== Object.getPrototypeOf(this)._events || (this._events = new ge(), this._eventsCount = 0), this._maxListeners = this._maxListeners || void 0;
}, ve.prototype.setMaxListeners = function(e2) {
  if ("number" != typeof e2 || e2 < 0 || isNaN(e2))
    throw new TypeError('"n" argument must be a positive number');
  return this._maxListeners = e2, this;
}, ve.prototype.getMaxListeners = function() {
  return we(this);
}, ve.prototype.emit = function(e2) {
  var t2, r2, n2, i2, o2, a2, s2, h2 = "error" === e2;
  if (a2 = this._events)
    h2 = h2 && null == a2.error;
  else if (!h2)
    return false;
  if (s2 = this.domain, h2) {
    if (t2 = arguments[1], !s2) {
      if (t2 instanceof Error)
        throw t2;
      var l2 = new Error('Uncaught, unspecified "error" event. (' + t2 + ")");
      throw l2.context = t2, l2;
    }
    return t2 || (t2 = new Error('Uncaught, unspecified "error" event')), t2.domainEmitter = this, t2.domain = s2, t2.domainThrown = false, s2.emit("error", t2), false;
  }
  if (!(r2 = a2[e2]))
    return false;
  var f2 = "function" == typeof r2;
  switch (n2 = arguments.length) {
    case 1:
      be(r2, f2, this);
      break;
    case 2:
      ye(r2, f2, this, arguments[1]);
      break;
    case 3:
      me(r2, f2, this, arguments[1], arguments[2]);
      break;
    case 4:
      ke(r2, f2, this, arguments[1], arguments[2], arguments[3]);
      break;
    default:
      for (i2 = new Array(n2 - 1), o2 = 1; o2 < n2; o2++)
        i2[o2 - 1] = arguments[o2];
      Ee(r2, f2, this, i2);
  }
  return true;
}, ve.prototype.addListener = function(e2, t2) {
  return Se(this, e2, t2, false);
}, ve.prototype.on = ve.prototype.addListener, ve.prototype.prependListener = function(e2, t2) {
  return Se(this, e2, t2, true);
}, ve.prototype.once = function(e2, t2) {
  if ("function" != typeof t2)
    throw new TypeError('"listener" argument must be a function');
  return this.on(e2, xe(this, e2, t2)), this;
}, ve.prototype.prependOnceListener = function(e2, t2) {
  if ("function" != typeof t2)
    throw new TypeError('"listener" argument must be a function');
  return this.prependListener(e2, xe(this, e2, t2)), this;
}, ve.prototype.removeListener = function(e2, t2) {
  var r2, n2, i2, o2, a2;
  if ("function" != typeof t2)
    throw new TypeError('"listener" argument must be a function');
  if (!(n2 = this._events))
    return this;
  if (!(r2 = n2[e2]))
    return this;
  if (r2 === t2 || r2.listener && r2.listener === t2)
    0 == --this._eventsCount ? this._events = new ge() : (delete n2[e2], n2.removeListener && this.emit("removeListener", e2, r2.listener || t2));
  else if ("function" != typeof r2) {
    for (i2 = -1, o2 = r2.length; o2-- > 0; )
      if (r2[o2] === t2 || r2[o2].listener && r2[o2].listener === t2) {
        a2 = r2[o2].listener, i2 = o2;
        break;
      }
    if (i2 < 0)
      return this;
    if (1 === r2.length) {
      if (r2[0] = void 0, 0 == --this._eventsCount)
        return this._events = new ge(), this;
      delete n2[e2];
    } else
      !function(e3, t3) {
        for (var r3 = t3, n3 = r3 + 1, i3 = e3.length; n3 < i3; r3 += 1, n3 += 1)
          e3[r3] = e3[n3];
        e3.pop();
      }(r2, i2);
    n2.removeListener && this.emit("removeListener", e2, a2 || t2);
  }
  return this;
}, ve.prototype.removeAllListeners = function(e2) {
  var t2, r2;
  if (!(r2 = this._events))
    return this;
  if (!r2.removeListener)
    return 0 === arguments.length ? (this._events = new ge(), this._eventsCount = 0) : r2[e2] && (0 == --this._eventsCount ? this._events = new ge() : delete r2[e2]), this;
  if (0 === arguments.length) {
    for (var n2, i2 = Object.keys(r2), o2 = 0; o2 < i2.length; ++o2)
      "removeListener" !== (n2 = i2[o2]) && this.removeAllListeners(n2);
    return this.removeAllListeners("removeListener"), this._events = new ge(), this._eventsCount = 0, this;
  }
  if ("function" == typeof (t2 = r2[e2]))
    this.removeListener(e2, t2);
  else if (t2)
    do {
      this.removeListener(e2, t2[t2.length - 1]);
    } while (t2[0]);
  return this;
}, ve.prototype.listeners = function(e2) {
  var t2, r2 = this._events;
  return r2 && (t2 = r2[e2]) ? "function" == typeof t2 ? [t2.listener || t2] : function(e3) {
    for (var t3 = new Array(e3.length), r3 = 0; r3 < t3.length; ++r3)
      t3[r3] = e3[r3].listener || e3[r3];
    return t3;
  }(t2) : [];
}, ve.listenerCount = function(e2, t2) {
  return "function" == typeof e2.listenerCount ? e2.listenerCount(t2) : Re.call(e2, t2);
}, ve.prototype.listenerCount = Re, ve.prototype.eventNames = function() {
  return this._eventsCount > 0 ? Reflect.ownKeys(this._events) : [];
};
var Be = "function" == typeof Object.create ? function(e2, t2) {
  e2.super_ = t2, e2.prototype = Object.create(t2.prototype, {
    constructor: {
      value: e2,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
} : function(e2, t2) {
  e2.super_ = t2;
  var r2 = function() {
  };
  r2.prototype = t2.prototype, e2.prototype = new r2(), e2.prototype.constructor = e2;
}, ze = /%[sdj%]/g;
function Le(e2) {
  if (!Ze(e2)) {
    for (var t2 = [], r2 = 0; r2 < arguments.length; r2++)
      t2.push(De(arguments[r2]));
    return t2.join(" ");
  }
  r2 = 1;
  for (var n2 = arguments, i2 = n2.length, o2 = String(e2).replace(ze, function(e3) {
    if ("%%" === e3)
      return "%";
    if (r2 >= i2)
      return e3;
    switch (e3) {
      case "%s":
        return String(n2[r2++]);
      case "%d":
        return Number(n2[r2++]);
      case "%j":
        try {
          return JSON.stringify(n2[r2++]);
        } catch (e4) {
          return "[Circular]";
        }
      default:
        return e3;
    }
  }), a2 = n2[r2]; r2 < i2; a2 = n2[++r2])
    Ne(a2) || !Ye(a2) ? o2 += " " + a2 : o2 += " " + De(a2);
  return o2;
}
function Te(t2, r2) {
  if (je(e.process))
    return function() {
      return Te(t2, r2).apply(this, arguments);
    };
  var n2 = false;
  return function() {
    return n2 || (formatAppLog("error", "at debug/lib-generate-test-usersig-es.min.js:3342", r2), n2 = true), t2.apply(this, arguments);
  };
}
var Me, Ce = {};
function De(e2, t2) {
  var r2 = {
    seen: [],
    stylize: Pe
  };
  return arguments.length >= 3 && (r2.depth = arguments[2]), arguments.length >= 4 && (r2.colors = arguments[3]), Fe(t2) ? r2.showHidden = t2 : t2 && function(e3, t3) {
    if (!t3 || !Ye(t3))
      return e3;
    var r3 = Object.keys(t3), n2 = r3.length;
    for (; n2--; )
      e3[r3[n2]] = t3[r3[n2]];
  }(r2, t2), je(r2.showHidden) && (r2.showHidden = false), je(r2.depth) && (r2.depth = 2), je(r2.colors) && (r2.colors = false), je(r2.customInspect) && (r2.customInspect = true), r2.colors && (r2.stylize = Ie), Oe(r2, e2, r2.depth);
}
function Ie(e2, t2) {
  var r2 = De.styles[t2];
  return r2 ? "\x1B[" + De.colors[r2][0] + "m" + e2 + "\x1B[" + De.colors[r2][1] + "m" : e2;
}
function Pe(e2, t2) {
  return e2;
}
function Oe(e2, t2, r2) {
  if (e2.customInspect && t2 && qe(t2.inspect) && t2.inspect !== De && (!t2.constructor || t2.constructor.prototype !== t2)) {
    var n2 = t2.inspect(r2, e2);
    return Ze(n2) || (n2 = Oe(e2, n2, r2)), n2;
  }
  var i2 = function(e3, t3) {
    if (je(t3))
      return e3.stylize("undefined", "undefined");
    if (Ze(t3)) {
      var r3 = "'" + JSON.stringify(t3).replace(/^"|"$/g, "").replace(/'/g, "\\'").replace(/\\"/g, '"') + "'";
      return e3.stylize(r3, "string");
    }
    if (n3 = t3, "number" == typeof n3)
      return e3.stylize("" + t3, "number");
    var n3;
    if (Fe(t3))
      return e3.stylize("" + t3, "boolean");
    if (Ne(t3))
      return e3.stylize("null", "null");
  }(e2, t2);
  if (i2)
    return i2;
  var o2 = Object.keys(t2), a2 = function(e3) {
    var t3 = {};
    return e3.forEach(function(e4, r3) {
      t3[e4] = true;
    }), t3;
  }(o2);
  if (e2.showHidden && (o2 = Object.getOwnPropertyNames(t2)), Xe(t2) && (o2.indexOf("message") >= 0 || o2.indexOf("description") >= 0))
    return Ue(t2);
  if (0 === o2.length) {
    if (qe(t2)) {
      var s2 = t2.name ? ": " + t2.name : "";
      return e2.stylize("[Function" + s2 + "]", "special");
    }
    if (We(t2))
      return e2.stylize(RegExp.prototype.toString.call(t2), "regexp");
    if (Ke(t2))
      return e2.stylize(Date.prototype.toString.call(t2), "date");
    if (Xe(t2))
      return Ue(t2);
  }
  var h2, l2, f2 = "", c2 = false, u2 = ["{", "}"];
  (h2 = t2, Array.isArray(h2) && (c2 = true, u2 = ["[", "]"]), qe(t2)) && (f2 = " [Function" + (t2.name ? ": " + t2.name : "") + "]");
  return We(t2) && (f2 = " " + RegExp.prototype.toString.call(t2)), Ke(t2) && (f2 = " " + Date.prototype.toUTCString.call(t2)), Xe(t2) && (f2 = " " + Ue(t2)), 0 !== o2.length || c2 && 0 != t2.length ? r2 < 0 ? We(t2) ? e2.stylize(RegExp.prototype.toString.call(t2), "regexp") : e2.stylize("[Object]", "special") : (e2.seen.push(t2), l2 = c2 ? function(e3, t3, r3, n3, i3) {
    for (var o3 = [], a3 = 0, s3 = t3.length; a3 < s3; ++a3)
      Ge(t3, String(a3)) ? o3.push(He(e3, t3, r3, n3, String(a3), true)) : o3.push("");
    return i3.forEach(function(i4) {
      i4.match(/^\d+$/) || o3.push(He(e3, t3, r3, n3, i4, true));
    }), o3;
  }(e2, t2, r2, a2, o2) : o2.map(function(n3) {
    return He(e2, t2, r2, a2, n3, c2);
  }), e2.seen.pop(), function(e3, t3, r3) {
    if (e3.reduce(function(e4, t4) {
      return t4.indexOf("\n"), e4 + t4.replace(/\u001b\[\d\d?m/g, "").length + 1;
    }, 0) > 60)
      return r3[0] + ("" === t3 ? "" : t3 + "\n ") + " " + e3.join(",\n  ") + " " + r3[1];
    return r3[0] + t3 + " " + e3.join(", ") + " " + r3[1];
  }(l2, f2, u2)) : u2[0] + f2 + u2[1];
}
function Ue(e2) {
  return "[" + Error.prototype.toString.call(e2) + "]";
}
function He(e2, t2, r2, n2, i2, o2) {
  var a2, s2, h2;
  if ((h2 = Object.getOwnPropertyDescriptor(t2, i2) || {
    value: t2[i2]
  }).get ? s2 = h2.set ? e2.stylize("[Getter/Setter]", "special") : e2.stylize("[Getter]", "special") : h2.set && (s2 = e2.stylize("[Setter]", "special")), Ge(n2, i2) || (a2 = "[" + i2 + "]"), s2 || (e2.seen.indexOf(h2.value) < 0 ? (s2 = Ne(r2) ? Oe(e2, h2.value, null) : Oe(e2, h2.value, r2 - 1)).indexOf("\n") > -1 && (s2 = o2 ? s2.split("\n").map(function(e3) {
    return "  " + e3;
  }).join("\n").substr(2) : "\n" + s2.split("\n").map(function(e3) {
    return "   " + e3;
  }).join("\n")) : s2 = e2.stylize("[Circular]", "special")), je(a2)) {
    if (o2 && i2.match(/^\d+$/))
      return s2;
    (a2 = JSON.stringify("" + i2)).match(/^"([a-zA-Z_][a-zA-Z_0-9]*)"$/) ? (a2 = a2.substr(1, a2.length - 2), a2 = e2.stylize(a2, "name")) : (a2 = a2.replace(/'/g, "\\'").replace(/\\"/g, '"').replace(/(^"|"$)/g, "'"), a2 = e2.stylize(a2, "string"));
  }
  return a2 + ": " + s2;
}
function Fe(e2) {
  return "boolean" == typeof e2;
}
function Ne(e2) {
  return null === e2;
}
function Ze(e2) {
  return "string" == typeof e2;
}
function je(e2) {
  return void 0 === e2;
}
function We(e2) {
  return Ye(e2) && "[object RegExp]" === Ve(e2);
}
function Ye(e2) {
  return "object" == typeof e2 && null !== e2;
}
function Ke(e2) {
  return Ye(e2) && "[object Date]" === Ve(e2);
}
function Xe(e2) {
  return Ye(e2) && ("[object Error]" === Ve(e2) || e2 instanceof Error);
}
function qe(e2) {
  return "function" == typeof e2;
}
function Ve(e2) {
  return Object.prototype.toString.call(e2);
}
function Ge(e2, t2) {
  return Object.prototype.hasOwnProperty.call(e2, t2);
}
function $e() {
  this.head = null, this.tail = null, this.length = 0;
}
De.colors = {
  bold: [1, 22],
  italic: [3, 23],
  underline: [4, 24],
  inverse: [7, 27],
  white: [37, 39],
  grey: [90, 39],
  black: [30, 39],
  blue: [34, 39],
  cyan: [36, 39],
  green: [32, 39],
  magenta: [35, 39],
  red: [31, 39],
  yellow: [33, 39]
}, De.styles = {
  special: "cyan",
  number: "yellow",
  boolean: "yellow",
  undefined: "grey",
  null: "bold",
  string: "green",
  date: "magenta",
  regexp: "red"
}, $e.prototype.push = function(e2) {
  var t2 = {
    data: e2,
    next: null
  };
  this.length > 0 ? this.tail.next = t2 : this.head = t2, this.tail = t2, ++this.length;
}, $e.prototype.unshift = function(e2) {
  var t2 = {
    data: e2,
    next: this.head
  };
  0 === this.length && (this.tail = t2), this.head = t2, ++this.length;
}, $e.prototype.shift = function() {
  if (0 !== this.length) {
    var e2 = this.head.data;
    return 1 === this.length ? this.head = this.tail = null : this.head = this.head.next, --this.length, e2;
  }
}, $e.prototype.clear = function() {
  this.head = this.tail = null, this.length = 0;
}, $e.prototype.join = function(e2) {
  if (0 === this.length)
    return "";
  for (var t2 = this.head, r2 = "" + t2.data; t2 = t2.next; )
    r2 += e2 + t2.data;
  return r2;
}, $e.prototype.concat = function(e2) {
  if (0 === this.length)
    return p.alloc(0);
  if (1 === this.length)
    return this.head.data;
  for (var t2 = p.allocUnsafe(e2 >>> 0), r2 = this.head, n2 = 0; r2; )
    r2.data.copy(t2, n2), n2 += r2.data.length, r2 = r2.next;
  return t2;
};
var Je = p.isEncoding || function(e2) {
  switch (e2 && e2.toLowerCase()) {
    case "hex":
    case "utf8":
    case "utf-8":
    case "ascii":
    case "binary":
    case "base64":
    case "ucs2":
    case "ucs-2":
    case "utf16le":
    case "utf-16le":
    case "raw":
      return true;
    default:
      return false;
  }
};
function Qe(e2) {
  switch (this.encoding = (e2 || "utf8").toLowerCase().replace(/[-_]/, ""), function(e3) {
    if (e3 && !Je(e3))
      throw new Error("Unknown encoding: " + e3);
  }(e2), this.encoding) {
    case "utf8":
      this.surrogateSize = 3;
      break;
    case "ucs2":
    case "utf16le":
      this.surrogateSize = 2, this.detectIncompleteChar = tt;
      break;
    case "base64":
      this.surrogateSize = 3, this.detectIncompleteChar = rt;
      break;
    default:
      return void (this.write = et);
  }
  this.charBuffer = new p(6), this.charReceived = 0, this.charLength = 0;
}
function et(e2) {
  return e2.toString(this.encoding);
}
function tt(e2) {
  this.charReceived = e2.length % 2, this.charLength = this.charReceived ? 2 : 0;
}
function rt(e2) {
  this.charReceived = e2.length % 3, this.charLength = this.charReceived ? 3 : 0;
}
Qe.prototype.write = function(e2) {
  for (var t2 = ""; this.charLength; ) {
    var r2 = e2.length >= this.charLength - this.charReceived ? this.charLength - this.charReceived : e2.length;
    if (e2.copy(this.charBuffer, this.charReceived, 0, r2), this.charReceived += r2, this.charReceived < this.charLength)
      return "";
    if (e2 = e2.slice(r2, e2.length), !((i2 = (t2 = this.charBuffer.slice(0, this.charLength).toString(this.encoding)).charCodeAt(t2.length - 1)) >= 55296 && i2 <= 56319)) {
      if (this.charReceived = this.charLength = 0, 0 === e2.length)
        return t2;
      break;
    }
    this.charLength += this.surrogateSize, t2 = "";
  }
  this.detectIncompleteChar(e2);
  var n2 = e2.length;
  this.charLength && (e2.copy(this.charBuffer, 0, e2.length - this.charReceived, n2), n2 -= this.charReceived);
  var i2;
  n2 = (t2 += e2.toString(this.encoding, 0, n2)).length - 1;
  if ((i2 = t2.charCodeAt(n2)) >= 55296 && i2 <= 56319) {
    var o2 = this.surrogateSize;
    return this.charLength += o2, this.charReceived += o2, this.charBuffer.copy(this.charBuffer, o2, 0, o2), e2.copy(this.charBuffer, 0, 0, o2), t2.substring(0, n2);
  }
  return t2;
}, Qe.prototype.detectIncompleteChar = function(e2) {
  for (var t2 = e2.length >= 3 ? 3 : e2.length; t2 > 0; t2--) {
    var r2 = e2[e2.length - t2];
    if (1 == t2 && r2 >> 5 == 6) {
      this.charLength = 2;
      break;
    }
    if (t2 <= 2 && r2 >> 4 == 14) {
      this.charLength = 3;
      break;
    }
    if (t2 <= 3 && r2 >> 3 == 30) {
      this.charLength = 4;
      break;
    }
  }
  this.charReceived = t2;
}, Qe.prototype.end = function(e2) {
  var t2 = "";
  if (e2 && e2.length && (t2 = this.write(e2)), this.charReceived) {
    var r2 = this.charReceived, n2 = this.charBuffer, i2 = this.encoding;
    t2 += n2.slice(0, r2).toString(i2);
  }
  return t2;
}, ot.ReadableState = it;
var nt = function(e2) {
  je(Me) && (Me = ""), e2 = e2.toUpperCase(), Ce[e2] || (new RegExp("\\b" + e2 + "\\b", "i").test(Me) ? Ce[e2] = function() {
    var t2 = Le.apply(null, arguments);
    formatAppLog("error", "at debug/lib-generate-test-usersig-es.min.js:3681", "%s %d: %s", e2, 0, t2);
  } : Ce[e2] = function() {
  });
  return Ce[e2];
}("stream");
function it(e2, t2) {
  e2 = e2 || {}, this.objectMode = !!e2.objectMode, t2 instanceof Ct && (this.objectMode = this.objectMode || !!e2.readableObjectMode);
  var r2 = e2.highWaterMark, n2 = this.objectMode ? 16 : 16384;
  this.highWaterMark = r2 || 0 === r2 ? r2 : n2, this.highWaterMark = ~~this.highWaterMark, this.buffer = new $e(), this.length = 0, this.pipes = null, this.pipesCount = 0, this.flowing = null, this.ended = false, this.endEmitted = false, this.reading = false, this.sync = true, this.needReadable = false, this.emittedReadable = false, this.readableListening = false, this.resumeScheduled = false, this.defaultEncoding = e2.defaultEncoding || "utf8", this.ranOut = false, this.awaitDrain = 0, this.readingMore = false, this.decoder = null, this.encoding = null, e2.encoding && (this.decoder = new Qe(e2.encoding), this.encoding = e2.encoding);
}
function ot(e2) {
  if (!(this instanceof ot))
    return new ot(e2);
  this._readableState = new it(e2, this), this.readable = true, e2 && "function" == typeof e2.read && (this._read = e2.read), ve.call(this);
}
function at(e2, t2, r2, n2, i2) {
  var o2 = function(e3, t3) {
    var r3 = null;
    $(t3) || "string" == typeof t3 || null == t3 || e3.objectMode || (r3 = new TypeError("Invalid non-string/buffer chunk"));
    return r3;
  }(t2, r2);
  if (o2)
    e2.emit("error", o2);
  else if (null === r2)
    t2.reading = false, function(e3, t3) {
      if (t3.ended)
        return;
      if (t3.decoder) {
        var r3 = t3.decoder.end();
        r3 && r3.length && (t3.buffer.push(r3), t3.length += t3.objectMode ? 1 : r3.length);
      }
      t3.ended = true, lt(e3);
    }(e2, t2);
  else if (t2.objectMode || r2 && r2.length > 0) {
    if (t2.ended && !i2) {
      var a2 = new Error("stream.push() after EOF");
      e2.emit("error", a2);
    } else if (t2.endEmitted && i2) {
      var s2 = new Error("stream.unshift() after end event");
      e2.emit("error", s2);
    } else {
      var h2;
      !t2.decoder || i2 || n2 || (r2 = t2.decoder.write(r2), h2 = !t2.objectMode && 0 === r2.length), i2 || (t2.reading = false), h2 || (t2.flowing && 0 === t2.length && !t2.sync ? (e2.emit("data", r2), e2.read(0)) : (t2.length += t2.objectMode ? 1 : r2.length, i2 ? t2.buffer.unshift(r2) : t2.buffer.push(r2), t2.needReadable && lt(e2))), function(e3, t3) {
        t3.readingMore || (t3.readingMore = true, de(ct, e3, t3));
      }(e2, t2);
    }
  } else
    i2 || (t2.reading = false);
  return function(e3) {
    return !e3.ended && (e3.needReadable || e3.length < e3.highWaterMark || 0 === e3.length);
  }(t2);
}
Be(ot, ve), ot.prototype.push = function(e2, t2) {
  var r2 = this._readableState;
  return r2.objectMode || "string" != typeof e2 || (t2 = t2 || r2.defaultEncoding) !== r2.encoding && (e2 = p.from(e2, t2), t2 = ""), at(this, r2, e2, t2, false);
}, ot.prototype.unshift = function(e2) {
  return at(this, this._readableState, e2, "", true);
}, ot.prototype.isPaused = function() {
  return false === this._readableState.flowing;
}, ot.prototype.setEncoding = function(e2) {
  return this._readableState.decoder = new Qe(e2), this._readableState.encoding = e2, this;
};
var st = 8388608;
function ht(e2, t2) {
  return e2 <= 0 || 0 === t2.length && t2.ended ? 0 : t2.objectMode ? 1 : e2 != e2 ? t2.flowing && t2.length ? t2.buffer.head.data.length : t2.length : (e2 > t2.highWaterMark && (t2.highWaterMark = function(e3) {
    return e3 >= st ? e3 = st : (e3--, e3 |= e3 >>> 1, e3 |= e3 >>> 2, e3 |= e3 >>> 4, e3 |= e3 >>> 8, e3 |= e3 >>> 16, e3++), e3;
  }(e2)), e2 <= t2.length ? e2 : t2.ended ? t2.length : (t2.needReadable = true, 0));
}
function lt(e2) {
  var t2 = e2._readableState;
  t2.needReadable = false, t2.emittedReadable || (nt("emitReadable", t2.flowing), t2.emittedReadable = true, t2.sync ? de(ft, e2) : ft(e2));
}
function ft(e2) {
  nt("emit readable"), e2.emit("readable"), pt(e2);
}
function ct(e2, t2) {
  for (var r2 = t2.length; !t2.reading && !t2.flowing && !t2.ended && t2.length < t2.highWaterMark && (nt("maybeReadMore read 0"), e2.read(0), r2 !== t2.length); )
    r2 = t2.length;
  t2.readingMore = false;
}
function ut(e2) {
  nt("readable nexttick read 0"), e2.read(0);
}
function dt(e2, t2) {
  t2.reading || (nt("resume read 0"), e2.read(0)), t2.resumeScheduled = false, t2.awaitDrain = 0, e2.emit("resume"), pt(e2), t2.flowing && !t2.reading && e2.read(0);
}
function pt(e2) {
  var t2 = e2._readableState;
  for (nt("flow", t2.flowing); t2.flowing && null !== e2.read(); )
    ;
}
function _t(e2, t2) {
  return 0 === t2.length ? null : (t2.objectMode ? r2 = t2.buffer.shift() : !e2 || e2 >= t2.length ? (r2 = t2.decoder ? t2.buffer.join("") : 1 === t2.buffer.length ? t2.buffer.head.data : t2.buffer.concat(t2.length), t2.buffer.clear()) : r2 = function(e3, t3, r3) {
    var n2;
    e3 < t3.head.data.length ? (n2 = t3.head.data.slice(0, e3), t3.head.data = t3.head.data.slice(e3)) : n2 = e3 === t3.head.data.length ? t3.shift() : r3 ? function(e4, t4) {
      var r4 = t4.head, n3 = 1, i2 = r4.data;
      e4 -= i2.length;
      for (; r4 = r4.next; ) {
        var o2 = r4.data, a2 = e4 > o2.length ? o2.length : e4;
        if (a2 === o2.length ? i2 += o2 : i2 += o2.slice(0, e4), 0 === (e4 -= a2)) {
          a2 === o2.length ? (++n3, r4.next ? t4.head = r4.next : t4.head = t4.tail = null) : (t4.head = r4, r4.data = o2.slice(a2));
          break;
        }
        ++n3;
      }
      return t4.length -= n3, i2;
    }(e3, t3) : function(e4, t4) {
      var r4 = p.allocUnsafe(e4), n3 = t4.head, i2 = 1;
      n3.data.copy(r4), e4 -= n3.data.length;
      for (; n3 = n3.next; ) {
        var o2 = n3.data, a2 = e4 > o2.length ? o2.length : e4;
        if (o2.copy(r4, r4.length - e4, 0, a2), 0 === (e4 -= a2)) {
          a2 === o2.length ? (++i2, n3.next ? t4.head = n3.next : t4.head = t4.tail = null) : (t4.head = n3, n3.data = o2.slice(a2));
          break;
        }
        ++i2;
      }
      return t4.length -= i2, r4;
    }(e3, t3);
    return n2;
  }(e2, t2.buffer, t2.decoder), r2);
  var r2;
}
function gt(e2) {
  var t2 = e2._readableState;
  if (t2.length > 0)
    throw new Error('"endReadable()" called on non-empty stream');
  t2.endEmitted || (t2.ended = true, de(vt, t2, e2));
}
function vt(e2, t2) {
  e2.endEmitted || 0 !== e2.length || (e2.endEmitted = true, t2.readable = false, t2.emit("end"));
}
function wt(e2, t2) {
  for (var r2 = 0, n2 = e2.length; r2 < n2; r2++)
    if (e2[r2] === t2)
      return r2;
  return -1;
}
function bt() {
}
function yt(e2, t2, r2) {
  this.chunk = e2, this.encoding = t2, this.callback = r2, this.next = null;
}
function mt(e2, t2) {
  Object.defineProperty(this, "buffer", {
    get: Te(function() {
      return this.getBuffer();
    }, "_writableState.buffer is deprecated. Use _writableState.getBuffer instead.")
  }), e2 = e2 || {}, this.objectMode = !!e2.objectMode, t2 instanceof Ct && (this.objectMode = this.objectMode || !!e2.writableObjectMode);
  var r2 = e2.highWaterMark, n2 = this.objectMode ? 16 : 16384;
  this.highWaterMark = r2 || 0 === r2 ? r2 : n2, this.highWaterMark = ~~this.highWaterMark, this.needDrain = false, this.ending = false, this.ended = false, this.finished = false;
  var i2 = false === e2.decodeStrings;
  this.decodeStrings = !i2, this.defaultEncoding = e2.defaultEncoding || "utf8", this.length = 0, this.writing = false, this.corked = 0, this.sync = true, this.bufferProcessing = false, this.onwrite = function(e3) {
    !function(e4, t3) {
      var r3 = e4._writableState, n3 = r3.sync, i3 = r3.writecb;
      if (function(e5) {
        e5.writing = false, e5.writecb = null, e5.length -= e5.writelen, e5.writelen = 0;
      }(r3), t3)
        !function(e5, t4, r4, n4, i4) {
          --t4.pendingcb, r4 ? de(i4, n4) : i4(n4);
          e5._writableState.errorEmitted = true, e5.emit("error", n4);
        }(e4, r3, n3, t3, i3);
      else {
        var o2 = Rt(r3);
        o2 || r3.corked || r3.bufferProcessing || !r3.bufferedRequest || xt(e4, r3), n3 ? de(St, e4, r3, o2, i3) : St(e4, r3, o2, i3);
      }
    }(t2, e3);
  }, this.writecb = null, this.writelen = 0, this.bufferedRequest = null, this.lastBufferedRequest = null, this.pendingcb = 0, this.prefinished = false, this.errorEmitted = false, this.bufferedRequestCount = 0, this.corkedRequestsFree = new zt(this);
}
function kt(e2) {
  if (!(this instanceof kt || this instanceof Ct))
    return new kt(e2);
  this._writableState = new mt(e2, this), this.writable = true, e2 && ("function" == typeof e2.write && (this._write = e2.write), "function" == typeof e2.writev && (this._writev = e2.writev)), ve.call(this);
}
function Et(e2, t2, r2, n2, i2, o2, a2) {
  t2.writelen = n2, t2.writecb = a2, t2.writing = true, t2.sync = true, r2 ? e2._writev(i2, t2.onwrite) : e2._write(i2, o2, t2.onwrite), t2.sync = false;
}
function St(e2, t2, r2, n2) {
  r2 || function(e3, t3) {
    0 === t3.length && t3.needDrain && (t3.needDrain = false, e3.emit("drain"));
  }(e2, t2), t2.pendingcb--, n2(), Bt(e2, t2);
}
function xt(e2, t2) {
  t2.bufferProcessing = true;
  var r2 = t2.bufferedRequest;
  if (e2._writev && r2 && r2.next) {
    var n2 = t2.bufferedRequestCount, i2 = new Array(n2), o2 = t2.corkedRequestsFree;
    o2.entry = r2;
    for (var a2 = 0; r2; )
      i2[a2] = r2, r2 = r2.next, a2 += 1;
    Et(e2, t2, true, t2.length, i2, "", o2.finish), t2.pendingcb++, t2.lastBufferedRequest = null, o2.next ? (t2.corkedRequestsFree = o2.next, o2.next = null) : t2.corkedRequestsFree = new zt(t2);
  } else {
    for (; r2; ) {
      var s2 = r2.chunk, h2 = r2.encoding, l2 = r2.callback;
      if (Et(e2, t2, false, t2.objectMode ? 1 : s2.length, s2, h2, l2), r2 = r2.next, t2.writing)
        break;
    }
    null === r2 && (t2.lastBufferedRequest = null);
  }
  t2.bufferedRequestCount = 0, t2.bufferedRequest = r2, t2.bufferProcessing = false;
}
function Rt(e2) {
  return e2.ending && 0 === e2.length && null === e2.bufferedRequest && !e2.finished && !e2.writing;
}
function At(e2, t2) {
  t2.prefinished || (t2.prefinished = true, e2.emit("prefinish"));
}
function Bt(e2, t2) {
  var r2 = Rt(t2);
  return r2 && (0 === t2.pendingcb ? (At(e2, t2), t2.finished = true, e2.emit("finish")) : At(e2, t2)), r2;
}
function zt(e2) {
  var t2 = this;
  this.next = null, this.entry = null, this.finish = function(r2) {
    var n2 = t2.entry;
    for (t2.entry = null; n2; ) {
      var i2 = n2.callback;
      e2.pendingcb--, i2(r2), n2 = n2.next;
    }
    e2.corkedRequestsFree ? e2.corkedRequestsFree.next = t2 : e2.corkedRequestsFree = t2;
  };
}
ot.prototype.read = function(e2) {
  nt("read", e2), e2 = parseInt(e2, 10);
  var t2 = this._readableState, r2 = e2;
  if (0 !== e2 && (t2.emittedReadable = false), 0 === e2 && t2.needReadable && (t2.length >= t2.highWaterMark || t2.ended))
    return nt("read: emitReadable", t2.length, t2.ended), 0 === t2.length && t2.ended ? gt(this) : lt(this), null;
  if (0 === (e2 = ht(e2, t2)) && t2.ended)
    return 0 === t2.length && gt(this), null;
  var n2, i2 = t2.needReadable;
  return nt("need readable", i2), (0 === t2.length || t2.length - e2 < t2.highWaterMark) && nt("length less than watermark", i2 = true), t2.ended || t2.reading ? nt("reading or ended", i2 = false) : i2 && (nt("do read"), t2.reading = true, t2.sync = true, 0 === t2.length && (t2.needReadable = true), this._read(t2.highWaterMark), t2.sync = false, t2.reading || (e2 = ht(r2, t2))), null === (n2 = e2 > 0 ? _t(e2, t2) : null) ? (t2.needReadable = true, e2 = 0) : t2.length -= e2, 0 === t2.length && (t2.ended || (t2.needReadable = true), r2 !== e2 && t2.ended && gt(this)), null !== n2 && this.emit("data", n2), n2;
}, ot.prototype._read = function(e2) {
  this.emit("error", new Error("not implemented"));
}, ot.prototype.pipe = function(e2, t2) {
  var r2 = this, n2 = this._readableState;
  switch (n2.pipesCount) {
    case 0:
      n2.pipes = e2;
      break;
    case 1:
      n2.pipes = [n2.pipes, e2];
      break;
    default:
      n2.pipes.push(e2);
  }
  n2.pipesCount += 1, nt("pipe count=%d opts=%j", n2.pipesCount, t2);
  var i2 = !t2 || false !== t2.end ? a2 : l2;
  function o2(e3) {
    nt("onunpipe"), e3 === r2 && l2();
  }
  function a2() {
    nt("onend"), e2.end();
  }
  n2.endEmitted ? de(i2) : r2.once("end", i2), e2.on("unpipe", o2);
  var s2 = function(e3) {
    return function() {
      var t3 = e3._readableState;
      nt("pipeOnDrain", t3.awaitDrain), t3.awaitDrain && t3.awaitDrain--, 0 === t3.awaitDrain && e3.listeners("data").length && (t3.flowing = true, pt(e3));
    };
  }(r2);
  e2.on("drain", s2);
  var h2 = false;
  function l2() {
    nt("cleanup"), e2.removeListener("close", d2), e2.removeListener("finish", p2), e2.removeListener("drain", s2), e2.removeListener("error", u2), e2.removeListener("unpipe", o2), r2.removeListener("end", a2), r2.removeListener("end", l2), r2.removeListener("data", c2), h2 = true, !n2.awaitDrain || e2._writableState && !e2._writableState.needDrain || s2();
  }
  var f2 = false;
  function c2(t3) {
    nt("ondata"), f2 = false, false !== e2.write(t3) || f2 || ((1 === n2.pipesCount && n2.pipes === e2 || n2.pipesCount > 1 && -1 !== wt(n2.pipes, e2)) && !h2 && (nt("false write response, pause", r2._readableState.awaitDrain), r2._readableState.awaitDrain++, f2 = true), r2.pause());
  }
  function u2(t3) {
    var r3;
    nt("onerror", t3), _2(), e2.removeListener("error", u2), 0 === (r3 = "error", e2.listeners(r3).length) && e2.emit("error", t3);
  }
  function d2() {
    e2.removeListener("finish", p2), _2();
  }
  function p2() {
    nt("onfinish"), e2.removeListener("close", d2), _2();
  }
  function _2() {
    nt("unpipe"), r2.unpipe(e2);
  }
  return r2.on("data", c2), function(e3, t3, r3) {
    if ("function" == typeof e3.prependListener)
      return e3.prependListener(t3, r3);
    e3._events && e3._events[t3] ? Array.isArray(e3._events[t3]) ? e3._events[t3].unshift(r3) : e3._events[t3] = [r3, e3._events[t3]] : e3.on(t3, r3);
  }(e2, "error", u2), e2.once("close", d2), e2.once("finish", p2), e2.emit("pipe", r2), n2.flowing || (nt("pipe resume"), r2.resume()), e2;
}, ot.prototype.unpipe = function(e2) {
  var t2 = this._readableState;
  if (0 === t2.pipesCount)
    return this;
  if (1 === t2.pipesCount)
    return e2 && e2 !== t2.pipes ? this : (e2 || (e2 = t2.pipes), t2.pipes = null, t2.pipesCount = 0, t2.flowing = false, e2 && e2.emit("unpipe", this), this);
  if (!e2) {
    var r2 = t2.pipes, n2 = t2.pipesCount;
    t2.pipes = null, t2.pipesCount = 0, t2.flowing = false;
    for (var i2 = 0; i2 < n2; i2++)
      r2[i2].emit("unpipe", this);
    return this;
  }
  var o2 = wt(t2.pipes, e2);
  return -1 === o2 ? this : (t2.pipes.splice(o2, 1), t2.pipesCount -= 1, 1 === t2.pipesCount && (t2.pipes = t2.pipes[0]), e2.emit("unpipe", this), this);
}, ot.prototype.on = function(e2, t2) {
  var r2 = ve.prototype.on.call(this, e2, t2);
  if ("data" === e2)
    false !== this._readableState.flowing && this.resume();
  else if ("readable" === e2) {
    var n2 = this._readableState;
    n2.endEmitted || n2.readableListening || (n2.readableListening = n2.needReadable = true, n2.emittedReadable = false, n2.reading ? n2.length && lt(this) : de(ut, this));
  }
  return r2;
}, ot.prototype.addListener = ot.prototype.on, ot.prototype.resume = function() {
  var e2 = this._readableState;
  return e2.flowing || (nt("resume"), e2.flowing = true, function(e3, t2) {
    t2.resumeScheduled || (t2.resumeScheduled = true, de(dt, e3, t2));
  }(this, e2)), this;
}, ot.prototype.pause = function() {
  return nt("call pause flowing=%j", this._readableState.flowing), false !== this._readableState.flowing && (nt("pause"), this._readableState.flowing = false, this.emit("pause")), this;
}, ot.prototype.wrap = function(e2) {
  var t2 = this._readableState, r2 = false, n2 = this;
  for (var i2 in e2.on("end", function() {
    if (nt("wrapped end"), t2.decoder && !t2.ended) {
      var e3 = t2.decoder.end();
      e3 && e3.length && n2.push(e3);
    }
    n2.push(null);
  }), e2.on("data", function(i3) {
    (nt("wrapped data"), t2.decoder && (i3 = t2.decoder.write(i3)), t2.objectMode && null == i3) || (t2.objectMode || i3 && i3.length) && (n2.push(i3) || (r2 = true, e2.pause()));
  }), e2)
    void 0 === this[i2] && "function" == typeof e2[i2] && (this[i2] = function(t3) {
      return function() {
        return e2[t3].apply(e2, arguments);
      };
    }(i2));
  return function(e3, t3) {
    for (var r3 = 0, n3 = e3.length; r3 < n3; r3++)
      t3(e3[r3], r3);
  }(["error", "close", "destroy", "pause", "resume"], function(t3) {
    e2.on(t3, n2.emit.bind(n2, t3));
  }), n2._read = function(t3) {
    nt("wrapped _read", t3), r2 && (r2 = false, e2.resume());
  }, n2;
}, ot._fromList = _t, kt.WritableState = mt, Be(kt, ve), mt.prototype.getBuffer = function() {
  for (var e2 = this.bufferedRequest, t2 = []; e2; )
    t2.push(e2), e2 = e2.next;
  return t2;
}, kt.prototype.pipe = function() {
  this.emit("error", new Error("Cannot pipe, not readable"));
}, kt.prototype.write = function(e2, t2, r2) {
  var n2 = this._writableState, i2 = false;
  return "function" == typeof t2 && (r2 = t2, t2 = null), p.isBuffer(e2) ? t2 = "buffer" : t2 || (t2 = n2.defaultEncoding), "function" != typeof r2 && (r2 = bt), n2.ended ? function(e3, t3) {
    var r3 = new Error("write after end");
    e3.emit("error", r3), de(t3, r3);
  }(this, r2) : function(e3, t3, r3, n3) {
    var i3 = true, o2 = false;
    return null === r3 ? o2 = new TypeError("May not write null values to stream") : p.isBuffer(r3) || "string" == typeof r3 || void 0 === r3 || t3.objectMode || (o2 = new TypeError("Invalid non-string/buffer chunk")), o2 && (e3.emit("error", o2), de(n3, o2), i3 = false), i3;
  }(this, n2, e2, r2) && (n2.pendingcb++, i2 = function(e3, t3, r3, n3, i3) {
    r3 = function(e4, t4, r4) {
      return e4.objectMode || false === e4.decodeStrings || "string" != typeof t4 || (t4 = p.from(t4, r4)), t4;
    }(t3, r3, n3), p.isBuffer(r3) && (n3 = "buffer");
    var o2 = t3.objectMode ? 1 : r3.length;
    t3.length += o2;
    var a2 = t3.length < t3.highWaterMark;
    a2 || (t3.needDrain = true);
    if (t3.writing || t3.corked) {
      var s2 = t3.lastBufferedRequest;
      t3.lastBufferedRequest = new yt(r3, n3, i3), s2 ? s2.next = t3.lastBufferedRequest : t3.bufferedRequest = t3.lastBufferedRequest, t3.bufferedRequestCount += 1;
    } else
      Et(e3, t3, false, o2, r3, n3, i3);
    return a2;
  }(this, n2, e2, t2, r2)), i2;
}, kt.prototype.cork = function() {
  this._writableState.corked++;
}, kt.prototype.uncork = function() {
  var e2 = this._writableState;
  e2.corked && (e2.corked--, e2.writing || e2.corked || e2.finished || e2.bufferProcessing || !e2.bufferedRequest || xt(this, e2));
}, kt.prototype.setDefaultEncoding = function(e2) {
  if ("string" == typeof e2 && (e2 = e2.toLowerCase()), !(["hex", "utf8", "utf-8", "ascii", "binary", "base64", "ucs2", "ucs-2", "utf16le", "utf-16le", "raw"].indexOf((e2 + "").toLowerCase()) > -1))
    throw new TypeError("Unknown encoding: " + e2);
  return this._writableState.defaultEncoding = e2, this;
}, kt.prototype._write = function(e2, t2, r2) {
  r2(new Error("not implemented"));
}, kt.prototype._writev = null, kt.prototype.end = function(e2, t2, r2) {
  var n2 = this._writableState;
  "function" == typeof e2 ? (r2 = e2, e2 = null, t2 = null) : "function" == typeof t2 && (r2 = t2, t2 = null), null != e2 && this.write(e2, t2), n2.corked && (n2.corked = 1, this.uncork()), n2.ending || n2.finished || function(e3, t3, r3) {
    t3.ending = true, Bt(e3, t3), r3 && (t3.finished ? de(r3) : e3.once("finish", r3));
    t3.ended = true, e3.writable = false;
  }(this, n2, r2);
}, Be(Ct, ot);
for (var Lt = Object.keys(kt.prototype), Tt = 0; Tt < Lt.length; Tt++) {
  var Mt = Lt[Tt];
  Ct.prototype[Mt] || (Ct.prototype[Mt] = kt.prototype[Mt]);
}
function Ct(e2) {
  if (!(this instanceof Ct))
    return new Ct(e2);
  ot.call(this, e2), kt.call(this, e2), e2 && false === e2.readable && (this.readable = false), e2 && false === e2.writable && (this.writable = false), this.allowHalfOpen = true, e2 && false === e2.allowHalfOpen && (this.allowHalfOpen = false), this.once("end", Dt);
}
function Dt() {
  this.allowHalfOpen || this._writableState.ended || de(It, this);
}
function It(e2) {
  e2.end();
}
function Pt(e2) {
  this.afterTransform = function(t2, r2) {
    return function(e3, t3, r3) {
      var n2 = e3._transformState;
      n2.transforming = false;
      var i2 = n2.writecb;
      if (!i2)
        return e3.emit("error", new Error("no writecb in Transform class"));
      n2.writechunk = null, n2.writecb = null, null != r3 && e3.push(r3);
      i2(t3);
      var o2 = e3._readableState;
      o2.reading = false, (o2.needReadable || o2.length < o2.highWaterMark) && e3._read(o2.highWaterMark);
    }(e2, t2, r2);
  }, this.needTransform = false, this.transforming = false, this.writecb = null, this.writechunk = null, this.writeencoding = null;
}
function Ot(e2) {
  if (!(this instanceof Ot))
    return new Ot(e2);
  Ct.call(this, e2), this._transformState = new Pt(this);
  var t2 = this;
  this._readableState.needReadable = true, this._readableState.sync = false, e2 && ("function" == typeof e2.transform && (this._transform = e2.transform), "function" == typeof e2.flush && (this._flush = e2.flush)), this.once("prefinish", function() {
    "function" == typeof this._flush ? this._flush(function(e3) {
      Ut(t2, e3);
    }) : Ut(t2);
  });
}
function Ut(e2, t2) {
  if (t2)
    return e2.emit("error", t2);
  var r2 = e2._writableState, n2 = e2._transformState;
  if (r2.length)
    throw new Error("Calling transform done when ws.length != 0");
  if (n2.transforming)
    throw new Error("Calling transform done when still transforming");
  return e2.push(null);
}
function Ht(e2) {
  if (!(this instanceof Ht))
    return new Ht(e2);
  Ot.call(this, e2);
}
function Ft() {
  ve.call(this);
}
Be(Ot, Ct), Ot.prototype.push = function(e2, t2) {
  return this._transformState.needTransform = false, Ct.prototype.push.call(this, e2, t2);
}, Ot.prototype._transform = function(e2, t2, r2) {
  throw new Error("Not implemented");
}, Ot.prototype._write = function(e2, t2, r2) {
  var n2 = this._transformState;
  if (n2.writecb = r2, n2.writechunk = e2, n2.writeencoding = t2, !n2.transforming) {
    var i2 = this._readableState;
    (n2.needTransform || i2.needReadable || i2.length < i2.highWaterMark) && this._read(i2.highWaterMark);
  }
}, Ot.prototype._read = function(e2) {
  var t2 = this._transformState;
  null !== t2.writechunk && t2.writecb && !t2.transforming ? (t2.transforming = true, this._transform(t2.writechunk, t2.writeencoding, t2.afterTransform)) : t2.needTransform = true;
}, Be(Ht, Ot), Ht.prototype._transform = function(e2, t2, r2) {
  r2(null, e2);
}, Be(Ft, ve), Ft.Readable = ot, Ft.Writable = kt, Ft.Duplex = Ct, Ft.Transform = Ot, Ft.PassThrough = Ht, Ft.Stream = Ft, Ft.prototype.pipe = function(e2, t2) {
  var r2 = this;
  function n2(t3) {
    e2.writable && false === e2.write(t3) && r2.pause && r2.pause();
  }
  function i2() {
    r2.readable && r2.resume && r2.resume();
  }
  r2.on("data", n2), e2.on("drain", i2), e2._isStdio || t2 && false === t2.end || (r2.on("end", a2), r2.on("close", s2));
  var o2 = false;
  function a2() {
    o2 || (o2 = true, e2.end());
  }
  function s2() {
    o2 || (o2 = true, "function" == typeof e2.destroy && e2.destroy());
  }
  function h2(e3) {
    if (l2(), 0 === ve.listenerCount(this, "error"))
      throw e3;
  }
  function l2() {
    r2.removeListener("data", n2), e2.removeListener("drain", i2), r2.removeListener("end", a2), r2.removeListener("close", s2), r2.removeListener("error", h2), e2.removeListener("error", h2), r2.removeListener("end", l2), r2.removeListener("close", l2), e2.removeListener("close", l2);
  }
  return r2.on("error", h2), e2.on("error", h2), r2.on("end", l2), r2.on("close", l2), e2.on("close", l2), e2.emit("pipe", r2), e2;
};
var Nt = {
  2: "need dictionary",
  1: "stream end",
  0: "",
  "-1": "file error",
  "-2": "stream error",
  "-3": "data error",
  "-4": "insufficient memory",
  "-5": "buffer error",
  "-6": "incompatible version"
};
function Zt() {
  this.input = null, this.next_in = 0, this.avail_in = 0, this.total_in = 0, this.output = null, this.next_out = 0, this.avail_out = 0, this.total_out = 0, this.msg = "", this.state = null, this.data_type = 2, this.adler = 0;
}
function jt(e2, t2, r2, n2, i2) {
  if (t2.subarray && e2.subarray)
    e2.set(t2.subarray(r2, r2 + n2), i2);
  else
    for (var o2 = 0; o2 < n2; o2++)
      e2[i2 + o2] = t2[r2 + o2];
}
var Wt = Uint8Array, Yt = Uint16Array, Kt = Int32Array, Xt = 4, qt = 0, Vt = 1, Gt = 2;
function $t(e2) {
  for (var t2 = e2.length; --t2 >= 0; )
    e2[t2] = 0;
}
var Jt = 0, Qt = 1, er = 2, tr = 29, rr = 256, nr = rr + 1 + tr, ir = 30, or = 19, ar = 2 * nr + 1, sr = 15, hr = 16, lr = 7, fr = 256, cr = 16, ur = 17, dr = 18, pr = [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 0], _r = [0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13], gr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 3, 7], vr = [16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15], wr = new Array(2 * (nr + 2));
$t(wr);
var br = new Array(2 * ir);
$t(br);
var yr = new Array(512);
$t(yr);
var mr = new Array(256);
$t(mr);
var kr = new Array(tr);
$t(kr);
var Er, Sr, xr, Rr = new Array(ir);
function Ar(e2, t2, r2, n2, i2) {
  this.static_tree = e2, this.extra_bits = t2, this.extra_base = r2, this.elems = n2, this.max_length = i2, this.has_stree = e2 && e2.length;
}
function Br(e2, t2) {
  this.dyn_tree = e2, this.max_code = 0, this.stat_desc = t2;
}
function zr(e2) {
  return e2 < 256 ? yr[e2] : yr[256 + (e2 >>> 7)];
}
function Lr(e2, t2) {
  e2.pending_buf[e2.pending++] = 255 & t2, e2.pending_buf[e2.pending++] = t2 >>> 8 & 255;
}
function Tr(e2, t2, r2) {
  e2.bi_valid > hr - r2 ? (e2.bi_buf |= t2 << e2.bi_valid & 65535, Lr(e2, e2.bi_buf), e2.bi_buf = t2 >> hr - e2.bi_valid, e2.bi_valid += r2 - hr) : (e2.bi_buf |= t2 << e2.bi_valid & 65535, e2.bi_valid += r2);
}
function Mr(e2, t2, r2) {
  Tr(e2, r2[2 * t2], r2[2 * t2 + 1]);
}
function Cr(e2, t2) {
  var r2 = 0;
  do {
    r2 |= 1 & e2, e2 >>>= 1, r2 <<= 1;
  } while (--t2 > 0);
  return r2 >>> 1;
}
function Dr(e2, t2, r2) {
  var n2, i2, o2 = new Array(sr + 1), a2 = 0;
  for (n2 = 1; n2 <= sr; n2++)
    o2[n2] = a2 = a2 + r2[n2 - 1] << 1;
  for (i2 = 0; i2 <= t2; i2++) {
    var s2 = e2[2 * i2 + 1];
    0 !== s2 && (e2[2 * i2] = Cr(o2[s2]++, s2));
  }
}
function Ir(e2) {
  var t2;
  for (t2 = 0; t2 < nr; t2++)
    e2.dyn_ltree[2 * t2] = 0;
  for (t2 = 0; t2 < ir; t2++)
    e2.dyn_dtree[2 * t2] = 0;
  for (t2 = 0; t2 < or; t2++)
    e2.bl_tree[2 * t2] = 0;
  e2.dyn_ltree[2 * fr] = 1, e2.opt_len = e2.static_len = 0, e2.last_lit = e2.matches = 0;
}
function Pr(e2) {
  e2.bi_valid > 8 ? Lr(e2, e2.bi_buf) : e2.bi_valid > 0 && (e2.pending_buf[e2.pending++] = e2.bi_buf), e2.bi_buf = 0, e2.bi_valid = 0;
}
function Or(e2, t2, r2, n2) {
  var i2 = 2 * t2, o2 = 2 * r2;
  return e2[i2] < e2[o2] || e2[i2] === e2[o2] && n2[t2] <= n2[r2];
}
function Ur(e2, t2, r2) {
  for (var n2 = e2.heap[r2], i2 = r2 << 1; i2 <= e2.heap_len && (i2 < e2.heap_len && Or(t2, e2.heap[i2 + 1], e2.heap[i2], e2.depth) && i2++, !Or(t2, n2, e2.heap[i2], e2.depth)); )
    e2.heap[r2] = e2.heap[i2], r2 = i2, i2 <<= 1;
  e2.heap[r2] = n2;
}
function Hr(e2, t2, r2) {
  var n2, i2, o2, a2, s2 = 0;
  if (0 !== e2.last_lit)
    do {
      n2 = e2.pending_buf[e2.d_buf + 2 * s2] << 8 | e2.pending_buf[e2.d_buf + 2 * s2 + 1], i2 = e2.pending_buf[e2.l_buf + s2], s2++, 0 === n2 ? Mr(e2, i2, t2) : (Mr(e2, (o2 = mr[i2]) + rr + 1, t2), 0 !== (a2 = pr[o2]) && Tr(e2, i2 -= kr[o2], a2), Mr(e2, o2 = zr(--n2), r2), 0 !== (a2 = _r[o2]) && Tr(e2, n2 -= Rr[o2], a2));
    } while (s2 < e2.last_lit);
  Mr(e2, fr, t2);
}
function Fr(e2, t2) {
  var r2, n2, i2, o2 = t2.dyn_tree, a2 = t2.stat_desc.static_tree, s2 = t2.stat_desc.has_stree, h2 = t2.stat_desc.elems, l2 = -1;
  for (e2.heap_len = 0, e2.heap_max = ar, r2 = 0; r2 < h2; r2++)
    0 !== o2[2 * r2] ? (e2.heap[++e2.heap_len] = l2 = r2, e2.depth[r2] = 0) : o2[2 * r2 + 1] = 0;
  for (; e2.heap_len < 2; )
    o2[2 * (i2 = e2.heap[++e2.heap_len] = l2 < 2 ? ++l2 : 0)] = 1, e2.depth[i2] = 0, e2.opt_len--, s2 && (e2.static_len -= a2[2 * i2 + 1]);
  for (t2.max_code = l2, r2 = e2.heap_len >> 1; r2 >= 1; r2--)
    Ur(e2, o2, r2);
  i2 = h2;
  do {
    r2 = e2.heap[1], e2.heap[1] = e2.heap[e2.heap_len--], Ur(e2, o2, 1), n2 = e2.heap[1], e2.heap[--e2.heap_max] = r2, e2.heap[--e2.heap_max] = n2, o2[2 * i2] = o2[2 * r2] + o2[2 * n2], e2.depth[i2] = (e2.depth[r2] >= e2.depth[n2] ? e2.depth[r2] : e2.depth[n2]) + 1, o2[2 * r2 + 1] = o2[2 * n2 + 1] = i2, e2.heap[1] = i2++, Ur(e2, o2, 1);
  } while (e2.heap_len >= 2);
  e2.heap[--e2.heap_max] = e2.heap[1], function(e3, t3) {
    var r3, n3, i3, o3, a3, s3, h3 = t3.dyn_tree, l3 = t3.max_code, f2 = t3.stat_desc.static_tree, c2 = t3.stat_desc.has_stree, u2 = t3.stat_desc.extra_bits, d2 = t3.stat_desc.extra_base, p2 = t3.stat_desc.max_length, _2 = 0;
    for (o3 = 0; o3 <= sr; o3++)
      e3.bl_count[o3] = 0;
    for (h3[2 * e3.heap[e3.heap_max] + 1] = 0, r3 = e3.heap_max + 1; r3 < ar; r3++)
      (o3 = h3[2 * h3[2 * (n3 = e3.heap[r3]) + 1] + 1] + 1) > p2 && (o3 = p2, _2++), h3[2 * n3 + 1] = o3, n3 > l3 || (e3.bl_count[o3]++, a3 = 0, n3 >= d2 && (a3 = u2[n3 - d2]), s3 = h3[2 * n3], e3.opt_len += s3 * (o3 + a3), c2 && (e3.static_len += s3 * (f2[2 * n3 + 1] + a3)));
    if (0 !== _2) {
      do {
        for (o3 = p2 - 1; 0 === e3.bl_count[o3]; )
          o3--;
        e3.bl_count[o3]--, e3.bl_count[o3 + 1] += 2, e3.bl_count[p2]--, _2 -= 2;
      } while (_2 > 0);
      for (o3 = p2; 0 !== o3; o3--)
        for (n3 = e3.bl_count[o3]; 0 !== n3; )
          (i3 = e3.heap[--r3]) > l3 || (h3[2 * i3 + 1] !== o3 && (e3.opt_len += (o3 - h3[2 * i3 + 1]) * h3[2 * i3], h3[2 * i3 + 1] = o3), n3--);
    }
  }(e2, t2), Dr(o2, l2, e2.bl_count);
}
function Nr(e2, t2, r2) {
  var n2, i2, o2 = -1, a2 = t2[1], s2 = 0, h2 = 7, l2 = 4;
  for (0 === a2 && (h2 = 138, l2 = 3), t2[2 * (r2 + 1) + 1] = 65535, n2 = 0; n2 <= r2; n2++)
    i2 = a2, a2 = t2[2 * (n2 + 1) + 1], ++s2 < h2 && i2 === a2 || (s2 < l2 ? e2.bl_tree[2 * i2] += s2 : 0 !== i2 ? (i2 !== o2 && e2.bl_tree[2 * i2]++, e2.bl_tree[2 * cr]++) : s2 <= 10 ? e2.bl_tree[2 * ur]++ : e2.bl_tree[2 * dr]++, s2 = 0, o2 = i2, 0 === a2 ? (h2 = 138, l2 = 3) : i2 === a2 ? (h2 = 6, l2 = 3) : (h2 = 7, l2 = 4));
}
function Zr(e2, t2, r2) {
  var n2, i2, o2 = -1, a2 = t2[1], s2 = 0, h2 = 7, l2 = 4;
  for (0 === a2 && (h2 = 138, l2 = 3), n2 = 0; n2 <= r2; n2++)
    if (i2 = a2, a2 = t2[2 * (n2 + 1) + 1], !(++s2 < h2 && i2 === a2)) {
      if (s2 < l2)
        do {
          Mr(e2, i2, e2.bl_tree);
        } while (0 != --s2);
      else
        0 !== i2 ? (i2 !== o2 && (Mr(e2, i2, e2.bl_tree), s2--), Mr(e2, cr, e2.bl_tree), Tr(e2, s2 - 3, 2)) : s2 <= 10 ? (Mr(e2, ur, e2.bl_tree), Tr(e2, s2 - 3, 3)) : (Mr(e2, dr, e2.bl_tree), Tr(e2, s2 - 11, 7));
      s2 = 0, o2 = i2, 0 === a2 ? (h2 = 138, l2 = 3) : i2 === a2 ? (h2 = 6, l2 = 3) : (h2 = 7, l2 = 4);
    }
}
$t(Rr);
var jr = false;
function Wr(e2) {
  jr || (!function() {
    var e3, t2, r2, n2, i2, o2 = new Array(sr + 1);
    for (r2 = 0, n2 = 0; n2 < tr - 1; n2++)
      for (kr[n2] = r2, e3 = 0; e3 < 1 << pr[n2]; e3++)
        mr[r2++] = n2;
    for (mr[r2 - 1] = n2, i2 = 0, n2 = 0; n2 < 16; n2++)
      for (Rr[n2] = i2, e3 = 0; e3 < 1 << _r[n2]; e3++)
        yr[i2++] = n2;
    for (i2 >>= 7; n2 < ir; n2++)
      for (Rr[n2] = i2 << 7, e3 = 0; e3 < 1 << _r[n2] - 7; e3++)
        yr[256 + i2++] = n2;
    for (t2 = 0; t2 <= sr; t2++)
      o2[t2] = 0;
    for (e3 = 0; e3 <= 143; )
      wr[2 * e3 + 1] = 8, e3++, o2[8]++;
    for (; e3 <= 255; )
      wr[2 * e3 + 1] = 9, e3++, o2[9]++;
    for (; e3 <= 279; )
      wr[2 * e3 + 1] = 7, e3++, o2[7]++;
    for (; e3 <= 287; )
      wr[2 * e3 + 1] = 8, e3++, o2[8]++;
    for (Dr(wr, nr + 1, o2), e3 = 0; e3 < ir; e3++)
      br[2 * e3 + 1] = 5, br[2 * e3] = Cr(e3, 5);
    Er = new Ar(wr, pr, rr + 1, nr, sr), Sr = new Ar(br, _r, 0, ir, sr), xr = new Ar(new Array(0), gr, 0, or, lr);
  }(), jr = true), e2.l_desc = new Br(e2.dyn_ltree, Er), e2.d_desc = new Br(e2.dyn_dtree, Sr), e2.bl_desc = new Br(e2.bl_tree, xr), e2.bi_buf = 0, e2.bi_valid = 0, Ir(e2);
}
function Yr(e2, t2, r2, n2) {
  Tr(e2, (Jt << 1) + (n2 ? 1 : 0), 3), function(e3, t3, r3, n3) {
    Pr(e3), n3 && (Lr(e3, r3), Lr(e3, ~r3)), jt(e3.pending_buf, e3.window, t3, r3, e3.pending), e3.pending += r3;
  }(e2, t2, r2, true);
}
function Kr(e2) {
  Tr(e2, Qt << 1, 3), Mr(e2, fr, wr), function(e3) {
    16 === e3.bi_valid ? (Lr(e3, e3.bi_buf), e3.bi_buf = 0, e3.bi_valid = 0) : e3.bi_valid >= 8 && (e3.pending_buf[e3.pending++] = 255 & e3.bi_buf, e3.bi_buf >>= 8, e3.bi_valid -= 8);
  }(e2);
}
function Xr(e2, t2, r2, n2) {
  var i2, o2, a2 = 0;
  e2.level > 0 ? (e2.strm.data_type === Gt && (e2.strm.data_type = function(e3) {
    var t3, r3 = 4093624447;
    for (t3 = 0; t3 <= 31; t3++, r3 >>>= 1)
      if (1 & r3 && 0 !== e3.dyn_ltree[2 * t3])
        return qt;
    if (0 !== e3.dyn_ltree[18] || 0 !== e3.dyn_ltree[20] || 0 !== e3.dyn_ltree[26])
      return Vt;
    for (t3 = 32; t3 < rr; t3++)
      if (0 !== e3.dyn_ltree[2 * t3])
        return Vt;
    return qt;
  }(e2)), Fr(e2, e2.l_desc), Fr(e2, e2.d_desc), a2 = function(e3) {
    var t3;
    for (Nr(e3, e3.dyn_ltree, e3.l_desc.max_code), Nr(e3, e3.dyn_dtree, e3.d_desc.max_code), Fr(e3, e3.bl_desc), t3 = or - 1; t3 >= 3 && 0 === e3.bl_tree[2 * vr[t3] + 1]; t3--)
      ;
    return e3.opt_len += 3 * (t3 + 1) + 5 + 5 + 4, t3;
  }(e2), i2 = e2.opt_len + 3 + 7 >>> 3, (o2 = e2.static_len + 3 + 7 >>> 3) <= i2 && (i2 = o2)) : i2 = o2 = r2 + 5, r2 + 4 <= i2 && -1 !== t2 ? Yr(e2, t2, r2, n2) : e2.strategy === Xt || o2 === i2 ? (Tr(e2, (Qt << 1) + (n2 ? 1 : 0), 3), Hr(e2, wr, br)) : (Tr(e2, (er << 1) + (n2 ? 1 : 0), 3), function(e3, t3, r3, n3) {
    var i3;
    for (Tr(e3, t3 - 257, 5), Tr(e3, r3 - 1, 5), Tr(e3, n3 - 4, 4), i3 = 0; i3 < n3; i3++)
      Tr(e3, e3.bl_tree[2 * vr[i3] + 1], 3);
    Zr(e3, e3.dyn_ltree, t3 - 1), Zr(e3, e3.dyn_dtree, r3 - 1);
  }(e2, e2.l_desc.max_code + 1, e2.d_desc.max_code + 1, a2 + 1), Hr(e2, e2.dyn_ltree, e2.dyn_dtree)), Ir(e2), n2 && Pr(e2);
}
function qr(e2, t2, r2) {
  return e2.pending_buf[e2.d_buf + 2 * e2.last_lit] = t2 >>> 8 & 255, e2.pending_buf[e2.d_buf + 2 * e2.last_lit + 1] = 255 & t2, e2.pending_buf[e2.l_buf + e2.last_lit] = 255 & r2, e2.last_lit++, 0 === t2 ? e2.dyn_ltree[2 * r2]++ : (e2.matches++, t2--, e2.dyn_ltree[2 * (mr[r2] + rr + 1)]++, e2.dyn_dtree[2 * zr(t2)]++), e2.last_lit === e2.lit_bufsize - 1;
}
function Vr(e2, t2, r2, n2) {
  for (var i2 = 65535 & e2 | 0, o2 = e2 >>> 16 & 65535 | 0, a2 = 0; 0 !== r2; ) {
    r2 -= a2 = r2 > 2e3 ? 2e3 : r2;
    do {
      o2 = o2 + (i2 = i2 + t2[n2++] | 0) | 0;
    } while (--a2);
    i2 %= 65521, o2 %= 65521;
  }
  return i2 | o2 << 16 | 0;
}
var Gr = function() {
  for (var e2, t2 = [], r2 = 0; r2 < 256; r2++) {
    e2 = r2;
    for (var n2 = 0; n2 < 8; n2++)
      e2 = 1 & e2 ? 3988292384 ^ e2 >>> 1 : e2 >>> 1;
    t2[r2] = e2;
  }
  return t2;
}();
function $r(e2, t2, r2, n2) {
  var i2 = Gr, o2 = n2 + r2;
  e2 ^= -1;
  for (var a2 = n2; a2 < o2; a2++)
    e2 = e2 >>> 8 ^ i2[255 & (e2 ^ t2[a2])];
  return -1 ^ e2;
}
var Jr, Qr = 0, en = 1, tn = 3, rn = 4, nn = 5, on = 0, an = 1, sn = -2, hn = -3, ln = -5, fn = -1, cn = 1, un = 2, dn = 3, pn = 4, _n = 2, gn = 8, vn = 9, wn = 286, bn = 30, yn = 19, mn = 2 * wn + 1, kn = 15, En = 3, Sn = 258, xn = Sn + En + 1, Rn = 32, An = 42, Bn = 69, zn = 73, Ln = 91, Tn = 103, Mn = 113, Cn = 666, Dn = 1, In = 2, Pn = 3, On = 4, Un = 3;
function Hn(e2, t2) {
  return e2.msg = Nt[t2], t2;
}
function Fn(e2) {
  return (e2 << 1) - (e2 > 4 ? 9 : 0);
}
function Nn(e2) {
  for (var t2 = e2.length; --t2 >= 0; )
    e2[t2] = 0;
}
function Zn(e2) {
  var t2 = e2.state, r2 = t2.pending;
  r2 > e2.avail_out && (r2 = e2.avail_out), 0 !== r2 && (jt(e2.output, t2.pending_buf, t2.pending_out, r2, e2.next_out), e2.next_out += r2, t2.pending_out += r2, e2.total_out += r2, e2.avail_out -= r2, t2.pending -= r2, 0 === t2.pending && (t2.pending_out = 0));
}
function jn(e2, t2) {
  Xr(e2, e2.block_start >= 0 ? e2.block_start : -1, e2.strstart - e2.block_start, t2), e2.block_start = e2.strstart, Zn(e2.strm);
}
function Wn(e2, t2) {
  e2.pending_buf[e2.pending++] = t2;
}
function Yn(e2, t2) {
  e2.pending_buf[e2.pending++] = t2 >>> 8 & 255, e2.pending_buf[e2.pending++] = 255 & t2;
}
function Kn(e2, t2) {
  var r2, n2, i2 = e2.max_chain_length, o2 = e2.strstart, a2 = e2.prev_length, s2 = e2.nice_match, h2 = e2.strstart > e2.w_size - xn ? e2.strstart - (e2.w_size - xn) : 0, l2 = e2.window, f2 = e2.w_mask, c2 = e2.prev, u2 = e2.strstart + Sn, d2 = l2[o2 + a2 - 1], p2 = l2[o2 + a2];
  e2.prev_length >= e2.good_match && (i2 >>= 2), s2 > e2.lookahead && (s2 = e2.lookahead);
  do {
    if (l2[(r2 = t2) + a2] === p2 && l2[r2 + a2 - 1] === d2 && l2[r2] === l2[o2] && l2[++r2] === l2[o2 + 1]) {
      o2 += 2, r2++;
      do {
      } while (l2[++o2] === l2[++r2] && l2[++o2] === l2[++r2] && l2[++o2] === l2[++r2] && l2[++o2] === l2[++r2] && l2[++o2] === l2[++r2] && l2[++o2] === l2[++r2] && l2[++o2] === l2[++r2] && l2[++o2] === l2[++r2] && o2 < u2);
      if (n2 = Sn - (u2 - o2), o2 = u2 - Sn, n2 > a2) {
        if (e2.match_start = t2, a2 = n2, n2 >= s2)
          break;
        d2 = l2[o2 + a2 - 1], p2 = l2[o2 + a2];
      }
    }
  } while ((t2 = c2[t2 & f2]) > h2 && 0 != --i2);
  return a2 <= e2.lookahead ? a2 : e2.lookahead;
}
function Xn(e2) {
  var t2, r2, n2, i2, o2, a2, s2, h2, l2, f2, c2 = e2.w_size;
  do {
    if (i2 = e2.window_size - e2.lookahead - e2.strstart, e2.strstart >= c2 + (c2 - xn)) {
      jt(e2.window, e2.window, c2, c2, 0), e2.match_start -= c2, e2.strstart -= c2, e2.block_start -= c2, t2 = r2 = e2.hash_size;
      do {
        n2 = e2.head[--t2], e2.head[t2] = n2 >= c2 ? n2 - c2 : 0;
      } while (--r2);
      t2 = r2 = c2;
      do {
        n2 = e2.prev[--t2], e2.prev[t2] = n2 >= c2 ? n2 - c2 : 0;
      } while (--r2);
      i2 += c2;
    }
    if (0 === e2.strm.avail_in)
      break;
    if (a2 = e2.strm, s2 = e2.window, h2 = e2.strstart + e2.lookahead, l2 = i2, f2 = void 0, (f2 = a2.avail_in) > l2 && (f2 = l2), r2 = 0 === f2 ? 0 : (a2.avail_in -= f2, jt(s2, a2.input, a2.next_in, f2, h2), 1 === a2.state.wrap ? a2.adler = Vr(a2.adler, s2, f2, h2) : 2 === a2.state.wrap && (a2.adler = $r(a2.adler, s2, f2, h2)), a2.next_in += f2, a2.total_in += f2, f2), e2.lookahead += r2, e2.lookahead + e2.insert >= En)
      for (o2 = e2.strstart - e2.insert, e2.ins_h = e2.window[o2], e2.ins_h = (e2.ins_h << e2.hash_shift ^ e2.window[o2 + 1]) & e2.hash_mask; e2.insert && (e2.ins_h = (e2.ins_h << e2.hash_shift ^ e2.window[o2 + En - 1]) & e2.hash_mask, e2.prev[o2 & e2.w_mask] = e2.head[e2.ins_h], e2.head[e2.ins_h] = o2, o2++, e2.insert--, !(e2.lookahead + e2.insert < En)); )
        ;
  } while (e2.lookahead < xn && 0 !== e2.strm.avail_in);
}
function qn(e2, t2) {
  for (var r2, n2; ; ) {
    if (e2.lookahead < xn) {
      if (Xn(e2), e2.lookahead < xn && t2 === Qr)
        return Dn;
      if (0 === e2.lookahead)
        break;
    }
    if (r2 = 0, e2.lookahead >= En && (e2.ins_h = (e2.ins_h << e2.hash_shift ^ e2.window[e2.strstart + En - 1]) & e2.hash_mask, r2 = e2.prev[e2.strstart & e2.w_mask] = e2.head[e2.ins_h], e2.head[e2.ins_h] = e2.strstart), 0 !== r2 && e2.strstart - r2 <= e2.w_size - xn && (e2.match_length = Kn(e2, r2)), e2.match_length >= En) {
      if (n2 = qr(e2, e2.strstart - e2.match_start, e2.match_length - En), e2.lookahead -= e2.match_length, e2.match_length <= e2.max_lazy_match && e2.lookahead >= En) {
        e2.match_length--;
        do {
          e2.strstart++, e2.ins_h = (e2.ins_h << e2.hash_shift ^ e2.window[e2.strstart + En - 1]) & e2.hash_mask, r2 = e2.prev[e2.strstart & e2.w_mask] = e2.head[e2.ins_h], e2.head[e2.ins_h] = e2.strstart;
        } while (0 != --e2.match_length);
        e2.strstart++;
      } else
        e2.strstart += e2.match_length, e2.match_length = 0, e2.ins_h = e2.window[e2.strstart], e2.ins_h = (e2.ins_h << e2.hash_shift ^ e2.window[e2.strstart + 1]) & e2.hash_mask;
    } else
      n2 = qr(e2, 0, e2.window[e2.strstart]), e2.lookahead--, e2.strstart++;
    if (n2 && (jn(e2, false), 0 === e2.strm.avail_out))
      return Dn;
  }
  return e2.insert = e2.strstart < En - 1 ? e2.strstart : En - 1, t2 === rn ? (jn(e2, true), 0 === e2.strm.avail_out ? Pn : On) : e2.last_lit && (jn(e2, false), 0 === e2.strm.avail_out) ? Dn : In;
}
function Vn(e2, t2) {
  for (var r2, n2, i2; ; ) {
    if (e2.lookahead < xn) {
      if (Xn(e2), e2.lookahead < xn && t2 === Qr)
        return Dn;
      if (0 === e2.lookahead)
        break;
    }
    if (r2 = 0, e2.lookahead >= En && (e2.ins_h = (e2.ins_h << e2.hash_shift ^ e2.window[e2.strstart + En - 1]) & e2.hash_mask, r2 = e2.prev[e2.strstart & e2.w_mask] = e2.head[e2.ins_h], e2.head[e2.ins_h] = e2.strstart), e2.prev_length = e2.match_length, e2.prev_match = e2.match_start, e2.match_length = En - 1, 0 !== r2 && e2.prev_length < e2.max_lazy_match && e2.strstart - r2 <= e2.w_size - xn && (e2.match_length = Kn(e2, r2), e2.match_length <= 5 && (e2.strategy === cn || e2.match_length === En && e2.strstart - e2.match_start > 4096) && (e2.match_length = En - 1)), e2.prev_length >= En && e2.match_length <= e2.prev_length) {
      i2 = e2.strstart + e2.lookahead - En, n2 = qr(e2, e2.strstart - 1 - e2.prev_match, e2.prev_length - En), e2.lookahead -= e2.prev_length - 1, e2.prev_length -= 2;
      do {
        ++e2.strstart <= i2 && (e2.ins_h = (e2.ins_h << e2.hash_shift ^ e2.window[e2.strstart + En - 1]) & e2.hash_mask, r2 = e2.prev[e2.strstart & e2.w_mask] = e2.head[e2.ins_h], e2.head[e2.ins_h] = e2.strstart);
      } while (0 != --e2.prev_length);
      if (e2.match_available = 0, e2.match_length = En - 1, e2.strstart++, n2 && (jn(e2, false), 0 === e2.strm.avail_out))
        return Dn;
    } else if (e2.match_available) {
      if ((n2 = qr(e2, 0, e2.window[e2.strstart - 1])) && jn(e2, false), e2.strstart++, e2.lookahead--, 0 === e2.strm.avail_out)
        return Dn;
    } else
      e2.match_available = 1, e2.strstart++, e2.lookahead--;
  }
  return e2.match_available && (n2 = qr(e2, 0, e2.window[e2.strstart - 1]), e2.match_available = 0), e2.insert = e2.strstart < En - 1 ? e2.strstart : En - 1, t2 === rn ? (jn(e2, true), 0 === e2.strm.avail_out ? Pn : On) : e2.last_lit && (jn(e2, false), 0 === e2.strm.avail_out) ? Dn : In;
}
function Gn(e2, t2, r2, n2, i2) {
  this.good_length = e2, this.max_lazy = t2, this.nice_length = r2, this.max_chain = n2, this.func = i2;
}
function $n() {
  this.strm = null, this.status = 0, this.pending_buf = null, this.pending_buf_size = 0, this.pending_out = 0, this.pending = 0, this.wrap = 0, this.gzhead = null, this.gzindex = 0, this.method = gn, this.last_flush = -1, this.w_size = 0, this.w_bits = 0, this.w_mask = 0, this.window = null, this.window_size = 0, this.prev = null, this.head = null, this.ins_h = 0, this.hash_size = 0, this.hash_bits = 0, this.hash_mask = 0, this.hash_shift = 0, this.block_start = 0, this.match_length = 0, this.prev_match = 0, this.match_available = 0, this.strstart = 0, this.match_start = 0, this.lookahead = 0, this.prev_length = 0, this.max_chain_length = 0, this.max_lazy_match = 0, this.level = 0, this.strategy = 0, this.good_match = 0, this.nice_match = 0, this.dyn_ltree = new Yt(2 * mn), this.dyn_dtree = new Yt(2 * (2 * bn + 1)), this.bl_tree = new Yt(2 * (2 * yn + 1)), Nn(this.dyn_ltree), Nn(this.dyn_dtree), Nn(this.bl_tree), this.l_desc = null, this.d_desc = null, this.bl_desc = null, this.bl_count = new Yt(kn + 1), this.heap = new Yt(2 * wn + 1), Nn(this.heap), this.heap_len = 0, this.heap_max = 0, this.depth = new Yt(2 * wn + 1), Nn(this.depth), this.l_buf = 0, this.lit_bufsize = 0, this.last_lit = 0, this.d_buf = 0, this.opt_len = 0, this.static_len = 0, this.matches = 0, this.insert = 0, this.bi_buf = 0, this.bi_valid = 0;
}
function Jn(e2) {
  var t2, r2 = function(e3) {
    var t3;
    return e3 && e3.state ? (e3.total_in = e3.total_out = 0, e3.data_type = _n, (t3 = e3.state).pending = 0, t3.pending_out = 0, t3.wrap < 0 && (t3.wrap = -t3.wrap), t3.status = t3.wrap ? An : Mn, e3.adler = 2 === t3.wrap ? 0 : 1, t3.last_flush = Qr, Wr(t3), on) : Hn(e3, sn);
  }(e2);
  return r2 === on && ((t2 = e2.state).window_size = 2 * t2.w_size, Nn(t2.head), t2.max_lazy_match = Jr[t2.level].max_lazy, t2.good_match = Jr[t2.level].good_length, t2.nice_match = Jr[t2.level].nice_length, t2.max_chain_length = Jr[t2.level].max_chain, t2.strstart = 0, t2.block_start = 0, t2.lookahead = 0, t2.insert = 0, t2.match_length = t2.prev_length = En - 1, t2.match_available = 0, t2.ins_h = 0), r2;
}
function Qn(e2, t2) {
  var r2, n2, i2, o2;
  if (!e2 || !e2.state || t2 > nn || t2 < 0)
    return e2 ? Hn(e2, sn) : sn;
  if (n2 = e2.state, !e2.output || !e2.input && 0 !== e2.avail_in || n2.status === Cn && t2 !== rn)
    return Hn(e2, 0 === e2.avail_out ? ln : sn);
  if (n2.strm = e2, r2 = n2.last_flush, n2.last_flush = t2, n2.status === An)
    if (2 === n2.wrap)
      e2.adler = 0, Wn(n2, 31), Wn(n2, 139), Wn(n2, 8), n2.gzhead ? (Wn(n2, (n2.gzhead.text ? 1 : 0) + (n2.gzhead.hcrc ? 2 : 0) + (n2.gzhead.extra ? 4 : 0) + (n2.gzhead.name ? 8 : 0) + (n2.gzhead.comment ? 16 : 0)), Wn(n2, 255 & n2.gzhead.time), Wn(n2, n2.gzhead.time >> 8 & 255), Wn(n2, n2.gzhead.time >> 16 & 255), Wn(n2, n2.gzhead.time >> 24 & 255), Wn(n2, 9 === n2.level ? 2 : n2.strategy >= un || n2.level < 2 ? 4 : 0), Wn(n2, 255 & n2.gzhead.os), n2.gzhead.extra && n2.gzhead.extra.length && (Wn(n2, 255 & n2.gzhead.extra.length), Wn(n2, n2.gzhead.extra.length >> 8 & 255)), n2.gzhead.hcrc && (e2.adler = $r(e2.adler, n2.pending_buf, n2.pending, 0)), n2.gzindex = 0, n2.status = Bn) : (Wn(n2, 0), Wn(n2, 0), Wn(n2, 0), Wn(n2, 0), Wn(n2, 0), Wn(n2, 9 === n2.level ? 2 : n2.strategy >= un || n2.level < 2 ? 4 : 0), Wn(n2, Un), n2.status = Mn);
    else {
      var a2 = gn + (n2.w_bits - 8 << 4) << 8;
      a2 |= (n2.strategy >= un || n2.level < 2 ? 0 : n2.level < 6 ? 1 : 6 === n2.level ? 2 : 3) << 6, 0 !== n2.strstart && (a2 |= Rn), a2 += 31 - a2 % 31, n2.status = Mn, Yn(n2, a2), 0 !== n2.strstart && (Yn(n2, e2.adler >>> 16), Yn(n2, 65535 & e2.adler)), e2.adler = 1;
    }
  if (n2.status === Bn)
    if (n2.gzhead.extra) {
      for (i2 = n2.pending; n2.gzindex < (65535 & n2.gzhead.extra.length) && (n2.pending !== n2.pending_buf_size || (n2.gzhead.hcrc && n2.pending > i2 && (e2.adler = $r(e2.adler, n2.pending_buf, n2.pending - i2, i2)), Zn(e2), i2 = n2.pending, n2.pending !== n2.pending_buf_size)); )
        Wn(n2, 255 & n2.gzhead.extra[n2.gzindex]), n2.gzindex++;
      n2.gzhead.hcrc && n2.pending > i2 && (e2.adler = $r(e2.adler, n2.pending_buf, n2.pending - i2, i2)), n2.gzindex === n2.gzhead.extra.length && (n2.gzindex = 0, n2.status = zn);
    } else
      n2.status = zn;
  if (n2.status === zn)
    if (n2.gzhead.name) {
      i2 = n2.pending;
      do {
        if (n2.pending === n2.pending_buf_size && (n2.gzhead.hcrc && n2.pending > i2 && (e2.adler = $r(e2.adler, n2.pending_buf, n2.pending - i2, i2)), Zn(e2), i2 = n2.pending, n2.pending === n2.pending_buf_size)) {
          o2 = 1;
          break;
        }
        o2 = n2.gzindex < n2.gzhead.name.length ? 255 & n2.gzhead.name.charCodeAt(n2.gzindex++) : 0, Wn(n2, o2);
      } while (0 !== o2);
      n2.gzhead.hcrc && n2.pending > i2 && (e2.adler = $r(e2.adler, n2.pending_buf, n2.pending - i2, i2)), 0 === o2 && (n2.gzindex = 0, n2.status = Ln);
    } else
      n2.status = Ln;
  if (n2.status === Ln)
    if (n2.gzhead.comment) {
      i2 = n2.pending;
      do {
        if (n2.pending === n2.pending_buf_size && (n2.gzhead.hcrc && n2.pending > i2 && (e2.adler = $r(e2.adler, n2.pending_buf, n2.pending - i2, i2)), Zn(e2), i2 = n2.pending, n2.pending === n2.pending_buf_size)) {
          o2 = 1;
          break;
        }
        o2 = n2.gzindex < n2.gzhead.comment.length ? 255 & n2.gzhead.comment.charCodeAt(n2.gzindex++) : 0, Wn(n2, o2);
      } while (0 !== o2);
      n2.gzhead.hcrc && n2.pending > i2 && (e2.adler = $r(e2.adler, n2.pending_buf, n2.pending - i2, i2)), 0 === o2 && (n2.status = Tn);
    } else
      n2.status = Tn;
  if (n2.status === Tn && (n2.gzhead.hcrc ? (n2.pending + 2 > n2.pending_buf_size && Zn(e2), n2.pending + 2 <= n2.pending_buf_size && (Wn(n2, 255 & e2.adler), Wn(n2, e2.adler >> 8 & 255), e2.adler = 0, n2.status = Mn)) : n2.status = Mn), 0 !== n2.pending) {
    if (Zn(e2), 0 === e2.avail_out)
      return n2.last_flush = -1, on;
  } else if (0 === e2.avail_in && Fn(t2) <= Fn(r2) && t2 !== rn)
    return Hn(e2, ln);
  if (n2.status === Cn && 0 !== e2.avail_in)
    return Hn(e2, ln);
  if (0 !== e2.avail_in || 0 !== n2.lookahead || t2 !== Qr && n2.status !== Cn) {
    var s2 = n2.strategy === un ? function(e3, t3) {
      for (var r3; ; ) {
        if (0 === e3.lookahead && (Xn(e3), 0 === e3.lookahead)) {
          if (t3 === Qr)
            return Dn;
          break;
        }
        if (e3.match_length = 0, r3 = qr(e3, 0, e3.window[e3.strstart]), e3.lookahead--, e3.strstart++, r3 && (jn(e3, false), 0 === e3.strm.avail_out))
          return Dn;
      }
      return e3.insert = 0, t3 === rn ? (jn(e3, true), 0 === e3.strm.avail_out ? Pn : On) : e3.last_lit && (jn(e3, false), 0 === e3.strm.avail_out) ? Dn : In;
    }(n2, t2) : n2.strategy === dn ? function(e3, t3) {
      for (var r3, n3, i3, o3, a3 = e3.window; ; ) {
        if (e3.lookahead <= Sn) {
          if (Xn(e3), e3.lookahead <= Sn && t3 === Qr)
            return Dn;
          if (0 === e3.lookahead)
            break;
        }
        if (e3.match_length = 0, e3.lookahead >= En && e3.strstart > 0 && (n3 = a3[i3 = e3.strstart - 1]) === a3[++i3] && n3 === a3[++i3] && n3 === a3[++i3]) {
          o3 = e3.strstart + Sn;
          do {
          } while (n3 === a3[++i3] && n3 === a3[++i3] && n3 === a3[++i3] && n3 === a3[++i3] && n3 === a3[++i3] && n3 === a3[++i3] && n3 === a3[++i3] && n3 === a3[++i3] && i3 < o3);
          e3.match_length = Sn - (o3 - i3), e3.match_length > e3.lookahead && (e3.match_length = e3.lookahead);
        }
        if (e3.match_length >= En ? (r3 = qr(e3, 1, e3.match_length - En), e3.lookahead -= e3.match_length, e3.strstart += e3.match_length, e3.match_length = 0) : (r3 = qr(e3, 0, e3.window[e3.strstart]), e3.lookahead--, e3.strstart++), r3 && (jn(e3, false), 0 === e3.strm.avail_out))
          return Dn;
      }
      return e3.insert = 0, t3 === rn ? (jn(e3, true), 0 === e3.strm.avail_out ? Pn : On) : e3.last_lit && (jn(e3, false), 0 === e3.strm.avail_out) ? Dn : In;
    }(n2, t2) : Jr[n2.level].func(n2, t2);
    if (s2 !== Pn && s2 !== On || (n2.status = Cn), s2 === Dn || s2 === Pn)
      return 0 === e2.avail_out && (n2.last_flush = -1), on;
    if (s2 === In && (t2 === en ? Kr(n2) : t2 !== nn && (Yr(n2, 0, 0, false), t2 === tn && (Nn(n2.head), 0 === n2.lookahead && (n2.strstart = 0, n2.block_start = 0, n2.insert = 0))), Zn(e2), 0 === e2.avail_out))
      return n2.last_flush = -1, on;
  }
  return t2 !== rn ? on : n2.wrap <= 0 ? an : (2 === n2.wrap ? (Wn(n2, 255 & e2.adler), Wn(n2, e2.adler >> 8 & 255), Wn(n2, e2.adler >> 16 & 255), Wn(n2, e2.adler >> 24 & 255), Wn(n2, 255 & e2.total_in), Wn(n2, e2.total_in >> 8 & 255), Wn(n2, e2.total_in >> 16 & 255), Wn(n2, e2.total_in >> 24 & 255)) : (Yn(n2, e2.adler >>> 16), Yn(n2, 65535 & e2.adler)), Zn(e2), n2.wrap > 0 && (n2.wrap = -n2.wrap), 0 !== n2.pending ? on : an);
}
Jr = [new Gn(0, 0, 0, 0, function(e2, t2) {
  var r2 = 65535;
  for (r2 > e2.pending_buf_size - 5 && (r2 = e2.pending_buf_size - 5); ; ) {
    if (e2.lookahead <= 1) {
      if (Xn(e2), 0 === e2.lookahead && t2 === Qr)
        return Dn;
      if (0 === e2.lookahead)
        break;
    }
    e2.strstart += e2.lookahead, e2.lookahead = 0;
    var n2 = e2.block_start + r2;
    if ((0 === e2.strstart || e2.strstart >= n2) && (e2.lookahead = e2.strstart - n2, e2.strstart = n2, jn(e2, false), 0 === e2.strm.avail_out))
      return Dn;
    if (e2.strstart - e2.block_start >= e2.w_size - xn && (jn(e2, false), 0 === e2.strm.avail_out))
      return Dn;
  }
  return e2.insert = 0, t2 === rn ? (jn(e2, true), 0 === e2.strm.avail_out ? Pn : On) : (e2.strstart > e2.block_start && (jn(e2, false), e2.strm.avail_out), Dn);
}), new Gn(4, 4, 8, 4, qn), new Gn(4, 5, 16, 8, qn), new Gn(4, 6, 32, 32, qn), new Gn(4, 4, 16, 16, Vn), new Gn(8, 16, 32, 32, Vn), new Gn(8, 16, 128, 128, Vn), new Gn(8, 32, 128, 256, Vn), new Gn(32, 128, 258, 1024, Vn), new Gn(32, 258, 258, 4096, Vn)];
var ei = 30, ti = 12;
function ri(e2, t2) {
  var r2, n2, i2, o2, a2, s2, h2, l2, f2, c2, u2, d2, p2, _2, g2, v2, w2, b2, y2, m2, k2, E2, S2, x2, R2;
  r2 = e2.state, n2 = e2.next_in, x2 = e2.input, i2 = n2 + (e2.avail_in - 5), o2 = e2.next_out, R2 = e2.output, a2 = o2 - (t2 - e2.avail_out), s2 = o2 + (e2.avail_out - 257), h2 = r2.dmax, l2 = r2.wsize, f2 = r2.whave, c2 = r2.wnext, u2 = r2.window, d2 = r2.hold, p2 = r2.bits, _2 = r2.lencode, g2 = r2.distcode, v2 = (1 << r2.lenbits) - 1, w2 = (1 << r2.distbits) - 1;
  e:
    do {
      p2 < 15 && (d2 += x2[n2++] << p2, p2 += 8, d2 += x2[n2++] << p2, p2 += 8), b2 = _2[d2 & v2];
      t:
        for (; ; ) {
          if (d2 >>>= y2 = b2 >>> 24, p2 -= y2, 0 === (y2 = b2 >>> 16 & 255))
            R2[o2++] = 65535 & b2;
          else {
            if (!(16 & y2)) {
              if (0 == (64 & y2)) {
                b2 = _2[(65535 & b2) + (d2 & (1 << y2) - 1)];
                continue t;
              }
              if (32 & y2) {
                r2.mode = ti;
                break e;
              }
              e2.msg = "invalid literal/length code", r2.mode = ei;
              break e;
            }
            m2 = 65535 & b2, (y2 &= 15) && (p2 < y2 && (d2 += x2[n2++] << p2, p2 += 8), m2 += d2 & (1 << y2) - 1, d2 >>>= y2, p2 -= y2), p2 < 15 && (d2 += x2[n2++] << p2, p2 += 8, d2 += x2[n2++] << p2, p2 += 8), b2 = g2[d2 & w2];
            r:
              for (; ; ) {
                if (d2 >>>= y2 = b2 >>> 24, p2 -= y2, !(16 & (y2 = b2 >>> 16 & 255))) {
                  if (0 == (64 & y2)) {
                    b2 = g2[(65535 & b2) + (d2 & (1 << y2) - 1)];
                    continue r;
                  }
                  e2.msg = "invalid distance code", r2.mode = ei;
                  break e;
                }
                if (k2 = 65535 & b2, p2 < (y2 &= 15) && (d2 += x2[n2++] << p2, (p2 += 8) < y2 && (d2 += x2[n2++] << p2, p2 += 8)), (k2 += d2 & (1 << y2) - 1) > h2) {
                  e2.msg = "invalid distance too far back", r2.mode = ei;
                  break e;
                }
                if (d2 >>>= y2, p2 -= y2, k2 > (y2 = o2 - a2)) {
                  if ((y2 = k2 - y2) > f2 && r2.sane) {
                    e2.msg = "invalid distance too far back", r2.mode = ei;
                    break e;
                  }
                  if (E2 = 0, S2 = u2, 0 === c2) {
                    if (E2 += l2 - y2, y2 < m2) {
                      m2 -= y2;
                      do {
                        R2[o2++] = u2[E2++];
                      } while (--y2);
                      E2 = o2 - k2, S2 = R2;
                    }
                  } else if (c2 < y2) {
                    if (E2 += l2 + c2 - y2, (y2 -= c2) < m2) {
                      m2 -= y2;
                      do {
                        R2[o2++] = u2[E2++];
                      } while (--y2);
                      if (E2 = 0, c2 < m2) {
                        m2 -= y2 = c2;
                        do {
                          R2[o2++] = u2[E2++];
                        } while (--y2);
                        E2 = o2 - k2, S2 = R2;
                      }
                    }
                  } else if (E2 += c2 - y2, y2 < m2) {
                    m2 -= y2;
                    do {
                      R2[o2++] = u2[E2++];
                    } while (--y2);
                    E2 = o2 - k2, S2 = R2;
                  }
                  for (; m2 > 2; )
                    R2[o2++] = S2[E2++], R2[o2++] = S2[E2++], R2[o2++] = S2[E2++], m2 -= 3;
                  m2 && (R2[o2++] = S2[E2++], m2 > 1 && (R2[o2++] = S2[E2++]));
                } else {
                  E2 = o2 - k2;
                  do {
                    R2[o2++] = R2[E2++], R2[o2++] = R2[E2++], R2[o2++] = R2[E2++], m2 -= 3;
                  } while (m2 > 2);
                  m2 && (R2[o2++] = R2[E2++], m2 > 1 && (R2[o2++] = R2[E2++]));
                }
                break;
              }
          }
          break;
        }
    } while (n2 < i2 && o2 < s2);
  n2 -= m2 = p2 >> 3, d2 &= (1 << (p2 -= m2 << 3)) - 1, e2.next_in = n2, e2.next_out = o2, e2.avail_in = n2 < i2 ? i2 - n2 + 5 : 5 - (n2 - i2), e2.avail_out = o2 < s2 ? s2 - o2 + 257 : 257 - (o2 - s2), r2.hold = d2, r2.bits = p2;
}
var ni = 15, ii = 852, oi = 592, ai = 0, si = 1, hi = 2, li = [3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 15, 17, 19, 23, 27, 31, 35, 43, 51, 59, 67, 83, 99, 115, 131, 163, 195, 227, 258, 0, 0], fi = [16, 16, 16, 16, 16, 16, 16, 16, 17, 17, 17, 17, 18, 18, 18, 18, 19, 19, 19, 19, 20, 20, 20, 20, 21, 21, 21, 21, 16, 72, 78], ci = [1, 2, 3, 4, 5, 7, 9, 13, 17, 25, 33, 49, 65, 97, 129, 193, 257, 385, 513, 769, 1025, 1537, 2049, 3073, 4097, 6145, 8193, 12289, 16385, 24577, 0, 0], ui = [16, 16, 16, 16, 17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23, 23, 24, 24, 25, 25, 26, 26, 27, 27, 28, 28, 29, 29, 64, 64];
function di(e2, t2, r2, n2, i2, o2, a2, s2) {
  var h2, l2, f2, c2, u2, d2, p2, _2, g2, v2 = s2.bits, w2 = 0, b2 = 0, y2 = 0, m2 = 0, k2 = 0, E2 = 0, S2 = 0, x2 = 0, R2 = 0, A2 = 0, B2 = null, z2 = 0, L2 = new Yt(ni + 1), T2 = new Yt(ni + 1), M2 = null, C2 = 0;
  for (w2 = 0; w2 <= ni; w2++)
    L2[w2] = 0;
  for (b2 = 0; b2 < n2; b2++)
    L2[t2[r2 + b2]]++;
  for (k2 = v2, m2 = ni; m2 >= 1 && 0 === L2[m2]; m2--)
    ;
  if (k2 > m2 && (k2 = m2), 0 === m2)
    return i2[o2++] = 20971520, i2[o2++] = 20971520, s2.bits = 1, 0;
  for (y2 = 1; y2 < m2 && 0 === L2[y2]; y2++)
    ;
  for (k2 < y2 && (k2 = y2), x2 = 1, w2 = 1; w2 <= ni; w2++)
    if (x2 <<= 1, (x2 -= L2[w2]) < 0)
      return -1;
  if (x2 > 0 && (e2 === ai || 1 !== m2))
    return -1;
  for (T2[1] = 0, w2 = 1; w2 < ni; w2++)
    T2[w2 + 1] = T2[w2] + L2[w2];
  for (b2 = 0; b2 < n2; b2++)
    0 !== t2[r2 + b2] && (a2[T2[t2[r2 + b2]]++] = b2);
  if (e2 === ai ? (B2 = M2 = a2, d2 = 19) : e2 === si ? (B2 = li, z2 -= 257, M2 = fi, C2 -= 257, d2 = 256) : (B2 = ci, M2 = ui, d2 = -1), A2 = 0, b2 = 0, w2 = y2, u2 = o2, E2 = k2, S2 = 0, f2 = -1, c2 = (R2 = 1 << k2) - 1, e2 === si && R2 > ii || e2 === hi && R2 > oi)
    return 1;
  for (; ; ) {
    p2 = w2 - S2, a2[b2] < d2 ? (_2 = 0, g2 = a2[b2]) : a2[b2] > d2 ? (_2 = M2[C2 + a2[b2]], g2 = B2[z2 + a2[b2]]) : (_2 = 96, g2 = 0), h2 = 1 << w2 - S2, y2 = l2 = 1 << E2;
    do {
      i2[u2 + (A2 >> S2) + (l2 -= h2)] = p2 << 24 | _2 << 16 | g2 | 0;
    } while (0 !== l2);
    for (h2 = 1 << w2 - 1; A2 & h2; )
      h2 >>= 1;
    if (0 !== h2 ? (A2 &= h2 - 1, A2 += h2) : A2 = 0, b2++, 0 == --L2[w2]) {
      if (w2 === m2)
        break;
      w2 = t2[r2 + a2[b2]];
    }
    if (w2 > k2 && (A2 & c2) !== f2) {
      for (0 === S2 && (S2 = k2), u2 += y2, x2 = 1 << (E2 = w2 - S2); E2 + S2 < m2 && !((x2 -= L2[E2 + S2]) <= 0); )
        E2++, x2 <<= 1;
      if (R2 += 1 << E2, e2 === si && R2 > ii || e2 === hi && R2 > oi)
        return 1;
      i2[f2 = A2 & c2] = k2 << 24 | E2 << 16 | u2 - o2 | 0;
    }
  }
  return 0 !== A2 && (i2[u2 + A2] = w2 - S2 << 24 | 64 << 16 | 0), s2.bits = k2, 0;
}
var pi = 0, _i = 1, gi = 2, vi = 4, wi = 5, bi = 6, yi = 0, mi = 1, ki = 2, Ei = -2, Si = -3, xi = -4, Ri = -5, Ai = 8, Bi = 1, zi = 2, Li = 3, Ti = 4, Mi = 5, Ci = 6, Di = 7, Ii = 8, Pi = 9, Oi = 10, Ui = 11, Hi = 12, Fi = 13, Ni = 14, Zi = 15, ji = 16, Wi = 17, Yi = 18, Ki = 19, Xi = 20, qi = 21, Vi = 22, Gi = 23, $i = 24, Ji = 25, Qi = 26, eo = 27, to = 28, ro = 29, no = 30, io = 31, oo = 32, ao = 852, so = 592;
function ho(e2) {
  return (e2 >>> 24 & 255) + (e2 >>> 8 & 65280) + ((65280 & e2) << 8) + ((255 & e2) << 24);
}
function lo() {
  this.mode = 0, this.last = false, this.wrap = 0, this.havedict = false, this.flags = 0, this.dmax = 0, this.check = 0, this.total = 0, this.head = null, this.wbits = 0, this.wsize = 0, this.whave = 0, this.wnext = 0, this.window = null, this.hold = 0, this.bits = 0, this.length = 0, this.offset = 0, this.extra = 0, this.lencode = null, this.distcode = null, this.lenbits = 0, this.distbits = 0, this.ncode = 0, this.nlen = 0, this.ndist = 0, this.have = 0, this.next = null, this.lens = new Yt(320), this.work = new Yt(288), this.lendyn = null, this.distdyn = null, this.sane = 0, this.back = 0, this.was = 0;
}
function fo(e2) {
  var t2;
  return e2 && e2.state ? ((t2 = e2.state).wsize = 0, t2.whave = 0, t2.wnext = 0, function(e3) {
    var t3;
    return e3 && e3.state ? (t3 = e3.state, e3.total_in = e3.total_out = t3.total = 0, e3.msg = "", t3.wrap && (e3.adler = 1 & t3.wrap), t3.mode = Bi, t3.last = 0, t3.havedict = 0, t3.dmax = 32768, t3.head = null, t3.hold = 0, t3.bits = 0, t3.lencode = t3.lendyn = new Kt(ao), t3.distcode = t3.distdyn = new Kt(so), t3.sane = 1, t3.back = -1, yi) : Ei;
  }(e2)) : Ei;
}
function co(e2, t2) {
  var r2, n2;
  return e2 ? (n2 = new lo(), e2.state = n2, n2.window = null, (r2 = function(e3, t3) {
    var r3, n3;
    return e3 && e3.state ? (n3 = e3.state, t3 < 0 ? (r3 = 0, t3 = -t3) : (r3 = 1 + (t3 >> 4), t3 < 48 && (t3 &= 15)), t3 && (t3 < 8 || t3 > 15) ? Ei : (null !== n3.window && n3.wbits !== t3 && (n3.window = null), n3.wrap = r3, n3.wbits = t3, fo(e3))) : Ei;
  }(e2, t2)) !== yi && (e2.state = null), r2) : Ei;
}
var uo, po, _o = true;
function go(e2) {
  if (_o) {
    var t2;
    for (uo = new Kt(512), po = new Kt(32), t2 = 0; t2 < 144; )
      e2.lens[t2++] = 8;
    for (; t2 < 256; )
      e2.lens[t2++] = 9;
    for (; t2 < 280; )
      e2.lens[t2++] = 7;
    for (; t2 < 288; )
      e2.lens[t2++] = 8;
    for (di(_i, e2.lens, 0, 288, uo, 0, e2.work, {
      bits: 9
    }), t2 = 0; t2 < 32; )
      e2.lens[t2++] = 5;
    di(gi, e2.lens, 0, 32, po, 0, e2.work, {
      bits: 5
    }), _o = false;
  }
  e2.lencode = uo, e2.lenbits = 9, e2.distcode = po, e2.distbits = 5;
}
function vo(e2, t2) {
  var r2, n2, i2, o2, a2, s2, h2, l2, f2, c2, u2, d2, p2, _2, g2, v2, w2, b2, y2, m2, k2, E2, S2, x2, R2 = 0, A2 = new Wt(4), B2 = [16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15];
  if (!e2 || !e2.state || !e2.output || !e2.input && 0 !== e2.avail_in)
    return Ei;
  (r2 = e2.state).mode === Hi && (r2.mode = Fi), a2 = e2.next_out, i2 = e2.output, h2 = e2.avail_out, o2 = e2.next_in, n2 = e2.input, s2 = e2.avail_in, l2 = r2.hold, f2 = r2.bits, c2 = s2, u2 = h2, E2 = yi;
  e:
    for (; ; )
      switch (r2.mode) {
        case Bi:
          if (0 === r2.wrap) {
            r2.mode = Fi;
            break;
          }
          for (; f2 < 16; ) {
            if (0 === s2)
              break e;
            s2--, l2 += n2[o2++] << f2, f2 += 8;
          }
          if (2 & r2.wrap && 35615 === l2) {
            r2.check = 0, A2[0] = 255 & l2, A2[1] = l2 >>> 8 & 255, r2.check = $r(r2.check, A2, 2, 0), l2 = 0, f2 = 0, r2.mode = zi;
            break;
          }
          if (r2.flags = 0, r2.head && (r2.head.done = false), !(1 & r2.wrap) || (((255 & l2) << 8) + (l2 >> 8)) % 31) {
            e2.msg = "incorrect header check", r2.mode = no;
            break;
          }
          if ((15 & l2) !== Ai) {
            e2.msg = "unknown compression method", r2.mode = no;
            break;
          }
          if (f2 -= 4, k2 = 8 + (15 & (l2 >>>= 4)), 0 === r2.wbits)
            r2.wbits = k2;
          else if (k2 > r2.wbits) {
            e2.msg = "invalid window size", r2.mode = no;
            break;
          }
          r2.dmax = 1 << k2, e2.adler = r2.check = 1, r2.mode = 512 & l2 ? Oi : Hi, l2 = 0, f2 = 0;
          break;
        case zi:
          for (; f2 < 16; ) {
            if (0 === s2)
              break e;
            s2--, l2 += n2[o2++] << f2, f2 += 8;
          }
          if (r2.flags = l2, (255 & r2.flags) !== Ai) {
            e2.msg = "unknown compression method", r2.mode = no;
            break;
          }
          if (57344 & r2.flags) {
            e2.msg = "unknown header flags set", r2.mode = no;
            break;
          }
          r2.head && (r2.head.text = l2 >> 8 & 1), 512 & r2.flags && (A2[0] = 255 & l2, A2[1] = l2 >>> 8 & 255, r2.check = $r(r2.check, A2, 2, 0)), l2 = 0, f2 = 0, r2.mode = Li;
        case Li:
          for (; f2 < 32; ) {
            if (0 === s2)
              break e;
            s2--, l2 += n2[o2++] << f2, f2 += 8;
          }
          r2.head && (r2.head.time = l2), 512 & r2.flags && (A2[0] = 255 & l2, A2[1] = l2 >>> 8 & 255, A2[2] = l2 >>> 16 & 255, A2[3] = l2 >>> 24 & 255, r2.check = $r(r2.check, A2, 4, 0)), l2 = 0, f2 = 0, r2.mode = Ti;
        case Ti:
          for (; f2 < 16; ) {
            if (0 === s2)
              break e;
            s2--, l2 += n2[o2++] << f2, f2 += 8;
          }
          r2.head && (r2.head.xflags = 255 & l2, r2.head.os = l2 >> 8), 512 & r2.flags && (A2[0] = 255 & l2, A2[1] = l2 >>> 8 & 255, r2.check = $r(r2.check, A2, 2, 0)), l2 = 0, f2 = 0, r2.mode = Mi;
        case Mi:
          if (1024 & r2.flags) {
            for (; f2 < 16; ) {
              if (0 === s2)
                break e;
              s2--, l2 += n2[o2++] << f2, f2 += 8;
            }
            r2.length = l2, r2.head && (r2.head.extra_len = l2), 512 & r2.flags && (A2[0] = 255 & l2, A2[1] = l2 >>> 8 & 255, r2.check = $r(r2.check, A2, 2, 0)), l2 = 0, f2 = 0;
          } else
            r2.head && (r2.head.extra = null);
          r2.mode = Ci;
        case Ci:
          if (1024 & r2.flags && ((d2 = r2.length) > s2 && (d2 = s2), d2 && (r2.head && (k2 = r2.head.extra_len - r2.length, r2.head.extra || (r2.head.extra = new Array(r2.head.extra_len)), jt(r2.head.extra, n2, o2, d2, k2)), 512 & r2.flags && (r2.check = $r(r2.check, n2, d2, o2)), s2 -= d2, o2 += d2, r2.length -= d2), r2.length))
            break e;
          r2.length = 0, r2.mode = Di;
        case Di:
          if (2048 & r2.flags) {
            if (0 === s2)
              break e;
            d2 = 0;
            do {
              k2 = n2[o2 + d2++], r2.head && k2 && r2.length < 65536 && (r2.head.name += String.fromCharCode(k2));
            } while (k2 && d2 < s2);
            if (512 & r2.flags && (r2.check = $r(r2.check, n2, d2, o2)), s2 -= d2, o2 += d2, k2)
              break e;
          } else
            r2.head && (r2.head.name = null);
          r2.length = 0, r2.mode = Ii;
        case Ii:
          if (4096 & r2.flags) {
            if (0 === s2)
              break e;
            d2 = 0;
            do {
              k2 = n2[o2 + d2++], r2.head && k2 && r2.length < 65536 && (r2.head.comment += String.fromCharCode(k2));
            } while (k2 && d2 < s2);
            if (512 & r2.flags && (r2.check = $r(r2.check, n2, d2, o2)), s2 -= d2, o2 += d2, k2)
              break e;
          } else
            r2.head && (r2.head.comment = null);
          r2.mode = Pi;
        case Pi:
          if (512 & r2.flags) {
            for (; f2 < 16; ) {
              if (0 === s2)
                break e;
              s2--, l2 += n2[o2++] << f2, f2 += 8;
            }
            if (l2 !== (65535 & r2.check)) {
              e2.msg = "header crc mismatch", r2.mode = no;
              break;
            }
            l2 = 0, f2 = 0;
          }
          r2.head && (r2.head.hcrc = r2.flags >> 9 & 1, r2.head.done = true), e2.adler = r2.check = 0, r2.mode = Hi;
          break;
        case Oi:
          for (; f2 < 32; ) {
            if (0 === s2)
              break e;
            s2--, l2 += n2[o2++] << f2, f2 += 8;
          }
          e2.adler = r2.check = ho(l2), l2 = 0, f2 = 0, r2.mode = Ui;
        case Ui:
          if (0 === r2.havedict)
            return e2.next_out = a2, e2.avail_out = h2, e2.next_in = o2, e2.avail_in = s2, r2.hold = l2, r2.bits = f2, ki;
          e2.adler = r2.check = 1, r2.mode = Hi;
        case Hi:
          if (t2 === wi || t2 === bi)
            break e;
        case Fi:
          if (r2.last) {
            l2 >>>= 7 & f2, f2 -= 7 & f2, r2.mode = eo;
            break;
          }
          for (; f2 < 3; ) {
            if (0 === s2)
              break e;
            s2--, l2 += n2[o2++] << f2, f2 += 8;
          }
          switch (r2.last = 1 & l2, f2 -= 1, 3 & (l2 >>>= 1)) {
            case 0:
              r2.mode = Ni;
              break;
            case 1:
              if (go(r2), r2.mode = Xi, t2 === bi) {
                l2 >>>= 2, f2 -= 2;
                break e;
              }
              break;
            case 2:
              r2.mode = Wi;
              break;
            case 3:
              e2.msg = "invalid block type", r2.mode = no;
          }
          l2 >>>= 2, f2 -= 2;
          break;
        case Ni:
          for (l2 >>>= 7 & f2, f2 -= 7 & f2; f2 < 32; ) {
            if (0 === s2)
              break e;
            s2--, l2 += n2[o2++] << f2, f2 += 8;
          }
          if ((65535 & l2) != (l2 >>> 16 ^ 65535)) {
            e2.msg = "invalid stored block lengths", r2.mode = no;
            break;
          }
          if (r2.length = 65535 & l2, l2 = 0, f2 = 0, r2.mode = Zi, t2 === bi)
            break e;
        case Zi:
          r2.mode = ji;
        case ji:
          if (d2 = r2.length) {
            if (d2 > s2 && (d2 = s2), d2 > h2 && (d2 = h2), 0 === d2)
              break e;
            jt(i2, n2, o2, d2, a2), s2 -= d2, o2 += d2, h2 -= d2, a2 += d2, r2.length -= d2;
            break;
          }
          r2.mode = Hi;
          break;
        case Wi:
          for (; f2 < 14; ) {
            if (0 === s2)
              break e;
            s2--, l2 += n2[o2++] << f2, f2 += 8;
          }
          if (r2.nlen = 257 + (31 & l2), l2 >>>= 5, f2 -= 5, r2.ndist = 1 + (31 & l2), l2 >>>= 5, f2 -= 5, r2.ncode = 4 + (15 & l2), l2 >>>= 4, f2 -= 4, r2.nlen > 286 || r2.ndist > 30) {
            e2.msg = "too many length or distance symbols", r2.mode = no;
            break;
          }
          r2.have = 0, r2.mode = Yi;
        case Yi:
          for (; r2.have < r2.ncode; ) {
            for (; f2 < 3; ) {
              if (0 === s2)
                break e;
              s2--, l2 += n2[o2++] << f2, f2 += 8;
            }
            r2.lens[B2[r2.have++]] = 7 & l2, l2 >>>= 3, f2 -= 3;
          }
          for (; r2.have < 19; )
            r2.lens[B2[r2.have++]] = 0;
          if (r2.lencode = r2.lendyn, r2.lenbits = 7, S2 = {
            bits: r2.lenbits
          }, E2 = di(pi, r2.lens, 0, 19, r2.lencode, 0, r2.work, S2), r2.lenbits = S2.bits, E2) {
            e2.msg = "invalid code lengths set", r2.mode = no;
            break;
          }
          r2.have = 0, r2.mode = Ki;
        case Ki:
          for (; r2.have < r2.nlen + r2.ndist; ) {
            for (; v2 = (R2 = r2.lencode[l2 & (1 << r2.lenbits) - 1]) >>> 16 & 255, w2 = 65535 & R2, !((g2 = R2 >>> 24) <= f2); ) {
              if (0 === s2)
                break e;
              s2--, l2 += n2[o2++] << f2, f2 += 8;
            }
            if (w2 < 16)
              l2 >>>= g2, f2 -= g2, r2.lens[r2.have++] = w2;
            else {
              if (16 === w2) {
                for (x2 = g2 + 2; f2 < x2; ) {
                  if (0 === s2)
                    break e;
                  s2--, l2 += n2[o2++] << f2, f2 += 8;
                }
                if (l2 >>>= g2, f2 -= g2, 0 === r2.have) {
                  e2.msg = "invalid bit length repeat", r2.mode = no;
                  break;
                }
                k2 = r2.lens[r2.have - 1], d2 = 3 + (3 & l2), l2 >>>= 2, f2 -= 2;
              } else if (17 === w2) {
                for (x2 = g2 + 3; f2 < x2; ) {
                  if (0 === s2)
                    break e;
                  s2--, l2 += n2[o2++] << f2, f2 += 8;
                }
                f2 -= g2, k2 = 0, d2 = 3 + (7 & (l2 >>>= g2)), l2 >>>= 3, f2 -= 3;
              } else {
                for (x2 = g2 + 7; f2 < x2; ) {
                  if (0 === s2)
                    break e;
                  s2--, l2 += n2[o2++] << f2, f2 += 8;
                }
                f2 -= g2, k2 = 0, d2 = 11 + (127 & (l2 >>>= g2)), l2 >>>= 7, f2 -= 7;
              }
              if (r2.have + d2 > r2.nlen + r2.ndist) {
                e2.msg = "invalid bit length repeat", r2.mode = no;
                break;
              }
              for (; d2--; )
                r2.lens[r2.have++] = k2;
            }
          }
          if (r2.mode === no)
            break;
          if (0 === r2.lens[256]) {
            e2.msg = "invalid code -- missing end-of-block", r2.mode = no;
            break;
          }
          if (r2.lenbits = 9, S2 = {
            bits: r2.lenbits
          }, E2 = di(_i, r2.lens, 0, r2.nlen, r2.lencode, 0, r2.work, S2), r2.lenbits = S2.bits, E2) {
            e2.msg = "invalid literal/lengths set", r2.mode = no;
            break;
          }
          if (r2.distbits = 6, r2.distcode = r2.distdyn, S2 = {
            bits: r2.distbits
          }, E2 = di(gi, r2.lens, r2.nlen, r2.ndist, r2.distcode, 0, r2.work, S2), r2.distbits = S2.bits, E2) {
            e2.msg = "invalid distances set", r2.mode = no;
            break;
          }
          if (r2.mode = Xi, t2 === bi)
            break e;
        case Xi:
          r2.mode = qi;
        case qi:
          if (s2 >= 6 && h2 >= 258) {
            e2.next_out = a2, e2.avail_out = h2, e2.next_in = o2, e2.avail_in = s2, r2.hold = l2, r2.bits = f2, ri(e2, u2), a2 = e2.next_out, i2 = e2.output, h2 = e2.avail_out, o2 = e2.next_in, n2 = e2.input, s2 = e2.avail_in, l2 = r2.hold, f2 = r2.bits, r2.mode === Hi && (r2.back = -1);
            break;
          }
          for (r2.back = 0; v2 = (R2 = r2.lencode[l2 & (1 << r2.lenbits) - 1]) >>> 16 & 255, w2 = 65535 & R2, !((g2 = R2 >>> 24) <= f2); ) {
            if (0 === s2)
              break e;
            s2--, l2 += n2[o2++] << f2, f2 += 8;
          }
          if (v2 && 0 == (240 & v2)) {
            for (b2 = g2, y2 = v2, m2 = w2; v2 = (R2 = r2.lencode[m2 + ((l2 & (1 << b2 + y2) - 1) >> b2)]) >>> 16 & 255, w2 = 65535 & R2, !(b2 + (g2 = R2 >>> 24) <= f2); ) {
              if (0 === s2)
                break e;
              s2--, l2 += n2[o2++] << f2, f2 += 8;
            }
            l2 >>>= b2, f2 -= b2, r2.back += b2;
          }
          if (l2 >>>= g2, f2 -= g2, r2.back += g2, r2.length = w2, 0 === v2) {
            r2.mode = Qi;
            break;
          }
          if (32 & v2) {
            r2.back = -1, r2.mode = Hi;
            break;
          }
          if (64 & v2) {
            e2.msg = "invalid literal/length code", r2.mode = no;
            break;
          }
          r2.extra = 15 & v2, r2.mode = Vi;
        case Vi:
          if (r2.extra) {
            for (x2 = r2.extra; f2 < x2; ) {
              if (0 === s2)
                break e;
              s2--, l2 += n2[o2++] << f2, f2 += 8;
            }
            r2.length += l2 & (1 << r2.extra) - 1, l2 >>>= r2.extra, f2 -= r2.extra, r2.back += r2.extra;
          }
          r2.was = r2.length, r2.mode = Gi;
        case Gi:
          for (; v2 = (R2 = r2.distcode[l2 & (1 << r2.distbits) - 1]) >>> 16 & 255, w2 = 65535 & R2, !((g2 = R2 >>> 24) <= f2); ) {
            if (0 === s2)
              break e;
            s2--, l2 += n2[o2++] << f2, f2 += 8;
          }
          if (0 == (240 & v2)) {
            for (b2 = g2, y2 = v2, m2 = w2; v2 = (R2 = r2.distcode[m2 + ((l2 & (1 << b2 + y2) - 1) >> b2)]) >>> 16 & 255, w2 = 65535 & R2, !(b2 + (g2 = R2 >>> 24) <= f2); ) {
              if (0 === s2)
                break e;
              s2--, l2 += n2[o2++] << f2, f2 += 8;
            }
            l2 >>>= b2, f2 -= b2, r2.back += b2;
          }
          if (l2 >>>= g2, f2 -= g2, r2.back += g2, 64 & v2) {
            e2.msg = "invalid distance code", r2.mode = no;
            break;
          }
          r2.offset = w2, r2.extra = 15 & v2, r2.mode = $i;
        case $i:
          if (r2.extra) {
            for (x2 = r2.extra; f2 < x2; ) {
              if (0 === s2)
                break e;
              s2--, l2 += n2[o2++] << f2, f2 += 8;
            }
            r2.offset += l2 & (1 << r2.extra) - 1, l2 >>>= r2.extra, f2 -= r2.extra, r2.back += r2.extra;
          }
          if (r2.offset > r2.dmax) {
            e2.msg = "invalid distance too far back", r2.mode = no;
            break;
          }
          r2.mode = Ji;
        case Ji:
          if (0 === h2)
            break e;
          if (d2 = u2 - h2, r2.offset > d2) {
            if ((d2 = r2.offset - d2) > r2.whave && r2.sane) {
              e2.msg = "invalid distance too far back", r2.mode = no;
              break;
            }
            d2 > r2.wnext ? (d2 -= r2.wnext, p2 = r2.wsize - d2) : p2 = r2.wnext - d2, d2 > r2.length && (d2 = r2.length), _2 = r2.window;
          } else
            _2 = i2, p2 = a2 - r2.offset, d2 = r2.length;
          d2 > h2 && (d2 = h2), h2 -= d2, r2.length -= d2;
          do {
            i2[a2++] = _2[p2++];
          } while (--d2);
          0 === r2.length && (r2.mode = qi);
          break;
        case Qi:
          if (0 === h2)
            break e;
          i2[a2++] = r2.length, h2--, r2.mode = qi;
          break;
        case eo:
          if (r2.wrap) {
            for (; f2 < 32; ) {
              if (0 === s2)
                break e;
              s2--, l2 |= n2[o2++] << f2, f2 += 8;
            }
            if (u2 -= h2, e2.total_out += u2, r2.total += u2, u2 && (e2.adler = r2.check = r2.flags ? $r(r2.check, i2, u2, a2 - u2) : Vr(r2.check, i2, u2, a2 - u2)), u2 = h2, (r2.flags ? l2 : ho(l2)) !== r2.check) {
              e2.msg = "incorrect data check", r2.mode = no;
              break;
            }
            l2 = 0, f2 = 0;
          }
          r2.mode = to;
        case to:
          if (r2.wrap && r2.flags) {
            for (; f2 < 32; ) {
              if (0 === s2)
                break e;
              s2--, l2 += n2[o2++] << f2, f2 += 8;
            }
            if (l2 !== (4294967295 & r2.total)) {
              e2.msg = "incorrect length check", r2.mode = no;
              break;
            }
            l2 = 0, f2 = 0;
          }
          r2.mode = ro;
        case ro:
          E2 = mi;
          break e;
        case no:
          E2 = Si;
          break e;
        case io:
          return xi;
        case oo:
        default:
          return Ei;
      }
  return e2.next_out = a2, e2.avail_out = h2, e2.next_in = o2, e2.avail_in = s2, r2.hold = l2, r2.bits = f2, (r2.wsize || u2 !== e2.avail_out && r2.mode < no && (r2.mode < eo || t2 !== vi)) && function(e3, t3, r3, n3) {
    var i3, o3 = e3.state;
    null === o3.window && (o3.wsize = 1 << o3.wbits, o3.wnext = 0, o3.whave = 0, o3.window = new Wt(o3.wsize)), n3 >= o3.wsize ? (jt(o3.window, t3, r3 - o3.wsize, o3.wsize, 0), o3.wnext = 0, o3.whave = o3.wsize) : ((i3 = o3.wsize - o3.wnext) > n3 && (i3 = n3), jt(o3.window, t3, r3 - n3, i3, o3.wnext), (n3 -= i3) ? (jt(o3.window, t3, r3 - n3, n3, 0), o3.wnext = n3, o3.whave = o3.wsize) : (o3.wnext += i3, o3.wnext === o3.wsize && (o3.wnext = 0), o3.whave < o3.wsize && (o3.whave += i3)));
  }(e2, e2.output, e2.next_out, u2 - e2.avail_out), c2 -= e2.avail_in, u2 -= e2.avail_out, e2.total_in += c2, e2.total_out += u2, r2.total += u2, r2.wrap && u2 && (e2.adler = r2.check = r2.flags ? $r(r2.check, i2, u2, e2.next_out - u2) : Vr(r2.check, i2, u2, e2.next_out - u2)), e2.data_type = r2.bits + (r2.last ? 64 : 0) + (r2.mode === Hi ? 128 : 0) + (r2.mode === Xi || r2.mode === Zi ? 256 : 0), (0 === c2 && 0 === u2 || t2 === vi) && E2 === yi && (E2 = Ri), E2;
}
var wo, bo = 1, yo = 7;
function mo(e2) {
  if (e2 < bo || e2 > yo)
    throw new TypeError("Bad argument");
  this.mode = e2, this.init_done = false, this.write_in_progress = false, this.pending_close = false, this.windowBits = 0, this.level = 0, this.memLevel = 0, this.strategy = 0, this.dictionary = null;
}
function ko(e2, t2) {
  for (var r2 = 0; r2 < e2.length; r2++)
    this[t2 + r2] = e2[r2];
}
mo.prototype.init = function(e2, t2, r2, n2, i2) {
  var o2;
  switch (this.windowBits = e2, this.level = t2, this.memLevel = r2, this.strategy = n2, 3 !== this.mode && 4 !== this.mode || (this.windowBits += 16), this.mode === yo && (this.windowBits += 32), 5 !== this.mode && 6 !== this.mode || (this.windowBits = -this.windowBits), this.strm = new Zt(), this.mode) {
    case bo:
    case 3:
    case 5:
      o2 = function(e3, t3, r3, n3, i3, o3) {
        if (!e3)
          return sn;
        var a2 = 1;
        if (t3 === fn && (t3 = 6), n3 < 0 ? (a2 = 0, n3 = -n3) : n3 > 15 && (a2 = 2, n3 -= 16), i3 < 1 || i3 > vn || r3 !== gn || n3 < 8 || n3 > 15 || t3 < 0 || t3 > 9 || o3 < 0 || o3 > pn)
          return Hn(e3, sn);
        8 === n3 && (n3 = 9);
        var s2 = new $n();
        return e3.state = s2, s2.strm = e3, s2.wrap = a2, s2.gzhead = null, s2.w_bits = n3, s2.w_size = 1 << s2.w_bits, s2.w_mask = s2.w_size - 1, s2.hash_bits = i3 + 7, s2.hash_size = 1 << s2.hash_bits, s2.hash_mask = s2.hash_size - 1, s2.hash_shift = ~~((s2.hash_bits + En - 1) / En), s2.window = new Wt(2 * s2.w_size), s2.head = new Yt(s2.hash_size), s2.prev = new Yt(s2.w_size), s2.lit_bufsize = 1 << i3 + 6, s2.pending_buf_size = 4 * s2.lit_bufsize, s2.pending_buf = new Wt(s2.pending_buf_size), s2.d_buf = 1 * s2.lit_bufsize, s2.l_buf = 3 * s2.lit_bufsize, s2.level = t3, s2.strategy = o3, s2.method = r3, Jn(e3);
      }(this.strm, this.level, 8, this.windowBits, this.memLevel, this.strategy);
      break;
    case 2:
    case 4:
    case 6:
    case yo:
      o2 = co(this.strm, this.windowBits);
      break;
    default:
      throw new Error("Unknown mode " + this.mode);
  }
  0 === o2 ? (this.write_in_progress = false, this.init_done = true) : this._error(o2);
}, mo.prototype.params = function() {
  throw new Error("deflateParams Not supported");
}, mo.prototype._writeCheck = function() {
  if (!this.init_done)
    throw new Error("write before init");
  if (0 === this.mode)
    throw new Error("already finalized");
  if (this.write_in_progress)
    throw new Error("write already in progress");
  if (this.pending_close)
    throw new Error("close is pending");
}, mo.prototype.write = function(e2, t2, r2, n2, i2, o2, a2) {
  this._writeCheck(), this.write_in_progress = true;
  var s2 = this;
  return de(function() {
    s2.write_in_progress = false;
    var h2 = s2._write(e2, t2, r2, n2, i2, o2, a2);
    s2.callback(h2[0], h2[1]), s2.pending_close && s2.close();
  }), this;
}, mo.prototype.writeSync = function(e2, t2, r2, n2, i2, o2, a2) {
  return this._writeCheck(), this._write(e2, t2, r2, n2, i2, o2, a2);
}, mo.prototype._write = function(e2, t2, r2, n2, i2, o2, a2) {
  if (this.write_in_progress = true, 0 !== e2 && 1 !== e2 && 2 !== e2 && 3 !== e2 && 4 !== e2 && 5 !== e2)
    throw new Error("Invalid flush value");
  null == t2 && (t2 = new p(0), n2 = 0, r2 = 0), i2._set ? i2.set = i2._set : i2.set = ko;
  var s2, h2 = this.strm;
  switch (h2.avail_in = n2, h2.input = t2, h2.next_in = r2, h2.avail_out = a2, h2.output = i2, h2.next_out = o2, this.mode) {
    case bo:
    case 3:
    case 5:
      s2 = Qn(h2, e2);
      break;
    case yo:
    case 2:
    case 4:
    case 6:
      s2 = vo(h2, e2);
      break;
    default:
      throw new Error("Unknown mode " + this.mode);
  }
  return 1 !== s2 && 0 !== s2 && this._error(s2), this.write_in_progress = false, [h2.avail_in, h2.avail_out];
}, mo.prototype.close = function() {
  this.write_in_progress ? this.pending_close = true : (this.pending_close = false, this.mode === bo || 3 === this.mode || 5 === this.mode ? function(e2) {
    var t2;
    e2 && e2.state && ((t2 = e2.state.status) !== An && t2 !== Bn && t2 !== zn && t2 !== Ln && t2 !== Tn && t2 !== Mn && t2 !== Cn ? Hn(e2, sn) : (e2.state = null, t2 === Mn && Hn(e2, hn)));
  }(this.strm) : function(e2) {
    if (!e2 || !e2.state)
      return Ei;
    var t2 = e2.state;
    t2.window && (t2.window = null), e2.state = null;
  }(this.strm), this.mode = 0);
}, mo.prototype.reset = function() {
  switch (this.mode) {
    case bo:
    case 5:
      wo = Jn(this.strm);
      break;
    case 2:
    case 6:
      wo = fo(this.strm);
  }
  0 !== wo && this._error(wo);
}, mo.prototype._error = function(e2) {
  this.onerror(Nt[e2] + ": " + this.strm.msg, e2), this.write_in_progress = false, this.pending_close && this.close();
};
var Eo = Object.freeze({
  NONE: 0,
  DEFLATE: bo,
  INFLATE: 2,
  GZIP: 3,
  GUNZIP: 4,
  DEFLATERAW: 5,
  INFLATERAW: 6,
  UNZIP: yo,
  Z_NO_FLUSH: 0,
  Z_PARTIAL_FLUSH: 1,
  Z_SYNC_FLUSH: 2,
  Z_FULL_FLUSH: 3,
  Z_FINISH: 4,
  Z_BLOCK: 5,
  Z_TREES: 6,
  Z_OK: 0,
  Z_STREAM_END: 1,
  Z_NEED_DICT: 2,
  Z_ERRNO: -1,
  Z_STREAM_ERROR: -2,
  Z_DATA_ERROR: -3,
  Z_BUF_ERROR: -5,
  Z_NO_COMPRESSION: 0,
  Z_BEST_SPEED: 1,
  Z_BEST_COMPRESSION: 9,
  Z_DEFAULT_COMPRESSION: -1,
  Z_FILTERED: 1,
  Z_HUFFMAN_ONLY: 2,
  Z_RLE: 3,
  Z_FIXED: 4,
  Z_DEFAULT_STRATEGY: 0,
  Z_BINARY: 0,
  Z_TEXT: 1,
  Z_UNKNOWN: 2,
  Z_DEFLATED: 8,
  Zlib: mo
});
var So = {};
Object.keys(Eo).forEach(function(e2) {
  So[e2] = Eo[e2];
}), So.Z_MIN_WINDOWBITS = 8, So.Z_MAX_WINDOWBITS = 15, So.Z_DEFAULT_WINDOWBITS = 15, So.Z_MIN_CHUNK = 64, So.Z_MAX_CHUNK = 1 / 0, So.Z_DEFAULT_CHUNK = 16384, So.Z_MIN_MEMLEVEL = 1, So.Z_MAX_MEMLEVEL = 9, So.Z_DEFAULT_MEMLEVEL = 8, So.Z_MIN_LEVEL = -1, So.Z_MAX_LEVEL = 9, So.Z_DEFAULT_LEVEL = So.Z_DEFAULT_COMPRESSION;
var xo = {
  Z_OK: So.Z_OK,
  Z_STREAM_END: So.Z_STREAM_END,
  Z_NEED_DICT: So.Z_NEED_DICT,
  Z_ERRNO: So.Z_ERRNO,
  Z_STREAM_ERROR: So.Z_STREAM_ERROR,
  Z_DATA_ERROR: So.Z_DATA_ERROR,
  Z_MEM_ERROR: So.Z_MEM_ERROR,
  Z_BUF_ERROR: So.Z_BUF_ERROR,
  Z_VERSION_ERROR: So.Z_VERSION_ERROR
};
function Ro(e2, t2, r2) {
  var n2 = [], i2 = 0;
  function o2() {
    for (var t3; null !== (t3 = e2.read()); )
      n2.push(t3), i2 += t3.length;
    e2.once("readable", o2);
  }
  function a2() {
    var t3 = p.concat(n2, i2);
    n2 = [], r2(null, t3), e2.close();
  }
  e2.on("error", function(t3) {
    e2.removeListener("end", a2), e2.removeListener("readable", o2), r2(t3);
  }), e2.on("end", a2), e2.end(t2), o2();
}
function Ao(e2, t2) {
  if ("string" == typeof t2 && (t2 = new p(t2)), !$(t2))
    throw new TypeError("Not a string or buffer");
  var r2 = So.Z_FINISH;
  return e2._processChunk(t2, r2);
}
function Bo(e2) {
  if (!(this instanceof Bo))
    return new Bo(e2);
  Io.call(this, e2, So.DEFLATE);
}
function zo(e2) {
  if (!(this instanceof zo))
    return new zo(e2);
  Io.call(this, e2, So.INFLATE);
}
function Lo(e2) {
  if (!(this instanceof Lo))
    return new Lo(e2);
  Io.call(this, e2, So.GZIP);
}
function To(e2) {
  if (!(this instanceof To))
    return new To(e2);
  Io.call(this, e2, So.GUNZIP);
}
function Mo(e2) {
  if (!(this instanceof Mo))
    return new Mo(e2);
  Io.call(this, e2, So.DEFLATERAW);
}
function Co(e2) {
  if (!(this instanceof Co))
    return new Co(e2);
  Io.call(this, e2, So.INFLATERAW);
}
function Do(e2) {
  if (!(this instanceof Do))
    return new Do(e2);
  Io.call(this, e2, So.UNZIP);
}
function Io(e2, t2) {
  if (this._opts = e2 = e2 || {}, this._chunkSize = e2.chunkSize || So.Z_DEFAULT_CHUNK, Ot.call(this, e2), e2.flush && e2.flush !== So.Z_NO_FLUSH && e2.flush !== So.Z_PARTIAL_FLUSH && e2.flush !== So.Z_SYNC_FLUSH && e2.flush !== So.Z_FULL_FLUSH && e2.flush !== So.Z_FINISH && e2.flush !== So.Z_BLOCK)
    throw new Error("Invalid flush flag: " + e2.flush);
  if (this._flushFlag = e2.flush || So.Z_NO_FLUSH, e2.chunkSize && (e2.chunkSize < So.Z_MIN_CHUNK || e2.chunkSize > So.Z_MAX_CHUNK))
    throw new Error("Invalid chunk size: " + e2.chunkSize);
  if (e2.windowBits && (e2.windowBits < So.Z_MIN_WINDOWBITS || e2.windowBits > So.Z_MAX_WINDOWBITS))
    throw new Error("Invalid windowBits: " + e2.windowBits);
  if (e2.level && (e2.level < So.Z_MIN_LEVEL || e2.level > So.Z_MAX_LEVEL))
    throw new Error("Invalid compression level: " + e2.level);
  if (e2.memLevel && (e2.memLevel < So.Z_MIN_MEMLEVEL || e2.memLevel > So.Z_MAX_MEMLEVEL))
    throw new Error("Invalid memLevel: " + e2.memLevel);
  if (e2.strategy && e2.strategy != So.Z_FILTERED && e2.strategy != So.Z_HUFFMAN_ONLY && e2.strategy != So.Z_RLE && e2.strategy != So.Z_FIXED && e2.strategy != So.Z_DEFAULT_STRATEGY)
    throw new Error("Invalid strategy: " + e2.strategy);
  if (e2.dictionary && !$(e2.dictionary))
    throw new Error("Invalid dictionary: it should be a Buffer instance");
  this._binding = new So.Zlib(t2);
  var r2 = this;
  this._hadError = false, this._binding.onerror = function(e3, t3) {
    r2._binding = null, r2._hadError = true;
    var n3 = new Error(e3);
    n3.errno = t3, n3.code = So.codes[t3], r2.emit("error", n3);
  };
  var n2 = So.Z_DEFAULT_COMPRESSION;
  "number" == typeof e2.level && (n2 = e2.level);
  var i2 = So.Z_DEFAULT_STRATEGY;
  "number" == typeof e2.strategy && (i2 = e2.strategy), this._binding.init(e2.windowBits || So.Z_DEFAULT_WINDOWBITS, n2, e2.memLevel || So.Z_DEFAULT_MEMLEVEL, i2, e2.dictionary), this._buffer = new p(this._chunkSize), this._offset = 0, this._closed = false, this._level = n2, this._strategy = i2, this.once("end", this.close);
}
Object.keys(xo).forEach(function(e2) {
  xo[xo[e2]] = e2;
}), Be(Io, Ot), Io.prototype.params = function(e2, t2, r2) {
  if (e2 < So.Z_MIN_LEVEL || e2 > So.Z_MAX_LEVEL)
    throw new RangeError("Invalid compression level: " + e2);
  if (t2 != So.Z_FILTERED && t2 != So.Z_HUFFMAN_ONLY && t2 != So.Z_RLE && t2 != So.Z_FIXED && t2 != So.Z_DEFAULT_STRATEGY)
    throw new TypeError("Invalid strategy: " + t2);
  if (this._level !== e2 || this._strategy !== t2) {
    var n2 = this;
    this.flush(So.Z_SYNC_FLUSH, function() {
      n2._binding.params(e2, t2), n2._hadError || (n2._level = e2, n2._strategy = t2, r2 && r2());
    });
  } else
    de(r2);
}, Io.prototype.reset = function() {
  return this._binding.reset();
}, Io.prototype._flush = function(e2) {
  this._transform(new p(0), "", e2);
}, Io.prototype.flush = function(e2, t2) {
  var r2 = this._writableState;
  if (("function" == typeof e2 || void 0 === e2 && !t2) && (t2 = e2, e2 = So.Z_FULL_FLUSH), r2.ended)
    t2 && de(t2);
  else if (r2.ending)
    t2 && this.once("end", t2);
  else if (r2.needDrain) {
    var n2 = this;
    this.once("drain", function() {
      n2.flush(t2);
    });
  } else
    this._flushFlag = e2, this.write(new p(0), "", t2);
}, Io.prototype.close = function(e2) {
  if (e2 && de(e2), !this._closed) {
    this._closed = true, this._binding.close();
    var t2 = this;
    de(function() {
      t2.emit("close");
    });
  }
}, Io.prototype._transform = function(e2, t2, r2) {
  var n2, i2 = this._writableState, o2 = (i2.ending || i2.ended) && (!e2 || i2.length === e2.length);
  if (null === !e2 && !$(e2))
    return r2(new Error("invalid input"));
  o2 ? n2 = So.Z_FINISH : (n2 = this._flushFlag, e2.length >= i2.length && (this._flushFlag = this._opts.flush || So.Z_NO_FLUSH)), this._processChunk(e2, n2, r2);
}, Io.prototype._processChunk = function(e2, t2, r2) {
  var n2 = e2 && e2.length, i2 = this._chunkSize - this._offset, o2 = 0, a2 = this, s2 = "function" == typeof r2;
  if (!s2) {
    var h2, l2 = [], f2 = 0;
    this.on("error", function(e3) {
      h2 = e3;
    });
    do {
      var c2 = this._binding.writeSync(t2, e2, o2, n2, this._buffer, this._offset, i2);
    } while (!this._hadError && _2(c2[0], c2[1]));
    if (this._hadError)
      throw h2;
    var u2 = p.concat(l2, f2);
    return this.close(), u2;
  }
  var d2 = this._binding.write(t2, e2, o2, n2, this._buffer, this._offset, i2);
  function _2(h3, c3) {
    if (!a2._hadError) {
      var u3 = i2 - c3;
      if (function(e3, t3) {
        if (!e3)
          throw new Error(t3);
      }(u3 >= 0, "have should not go down"), u3 > 0) {
        var d3 = a2._buffer.slice(a2._offset, a2._offset + u3);
        a2._offset += u3, s2 ? a2.push(d3) : (l2.push(d3), f2 += d3.length);
      }
      if ((0 === c3 || a2._offset >= a2._chunkSize) && (i2 = a2._chunkSize, a2._offset = 0, a2._buffer = new p(a2._chunkSize)), 0 === c3) {
        if (o2 += n2 - h3, n2 = h3, !s2)
          return true;
        var g2 = a2._binding.write(t2, e2, o2, n2, a2._buffer, a2._offset, a2._chunkSize);
        return g2.callback = _2, void (g2.buffer = e2);
      }
      if (!s2)
        return false;
      r2();
    }
  }
  d2.buffer = e2, d2.callback = _2;
}, Be(Bo, Io), Be(zo, Io), Be(Lo, Io), Be(To, Io), Be(Mo, Io), Be(Co, Io), Be(Do, Io);
var Po = {
  codes: xo,
  createDeflate: function(e2) {
    return new Bo(e2);
  },
  createInflate: function(e2) {
    return new zo(e2);
  },
  createDeflateRaw: function(e2) {
    return new Mo(e2);
  },
  createInflateRaw: function(e2) {
    return new Co(e2);
  },
  createGzip: function(e2) {
    return new Lo(e2);
  },
  createGunzip: function(e2) {
    return new To(e2);
  },
  createUnzip: function(e2) {
    return new Do(e2);
  },
  deflate: function(e2, t2, r2) {
    return "function" == typeof t2 && (r2 = t2, t2 = {}), Ro(new Bo(t2), e2, r2);
  },
  deflateSync: function(e2, t2) {
    return Ao(new Bo(t2), e2);
  },
  gzip: function(e2, t2, r2) {
    return "function" == typeof t2 && (r2 = t2, t2 = {}), Ro(new Lo(t2), e2, r2);
  },
  gzipSync: function(e2, t2) {
    return Ao(new Lo(t2), e2);
  },
  deflateRaw: function(e2, t2, r2) {
    return "function" == typeof t2 && (r2 = t2, t2 = {}), Ro(new Mo(t2), e2, r2);
  },
  deflateRawSync: function(e2, t2) {
    return Ao(new Mo(t2), e2);
  },
  unzip: function(e2, t2, r2) {
    return "function" == typeof t2 && (r2 = t2, t2 = {}), Ro(new Do(t2), e2, r2);
  },
  unzipSync: function(e2, t2) {
    return Ao(new Do(t2), e2);
  },
  inflate: function(e2, t2, r2) {
    return "function" == typeof t2 && (r2 = t2, t2 = {}), Ro(new zo(t2), e2, r2);
  },
  inflateSync: function(e2, t2) {
    return Ao(new zo(t2), e2);
  },
  gunzip: function(e2, t2, r2) {
    return "function" == typeof t2 && (r2 = t2, t2 = {}), Ro(new To(t2), e2, r2);
  },
  gunzipSync: function(e2, t2) {
    return Ao(new To(t2), e2);
  },
  inflateRaw: function(e2, t2, r2) {
    return "function" == typeof t2 && (r2 = t2, t2 = {}), Ro(new Co(t2), e2, r2);
  },
  inflateRawSync: function(e2, t2) {
    return Ao(new Co(t2), e2);
  },
  Deflate: Bo,
  Inflate: zo,
  Gzip: Lo,
  Gunzip: To,
  DeflateRaw: Mo,
  InflateRaw: Co,
  Unzip: Do,
  Zlib: Io
};
class LibGenerateTestUserSig {
  constructor(e2, t2, r2) {
    this.SDKAPPID = e2, this.EXPIRETIME = r2, this.PRIVATEKEY = t2;
  }
  genTestUserSig(e2) {
    return this._isNumber(this.SDKAPPID) ? this._isString(this.PRIVATEKEY) ? this._isString(e2) ? this._isNumber(this.EXPIRETIME) ? (formatAppLog("log", "at debug/lib-generate-test-usersig-es.min.js:6138", "sdkAppID=" + this.SDKAPPID + " key=" + this.PRIVATEKEY + " userID=" + e2 + " expire=" + this.EXPIRETIME), this.genSigWithUserbuf(e2, this.EXPIRETIME, null)) : (formatAppLog("error", "at debug/lib-generate-test-usersig-es.min.js:6138", "expireTime must be a number"), "") : (formatAppLog("error", "at debug/lib-generate-test-usersig-es.min.js:6138", "userID must be a string"), "") : (formatAppLog("error", "at debug/lib-generate-test-usersig-es.min.js:6138", "privateKey must be a string"), "") : (formatAppLog("error", "at debug/lib-generate-test-usersig-es.min.js:6138", "sdkAppID must be a number"), "");
  }
  newBuffer(e2, t2) {
    return p.from ? p.from(e2, t2) : new p(e2, t2);
  }
  unescape(e2) {
    return e2.replace(/_/g, "=").replace(/\-/g, "/").replace(/\*/g, "+");
  }
  escape(e2) {
    return e2.replace(/\+/g, "*").replace(/\//g, "-").replace(/=/g, "_");
  }
  encode(e2) {
    return this.escape(this.newBuffer(e2).toString("base64"));
  }
  decode(e2) {
    return this.newBuffer(this.unescape(e2), "base64");
  }
  base64encode(e2) {
    return this.newBuffer(e2).toString("base64");
  }
  base64decode(e2) {
    return this.newBuffer(e2, "base64").toString();
  }
  _hmacsha256(e2, t2, r2, n2) {
    let i2 = "TLS.identifier:" + e2 + "\n";
    i2 += "TLS.sdkappid:" + this.SDKAPPID + "\n", i2 += "TLS.time:" + t2 + "\n", i2 += "TLS.expire:" + r2 + "\n", null != n2 && (i2 += "TLS.userbuf:" + n2 + "\n");
    let o2 = te.HmacSHA256(i2, this.PRIVATEKEY);
    return te.enc.Base64.stringify(o2);
  }
  _utc() {
    return Math.round(Date.now() / 1e3);
  }
  _isNumber(e2) {
    return null !== e2 && ("number" == typeof e2 && !isNaN(e2 - 0) || "object" == typeof e2 && e2.constructor === Number);
  }
  _isString(e2) {
    return "string" == typeof e2;
  }
  genSigWithUserbuf(e2, t2, r2) {
    let n2 = this._utc(), i2 = {
      "TLS.ver": "2.0",
      "TLS.identifier": e2,
      "TLS.sdkappid": this.SDKAPPID,
      "TLS.time": n2,
      "TLS.expire": t2
    }, o2 = "";
    if (null != r2) {
      let a3 = this.base64encode(r2);
      i2["TLS.userbuf"] = a3, o2 = this._hmacsha256(e2, n2, t2, a3);
    } else
      o2 = this._hmacsha256(e2, n2, t2, null);
    i2["TLS.sig"] = o2;
    let a2 = JSON.stringify(i2), s2 = Po.deflateSync(this.newBuffer(a2)).toString("base64"), h2 = this.escape(s2);
    return formatAppLog("log", "at debug/lib-generate-test-usersig-es.min.js:6208", "ret=" + h2), h2;
  }
  validate(e2) {
    let t2 = this.decode(e2), r2 = Po.inflateSync(t2);
    formatAppLog("log", "at debug/lib-generate-test-usersig-es.min.js:6214", "validate ret=" + r2);
  }
}
const SDKAPPID = 1400790383;
const EXPIRETIME = 604800;
const SECRETKEY = "9c8719393bcae45adde034cf282c01b8b8c8ea31a3bb4431bff49f8449ba40d7";
function genTestUserSig(userID) {
  const generator = new LibGenerateTestUserSig(SDKAPPID, SECRETKEY, EXPIRETIME);
  const userSig = generator.genTestUserSig(userID);
  return {
    sdkAppId: SDKAPPID,
    userSig
  };
}
const _export_sfc = (sfc, props) => {
  const target = sfc.__vccOpts || sfc;
  for (const [key, val] of props) {
    target[key] = val;
  }
  return target;
};
const _sfc_main$1 = {
  name: "TrtcLocalView",
  props: {
    viewId: {
      type: String,
      default: ""
    }
  }
};
function _sfc_render$1(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_TRTCCloudUniPlugin_TXLocalViewComponent = resolveComponent("TRTCCloudUniPlugin-TXLocalViewComponent");
  return openBlock(), createBlock(_component_TRTCCloudUniPlugin_TXLocalViewComponent, { viewId: $props.viewId }, null, 8, ["viewId"]);
}
const TrtcLocalView = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["render", _sfc_render$1]]);
Math.floor(Math.random() * 1e5).toString();
"user_" + Math.floor(Math.random() * 1e5).toString();
const _sfc_main = {
  components: {
    TrtcLocalView
  },
  onLoad(options) {
    formatAppLog("log", "at pages/examples/multiCall/index.nvue:40", options, "检测页");
  },
  data() {
    return {
      trtcCloud: null,
      sdkAppId: 0,
      userSig: "",
      roomId: 194776,
      userId: "user_" + Math.floor(Math.random() * 1e5).toString(),
      appScene: TRTCAppScene.TRTCAppSceneVideoCall,
      // TRTCAppSceneVideoCall
      isFrontCamera: true,
      streamType: TRTCVideoStreamType.TRTCVideoStreamTypeBig,
      remoteUserIdList: [],
      remoteAudioMuteState: false
    };
  },
  mounted() {
    if (uni.getSystemInfoSync().platform === "android") {
      permision.requestAndroidPermission("android.permission.RECORD_AUDIO");
      permision.requestAndroidPermission("android.permission.CAMERA");
    }
    const { sdkAppId, userSig } = genTestUserSig(this.userId);
    this.sdkAppId = sdkAppId;
    this.userSig = userSig;
    if (!sdkAppId || !userSig) {
      uni.showToast({
        title: "请填写 sdkAppId、userSig",
        icon: "none"
      });
    }
    this.enterRoom();
    this.startLocalPreview();
  },
  unmounted() {
    this.handleUninstallEvents();
  },
  onUnload() {
    this.destroyInstance();
    formatAppLog("log", "at pages/examples/multiCall/index.nvue:80", "- onUnload");
  },
  onBackPress() {
    this.destroyInstance();
  },
  methods: {
    bindInputChange(event, type) {
      const value = event.detail.value;
      switch (type) {
        case "roomId": {
          this.roomId = value;
          break;
        }
        case "userId": {
          this.userId = value;
          break;
        }
      }
    },
    destroyInstance() {
      if (this.trtcCloud) {
        TrtcCloud.destroyInstance();
        this.trtcCloud = null;
        uni.showToast({
          title: "销毁实例",
          icon: "none"
        });
      }
    },
    createTrtcCloud() {
      uni.showToast({
        title: "创建实例 ",
        icon: "none"
      });
      this.trtcCloud = TrtcCloud.createInstance();
      this.handleEvents();
    },
    enterRoom() {
      try {
        this.createTrtcCloud();
        const { roomId, userId, sdkAppId, appScene, userSig } = this;
        let param = { roomId: +roomId, userId, sdkAppId, userSig };
        this.trtcCloud.enterRoom(param, appScene);
        uni.showToast({
          title: "enterRoom ",
          icon: "none"
        });
        this.startLocalAudio();
        setTimeout(() => {
          formatAppLog("log", "at pages/examples/multiCall/index.nvue:134", "退出");
          this.exitRoom();
        }, 15 * 1e3);
      } catch (error) {
        formatAppLog("log", "at pages/examples/multiCall/index.nvue:139", `enterRoom error = ${JSON.stringify(error)}`);
      }
    },
    exitRoom() {
      try {
        this.stopLocalPreview();
        this.stopRemoteView();
        this.stopLocalAudio();
        this.trtcCloud.exitRoom();
        this.remoteUserIdList = [];
        this.startLocalPreview();
        setTimeout(() => {
          formatAppLog("log", "at pages/examples/multiCall/index.nvue:151", "开启");
          this.enterRoom();
        }, 10 * 1e3);
      } catch (e2) {
      }
    },
    // 本地预览
    startLocalPreview() {
      this.trtcCloud.startLocalPreview(this.isFrontCamera, this.userId);
      uni.showToast({
        title: "localPreview",
        icon: "none"
      });
    },
    stopLocalPreview() {
      this.trtcCloud.stopLocalPreview();
    },
    startLocalAudio() {
      this.trtcCloud.startLocalAudio();
    },
    stopLocalAudio() {
      this.trtcCloud.stopLocalAudio();
    },
    stopRemoteView() {
      for (let i2 = 0; i2 < this.remoteUserIdList.length; i2++) {
        this.trtcCloud.stopRemoteView(this.remoteUserIdList[i2], this.streamType);
      }
    },
    muteAllRemoteAudio() {
      this.remoteAudioMuteState = !this.remoteAudioMuteState;
      this.trtcCloud.muteAllRemoteAudio(this.remoteAudioMuteState);
    },
    handleEvents() {
      this.trtcCloud.on("onWarning", (res) => {
        uni.showToast({
          title: `onWarning: ${JSON.stringify(res)}`,
          icon: "none"
        });
      });
      this.trtcCloud.on("onError", (res) => {
        formatAppLog("log", "at pages/examples/multiCall/index.nvue:192", "- onError: ", JSON.stringify(res));
        uni.showToast({
          title: `error: ${JSON.stringify(res)}`,
          icon: "none"
        });
      });
      this.trtcCloud.on("onEnterRoom", (result) => {
        formatAppLog("log", "at pages/examples/multiCall/index.nvue:199", `- onEnterRoom = ${result}`);
        if (result > 0) {
          uni.showToast({
            title: `进房成功，耗时: ${result}ms`,
            icon: "none"
          });
        } else {
          formatAppLog("log", "at pages/examples/multiCall/index.nvue:206", `enter room failed，error code = ${result}`);
        }
      });
      this.trtcCloud.on("onExitRoom", (reason) => {
        const reasonList = ["主动调用 exitRoom 退房", "被服务器踢出当前房间", "当前房间整个被解散"];
        uni.showToast({
          title: `退房 ${reasonList[reason]}`,
          icon: "none",
          duration: 1e3
        });
      });
      this.trtcCloud.on("onFirstVideoFrame", (res) => {
        formatAppLog("log", "at pages/examples/multiCall/index.nvue:218", `渲染的首帧画面响应 = ${JSON.stringify(res)}`);
      });
      this.trtcCloud.on("onRemoteUserEnterRoom", (userId) => {
        if (!this.remoteUserIdList.includes(userId)) {
          this.remoteUserIdList.push(userId);
        }
        uni.showToast({
          title: `远端进房: userId = ${userId}`,
          icon: "none"
        });
      });
      this.trtcCloud.on("onUserVideoAvailable", (res) => {
        const {
          userId,
          available
        } = res;
        if (userId && available && this.remoteUserIdList.includes(userId)) {
          this.trtcCloud.startRemoteView(userId, this.streamType, userId);
        }
      });
      this.trtcCloud.on("onRemoteUserLeaveRoom", (res) => {
        const { userId, reason } = res;
        if (userId) {
          const list = this.remoteUserIdList.filter((val) => val !== userId);
          this.remoteUserIdList = list;
        }
      });
      this.trtcCloud.on("onUserSubStreamAvailable", (res) => {
        const { userId, available } = res;
        formatAppLog("log", "at pages/examples/multiCall/index.nvue:247", "onUserSubStreamAvailable ", userId, available);
      });
    },
    handleUninstallEvents() {
      this.trtcCloud.off("*");
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_trtc_local_view = resolveComponent("trtc-local-view");
  const _component_button = resolveComponent("button");
  return openBlock(), createElementBlock("scroll-view", {
    scrollY: true,
    showScrollbar: true,
    enableBackToTop: true,
    bubble: "true",
    style: { flexDirection: "column" }
  }, [
    createElementVNode("div", { class: "trtc-container" }, [
      createElementVNode("view", { class: "trtc-video-area" }, [
        createElementVNode("view", {
          class: "trtc-video-view",
          id: "root"
        }, [
          createVNode(_component_trtc_local_view, {
            viewId: $data.userId,
            style: { "height": "803.84rpx", "flex": "1" }
          }, null, 8, ["viewId"])
        ])
      ]),
      createElementVNode("view", { class: "trtc-row-area" }, [
        createElementVNode("u-text", { class: "trtc-title-text" }, "roomId"),
        createElementVNode("u-input", { type: "text" }),
        createElementVNode("u-input", {
          class: "trtc-input",
          style: { "width": "182.69rpx" },
          type: "number",
          autoBlur: "true",
          value: $data.roomId,
          onInput: _cache[0] || (_cache[0] = ($event) => $options.bindInputChange($event, "roomId"))
        }, null, 40, ["value"]),
        createElementVNode("u-text", { class: "trtc-title-text" }, "userId"),
        createElementVNode("u-input", {
          class: "trtc-input",
          style: { "width": "182.69rpx" },
          autoBlur: "true",
          value: $data.userId,
          onInput: _cache[1] || (_cache[1] = ($event) => $options.bindInputChange($event, "userId"))
        }, null, 40, ["value"])
      ]),
      createElementVNode("view", { class: "trtc-line" }),
      createVNode(_component_button, {
        class: "trtc-btn-long",
        style: { "margin-top": "20rpx" },
        type: "primary",
        onClick: $options.enterRoom
      }, {
        default: withCtx(() => [
          createTextVNode("加入房间")
        ]),
        _: 1
      }, 8, ["onClick"]),
      createVNode(_component_button, {
        class: "trtc-btn-long",
        style: { "margin-top": "20rpx" },
        type: "primary",
        onClick: $options.startLocalPreview
      }, {
        default: withCtx(() => [
          createTextVNode("浏览视频流")
        ]),
        _: 1
      }, 8, ["onClick"]),
      createVNode(_component_button, {
        class: "trtc-btn-long",
        style: { "margin-top": "20rpx" },
        type: "primary",
        onClick: $options.muteAllRemoteAudio
      }, {
        default: withCtx(() => [
          createTextVNode(toDisplayString($data.remoteAudioMuteState ? "Unmute" : "Mute") + " All Remote Audio", 1)
        ]),
        _: 1
      }, 8, ["onClick"]),
      createVNode(_component_button, {
        class: "trtc-btn-long",
        style: { "margin-top": "20rpx" },
        type: "primary",
        onClick: $options.exitRoom
      }, {
        default: withCtx(() => [
          createTextVNode("退出房间")
        ]),
        _: 1
      }, 8, ["onClick"]),
      createVNode(_component_button, {
        class: "trtc-btn-long",
        type: "primary",
        onClick: $options.destroyInstance
      }, {
        default: withCtx(() => [
          createTextVNode("摧毁示例")
        ]),
        _: 1
      }, 8, ["onClick"])
    ])
  ]);
}
const index = /* @__PURE__ */ _export_sfc(_sfc_main, [["render", _sfc_render]]);
export {
  index as default
};
