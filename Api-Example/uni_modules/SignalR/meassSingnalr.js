var app = app || {};
(function () {
    //Check if SignalR is defined
    if (!SignalR) {
        return;
    }
    //Create namespaces
    app.message = app.message || {};

    app.signalr = app.signalr || {};
    app.signalr.hubs = app.signalr.hubs || {};
    app.signalr.reconnectTime = app.signalr.reconnectTime || 5000;
    app.signalr.maxTries = app.signalr.maxTries || 8;
    app.signalr.increaseReconnectTime = app.signalr.increaseReconnectTime || function (time) {
        return time * 2;
    };

    

    var messageHub = null;

    // Configure the connection
    function configureConnection(connection) {
        // Set the common hub
        app.signalr.hubs.message = connection;
        messageHub = connection;

        let reconnectTime = 5000;
        let tries = 1;
        let maxTries = 8;

        function tryReconnect() {
            if (tries > maxTries) {
                return;
            } else {
                connection.start()
                    .then(function () {
                        reconnectTime = 5000;
                        tries = 1;
                    }).catch(function () {
                        tries += 1;
                        reconnectTime *= 2;
                        setTimeout(function () {
                            tryReconnect();
                        }, reconnectTime);
                    });
            }
        }

        // Reconnect if hub disconnects
        connection.onclose(function (e) {
            if (e) {
                abp.log.debug('Chat connection closed with error: ' + e);
            }
            else {
                abp.log.debug('Chat disconnected');
            }

            if (!app.signalr.autoConnect) {
                return;
            }
            tryReconnect();
        });

        // Register to get notifications
        registerMessageEvents(connection);
    }

    // Connect to the server
    app.signalr.connect = function () {
        // Start the connection.
        //adminPath
        /*'http://localhost:44302/' */
		console.log("Singnalr,------------------------------")
        startConnection( "https://admin.cexam.com.cn/signalr-message?MiniStudentId=208199", configureConnection)
            .then(function (connection) {
                abp.log.debug('Connected to SignalR MESSAGE server!');
                //abp.event.trigger('app.chat.connected');
                // Call the Register method on the hub.
                connection.invoke('register').then(function () {
                    abp.log.debug('Registered to the SignalR MESSAGE server!');
                });
            })
            .catch(function (error) {
                abp.log.debug(error.message);
            });
    };

    // Starts a connection with transport fallback - if the connection cannot be started using
    // the webSockets transport the function will fallback to the serverSentEvents transport and
    // if this does not work it will try longPolling. If the connection cannot be started using
    // any of the available transports the function will return a rejected Promise.
    function startConnection(url, configureConnection) {
        if (app.signalr.remoteServiceBaseUrl) {
            url = app.signalr.remoteServiceBaseUrl + url;
        }

        // Add query string: https://github.com/aspnet/SignalR/issues/680
        if (signalrQs) {
            url += '?' + signalrQs;
        }

        return function start(transport) {
            abp.log.debug('Starting connection using ' + signalR.HttpTransportType[transport] + ' transport');

            var connection = new signalR.HubConnectionBuilder()
                .withUrl(url, transport)
                .configureLogging(signalR.LogLevel.Error)
                .build();


            if (configureConnection && typeof configureConnection === 'function') {
                configureConnection(connection);
            }

            return connection.start()
                .then(function () {
                    return connection;
                })
                .catch(function (error) {
                    abp.log.debug('Cannot start the connection using ' + signalR.HttpTransportType[transport] + ' transport. ' + error.message);
                    if (transport !== signalR.HttpTransportType.LongPolling) {
                        return start(transport + 1);
                    }

                    return Promise.reject(error);
                });
        }(signalR.HttpTransportType.WebSockets);
    }

    app.signalr.startConnection = startConnection;

    if (app.signalr.autoConnect === undefined) {
        app.signalr.autoConnect = true;
    }

    if (app.signalr.autoConnect) {
        app.signalr.connect();
    }

    app.signalr.autoConnect = app.signalr.autoConnect === undefined ? true : app.signalr.autoConnect;
    app.signalr.autoReconnect = app.signalr.autoReconnect === undefined ? true : app.signalr.autoReconnect;
    app.signalr.connect = app.signalr.connect ;
    app.signalr.startConnection = app.signalr.startConnection || startConnection;

    function registerMessageEvents(connection) {
        connection.on('getMessage', function (message) {
            if (message.audioUrl) {
                abp.message.warn('接收到监考老师发来的语音消息，请注意收听！');
                abp.event.trigger('app.message.getAudioMessage', message.audioUrl);
            } else {
                abp.message.warn(message.message, '接收到监考老师发来的消息');
            }
        });
        connection.on('getExamEnded', function (message) {
            abp.message.warn('已被监考老师强制交卷', '接收到监考老师发来的消息');
            window.location = '/test/complete';
        });

        connection.on('getMonitoring', function (streamTypes) {
            abp.event.trigger('app.message.getMonitoring', streamTypes);
        });

        connection.on('getRecording', function (setRecord) {
            abp.event.trigger('app.message.getRecording', setRecord);
        });

        // connection.on('getMiniHeart', function () {
        //     abp.event.trigger('app.message.getMiniHeart');
        // });
        connection.on('sendStudentCamera', function () {
            abp.event.trigger('app.message.sendStudentCamera');
        });
        connection.on('changeSetting', function (message) {
            if (message.settingType == 'addedTestpaperDuration') {
                abp.event.trigger('app.message.addedTestpaperDuration', message);
            } else if (message.settingType == 'addedUnitDuration'){
                abp.event.trigger('app.message.addedUnitDuration', message);
            } else if (message.settingType == 'changeUnit') {
                abp.event.trigger('app.message.changeUnit', message);
            } else if (message.settingType == 'clearInfo') {
                abp.event.trigger('app.message.clearInfo');
            } else if (message.settingType == 'move') {
                abp.event.trigger('app.message.move',message);
            }
        });
        connection.on('getCloseFace', function () {
            location.reload();
        });
        connection.on('getCloseFaceCheatDetection', function () {
            abp.event.trigger('app.message.getCloseFaceCheatDetection');
        });
        connection.on('getPassFace', function () {
            abp.event.trigger('app.message.getPassFace');
        });
    }

    app.message.sendStudentState = function (data, callback) {
        if (messageHub.connection.connectionState !== signalR.HubConnectionState.Connected) {
            callback && callback();
            return;
        }

        messageHub.invoke('sendStudentState', data).then(function (result) {
            callback && callback();
        });
    }
    app.message.setExamEnded = function (messageData, callback) {
        if (messageHub.connection.connectionState !== signalR.HubConnectionState.Connected) {
            callback && callback();
            return;
        }

        messageHub.invoke('setExamEnded', messageData).then(function (result) {
            callback && callback();
        });
    }
    app.message.sendStreamState = function (publishNotice, callback) {
        if (messageHub.connection.connectionState !== signalR.HubConnectionState.Connected) {
            callback && callback();
            return;
        }

        messageHub.invoke('sendStreamState', publishNotice).then(function (result) {
            callback && callback();
        });
    }

    app.message.getMiniCarema = function (data, callback) {
        if (messageHub.connection.connectionState !== signalR.HubConnectionState.Connected) {
            callback && callback();
            return;
        }

        messageHub.invoke('getMiniCarema', data).then(function (result) {
            callback && callback();
        });
    }

    app.message.sendUpdateEvents = function (eventInput, callback) {
        if (messageHub.connection.connectionState !== signalR.HubConnectionState.Connected) {
            callback && callback();
            return;
        }

        messageHub.invoke('sendUpdateEvents', eventInput).then(function (result) {
            callback && callback();
        });
    }

    app.message.cheatSnap = function (data, callback) {
        if (messageHub.connection.connectionState !== signalR.HubConnectionState.Connected) {
            callback && callback();
            return;
        }

        messageHub.invoke('cheatSnap', data).then(function (result) {
            callback && callback();
        });
    }

    app.message.sendHelp = function (data, callback) {
        if (messageHub.connection.connectionState !== signalR.HubConnectionState.Connected) {
            callback && callback();
            return;
        }

        messageHub.invoke('sendHelp', data).then(function (result) {
            callback && callback();
        });
    }
})();   