## 1.3.5（2021-12-03）
更新文档，新增链接失败，断开重试示例
## 1.3.4（2021-12-03）
更新文档
## 1.3.3（2021-12-02）
更新文档，新增链接失败，断开重试示例
## 1.3.2（2021-12-02）
更新文档，新增链接失败，断开重试示例
## 1.3.1（2021-02-26）
* 更新兼容SSE， LongPolling
* 更新支持uni_modules，
* 更新示例项目
